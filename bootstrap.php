<?php
/**
 * bootstrap
 *
 * @package Sngine
 * @author Zamblek
 */

define('SYS_VER', '2.5.1');
define('ABSPATH', dirname(__FILE__) . '/');
define('BASEPATH', dirname($_SERVER['PHP_SELF']));

apcu_clear_cache();

// $memcache_obj = memcache_connect('127.0.0.1', 11211);

// memcache_flush($memcache_obj);

require_once(ABSPATH . 'vendor/autoload.php');
require_once(ABSPATH . 'includes/config.php');
if(!empty($_REQUEST['chet'])){
  require_once(ABSPATH . 'includes/custom.php');
}
require_once(ABSPATH . 'includes/bugsnag.php');

ini_set("display_errors", false);
  error_reporting(E_ALL ^ E_NOTICE);

/*if (DEBUGGING) {
  ini_set("display_errors", true);
  error_reporting(E_ALL ^ E_NOTICE);
} else {
  ini_set("display_errors", false);
  error_reporting(0);
}*/

require_once(ABSPATH . 'includes/functions.php');

function getLanguages($db, &$system)
{
  $languages = [];
  $get_languages = $db->query("SELECT * FROM system_languages WHERE enabled = '1'") or _error(SQL_ERROR);
  while ($language = $get_languages->fetch_assoc()) {
    /* set system langauge */
    if (isset($_COOKIE['s_lang'])) {
      if ($_COOKIE['s_lang'] == $language['code']) {
        $system['language'] = $language;
        T_setlocale(LC_MESSAGES, $system['language']['code']);
      }
    } else {
      if (($language['default'])) {
        $system['language'] = $language;
        T_setlocale(LC_MESSAGES, $system['language']['code']);
      }
    }
    $languages[] = $language;
  }
  $system['languages'] = $languages;
}

function getTheme($db)
{
  $get_theme = $db->query("SELECT * FROM system_themes WHERE system_themes.default = '1'") or _error(SQL_ERROR);
  $theme = $get_theme->fetch_assoc();
  return $theme['name'];
}

function incrementFollow($db){
	$get_theme = $db->query("SELECT `boss_followers`,`followers_date` FROM users WHERE users.user_id = '356'") or _error(SQL_ERROR);
	$theme = $get_theme->fetch_assoc();
	return $theme;
}

function incrementPostViews($db, $id)
{
  $db->query("UPDATE `broadcasts` SET views = views+1 WHERE `post_id` = " . (int)$id);
}

function getStaticPages($db)
{
  $static_pages = array();
  $get_static = $db->query("SELECT page_url, page_title FROM static_pages WHERE in_footer = '1'") or _error(SQL_ERROR);
  if ($get_static->num_rows > 0) {
    while ($static_page = $get_static->fetch_assoc()) {
      $static_pages[] = $static_page;
    }
  }
  return $static_pages;
}

function getSystemDate()
{
  //some magic goes here...
  //TODO what is this, just moved to function
  $minutes_to_add = 0;
  $DateTime = new DateTime();
  $DateTime->add(new DateInterval('PT' . $minutes_to_add . 'M'));
  return $DateTime->format('Y-m-d H:i:s');
}



session_start();
if (!isset($_SESSION['secret'])) {
  $_SESSION['secret'] = get_hash_token();
}

// i18n config
T_setlocale(LC_MESSAGES, DEFAULT_LOCALE);
$domain = 'messages';
T_bindtextdomain($domain, ABSPATH . 'content/languages/locale');
T_bind_textdomain_codeset($domain, 'UTF-8');
T_textdomain($domain);

$db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$db->set_charset('utf8mb4');
if (mysqli_connect_error()) {
  _error(DB_ERROR);
}


//NOTICE: global var $system in storing in config file
getLanguages($db, $system);
$system['theme'] = getTheme($db);
$staticPages = getStaticPages($db);
$date = getSystemDate();

if (isset($_GET['post_id']) || is_numeric($_GET['post_id'])) {
  incrementPostViews($db, (int)$_GET['post_id']);
}


// smarty config
$smarty = new Smarty;
$smarty->template_dir = ABSPATH . 'content/themes/' . $system['theme'] . '/templates';
$smarty->compile_dir = ABSPATH . 'content/themes/' . $system['theme'] . '/templates_compiled';
$smarty->cache_dir = ABSPATH . 'content/themes/' . $system['theme'] . '/cache';
$smarty->loadFilter('output', 'trimwhitespace');

require_once(ABSPATH . 'includes/class-aws-s3.php');
require_once(ABSPATH . 'includes/class-email.php');

// get user & online friends if chat enabled
//$cache = new Cache();
//global $cache;

try {
  $user = new User();
  if ($user->_logged_in) {
    // [1] get online friends
    if ($user->_data['user_chat_enabled']) {
      /* get online friends */
      $online_friends = array();//$user->get_online_friends();
      /* assign online friends */
      $smarty->assign('online_friends', $online_friends);
    }
    // [2] check if user subscribed
    if ($system['packages_enabled']) {
      $user->check_user_package();
    }
  }
} catch (Exception $e) {
  _error(SQL_ERROR);
}


// init affiliates system
$user->init_affiliates();


// check if system is live
if (!$system['system_live'] && ((!$user->_logged_in && !isset($override_shutdown)) || ($user->_logged_in && $user->_data['user_group'] != 1))) {
  _error(__('System Message'), "<p class='text-center'>" . $system['system_message'] . "</p>");
}


// check if the viewer is banned
if ($user->_logged_in && $user->_data['user_group'] != '1' && $user->_data['user_banned']) {
  _error(__("System Message"), __("Your account has been blocked"));
}

// auto incremental Follwers
$a = incrementFollow($db);
if($a['followers_date'] != date("Y-m-d")){
	if(date(l) == 'Saturday'){
		$incr = 35000;
	}
	elseif(date(l) == 'Sunday'){
		$incr = 25000;
	}
	else{
		$incr = 30000;
	}
	$now = $a['boss_followers'] + $incr;
	$cur_date = date("Y-m-d");
	$status = $db->query("UPDATE users SET `boss_followers` = $now,`followers_date` = CURDATE() WHERE users.user_id = '356'") or _error(SQL_ERROR);
}
$totoalFollowers = $a['boss_followers'] + count($user->get_followers_ids(356));

// get ads (header & footer)
$ads_master['header'] = $user->ads('header');
$ads_master['footer'] = $user->ads('footer');

// assign system varibles
$smarty->assign('secret', $_SESSION['secret']);
$smarty->assign('session_hash', session_hash($system['session_hash']));
$smarty->assign('__', __);
$smarty->assign('system', $system);
$smarty->assign('date', $date);
$smarty->assign('static_pages', $staticPages);
$smarty->assign('user', $user);
$smarty->assign('totoalFollowers', $totoalFollowers);
$smarty->assign('ads_master', $ads_master);
