<?php

require '../vendor/autoload.php';

require '../vendor/phpFFmpeg/autoload.php';

use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\MultipartUploader;
use Aws\S3\S3Client;

set_time_limit("0");
ini_set('memory_limit', '-1');


require_once('../bootstrap.php');
require_once('../includes/class-image.php');
require_once('../includes/SimpleImage.php');

//date_default_timezone_set( 'Asia/Tashkent' );

//user_error( print_r( $_FILES, true ) );

$uploads_dir = 'tmp/';
$target_path = $uploads_dir . basename( $_FILES[ 'file' ][ 'name' ] );

if ( move_uploaded_file( $_FILES[ 'file' ][ 'tmp_name' ], $target_path ) )
{
    echo 'File uploaded: ' . $target_path;
}
else
{
    echo 'Error in uploading file ' . $target_path;
}