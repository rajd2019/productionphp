<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Upload File using jQuery.ajax() with progress support</title>
	
<style>
/* progress bar */
#progress-wrp {
    border: 1px solid #0099CC;
    padding: 1px;
    position: relative;
    border-radius: 3px;
    margin: 10px;
    text-align: left;
    background: #fff;
    box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
}
#progress-wrp .progress-bar{
	height: 20px;
    border-radius: 3px;
    background-color: #f39ac7;
    width: 0;
    box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
}
#progress-wrp .status{
	top:3px;
	left:50%;
	position:absolute;
	display:inline-block;
	color: #000000;
}
</style>	
</head>
<body>


<input type="file" name="file" id="sel-file"/>
<button id="send">Send</button>

<div id="progress-wrp"><div class="progress-bar"></div ><div class="status">0%</div></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
    jQuery( document ).ready( function ()
    {
		var progress_bar_id 		= '#progress-wrp';
		//reset progressbar
		$(progress_bar_id +" .progress-bar").css("width", "0%");
		$(progress_bar_id + " .status").text("0%");
	
        $( '#send' ).on( 'click', function ()
        {
            var file = $( '#sel-file' ).get( 0 ).files[0],
                formData = new FormData();
            
            formData.append( 'file', file );
            console.log( file );
            $.ajax( {
                url        : 'https://www.guo.media/save-file-aws.php',
                type       : 'POST',
                contentType: false,
                cache      : false,
                processData: false,
                data       : formData,
                xhr        : function ()
                {
                    /*var jqXHR = null;
                    if ( window.ActiveXObject )
                    {
                        jqXHR = new window.ActiveXObject( "Microsoft.XMLHTTP" );
                    }
                    else
                    {
                        jqXHR = new window.XMLHttpRequest();
                    }
                    //Upload progress
                    jqXHR.upload.addEventListener( "progress", function ( evt )
                    {
                        if ( evt.lengthComputable )
                        {
                            var percentComplete = Math.round( (evt.loaded * 100) / evt.total );
                            //Do something with upload progress
                            console.log( 'Uploaded percent', percentComplete );
                        }
                    }, false );
                    //Download progress
                    jqXHR.addEventListener( "progress", function ( evt )
                    {
                        if ( evt.lengthComputable )
                        {
                            var percentComplete = Math.round( (evt.loaded * 100) / evt.total );
                            //Do something with download progress
                            console.log( 'Downloaded percent', percentComplete );
                        }
                    }, false );
                    return jqXHR;*/
					
					var xhr = $.ajaxSettings.xhr();
					if (xhr.upload) {
						xhr.upload.addEventListener('progress', function(event) {
							var percent = 0;
							var position = event.loaded || event.position;
							var total = event.total;
							if (event.lengthComputable) {
								percent = Math.ceil(position / total * 100);
							}
							//update progressbar
							$(progress_bar_id +" .progress-bar").css("width", + percent +"%");
							$(progress_bar_id + " .status").text(percent +"%");
						}, true);
					}
					return xhr;
                },
                success    : function ( data )
                {
                    //Do something success-ish
                    console.log( 'Completed.' );
					alert('Completed')
                }
            } );
        } );
    } );
</script>

</body>
</html>