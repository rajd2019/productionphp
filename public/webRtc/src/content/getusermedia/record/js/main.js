/*
*  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
*
*  Use of this source code is governed by a BSD-style license
*  that can be found in the LICENSE file in the root of the source
*  tree.
*/

// This code is adapted from
// https://rawgit.com/Miguelao/demos/master/mediarecorder.html

'use strict';

/* globals MediaRecorder */

var mediaSource = new MediaSource();
mediaSource.addEventListener('sourceopen', handleSourceOpen, false);
var mediaRecorder;
var recordedBlobs;
var sourceBuffer;

var recordedVideo = document.querySelector('video#recorded');
recordedVideo.addEventListener('error', function(ev) {
  console.error('MediaRecording.recordedMedia.error()');
  alert(`Your browser can not play ${recordedVideo.src} media clip. event: ${JSON.stringify(ev)}`);
}, true);

var recordButton = document.querySelector('button#record');
recordButton.addEventListener('click', () => {
  if (recordButton.textContent === 'Start Recording') {
    startRecording();
  } else {
    stopRecording();
    recordButton.textContent = 'Start Recording';
    playButton.disabled = false;
    downloadButton.disabled = false;
  }
});

var playButton = document.querySelector('button#play');
playButton.addEventListener('click', () => {
  var superBuffer = new Blob(recordedBlobs, {type: 'video/webm'});
  recordedVideo.src = window.URL.createObjectURL(superBuffer);
  // workaround for non-seekable video taken from
  // https://bugs.chromium.org/p/chromium/issues/detail?id=642012#c23
  recordedVideo.addEventListener('loadedmetadata', () => {
    if (recordedVideo.duration === Infinity) {
      recordedVideo.currentTime = 1e101;
      recordedVideo.ontimeupdate = function() {
        recordedVideo.currentTime = 0;
        recordedVideo.ontimeupdate = function() {
          delete recordedVideo.ontimeupdate;
          recordedVideo.play();
        };
      };
    } else {
      recordedVideo.play();
    }
  });
});

var downloadButton = document.querySelector('button#download');
downloadButton.addEventListener('click', () => {
		
	var blob = new Blob(recordedBlobs, {type: 'video/mp4'});
	var url = window.URL.createObjectURL(blob);
	var a = document.createElement('a');

	/*a.style.display = 'none';
	a.href = url;
	a.download = 'test.webm';
	document.body.appendChild(a);
	a.click();
	setTimeout(() => {
	document.body.removeChild(a);
	window.URL.revokeObjectURL(url);
	}, 100);*/
	
	//var blob2 = recordedBlobs.getBlob();
	
	// generating a random file name
	var fileName = getFileName('mp4');

	// we need to upload "File" --- not "Blob"
	var fileObject = new File([blob], fileName, {
		type: 'video/mp4'
	});
	
	var formData = new FormData();
	
	// recorded data
    formData.append('video-blob', fileObject);
						
	// file name
    formData.append('video-filename', fileObject.name);
	
	// file name
    formData.append('type', 'take-video');
	
	var progress_bar_id 		= '#progress-wrp';
	//reset progressbar
	$(progress_bar_id +" .progress-bar").css("width", "0%");
	$(progress_bar_id + " .status").text("0%");
		
	$.ajax({
		url: 'https://www.guo.media/includes/ajax/data/upload.php', // replace with your own server URL
		data: formData,
		cache: false,
		contentType: false,
		processData: false,
		type: 'POST',
		dataType: 'json',
		xhr: function ()
		{
			var xhr = $.ajaxSettings.xhr();
			if (xhr.upload) {
				xhr.upload.addEventListener('progress', function(event) {
					var percent = 0;
					var position = event.loaded || event.position;
					var total = event.total;
					if (event.lengthComputable) {
						percent = Math.ceil(position / total * 100);
					}
					//update progressbar
					$(progress_bar_id +" .progress-bar").css("width", + percent +"%");
					$(progress_bar_id + " .status").text(percent +"%");
					
					if(percent == '100'){						
						$('#progress-wrp').html('<div style="text-align:center;">Internal processing please wait....<div>');						
					}
				}, true);
			}
			return xhr;
		},
		success: function(response) {
						
			if (response.file != '') {
				//alert('successfully uploaded recorded blob');

				// file path on server
				//var fileDownloadURL = 'https://webrtcweb.com/RecordRTC/uploads/' + fileObject.name;

				// preview the uploaded file URL
				//document.getElementById('header').innerHTML = '<a href="' + fileDownloadURL + '" target="_blank">' + fileDownloadURL + '</a>';

				// preview uploaded file in a VIDEO element
				//document.getElementById('your-video-id').src = fileDownloadURL;

				// open uploaded file in a new tab
				//window.open(fileDownloadURL);
					
				/* show publisher meta */
				var publisher = $('.publisher');
				
				$('.publisher-meta[data-meta="video"]').show();
				/* add the attachment to publisher data */
				
				console.log(publisher);
				//var object_v = publisher.data('video');
				
				//object_v['source'] = response.file;
				/* add publisher-attachments */
				
				var ob_j = {'source':response.file};
								
				//console.log(object_v);
				publisher.data('video', ob_j);
				
				$(".popup-close").click();
				
			} else {
				alert(response); // error/failure
			}
		}
	});
	
});

// window.isSecureContext could be used for Chrome
var isSecureOrigin = location.protocol === 'https:' || location.hostname === 'localhost';
if (!isSecureOrigin) {
  alert('getUserMedia() must be run from a secure origin: HTTPS or localhost.\n\nChanging protocol to HTTPS');
  location.protocol = 'HTTPS';
}

var constraints = {
  audio: true,
  video: true
};

function handleSourceOpen(event) {
  console.log('MediaSource opened');
  sourceBuffer = mediaSource.addSourceBuffer('video/webm; codecs="vp8"');
  console.log('Source buffer: ', sourceBuffer);
}

function handleDataAvailable(event) {
  if (event.data && event.data.size > 0) {
    recordedBlobs.push(event.data);
  }
}

function handleStop(event) {
  console.log('Recorder stopped: ', event);
}

function startRecording() {
  recordedBlobs = [];
  var options = {mimeType: 'video/webm;codecs=vp9'};
  if (!MediaRecorder.isTypeSupported(options.mimeType)) {
    console.log(options.mimeType + ' is not Supported');
    options = {mimeType: 'video/webm;codecs=vp8'};
    if (!MediaRecorder.isTypeSupported(options.mimeType)) {
      console.log(options.mimeType + ' is not Supported');
      options = {mimeType: 'video/webm'};
      if (!MediaRecorder.isTypeSupported(options.mimeType)) {
        console.log(options.mimeType + ' is not Supported');
        options = {mimeType: ''};
      }
    }
  }
  try {
    mediaRecorder = new MediaRecorder(window.stream, options);
  } catch (e) {
    console.error(`Exception while creating MediaRecorder: ${e}`);
    alert(`Exception while creating MediaRecorder: ${e}. mimeType: ${options.mimeType}`);
    return;
  }
  console.log('Created MediaRecorder', mediaRecorder, 'with options', options);
  recordButton.textContent = 'Stop Recording';
  playButton.disabled = true;
  downloadButton.disabled = true;
  mediaRecorder.onstop = handleStop;
  mediaRecorder.ondataavailable = handleDataAvailable;
  mediaRecorder.start(10); // collect 10ms of data
  console.log('MediaRecorder started', mediaRecorder);
}

function stopRecording() {
  mediaRecorder.stop();
  console.log('Recorded Blobs: ', recordedBlobs);
  recordedVideo.controls = true;
}

function handleSuccess(stream) {
  recordButton.disabled = false;
  console.log('getUserMedia() got stream: ', stream);
  window.stream = stream;

  var gumVideo = document.querySelector('video#gum');
  gumVideo.srcObject = stream;
}

function handleError(error) {
  console.log('navigator.getUserMedia error: ', error);
}

navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);


// This code is added by Abhishek custom code
// this function is used to generate random file name
function getFileName(fileExtension) {
	var d = new Date();
	var year = d.getUTCFullYear();
	var month = d.getUTCMonth();
	var date = d.getUTCDate();
	return 'RecordRTC-' + year + month + date + '-' + getRandomString() + '.' + fileExtension;
}

function getRandomString() {
	if (window.crypto && window.crypto.getRandomValues && navigator.userAgent.indexOf('Safari') === -1) {
		var a = window.crypto.getRandomValues(new Uint32Array(3)),
			token = '';
		for (var i = 0, l = a.length; i < l; i++) {
			token += a[i].toString(36);
		}
		return token;
	} else {
		return (Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
	}
}
