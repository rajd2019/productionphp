/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */

'use strict';

var snapshotButton2 = document.querySelector('button#snapshot');
//var filterSelect2 = document.querySelector('select#filter');

// Put variables in global scope to make them available to the browser console.
var video2 = window.video = document.querySelector('#take-photo-id');
var canvas2 = window.canvas = document.querySelector('canvas');
canvas2.width = 480;
canvas2.height = 360;

snapshotButton2.onclick = function() {
  //canvas2.className = filterSelect2.value;
  canvas2.getContext('2d').drawImage(video2, 0, 0, canvas2.width, canvas2.height); 
};

/*filterSelect2.onchange = function() {
  video2.className = filterSelect2.value;
};*/

var downloadButton = document.querySelector('button#download');
downloadButton.addEventListener('click', () => {
		
	//const blob = new Blob(recordedBlobs, {type: 'image/jpg'});
	//const url = window.URL.createObjectURL(blob);
	//const a = document.createElement('a');

	/*a.style.display = 'none';
	a.href = url;
	a.download = 'test.webm';
	document.body.appendChild(a);
	a.click();
	setTimeout(() => {
	document.body.removeChild(a);
	window.URL.revokeObjectURL(url);
	}, 100);*/
	
	  
	var data_i = canvas2.toDataURL('image/jpg');
      
	var i_file = dataURItoBlob(data_i);  
		
	var formData = new FormData();
	
	// recorded data
    formData.append('photo-blob', i_file);
						
	// file name
    //formData.append('photo-filename', fileObject.name);
	
	// file name
    formData.append('type', 'take-photo');
	
	$("#progress-wrp").show();
	var progress_bar_id = '#progress-wrp';
	//reset progressbar
	$(progress_bar_id +" .progress-bar").css("width", "0%");
	$(progress_bar_id + " .status").text("0%");
		
	$.ajax({
		url: 'https://www.guo.media/includes/ajax/data/upload.php', // replace with your own server URL
		data: formData,
		cache: false,
		contentType: false,
		processData: false,
		type: 'POST',
		dataType: 'json',
		xhr: function ()
		{
			var xhr = $.ajaxSettings.xhr();
			if (xhr.upload) {
				xhr.upload.addEventListener('progress', function(event) {
					var percent = 0;
					var position = event.loaded || event.position;
					var total = event.total;
					if (event.lengthComputable) {
						percent = Math.ceil(position / total * 100);
					}
					//update progressbar
					$(progress_bar_id +" .progress-bar").css("width", + percent +"%");
					$(progress_bar_id + " .status").text(percent +"%");
					
					if(percent == '100'){						
						$('#progress-wrp').html('<div style="text-align:center;">Internal processing please wait....<div>');						
					}
				}, true);
			}
			return xhr;
		},
		success: function(response) {
			
			if (response.files[0] != '') {
				
				/* show upload loader */
				var publisher = $('.publisher');
				
				publisher.data('photo', {});
				var attachments = $('.publisher-attachments');
				//var loader = $('<ul></ul>').appendTo(attachments);
				attachments.show();
				
				var attachmentsNum = attachments.find('li').length + 1;
				var itemWidth = parseInt(attachments.width()) / attachmentsNum + 'px';
				var itemHeight = 'auto';
				if (attachmentsNum > 1) {
				  itemHeight = itemWidth;
				}
			
				
				var obj_r = [];
				
				//var attachments = $('.publisher-attachments');
				
				//for (var i in response.files) {
										
				  //console.log(response.files);
				  obj_r[response.files[0]] = response.files[0];
				  /* add publisher-attachments */
				  var image_path = uploads_path + '/' + response.files[0];
				  if ($(window).width() > 1024) {
					attachments.find('ul').append(render_template("#publisher-attachments-item", {
					  'src': response.files[0],
					  'image_path': image_path
					}));
				  }
				  else {
					attachments.find('ul').append(render_template("#publisher-attachments-item", {
					  'src': response.files[0],
					  'image_path': image_path,
					  'image_width': itemWidth,
					  'image_height': itemHeight
					}));
				  }
				//}
				
				publisher.data('photos', obj_r);
				
				$(".popup-close").click();
			} else {
				alert(response); // error/failure
			}
		}
	});
	
});

var constraints = {
  audio: false,
  video: true
};

function handleSuccess(stream) {
  window.stream = stream; // make stream available to browser console
  video2.srcObject = stream;
}

function handleError(error) {
  console.log('navigator.getUserMedia error: ', error);
}

navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);


// This code is added by Abhishek custom code
// this function is used to generate random file name
function getFileName(fileExtension) {
	var d = new Date();
	var year = d.getUTCFullYear();
	var month = d.getUTCMonth();
	var date = d.getUTCDate();
	return 'RecordRTC-' + year + month + date + '-' + getRandomString() + '.' + fileExtension;
}

function getRandomString() {
	if (window.crypto && window.crypto.getRandomValues && navigator.userAgent.indexOf('Safari') === -1) {
		var a = window.crypto.getRandomValues(new Uint32Array(3)),
			token = '';
		for (var i = 0, l = a.length; i < l; i++) {
			token += a[i].toString(36);
		}
		return token;
	} else {
		return (Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
	}
}

function dataURItoBlob(dataURI) {
	// convert base64/URLEncoded data component to raw binary data held in a string
	var byteString;
	if (dataURI.split(',')[0].indexOf('base64') >= 0)
		byteString = atob(dataURI.split(',')[1]);
	else
		byteString = unescape(dataURI.split(',')[1]);
	// separate out the mime component
	var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
	// write the bytes of the string to a typed array
	var ia = new Uint8Array(byteString.length);
	for (var i = 0; i < byteString.length; i++) {
		ia[i] = byteString.charCodeAt(i);
	}
	return new Blob([ia], {type:mimeString});
}
