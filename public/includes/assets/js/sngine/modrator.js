
var url  = ajax_path+"modrator/delete.php";
$('body').on('click', '.js_admin-deleter-modrator', function () {
    var handle = $(this).data('handle');
    var id = $(this).data('id');
    var node = $(this).data('node');
    confirm(__['Delete'], __['Are you sure you want to delete this?'], function() {
        $.post(url, {'handle': handle, 'id': id, 'node': node}, function(response) {
            /* check the response */
            if(response.callback) {
                eval(response.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
        .fail(function() {
            modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
        });
    });
});

