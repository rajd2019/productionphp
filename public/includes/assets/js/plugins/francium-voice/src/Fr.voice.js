/**
.---------------------------------------------------------------------------.
| The Francium Project                                                      |
| ------------------------------------------------------------------------- |
| This software "voice" is a part of the Francium (Fr) project.             |
| http://subinsb.com/the-francium-project                                   |
| ------------------------------------------------------------------------- |
|    Author: Subin Siby                                                     |
| Copyright (c) 2014 - 2015, Subin Siby. All Rights Reserved.               |
| ------------------------------------------------------------------------- |
|   License: Distributed under the Apache License, Version 2.0              |
|            http://www.apache.org/licenses/LICENSE-2.0                     |
| This program is distributed in the hope that it will be useful - WITHOUT  |
| ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
| FITNESS FOR A PARTICULAR PURPOSE.                                         |
'---------------------------------------------------------------------------'
*/

/**
.---------------------------------------------------------------------------.
|  Software:      Francium Voice                                            |
|  Version:       0.6 (Last Updated on 2017 Feruary 01)                     |
|  Documentation: http://subinsb.com/html5-record-mic-voice                 |
|  Contribute:    https://github.com/subins2000/Francium-voice              |
'---------------------------------------------------------------------------'
*/

(function(window){
	window.Fr = window.Fr || {};
	Fr.voice = {

		/**
		 * Path to mp3Worker.js
		 * Only needed if you're gonna use MP3 conversion
		 * You should also include libmp3lame.min.js
		 * You can get both files from https://github.com/subins2000/Francium-voice/blob/master/js/
		 */
		mp3WorkerPath: "src/mp3Worker.js",

		stream: false,
		input: false,

		init_called: false,

		/**
		 * @type function setTimeout() function will be stored here
		 */
		stopRecordingTimeout: false,

		/**
		 * Initialize. Set up variables.
		 */
        init: function () {
            try {
                // Fix up for prefixing
                window.AudioContext = window.AudioContext || window.webkitAudioContext;

                // Older browsers might not implement mediaDevices at all, so we set an empty object first
                if (navigator.mediaDevices === undefined) {
                    navigator.mediaDevices = {};
                }

				// Some browsers partially implement mediaDevices. We can't just assign an object
				// with getUserMedia as it would overwrite existing properties.
				// Here, we will just add the getUserMedia property if it's missing.
                if (navigator.mediaDevices.getUserMedia === undefined) {
                    navigator.mediaDevices.getUserMedia = function(constraints) {

                        // First get ahold of the legacy getUserMedia, if present
                        var getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

                        // Some browsers just don't implement it - return a rejected promise with an error
                        // to keep a consistent interface
                        if (!getUserMedia) {
                            return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
                        }

                        // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
                        return new Promise(function(resolve, reject) {
                            getUserMedia.call(navigator, constraints, resolve, reject);
                        });
                    }
                }
                this.context = new AudioContext();
            } catch (e) {
                alert('Web Audio API is not supported in this browser');
            }
        },

		/**
		 * Start recording audio
		 */
        record: function (output, finishCallback, recordingCallback) {
            finishCallback = finishCallback || function () { };
            recordingCallback = recordingCallback || function () { };

            if (this.init_called === false) {
                this.init();
                this.init_called = true;
            }

            var $that = this;

            navigator.mediaDevices.getUserMedia({audio: true})
                .then(function (stream) {
                    $that.input = $that.context.createMediaStreamSource(stream);
                    if (output === true) {
                        $that.input.connect($that.context.destination);
                    }

                    $that.recorder = new Recorder($that.input, {
                        'mp3WorkerPath': $that.mp3WorkerPath,
                        'recordingCallback': recordingCallback
                    });

                    $that.stream = stream;
                    $that.recorder.record();
                    finishCallback(stream);
                })
                .catch(function (err) {
                    console.log(err.name + ": " + err.message);
                });
        },

		/**
		 * Pause the recording
		 */
		pause: function(){
			this.recorder.stop();
		},

		resume: function(){
			this.recorder.record();
		},

		/**
		 * Stop recording audio.
		 * This will reset the recorded audio and the
		 * recorded audio can't be played or exported after.
		 * @return {Fr.voice}
		 */
		stop: function(){
			this.recorder.stop();
			this.recorder.clear();
			this.stream.getTracks().forEach(function (track) {
				track.stop();
			});
			return this;
		},

		/**
		 * Export the recorded audio as WAV in different formats
		 * @param {[type]} [varname] [description]
		 */
		export: function(callback, type){
			this.recorder.exportWAV(function(blob){
				Fr.voice.callExportCallback(blob, callback, type);
			});
		},

		exportMP3: function(callback, type){
			this.recorder.exportMP3(function(blob){
				Fr.voice.callExportCallback(blob, callback, type);
			});
		},

		/**
		 * Call the export callback with data it requires
		 * @param  {Blob}     blob     Exported blob
		 * @param  {string}   type     Type of data to export
		 * @param  {Function} callback Export callback
		 */
		callExportCallback: function(blob, callback, type) {
			if(typeof type === "undefined" || type == "blob"){
				callback(blob);
			}else if (type === "base64"){
				var reader = new window.FileReader();
				reader.readAsDataURL(blob);
				reader.onloadend = function() {
					base64data = reader.result;
					callback(base64data);
				};
			}else if(type === "URL"){
				var url = URL.createObjectURL(blob);
				callback(url);
			}
		},

		/**
		 * Pause the recording after a specific time
		 * @param  integer time Time in milliseconds
		 * @return void
		 */
		stopRecordingAfter: function(time, callback){
			var callback = callback || function(){};

			clearTimeout(this.stopRecordingTimeout);
			this.stopRecordingTimeout = setTimeout(function(){
				Fr.voice.pause();
				callback();
			}, time);
		}
	};
})(window);
