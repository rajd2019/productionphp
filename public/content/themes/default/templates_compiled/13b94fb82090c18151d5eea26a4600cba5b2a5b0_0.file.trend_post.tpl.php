<?php
/* Smarty version 3.1.31, created on 2018-08-31 14:13:28
  from "/var/app/current/content/themes/default/templates/trend_post.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b894d0820f6d9_83529961',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '13b94fb82090c18151d5eea26a4600cba5b2a5b0' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/trend_post.tpl',
      1 => 1527699518,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:_sign_form.tpl' => 1,
    'file:_directory.tpl' => 1,
    'file:__feeds_user.tpl' => 1,
    'file:_announcements.tpl' => 1,
    'file:_publisher.tpl' => 1,
    'file:_boosted_post.tpl' => 1,
    'file:_trend_posts.tpl' => 4,
    'file:_ads.tpl' => 1,
    'file:_widget.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_5b894d0820f6d9_83529961 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
    <!-- page content -->
    <div class="index-wrapper" <?php if (!$_smarty_tpl->tpl_vars['system']->value['system_wallpaper_default'] && $_smarty_tpl->tpl_vars['system']->value['system_wallpaper']) {?> style="background-image: url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['system']->value['system_wallpaper'];?>
')" <?php }?>>
        <div class="container">
            <div class="index-intro">
                <h1>
                    <?php echo __("Welcome to");?>
 <?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>

                </h1>
                <p>
                    <?php echo __("Share your memories, connect with others, make new friends");?>

                </p>
            </div>

            <div class="row relative">
                
                <?php if ($_smarty_tpl->tpl_vars['random_profiles']->value) {?>
                    <!-- random 4 users -->
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['random_profiles']->value, 'random_profile');
$_smarty_tpl->tpl_vars['random_profile']->index = -1;
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['random_profile']->value) {
$_smarty_tpl->tpl_vars['random_profile']->index++;
$__foreach_random_profile_0_saved = $_smarty_tpl->tpl_vars['random_profile'];
?>
                        <a href="/<?php echo $_smarty_tpl->tpl_vars['random_profile']->value['user_name'];?>
" class="fly-head"
                            <?php if ($_smarty_tpl->tpl_vars['random_profile']->index == 0) {?> style="top: 140px;left: 50px;" <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['random_profile']->index == 1) {?> style="bottom: 60px;left: 210px;" <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['random_profile']->index == 2) {?> style="top: 140px;right: 50px;" <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['random_profile']->index == 3) {?> style="bottom: 60px;right: 210px;" <?php }?>
                            data-toggle="tooltip" data-placement="top" title="<?php echo $_smarty_tpl->tpl_vars['random_profile']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['random_profile']->value['user_lastname'];?>
">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['random_profile']->value['user_picture'];?>
">
                        </a>
                    <?php
$_smarty_tpl->tpl_vars['random_profile'] = $__foreach_random_profile_0_saved;
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    <!-- random 4 users -->
                <?php }?>

                <!-- sign in/up form -->
                <?php $_smarty_tpl->_subTemplateRender('file:_sign_form.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                <!-- sign in/up form -->
            </div>
        </div>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['system']->value['directory_enabled']) {?>
        <?php $_smarty_tpl->_subTemplateRender('file:_directory.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php }?>
    <!-- page content -->
<?php } else { ?>
	<div class="search-result-heading">
        <div class="container">
            <?php echo $_smarty_tpl->tpl_vars['trend_string']->value;?>

        </div>
    </div>
    <!-- page content -->
    <div class="container mt20 offcanvas">
        <div class="row">

            <!-- left panel -->
            <div class="col-sm-4 col-md-3 offcanvas-sidebar">
                <div class="site_bar_new">
                    <div class="profile-bg-thumb">
                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_cover']) {?>
                        <a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_cover'];?>
">
                        </a>
                    <?php }?>
                    </div>
                    <div class="side_profile">
                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_picture']) {?><div class="pro_thumb">
                        <a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_picture'];?>
">
                        </a>
                    </div><?php }?>
                    
                    <div class="admin_detail">
                    <h3><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>"><?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_lastname'];?>
</a></h3>
                    <span><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>">@<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
</a></span>
                    </div>
                    <ul>
                    <li><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><?php echo __("Posts");?>
<span><?php if ($_smarty_tpl->tpl_vars['user']->value->_data['posts_count']) {
echo $_smarty_tpl->tpl_vars['user']->value->_data['posts_count'];
} else { ?>0<?php }?></span></a></li>
                    <li><a href="/followings.php?username=<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><?php echo __("Followings");?>
<span><?php if ($_smarty_tpl->tpl_vars['user']->value->_data['followings_count']) {
echo $_smarty_tpl->tpl_vars['user']->value->_data['followings_count'];
} else { ?>0<?php }?></span></a></li>
                    <li><a href="/followers.php?username=<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><?php echo __("Followers");?>
<span><?php if ($_smarty_tpl->tpl_vars['user']->value->_data['followers_count']) {
echo $_smarty_tpl->tpl_vars['user']->value->_data['followers_count'];
} else { ?>0<?php }?></span></a></li>
                    </ul>
                    </div> 
                    
                    <!-- people you may know -->
                    <?php if (count($_smarty_tpl->tpl_vars['user']->value->_data['new_people']) > 0) {?>
                        <div class="panel panel-default panel-widget">
                            <div class="panel-heading">
                                <div class="pull-right flip">
                                    <small><a href="/people"><?php echo __("See All");?>
</a></small>
                                </div>
                                <strong><?php echo __("Who to follow");?>
</strong>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['user']->value->_data['new_people'], '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_user.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_connection'=>"add",'_small'=>true), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                            </div>
                        </div>
                    <?php }?>
                     <!-- people you may know -->
                    
<!--                    <div class="sidebar_trends">
                    <h3><?php echo __("Weekly Trends for you");?>
</h3>
                    
                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['popular_tag'] != 'No Record Found') {?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['user']->value->_data['popular_tag'], 'custom_field');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['custom_field']->value) {
?>
                    <div class="fb_post_rep">
                    <h4><a href="/trend_post.php?tags=<?php echo $_smarty_tpl->tpl_vars['custom_field']->value['url_tags'];?>
"><?php echo $_smarty_tpl->tpl_vars['custom_field']->value['tags'];?>
</a></h4>
                    <p><?php echo $_smarty_tpl->tpl_vars['custom_field']->value['count'];?>
 <?php echo __("Posts");?>
</p>
                    </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    <?php } else { ?>
                    <h4><?php echo __("No Trends for you");?>
.</h4>
                    <?php }?>
                    
                    </div>-->
                
                </div>

            </div>
            <!-- left panel -->

            <div class="col-sm-8 col-md-9 offcanvas-mainbar">
                <div class="row">
                    <!-- center panel -->
                    <div class="col-sm-12 col-md-8">

                        <!-- announcments -->
                        <?php $_smarty_tpl->_subTemplateRender('file:_announcements.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                        <!-- announcments -->

                        <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>

                            <!-- stories -->
                           
                            <!-- stories -->

                            <!-- publisher -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_publisher.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_handle'=>"me",'_privacy'=>true), 0, false);
?>

                            <!-- publisher -->

                            <!-- boosted post -->
                            <?php if ($_smarty_tpl->tpl_vars['boosted_post']->value) {?>
                                <?php $_smarty_tpl->_subTemplateRender('file:_boosted_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('post'=>$_smarty_tpl->tpl_vars['boosted_post']->value), 0, false);
?>

                            <?php }?>
                            <!-- boosted post -->

                            <!-- posts stream -->

                             
                            <?php $_smarty_tpl->_subTemplateRender('file:_trend_posts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>"newsfeed"), 0, false);
?>


                            <!-- posts stream -->

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "saved") {?>
                            <!-- saved posts stream -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_trend_posts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>"saved",'_title'=>__("Saved Posts")), 0, true);
?>

                            <!-- saved posts stream -->

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "articles") {?>
                            <!-- saved posts stream -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_trend_posts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>"posts_profile",'_id'=>$_smarty_tpl->tpl_vars['user']->value->_data['user_id'],'_filter'=>"article",'_title'=>__("My Articles")), 0, true);
?>

                            <!-- saved posts stream -->

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "products") {?>
                            <!-- saved posts stream -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_trend_posts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>"posts_profile",'_id'=>$_smarty_tpl->tpl_vars['user']->value->_data['user_id'],'_filter'=>"product",'_title'=>__("My Products")), 0, true);
?>

                            <!-- saved posts stream -->

                        <?php }?>
                    </div>
                    <!-- center panel -->

                    <!-- right panel -->
                    <div class="col-sm-12 col-md-4">
                        <!-- pro members -->
                        <?php if (count($_smarty_tpl->tpl_vars['pro_members']->value) > 0) {?>
                            <div class="panel panel-default panel-friends">
                                <div class="panel-heading">
                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['packages_enabled'] && !$_smarty_tpl->tpl_vars['user']->value->_data['user_subscribed']) {?>
                                        <div class="pull-right flip">
                                            <small><a href="/packages"><?php echo __("Upgrade");?>
</a></small>
                                        </div>
                                    <?php }?>
                                    <strong class="text-primary"><i class="fa fa-bolt"></i> <?php echo __("Pro Users");?>
</strong>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pro_members']->value, '_member');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_member']->value) {
?>
                                            <div class="col-xs-4">
                                                <a class="friend-picture" href="/<?php echo $_smarty_tpl->tpl_vars['_member']->value['user_name'];?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['_member']->value['user_picture'];?>
);" >
                                                    <span class="friend-name"><?php echo $_smarty_tpl->tpl_vars['_member']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['_member']->value['user_lastname'];?>
</span>
                                                </a>
                                            </div>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </div>
                                </div>
                            </div>
                        <?php }?>
                        <!-- pro members -->

                        <!-- boosted pages -->
                        <?php if (count($_smarty_tpl->tpl_vars['promoted_pages']->value) > 0) {?>
                            <div class="panel panel-default panel-friends">
                                <div class="panel-heading">
                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['packages_enabled'] && !$_smarty_tpl->tpl_vars['user']->value->_data['user_subscribed']) {?>
                                        <div class="pull-right flip">
                                            <small><a href="/packages"><?php echo __("Upgrade");?>
</a></small>
                                        </div>
                                    <?php }?>
                                    <strong class="text-primary"><i class="fa fa-bullhorn fa-fw"></i> <?php echo __("Pro Pages");?>
</strong>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['promoted_pages']->value, '_page');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_page']->value) {
?>
                                            <div class="col-xs-4">
                                                <a class="friend-picture" href="/pages/<?php echo $_smarty_tpl->tpl_vars['_page']->value['page_name'];?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['_page']->value['page_picture'];?>
);" >
                                                    <span class="friend-name"><?php echo $_smarty_tpl->tpl_vars['_page']->value['page_title'];?>
</span>
                                                </a>
                                            </div>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </div>
                                </div>
                            </div>
                        <?php }?>
                         <!-- boosted pages -->

                        <?php $_smarty_tpl->_subTemplateRender('file:_ads.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:_widget.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


                        

                        <!-- suggested pages -->
                      
                        <!-- suggested pages -->

                        <!-- suggested groups -->
                     
                        <!-- suggested groups -->

                        <!-- suggested events -->
                    
                        <!-- suggested events -->

                        <!-- mini footer -->
                       
                        <!-- mini footer -->
                    </div>
                    <!-- right panel -->
                </div>
            </div>

        </div>
    </div>
    <!-- page content -->
<?php }?>

<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
