<?php
/* Smarty version 3.1.31, created on 2018-09-07 13:35:17
  from "/var/app/current/content/themes/default/templates/_header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b927e95d70403_44226181',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b963ad9c78fa4144c31d2a9fa86e54d2c2612572' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/_header.tpl',
      1 => 1536327294,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ajax.search.tpl' => 1,
    'file:__feeds_conversation.tpl' => 1,
    'file:_header.messages.tpl' => 1,
    'file:_header.friend_requests.tpl' => 1,
    'file:_header.notifications.tpl' => 1,
    'file:_header.search.tpl' => 1,
    'file:_ads.tpl' => 1,
  ),
),false)) {
function content_5b927e95d70403_44226181 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style>
li.take-photo {
    background: #5a656b;
    border-radius: 5px;
    margin: 6px 8px 0 0;
}
li.take-video {
    background: #5a656b;
    border-radius: 5px;
    margin: 6px 0 0 0;
}
.take-photo a.take-photo {
    padding: 3px 5px;
    display: block;
    border-radius: 21px !important;
    color: #fff;
    margin: 0 0 0;
	font-size: 12px;
}
.take-video a.take-video {
    padding: 3px 5px;
    display: block;
    border-radius: 21px !important;
    color: #fff;
    margin: 0 0 0;
	font-size: 12px;
}
@media (min-width: 768px){
.main-header .navbar-form .form-control {
    height: 36px;
    line-height: 20px;
    padding-bottom: 5px;
    padding-top: 5px;
    padding-right: 35px;
    border-radius: 21px;
    background: #fafafa;
    color: #5a656b;
    font-size: 12px;
}
.nav-search {
    position: absolute;
    border: none;
    right: 4px;
    top: 19px;
    outline: none !important;
}
.inner_link .top-menues i{
    margin: 12px 11px 0 0 !important;
    float: left;
    line-height: 22px;
	width:24px;
	height:24px;
}
.inner_link .top-menues .hand_icon {
    top: auto;
    position: static;
}
.main-header .header-right .nav{
	margin:6px 0 0 0;
}

.navbar-container span.text-link.dev {
    border: 1px solid #e6ecf0;
    line-height: 22px;
    padding-bottom: 5px;
    padding-top: 5px;
    /* padding-right: 35px; */
    border-radius: 21px;
    background: #fafafa;
    color: #5a656b;
    font-size: 12px !important;
    text-align: center !important;
    padding: 7px 15px;
    margin: 12px 2px 0 5px;
    color: #7697b7;
}

}
@media screen and (max-width:767px){
.top-menues.support-popup i {    
	 width: 21px;
    height: 21px;
    display: inline-block;
    background-position: 0 0;
    background-size: 100%;
    margin: 3px 0 0 4px;
}

ul.nav.navbar-nav {
    float: left;
    width: auto;
}
.navbar-container span.text-link.dev.menu_bfr_login{
  margin: -1px 0 0 7px !important;
}
.mobile-menu span.text-link.dev.pull-right {
	margin: -4px 0 0 7px !important;
}
.navbar-container span.text-link.dev {
    border: 1px solid #e6ecf0;
    line-height: 22px;
    padding-bottom: 5px;
    padding-top: 5px;
    /* padding-right: 35px; */
    border-radius: 21px;
    background: #fafafa;
    color: #5a656b;
    font-size: 13px !important;
    text-align: center !important;
    padding: 5px 15px;
    margin: 5px 2px 0 5px !important;
    color: #7697b7;
    float: right;
}
.top-menues.support-popup img {
	width:100%;
	background: #5a656b;
}
li.take-photo, li.take-video{
	margin-top:2px;
}
}
@supports (-webkit-overflow-scrolling: touch) {
.modal-dialog{
	transform: none !important;
}
}
</style>
	<?php echo '<script'; ?>
 type="text/javascript">
      $(document).ready(function(){

          if( $(window).width() > 767){
            $('#desktop-live-msg-id').on('click', function(){

                $('#mobile-live-direct-msgid').css('display','block !important');

                $('#mobile-live-direct-msgid').removeClass('visible-xs');

                $('#mobile-live-direct-msgid > ul.navbar-nav > li').css('display','none');

                $('#myModal').modal('show');

            });
          }else{
               //$('#mobile-live-direct-msgid').addClass('visible-xs');
               //$('#mobile-live-direct-msgid > ul.navbar-nav > li').css('display','block');
          }
      });
    <?php echo '</script'; ?>
>

<?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
    <!--<div class="send-user-message" data-toggle="modal" data-url="posts/compose.php?do=create">
        <div class="add">
            <i class="fa fa-plus-circle"></i>
        </div>
    </div>
    <button class="send-user-message">Send Message</button>-->
    <body data-hash-tok="<?php echo $_smarty_tpl->tpl_vars['session_hash']->value['token'];?>
" data-hash-pos="<?php echo $_smarty_tpl->tpl_vars['session_hash']->value['position'];?>
" class="visitor n_chat">
<?php } else { ?>
    <body data-hash-tok="<?php echo $_smarty_tpl->tpl_vars['session_hash']->value['token'];?>
" data-hash-pos="<?php echo $_smarty_tpl->tpl_vars['session_hash']->value['position'];?>
" data-chat-enabled="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_chat_enabled'];?>
" class="<?php if (!$_smarty_tpl->tpl_vars['system']->value['chat_enabled']) {?>n_chat<?php }
if ($_smarty_tpl->tpl_vars['system']->value['activation_enabled'] && !$_smarty_tpl->tpl_vars['user']->value->_data['user_activated']) {?> n_activated<?php }
if (!$_smarty_tpl->tpl_vars['system']->value['system_live']) {?> n_live<?php }?>">

	<!--<button class="send-user-message">Send Message</button>-->
    <div class="send-user-message" data-toggle="modal" data-url="posts/compose.php?do=create">
        <div class="add">
            <i class="fa fa-plus-circle"></i>
        </div>
    </div>
    <input type="hidden" id="currnt_post_id" value="">
<?php }?>
    <!-- main wrapper -->
    <div class="main-wrapper">
        <div class="search_field">
            <form id="header-search">
                <input id="search-input" type="text" class="form-control" placeholder='<?php echo __("Search for everything");?>
' required autocomplete="off">
                <button type="submit" class="Icon Icon--medium Icon--search nav-search" tabindex="-1"></button>

                <div id="search-results" class="dropdown-menu dropdown-widget dropdown-search js_dropdown-keepopen">
                    <div class="dropdown-widget-header">
                        <?php echo __("Search Results");?>

                    </div>
                    <div class="dropdown-widget-body">
                        <div class="loader loader_small ptb10"></div>
                    </div>
                    <a class="dropdown-widget-footer" id="search-results-all" href="/search/"><?php echo __("See All Results");?>
</a>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in && count($_smarty_tpl->tpl_vars['user']->value->_data['search_log']) > 0) {?>
                    <div id="search-history" class="dropdown-menu dropdown-widget dropdown-search js_dropdown-keepopen">
                        <div class="dropdown-widget-header">
                            <span class="text-link pull-right flip js_clear-searches">
                                <?php echo __("Clear");?>

                            </span>
                            <i class="fa fa-clock-o"></i> <?php echo __("Recent Searches");?>

                        </div>
                        <div class="dropdown-widget-body">
                            <?php $_smarty_tpl->_subTemplateRender('file:ajax.search.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('results'=>$_smarty_tpl->tpl_vars['user']->value->_data['search_log']), 0, false);
?>

                        </div>
                        <a class="dropdown-widget-footer" id="search-results-all" href="/search/"><?php echo __("Advanced Search");?>
</a>
                    </div>
                <?php }?>
            </form>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
            <div class="sticky_header_second">
                <ul>
                    <li><a <?php if ($_smarty_tpl->tpl_vars['last_dir']->value == 'home') {?> class="active" <?php }?>  href="/"><i class="Icon Icon--home Icon--large"></i></a></li>
                    <li><a id="search_toggle" href="javascript:void(0)"><i class="Icon Icon--medium Icon--search"></i></a></li>
                    <li>
                        <a href="/notifications" <?php if ($_smarty_tpl->tpl_vars['last_dir']->value == 'notifications' || $_smarty_tpl->tpl_vars['last_dir']->value == 'mentions') {?> class="active" <?php }?>>
                            <span class="label <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_live_notifications_counter'] == 0) {?>hidden<?php }?>">
                                <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_live_notifications_counter'];?>

                            </span>
                            <i class="Icon Icon--notifications Icon--large"></i>
                        </a>
                    </li>
                    <li class="dropdown js_live-messages top-menues" id="mobile-live-msg-id">
                        <a class="left-nav-channel-title title-messaging"><i class="Icon Icon--dm Icon--large"></i></a>
                        <!-- <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                            <div class="dropdown-menu dropdown-widget with-arrow live-chat-massage">
                                <div class="dropdown-widget-header">
                                    <?php echo __("Messages");?>

                                    <a class="pull-right flip text-link js_chat-start" href="/messages/new"><?php echo __("Send a New Message");?>
</a>
                                </div>
                                <div class="dropdown-widget-body">
                                <div class="js_scroller">
                                    <?php if (count($_smarty_tpl->tpl_vars['user']->value->_data['conversations']) > 0) {?>
                                        <ul>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['user']->value->_data['conversations'], 'conversation');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['conversation']->value) {
?>
                                        <?php $_smarty_tpl->_subTemplateRender('file:__feeds_conversation.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                        </ul>
                                    <?php } else { ?>
                                        <p class="text-center text-muted mt10">
                                            <?php echo __("No messages");?>

                                        </p>
                                    <?php }?>
                                </div>
                            </div>
                            <a class="dropdown-widget-footer" href="/messages"><?php echo __("See All");?>
</a>
                            </div>
                        <?php }?>-->
                    </li>
					<?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in && !$_smarty_tpl->tpl_vars['user']->value->_is_admin) {?>
						<li class="top-menues support-popup">
							<a href="javascript:void(0)" title="Support">
								<!--<i class="Icon Icon--info Icon--large"></i>     -->
								<i class="">
								  <img src="/content/themes/default/images/technical-support.png?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
" alt="Support" >
								</i>
							</a>
						</li>
					 <?php }?>

					 <li class="top-menues">
						<a href="javascript:void(0)" title="Info">
							<i class="Icon Icon--info Icon--large"></i>
						</a>
					</li>
                    <li><!--a href="/broadcasts" <?php if ($_smarty_tpl->tpl_vars['last_dir']->value == 'broadcasts') {?> class="active" <?php }?>><i class="Icon Icon--cameraVideo Icon--large"></i></a-->
                    <div class="dropdown custom-dropdown-btn">
                         <a href="/broadcasts" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown">
                            <i class="Icon Icon--cameraVideo Icon--large mobile-broadcast"></i></a>
                         <div class="dropdown-menu menu-custom-button" aria-labelledby="dropdownMenuButton">
                           <a class="dropdown-item" href="/broadcasts"><?php echo __("Watch Broadcasts");?>
</a>

                         </div>
                       </div>
                    </li>
                </ul>
            </div>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in && $_smarty_tpl->tpl_vars['system']->value['activation_enabled'] && !$_smarty_tpl->tpl_vars['user']->value->_data['user_activated']) {?>
            <!-- top-bar -->
            <div class="top-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 hidden-xs">
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['activation_type'] == "email") {?>
                                <?php echo __("Please go to");?>
 <span class="text-primary"><?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_email'];?>
</span> <?php echo __("to complete the sign-up process");?>
.
                            <?php } else { ?>
                                <?php echo __("Please check the SMS on your phone");?>
 <strong><?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_phone'];?>
</strong> <?php echo __("to complete the sign-up process");?>
.
                            <?php }?>
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['activation_type'] == "email") {?>
                                <span class="text-link" data-toggle="modal" data-url="core/activation_email_resend.php">
                                    <?php echo __("Resend Activation Email");?>

                                </span>
                                 -
                                <span class="text-link" data-toggle="modal" data-url="#activation-email-reset">
                                    <?php echo __("Change Email");?>

                                </span>
                            <?php } else { ?>
                                <span class="btn btn-info btn-sm mr10" data-toggle="modal" data-url="#activation-phone"><?php echo __("Enter Code");?>
</span>
                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_phone']) {?>
                                    <span class="text-link" data-toggle="modal" data-url="core/activation_phone_resend.php">
                                        <?php echo __("Resend SMS");?>

                                    </span>
                                     -
                                <?php }?>
                                <span class="text-link" data-toggle="modal" data-url="#activation-phone-reset">
                                    <?php echo __("Change Phone Number");?>

                                </span>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- top-bar -->
        <?php }?>

        <?php if (!$_smarty_tpl->tpl_vars['system']->value['system_live']) {?>
            <!-- top-bar alert-->
            <div class="top-bar alert-bar">
                <div class="container">
                    <i class="fa fa-exclamation-triangle fa-lg pr5"></i>
                    <span class="hidden-xs"><?php echo __("The system has been shutted down");?>
.</span>
                    <span><?php echo __("Turn it on from");?>
</span> <a href="/admincp/settings"><?php echo __("Admin Panel");?>
</a>
                </div>
            </div>
            <!-- top-bar alert-->
        <?php }?>

        <div class="main-header">
            <div class="container header-container">
                  <?php $_smarty_tpl->_subTemplateRender('file:_header.messages.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                <!-- navbar-container -->
                <div class="navbar-container pull-left">
                  <!-- messages -->

                  <!-- messages -->

                   <div class="mobile-menu visible-xs" id="mobile-live-direct-msgid">
                    <ul class="nav navbar-nav">
                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                          <?php $_smarty_tpl->_assignInScope('dirs', explode("/",$_SERVER['REQUEST_URI']));
?>

                              <!-- home -->
                              <li class="top-menues">
                                  <a href="/" <?php ob_start();
echo $_smarty_tpl->tpl_vars['dirs']->value[count($_smarty_tpl->tpl_vars['dirs']->value)-1];
$_prefixVariable1=ob_get_clean();
if ($_prefixVariable1 == '') {?>class="active"<?php }?>>
                                      <i class="Icon Icon--home Icon--large"></i>
                                      <span><?php echo __("Home");?>
</span>
                                  </a>
                              </li>
                              <!-- home -->

                              <!-- friend requests -->
                              <?php $_smarty_tpl->_subTemplateRender('file:_header.friend_requests.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                              <!-- friend requests -->

                              <!-- notifications -->
                              <?php $_smarty_tpl->_subTemplateRender('file:_header.notifications.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                              <!-- notifications -->

                              <!-- messages -->

                              <!-- messages -->

                              <!-- search -->
                              <li class="visible-xs-block top-menues">
                                  <a href="/search">
                                      <i class="fa fa-search fa-lg"></i>
                                  </a>
                              </li>

                              <!-- search -->
                        <?php } else { ?>
                          <a class="mobile-signin" href="/signin">
                            <i class="Icon Icon--home Icon--large"></i>
                            <span class="text-link dev pull-right">
                              <?php echo __("Sign in");?>

                            </span>
                          </a>
                        <?php }?>

                    </ul>
                    </div>

                </div>
                <!-- navbar-container -->

                    <div class="brand-container  <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>user_logout_brand<?php }?>">
                        <!-- brand -->
                        <a href="/" class="brand">
                            <?php if (SYSTEM_LOGO) {?>
                                <img width="60" src="<?php echo SYSTEM_LOGO;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
">
                            <?php } else { ?>
                                <?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>

                            <?php }?>
                        </a>
                        <!-- brand -->
                    </div>

                    <div class="inner_link">
                      <ul>
                        <li>


                          <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                           <?php if ($_smarty_tpl->tpl_vars['system']->value['REQUEST_URI'] == '/') {?>
								<a href="/" class="top-nav-active">
								  <i class="Icon Icon--home Icon--large"></i>
								  <?php echo __("Home");?>

								</a>
							   <?php } else { ?>
							    <a href="/">
								  <i class="Icon Icon--home Icon--large"></i>
								  <?php echo __("Home");?>

								</a>
								<?php }?>
                          <?php } else { ?>
                            <a href="/signin">
                              <i class="Icon Icon--home Icon--large"></i>
                              <?php echo __("Sign in");?>

                            </a>
                          <?php }?>


                        </li>
                        <li><!--Link to Guo's page -->
                          <?php if ($_smarty_tpl->tpl_vars['system']->value['REQUEST_URI'] == '/milesguo') {?>							
							  <a href="/milesguo" class="top-nav-active">
								<i class="hand_icon">
								  <img src="/content/themes/default/images/hand.png?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
" alt="hand" width="19">
								</i>
								<?php echo __("Guo");?>

							  </a>
						   <?php } else { ?>
							<a href="/milesguo">
								<i class="hand_icon">
								  <img src="/content/themes/default/images/hand.png?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
" alt="hand" width="19">
								</i>
								<?php echo __("Guo");?>

							  </a>
							<?php }?>
                        </li>

						 <!-- Support -->
						 <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in && !$_smarty_tpl->tpl_vars['user']->value->_is_admin) {?>
                            <li class="top-menues support-popup">
                                <a href="javascript:void(0)">
                                    <i class="">
									  <img src="/content/themes/default/images/technical-support.png?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
" alt="" width="21">
									</i>
									<?php echo __("Support");?>

                                </a>
                            </li>

							<li class="dropdown js_live-messages top-menues" id="desktop-live-msg-id">
							<a style="cursor: pointer;"  class="left-nav-channel-title title-messaging"><i class="Icon Icon--dm Icon--large"></i><?php echo __("Messages");?>
</a></li>
						 <?php }?>
                            <!-- Support -->
                     </ul>
                   </div>



               <!-- navbar-collapse -->
                <div class="header-right <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?> mobile_user_head <?php }?>">
                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                        <!-- search -->
                        <?php $_smarty_tpl->_subTemplateRender('file:_header.search.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                        <!-- search -->
                    <?php }?>
                    <!-- navbar-container -->
                    <div class="navbar-container">
                       <div class="mobile-menu">
                        <ul class="nav navbar-nav">
                            <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>

                                <!-- search -->

                                <!-- user-menu -->
                                <li class="dropdown">
                                    <a class="dropdown-toggle user-menu">
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_picture'];?>
" alt="">
                                    </a>
                                    <ul class="dropdown-menu">
                                    <span class="caret-outer"></span>
                                    <span class="caret-inner"></span>
                                     <li><div class="user_name_id">

                                     <b class="fullname"><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_lastname'];?>
</a></b>
                                     <p class="name"><span class="username u-dir" dir="ltr"><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
">@<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
</a></span></p>
                                     </div>
                                     </li>
                                     <li class="divider"></li>
                                        <li>
                                            <a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><span class="DashUserDropdown-linkIcon Icon Icon--medium Icon--me"></span><?php echo __("Profile");?>
</a>
                                        </li>
                                        <li>
                                            <a href="/settings/privacy"><span class="DashUserDropdown-linkIcon Icon Icon--medium Icon--promotedStroked"></span><?php echo __("Privacy");?>
</a>
                                        </li>
                                        <li>
                                            <a href="/settings"><span class="DashUserDropdown-linkIcon Icon Icon--medium Icon--cog"></span><?php echo __("Settings");?>
</a>
                                        </li>
                                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_is_admin) {?>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="/admincp"><?php echo __("Admin Panel");?>
</a>
                                            </li>
                                        <?php }?>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="/signout"><?php echo __("Log Out");?>
</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="visible-xs-block">
                                    <a href="#" data-toggle="offcanvas" data-target=".sidebar-nav">
                                        <i class="fa fa-bars fa-lg"></i>
                                    </a>
                                </li>
                                <!-- user-menu -->
                            <?php }?>
                        </ul>
                        <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                        <span class="text-link dev menu_bfr_login translate_button" data-toggle="modal" data-url="#translator">
                        <?php } else { ?>
                        <span class="text-link dev menu_aftr_login translate_button" data-toggle="modal" data-url="#translator">
                        <?php }?>

						 <?php echo $_smarty_tpl->tpl_vars['system']->value['language']['title'];?>
</span>
             <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
						 <!--<span class="contact-icon watch_broadcasts">
                            <a href="/broadcasts">
                                  <span class="Icon icon_custom Icon--cameraVideo Icon--small"></span><?php echo __("GuoTV");?>

                            </a>
                        </span>


                        <a href="/broadcasts" <?php if ($_smarty_tpl->tpl_vars['last_dir']->value == 'broadcasts') {?> class="active" <?php }?>><span class="broadcasts_icon Icon Icon--cameraVideo Icon--small"></span></a>-->
						<a href="/broadcasts"  class="active"><span style="font-size:23px !important;" class="broadcasts_icon Icon Icon--cameraVideo Icon--large"></span></a>
						<span class="contact-icon">
							<div class="dropdown">
                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_broadcast_status) {?>
								<span id="blink-tv">
									<a href="/broadcasts" class="btn btn-secondary dropdown-toggle blink-custom" type="button" id="dropdownMenuButton">
										<span class="Icon icon_custom Icon--cameraVideo Icon--small "></span><?php echo __("GuoTV");?>

									</a>
								</span>
								<?php } else { ?>
								<span id="blink-tv">
									 <a href="/broadcasts" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" id="blink_tv">
										<span class="Icon icon_custom Icon--cameraVideo Icon--small "></span><?php echo __("GuoTV");?>

									</a>
								</span>
								<?php }?>

								<div class="dropdown-menu menu-custom-button" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="/broadcasts"><?php echo __("Broadcasts");?>
</a>

								</div>
                            </div>
						</span>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                         <span class="contact-icon">
                            <div class="dropdown">
							<?php if ($_smarty_tpl->tpl_vars['user']->value->_broadcast_status) {?>
							<span id="blink-tv">
                                <a href="/broadcasts" class="btn btn-secondary dropdown-toggle blink-custom" type="button" id="dropdownMenuButton">
                                    <span class="Icon icon_custom Icon--cameraVideo Icon--small "></span><?php echo __("GuoTV");?>

                                </a>
							</span>
							<?php } else { ?>
							<span id="blink-tv">
								 <a href="/broadcasts" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" id="blink_tv">
                                    <span class="Icon icon_custom Icon--cameraVideo Icon--small "></span><?php echo __("GuoTV");?>

                                </a>
							</span>
							<?php }?>
                                <!--<div class="dropdown-menu menu-custom-button" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="/broadcasts"><?php echo __("Watch Broadcasts");?>
</a>
                                    <a class="dropdown-item" data-toggle="modal" data-target="#do_broad" href="javascript:void(0)"><?php echo __("Start Broadcast");?>
</a>
                                </div>-->

								<div class="dropdown-menu menu-custom-button" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="/broadcasts"><?php echo __("Broadcasts");?>
</a>

								<?php if (($_smarty_tpl->tpl_vars['user']->value->_data['user_name'] == "milesguo" || $_smarty_tpl->tpl_vars['user']->value->_data['user_name'] == "MilesGuo") || ($_smarty_tpl->tpl_vars['user']->value->_data['user_name'] == "test17" || $_smarty_tpl->tpl_vars['user']->value->_data['user_name'] == "Test17")) {?>
									<?php if (!$_smarty_tpl->tpl_vars['user']->value->_broadcast_status) {?>

									<?php } else { ?>
										<span id="broad_start_stop"><button type="button" id="flash-button" class="flash-button-stop" onclick="BroadCastStop()">Stop Broadcast</button></span>
									<?php }?>
								<?php }?>
								</div>
                            </div>
                        </span>
                         <?php }?>
						</div>

                    </div>
                    <!-- navbar-container -->
                </div>
                <!-- navbar-collapse -->

            </div>
        </div>

        <!-- ads -->
        <?php $_smarty_tpl->_subTemplateRender('file:_ads.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_ads'=>$_smarty_tpl->tpl_vars['ads_master']->value['header'],'_master'=>true), 0, false);
?>

        <!-- ads -->
<?php }
}
