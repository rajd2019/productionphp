<?php
/* Smarty version 3.1.31, created on 2018-08-31 12:48:42
  from "/var/app/current/content/themes/default/templates/__feeds_notification.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b89392ab6daf3_99350621',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '059073684bbad54b18bc7092948f1a55e1f1d829' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/__feeds_notification.tpl',
      1 => 1527699522,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b89392ab6daf3_99350621 (Smarty_Internal_Template $_smarty_tpl) {
?>
<li class="feeds-item <?php if (!$_smarty_tpl->tpl_vars['notification']->value['seen']) {?>unread<?php }?>" data-id="<?php echo $_smarty_tpl->tpl_vars['notification']->value['notification_id'];?>
">

    <a class="data-container" href="<?php echo $_smarty_tpl->tpl_vars['notification']->value['url'];?>
" target="<?php if ($_smarty_tpl->tpl_vars['notification']->value['action'] == 'invite') {?>_new<?php }?>">

        <img class="data-avatar" src="<?php echo $_smarty_tpl->tpl_vars['notification']->value['user_picture'];?>
" alt="">

        <div class="data-content">

            <div><span class="name"><?php echo $_smarty_tpl->tpl_vars['notification']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['notification']->value['user_lastname'];?>
</span><div class="time js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['notification']->value['time'];?>
"><?php echo $_smarty_tpl->tpl_vars['notification']->value['time'];?>
</div></div>

            <div><i class="Icon Icon--Icon Icon--medium <?php echo $_smarty_tpl->tpl_vars['notification']->value['icon'];?>
 pr5"></i> <?php echo __(((string)$_smarty_tpl->tpl_vars['notification']->value['message']));?>
</div>

        </div>

    </a>

</li><?php }
}
