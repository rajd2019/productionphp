<?php
/* Smarty version 3.1.31, created on 2018-08-31 12:50:36
  from "/var/app/current/content/themes/default/templates/about.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b89399c9a1c17_91817276',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e9b999c09ff35bcfa3ccb53792ebe69272efa0a2' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/about.tpl',
      1 => 1527699508,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:_sidebar.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_5b89399c9a1c17_91817276 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- page content -->
<div class="page-title">
    <?php echo __("About GUO.MEDIA");?>

</div>

<div class="container offcanvas">
    <div class="row">

	    <!-- side panel -->
	    <div class="col-xs-12 visible-xs-block offcanvas-sidebar mt20">
	        <?php $_smarty_tpl->_subTemplateRender('file:_sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	    </div>
	    <!-- side panel -->

	    <div class="col-xs-12 offcanvas-mainbar">
			<div class="row">
		        <div class="col-xs-10 col-xs-offset-1 text-readable ptb10">
		            <p>
                	<?php echo __("GUO.MEDIA is now launching our beta version. We need your support.We are looking forward to your feedback in order to improve our services.We strive to provide a free social networking and livestreaming platform where everyone can express their views and opinions freely.Please join us in promoting judicial and media independence, democracy and religious freedom in China! Everything is just beginning!");?>

            		</p>
                    <h3 class="text-info"><?php echo __("Social networking");?>
</h3>
                    <p>
                	<?php echo __("GUO.MEDIA is an all-in-one social networking platform available in both website and mobile app versions, where users can build their own user profiles, create their own posts and follow each other. This is a platform for people to share their views freely on everything.");?>

            		</p>
                    <h3 class="text-info"><?php echo __("Livestream broadcast");?>
</h3>
                    <p>
                    <?php echo __("Users can broadcast a live video via our app. Your followers will receive notifications when live streaming has started, to let everyone also able to watch your broadcast.");?>

            		</p>
					 <h3 class="text-info"><?php echo __("More to come!");?>
</h3>
                    <p>
                    <?php echo __("This is just a beta version of GUO.MEDIA . We value and appreciate your suggestions in order to improve our services. More exciting features will be launched in due course to facilitate freedom of expression and democracy development in China. Everything is just beginning!");?>

            		</p>
		        </div>
		    </div>
	    </div>
	    
	</div>
</div>
<!-- page content -->

<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
