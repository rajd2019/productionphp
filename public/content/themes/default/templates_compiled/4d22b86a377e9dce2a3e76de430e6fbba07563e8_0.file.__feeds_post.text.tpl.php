<?php
/* Smarty version 3.1.31, created on 2018-08-31 12:48:37
  from "/var/app/current/content/themes/default/templates/__feeds_post.text.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b8939255227a7_15219319',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4d22b86a377e9dce2a3e76de430e6fbba07563e8' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/__feeds_post.text.tpl',
      1 => 1535522934,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b8939255227a7_15219319 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style>
.post-replace .post-text{
border-bottom:0px !important;
line-height:normal;
white-space: normal;
}
.post-replace .post-text p {
    margin-bottom: 0px !important;
    line-height: 22px !important;
}
.btn-translate {
    ffloat: none;
	display: block !important;
	margin: 0 0 10px;
	text-align: right;
}
a[data-readmore-toggle="rmjs-3"] {
    position: absolute;
}
.btn-translate span:hover {
    color: #2a78c2;
    text-decoration: underline;
}
.btn-translate span {
    color: #657786;
    text-decoration: none;
}
span.trans_by {
    font-size: 12px;
    margin: 9px 0 0;
    display: block;
    color: #000;
	font-family: 'Segoe UI',SegoeUI,"Helvetica Neue",Helvetica,Arial,sans-serif;
    font-weight: bold;
}
span.trans_by img {
    width: 14px;
    margin: -4px 0 0;
}
</style>
<div class="post-replace post-listing" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" >

    <?php if ($_smarty_tpl->tpl_vars['_post']->value['broadcast_name'] != '') {?>

    <div class="post-text js_readmore" dir="auto"><?php echo $_smarty_tpl->tpl_vars['_post']->value['broadcast_name'];?>
</div>

    <?php }?>

    <div class="post-text js_readmore rrr" dir="auto" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"><?php echo htmlspecialchars_decode($_smarty_tpl->tpl_vars['post']->value['text_plain'], ENT_QUOTES);?>
</div>

    <div class="post-text-plain hidden" id="hidden-post-<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['post']->value['text_plain'];?>
</div>

</div>

<?php if ($_smarty_tpl->tpl_vars['is_ajax_post']->value != 'Yes') {?>
<div class="btn-translate" data-id-translate="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"><i class="fa fa-globe" aria-hidden="true"></i> <span onclick="TranslateText(<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
)"><?php echo __("Translate");?>
</span></div>
<div class="text-translate" style="display:none;" id="t-translate-<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
	<div class="post-media">
		<div class="post-media-meta">
		<!-- Start Translate Text Via Microsoft Translate API -->
			<div class=".post-text" id="translated-text-<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" style="text-align:justify;"></div>
		<!-- End Translate Text Via Microsoft Translate API -->
		<span class="trans_by" id="translated-by-<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" style="display:none;"><span style="font-weight:normal;">Translated by</span> <img src="/content/themes/default/images/microsoft_PNG20.png"> Microsoft</span>
		<div class="loader loader_middium" id="translate-loader-<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" style="display:none;"></div>
		</div>
	</div>
</div>
<input type="hidden" id="translate-toggle-val-<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" value="0">
<?php }
}
}
