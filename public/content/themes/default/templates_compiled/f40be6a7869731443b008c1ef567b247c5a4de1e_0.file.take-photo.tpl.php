<?php
/* Smarty version 3.1.31, created on 2018-09-07 13:23:12
  from "/var/app/current/content/themes/default/templates/take-photo.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b927bc03dde00_97158400',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f40be6a7869731443b008c1ef567b247c5a4de1e' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/take-photo.tpl',
      1 => 1536326566,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b927bc03dde00_97158400 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style>
.modal-dialog div#container {
    max-width: none;
}
.modal-dialog video, .modal-dialog .canvas-outer {
	width: calc(50% - 30px) !important;
    margin: 0 15px;
    display: inline-block;
    float: left;
    height: calc(100vh - 20vh - 127px) !important;
	background: #000;
	text-align:center;
}
.modal-dialog .canvas-outer canvas {
	max-width:100%;
	max-height:100%;
	min-height:100%;
}	
.modal-dialog .modal-content-modal3 {
    height: 100%;
    overflow-y: auto;
}
.modal-dialog button#snapshot, .modal-dialog button#download, .modal-dialog button.popup-close {
    color: #fff;
    font-size: 12px;
    width: auto;
    padding: 7px 14px;
    border-radius: 4px;
	margin: 17px 0 0 7px;
	font-size: 14px;
    font-weight: bold;
    line-height: 20px;
    padding: 6px 16px;
    border-radius: 20px;
	padding: 8px 16px;
	border-radius: 20px;
	box-shadow: none;
	border: 0;
	float:left;
	background-color: #337ab7;
    border-color: #2e6da4;
}
.modal-dialog h1 {
    border-bottom: solid 1px;
    padding: 0 8px 7px;
    margin: 11px 15px 19px;
}
.select-style {
    float: left;
    margin: 18px 0 0 18px;
}
.select-style button.btn.dropdown-toggle.btn-default {
    padding: 8px 17px;
    border-radius: 20px;
}
.select-style .bootstrap-select.btn-group .dropdown-menu li a span.text {
    display: inline-block;
    color: #000 !important;
    opacity: 1;
    background: no-repeat;
}
.bootstrap-select.btn-group .dropdown-menu li a:hover span.text {
    color: #fff !important;
}
/* progress bar */
#progress-wrp {
    border: 1px solid #0099CC;
    padding: 1px;
    position: relative;
    border-radius: 3px;
    margin: 10px;
    text-align: left;
    background: #fff;
    box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
	float: left;
	width: calc(100% - 695px);
	margin: 22px 0 0 33px;
}
#progress-wrp .progress-bar{
	height: 20px;
    border-radius: 3px;
    background-color: #f39ac7;
    width: 0;
    box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
}
#progress-wrp .status{
	top:3px;
	left:50%;
	position:static;
	display:inline-block;
	color: #fff;
}
.mobile-bar{
		display:none;
	}
	.desktop-bar{
		display:block;
	}
@media (max-width: 768px){
	.modal-dialog h1{
		font-size: 30px;
	}
	.modal-dialog button#snapshot, .modal-dialog button#download, .modal-dialog button.popup-close {
		color: #fff;
		width: auto;
		margin: 0;
		font-size: 12px;
		font-weight: bold;
		line-height: 20px;
		padding: 8px 0;
		border:0;
		border-right: solid 1px #fff;
		-moz-box-shadow: inset 0px 1px 0px 0px #c7e6ff;
		-webkit-box-shadow: inset 0px 1px 0px 0px #c7e6ff;
		box-shadow: inset 0px 1px 0px 0px #c7e6ff;
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #afd8fa), color-stop(1, #3379b7));
		background: -moz-linear-gradient(top, #afd8fa 5%, #3379b7 100%);
		background: -webkit-linear-gradient(top, #afd8fa 5%, #3379b7 100%);
		background: -o-linear-gradient(top, #afd8fa 5%, #3379b7 100%);
		background: -ms-linear-gradient(top, #afd8fa 5%, #3379b7 100%);
		background: linear-gradient(to bottom, #afd8fa 5%, #3379b7 100%);
			background-color: rgba(0, 0, 0, 0);
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#afd8fa', endColorstr='#3379b7',GradientType=0);
		background-color: #afd8fa;
		-moz-border-radius: 0;
		-webkit-border-radius: 0;
		border-radius: 0;
		flex:1
	}
	.button-div{
		display:flex;
		margin:0 15px 15px;
		border-radius: 5px;
		overflow: hidden;
		padding: 30px 0 0;
		position: relative;
	}
	.select-style {
		float: none;
		margin: 12px 15px ;
		width: calc(100% - 30px);
	}
	.modal-dialog button.popup-close {
		border: 0 !important;
	}
	#progress-wrp {
		width: calc(100%);
		margin: 22px 0 0 33px;
		margin: 0;
	}
	.mobile-bar{
		display:block
	}
	.desktop-bar{
		display:none
	}
}
@media (max-width: 500px){
	.modal-dialog h1{
		font-size: 20px;
	}
}
@media (max-width: 767px) and (min-height: 480px){
	.modal-dialog {
		width: 100vw !important;
		height: 100vh !important;
	}
	.modal-dialog video, .modal-dialog .canvas-outer {
		width: calc(100% - 30px) !important;
		margin: 0 15px 10px;
		float: none;
		height: calc(40vh - 50px) !important;
	}
	.select-style {
		margin-top:0;
	}
}
@media (max-width: 800px) and (min-height: 1200px){
	.modal-dialog {
		width: 100vw !important;
		height: 100vh !important;
	}
	.modal-dialog video, .modal-dialog .canvas-outer {
		width: 100% !important;
		margin: 0 0 10px;
		float: none;
		height: calc(40vh - 50px) !important;
	}
	.select-style {
		margin-top:0;
	}
}
</style>
<div id="container">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  <span aria-hidden="true">&times;</span>
	</button>
    <h1>
	
	Take picture
    </h1>

    <video playsinline autoplay id="take-photo-id"></video>
	<div class="canvas-outer"><canvas></canvas></div>
	<div class="clearfix"></div>
	<!--<div class="select-style">
			<label for="filter">Filter: </label>
			<select id="filter">
				<option value="none">None</option>
				<option value="blur">Blur</option>
				<option value="grayscale">Grayscale</option>
				<option value="invert">Invert</option>
				<option value="sepia">Sepia</option>
			</select>
		</div>-->
		<div id="progress-wrp"  class="mobile-bar" style="display:none;"><div class="progress-bar"><div class="status">0%</div></div ></div>
	<div class="button-div">
		
		<button id="snapshot">Take snapshot</button>
		<button id="download">Upload</button>
		<button type="button" class="btn btn-secondary popup-close" data-dismiss="modal">Close</button>
		
		<div id="progress-wrp"  class="desktop-bar" style="display:none;"><div class="progress-bar"><div class="status">0%</div></div ></div>
	</div>
	
    <!--p>Draw a frame from the getUserMedia video stream onto the canvas element, then apply CSS filters.</p>

    <p>The variables <code>canvas</code>, <code>video</code> and <code>stream</code> are in global scope, so you can
        inspect them from the console.</p>

    <a href="https://github.com/webrtc/samples/tree/gh-pages/src/content/getusermedia/filter"
       title="View source for this page on GitHub" id="viewSource">View source on GitHub</a-->
</div>

<?php echo '<script'; ?>
 src="/includes/assets/js/plugins/webRtc/adapter-latest.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="/includes/assets/js/plugins/webRtc/src/content/getusermedia/filter/js/main.js?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
" async><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
>
	$(document).ready(function(){
		$('#filter').selectpicker();
	})
	<?php echo '</script'; ?>
><?php }
}
