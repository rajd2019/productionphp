<?php
/* Smarty version 3.1.31, created on 2018-08-31 12:48:36
  from "/var/app/current/content/themes/default/templates/_sidebar.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b893924e3d056_46311255',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1cd8693fc10f0d652a12e7ed135fbcdcda056c8a' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/_sidebar.tpl',
      1 => 1534853695,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b893924e3d056_46311255 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="site_bar_new">
<div class="sidebar_custom_trands">
<a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>"><div class="profile-bg-thumb">

<?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_cover']) {?><img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_cover'];?>
"><?php }?>

</div></a>

<div class="side_profile">

<?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_picture']) {?><div class="pro_thumb"><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>"><img src="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_picture'];?>
"></a></div><?php }?>



<div class="admin_detail">

<h3><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>"><?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_lastname'];?>
</a></h3>

<span><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>">@<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
</a></span>

</div>

<ul>

<li><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><?php echo __("Posts");?>
<span><?php if ($_smarty_tpl->tpl_vars['user']->value->_data['posts_count']) {
echo $_smarty_tpl->tpl_vars['user']->value->_data['posts_count'];
} else { ?>0<?php }?></span></a></li>

<li><a href="/followings.php?username=<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><?php echo __("Following");?>
<span><?php if ($_smarty_tpl->tpl_vars['user']->value->_data['followings_count']) {
echo $_smarty_tpl->tpl_vars['user']->value->_data['followings_count'];
} else { ?>0<?php }?></span></a></li>

<li><a href="/followers.php?username=<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><?php echo __("Followers");?>
<span><?php if ($_smarty_tpl->tpl_vars['user']->value->_data['followers_count']) {
echo $_smarty_tpl->tpl_vars['user']->value->_data['followers_count'];
} else { ?>0<?php }?></span></a></li>

<!--<li><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><?php echo __("Broadcasts");?>
<span><?php if ($_smarty_tpl->tpl_vars['user']->value->_data['broadcast_count']) {
echo $_smarty_tpl->tpl_vars['user']->value->_data['broadcast_count'];
} else { ?>0<?php }?></span></a></li>-->

</ul>

</div>
</div>


<!-- Disable weekily trends -->
<!--<div class="sidebar_trends">

<h3 title="<?php echo __("Weekly Trends for you");?>
"><?php echo __("Weekly Trends for you");?>
</h3>



<?php if ($_smarty_tpl->tpl_vars['user']->value->_data['popular_tag'] != 'No Record Found') {?>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['user']->value->_data['popular_tag'], 'custom_field');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['custom_field']->value) {
?>

<div class="fb_post_rep">

<h4><a href="/trend_post.php?tags=<?php echo $_smarty_tpl->tpl_vars['custom_field']->value['url_tags'];?>
"><?php echo $_smarty_tpl->tpl_vars['custom_field']->value['tags'];?>
</a></h4>

<p><?php echo $_smarty_tpl->tpl_vars['custom_field']->value['count'];?>
 <?php echo __("Posts");?>
</p>

</div>

<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


<?php } else { ?>

<h4><?php echo __("No Trends for you.");?>
</h4>

<?php }?>



</div>-->



</div><?php }
}
