<?php
/* Smarty version 3.1.31, created on 2018-09-07 14:49:11
  from "/var/app/current/content/themes/default/templates/_video_post.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b928fe7a7b160_97576467',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '21b51641a347dc2d5c737e6873d13f54a83c4f19' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/_video_post.tpl',
      1 => 1536321856,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:__video_post.body.tpl' => 1,
    'file:__feeds_post.social.tpl' => 1,
  ),
),false)) {
function content_5b928fe7a7b160_97576467 (Smarty_Internal_Template $_smarty_tpl) {
?>
  <?php echo '<script'; ?>
 src="/content/themes/default/templates/static/js/jquery-1.11.3.min.js"><?php echo '</script'; ?>
>
  <link rel="stylesheet" href="/content/themes/default/templates/static_shared/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="/content/themes/default/templates/static_shared/css/sample-chat.css">
   <link href='https://fonts.googleapis.com/css?family=Exo+2:400,900italic,900,800italic,800,700italic,700,600italic,600,500italic,500,400italic,300italic,200italic,200,100italic,100,300'
        rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,900italic,900,800italic,800,700italic,700,600italic,600,500italic,500,400italic,300italic,200italic,200,100italic,100,300'
        rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="/includes/assets/css/font-awesome/css/custom.css">
  <link href="https://unpkg.com/video.js/dist/video-js.css" rel="stylesheet">
  <?php echo '<script'; ?>
 src="https://unpkg.com/video.js/dist/video.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="https://unpkg.com/videojs-contrib-hls/dist/videojs-contrib-hls.js"><?php echo '</script'; ?>
>
  
  <style>
      .video-title, .video-des, .video-view {
          padding:5px;
      }
      .description {
          margin-top:10px;
      }
  </style>
  <div class="col-sm-12" style="margin-top:20px">
<?php if (!$_smarty_tpl->tpl_vars['standalone']->value) {?><li><?php }?>
    <!-- post -->
    <div class="post <?php if ($_smarty_tpl->tpl_vars['boosted']->value) {?>boosted<?php }?>" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">

        <?php if ($_smarty_tpl->tpl_vars['standalone']->value && $_smarty_tpl->tpl_vars['pinned']->value) {?>
            <div class="pin-icon" data-toggle="tooltip" title="<?php echo __('Pinned Post');?>
">
                <i class="fa fa-bookmark"></i>
            </div>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['standalone']->value && $_smarty_tpl->tpl_vars['boosted']->value) {?>
            <div class="boosted-icon" data-toggle="tooltip" title="<?php echo __('Promoted');?>
">
                <i class="fa fa-bullhorn"></i>
            </div>
        <?php }?>

       <?php if (!$_smarty_tpl->tpl_vars['_shared']->value && $_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
      <div class="pull-right flip dropdown custom-dropdown-profile"> 
        <i class="fa fa-chevron-down dropdown-toggle" data-toggle="dropdown"></i>
        <ul class="dropdown-menu post-dropdown-menu">
         <?php if ($_smarty_tpl->tpl_vars['post']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
          <li> <a href="#" class="" data-toggle="modal" data-url="chat/direct_share.php?id=<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
            <div class="action no-desc"> <i class="fa fa-bookmark fa-fw"></i> <span><?php echo __("Share via Direct Message ");?>
</span> </div>
            </a> </li>
            <li><span id="p<?php echo $_smarty_tpl->tpl_vars['post']->value['user_id'];?>
" style="display:none;"></span> <a href="javascript:void(0)"  data-clipboard-text="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/posts/<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" class="copy_link">
            <div class="action no-desc"> <i class="fa fa-bolt fa-fw"></i> <span><?php echo __("Copy Link to Post");?>
</span> </div>
            </a> </li>
            <li> <?php if ($_smarty_tpl->tpl_vars['post']->value['pinned']) {?> <a href="#" class="js_unpin-post">
            <div class="action no-desc"> <i class="fa fa-thumb-tack fa-fw"></i> <span><?php echo __("Unpin Post");?>
</span> </div>
            </a> <?php } else { ?> <a href="#" class="js_pin-post">
            <div class="action no-desc"> <i class="fa fa-thumb-tack fa-fw"></i> <span><?php echo __("Pin Post");?>
</span> </div>
            </a> <?php }?> </li>
             <li> <a href="#" class="js_delete-post">
            <div class="action no-desc"> <i class="fa fa-trash-o fa-fw"></i> <?php echo __("Delete Post");?>
 </div>
            </a> </li>
          <?php } else { ?>
           <li><span id="p<?php echo $_smarty_tpl->tpl_vars['post']->value['user_id'];?>
" style="display:none;">/includes/ajax/posts/product_editor.php?post_id=<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
</span> <a href="javascript:void(0)"  data-clipboard-text="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/posts/<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" class="copy_link">
            <div class="action no-desc"> <i class="fa fa-bolt fa-fw"></i> <span><?php echo __("Copy Link to Post");?>
</span> </div>
            </a> </li>

    <li class="sendbird-chat-cls-shared">
    <input type="hidden" name="accessToken" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['accessToken'];?>
">
      <div class="init-check"></div>
    
      <div class="sample-body">
    
        <!-- left nav -->
          <a href="//sendbird.com" target="_blank"><div class="left-nav-icon"></div></a>
          <div class="left-nav-channel-select">
            <!-- <button type="button" class="left-nav-button left-nav-open" id="btn_open_chat">
              OPEN CHANNEL
              <div class="left-nav-button-guide"></div>
            </button> -->
            <input type="hidden" name="post-url" id="post-url" value="http://demo2.ongraph.com/demo/chat/direct_share.php?id=<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
            <a href="#" class="shared_messaging_chat">
            <!-- <button type="button" class="left-nav-button left-nav-messaging shared_messaging_chat" > -->
              <div class="action no-desc"> <i class="fa fa-bookmark fa-fw"></i> <span><?php echo __("Share via Direct Message ");?>
</span> </div>
              <div class="left-nav-button-guide"></div>
           </a>
            <!-- </button> -->
          </div>
      </div>
    </li>
        
    <li> <a href="#" class="js_hide-post">
      <div class="action"> <i class="fa fa-eye-slash fa-fw"></i> <?php echo __("Mute Post");?>
 </div>
      <div class="action-desc"><?php echo __("See fewer posts like this");?>
</div>
      </a> </li>
    <li> <a href="#" class="js_block-comment" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['user_id'];?>
">
      <div class="action"> <i class="fa fa-ban fa-fw"></i> <?php echo __("Block");?>
 <?php echo $_smarty_tpl->tpl_vars['post']->value['post_author_name'];?>
</div>
      <div class="action-desc"><?php echo __("Block this user");?>
</div>
      </a> </li>
    <li> <a href="#" class="js_report" data-handle="post" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
      <div class="action no-desc"> <i class="fa fa-flag fa-fw"></i> <?php echo __("Report post");?>
 </div>
      </a> </li>

     <?php }?>
   
    </ul>
  </div>
  <?php }?>

        <!-- post body -->
         <?php if ($_smarty_tpl->tpl_vars['post']->value['post_id'] != 'a') {?>
        <div class="post-body" style="padding-bottom: 50px;" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
            
            <?php $_smarty_tpl->_subTemplateRender('file:__video_post.body.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_post'=>$_smarty_tpl->tpl_vars['post']->value,'_shared'=>false), 0, false);
?>


            <!-- post stats -->
            
            <!-- post stats -->

            <!-- post actions -->
            
            <!-- post actions -->

        </div>
        <?php }?>
        <div class="custom-post-actions" style="float: none !important;display: block !important;">
        <div class="post-actions">
                <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                <!-- comment -->
                <span class="text-clickable js-custom-share-comment mr20" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" data-handle="post">
                    <i class="Icon Icon--medium Icon--reply" data-tooltip="tooltip" data-placement="top" title="" data-original-title="<?php echo __("Reply");?>
"></i> <span><!--<?php echo __("Comment");?>
--><?php echo $_smarty_tpl->tpl_vars['post']->value['comments'];?>
</span>
                </span>
                <!-- comment -->

                <!-- share -->

                    <span class="text-clickable mr20 share-tweet-post" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
                        <i class="Icon Icon--medium Icon--retweet" data-tooltip="tooltip" data-placement="top" title="" data-original-title="<?php echo __("Repost");?>
"></i> <span><!--<?php echo __("Share");?>
--><?php echo $_smarty_tpl->tpl_vars['post']->value['shares'];?>
</span>
                    </span>

                <!-- share -->
                <!-- like -->
                <span class="text-clickable <?php if ($_smarty_tpl->tpl_vars['post']->value['i_like']) {?>text-active js_unlike-post<?php } else { ?>js_like-post<?php }?>">
                    <i class="Icon Icon--heart Icon--medium" data-tooltip="tooltip" data-placement="top" title="" data-original-title="<?php echo __("Like");?>
"></i> <i class="Icon Icon--heartBadge Icon--medium"></i>
                    <span class="span-counter_<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['post']->value['likes'];?>
</span>
                </span>
                <!-- like -->

                <!-- Direct Message -->

                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_id'] != $_smarty_tpl->tpl_vars['post']->value['author_id']) {?>
                <span class="text-clickable direct_message_frm_post" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
                    <i class="Icon Icon--envelope Icon--medium" data-tooltip="tooltip" data-placement="top" title="" data-original-title="<?php echo __("Direct message");?>
"></i>

                </span>
                <?php }?>
                <!-- Direct Message -->
                <?php } else { ?>
                    <a href="/signin"><?php echo __("Please log in to like, share and comment!");?>
</a>
                <?php }?>
				<div class="sharethis-inline-share-buttons" data-url="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/posts/<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"></div>
        </div>
        
        </div>
        <div style="float: right; padding: 0 15px 15px 15px;position: relative;    width: auto; margin-top: -38px; display: inline-block;">
            <span class="total_views"><span class="update_count"><?php echo $_smarty_tpl->tpl_vars['totalviews']->value;?>
</span> <?php echo __('Views');?>
</span>
        </div>
        <!-- post body -->

        <!-- post footer -->
        <div class="post-footer <?php if (!$_smarty_tpl->tpl_vars['standalone']->value) {?>x-hidden<?php }?>">
            <!-- social sharing -->
            <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.social.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <!-- social sharing -->

            <!-- comments -->
            
            <!-- comments -->
        </div>
        <!-- post footer -->

    </div>
    <!-- post -->
<?php if (!$_smarty_tpl->tpl_vars['standalone']->value) {?></li><?php }?>

<!--div class="other-title-bar video-page" style="margin-top:30px;">
    <h4>How to broadcast in Guo Media?</h4>
    <p>We welcome everyone to express their viewpoints freely in our platform. Currently, we allow users to do broadcast via our mobile apps while audience can watch in both website and mobile apps. Please download our apps in App Store to become one of our broadcasters today! In the future, we will allow users to broadcast via website as well. Thank you so much for your support, everything is just beginning ️️️! 
</p>
<div class="google_link"> <a href="#"><img src="/content/themes/default/images/google-play.png" alt="google-play"></a> <a href="#"><img src="/content/themes/default/images/apple-store.png" alt="apple-store"></a> </div>
</div-->
</div>


  <?php echo '<script'; ?>
 src="/content/themes/default/templates/static_shared/lib/SendBird.min.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="/content/themes/default/templates/static_shared/js/util.js"><?php echo '</script'; ?>
>
  <!-- <?php echo '<script'; ?>
 src="/content/themes/default/templates/static_shared/js/chat.js"><?php echo '</script'; ?>
> --><?php }
}
