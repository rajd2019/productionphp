<?php
/* Smarty version 3.1.31, created on 2018-08-31 13:16:19
  from "/var/app/current/content/themes/default/templates/privacy.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b893fa326a813_06115876',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '02d6cdd90cc4abca1aab62d03ec7898e5d1a3e3b' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/privacy.tpl',
      1 => 1527699516,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:_sidebar.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_5b893fa326a813_06115876 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<style>
  @media(max-width: 767px)
  {
	  .panel-default {
		text-align: left;    
	  }                                                                                                                                                                            
	.offcanvas-sidebar{
	width:100%
	}
  }
</style>

<!-- page content -->
<div class="page-title">
    <?php echo __("Privacy");?>
 
</div>

<div class="container offcanvas">
    <div class="row">

	    <!-- side panel -->
	    <div class="col-xs-12 visible-xs-block offcanvas-sidebar mt20">
	        <?php $_smarty_tpl->_subTemplateRender('file:_sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	    </div>
	    <!-- side panel -->

	    <div class="col-xs-12 offcanvas-mainbar">
			<div class="row">
		        <div class="col-xs-10 col-xs-offset-1 text-readable ptb10">
				<?php if ($_smarty_tpl->tpl_vars['system']->value['language']['code'] == 'en_us') {?>
		            <h3 class="text-info"> <?php echo __("Personal Data (Privacy) Policy Statement");?>
 (“<?php echo __("Privacy Policy");?>
”) </h3>
                    <p><?php echo __("China Golden Spring Group (Hong Kong) Limited");?>
 (“<?php echo __("Company");?>
”) <?php echo __("is based in Hong Kong and pledges to meet fully with the requirements of the Personal Data (Privacy) Ordinance, Chapter 486 of the Laws of Hong Kong. In doing so, the Company will ensure compliance by its staff to the strictest standards of security and confidentiality in respect of all personal information and data submitted by users via GUO.MEDIA and its sub-domains (collectively,");?>
 “<?php echo __("Sites");?>
”) <?php echo __("and the Company will not release such information to anyone without the prior consent of the relevant user(s) of the Sites (whether registered or not)");?>
 (“<?php echo __("User(s)");?>
”) <?php echo __("except to the authorized persons listed under paragraph 3 below.");?>
 </p>
                    <p><?php echo __("Users are strongly recommended to read this Privacy Policy carefully to have an understanding of the Company’s policy and practices with regard to the treatment of personal information and data provided by the Users on the Sites. This Privacy Policy is applicable to both registered and non-registered Users, and the terms contained herein may be updated, revised, varied and/or amended from time to time.");?>
</p>
                    <p><?php echo __("If Users have questions or concerns regarding this Privacy Policy, they should email to support@guo.media.");?>
</p>
                    
                    <ol type="1">
                    <li>
                    <h3 class="text-info"><?php echo __("1. Purpose of Collection of Personal Data");?>
</h3> 
                    <p><?php echo __("In the course of using the Sites, Users may disclose or be asked to provide personal information and/or data. In order to have the benefit of and enjoy various services offered by the Sites, it may be necessary for Users to provide the Company with their personal information and/or data. Although Users are not obliged to provide the information and/or data as requested on the Sites, the Company will not be able to render certain services on the Sites in the event that Users fail to do so.");?>
</p> 
                    <p><?php echo __("The Company’s purposes for collection of information and data on the Sites include but not limited to the following:");?>
</p> 
                    <ol type="a"> 
                    <li><?php echo __("(a) for the daily operation of the services provided to Users;");?>
</li> 
                    <li><?php echo __("(b) to identify Users who have posted advertisements, materials, messages, photos, views or comments or such other information (collectively");?>
 “<?php echo __("Information");?>
”) <?php echo __("on the Sites;");?>
</li> 
                    <li><?php echo __("(c) to identify Users who have viewed the Information posted on the Sites;");?>
</li> 
                    <li><?php echo __("(d) to provide Users with marketing and promotional materials for their enjoyment of benefits as members of the Sites (for further details, please refer to paragraph 4 headed");?>
 “<?php echo __("Subscription of Newsletter/Promotional Materials/Marketing Materials");?>
” <?php echo __("below);");?>
</li> 
                    <li><?php echo __("(e) to identify Users who have enjoyed their benefits as members of the Sites by receiving and using marketing and promotional materials;");?>
</li> <li><?php echo __("(f) to provide Users with a platform and forum for posting photos, sharing and discussing their insights;");?>
</li> 
                    <li><?php echo __("(g) to allow members of the Sites to enjoy their benefits as members by enrolling for special events hosted by the Company and/or its affiliates;");?>
 </li> 
                    <li><?php echo __("(h) to design and provide products and services to Users in relation to the above purposes;");?>
 </li> 
                    <li><?php echo __("(i) to compile aggregate statistics about the Users and to analyze the service usage of the Sites for the Company’s internal use; and");?>
</li> 
                    <li><?php echo __("(j) to facilitate the Company and/or its affiliates to use the Users’ personal data for purposes relating to the provision of services offered by the Company and marketing services, special events and/or promotions of the Company, its affiliates and/or their respective clients.");?>
</li> 
                    </ol> 
                    <p><?php echo __("If the User is under the age of 13, the Company would strongly recommend him/her to seek prior consent from a person with parental responsibility for him/her, e.g. parent or guardian, who may contact the responsible personnel of the Company at support@guo.media for registering the User as member of the Sites.");?>
</p> 
                    <p><?php echo __("The Company strives to only collect personal data of Users which is necessary for the purposes set out hereinabove.");?>
</p> 
                    <p><?php echo __("The Company may use the Users’ personal data for a purpose other than those set out hereinabove. The Company will obtain consent from the Users for such use prior to the use.  If User is a minor, the prescribed consent should be given by his/her parent or guardian.");?>
</p> 
                    </li>
                    <li> 
                    <h3 class="text-info"><?php echo __("2. Collection of Personal Data");?>
</h3> 
                    <p><?php echo __("The Company may collect personal information and/or data about a User such as his/her name, log-in ID and password, address, email address, phone number, age, sex, date of birth, country of residence, nationality, education level and work experience that is/are not otherwise publicly available. Occasionally, the Company may also collect additional personal information and/or data from a User in connection with contests, surveys, or special offers.");?>
</p> 
                    <p><?php echo __("Only duly authorized staff of the Company will be permitted to access the Users’ personal information and data, and the Company shall not release such personal information and data to any third parties save and except for the circumstances listed out under the Paragraph 3. entitled “Disclosure or Transfer of Data”.");?>
</p> 
                    </li>
                    
                    <li> 
                    <h3 class="text-info"><?php echo __("3. Disclosure or Transfer of Data");?>
</h3>
                    <p><?php echo __("The Company agrees to take all practicable steps to keep all personal information and data of Users confidential and/or undisclosed, subject to the following.");?>
</p> 
                    <p><?php echo __("Generally speaking, the Company will only disclose and/or transfer Users’ personal information and/or data to the Company’s personnel and staff for the purpose of providing services to Users. However, the Company may disclose and/or transfer such information and/or data to third parties under the following circumstances:");?>
</p> 
                    <ol type="a"> 
                    <li><?php echo __("(a) where the information and/or data is disclosed and/or transferred to any third party suppliers or external service providers who have been duly authorized by the Company to use such information and/or data and who will facilitate the services on the Sites, under a duty of confidentiality;");?>
</li> 
                    <li><?php echo __("(b) where the information and/or data is disclosed and/or transferred to any agents, affiliates or associates of the Company who have been duly authorized by the Company to use such information and/or data;");?>
</li> 
                    <li><?php echo __("(c) where the Company needs to protect and defend its rights and property;");?>
</li> 
                    <li><?php echo __("(d) where the Company considers necessary to do so in order to comply with the applicable laws and regulations, including without limitation compliance with a judicial proceeding, court order, or legal process served on the Sites; and");?>
</li> 
                    <li><?php echo __("(e) where the Company deems necessary in order to maintain and improve the services on the Sites.");?>
</li> 
                    </ol> 
                    <p><?php echo __("Personal data collected via the Sites may be transferred, stored and processed in any country in which the Company or its affiliates operate. By using the Sites, Users are deemed to have agreed to, consented to and authorized the Company to disclose and/or transfer their personal information and data under the circumstances stated above, as well as to any transfer of information (including the Information) outside of the Users’ country.");?>
</p> 
                    </li>
                    
                    <li> 
                    <h3 class="text-info"><?php echo __("4. Subscription of Newsletter/Promotional Materials/Marketing Materials");?>
</h3> 
                    <p><?php echo __("The Company and its affiliates may from time to time send to members and Users of the Sites newsletters, promotional materials and marketing materials based on the personal information and data that they have provided to the Company. The Company may use Users’ data in direct marketing and the Company requires the Users’ consent (which includes an indication of no objection) for that purpose. In this connection, please note that:");?>
</p> 
                    <ol type="a"> 
                    <li><?php echo __("(a) the name, log-in ID and password, contact details, age, sex, date of birth, country of residence, nationality, education level and work experience of Users held by the Company from time to time may be used by the Company and/or its authorised personnel or staff in direct marketing;");?>
</li> 
                    <li><?php echo __("(b) the following classes of services, products and subjects may be marketed:");?>

          			<ol type="i"> 
                    <li><?php echo __("I. special events hosted by the Company and its affiliates for members and Users, including but not limited to courses, workshops, and competitions;");?>
</li> 
                    <li><?php echo __("II. reward, loyalty or privileges programmes and related products and services;");?>
</li> 
                    <li><?php echo __("III. special offers including coupons, discounts, group purchase offers and promotional campaigns;");?>
</li> 
                    <li><?php echo __("IV. products and services offered by the Company’s affiliates and advertisers (the names of such affiliates and advertisers can be found in the relevant advertisements and/or promotional or marketing materials for the relevant products and services, as the case may be);");?>
</li> 
                    <li><?php echo __("V. donations and contributions for charitable and/or non-profit making purposes;");?>
</li> 
                    </ol> 
                    </li> 
                    <li><?php echo __("(c) The above products, services and subjects may be provided or (in the case of donations and contributions) solicited by the Company and/or:");?>

          			<ol type="i"> 
                    <li><?php echo __("I. the Company’s affiliates;");?>
</li> 
                    <li><?php echo __("II. third party service providers providing the products, services and subjects listed in paragraph (b) above; and");?>
</li> 
                    <li><?php echo __("III. charitable or non-profit marking organizations;");?>
</li> 
                    <li><?php echo __("IV. in addition to marketing the above services, products and subject itself, the Company also intends to provide the data described in paragraph (a) above to all or any of the persons described in paragraph (c) above for use by them in marketing those services, products and subjects, and the Company requires the Users’ written consent (which includes an indication of no objection) for that purpose;");?>
</li> 
                    <li><?php echo __("V. the Company may receive money or other property in return for providing the data to the other persons in paragraph (d) above and, when requesting the Users’ written consent as described in paragraph (d) above, the Company will inform the Users if the Company receives any money or other property in return for providing the data to the other persons.");?>
</li> 
                    </ol>
          			<?php echo __("Suitable measures are implemented to make available to such members the options to “opt-out” of receiving such materials. In this regard, Users may choose to sign up or unsubscribe for such materials by logging into the registration or user account maintenance webpage, or clicking on the automatic link appearing in each newsletter/message, or contact us by sending email to support@guo.media. ");?>

                    </li> 
                    </ol> 
                    </li>
                    
                    <li> 
                    <h3 class="text-info"><?php echo __("5. Access");?>
</h3> 
                    <p><?php echo __("Any User is entitled to request access to or make amendments to his/her own personal information and data kept with the Company by contacting us via email to support@guo.media.");?>
 </p> 
                    <p><?php echo __("In the event that a User wishes to access or amend his/her personal information and data, the Company may request him/her to provide personal details in order to verify and confirm his/her identity. HKID card number or passport number or business registration certificate number cannot be amended unless such data is proved to be inaccurate. The Company is required to respond to a User’s requests within 40 days of his/her request and will endeavor to do so wherever possible.");?>
</p> 
                    </li>
                    
                    <li id="cookie"> 
                    <h3 class="text-info"> <?php echo __("6. Cookies and Log Files");?>
</h3> 
                    <p><?php echo __("The Company does not collect any personally identifiable information from any Users whilst they visit and browse the Sites, save and except where such information of the Users is expressly requested. When Users access the Sites, the Company records their visits only and do not collect their personal information or data. The Sites’ server software will also record the domain name server address and track the pages the Users visit and store such information in “cookies”, and gather and store information like internet protocol (IP) addresses, browser type, referring/exit pages, operating system, date/time stamp, and clickstream data in log files. All these are done without the Users being aware that they are occurring.");?>
</p> 
                    <p><?php echo __("The Company does not link the information and data automatically collected in the above manner to any personally identifiable information. The Company generally uses such automatically collected information and data to estimate the audience size of the Sites, gauge the popularity of various parts of the Sites, track Users’ movements and number of entries in the Company’s promotional activities and special events, measure Users’ traffic patterns and administer the Sites. Such automatically collected information and data will not be disclosed save and except in accordance with the Paragraph 3 entitled Disclosure or Transfer of Data.");?>
 </p> 
                    </li>
                    
                    <li> 
                    <h3 class="text-info"><?php echo __("7. Links to Other Websites");?>
</h3> 
                    <p><?php echo __("The Sites may provide links to other websites which are not owned or controlled by the Company. Personal information and data from Users may be collected on these other websites when Users visit such websites and make use of the services provided therein. Where and when Users decide to click on any advertisement or hyperlink on the Sites which grants Users access to another website, the protection of Users’ personal information and data which are deemed to be private and confidential may be exposed in these other websites.");?>
</p> 
                    <p><?php echo __("Non-registered Users who gain access to the Sites via their accounts in online social networking tools (including but not limited to Facebook, Twitter, etc.) are deemed to have consented to the terms of this Privacy Policy, and such Users’ personal data which they have provided to those networking tools may be obtained by the Company and be used by the Company and its authorized persons in and outside of the User’s country for the purpose of providing services and marketing materials to the Users. The Company and its authorized personnel may gain access to and use these Users’ personal data so obtained, subject to the other provisions of this Privacy Policy.");?>
</p> 
                    <p><?php echo __("This Privacy Policy is only applicable to the Sites. Users are reminded that this Privacy Policy grants no protection to Users’ personal information and data that may be exposed on websites other than the Sites, and the Company is not responsible for the privacy practices of such other websites. Users are strongly recommended to refer to the privacy policy of such other websites.");?>
</p> 
                    </li>
                    
                    <li> 
                    <h3 class="text-info"><?php echo __("8. Security");?>
</h3> 
                    <p><?php echo __("The security of Users’ personal information and data is important to the Company. The Company will always strive to ensure that Users’ personal information and data will be protected against unauthorized access. The Company has implemented appropriate electronic and managerial measures in order to safeguard, protect and secure Users’ personal information and data.");?>
</p> 
                    <p><?php echo __("All personal information and data provided by Users are only accessible by the authorized personnel of the Company or its authorized third parties, and such personnel shall be instructed to observe the terms of this Privacy Policy when accessing such personal information and data. Users may rest assured that their personal information and data will only be kept for as long as is necessary to fulfill the purpose for which it is collected.  Registered Users should safeguard his/her unique Username and Password by keeping it secret and confidential and never share these details with anyone.");?>

                    </p> 
                    <p><?php echo __("The Company uses third party payment gateway service providers to facilitate electronic transactions on the Sites.  Regarding sensitive information provided by Users, such as credit card number for completing any electronic transactions, the web browser and third party payment gateway communicate such information using secure socket layer technology (SSL).");?>
</p> 
                    <p><?php echo __("The Company follows generally accepted industry standards to protect the personal information and data submitted by Users to the Sites, both during transmission and once the Company receives it. However, no method of transmission over the Internet, or method of electronic storage, is 100% secure. Therefore, while the Company strives to protect Users’ personal information and data against unauthorized access, the Company cannot guarantee its absolute security.");?>
</p> 
                    </li>
                    
                    <li> 
                    <h3 class="text-info"><?php echo __("9. Retention of Personal Data");?>
</h3> 
                    <p><?php echo __("Once the Company has obtained a User’s personal information and/or data, it will be maintained securely in the Company’s system. Subject to legal requirements, the personal information and/or data of Users will be retained by the Company after deactivation of the relevant service until the User requests the Company in writing to erase his/her own personal information and/or data from the Company's database or to terminate his/her membership of the Sites.");?>
</p> 
                    </li>
                    
                    <li> 
                    <h3 class="text-info"><?php echo __("10. Changes in this Privacy Policy");?>
 </h3> 
                    <p><?php echo __("The Company reserves the right to update, revise, modify or amend this Privacy Policy in the following manner at any time as the Company deems necessary and Users are strongly recommended to review this Privacy Policy frequently. If the Company decides to update, revise, modify or amend this Privacy Policy, the Company will post those changes to this webpage and/or other places the Company deems appropriate so that Users would be aware of what information the Company collects, how the Company uses it, and under what circumstances, if any, the Company discloses it.</p> <p>If the Company makes material changes to this Privacy Policy, the Company will notify Users on this webpage, by email, or by means of a notice on the home page of the Company.");?>
</p> 
                  
					<li> 
                    <h3 class="text-info"><?php echo __("11. Discrepancy");?>
 </h3> 
                    <p><?php echo __("This English version of the Terms and Conditions and any foreign language translation, the English version shall prevail.");?>
</p> </li>
					<li> 
                    <h3 class="text-info"><?php echo __("12. Changes in this Privacy Policy");?>
 </h3> 

                    <p><?php echo __("For any query, please contact us by sending email to support@guo.media.");?>
</p> 
                    <p><?php echo __("Effective: 23rd December 2017");?>
</p> </li>
                    </ol>
					<?php } else { ?>
					<p>私隱政策</p>

</p><p>中國金泉集團（香港）有限公司和/或其附屬公司和/或關聯公司（“本公司”) 的總部設於香港並承諾遵守香港法例第486章《個人資料（私隱）條例》的規定。本公司將確保其員工對用戶通過《guo.media》 (又名《郭傳媒》)、其子域、任何其他由本公司經營/開發的網站、媒體平臺或應用程式和流動網絡（“渠道”）提交的所有個人信息和數據遵守最嚴格的安全和保密標準。不經渠道用戶同意(無論註冊與否)(“用戶”)，本公司不會向任何人洩露此等個人資料，惟以下第3條“資料的披露或傳遞”中列出的獲授權人士除外。

</p><p>本公司強烈建議用戶仔細閱讀本私隱政策以便瞭解本公司對於用戶在渠道上提供的個人資料的相關政策和實際應用。本私隱政策適用於註冊和非註冊用戶，並且當本公司認為必要時可不定期更新、修訂、更改和/或修改相關條款內容。

</p><p>如果用戶對該私隱政策有任何疑問或顧慮，請電郵support@guo.media聯絡本公司。

</p><p>1.	收集個人資料的目的
</p><p>1.	本公司在渠道中收集個人資料的目的包括但不僅限於以下幾點：
</p><p>a.	為用戶提供服務的日常運作；
</p><p>b.	辨認及確認在渠道上發佈廣告、材料、照片、評論或其他資訊（“資訊”）的用戶身份；
</p><p>c.	識別瀏覽過渠道上資訊的用戶；
</p><p>d.	向用戶提供營銷和宣傳資訊作為會員的福利（有關進一步詳情，請參閱下述第4條關於“通訊、宣傳資訊、營銷資訊訂閱”的條款）；
</p><p>e.	辨別用戶通過營銷和宣傳資訊已享受過其為渠道會員之福利；
</p><p>f.	為用戶提供渠道和論壇以上傳照片、分享和交流有關服務或產品的意見；
</p><p>g.	作為會員福利，允許渠道會員參加本公司舉辦的特別活動；
</p><p>h.	就有關用戶於本公司之會員賬戶的行政通知和通信而聯絡有關用戶和/或就用戶通過渠道使用本公司提供的任何服務而發出有關的通知及確認；
</p><p>i.	統計渠道使用量的資料並分析渠道及服務使用情況以供本公司內部使用；
</p><p>j.	使用用戶的個人資料以促進本公司為用戶提供本公司之服務、本公司及/或其客戶之營銷服務、特別活動或促銷活動。

	</p><p>如用戶未滿13歲，本公司強烈建議他/她尋求父母或監護人的事先同意，其父母或監護人可電郵support@guo.media聯絡本公司，為用戶登記成為渠道會員。

	</p><p>本公司只會收集本公司認為相關且必要的個人資料；而不會就上述用途收集過量資料。

	</p><p>除上述目的以外，本公司亦有可能需要使用用戶的個人資料作其他目的，就此，本公司會徵求用戶的事先允許。如用戶為未成年人，本公司將會向他/她的父母或監護人徵求允許。
</p><p>
	<p>個人資料收集
	<p>本公司收集有關用戶個人資料和/或數據，如用戶名稱、登錄ID和密碼、地址、電子郵件地址、電話號碼、年齡、性別、出生日期、居住的國家、國籍、教育程度和工作經驗等非公開的資料。本公司亦會偶爾可從比賽、用戶調查、或特別優惠的方式收集額外的個人資料和/或數據。
</p><p>
	只有本公司授權的員工才會被允許接觸用戶的個人資料和數據，除了下述第3條 “資料的披露或傳遞” 所列出的情況外，本公司不得向任何第三方披露此等個人資料和數據。

	</p><p>資料的披露或傳遞
	</p><p>根據下列條款，本公司將採取各種可行措施以保障所有用戶個人資料和數據免其遭披露。

	</p><p>一般來說，本公司只將用戶的個人資料和/或數據披露和/或提供予本公司內部員工用作上述“收集個人資料的目的” 內提及的目的。然而，在下列情況下，本公司可能會將其資料和/或數據傳遞給第三方：
</p><p>a.	在保密原則下，當資料和/或數據披露和/或提供給任何本公司授權的第三方供應商或外部供應商，以促進渠道所提供的服務，包括但不限於受保密義務約束的商家;
</p><p>b.	當資料和/或數據披露和/或轉讓予本公司授權的任何代理人或其關聯公司，經本公司授權使用該等資料和/或數據；
</p><p>c.	本公司需要保護和捍衛其權利和財產；
</p><p>d.	本公司認為根據適用的法律法規，包括但不限於司法程序，法院命令，或渠道上的法律程序，而有必要提供；及
</p><p>e.	在本公司認為有必要以維持和改善渠道的服務。
</p><p>通過渠道所收集的個人資料可以在本公司運作的任何國家內轉移，儲存或處理。一旦使用渠道，用戶將被視為同意並授權本公司在上述情況下披露和轉載他們的個人資料和數據，以及將用戶資料（包括個人資料）轉載到其所在以外的其他國家。

	</p><p>通訊、宣傳資訊、營銷資訊訂閱
	</p><p>本公司會不時根據用戶在渠道上提供的個人資料和數據通過電子郵件和/或短信服務（SMS）向其傳送通訊、宣傳資訊和營銷資訊，及有關本公司或其第三方廣告商的產品、服務及相關的資訊。本公司在取得用戶的允許後（包括不反對的指示）利用用戶資料或數據進行直接營銷。在這方面，請注意：
</p><p>a.	本公司或其授權人和其員工或會在直銷中使用本公司持有的用戶名稱、登錄ID和密碼、聯繫資料、年齡、性別、出生日期、居住的國家、國籍、教育程度和工作履歷。
</p><p>b.	以下類別的服務，產品和項目或會被推銷：
</p><p>i.	本公司為會員和用戶舉辦的特別活動，包括但不限於講座、研討會、競賽等形式；
</p><p>ii.	獎賞，忠誠或特別待遇計劃和相關的產品和服務；
</p><p>iii.	優惠包括優惠券，折扣，團購優惠和促銷活動；
</p><p>iv.	由本公司和其廣告商提供的產品和服務 (這些實體的名稱可以在相關的產品和服務的廣告和/或營銷材料相中找到，因具體情況而定) ；
</p><p>v.	非牟利目的的慈善捐贈和捐款。
</p><p>c.	上述產品、服務和項目可由以下第三方提供或經本公司徵求（在捐贈和捐款的情況下）:
</p><p>i.	本公司的關聯公司；
</p><p>ii.	第三方服務供應商提供上述第（2）項所列的產品、服務和項目；及
</p><p>iii.	慈善或非牟利機構；
</p><p>d.	除行銷上述服務、產品和項目外，本公司亦有意向上述第（c）項中所述的全部或任何人士提供上述第（a）項所述的數據以促銷其服務、產品和項目。用於這一目的時，本公司需要徵求用戶書面同意（其中包括不反對的指示）;
</p><p>e.	本公司可能會為上述第（d）項所述的其他人士提供數據以換取金錢或其他財產等報酬，而當本公司徵求如上述（d）項所需的用戶書面同意，本公司將會通知用戶如本公司透過提供數據給其他人士而獲得金錢或其他財產的回報。

	</p><p>會員可以採取下列方法選項 “退出” 而不再接收此類資訊。用戶可以選擇登錄會員帳戶維護頁面，或點擊相關通訊/信息的自動連結，或可電郵support@guo.media聯絡本公司。

	</p><p>聯繫
	</p><p>任何用戶都有權要求聯繫本公司或修改他/她自己的個人信息和保存在公司的數據，可通過support@guo.media與我們聯繫。
	</p><p>如果用戶希望聯繫本公司或修改他/她的個人信息和數據，本公司可以要求他/她提供個人信息，以便核實和確認他/她的身份。香港身份證號碼或護照號碼或商業登記證號碼不可修改，除非這些數據被證實是不准確的。本公司盡力在用戶要求後的40天內回復用戶請求。

	</p><p>Cookies和日誌檔
	</p><p>除非用戶明確要求，當用戶進入並瀏覽渠道時，本公司不會收集保存任何個人身份的資料。當用戶登入渠道時，本公司僅記錄他們的登入而不收集他們的個人資料或數據。網站的伺服器軟體也只記錄功能變數名稱伺服器位址、追蹤用戶進入的頁面並將這些資訊儲存在“cookies”中，並諸如互聯網協定（IP）位址、瀏覽器類型、進/退頁面、作業系統、日期/時間戳記和點擊流量等資訊記錄在日誌檔中。所有這些都在用戶不知不覺中發生。

	</p><p>本公司不會以上述方式自動收集的信息和數據與任何個人身份信息連接。本公司一般會使用自動收集信息和數據來評估渠道的受眾規模、不同渠道的受歡迎程度、追蹤本公司的促銷活動和特別項目中的用戶變動及參賽人數，測量用戶的流量模式和管理渠道。這種自動收集資訊和資料除按照以上第3條款“資料的披露或傳遞”規定中披露，不會對外披露。


	</p><p>連結到其他網站/媒體平臺/應用程式
	</p><p>渠道會提供非本公司擁有或控制的其他網站/平臺/應用程式的連結。當用戶進入這些網站/平臺/應用程式和使用其提供的服務時，該用戶的個人資料和/或數據可能會在這些網站/平臺/應用程式中被收集。當用戶決定點擊渠道上允許用戶進入另一個網站/平臺/應用程式的任何廣告或超連結，該用戶的私密保護個人資料可能會顯露在這些其他網站/平臺/應用程式中。

	<p>非註冊用戶通過他們的線上社交網路工具帳戶（包括但不限於Facebook）登入或使用任何渠道將被視為已同意本私隱政策，本公司可得到他們提供予該等網路工具的個人資料，以便本公司和本公司授權人員於不同的國家為渠道使用者提供服務和營銷資料。如用戶通過他/她的Facebook帳戶登入任何渠道，本公司將會收集和儲存用戶已選擇提供給渠道的個人資料和信息，例如用戶的姓名、個人資料相片、性別、朋友列表。如用戶使用他/她的Google帳戶登入任何渠道，本公司將會收集和儲存用戶的手機號碼和電子郵件地址。這些用戶被視為同意本公司及其授權人員接觸和使用所得到的個人資料。

	</p><p>本私隱政策只適用於渠道。在此要提醒用戶，本私隱政策並不會對用戶顯露於其他網站/平臺/應用程式的個人資料數據而提供保障。有見及此，本公司強烈建議用戶應參考該等網站/平臺/應用程式的私隱政策。為免存疑，本公司不會就就用戶使用該等其他網站/平臺/應用程式承擔任何責任或義務。
</p><p>
	保安措施</p>
	<p>用戶的個人資料和/或數據的保障對本公司非常重要。本公司致力於確保用戶的個人資料和/或數據免受未經授權的接觸。本公司實施採用適當的電子、管理方面的措施以維護和保障用戶的個人資料安全。
</p>
	<p>所有用戶的個人資料和/或數據由本公司或其授權的第三方之授權人員才可接觸，而當這些授權人員接觸此類個人信息和數據時，會遵守本私隱政策的條款。用戶可以放心，他們的個人信息和數據只為有必要履行收集信息之目的而被保留，除非用戶要求本公司停止持有其個人信息或終止他/她的會員帳戶。這樣的要求可能會導致本公司無法於將來為用戶提供進一步的服務或信息和/或宣傳資料。
</p>
	<p>本公司採用第三方支付平臺服務供應商以促進渠道上的電子交易。對於用戶提供的敏感資訊，如完成電子付款交易的信用卡號碼，瀏覽器和第三方支付平臺直接使用安全通訊端層技術（SSL）來交流該等資訊。本公司或商家及提供服務的餐廳都不會收集或儲存任何用戶通過渠道提供的信用卡資料。
</p>
	<p>本公司遵循公認的行業標準保障用戶經渠道提交的個人資料和數據。然而，沒有一種百份之一百安全的互聯網傳輸或電子儲存方法。因此，儘管本公司致力於保護用戶的個人資料和數據免受未經授權的接觸，本公司亦不能保證其絕對安全。
</p>
	<p>個人資料的保存</p>
	<p>用戶的個人資料和/或數據一經取得將安全地保存在本公司的系統中。根據法律規定，用戶的個人資料和/或數據將會由本公司保留，直至用戶以書面形式要求本公司從資料庫中刪除其個人資料和/或數據或終止其個人在渠道的會員資格。
</p>
	<p>私隱政策的變更
	<p>當本公司認為必要時，本公司有權隨時按以下方式更新、修改、修訂或更改本私隱政策，強烈建議用戶頻繁地查看本隱私政策。若本公司決定更新、修改、修訂或更改本私隱政策，本公司將在此網頁和/或其他本公司認為合適的地方發佈該等變更資訊，以便於用戶能夠及時瞭解本公司收集的資料、本公司如何使用，以及在何種情況下本公司會披露該等資料。</p>
	<p>若本公司的私隱政策有重大變化，本公司將在此網頁上、透過電子郵件、或在本公司的主頁上發佈通告的方式以通知各用戶。</p>

	<p>歧義</p>
	<p>如英文版本與其他語言的版本有任何歧義，概以英文版本為準。</p>

	<p>聯絡方法</p>
	<p>如有任何疑問，請電郵support@guo.media聯絡本公司。</p>

<p>生效日期：2018年1月22日</p>

					<?php }?>
		        </div>
		    </div>
	    </div>
	    
	</div>
</div>
<!-- page content -->

<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
