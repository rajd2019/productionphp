<?php
/* Smarty version 3.1.31, created on 2018-08-31 12:51:50
  from "/var/app/current/content/themes/default/templates/__feeds_post_hover.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b8939e63f7d40_39844193',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '982908f2cb29f99a9586098e087bdd010c34dcd8' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/__feeds_post_hover.tpl',
      1 => 1531814016,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:__feeds_post.body.tpl' => 1,
    'file:__feeds_post.social.tpl' => 1,
    'file:__feeds_post.comments.tpl' => 1,
  ),
),false)) {
function content_5b8939e63f7d40_39844193 (Smarty_Internal_Template $_smarty_tpl) {
if (!$_smarty_tpl->tpl_vars['standalone']->value) {?><li><?php }?>
    <!-- post -->
    <div class="post <?php if ($_smarty_tpl->tpl_vars['boosted']->value) {?>boosted<?php }?>" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">

        <?php if ($_smarty_tpl->tpl_vars['standalone']->value && $_smarty_tpl->tpl_vars['pinned']->value) {?>
            <div class="pin-icon" data-toggle="tooltip" title="<?php echo __('Pinned Post');?>
">
                <i class="fa fa-bookmark"></i>
            </div>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['standalone']->value && $_smarty_tpl->tpl_vars['boosted']->value) {?>
            <div class="boosted-icon" data-toggle="tooltip" title="<?php echo __('Promoted');?>
">
                <i class="fa fa-bullhorn"></i>
            </div>
        <?php }?>

        <!-- post body -->
        <div class="">
            
            <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.body.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_post'=>$_smarty_tpl->tpl_vars['post']->value,'_shared'=>false,'onhover'=>'detail'), 0, false);
?>


            <!-- post stats -->
            <div class="post-stats">
				<?php if ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "video" && $_smarty_tpl->tpl_vars['post']->value['video']) {?>
                 <?php ob_start();
echo $_smarty_tpl->tpl_vars['post']->value['post_id'];
$_prefixVariable1=ob_get_clean();
echo $_smarty_tpl->tpl_vars['user']->value->video_view_manager($_prefixVariable1);?>

                    <!-- views -->
                        <span class="js_post-views-num"><?php ob_start();
echo $_smarty_tpl->tpl_vars['post']->value['views'];
$_prefixVariable2=ob_get_clean();
echo $_smarty_tpl->tpl_vars['user']->value->views_format($_prefixVariable2);?>
</span>
                    <!-- views -->
                <?php }?>
                 <!-- views 
                <span class="text-clickable" data-toggle="modal" data-url="posts/who_likes.php?post_id=<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
                    <i class="fa fa-thumbs-o-up"></i> 
                    <span class="js_post-likes-num">
                        <?php echo $_smarty_tpl->tpl_vars['post']->value['views'];?>
 views
                    </span>
                </span>
                views -->
                <!-- likes -->
                <!--
                <span class="text-clickable" data-toggle="modal" data-url="posts/who_likes.php?post_id=<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
                    <i class="fa fa-thumbs-o-up"></i> 
                    <span class="js_post-likes-num">
                        <?php echo $_smarty_tpl->tpl_vars['post']->value['likes'];?>

                    </span>
                </span>
                -->
                <!-- likes -->
				<!--
                <span class="pull-right flip">
                -->
                    <!-- comments -->
                    <!--
                    <span class="text-clickable js_comments-toggle">
                        <i class="fa fa-comments"></i> <?php echo $_smarty_tpl->tpl_vars['post']->value['comments'];?>
 <?php echo __("Comments");?>

                    </span>
                    -->
                    <!-- comments -->

                    <!-- shares -->
                    <!--
                    <span class="text-clickable ml10 <?php if ($_smarty_tpl->tpl_vars['post']->value['shares'] == 0) {?>x-hidden<?php }?>" data-toggle="modal" data-url="posts/who_shares.php?post_id=<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
                        <i class="fa fa-share"></i> <?php echo $_smarty_tpl->tpl_vars['post']->value['shares'];?>
 <?php echo __("Shares");?>

                    </span>
                    -->
                    <!-- shares -->
                <!--</span>-->
            </div>
            <!-- post stats -->

            <!-- post actions -->
            <div class="post-actions">
                <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                    <!-- comment -->
                    <span class="text-clickable mr20" id="detail-hover-comment">
                        <i class="Icon Icon--medium Icon--reply"></i> <span id="span-comments-counter-<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"><!--<?php echo __("Comment");?>
&nbsp;--><?php echo $_smarty_tpl->tpl_vars['post']->value['comments'];?>
</span>
                    </span>
                    <!-- comment -->

                    <!-- share -->
                    <?php if ($_smarty_tpl->tpl_vars['post']->value['privacy'] == "public") {?>
                        <span class="text-clickable mr20 share-tweet-post" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
                            <i class="Icon Icon--medium Icon--retweet"></i> <span id="span-share-counter-<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"><!--<?php echo __("Share");?>
&nbsp;--><?php echo $_smarty_tpl->tpl_vars['post']->value['shares'];?>
</span>
                        </span>
                    <?php }?>
                    <!-- share -->
                    <!-- like -->
                    <span class="text-clickable <?php if ($_smarty_tpl->tpl_vars['post']->value['i_like']) {?>text-active js_unlike-post<?php } else { ?>js_like-post<?php }?>">
                        <i class="Icon Icon--heart Icon--medium"></i><i class="Icon Icon--heartBadge Icon--medium"></i> 
                        <span class="span-counter_<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['post']->value['likes'];?>
</span>
                    </span>
                    <!-- like -->

                <?php } else { ?>
                    <a href="/signin"><?php echo __("Please log in to like, share and comment!");?>
</a>
                <?php }?>
				<div class="sharethis-inline-share-buttons" data-url="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/posts/<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"></div>
            </div>
            <!-- post actions -->

        </div>
        <!-- post body -->

        <!-- post footer -->
        <div class="<?php if (!$_smarty_tpl->tpl_vars['standalone']->value) {?>x-hidden<?php }?>">
            <!-- social sharing -->
            <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.social.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <!-- social sharing -->

            <!-- comments -->
            <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.comments.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <!-- comments -->
        </div>
        <!-- post footer -->

    </div>
    <!-- post -->
<?php if (!$_smarty_tpl->tpl_vars['standalone']->value) {?></li><?php }
}
}
