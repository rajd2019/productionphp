<?php
/* Smarty version 3.1.31, created on 2018-08-31 23:43:13
  from "/var/app/current/content/themes/default/templates/ajax.message.autocomplete.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b89d291e2c999_23067708',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cac5d24ab7cbb49756402ef7cf1734faed295159' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/ajax.message.autocomplete.tpl',
      1 => 1535704546,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b89d291e2c999_23067708 (Smarty_Internal_Template $_smarty_tpl) {
?>
<li class="DMTypeaheadHeader">
    <span>People</span>
</li>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['users']->value, '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
if ($_smarty_tpl->tpl_vars['_user']->value['user_firstname'] == '' || $_smarty_tpl->tpl_vars['_user']->value['user_lastname'] == '') {?>

<li class="DMTokenizedMultiselectSuggestion DMTypeaheadSuggestions-item" data-uuid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['uuid'];?>
" role="option" tabindex="-1" onclick="addNewMemberOnNewDialog('<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
', '<?php echo $_smarty_tpl->tpl_vars['_user']->value['uuid'];?>
')">

<?php } else { ?>

<li class="DMTokenizedMultiselectSuggestion DMTypeaheadSuggestions-item" data-uuid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['uuid'];?>
" role="option" tabindex="-1" onclick="addNewMemberOnNewDialog('<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>
', '<?php echo $_smarty_tpl->tpl_vars['_user']->value['uuid'];?>
')">

<?php }?>

        <div class="DMTokenizedMultiselectSuggestion-body">
            <div class="DMTypeaheadItem">
                <div class="DMTypeaheadItem-avatar" aria-hidden="true">
                    <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">
                        <span class="DMAvatar-container">
                            <img class="DMAvatar-image" src="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
" alt="<?php if ($_smarty_tpl->tpl_vars['_user']->value['user_firstname'] == '' || $_smarty_tpl->tpl_vars['_user']->value['user_lastname'] == '') {?>
                            <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>

                            <?php } else { ?>
                            <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>

                            <?php }?>">
                        </span>
                    </div>
                </div>
                <div class="DMTypeaheadItem-body">
                    <div class="DMTypeaheadItem-title account-group">
                        <b class="fullname">
                          <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_firstname'] == '' || $_smarty_tpl->tpl_vars['_user']->value['user_lastname'] == '') {?>
                          <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>

                          <?php } else { ?>
                          <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>

                          <?php }?>
                        </b>
                        <span class="UserBadges">
                            <span class="Icon Icon--verified">
                                <span class="u-hiddenVisually">Verified account</span>
                            </span>
                        </span>
                        <span class="UserNameBreak">&nbsp;</span>
                        <span class="username u-dir u-textTruncate" dir="ltr">
                            @<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>

                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="DMTokenizedMultiselectSuggestion-state">
            <span class="DMTokenizedMultiselectSuggestion-selectedIndicator Icon Icon--check"></span>
            <span class="DMTokenizedMultiselectSuggestion-preselectedIndicator">In Group</span>
        </div>
    </li>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
