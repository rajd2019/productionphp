<?php
/* Smarty version 3.1.31, created on 2018-08-31 12:49:03
  from "/var/app/current/content/themes/default/templates/__feeds_post_photo.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b89393fd12839_82608281',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '307eb582210126f79ac1e27678cd9f1b7290c3b4' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/__feeds_post_photo.tpl',
      1 => 1527699522,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:__feeds_post.social.tpl' => 1,
    'file:__feeds_post.comments.tpl' => 1,
  ),
),false)) {
function content_5b89393fd12839_82608281 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- post body -->
<div class="post-body <?php if ($_smarty_tpl->tpl_vars['_lightbox']->value) {?>pt0<?php }?>">

    <!-- post header -->
    <div class="post-header">
        <!-- post picture -->
        <div class="post-avatar">
            <a class="post-avatar-picture" href="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_author_url'];?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['post']->value['post_author_picture'];?>
);">
            </a>
        </div>
        <!-- post picture -->

        <!-- post meta -->
        <div class="post-meta">
            <!-- post menu & author name & type meta & feeling -->
            <div>
                <!-- post author name -->
                <span class="js_user-popover" data-type="<?php echo $_smarty_tpl->tpl_vars['post']->value['user_type'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['post']->value['user_id'];?>
">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_author_url'];?>
"><?php echo $_smarty_tpl->tpl_vars['post']->value['post_author_name'];?>
</a>
                </span>
                <?php if ($_smarty_tpl->tpl_vars['post']->value['post_author_verified']) {?>
                    <?php if ($_smarty_tpl->tpl_vars['post']->value['user_type'] == "user") {?>
                    <i data-toggle="tooltip" data-placement="top" title='<?php echo __("Verified User");?>
' class="fa fa-check-circle fa-fw verified-badge"></i>
                    <?php } else { ?>
                    <i data-toggle="tooltip" data-placement="top" title='<?php echo __("Verified Page");?>
' class="fa fa-check-circle fa-fw verified-badge"></i>
                    <?php }?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['post']->value['user_subscribed']) {?>
                <i data-toggle="tooltip" data-placement="top" title='<?php echo __("Pro User");?>
' class="fa fa-bolt fa-fw pro-badge"></i>
                <?php }?>
                <!-- post author name -->
            </div>
            <!-- post menu & author name & type meta & feeling -->

            <!-- post time & location & privacy -->
            <div class="post-time">
                <a href="/posts/<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" class="js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['post']->value['time'];?>
"><?php echo $_smarty_tpl->tpl_vars['post']->value['time'];?>
</a>

                <?php if ($_smarty_tpl->tpl_vars['post']->value['location']) {?>
                ·
                <i class="fa fa-map-marker"></i> <span><?php echo $_smarty_tpl->tpl_vars['post']->value['location'];?>
</span>
                <?php }?>

                 
               
            </div>
            <!-- post time & location & privacy -->
        </div>
        <!-- post meta -->
    </div>
    <!-- post header -->

    <!-- photo -->
    <?php if (!$_smarty_tpl->tpl_vars['_lightbox']->value) {?>
        <div class="mt10 clearfix">
            <div class="pg_wrapper">
                <div class="pg_1x">
                    <a href="/photos/<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" class="js_lightbox"  data-id="<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['photo']->value['source'];?>
" data-context="<?php if ($_smarty_tpl->tpl_vars['photo']->value['is_single']) {?>album<?php } else { ?>post<?php }?>">
                        <div class="text" style=" position: absolute; width: 595px; height: 100%;"></div><img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['photo']->value['source'];?>
">
                    </a>
                </div>
            </div>
        </div>
    <?php }?>
    <!-- photo -->

    <!-- post actions -->
    <div class="post-actions">
                <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                    <!-- comment -->
                    <span class="text-clickable mr20 js-custom-share-comment" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" data-handle="post">
                        <i class="Icon Icon--medium Icon--reply"></i> <span><!--<?php echo __("Comment");?>
&nbsp;--><?php echo $_smarty_tpl->tpl_vars['post']->value['comments'];?>
</span>
                    </span>
                    <!-- comment -->

                    <!-- share -->
                    <?php if ($_smarty_tpl->tpl_vars['post']->value['privacy'] == "public") {?>
                        <span class="text-clickable mr20 share-tweet-post" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
                            <i class="Icon Icon--medium Icon--retweet"></i> <span><!--<?php echo __("Share");?>
&nbsp;--><?php echo $_smarty_tpl->tpl_vars['post']->value['shares'];?>
</span>
                        </span>
                    <?php }?>
                    <!-- share -->
                    <!-- like -->
                    <span class="text-clickable <?php if ($_smarty_tpl->tpl_vars['post']->value['i_like']) {?>text-active js_unlike-post<?php } else { ?>js_like-post<?php }?>">
                        <i class="Icon Icon--heart Icon--medium"></i><i class="Icon Icon--heartBadge Icon--medium"></i> <span class="span-counter"><!--<?php echo __("Like");?>
&nbsp;--><?php echo $_smarty_tpl->tpl_vars['post']->value['likes'];?>
</span>
                    </span>
                    <!-- like -->

                <?php } else { ?>
                    <a href="/signin"><?php echo __("Please log in to like, share and comment!");?>
</a>
                <?php }?>
				<div class="sharethis-inline-share-buttons" data-url="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/posts/<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"></div>
        </div>
    <!-- post actions -->
</div>

<!-- post footer -->
<div class="post-footer custom-photo-comment">
    <!-- social sharing -->
    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.social.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <!-- social sharing -->

    <!-- comments -->
    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.comments.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_is_photo'=>!$_smarty_tpl->tpl_vars['photo']->value['is_single'] ? true : false), 0, false);
?>

    <!-- comments -->
</div>
<!-- post footer --><?php }
}
