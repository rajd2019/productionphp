<?php
/* Smarty version 3.1.31, created on 2018-08-31 12:51:50
  from "/var/app/current/content/themes/default/templates/ajax.posts_detail.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b8939e63dbde4_45871966',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bf8e90443af6523a5d4eb8d83fc881fa2f4dad73' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/ajax.posts_detail.tpl',
      1 => 1534833977,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:__feeds_post_hover.tpl' => 1,
  ),
),false)) {
function content_5b8939e63dbde4_45871966 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="modal-body post-hover-content" >
    <div class="js_scroller_popup" id="js_scroller_popup" onscroll="viewMoreComments(this)">
		<?php $_smarty_tpl->_subTemplateRender('file:__feeds_post_hover.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('standalone'=>true), 0, false);
?>

	</div>
</div>

<?php echo '<script'; ?>
>

function viewMoreComments(o) {
    var busy= false;
    if(o.offsetHeight + o.scrollTop == o.scrollHeight && (!busy)){

      if($( "#more-comments-id" ).length == 1){
          load_more_comments($('.js_see-more-comments'));
          busy = true;
      }
    }
}
<?php echo '</script'; ?>
>
<?php }
}
