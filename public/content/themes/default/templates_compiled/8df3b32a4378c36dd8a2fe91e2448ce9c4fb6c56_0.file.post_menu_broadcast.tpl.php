<?php
/* Smarty version 3.1.31, created on 2018-08-31 12:55:22
  from "/var/app/current/content/themes/default/templates/post_menu_broadcast.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b893aba4c5d51_68693451',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8df3b32a4378c36dd8a2fe91e2448ce9c4fb6c56' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/post_menu_broadcast.tpl',
      1 => 1527699516,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:__broadcast_post.tpl' => 1,
  ),
),false)) {
function content_5b893aba4c5d51_68693451 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- posts-filter -->

<div class="posts-filter">

    <span><?php if ($_smarty_tpl->tpl_vars['_title']->value) {
echo $_smarty_tpl->tpl_vars['_title']->value;
} else {
echo __("Recent Updates");
}?></span>

    <?php if (!$_smarty_tpl->tpl_vars['_filter']->value) {?>

    <div class="pull-right flip">

        <div class="btn-group btn-group-xs js_posts-filter" data-value="all" title='<?php echo __("All");?>
'>

            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">

                <i class="btn-group-icon fa fa-bars fa-fw"></i> <span class="btn-group-text"><?php echo __("All");?>
</span> <span class="caret"></span>

            </button>

            <ul class="dropdown-menu" role="menu">

                <li><a href="#" data-title='<?php echo __("All");?>
' data-value="all"><i class="fa fa-bars fa-fw"></i> <?php echo __("All");?>
</a></li>

                <li><a href="#" data-title='<?php echo __("Text");?>
' data-value=""><i class="fa fa-comment fa-fw"></i> <?php echo __("Text");?>
</a></li>

                <li><a href="#" data-title='<?php echo __("Photos");?>
' data-value="photos"><i class="fa fa-file-image-o fa-fw"></i> <?php echo __("Photos");?>
</a></li>

                <li><a href="#" data-title='<?php echo __("Videos");?>
' data-value="video"><i class="fa fa-film fa-fw"></i> <?php echo __("Videos");?>
</a></li>

                <li><a href="#" data-title='<?php echo __("Audios");?>
' data-value="audio"><i class="fa fa-music fa-fw"></i> <?php echo __("Audios");?>
</a></li>

                <li><a href="#" data-title='<?php echo __("Files");?>
' data-value="file"><i class="fa fa-file-text-o fa-fw"></i> <?php echo __("Files");?>
</a></li>

                <li><a href="#" data-title='<?php echo __("Polls");?>
' data-value="poll"><i class="fa fa-pie-chart fa-fw"></i> <?php echo __("Polls");?>
</a></li>

                <li><a href="#" data-title='<?php echo __("Products");?>
' data-value="product"><i class="fa fa-tag fa-fw"></i> <?php echo __("Products");?>
</a></li>

                <li><a href="#" data-title='<?php echo __("Articles");?>
' data-value="article"><i class="fa fa-file-text fa-fw"></i> <?php echo __("Articles");?>
</a></li>

                <li><a href="#" data-title='<?php echo __("Maps");?>
' data-value="map"><i class="fa fa-map-marker fa-fw"></i> <?php echo __("Maps");?>
</a></li>

            </ul>

        </div>

    </div>

    <?php }?>

</div>

<!-- posts-filter -->



<!-- posts-loader -->

<div class="post x-hidden js_posts_loader">

	<div class="post-body">

		<div class="panel-effect">

			<div class="fake-effect fe-0"></div>

			<div class="fake-effect fe-1"></div>

			<div class="fake-effect fe-2"></div>

			<div class="fake-effect fe-3"></div>

			<div class="fake-effect fe-4"></div>

			<div class="fake-effect fe-5"></div>

			<div class="fake-effect fe-6"></div>

			<div class="fake-effect fe-7"></div>

			<div class="fake-effect fe-8"></div>

			<div class="fake-effect fe-9"></div>

			<div class="fake-effect fe-10"></div>

			<div class="fake-effect fe-11"></div>

		</div>

	</div>

</div>

<!-- posts-loader -->



<ul class="ProfileHeading-toggle">

			<?php if (count($_smarty_tpl->tpl_vars['count_posts']->value) > 0) {?>

        	<li class="ProfileHeading-toggleItem  u-textUserColor" data-element-term="photos_and_videos_toggle">

            <a class="ProfileHeading-toggleLink js-nav" href="/profile.php?username=<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_name'];?>
" data-nav="tweets_with_replies_toggle">

                <?php echo __("Posts");?>


            </a>

            </li>

            <?php }?>

            

            <?php if (count($_smarty_tpl->tpl_vars['posts']->value) > 0) {?>

            <li class="ProfileHeading-toggleItem" data-element-term="tweets_with_replies_toggle">
                <a class="ProfileHeading-toggleLink js-nav" href="/likes.php?username=<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_name'];?>
" data-nav="tweets_with_replies_toggle">
                 <span aria-hidden="true"> <?php echo __("Likes & Comments");?>
</span>

            </li>

            <?php }?>

            

            <?php if (count($_smarty_tpl->tpl_vars['count_media_posts']->value) > 0) {?>

            <li class="ProfileHeading-toggleItem  u-textUserColor" data-element-term="photos_and_videos_toggle">

                <a class="ProfileHeading-toggleLink js-nav" href="/media.php?username=<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_name'];?>
" data-nav="photos_and_videos_toggle">

                  <?php echo __("Media");?>


                </a>

            </li>

            <?php }?>

            <?php if ($_smarty_tpl->tpl_vars['profile']->value['broadcast_count'] > 0) {?>

            <li class="ProfileHeading-toggleItem  u-textUserColor is-active" data-element-term="photos_and_videos_toggle">

                <a class="ProfileHeading-toggleLink js-nav" href="/post_broadcast.php?username=<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_name'];?>
" data-nav="photos_and_videos_toggle">

                  <?php echo __("Broadcasts");?>


                </a>

            </li>

            <?php }?>   

        </ul>



<?php if (count($_smarty_tpl->tpl_vars['posts']->value) > 0) {?>

	<div class="js_posts_stream" data-get="<?php echo $_smarty_tpl->tpl_vars['_get']->value;?>
" data-filter="<?php if ($_smarty_tpl->tpl_vars['_filter']->value) {
echo $_smarty_tpl->tpl_vars['_filter']->value;
} else { ?>all<?php }?>" <?php if ($_smarty_tpl->tpl_vars['_id']->value) {?>data-id="<?php echo $_smarty_tpl->tpl_vars['_id']->value;?>
"<?php }?>>

		<ul>

			<!-- posts -->

			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['posts']->value, 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>

			<?php $_smarty_tpl->_subTemplateRender('file:__broadcast_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>$_smarty_tpl->tpl_vars['_get']->value), 0, true);
?>


			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


			<!-- posts -->

		</ul>



	</div>

<?php } else { ?>

	<div class="js_posts_stream" data-get="<?php echo $_smarty_tpl->tpl_vars['_get']->value;?>
" data-filter="<?php if ($_smarty_tpl->tpl_vars['_filter']->value) {
echo $_smarty_tpl->tpl_vars['_filter']->value;
} else { ?>all<?php }?>" <?php if ($_smarty_tpl->tpl_vars['_id']->value) {?>data-id="<?php echo $_smarty_tpl->tpl_vars['_id']->value;?>
"<?php }?>>

		<ul>

			<!-- no posts -->

			<div class="text-center x-muted">

				<i class="fa fa-newspaper-o fa-4x"></i>

				<p class="mb10"><strong><?php echo __("No posts to show");?>
</strong></p>

			</div>

			<!-- no posts -->

		</ul>

	</div>

<?php }
}
}
