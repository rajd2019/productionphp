<?php
/* Smarty version 3.1.31, created on 2018-09-07 13:30:51
  from "/var/app/current/content/themes/default/templates/take-video.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b927d8b379595_75272107',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c5c835f571cff6bb47791842226c08d6464eceda' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/take-video.tpl',
      1 => 1536327040,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b927d8b379595_75272107 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style>
.modal-dialog div#container {
    max-width: none;
}
.modal-dialog video {
	width: calc(50% - 30px) !important;
    margin: 0 15px;
    display: inline-block;
    float: left;
    height: calc(100vh - 20vh - 127px) !important;
	background: #000;
}
.modal-dialog .modal-content-modal3 {
    height: 100%;
    overflow-y: auto;
}
.modal-dialog button#record, .modal-dialog button#play, .modal-dialog button#download, .modal-dialog button.popup-close {
    color: #fff;
    font-size: 12px;
    width: auto;
    padding: 7px 14px;
    border-radius: 4px;
	margin: 17px 0 0 7px;
	background-color: #337ab7;
    border-color: #2e6da4;
	font-size: 14px;
    font-weight: bold;
    line-height: 20px;
    padding: 6px 16px;
    border-radius: 20px;
	padding: 8px 16px;
	border-radius: 20px;
	box-shadow: none;
	border: 0;
	float:left;
}
.modal-dialog h1 {
    border-bottom: solid 1px;
    padding: 0 8px 7px;
    margin: 11px 15px 19px;
}
.modal-dialog button#record {
    margin-left:15px;
}
/* progress bar */
#progress-wrp {
    border: 1px solid #0099CC;
    padding: 1px;
    position: relative;
    border-radius: 3px;
    margin: 10px;
    text-align: left;
    background: #fff;
    box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
	float: left;
	width: calc(100% - 458px);
	margin: 22px 0 0 33px;
}
#progress-wrp .progress-bar{
	height: 20px;
    border-radius: 3px;
    background-color: #f39ac7;
    width: 0;
    box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
}
#progress-wrp .status{
	top:3px;
	left:50%;
	position:static;
	display:inline-block;
	color: #fff;
}
.mobile-bar{
		display:none
	}
	.desktop-bar{
		display:block
	}
@media (max-width: 768px){
	.modal-dialog h1{
		font-size: 30px;
	}
	.modal-dialog button#record, .modal-dialog button#play, .modal-dialog button#download, .modal-dialog button.popup-close {
		color: #fff;
		width: auto;
		margin: 0;
		font-size: 12px;
		font-weight: bold;
		line-height: 20px;
		padding: 8px 15px;
		border:0;
		border-right: solid 1px #fff;
		-moz-border-radius: 0;
		-webkit-border-radius: 0;
		border-radius: 0;
		white-space: nowrap;
	}
	.button-div{
		display:flex;
		margin:0 15px 15px;
		border-radius: 5px;
		overflow: hidden;
		padding: 0 0 0;
		position: relative;
	}
	.select-style {
		float: none;
		margin: 3px 15px 12px;
		width: calc(100% - 30px);
		
	}
	.modal-dialog button.popup-close {
		border: 0 !important;
	}
	#progress-wrp {
		width: calc(100%);
		margin: 22px 0 0 33px;
		margin: 0;
	}
	.mobile-bar{
		display:block
	}
	.desktop-bar{
		display:none
	}
}
@media (max-width: 500px){
	.modal-dialog h1{
		font-size: 20px;
	}
}
@media (max-width: 767px) and (min-height: 480px){
	.modal-dialog {
		width: 100vw !important;
		height: 100vh !important;
	}
	.modal-dialog video{
		width: calc(100% - 30px) !important;
		margin: 0 15px 10px;
		float: none;
		height: calc(43vh - 50px) !important;
	}
	
}
@media (max-width: 800px) and (min-height: 1200px){
	.modal-dialog {
		width: 100vw !important;
		height: 100vh !important;
	}
	.modal-dialog video{
		width: 100% !important;
		margin: 0 0 10px;
		float: none;
		height: calc(43vh - 50px) !important;
	}
	
}


</style>
<!--link rel="stylesheet" href="/includes/assets/js/plugins/webRtc/src/css/main.css"-->
<!--link rel="stylesheet" href="/includes/assets/js/plugins/webRtc/src/content/getusermedia/record/css/main.css"-->
<div id="container">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  <span aria-hidden="true">&times;</span>
	</button>
    <h1>Take video</h1>

    <!--p>For more information see the MediaStream Recording API <a
            href="http://w3c.github.io/mediacapture-record/MediaRecorder.html"
            title="W3C MediaStream Recording API Editor's Draft">Editor's&nbsp;Draft</a>.</p-->

    <video id="gum" playsinline autoplay muted></video>
    <video id="recorded" playsinline loop controls></video>
	<div class="clearfix"></div>
	<div id="progress-wrp" class="mobile-bar" style="display:none;"><div class="progress-bar"><div class="status">0%</div></div ></div>
    <div class="button-div">
        <button id="record" disabled style="background-color:green;">Start Recording</button>
        <button id="play" disabled>Play</button>
        <button id="download" disabled>Upload</button>
		<button type="button" class="btn btn-secondary popup-close" data-dismiss="modal">Close</button>
		
		<div id="progress-wrp" class="desktop-bar" style="display:none;"><div class="progress-bar"><div class="status">0%</div></div ></div>
    </div>
	
    <!--a href="https://github.com/webrtc/samples/tree/gh-pages/src/content/getusermedia/record"
       title="View source for this page on GitHub" id="viewSource">View source on GitHub</a-->

</div>

<!-- include adapter for srcObject shim -->
<?php echo '<script'; ?>
 src="/includes/assets/js/plugins/webRtc/adapter-latest.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/includes/assets/js/plugins/webRtc/src/content/getusermedia/record/js/main.js?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
" async><?php echo '</script'; ?>
>
<?php }
}
