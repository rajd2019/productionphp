<?php
/* Smarty version 3.1.31, created on 2018-08-31 14:09:55
  from "/var/app/current/content/themes/default/templates/resetmob.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b894c33f35957_67464338',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8ed1bad5da89f3a46696c33f24875190840b18f8' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/resetmob.tpl',
      1 => 1533887900,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_5b894c33f35957_67464338 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- page content -->
<div class="page-title">
    <?php echo __("Reset Password");?>

</div>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="modal-header">           

            <h5 class="modal-title"><?php echo __("Reset Password Confirmation Code");?>
</h5>

        </div>

        <form class="js_ajax-forms" data-url="core/forget_password_confirm.php">

            <div class="modal-body">

                <div class="mb20">

                    <?php echo __("Check your email");?>
 - <?php echo __("We sent you an email with a six-digit confirmation code. Enter it below to continue to reset your password");?>
.

                </div>

                <div class="row">

                    <div class="col-md-6">

                        <div class="form-group">

                            <input name="reset_key" type="text" class="form-control" placeholder="######" required autofocus>

                        </div>



                        <!-- error -->

                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                        <!-- error -->

                    </div>

                    <!--<div class="col-md-6">

                        <label class="mb0"><?php echo __("We sent your code to");?>
</label> {{email}}

                    </div>-->

                </div>

            </div>

            <div class="modal-footer">

                <input name="email" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['r_email']->value;?>
">

                <button type="submit" class="btn btn-primary"><?php echo __("Continue");?>
</button>

                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __("Cancel");?>
</button>

            </div>

        </form>
        </div>
    </div>
</div>
<!-- page content -->


<?php echo '<script'; ?>
 type="text/javascript">
	$(document).ready(function(e){
				
	});	
<?php echo '</script'; ?>
>


<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
