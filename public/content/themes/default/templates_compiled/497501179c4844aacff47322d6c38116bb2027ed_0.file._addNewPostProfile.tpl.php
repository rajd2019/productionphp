<?php
/* Smarty version 3.1.31, created on 2018-08-31 12:48:57
  from "/var/app/current/content/themes/default/templates/_addNewPostProfile.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b893939b13b07_04471723',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '497501179c4844aacff47322d6c38116bb2027ed' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/_addNewPostProfile.tpl',
      1 => 1531812697,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_emoji-menu.tpl' => 1,
  ),
),false)) {
function content_5b893939b13b07_04471723 (Smarty_Internal_Template $_smarty_tpl) {
?>

<!--Model-->
                                      <!-- post message -->
                                       
<div class="custom_post_social modal fade" role="dialog" id="postsocial">
    <div class="modal-dialog">
        <div class="modal-content-modal2">
            <div class="modal-header">
                <h3 class="modal-title">Compose new Post</h3>
                <button type="button" id="close-modal-popup" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body post_custom post_custom_mobile">
                <div class="max-600">
                    <div class="post-avatar">
                        <a class="post-avatar-picture" href="<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_author_url'];?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_picture'];?>
);"></a>
                    </div>
                    <div class="x-form publisher" data-handle="me_ajax" <?php if ($_smarty_tpl->tpl_vars['_id']->value) {?>data-id="<?php echo $_smarty_tpl->tpl_vars['_id']->value;?>
" <?php }?>>
                            <!-- publisher loader -->
                            <div class="publisher-loader">
                                <div class="loader loader_small"></div>
                            </div>
                            <!-- publisher loader -->
                            <!-- publisher tabs -->
                            <!-- publisher tabs -->
                            <!-- post product -->
                            <div class="publisher-meta top" data-meta="product">
                                <i class="fa fa-tag fa-fw"></i>
                                <input name="name" type="text" placeholder='<?php echo __("What are you selling?");?>
'>
                            </div>
                            <div class="publisher-meta top" data-meta="product">
                                <i class="fa fa-money fa-fw"></i>
                                <input name="price" type="text" placeholder='<?php echo __("Add price");?>
 (<?php echo $_smarty_tpl->tpl_vars['system']->value["system_currency"];?>
)'>
                            </div>
                            <div class="publisher-meta top" data-meta="product">
                                <i class="fa fa-shopping-basket fa-fw"></i>
                                <select name="category_id">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['market_categories']->value, 'category');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['category']->value['category_id'];?>
"><?php echo __($_smarty_tpl->tpl_vars['category']->value['category_name']);?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </select>
                            </div>
                            <div class="publisher-meta top" data-meta="product">
                                <i class="fa fa-map-marker fa-fw"></i>
                                <input name="location" class="js_geocomplete" type="text" placeholder='<?php echo __("Add Location (optional)");?>
'>
                            </div>
                            <!-- post product -->
                            <!-- post message -->
                            <div class="relative focused" id="postBoxDiv">
                                <div class="post_area_custom">
                                    <div class="post-avatar" style="float:left;">
                                        <a class="post-avatar-picture" href="<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_author_url'];?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_picture'];?>
);"></a>
                                    </div>
                                    <textarea dir="auto" style="height:30px;overflow: hidden;" id="postBoxPro" class="js_mention js_publisher-scraper" ></textarea>

                                    <span class="js_emoji-menu-toggle" style="float:right;width: 7%; display:none;" data-toggle="tooltip" data-placement="top" title='<?php echo __("Insert an emoji");?>
'><!--i class="fa fa-smile-o fa-fw"--></i></span><?php $_smarty_tpl->_subTemplateRender('file:_emoji-menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


                                    <span class="text-counter" id="count_char" style="float:right;width: 7%; display:none;"></span> 

                                </div>
                                <!-- publisher scraper -->
                                <div class="publisher-scraper"></div>
                                <!-- publisher scraper -->
                                <!-- post attachments -->
                                <div class="publisher-attachments attachments clearfix x-hidden">
                                    <ul id="sortable"></ul>
                                </div>
                                <!-- post attachments -->
                                <!-- post album -->
                                <div class="publisher-meta" data-meta="album">
                                    <i class="fa fa-picture-o fa-fw"></i>
                                    <input type="text" placeholder='<?php echo __("Album title");?>
'>
                                </div>
                                <!-- post album -->
                                <!-- post poll -->
                                <div class="publisher-meta remove_all_poll" data-meta="poll">
                                    <button type="button" class="btn btn-primary remove_poll"><?php echo __("Remove Poll");?>
</button>
                                </div>
                                <div class="publisher-meta custom_poll" data-meta="poll">
                                    <i class="fa fa-plus fa-fw"></i>
                                    <input type="text" placeholder='<?php echo __("Add an option");?>
...'>
                                </div>
                                <div class="publisher-meta custom_poll" data-meta="poll">
                                    <i class="fa fa-plus fa-fw"></i>
                                    <input type="text" placeholder='<?php echo __("Add an option");?>
...'>
                                </div>
                                <!-- post poll -->
                                <!-- post video -->
                                <div class="publisher-meta" data-meta="video">
                                    <i class="fa fa-video-camera fa-fw"></i> <?php echo __("Video uploaded successfully");?>

                                </div>
                                <!-- post video -->
                                <!-- post audio -->
                                <div class="publisher-meta" data-meta="audio">
                                    <i class="fa fa-music fa-fw"></i> <?php echo __("Audio uploaded successfully");?>

                                </div>
                                <!-- post audio -->
                                <!-- post file -->
                                <div class="publisher-meta" data-meta="file">
                                    <i class="fa fa-file-text-o fa-fw"></i> <?php echo __("File uploaded successfully");?>

                                </div>
                                <!-- post file -->
                                <!-- post location -->
                                <div class="publisher-meta" data-meta="location">
                                    <i class="fa fa-map-marker fa-fw"></i>
                                    <input class="js_geocomplete" type="text" placeholder='<?php echo __("Where are you?");?>
'>
                                </div>
                                <!-- post location -->
                                <!-- post feelings -->
                                <div class="publisher-meta feelings" data-meta="feelings">
                                    <div id="feelings-menu-toggle" data-init-text='<?php echo __("What are you doing?");?>
'><?php echo __("What are you doing?");?>
</div>
                                    <div id="feelings-data" style="display: none">
                                        <input type="text" placeholder='<?php echo __("What are you doing?");?>
'>
                                        <span></span>
                                    </div>
                                    <div id="feelings-menu" class="dropdown-menu dropdown-widget">
                                        <div class="dropdown-widget-body ptb5">
                                            <div class="js_scroller">
                                                <ul class="feelings-list">
                                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['feelings']->value, 'feeling');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['feeling']->value) {
?>
                                                    <li class="feeling-item js_feelings-add" data-action="<?php echo $_smarty_tpl->tpl_vars['feeling']->value['action'];?>
" data-placeholder="<?php echo __($_smarty_tpl->tpl_vars['feeling']->value['placeholder']);?>
">
                                                        <div class="icon">
                                                            <i class="twa twa-3x twa-<?php echo $_smarty_tpl->tpl_vars['feeling']->value['icon'];?>
"></i>
                                                        </div>
                                                        <div class="data">
                                                            <?php echo __($_smarty_tpl->tpl_vars['feeling']->value['text']);?>

                                                        </div>
                                                    </li>
                                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="feelings-types" class="dropdown-menu dropdown-widget">
                                        <div class="dropdown-widget-body ptb5">
                                            <div class="js_scroller">
                                                <ul class="feelings-list">
                                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['feelings_types']->value, 'type');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['type']->value) {
?>
                                                    <li class="feeling-item js_feelings-type" data-type="<?php echo $_smarty_tpl->tpl_vars['type']->value['action'];?>
" data-icon="<?php echo $_smarty_tpl->tpl_vars['type']->value['icon'];?>
">
                                                        <div class="icon">
                                                            <i class="twa twa-3x twa-<?php echo $_smarty_tpl->tpl_vars['type']->value['icon'];?>
"></i>
                                                        </div>
                                                        <div class="data">
                                                            <?php echo __($_smarty_tpl->tpl_vars['type']->value['text']);?>

                                                        </div>
                                                    </li>
                                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- post feelings -->
                            </div>
                            <!-- post message -->
                            <!-- publisher-footer -->
            <div class="publisher-footer clearfix" style="display:block;">
                <!-- publisher-tools -->
                <ul class="publisher-tools">
                    <li data-toggle="tooltip" data-placement="top" title='<?php echo __("Create Post");?>
'>
                        <span class="js_publisher-tab" data-tab="post">

                        <i class="fa fa-pencil fa-fw"></i> <span class="hidden-xs"></span>
                        </span>
                    </li>
                    <?php if ($_smarty_tpl->tpl_vars['system']->value['photos_enabled']) {?>
                    <li >
                        <span class="publisher-tools-attach js_publisher-photos newalbumClass" style="float:right;width: 7%; height:30px; padding-top:0px;">

                                <i class="fa fa-camera fa-fw js_x-uploader" data-handle="publisher" data-multiple="true"></i>

                            </span>
                    </li>
                    <?php }?> <?php if ($_smarty_tpl->tpl_vars['system']->value['geolocation_enabled']) {?>
                    
                    <?php }?>
                    
                    <?php if ($_smarty_tpl->tpl_vars['system']->value['market_enabled'] && $_smarty_tpl->tpl_vars['_handle']->value != "group" && $_smarty_tpl->tpl_vars['_handle']->value != "event") {?>
                    <li class="relative" data-toggle="tooltip" data-placement="top" title='<?php echo __("Sell Something");?>
'>
                        <span class="js_publisher-tab" data-tab="product">

                        <i class="fa fa-tag fa-fw"></i>

                    </span>
                    </li>
                    <?php }?> <?php if ($_smarty_tpl->tpl_vars['system']->value['blogs_enabled'] && $_smarty_tpl->tpl_vars['_handle']->value != "group" && $_smarty_tpl->tpl_vars['_handle']->value != "event") {?>
                    
                    <?php }?> <?php if ($_smarty_tpl->tpl_vars['system']->value['videos_enabled']) {?>
                    <li id="video-select" class="relative" data-toggle="tooltip" data-placement="top" title='<?php echo __("Add Video");?>
'>
                        
                    <span class="publisher-tools-attach js_publisher-tab" data-tab="video">

                        <i class="fa fa-video-camera fa-fw js_x-uploader" data-handle="publisher" data-type="video"></i>

                    </span>
                    </li>
                    <?php }?> 
                    
                    <?php if ($_smarty_tpl->tpl_vars['system']->value['file_enabled']) {?>
                        <?php if (($_smarty_tpl->tpl_vars['user']->value->_data['user_name'] == "milesguo" || $_smarty_tpl->tpl_vars['user']->value->_data['user_name'] == "Milesguo")) {?>
                        <li>
                            <span class="publisher-tools-attach js_publisher-tab" data-tab="file">
                                <i class="fa fa-paperclip fa-fw js_x-uploader" data-handle="publisher" data-type="file"></i> 
                            </span>
                        </li>
                        <?php }?>
                    <?php }?>
                    
                    <?php if ($_smarty_tpl->tpl_vars['system']->value['audio_enabled']) {?>
                     <li id="audio-select" class="relative" >

                        <span class="publisher-tools-attach js_publisher-tab" data-tab="audio">

                            <i class="fa fa-music fa-fw js_x-uploader" data-handle="publisher" data-type="audio"></i>

                        </span>

                    </li> 
                    <?php }?>
                    <li data-toggle="tooltip" class="" data-placement="top">
                        <span class="publisher-tools-attach js_publisher-photos newalbumClass picker_profile" style="overflow:visible">
                        <i class="fa fa-smile-o fa-fw"></i>
                    </span>
                    </li>
            
                    <?php echo '<script'; ?>
>
                    
                        $(function() {
                            
                            $('.picker_profile').lsxEmojiPicker({
                                closeOnSelect: true,
                                twemoji: true,
                                onSelect: function(emoji){
                                    
                                    tinymce.activeEditor.execCommand('mceInsertContent', false, emoji.value)
                                }
                            });
                            
                        });
                   
                    <?php echo '</script'; ?>
>
                </ul>
                <!-- publisher-tools -->
                <!-- publisher-buttons -->
                <div class="pull-right flip mt5">
                    <div class="max-600">
                        <a href="javascript:void(0)" id="close-update-popup"><i class="fa fa-times"></i></a>
                        <?php if ($_smarty_tpl->tpl_vars['_privacy']->value) {?>
                        <!-- privacy -->
                        <div class="btn-group js_publisher-privacy" data-toggle="tooltip" data-placement="top" data-value="public" title='<?php echo __("Shared with: Public");?>
'>
                        </div>
                        <!-- privacy -->
                        <?php }?>
                        <span class="text-counter" id="post_count_char_innerpage" style="display:inline-block;"></span>
                        
                        <button type="button" class="btn btn-primary js_publisher"><?php echo __("Post");?>
</button>
                    </div>
                </div>
                <!-- publisher-buttons -->
            </div>
                            <!-- publisher-footer -->
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div> <?php }
}
