<?php
/* Smarty version 3.1.31, created on 2018-08-31 14:23:38
  from "/var/app/current/content/themes/default/templates/search_people.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b894f6a9a0d89_09791216',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '52c86fb69ce62b2af84addee8ce3cfb39fdbf763' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/search_people.tpl',
      1 => 1532937426,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:_ads.tpl' => 1,
    'file:_widget.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_5b894f6a9a0d89_09791216 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- page content -->
<div class="search-result-heading">
    <div class="container">
        <?php echo $_smarty_tpl->tpl_vars['query']->value;?>

    </div>
</div>


<div class="container mt20 offcanvas">
    <div class="row">
    
        
        <div class="col-sm-11 col-md-12 offcanvas-mainbar">
            <div class="row">
                <!-- left panel -->
                <div class="col-sm-12">
                    <!-- search form -->
                    
                    <!-- search form -->


                    <?php if ($_smarty_tpl->tpl_vars['query']->value) {?>
                    <!-- search results -->
                     <div class="panel-body tab-content">
                        <div class="tab-pane <?php if ($_smarty_tpl->tpl_vars['follow']->value == '') {?> active <?php }?>" id="top">
                            
                            <?php if (count($_smarty_tpl->tpl_vars['results']->value['limit_user']) > 0) {?>
                            <div class="custom-view"><p class="mt10 mb10"><strong><?php echo __("People");?>
</strong></p>
                            <?php if (count($_smarty_tpl->tpl_vars['results']->value['users']) > 8) {?>
                            <span class="people-view-all"><a href="#users" data-toggle="tab"><?php echo __("View All");?>
</a></span>
                            <?php }?>
                            </div>
                                
                                <div class="row">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['limit_user'], '_user', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['_user']->value) {
?>
                                    <div class="col-sm-3">
                                     <div class="site_bar_new custom-site-bar-new">
                                         <div class="profile-bg-thumb"> 
                                        <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_cover']) {?>
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_cover'];?>
">
                                        <?php }?>
                                        </div>
                                        <div class="side_profile">
                                        <div class="pro_thumb"> <a href="/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>"><img src="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>
"></a></div>
                                        
                                        <div class="admin_detail">
                                        <span> 
                                        
                                        <?php if ($_smarty_tpl->tpl_vars['results']->value['custom_login_user_id'] != $_smarty_tpl->tpl_vars['_user']->value['user_id']) {?>
                                        
                                        <?php if ($_smarty_tpl->tpl_vars['_user']->value['following'] == 0) {?>
                                            <a class="js_follow follow_btn 1122<?php echo $_smarty_tpl->tpl_vars['_user']->value['following'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" href="#"><?php echo __("Follow");?>
</a>
                                        <?php } else { ?>  
                                            <a class="btn btn-default js_unfollow" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" href="#"><i class="fa fa-check"></i><?php echo __("Following");?>
</a>
                                        <?php }?>
                                        
                                        <?php }?>
                                        
                                        </span>
                                        <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
">
                                            <a href="/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>"><span><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];
echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>
 </span> <span class="user-at-btn">@<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
</span></a>
                                        </span>
                                        
                                        
                                        </div>
                                        <p><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_biography'];?>
</p>
                                        </div>
                                    </div>
                                    </div>
                                        <?php if (($_smarty_tpl->tpl_vars['k']->value+1)%4 == 0) {?>
                                            <div class="clearfix"></div>
                                        <?php }?>
                                 <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </div>

                                <?php }?>

                               
                                
                               
                                
                                
                                
                                
                                
                                
                                
                                
                              
                                
                        </div>


                        

                        <div class="tab-pane active" id="users">
                            
                                
                                <div class="row">
                                <?php if (count($_smarty_tpl->tpl_vars['results']->value['users']) > 8) {?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['users'], '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
                                    <div class="col-sm-3">
                                     <div class="site_bar_new custom-site-bar-new">
                                        <div class="profile-bg-thumb"> 
                                        <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_cover']) {?>
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_cover'];?>
">
                                        <?php }?>
                                        </div>
                                        <div class="side_profile">
                                        <div class="pro_thumb"> <a href="/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>"><img src="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>
"></a></div>
                                        
                                        <div class="admin_detail">
                                        <span> 
                                        <?php if ($_smarty_tpl->tpl_vars['results']->value['custom_login_user_id'] != $_smarty_tpl->tpl_vars['_user']->value['user_id']) {?>
                                        
                                        <?php if ($_smarty_tpl->tpl_vars['_user']->value['following'] == 0) {?>
                                            <a class="js_follow follow_btn 22" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" href="#"><?php echo __("Follow");?>
</a>
                                        <?php } else { ?>  
                                            <a class="btn btn-default js_unfollow" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" href="#"><i class="fa fa-check"></i><?php echo __("Following");?>
</a>
                                        <?php }?>
                                         <?php }?>
                                        
                                        </span>
                                        <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
">
                                            <a href="/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>"><span><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];
echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>
 </span> <span class="user-at-btn">@<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
</span></a>
                                        </span>
                                        
                                        
                                        </div>
                                        <p><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_biography'];?>
</p>
                                        </div>
                                    </div>
                                    </div>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </div>
                                
                                <?php } else { ?>
                               
                                <?php }?>
                        </div>
                            
                            
                           
                            
                           
                    <!-- search results -->
                    <?php }?>
                </div>
                <!-- left panel -->

                <!-- right panel -->
                <div class="col-sm-4">
                    <?php $_smarty_tpl->_subTemplateRender('file:_ads.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:_widget.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>
                <!-- right panel -->
            </div>
        </div>
        

        
    </div>

<!-- page content -->

<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
