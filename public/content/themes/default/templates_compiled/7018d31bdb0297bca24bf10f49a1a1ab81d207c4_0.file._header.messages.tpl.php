<?php
/* Smarty version 3.1.31, created on 2018-08-31 12:48:13
  from "/var/app/current/content/themes/default/templates/_header.messages.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b89390de3ec78_97590302',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7018d31bdb0297bca24bf10f49a1a1ab81d207c4' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/_header.messages.tpl',
      1 => 1535715089,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_header.messages.emoji.tpl' => 1,
  ),
),false)) {
function content_5b89390de3ec78_97590302 (Smarty_Internal_Template $_smarty_tpl) {
?>
<link rel="stylesheet" href="/content/themes/default/templates/static/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="/content/themes/default/templates/static/css/sample-chat.css?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
">
<link href='https://fonts.googleapis.com/css?family=Exo+2:400,900italic,900,800italic,800,700italic,700,600italic,600,500italic,500,400italic,300italic,200italic,200,100italic,100,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Lato:400,900italic,900,800italic,800,700italic,700,600italic,600,500italic,500,400italic,300italic,200italic,200,100italic,100,300' rel='stylesheet' type='text/css'>

<!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] -->
<link rel="stylesheet" href="/includes/assets/css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/includes/assets/css/twemoji-awesome/twemoji-awesome.min.css">
<link rel="stylesheet" href="/includes/assets/css/flag-icon/css/flag-icon.min.css">
<!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] -->
<!-- JS Files -->
<?php echo '<script'; ?>
 src="/includes/assets/js/jquery/jquery-3.2.1.min.js" ><?php echo '</script'; ?>
>
<style type="text/css">
    .DMComposer-send {
        position: absolute;
        right: 15px;
        bottom: 10px;
    }

    .TweetBoxExtras {
        position: absolute;
        right: 90px;
        bottom: 10px;
    }

    .DMComposer-attachment {
		border-bottom: 0px;
		margin: 0 2px;
		padding: 0;
		background: #fff;
		border-bottom:solid 1px #ddd;
    }
	.DMComposer-tweet .stream-item-header img#post_avatar_id {
		border-radius: 50%;
		width: 32px;
		height: 32px;
		margin: 0 5px 4px 0;
	}
    .DMComposer-editor {
        resize: none;
        width: 250px !important;
        height: 40px;
    }
	.DMComposer-tweet .js-tweet-text-container {
		margin: 7px 0 0;
	}
	.DMComposer-tweet .js-tweet-text-container p {
		margin: 0;
	}
	#user_loading_id {
		width: 100%;
		display: table;
		vertical-align: middle;
		position: absolute;
		top: calc(50% - 15px);
	}
	.loader.loader_medium {
		display: table-cell;
		height: 100%;
		vertical-align: middle;
		text-align: center;
	}
	.loader.loader_medium:after {
		width: 25px;
		height: 25px;
		float: none;
		text-align: center;
		display: inline-block;
	}
	.navbar-nav .open.attachment .dropdown-menu {
		width: 55px !important;
		min-width: fit-content !important;
		padding: 6px 5px 0;
		margin: 0;
		/* box-shadow: none !important; */
		/* border: 0; */
		bottom: 31px;
		right: auto;
		left: -19px !important;
		border-top: 1px solid rgba(0,0,0,.15) !important;
	}
	.navbar-nav .open.attachment .TweetBoxExtras-item {
		background: #E8F5FD;
		border-radius: 50%;
		padding: 0;
		border: solid 1px #c1c1c1;
		width: 42px;
		text-align: center;
		margin-bottom: 6px;
	}
	.navbar-nav .open.attachment .DMComposer .icon-btn {
		font-size: 21px;
		line-height: normal;
		padding:0;
		margin:0;
	}
	.DMComposer .icon-btn .Icon {
    font-size: 20px;
	}
	.attachment button.btn.btn-secondary.dropdown-toggle {
		border: 0;
		background: transparent !important;
		box-shadow: none;
		padding: 0;
		margin: 0 4px -2px 0;
		font-size: 25px;
	}
	.DMComposer .icon-btn {
		border: none !important;
		background: none !important;
		padding: 0;
		margin: 0;
		line-height: normal;
	}
	.DMComposer-mediaPicker .photo-selector {
		margin: -3px -1px 0 0 !important;
		padding: 7px 0;
	}
	.DMComposer-gifSearch .photo-selector {
		padding: 8px 0;
	}
	.u-borderUserColorLighter {
		width: calc(100% - 117px);

		border-radius: 4px;
	}
	textarea#custom-editor {
	    width: calc(100% - 32px) !important;
		padding: 0;
		height: auto;
		float: left;
	}
	.DMComposer .RichEditor-scrollContainer {
		max-height: 96px !important;
		padding: 10px 0 10px 10px;
		background:#fff;
	}
	.emoji-button-outer {
		float: right;
		padding: 0px 5px 0 0;
		width:100%;
	}
	.DMComposer .TweetBoxExtras {
		margin: 0 5px 12px;
	}
	.DMComposer-send {
		position: absolute;
		right: 15px;
		bottom: 23px;
	}
	.emoji-button {
		cursor: pointer;
		margin: 7px 5px;
		float: right;
	}
	/*.emoji-picker{
		top: auto !important;
		left: auto !important;
	}*/
	.DMActivity.DMConversationSettings.js-ariaDocument.u-chromeOverflowFix.DMActivity--open {
		width: 100%;
	}
	.js-ariaDocument.u-chromeOverflowFix {
		width: 100%;
	}
	/*.emoji-picker{
		top: 233px !important;
		left: 702.109px !important;
	}*/
	.u-scrollY {
		overflow-y: auto !important;
	}
	.DMComposer-tweet .stream-item-header {
		padding: 11px 11px;
	}
    @media (max-width: 767px) {
      .DMComposer-editor {
        width: auto !important;
      }

      .icon-btn {
        padding: 4px 6px;
      }
	.DMComposer-send {
		position: absolute;
		right: 6px;
		bottom: 23px;
	}
	.DMComposer .TweetBoxExtras {
		margin: 0 0 12px;
	}

	.TweetBoxExtras {
		position: absolute;
		right: 79px;
		bottom: 10px;
	}
	.u-borderUserColorLighter {
		width: calc(100% - 103px);
	}
	.vertical-align-center .modal-content {
		width: 100%;
    }
	.u-chromeOverflowFix{
		border-radius:0;
	}
	.DMActivity-toolbar{
		    -ms-flex-preferred-size: inherit;
		flex-basis: inherit;
	}
	.emoji-picker{
		top: calc(100vh - 380px) !important;
		left: calc(100% - 221px) !important;
	}
	}
    @media (max-width: 320px) {
     .photo-selector {
        right: -26px;
     }

      .photo-selector .icon-btn {
        padding: 4px 2px;
     }

      .DMComposer-send {
        right: 10px;
     }

      .DMComposer-send .EdgeButton {
        padding: 5px 10px;
        border-radius: 10px;
        font-weight: normal;
      }
    }
</style>


<li class="sendbird-chat-cls">
    <input type="hidden" name="accessToken" id="userAccessToken" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['accessToken'];?>
" />
    <input type="hidden" name="userPrivacy" id="userPrivacy" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_message'];?>
" />
    <input type="hidden" name="userNickName" id="userNickName" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_firstname'];?>
" />
    <input type="hidden" name="user-userId" id="user-userId" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['uuid'];?>
" />
    <input type="hidden" name="user-accessToken" id="user-accessToken" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['sendbird_access_token'];?>
" />

    <div class="init-check"></div>
    <div class="sample-body">
        <!-- left nav -->
        <div class="left-nav-channel-select">
            <!-- <button type="button" class="left-nav-button left-nav-open" id="btn_open_chat">
                OPEN CHANNEL
                <div class="left-nav-button-guide"></div>
            </button> -->
            <!-- button type="button" class="left-nav-button left-nav-messaging" id="btn_messaging_chat">
                Messages
                <div class="left-nav-button-guide"></div>
            </button> -->
        </div>

        <div class="left-nav-channel-section">
            <!-- <div class="left-nav-channel-title">OPEN CHAT</div>
            <div class="left-nav-channel-empty">Get started to select<br>a channel</div>
            <div id="open_channel_list"></div> -->
            <!-- <div class="left-nav-channel-title title-messaging"  data-toggle="modal" data-target="#myModal">GROUP CHAT</div>
            </div> -->
            <div class="left-nav-user" style="display: none;">
                <div class="left-nav-user left-nav-user-icon"></div>
                <div class="left-nav-user left-nav-login-user">
                    <div class="left-nav-user left-nav-user-title"><?php echo __("Username");?>
</div>
                    <div class="left-nav-user left-nav-user-nickname"></div>
                </div>
            </div>

            <!-- chat section -->
            <div id="chatModal" class="modal fade" role="dialog">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-align-center">
                        <div class="modal-content DMConversation">
                            <div class="DMActivity-header">
                                <div class="DMActivity-navigation">
                                    <button type="button" class="DMActivity-back u-textUserColorHover transparent-no-border-button" onclick="DMConversation_backToMainModal()">
                                        <span class="Icon Icon--caretLeft u-linkComplex-target Icon--medium"></span>
                                        <span class="u-hiddenVisually"><?php echo __("Back to inbox");?>
</span>
                                    </button>
                                </div>
                                <h2 class="DMActivity-title js-ariaTitle" id="dm_dialog-header">
                                    <div class="DMUpdateAvatar has-defaultAvatar" aria-haspopup="true" data-has-custom-avatar="true">
                                        <div class="DMPopover DMPopover--center">
                                            <button class="DMPopover-button transparent-no-border-button" aria-haspopup="true">
                                                <span class="u-hiddenVisually"><?php echo __("Update group photo.");?>
</span>
                                                <div class="DMUpdateAvatar-avatar">
                                                    <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">
                                                        <span class="DMAvatar-container">
                                                            <img class="DMAvatar-image" src="https://abs.twimg.com/sticky/default_profile_images/default_profile_200x200.png" alt="Stefan Stoica" title="Stefan Stoica">
                                                        </span>
                                                    </div>
                                                </div>
                                            </button>
                                            <div class="DMPopover-content Caret Caret--top Caret--stroked">
                                                <ul class="DMPopoverMenu u-textCenter js-focus-on-open u-dropdownUserColor" tabindex="-1" role="menu">
                                                    <li class="DMUpdateAvatar-view">
                                                        <button type="button" class="DMPopoverMenu-button"><?php echo __("View photo");?>
</button>
                                                    </li>
                                                    <li class="DMUpdateAvatar-change">
                                                        <button type="button" class="DMPopoverMenu-button"><?php echo __("Upload photo");?>
</button>
                                                    </li>
                                                    <li class="DMUpdateAvatar-remove">
                                                        <button type="button" class="DMPopoverMenu-button"><?php echo __("Remove");?>
</button>
                                                    </li>
                                                </ul>
                                                <div class="DMUpdateAvatar-photoSelector photo-selector" tabindex="-1" aria-hidden="true">
                                                    <div class="image-selector">
                                                        <input type="hidden" name="media_file_name" class="file-name">
                                                        <input type="hidden" name="media-data-empty" class="file-data">
                                                        <label class="t1-label">
                                                            <span class="u-hiddenVisually"><?php echo __("Add Photo");?>
</span>
                                                            <input type="file" name="media[]" class="file-input js-tooltip" accept="image/*" tabindex="-1" title="Add Photo">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="DMUpdateName u-textTruncate">
                                        <div class="DMUpdateName-header account-group">
                                            <span class="DMUpdateName-name u-textTruncate edit-allowed js-tooltip" data-original-title="Click to edit group name" title="Click to edit group name">
                                                <?php echo __("Chat Channel");?>

                                            </span>
                                            <span class="UserBadges">
                                                <span class="Icon Icon--verified js-verified hidden">
                                                    <span class="u-hiddenVisually"><?php echo __("Verified account");?>
</span>
                                                </span>
                                                <span class="Icon Icon--protected js-protected hidden">
                                                    <span class="u-hiddenVisually"><?php echo __("Protected Tweets");?>
</span>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="DMUpdateName-screenName u-textTruncate"></div>
                                        <div class="DMUpdateName-controls">
                                            <span class="DMUpdateName-spinner DMSpinner u-hidden"></span>
                                            <div class="DMUpdateName-form input-group is-invalid u-hidden">
                                                <input type="text" class="DMUpdateName-input" aria-label="Edit group name">
                                                <button class="DMUpdateName-confirm u-textUserColorLight">
                                                    <span class="Icon Icon--check"></span>
                                                    <span class="u-hiddenVisually"><?php echo __("Save group name");?>
</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </h2>
                                <div class="DMActivity-toolbar">
                                    <button class="DMConversation-convoSettings dm-to-convoSettings u-textUserColorHover transparent-no-border-button">
                                        <span class="Icon Icon--info Icon--medium"></span>
                                        <span class="u-hiddenVisually"><?php echo __("Conversation Settings");?>
</span>
                                    </button>
                                    <button type="button" class="DMActivity-close u-textUserColorHover close_chat transparent-no-border-button" style="position: static !important;">
                                        <a href="javascript:DMConversation_closeLiveChatDiag();" class="custom-button-delete">
                                            <span class="Icon Icon--close Icon--medium"></span>
                                            <span class="u-hiddenVisually"><?php echo __("Close");?>
</span>
                                        </a>
                                    </button>
                                </div>
                            </div>
                            <div class="DMActivity-container">
                                <div class="DMActivity-notice">
                                    <div class="DMNotice DMNotice--error DMErrorBar" style="display: none;"></div>
                                    <div class="DMNotice DMNotice--toast" style="display: none;"></div>
                                    <div class="DMNotice DMDeleteMessage" style="display: none;">
                                        <div class="DMNotice-message"><?php echo __("Are you sure you want to delete this message?");?>
</div>
                                        <div class="DMNotice-actions u-emptyHide">
                                            <button type="button" class="DMDeleteMessage-cancel EdgeButton EdgeButton--tertiary"><?php echo __("Cancel");?>
</button>
                                            <button type="button" class="DMDeleteMessage-confirm EdgeButton EdgeButton--danger js-initial-focus"><?php echo __("Delete");?>
</button>
                                        </div>
                                    </div>
                                    <div class="DMNotice DMReportMessage" style="display: none;"></div>
                                    <div class="DMNotice DMResendMessage DMNotice--error" style="display: none;"></div>
                                </div>
                                <div class="DMActivity-body js-ariaBody DMConversation-container">
                                    <div class="DMConversation-newMessagesPillContainer u-table">
                                        <div class="DMConversation-newMessagesPill is-hidden">
                                            <span class="Icon Icon--arrowDown"></span>
                                            <?php echo __("New messages");?>

                                        </div>
                                    </div>
                                    <div class="DMConversation-scrollContainer js-modal-scrollable" aria-atomic="false" role="log" aria-live="polite">
                                        <span class="DMConversation-spinner DMSpinner u-hidden"></span>
                                        <ol class="DMConversation-content dm-convo js-dm-conversation" data-thread-id="">
                                            <li class="DirectMessage DirectMessage--sent clearfix dm js-dm-item">
                                                <div class="DirectMessage-container">
                                                    <div class="DirectMessage-avatar">ab</div>
                                                    <div class="DirectMessage-message with-text dm-message u-chromeOverflowFix">
                                                        <div class="DirectMessage-contentContainer">abc</div>
                                                    </div>
                                                </div>
                                                <div class="DirectMessage-footer">
                                                    <div class="DirectMessage-footerItem">
                                                        <div class="DMReadReceipt">
                                                            <div class="DMReadReceipt-statusMessage">1h</div>
                                                            <span class="DMReadReceipt-check">adsf</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="DirectMessage-footer"></div>
                                            </li>
                                        </ol>
                                        <div class="DMConversation-typingIndicator u-hidden">
                                            <div class="DMTypingIndicator">
                                                <div class="DMTypingIndicator-avatarsContainer"></div>
                                                <div class="DMTypingIndicator-messageBubble">
                                                    <div class="TypingIndicatorMessageBubble is-hidden Caret Caret--left">
                                                        <div class="TypingIndicatorMessageBubble-dotContainer">
                                                            <div class="TypingIndicatorMessageBubble-dot"></div>
                                                            <div class="TypingIndicatorMessageBubble-dot"></div>
                                                            <div class="TypingIndicatorMessageBubble-dot"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="DMActivity-footer u-emptyHide">
                                    <div class="DMConversation-sendingStateIndicator u-bgUserColorLightest u-textUserColorLight" style="display: none">
                                        <div class="DMSendingStateIndicator">
                                            <span><?php echo __("Sending message 1 of 1");?>
</span>
                                            <span>...</span>
                                        </div>
                                    </div>
                                    <div class="DMConversation-blankmsgStateIndicator u-bgUserColorLightest u-textUserColorLight" style="display: none">
                                        <div class="DMSendingStateIndicator">

                                        </div>
                                    </div>
                                    <div class="DMConversation-trustRequest u-hidden"></div>
                                    <div class="DMConversation-composer u-bgUserColorLightest">
                                      <div class="loader loader_medium x-hidden" id="showSlowLoaderPost" style="display:none;text-align:center"></div>
                                        <form class="DMComposer tweet-form">
                                            <div class="u-borderUserColorLighter">

                                                <div class="DMComposer-attachment" >
                                                  <!-- <button class="DMComposer-discardAttachment">
                                                    <span class="Icon Icon--close"></span>
                                                  </button> -->
                                                  <button type="button" class="close" id="DMComposer-discardAttachment" >X</button>
                                                    <div class="DMComposer-tweet" id="org_post_detail" org-post-id="">

                                                        <div class="modal-tweet">
                                                          <div class="tweet js-stream-tweet js-actionable-tweet js-profile-popup-actionable dismissible-content
                                                                 original-tweet js-original-tweet">

                                                              <div class="context">


                                                              </div>

                                                              <div class="content">

                                                                <div class="stream-item-header">
                                                                    <a class="account-group js-account-group js-action-profile js-user-profile-link js-nav" href="" data-user-id="" tabindex="-1" id="post_author_url_id">
                                                                       <img class="avatar js-action-profile-avatar" src="" alt="" id="post_avatar_id" width="40" height="40">
                                                                      <!-- <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">
                                                                        <span class="DMAvatar-container">
                                                                          <img class="DMAvatar-image" src="" alt="" title="" id="post_author_name_id">
                                                                       </span>
                                                                     </div> -->
                                                                      <span class="FullNameGroup">
                                                                        <strong class="fullname show-popup-with-id u-textTruncate" id="post_author_name_id" data-aria-label-part=""></strong>
                                                                        <span></span>
                                                                      <span class="UserBadges">
                                                                        <span class="Icon Icon--verified">
                                                                          <span class="u-hiddenVisually">Verified account</span>
                                                                        </span>
                                                                      </span>
                                                                      <span class="UserNameBreak">&nbsp;</span>
                                                                      </span>
                                                                      <span class="username u-dir u-textTruncate" dir="ltr" data-aria-label-part="" id="post_author_username_id">

                                                                      </span>
                                                                    </a>

                                                                  <!-- <small class="time">
                                                                    <a href="#" class="tweet-timestamp js-permalink js-nav js-tooltip" data-conversation-id="" tabindex="-1" data-original-title="5:50 PM - 21 Aug 2018">
                                                                      <span class="_timestamp js-short-timestamp js-relative-timestamp" data-time="1534899020" data-time-ms="1534899020000" data-long-form="true" aria-hidden="true">6h</span><span class="u-hiddenVisually" data-aria-label-part="last">6 hours ago</span></a>
                                                                  </small> -->

                                                                <!-- <div class="ProfileTweet-action ProfileTweet-action--more js-more-ProfileTweet-actions">
                                                                <div class="dropdown">
                                                                <button class="ProfileTweet-actionButton u-textUserColorHover dropdown-toggle js-dropdown-toggle" type="button">
                                                                  <div class="IconContainer js-tooltip" title="More">
                                                                    <span class="Icon Icon--caretDownLight Icon--small"></span>
                                                                    <span class="u-hiddenVisually">More</span>
                                                                  </div>
                                                                </button>
                                                                <div class="dropdown-menu is-autoCentered">
                                                                  <div class="dropdown-caret">
                                                                    <div class="caret-outer"></div>
                                                                    <div class="caret-inner"></div>
                                                                  </div>
                                                                  <ul>
                                                                    <li class="copy-link-to-tweet js-actionCopyLinkToTweet">
                                                                      <button type="button" class="dropdown-link">Copy link to Tweet</button>
                                                                    </li>
                                                                    <li class="embed-link js-actionEmbedTweet" data-nav="embed_tweet">
                                                                      <button type="button" class="dropdown-link">Embed Tweet</button>
                                                                    </li>
                                                                    <li class="mute-user-item"><button type="button" class="dropdown-link">Mute <span class="username u-dir u-textTruncate" dir="ltr">@<b>arrahman</b></span></button></li>
                                                                    <li class="unmute-user-item"><button type="button" class="dropdown-link">Unmute <span class="username u-dir u-textTruncate" dir="ltr">@<b>arrahman</b></span></button></li>

                                                                    <li class="block-link js-actionBlock" data-nav="block">
                                                                    <button type="button" class="dropdown-link">Block <span class="username u-dir u-textTruncate" dir="ltr">@<b>arrahman</b></span></button>
                                                                    </li>
                                                                    <li class="unblock-link js-actionUnblock" data-nav="unblock">
                                                                    <button type="button" class="dropdown-link">Unblock <span class="username u-dir u-textTruncate" dir="ltr">@<b>arrahman</b></span></button>
                                                                    </li>
                                                                    <li class="report-link js-actionReport" data-nav="report">
                                                                      <button type="button" class="dropdown-link">Report Tweet</button>
                                                                    </li>
                                                                    <li class="dropdown-divider"></li>
                                                                    <li class="js-actionMomentMakerAddTweetToOtherMoment MomentMakerAddTweetToOtherMoment">
                                                                      <button type="button" class="dropdown-link">Add to other Moment</button>
                                                                    </li>
                                                                    <li class="js-actionMomentMakerCreateMoment">
                                                                      <button type="button" class="dropdown-link">Add to new Moment</button>
                                                                    </li>
                                                                  </ul>
                                                             </div>
                                                          </div>
                                                          </div> -->





                                                          <div class="stream-item-footer">
                                                            <div class="ProfileTweet-actionCountList u-hiddenVisually">
                                                              <span class="ProfileTweet-action--reply u-hiddenVisually">
                                                                <span class="ProfileTweet-actionCount" data-tweet-stat-count="51">
                                                                  <span class="ProfileTweet-actionCountForAria" id="profile-tweet-action-reply-count-aria-1032067409866354688" data-aria-label-part="">51 replies</span>
                                                                </span>
                                                              </span>
                                                              <span class="ProfileTweet-action--retweet u-hiddenVisually">
                                                                <span class="ProfileTweet-actionCount" data-tweet-stat-count="174">
                                                                  <span class="ProfileTweet-actionCountForAria" id="profile-tweet-action-retweet-count-aria-1032067409866354688" data-aria-label-part="">174 retweets</span>
                                                                </span>
                                                              </span>
                                                              <span class="ProfileTweet-action--favorite u-hiddenVisually">
                                                                <span class="ProfileTweet-actionCount" data-tweet-stat-count="2460">
                                                                  <span class="ProfileTweet-actionCountForAria" id="profile-tweet-action-favorite-count-aria-1032067409866354688" data-aria-label-part="">2,460 likes</span>
                                                                </span>
                                                              </span>
                                                          </div>

                                                          <!-- <div class="ProfileTweet-actionList js-actions" role="group" aria-label="Tweet actions">
                                                              <div class="ProfileTweet-action ProfileTweet-action--reply">
                                                            <button class="ProfileTweet-actionButton js-actionButton js-actionReply" data-modal="ProfileTweet-reply" type="button" aria-describedby="profile-tweet-action-reply-count-aria-1032067409866354688">
                                                              <div class="IconContainer js-tooltip" title="Reply">
                                                                <span class="Icon Icon--medium Icon--reply"></span>
                                                                <span class="u-hiddenVisually">Reply</span>
                                                              </div>
                                                                <span class="ProfileTweet-actionCount" data-tweet-stat-count="51">
                                                                  <span class="ProfileTweet-actionCountForPresentation" aria-hidden="true">51</span>
                                                                </span>
                                                            </button>
                                                          </div>

                                                          <div class="ProfileTweet-action ProfileTweet-action--retweet js-toggleState js-toggleRt">
                                                            <button class="ProfileTweet-actionButton  js-actionButton js-actionRetweet" data-modal="ProfileTweet-retweet" type="button" aria-describedby="profile-tweet-action-retweet-count-aria-1032067409866354688">
                                                              <div class="IconContainer js-tooltip" title="Retweet">
                                                                <span class="Icon Icon--medium Icon--retweet"></span>
                                                                <span class="u-hiddenVisually">Retweet</span>
                                                              </div>
                                                                <span class="ProfileTweet-actionCount" data-tweet-stat-count="174">
                                                              <span class="ProfileTweet-actionCountForPresentation" aria-hidden="true">174</span>
                                                            </span>

                                                            </button><button class="ProfileTweet-actionButtonUndo js-actionButton js-actionRetweet" data-modal="ProfileTweet-retweet" type="button">
                                                              <div class="IconContainer js-tooltip" title="Undo retweet">
                                                                <span class="Icon Icon--medium Icon--retweet"></span>
                                                                <span class="u-hiddenVisually">Retweeted</span>
                                                              </div>
                                                                <span class="ProfileTweet-actionCount" data-tweet-stat-count="174">
                                                              <span class="ProfileTweet-actionCountForPresentation" aria-hidden="true">174</span>
                                                            </span>

                                                            </button>
                                                          </div>

                                                          <div class="ProfileTweet-action ProfileTweet-action--favorite js-toggleState">
                                                            <button class="ProfileTweet-actionButton js-actionButton js-actionFavorite" type="button" aria-describedby="profile-tweet-action-favorite-count-aria-1032067409866354688">
                                                              <div class="IconContainer js-tooltip" title="Like">
                                                                <span role="presentation" class="Icon Icon--heart Icon--medium"></span>
                                                                <div class="HeartAnimation"></div>
                                                                <span class="u-hiddenVisually">Like</span>
                                                              </div>
                                                                <span class="ProfileTweet-actionCount" data-tweet-stat-count="2460">
                                                              <span class="ProfileTweet-actionCountForPresentation" aria-hidden="true">2.5K</span>
                                                            </span>

                                                            </button><button class="ProfileTweet-actionButtonUndo ProfileTweet-action--unfavorite u-linkClean js-actionButton js-actionFavorite" type="button">
                                                              <div class="IconContainer js-tooltip" title="Undo like">
                                                                <span role="presentation" class="Icon Icon--heart Icon--medium"></span>
                                                                <div class="HeartAnimation"></div>
                                                                <span class="u-hiddenVisually">Liked</span>
                                                              </div>
                                                                <span class="ProfileTweet-actionCount" data-tweet-stat-count="2460">
                                                              <span class="ProfileTweet-actionCountForPresentation" aria-hidden="true">2.5K</span>
                                                            </span>

                                                            </button>
                                                          </div>


                                                          <div class="ProfileTweet-action ProfileTweet-action--dm">
                                                              <button class="ProfileTweet-actionButton u-textUserColorHover js-actionButton js-actionShareViaDM" type="button" data-nav="share_tweet_dm">
                                                                <div class="IconContainer js-tooltip" title="Direct message">
                                                                  <span class="Icon Icon--medium Icon--dm"></span>
                                                                  <span class="u-hiddenVisually">Direct message</span>
                                                                </div>
                                                              </button>
                                                          </div>
                                                          </div> -->
														  <div class="js-tweet-text-container">
																<p class="TweetTextSize  js-tweet-text tweet-text" data-aria-label-part="0" lang="en" id="post_content_id">

																</p>
															  </div>
                                                          </div>
                                                          </div>
                                                          </div>
                                                          </div>
                                                        </div>
                                                        <button class="DMComposer-discardTweet">
                                                            <span class="Icon Icon--close"></span>
                                                            <span class="u-hiddenVisually"><?php echo __("Remove tweet attachment");?>
</span>
                                                        </button>
                                                    </div>
                                                    <div class="DMComposer-media">
                                                        <div class="thumbnail-container">
                                                            <div class="thumbnail-wrapper">
                                                                <div class="ComposerThumbnails"></div>
                                                                <div class="js-attribution attribution"></div>
                                                                <div class="ComposerVideoInfo u-hidden"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ComposerDragHelp"></div>
                                                <div class="RichEditor RichEditor--emojiPicker is-fakeFocus">
                                                    <div class="RichEditor-mozillaCursorWorkaround">&nbsp;</div>
                                                    <div class="RichEditor-container u-borderRadiusInherit">
                                                        <div class="RichEditor-scrollContainer u-borderRadiusInherit">
                                                            <div data-emojiarea data-type="unicode" data-global-picker="false">
																	<div class="emoji-button" style="float: right;position: relative;right: 21px;;height: 0px;top: 8px;">&#x1f604;</div>
																		<textarea class="DMComposer-editor tweet-box rich-editor js-initial-focus is-showPlaceholder" id="custom-editor" style="box-shadow: none;">
																		</textarea>
																</div>

                                                            <div class="RichEditor-pictographs" aria-hidden="true"></div>
                                                        </div>
                                                        <!--<div class="RichEditor-rightItems RichEditor-bottomItems" style="left: 140px;top: 464px;">
                                                            <div class="EmojiPicker EmojiPicker--top">
                                                                <button type="button" class="EmojiPicker-trigger js-dropdown-toggle js-tooltip u-textUserCoverHover transparent-no-border-button" data-delay="150" aria-haspopup="true" data-original-title="Add emoji">
                                                                    <span class="Icon Icon--smiley"></span>
                                                                    <span class="text u-hiddenVisually"><?php echo __("Add emoji");?>
</span>
                                                                </button>
                                                                <?php $_smarty_tpl->_subTemplateRender('file:_header.messages.emoji.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                                            </div>
                                                        </div>-->
                                                    </div>
                                                    <div class="RichEditor-mozillaCursorWorkaround">&nbsp;</div>
                                                </div>
                                            </div>
                                            <div class="TweetBoxExtras">
                                                <!-- Default dropup button -->
												<div class="btn-group dropup attachment">
												  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<i class="fa fa-paperclip" aria-hidden="true"></i>
												  </button>
												  <div class="dropdown-menu">
													<div class="DMComposer-gifSearch TweetBoxExtras-item">
														<div class="photo-selector">
															<button aria-hidden="true" class="btn icon-btn js-tooltip" type="button" tabindex="-1" data-original-title="Add photo or video">
																<span class="tweet-camera Icon">&#x1F4F9;</span>
																<span class="text add-photo-label u-hiddenVisually"><?php echo __("Add photo or video");?>
</span>
															</button>
															<div class="image-selector">
																<input type="hidden" name="media_data_empty" class="file-data">
																<div class="multi-photo-data-container hidden"></div>
																<label class="t1-label">
																	<span class="visuallyhidden"><?php echo __("Add photo or video");?>
</span>
																	<input type="file" name="media_empty" accept="video/mp4, video/x-m4v" multiple class="file-input js-tooltip" data-original-title="Add photo or video" data-delay="150">
																</label>
															</div>
														</div>
													</div>
													<div class="DMComposer-mediaPicker TweetBoxExtras-item">
														<div class="photo-selector">
															<button aria-hidden="true" class="btn icon-btn js-tooltip" type="button" tabindex="-1" data-original-title="Add photo or video">
																<span class="tweet-camera Icon Icon--media"></span>
																<span class="text add-photo-label u-hiddenVisually"><?php echo __("Add photo or video");?>
</span>
															</button>
															<div class="image-selector">
																<input type="hidden" name="media_data_empty" class="file-data">
																<div class="multi-photo-data-container hidden"></div>
																<label class="t1-label">
																	<span class="visuallyhidden"><?php echo __("Add photo or video");?>
</span>
																	<input type="file" name="media_empty" accept="image/gif, image/jpeg, image/jpg, image/png" multiple class="file-input js-tooltip" data-original-title="Add photo or video" data-delay="150">
																</label>
															</div>
														</div>
													</div>
												  </div>
												</div>
                                                <div class="DMComposer-quickReplyDismiss"></div>
                                            </div>
                                            <div class="DMComposer-send">
                                                <button class="EdgeButton EdgeButton--primary tweet-action disabled" id="sendbird_send_btn_id" aria-disabled="true" type="button">
                                                    <span class="button-text messaging-text"><?php echo __("Send");?>
</span>
                                                </button>
                                            </div>
                                            <div class="position: absolute; visibility: hidden;"></div>
                                        </form>
                                    </div>
                                    <div class="DMConversation-readonly">
                                        <div class="DMConversation-readOnlyFooter">
                                            <?php echo __("You can no longer send messages to this person.");?>

                                            <a href="#" target="_blank" class="learn-more" rel="noopener"><?php echo __("Learn more");?>
</a>
                                        </div>
                                    </div>
                                    <div class="DMConversation-feedback">
                                        <div class="DMFeedback" style="display: none">
                                            <button type="button" class="DMFeedback-dismiss">
                                                <span class="Icon Icon--close"></span>
                                                <span class="u-hiddenVisually"><?php echo __("Dismiss");?>
</span>
                                            </button>
                                            <iframe class="B2CFeedback" data-current-view scrolling="no" frameborder="0" height="0"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-section message-popup message-list" style="display: none;">
                <!-- modal-bg -->
                <div class="right-section__modal-bg"></div>
                <!-- top -->
                <div class="DMActivity-header">
                </div>
                <!-- chat -->
                <div class="chat">
                    <div class="chat-canvas"></div>

                    <div class="chat-input">
                        <div id="container">
                        </div>
                        <label class="chat-input-file" for="chat_file_input">
                            <input type="file" name="file" id="chat_file_input" style="display: none;">
                        </label>

                        <div class="chat-input-text">
                            <textarea class="chat-input-text__field" placeholder="Write a chat" disabled="true" autofocus></textarea>
                        </div>
                        <!--  <div class="attachement_section">
                            <i class="fa fa-camera"></i>
                            <i class="fa fa-paper-plane"></i>
                        </div> -->
                    </div>
                    <div class="footer_send">
                        <button type="submit" class="btn btn-primary disabled post-chat" ><?php echo __("Send");?>
</button>
                    </div>
                    <label class="chat-input-typing"></label>
                </div>
            </div> <!-- // end chat section -->
        </div>
        <!-----------------------
            modal
        ------------------------>
        <div class="modal-messaging">
            <div class="modal-messaging-top">
                <i class="fa fa-times"></i>
                <label class="modal-messaging-top__title"><?php echo __("Group Channel");?>
</label>
                <label class="modal-messaging-top__desc"><?php echo __("Member list shows people inside the application.");?>
</label>
            </div>
            <div class="modal-messaging-list">
                <div class="modal-messaging-list__item">Username1<div class="modal-messaging-list__icon"></div></div>
                <div class="modal-messaging-list__item">Username2<div class="modal-messaging-list__icon modal-messaging-list__icon--select"></div></div>
                <div class="modal-messaging-more">MORE<div class="modal-messaging-more__icon"></div></div>
            </div>
            <div class="modal-messaging-bottom">
                <button type="button" class="btn btn-primary active" onclick="startMessaging()"><?php echo __("START MESSAGE");?>
</button>
            </div>
        </div>
        <div class="modal-member">
            <div class="modal-member-title"><?php echo __("CURRENT MEMBER LIST");?>
</div>
            <div class="modal-member-list"></div>
        </div>

        <div class="modal-invite">
            <div class="modal-invite-title"><?php echo __("USER LIST");?>
</div>
            <div class="modal-invite-top">
                <label class="modal-messaging-top__title modal-invite-top__title"><?php echo __("Group Channel");?>
</label>
                <label class="modal-invite-top__desc"><?php echo __("Member list shows people inside the application.");?>
</label>
            </div>
            <div class="modal-messaging-list modal-invite-list">
            </div>
            <div class="modal-invite-bottom">
                <button type="button" class="modal-invite-bottom__button" onclick="inviteMember()"><?php echo __("Invite");?>
</button>
            </div>
        </div>

        <div class="modal-leave-channel">
            <div class="modal-leave-channel-card">
                <div class="modal-leave-channel-title"><?php echo __("Are you Sure?");?>
</div>
                <div class="modal-leave-channel-desc"><?php echo __("Do you want to leave this channel?");?>
</div>
                <div class="modal-leave-channel-separator"></div>
                <div class="modal-leave-channel-bottom">
                    <button type="button" class="modal-leave-channel-button modal-leave-channel-close"><?php echo __("Cancel");?>
</button>
                    <button type="button" class="modal-leave-channel-button modal-leave-channel-submit"><?php echo __("Yes");?>
</button>
                </div>
            </div>
        </div>

        <div class="modal-hide-channel">
            <div class="modal-hide-channel-card">
                <div class="modal-hide-channel-title"><?php echo __("Are you Sure?");?>
</div>
                <div class="modal-hide-channel-desc"><?php echo __("Do you want to hide this channel?");?>
</div>
                <div class="modal-hide-channel-separator"></div>
                <div class="modal-hide-channel-bottom">
                    <button type="button" class="modal-hide-channel-button modal-hide-channel-close"><?php echo __("Cancel");?>
</button>
                    <button type="button" class="modal-hide-channel-button modal-hide-channel-submit"><?php echo __("Yes");?>
</button>
                </div>
            </div>
        </div>

        <div class="modal-confirm">
            <div class="modal-confirm-card">
                <div class="modal-confirm-title"><?php echo __("Are you Sure?");?>
</div>
                <div class="modal-confirm-desc"><?php echo __("Do you want to hide this channel?");?>
</div>
                <div class="modal-confirm-separator"></div>
                <div class="modal-confirm-bottom">
                    <button type="button" class="modal-confirm-button modal-confirm-close"><?php echo __("Cancel");?>
</button>
                    <button type="button" class="modal-confirm-button modal-confirm-submit"><?php echo __("Yes");?>
</button>
                </div>
            </div>
        </div>

        <div class="modal-input">
            <div class="modal-input-card">
                <div class="modal-input-title"><?php echo __("Type info");?>
</div>
                <div class="modal-input-desc"><?php echo __("Create Open Channel");?>
</div>
                <div class="modal-input-box">
                    <input type="text" class="modal-input-box-elem" />
                </div>
                <div class="modal-input-separator"></div>
                <div class="modal-input-bottom">
                    <button type="button" class="modal-input-button modal-input-close"><?php echo __("Cancel");?>
</button>
                    <button type="button" class="modal-input-button modal-input-submit"><?php echo __("Create");?>
</button>
                </div>
            </div>
        </div>
    </div>
</li>

<div id="myModal" class="modal fade direct_chat" role="dialog" style="position: fixed;width: 100%;">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="DMActivity DMInbox js-ariaDocument u-chromeOverflowFix DMActivity--open" role="document">
                    <div class="DMActivity-header">
                        <h2 class="DMActivity-title js-ariaTitle" id="dm_dialog-header"><?php echo __("Direct Message");?>
</h2>
                        <div class="DMActivity-toolbar">
                            <button type="button" class="DMInbox-toolbar EdgeButton EdgeButton--small EdgeButton--secondary EdgeButton--icon mark-all-read js-tooltip" title="Mark all as read">
                                <span class="Icon Icon--markAllRead"></span>
                                <span class="u-hiddenVisually"><?php echo __("Mark all as read");?>
</span>
                            </button>
                            <button type="button" class="DMInbox-toolbar DMComposeButton EdgeButton EdgeButton--small EdgeButton--primary dm-new-button js-initial-focus">
                                <span><?php echo __("New Message");?>
</span>
                            </button>
                            <button type="button" class="DMActivity-close js-close u-textUserColorHover transparent-no-border-button" onclick="DMInbox_closeDiag()">
                                <span class="Icon Icon--close Icon--medium"></span>
                                <span class="u-hiddenVisually"><?php echo __("Close");?>
</span>
                            </button>
                        </div>
                    </div>
                    <div class="DMActivity-container">
                        <div class="DMActivity-notice">
                            <div class="DMNotice DMNotice--error DMErrorBar" style="display: none;">
                                <div class="DMNotice-message">
                                    <div class="DMErrorBar-text"></div>
                                </div>
                                <div class="DMNotice-actions u-emptyHide"></div>
                                <button type="button" class="DMNotice-dismiss">
                                    <span class="Icon Icon--close"></span>
                                    <span class="u-hiddenVisually"><?php echo __("Dismiss");?>
</span>
                                </button>
                            </div>
                            <div class="DMNotice DMNotice--toast" style="display: none">
                                <div class="DMNotice-message"></div>
                                <div class="DMNotice-actions u-emptyHide"></div>
                                <button type="button" class="DMNotice-dismiss">
                                    <span class="Icon Icon--close"></span>
                                    <span class="u-hiddenVisually"><?php echo __("Dismiss");?>
</span>
                                </button>
                            </div>
                            <div class="DMNotice DMNotice--explicitDismiss DMNotificationPermissionRequest" style="display: none">
                                <div class="DMNotice-message">
                                    <?php echo __("Would you like to receive browser notifications such as Messages, Follows, and Likes?");?>

                                </div>
                                <div class="DMNotice-actions u-emptyHide">
                                    <button type="button" class="DMNotificationsPermissionRequest-later EdgeButton EdgeButton--tertiary js-prompt-later"><?php echo __("Maybe later");?>
</button>
                                    <button type="button" class="DMNotificationsPermissionRequest-accept EdgeButton EdgeButton--secondary js-prompt-accept"><?php echo __("Enable notifications");?>
</button>
                                </div>
                            </div>
                        </div>
                        <nav class="DMInbox-tab u-hidden" aria-label="Direct Message Inboxes">
                            <ul class="DMInbox-tabToggle">
                                <li class="DMInbox-tabToggleItem DMInbox-inboxTab is-active">
                                    <a role="button" href="#" class="DMInbox-tabCopy"><?php echo __("Inbox");?>
</a>
                                </li>
                                <li class="DMInbox-tabToggleItem DMInbox-requestTab">
                                    <a role="button" href="#" class="DMInbox-tabCopy">$<?php echo __("Requests");?>
</a>
                                </li>
                            </ul>
                        </nav>
                        <div class="DMActivity-body js-ariaBody">
                            <div class="DMInbox-content u-scrollY">
                                <div class="DMInbox-primary">
                                    <ul class="DMInbox-conversations"></ul>
                                    <div class="DMInbox-empty">
                                        <div class="DMEmptyState">
                                            <h2 class="DMEmptyState-header"><?php echo __("Send a message, get a message");?>
</h2>
                                            <div class="DMEmptyState-details">
                                                <p><?php echo __("Direct Messages are private conversations between you and other people on Twitter. Share Tweets, media, and more!");?>
</p>
                                            </div>
                                            <div class="DMEmptyState-cta">
                                                <button type="button" class="EdgeButton EdgeButton--primary dm-new-button"><?php echo __("Start a conversation");?>
</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="DMInbox-spinner u-hidden">
                                        <div class="DMSpinner"></div>
                                    </div>
                                </div>
                                <div class="DMInbox-secondary u-hidden is-empty">
                                    <div class="DMInbox-secondaryInboxCopy">
                                        <?php echo __("This is where you'll see messages from people you don't follow. They won't know you've seen them until you accept it.");?>

                                    </div>
                                    <ul class="DMInbox-untrustedConversations"></ul>
                                    <div class="DMInbox-empty">
                                        <div class="DMEmptyState">
                                            <h2 class="DMEmptyState-header"><?php echo __("You don't have any message requests");?>
</h2>
                                            <div class="DMEmptyState-details">
                                                <p><?php echo __("This is where you'll see messages from people you don't follow. They won't know you've seen them until you accept it.");?>
</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="DMInbox-spinner u-hidden">
                                        <div class="DMSpinner"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="DMActivity-footer u-emptyHide"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="newMessagesModal" class="modal fade headerdmmsg" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="DMActivity DMCompose js-ariaDocument u-chromeOverflowFix DMActivity--open" role="document">
                    <div class="DMActivity-header">
                        <div class="DMActivity-navigation">
                            <button type="button" class="DMActivity-back u-textTextUserColorHover transparent-no-border-button">
                                <span class="Icon Icon--caretLeft u-linkComplex-target Icon--medium"></span>
                                <span class="u-hiddenVisually"><?php echo __("Back to inbox");?>
</span>
                            </button>
                        </div>
                        <h2 class="DMActivity-title js-ariaTitle"><?php echo __("New Message");?>
</h2>
                        <div class="DMActivity-toolbar">
                            <button type="button" class="DMActivity-close js-close u-textUserColorHover transparent-no-border-button">
                                <span class="Icon Icon--close Icon--medium"></span>
                                <span class="u-hiddenVisually">Close</span>
                            </button>
                        </div>
                    </div>
                    <div class="DMActivity-container">
                        <div class="DMActivity-notice"></div>
                        <div class="DMActivity-body js-ariaBody">
                            <div class="DMDialogTypeahead">
                                <span class="DMTypeaheadHeader"><?php echo __("Send message to:");?>
</span>
                                <ul class="TokenizedMultiselect-inputContainer">
                                    <li id="TokenizedMultiselect-root">
                                        <textarea class="TokenizedMultiselect-input twttr-directmessage-input js-initial-focus dm-to-input" aria-autocomplete="list" aria-expanded="true" rows="1" type="text" placeholder="Enter a name"></textarea>
                                    </li>
                                </ul>
                                <ul id="DMComposeTypeaheadSuggestions" class="DMTypeaheadSuggestions u-scrollY" role="listbox">
								<div class="loading" id="user_loading_id" style="display: none">
										<div class="loader loader_medium"></div>
									</div>
                                </ul>
                            </div>
                        </div>
                        <div class="DMActivity-footer u-emptyHide">
                            <div class="DMButtonBar">
                                <button type="button" class="EdgeButton EdgeButton--primary dm-initiate-conversation"><?php echo __("Next");?>
</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="conversationSettingsModal" class="modal fade" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="DMActivity DMConversationSettings js-ariaDocument u-chromeOverflowFix DMActivity--open" role="document">
                    <div class="DMActivity-header">
                        <div class="DMActivity-navigation">
                            <button type="button" class="DMActivity-back u-textUserColorHover transparent-no-border-button" to-convo="true">
                                <span class="Icon Icon--caretLeft u-linkComplex-target Icon--medium"></span>
                                <span class="u-hiddenVisually"><?php echo __("Back to inbox");?>
</span>
                            </button>
                        </div>
                        <h2 class="DMActivity-title js-ariaTitle" id="dm_dialog-header"><?php echo __("Group info");?>
</h2>
                        <div class="DMActivity-toolbar">
                            <div class="DMConversationSettings-dropdown u-posRelative u-textLeft u-textUserColorHover">
                                <div class="DMPopover DMPopover--left">
                                    <button class="DMPopover-button transparent-no-border-button" aria-haspopup="true">
                                        <span class="u-hiddenVisually"><?php echo __("More");?>
</span>
                                        <span class="Icon Icon--dots Icon--medium"></span>
                                    </button>
                                    <div class="DMPopover-content Caret Caret--top Caret--stroked ">
                                        <ul class="DMPopoverMenu js-focus-on-open u-dropdownUserColor" tabindex="-1" role="menu">
                                            <li class="js-actionEditGroupName">
                                                <button type="button" class="DMPopoverMenu-button"><?php echo __("Edit group name");?>
</button>
                                            </li>
                                            <li class="js-actionChangeAvatar">
                                                <button type="button" class="DMPopoverMenu-button"><?php echo __("Upload new photo");?>
</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="DMActivity-close js-close u-textUserColorHover transparent-no-border-button">
                                <span class="Icon Icon--close Icon--medium"></span>
                                <span class="u-hiddenVisually"><?php echo __("Close");?>
</span>
                            </button>
                        </div>
                    </div>
                    <div class="DMActivity-container">
                        <div class="DMActivity-notice">
                            <div class="DMNotice DMNotice--error DMErrorBar" style="display: none;">
                                <div class="DMNotice-message">
                                    <div class="DMErrorBar-text"></div>
                                </div>
                                <div class="DMNotice-actions u-emptyHide"></div>
                                <button type="button" class="DMNotice-dismiss">
                                    <span class="Icon Icon--close"></span>
                                    <span class="u-hiddenVisually"><?php echo __("Dismiss");?>
</span>
                                </button>
                            </div>
                            <div class="DMNotice DMNotice--toast " style="display: none;">
                                <div class="DMNotice-message"></div>
                                <div class="DMNotice-actions u-emptyHide"></div>
                                <button type="button" class="DMNotice-dismiss">
                                    <span class="Icon Icon--close"></span>
                                    <span class="u-hiddenVisually"><?php echo __("Dismiss");?>
</span>
                                </button>
                            </div>
                            <div class="DMNotice DMDeleteConversation" style="display: none;">
                                <div class="DMNotice-message">
                                    <span class="DMDeleteConversation-message">
                                        <?php echo __("This conversation history will be deleted from your inbox.");?>

                                    </span>
                                </div>
                                <div class="DMNotice-actions u-emptyHide">
                                    <button type="button" class="DMDeleteConversation-cancel EdgeButton EdgeButton--tertiary"><?php echo __("Cancel");?>
</button>
                                    <button type="button" class="DMDeleteConversation-confirm EdgeButton EdgeButton--danger js-initial-focus"><?php echo __("Leave");?>
</button>
                                </div>
                            </div>
                        </div>
                        <div class="DMActivity-body js-ariaBody DMConversationSettings-container js-initial-focus flex-module u-scrollY">
                            <div class="DMConversationSettings-avatar">
                                <div class="DMUpdateAvatar has-defaultAvatar" aria-haspopup="true" data-has-custom-avatar="false">
                                    <div class="DMPopover DMPopover--center">
                                        <button class="transparent-no-border-button DMPopover-button" aria-haspopup="true">
                                            <span class="u-hiddenVisually"><?php echo __("Update group photo.");?>
</span>
                                            <div class="DMUpdateAvatar-avatar">
                                                <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">
                                                    <span class="DMAvatar-container">
                                                        <img class="DMAvatar-image" src="" alt="" title="">
                                                    </span>
                                                </div>
                                            </div>
                                        </button>
                                        <div class="DMPopover-content Caret Caret--top Caret--stroked ">
                                            <ul class="DMPopoverMenu u-textCenter js-focus-on-open u-dropdownUserColor" tabindex="-1" role="menu">
                                                <li class="DMUpdateAvatar-view">
                                                    <button type="button" class="DMPopoverMenu-button"><?php echo __("View photo");?>
</button>
                                                </li>
                                                <li class="DMUpdateAvatar-change">
                                                    <button type="button" class="DMPopoverMenu-button"><?php echo __("Upload photo");?>
</button>
                                                </li>
                                                <li class="DMUpdateAvatar-remove">
                                                    <button type="button" class="DMPopoverMenu-button"><?php echo __("Remove");?>
</button>
                                                </li>
                                            </ul>
                                            <div class="DMUpdateAvatar-photoSelector photo-selector" tabindex="-1" aria-hidden="true">
                                                <div class="image-selector">
                                                    <input type="hidden" name="media_file_name" class="file-name">
                                                    <input type="hidden" name="media_data_empty" class="file-data">
                                                    <label class="t1-label">
                                                        <span class="u-hiddenVisually"><?php echo __("Add Photo");?>
</span>
                                                        <input type="file" name="media[]" class="file-input js-tooltip" accept="image/*" tabindex="-1" title="Add Photo">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="DMConversationSettings-name">
                                <div class="DMUpdateName u-textTruncate">
                                    <div class="DMUpdateName-header account-group">
                                        <span class="DMUpdateName-name u-textTruncate edit-allowed js-tooltip" title="Click to edit group name">Double Bear, Daniel Lee</span>
                                        <span class="UserBadges">
                                            <span class="Icon Icon--verified js-verified hidden">
                                                <span class="u-hiddenVisually"><?php echo __("Verified account");?>
</span>
                                            </span>
                                            <span class="Icon Icon--protected js-protected hidden">
                                                <span class="u-hiddenVisually"><?php echo __("Protected Tweets");?>
</span>
                                            </span>
                                        </span>
                                    </div>
                                    <div class="DMUpdateName-screenName u-textTruncate"></div>
                                    <div class="DMUpdateName-controls">
                                        <span class="DMUpdateName-spinner DMSpinner u-hidden"></span>
                                        <div class="DMUpdateName-form input-group u-hidden">
                                            <input type="text" class="DMUpdateName-input" aria-label="Edit group name">
                                            <button class="DMUpdateName-confirm u-textUserColorLight">
                                                <span class="Icon Icon--check"></span>
                                                <span class="u-hiddenVisually"><?php echo __("Save group name");?>
</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="DMConversationSettings-notifications">
                                <h3>Notifications</h3>
                                <label class="t1-label checkbox">
                                    <input type="checkbox" name="dm[toggle_notifications]"><?php echo __("Mute notifications");?>

                                </label>
                                <p class="DMConversationSettings-notificationsFooter t1-infotext"><?php echo __("Disable all notifications from this conversation. This will not remove you from the group.");?>
</p>
                            </div>
                            <div class="DMConversationSettings-mentions u-hidden">
                                <label class="t1-label checkbox">
                                    <input type="checkbox" name="dm[toggle_mentions]"><?php echo __("Mentions");?>

                                </label>
                                <p class="DMConversationSettings-mentionsFooter t1-infotext"><?php echo __("Receive notifications when people mention you in this group.");?>
</p>
                            </div>
                            <div class="DMConversationSettings-subscriptions u-hidden">
                                <h3>Subscriptions</h3>
                                <label class="t1-label checkbox">
                                    <input type="checkbox" name="dm[toggle_subscriptions]"><?php echo __("Subscribe to updates");?>

                                </label>
                                <p class="DMConversationSettings-subscriptionsFooter t1-infotext"></p>
                            </div>
                        </div>
                        <div class="DMActivity-footer u-emptyHide">
                            <div class="DMConversationSettings-footer u-flexRow u-bgUserColorLightest">
                                <!--<button type="button" class="EdgeButton EdgeButton--secondary js-actionReportConversation">Report conversation</button>-->
                                <button type="button" class="EdgeButton EdgeButton--danger js-actionDeleteConversation"><?php echo __("Leave conversation");?>
</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--<div class="modal-overlay custom-overlay"></div>-->


<?php echo '<script'; ?>
 type="text/javascript">
    $('.DMConversation-convoSettings.dm-to-convoSettings').click(function () {
        $('.right-section.message-popup').css('display', 'none');
        //$('.DMConversationSettings').addClass('DMActivity--open');
    });
$('.title-messaging').on('click', function(){
    $('#myModal').modal('show');
    $('body').addClass('modal-open');
	$('html').css({'position':'fixed'});
});

$('.DMInbox-toolbar').click(function () {

	$('html').css({'position':'fixed'});
});

//new code
$('.DMActivity-close').click(function () {
  $('.DMComposer-attachment').hide();
  $('#org_post_detail').attr('org-post-id', '');
  $('#post_author_name_id').text('');
  $('#post_author_url_id').attr('href','');
  $('#post_author_url_id').attr('data-user-id', '');
  $("#post_avatar_id").attr('src', '');
  $('#post_author_username_id').html('');
  $('#post_content_id').html('');
  $('#TokenizedMultiselect-root .InputToken').remove();
	$('#myModal').modal('hide');
	$('html').css({'position':''});
  $('body').removeClass('modal-open');
});

$('.close_chat').click(function () {

    $('form.DMComposer').removeClass('has-thumbnail');
    $('.DMComposer-attachment .ComposerThumbnails').html('');
    $('#chatModal').modal('hide');
    $('html').css({'position':''});
});

$('#chatModal').on('shown.bs.modal', function (e) {
    $('.thumbnail-container').css('display', 'block');
    $('.DMComposer-mediaPicker input[type="file"]').attr('disabled', false);
});

$('.DMActivity-back').click(function () {
    $('#TokenizedMultiselect-root .InputToken').remove();
    $('.thumbnail-container').css('display', 'none');
    $('form.DMComposer').removeClass('has-thumbnail');
    $('.DMComposer-attachment .ComposerThumbnails').html('');

});
$(document).on("click", function (e) {
	if ($(e.target).is(".navbar-nav .open.attachment") === false) {
		$(".navbar-nav .open.attachment").removeClass("open");
	}
});
<?php echo '</script'; ?>
>


<?php echo '<script'; ?>
 src="/content/themes/default/templates/static/lib/SendBird.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/content/themes/default/templates/static/js/util.js?v=1"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/content/themes/default/templates/static_shared/js/chat.js?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/locale/es-us.js"><?php echo '</script'; ?>
>
<?php }
}
