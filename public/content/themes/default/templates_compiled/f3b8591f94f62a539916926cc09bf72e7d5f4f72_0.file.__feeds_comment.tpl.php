<?php
/* Smarty version 3.1.31, created on 2018-08-31 12:48:28
  from "/var/app/current/content/themes/default/templates/__feeds_comment.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b89391cca8120_23345039',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f3b8591f94f62a539916926cc09bf72e7d5f4f72' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/__feeds_comment.tpl',
      1 => 1531999524,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:__feeds_comment.text.tpl' => 1,
    'file:__feeds_comment.tpl' => 2,
    'file:_emoji-menu.tpl' => 1,
  ),
),false)) {
function content_5b89391cca8120_23345039 (Smarty_Internal_Template $_smarty_tpl) {
?>
<li>
    <div class="comment <?php if ($_smarty_tpl->tpl_vars['_is_reply']->value) {?>reply<?php }?>" data-id="<?php echo $_smarty_tpl->tpl_vars['_comment']->value['comment_id'];?>
" id="comment_<?php echo $_smarty_tpl->tpl_vars['_comment']->value['comment_id'];?>
">
        <!-- comment avatar -->
        <div class="comment-avatar">
            <a class="comment-avatar-picture" href="<?php echo $_smarty_tpl->tpl_vars['_comment']->value['author_url'];?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['_comment']->value['author_picture'];?>
);">
            </a>
        </div>
        <!-- comment avatar -->

        <!-- comment body -->
        <div class="comment-data">
            <!-- comment menu -->
            <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                <?php if (!$_smarty_tpl->tpl_vars['_comment']->value['edit_comment'] && !$_smarty_tpl->tpl_vars['_comment']->value['delete_comment']) {?>
                  

                    <div class="comment-btn dropdown pull-right flip">
                        <i class="fa fa-chevron-down dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" data-placement="top" title='<?php echo __("Report or Block User");?>
'></i>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#" class="js_report" data-handle="comment" data-id="<?php echo $_smarty_tpl->tpl_vars['_comment']->value['comment_id'];?>
"><?php echo __("Report");?>
</a>
                            </li>
                            <li>
                                <a href="#" class="js_block-comment" data-id="<?php echo $_smarty_tpl->tpl_vars['_comment']->value['user_id'];?>
"><?php echo __("Block User");?>
</a>
                            </li>
                        </ul>
                    </div>
                     
                 <?php } elseif (!$_smarty_tpl->tpl_vars['_comment']->value['edit_comment'] && $_smarty_tpl->tpl_vars['_comment']->value['delete_comment']) {?>
               


                    <div class="comment-btn dropdown pull-right flip">
                        <i class="fa fa-chevron-down dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" data-placement="top" title='<?php echo __("Report or Block User");?>
'></i>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#" class="js_delete-comment" ><?php echo __("Delete Comment");?>
</a>
                            </li>
                            <li>
                                <a href="#" class="js_block-comment" data-id="<?php echo $_smarty_tpl->tpl_vars['_comment']->value['user_id'];?>
"><?php echo __("Block User");?>
</a>
                            </li>
                        </ul>
                    </div>
                <?php } else { ?>
                    <div class="comment-btn dropdown pull-right flip">
                        <i class="fa fa-times dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" data-placement="top" title='<?php echo __("Edit or Delete");?>
'></i>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#" class="js_edit-comment"><?php echo __("Edit Comment");?>
</a>
                            </li>
                            <li>
                                <a href="#" class="js_delete-comment"><?php echo __("Delete Comment");?>
</a>
                            </li>
                        </ul>
                    </div>
                     
                <?php }?>
            <?php }?>
            <!-- comment menu -->

            <!-- comment author & text  -->
            <div class="mb5 js_notifier-flasher">
                <!-- author -->
                <span class="text-semibold js_user-popover" data-type="<?php echo $_smarty_tpl->tpl_vars['_comment']->value['user_type'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['_comment']->value['user_id'];?>
">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['_comment']->value['author_url'];?>
"  >
                        <span class="first-name-custom"><?php echo $_smarty_tpl->tpl_vars['_comment']->value['author_name'];?>
</span>
                        <span class="second-name-custom">@<?php echo $_smarty_tpl->tpl_vars['_comment']->value['user_name'];?>
</span>
                    </a>
                </span>
                <?php if ($_smarty_tpl->tpl_vars['_comment']->value['author_verified']) {?>
                <i data-toggle="tooltip" data-placement="top" title='<?php echo __("Verified User");?>
' class="fa fa-check-circle fa-fw verified-badge"></i>
                <?php }?>
                <!-- author -->

                <!-- text -->
                <?php $_smarty_tpl->_subTemplateRender('file:__feeds_comment.text.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                <!-- text -->
            </div>
            <!-- comment author & text  -->

            <!-- comment actions & time  -->
            <div>
                <!-- actions -->          
                <!-- actions -->
                <!-- time  -->
                <small class="text-x-muted js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['_comment']->value['time'];?>
"><?php echo $_smarty_tpl->tpl_vars['_comment']->value['time'];?>
</small>
                <!-- time  -->
            </div>

            <div class="custom-post-actions">
                <div class="post-actions">
                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                            <!-- comment -->
                            <span class="text-clickable mr20 js-custom-share-comment" data-id="<?php echo $_smarty_tpl->tpl_vars['_comment']->value['comment_id'];?>
" data-target="#modal-comment-reply" data-handle="comment">
                                <i class="Icon Icon--medium Icon--reply"></i> <span id="span-share-counter-inside-<?php echo $_smarty_tpl->tpl_vars['_comment']->value['comment_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['_comment']->value['replies'];?>
</span>
                            </span>
                            <!-- comment -->
                            <!-- like -->
                            <span class="text-clickable <?php if ($_smarty_tpl->tpl_vars['_comment']->value['i_like']) {?>text-active js_unlike-comment<?php } else { ?>js_like-comment<?php }?>">
                                <i class="Icon Icon--heart Icon--medium"></i><i class="Icon Icon--heartBadge Icon--medium"></i> 
                                <span class="span-counter_<?php echo $_smarty_tpl->tpl_vars['_comment']->value['comment_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['_comment']->value['likes'];?>
</span>
                            </span>
                            <!-- like -->

                            <?php if (!$_smarty_tpl->tpl_vars['_is_reply']->value) {?>
                                <?php if (!$_smarty_tpl->tpl_vars['standalone']->value && $_smarty_tpl->tpl_vars['_comment']->value['replies'] > 0) {?>
                                <!-- <div class="ptb10 plr10 js_replies-toggle"> -->
                                    <span class="text-link ptb10 plr10 js_replies-toggle">
                                        <i class="fa fa-comments-o"></i>
                                        <?php echo $_smarty_tpl->tpl_vars['_comment']->value['replies'];?>
 <?php echo __("Replies");?>

                                    </span>
                                <!-- </div> -->
                                <?php }?>
                            <?php }?>

                        <?php } else { ?>
                            <a href="/signin"><?php echo __("Please log in to like, share and comment!");?>
</a>
                        <?php }?>
						<div class="sharethis-inline-share-buttons" data-url="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/posts/<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"></div>
                </div>
            </div>
            <!-- comment actions & time  -->

            <!-- comment replies  -->
            <?php if (!$_smarty_tpl->tpl_vars['_is_reply']->value) {?>
                
                <div style="clear: both"></div>
                <div class="loading rply_loader_cls" id="post_comments_loading_id_<?php echo $_smarty_tpl->tpl_vars['_comment']->value['comment_id'];?>
" style="display: none">
                    <div class="loader loader_medium"></div>
                </div>
                <div class="comment-replies <?php if (!$_smarty_tpl->tpl_vars['standalone']->value) {?>x-hidden<?php }?>">
                    <!-- previous replies -->
                    <?php if ($_smarty_tpl->tpl_vars['_comment']->value['replies'] >= $_smarty_tpl->tpl_vars['system']->value['min_results']) {?>
                        <div class="pb10 text-center js_see-more" data-get="comment_replies" data-id="<?php echo $_smarty_tpl->tpl_vars['_comment']->value['comment_id'];?>
" data-remove="true">
                            <span class="text-link">
                                <i class="fa fa-comment-o"></i>
                                <?php echo __("View previous replies");?>

                            </span>
                            <div class="loader loader_small x-hidden"></div>
                        </div>
                    <?php }?>
                    <!-- previous replies -->

                    <!-- replies -->
                    <ul class="js_replies">
                        <?php if ($_smarty_tpl->tpl_vars['_comment']->value['replies'] > 0) {?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['_comment']->value['comment_replies'], 'reply');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['reply']->value) {
?>
                            <?php $_smarty_tpl->_subTemplateRender('file:__feeds_comment.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_comment'=>$_smarty_tpl->tpl_vars['reply']->value,'_is_reply'=>true), 0, true);
?>

                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        <?php }?>
                    </ul>
                    <!-- replies -->

                    <!-- post a reply -->
                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                        <div class="x-hidden js_reply-form">
                            <div class="x-form comment-form">
                                <textarea dir="auto" class="js_autosize js_mention js_post-reply" rows="1" placeholder='<?php echo __("Write a reply");?>
'></textarea>
                                <div class="x-form-tools">
                                    <div class="x-form-tools-post js_post-reply">
                                        <i class="fa fa-paper-plane-o"></i>
                                    </div>
                                    <div class="x-form-tools-attach">
                                        <i class="fa fa-camera js_x-uploader" data-handle="comment"></i>
                                    </div>
                                    <div class="x-form-tools-emoji js_emoji-menu-toggle">
                                        <i class="fa fa-smile-o fa-lg"></i>
                                    </div>
                                    <?php $_smarty_tpl->_subTemplateRender('file:_emoji-menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                </div>
                            </div>
                            <div class="comment-attachments attachments clearfix x-hidden">
                                <ul>
                                    <li class="loading">
                                        <div class="loader loader_small"></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    <?php }?>
                    <!-- post a reply -->
                </div>
            <?php }?>
            <!-- comment replies  -->
        </div>
        <!-- comment body -->
    </div>
</li>
<style type="text/css">
    .rply_loader_cls .loader.loader_medium {

    display: inherit;

}
</style><?php }
}
