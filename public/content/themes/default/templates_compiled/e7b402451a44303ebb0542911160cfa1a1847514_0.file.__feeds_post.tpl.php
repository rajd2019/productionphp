<?php
/* Smarty version 3.1.31, created on 2018-09-07 12:06:06
  from "/var/app/current/content/themes/default/templates/__feeds_post.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b9269ae129ff9_00490007',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e7b402451a44303ebb0542911160cfa1a1847514' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/__feeds_post.tpl',
      1 => 1536321964,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:__feeds_post.body.tpl' => 1,
    'file:__feeds_post.social.tpl' => 1,
  ),
),false)) {
function content_5b9269ae129ff9_00490007 (Smarty_Internal_Template $_smarty_tpl) {
if (!$_smarty_tpl->tpl_vars['standalone']->value) {?><li><?php }?>
    <!-- post -->
    <div class="post <?php if ($_smarty_tpl->tpl_vars['boosted']->value) {?>boosted<?php }?>" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">

        <?php if ($_smarty_tpl->tpl_vars['standalone']->value && $_smarty_tpl->tpl_vars['pinned']->value) {?>
            <div class="pin-icon" data-toggle="tooltip" title="<?php echo __('Pinned Post');?>
">
                <i class="fa fa-bookmark"></i>
            </div>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['standalone']->value && $_smarty_tpl->tpl_vars['boosted']->value) {?>
            <div class="boosted-icon" data-toggle="tooltip" title="<?php echo __('Promoted');?>
">
                <i class="fa fa-bullhorn"></i>
            </div>
        <?php }?>

       <?php if (!$_smarty_tpl->tpl_vars['_shared']->value && $_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
      <div class="pull-right flip dropdown custom-dropdown-profile"> 
        <i class="fa fa-chevron-down dropdown-toggle" data-toggle="dropdown"></i>
        <ul class="dropdown-menu post-dropdown-menu">
         <?php if ($_smarty_tpl->tpl_vars['post']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
          <li> <a href="#" class="" data-toggle="modal" data-url="chat/direct_share.php?id=<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
            <div class="action no-desc"> <i class="fa fa-bookmark fa-fw"></i> <span><?php echo __("Share via Direct Message ");?>
</span> </div>
            </a> </li>
            <li><span id="p<?php echo $_smarty_tpl->tpl_vars['post']->value['user_id'];?>
" style="display:none;"></span> <a href="javascript:void(0)"  data-clipboard-text="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/posts/<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" class="copy_link">
            <div class="action no-desc"> <i class="fa fa-bolt fa-fw"></i> <span><?php echo __("Copy Link to Post");?>
</span> </div>
            </a> </li>
            <li> <?php if ($_smarty_tpl->tpl_vars['post']->value['pinned']) {?> <a href="#" class="js_unpin-post">
            <div class="action no-desc"> <i class="fa fa-thumb-tack fa-fw"></i> <span><?php echo __("Unpin Post");?>
</span> </div>
            </a> <?php } else { ?> <a href="#" class="js_pin-post">
            <div class="action no-desc"> <i class="fa fa-thumb-tack fa-fw"></i> <span><?php echo __("Pin Post");?>
</span> </div>
            </a> <?php }?> </li>
             <li> <a href="#" class="js_delete-post">
            <div class="action no-desc"> <i class="fa fa-trash-o fa-fw"></i> <?php echo __("Delete Post");?>
 </div>
            </a> </li>
          <?php } else { ?>
           <li><span id="p<?php echo $_smarty_tpl->tpl_vars['post']->value['user_id'];?>
" style="display:none;">/includes/ajax/posts/product_editor.php?post_id=<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
</span> <a href="javascript:void(0)"  data-clipboard-text="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/posts/<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" class="copy_link">
            <div class="action no-desc"> <i class="fa fa-bolt fa-fw"></i> <span><?php echo __("Copy Link to Post");?>
</span> </div>
            </a> </li>
               <li> <a href="#" class="js_hide-post">
            <div class="action"> <i class="fa fa-eye-slash fa-fw"></i> <?php echo __("Mute Post");?>
 </div>
            <div class="action-desc"><?php echo __("See fewer posts like this");?>
</div>
            </a> </li>
          <li> <a href="#" class="js_block-comment" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['user_id'];?>
">
            <div class="action"> <i class="fa fa-ban fa-fw"></i> <?php echo __("Block");?>
 <?php echo $_smarty_tpl->tpl_vars['post']->value['post_author_name'];?>
</div>
            <div class="action-desc"><?php echo __("Block this user");?>
</div>
            </a> </li>
          <li> <a href="#" class="js_report" data-handle="post" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
            <div class="action no-desc"> <i class="fa fa-flag fa-fw"></i> <?php echo __("Report post");?>
 </div>
            </a> </li>
         <?php }?>
       
        </ul>
      </div>
      <?php }?>

        <!-- post body -->
         <?php if ($_smarty_tpl->tpl_vars['post']->value['post_id'] != 'a') {?>
        <div class="post-body" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" style="padding-bottom: 5px;">
            
            <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.body.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_post'=>$_smarty_tpl->tpl_vars['post']->value,'_shared'=>false), 0, false);
?>


            <!-- post stats -->
            
            <!-- post stats -->

            <!-- post actions -->
            
            <!-- post actions -->

        </div>
        <?php }?>
         <div class="custom-post-actions bottom-icons" style="float:none;">
            <?php if ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "video" && $_smarty_tpl->tpl_vars['post']->value['video'] && $_smarty_tpl->tpl_vars['post']->value['is_broadcast']) {?>
                <div>
                <?php ob_start();
echo $_smarty_tpl->tpl_vars['post']->value['post_id'];
$_prefixVariable1=ob_get_clean();
echo $_smarty_tpl->tpl_vars['user']->value->video_view_manager($_prefixVariable1);?>

                       <!-- views -->
                           <span class="js_post-views-num" style="font-size: 11px;color: #999;"><?php ob_start();
echo $_smarty_tpl->tpl_vars['post']->value['views'];
$_prefixVariable2=ob_get_clean();
echo $_smarty_tpl->tpl_vars['user']->value->views_format($_prefixVariable2);?>
</span>
                       <!-- views -->
           </div>
            <?php }?>
        <div class="post-actions">
                <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                    <!-- comment -->
                    <span class="text-clickable mr20" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" data-handle="post">
                        <i class="Icon Icon--medium Icon--reply post_comment_evnt" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" data-tooltip="tooltip" data-placement="top" title="" data-original-title="<?php echo __("Reply");?>
"></i> <span id="span-comments-counter_<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"><!--<?php echo __("Comment");?>
&nbsp;--><?php echo $_smarty_tpl->tpl_vars['post']->value['comments'];?>
</span>
                    </span>
                    <!-- comment -->

                    <!-- share -->
                    <?php if ($_smarty_tpl->tpl_vars['post']->value['privacy'] == "public") {?>
                        <span class="text-clickable mr20 share-tweet-post" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
                            <i class="Icon Icon--medium Icon--retweet" data-tooltip="tooltip" data-placement="top" title="" data-original-title="<?php echo __("Repost");?>
"></i> <span id="span-share-counter_<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"><!--<?php echo __("Share");?>
&nbsp;--><?php echo $_smarty_tpl->tpl_vars['post']->value['shares'];?>
</span>
                        </span>
                    <?php }?>
                    <!-- share -->
                    <!-- like -->
                    <span class="text-clickable <?php if ($_smarty_tpl->tpl_vars['post']->value['i_like']) {?>text-active js_unlike-post<?php } else { ?>js_like-post<?php }?> mr20">
                        <i class="Icon Icon--heart Icon--medium" data-tooltip="tooltip" data-placement="top" title="" data-original-title="<?php echo __("Like");?>
"></i>
                        <i class="Icon Icon--heartBadge Icon--medium"></i>
                        <span class="span-counter_<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"><!--<?php echo __("Like");?>
&nbsp;--><?php echo $_smarty_tpl->tpl_vars['post']->value['likes'];?>
</span>
                    </span>
                    <!-- like -->
                    <!-- Direct Message -->

                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_id'] != $_smarty_tpl->tpl_vars['post']->value['author_id']) {?>
                    <span class="text-clickable direct_message_frm_post" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
                        <i class="Icon Icon--envelope Icon--medium" data-tooltip="tooltip" data-placement="top" title="" data-original-title="<?php echo __("Direct message");?>
"></i>

                    </span>
                    <?php }?>
                    <!-- Direct Message -->
                    <!-- Download -->
                    <?php if ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "video" && $_smarty_tpl->tpl_vars['post']->value['video'] && $_smarty_tpl->tpl_vars['post']->value['is_broadcast']) {?>
        
                   <span class="download-video-link">
                       <a href="/download_broadcast.php?video_url=<?php echo BROADCAST_URL;?>
/<?php echo $_smarty_tpl->tpl_vars['post']->value['video']['source'];?>
" class="download-broadcast"><i class="fa fa-download Icon--medium"></i></a>
                   </span>
                    <?php }?>
                   <!-- Download --> 

                <?php } else { ?>
                    <a href="/signin"><?php echo __("Please log in to like, share and comment!");?>
</a>
                <?php }?>
				<div class="sharethis-inline-share-buttons" data-url="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/posts/<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"></div>
        </div>
        </div>
        <!-- post body -->

        <!-- post footer -->
        <div class="post-footer <?php if (!$_smarty_tpl->tpl_vars['standalone']->value) {?>x-hidden<?php }?>">
            <!-- social sharing -->
            <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.social.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <!-- social sharing -->

            <!-- comments -->
            
            <!-- comments -->
        </div>
        <!-- post footer -->

    </div>
    <!-- post -->
<div id="openGallery"></div>
<?php echo '<script'; ?>
 id="lightbox" type="text/template">

    <div class="lightbox custom-lightbox-container">
      
        <div class="container lightbox-container">
            
            <button data-toggle="tooltip" data-placement="bottom" title='<?php echo __("Press Esc to close");?>
' type="button" class="close lightbox-close js_lightbox-close">
                <span class="Icon Icon--close Icon--large"></span>
            </button>
                
            <div class="lightbox-preview">
                 
                <div id="divLoading" style="display : none;position : fixed;z-index: 100;background-image : url('/content/themes/default/images/loading.gif');background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"></div> 
                

                <div class="post-image"><div style="display: table-cell;vertical-align: middle;" id="test-swap"><img alt="" class="img-responsive" src="{{image}}"></div></div>
                <div class="hover-text">
                </div>
                <div class="test-post-actions"></div>


            </div>

        </div>

    </div>

<?php echo '</script'; ?>
>
    
<?php if (!$_smarty_tpl->tpl_vars['standalone']->value) {?></li><?php }
}
}
