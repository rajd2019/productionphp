<?php
/* Smarty version 3.1.31, created on 2018-08-31 12:48:44
  from "/var/app/current/content/themes/default/templates/__feeds_followers.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b89392c7cc839_24190635',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b2c800bb64d8588b3663aacef9c4551d00670dce' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/__feeds_followers.tpl',
      1 => 1530181977,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b89392c7cc839_24190635 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_truncate')) require_once '/var/app/current/vendor/smarty/smarty/libs/plugins/modifier.truncate.php';
?>
<div class="js_posts_stream" data-get="<?php echo $_smarty_tpl->tpl_vars['_get']->value;?>
" data-filter="<?php if ($_smarty_tpl->tpl_vars['_filter']->value) {
echo $_smarty_tpl->tpl_vars['_filter']->value;
} else { ?>all<?php }?>" <?php if ($_smarty_tpl->tpl_vars['_id']->value) {?>data-id="<?php echo $_smarty_tpl->tpl_vars['_id']->value;?>
"<?php }?>>

		<ul>

			<!-- posts -->

                    <li>

                    <h3 class="AdaptiveSearchPage-moduleTitle"><?php echo __("People");?>
</h3>

                    <div class="row">

                         <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['people']->value, '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>

                    

                        	

                            <div class="col-sm-4">

                            	<div class="site_bar_new custom-site-bar-new"> 



                                    <div class="profile-bg-thumb">

                                    <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_cover']) {?><img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_cover'];?>
"><?php }?>

                                    </div>

                                    <div class="side_profile" style="height: 175px;"> 

                                        <div class="pro_thumb"> 

                                            <a href="/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>">

                                                <img class="data-avatar" src="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>
">

                                            </a>

                                        </div> 

                                        <div class="admin_detail"> 

                                            <span> 
											<?php if ($_smarty_tpl->tpl_vars['_user']->value['connection'] == 'follow') {?> 
                                                <a class="js_follow follow_btn" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" ><?php echo __("Follow");?>
</a>
                                            <?php } else { ?>  
                                                <a class="btn btn-default js_unfollow" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
"><i class="fa fa-check"></i><?php echo __("Following");?>
</a>
											<?php }?>
                                            </span> 

                                            <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
"> 

                                                <a href="/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>">

                                                    <span><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>
</span> 

                                                    <span class="user-at-btn">@<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
</span>

                                                </a> 

                                            </span> 

                                        </div> 

                                        <p><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['_user']->value['user_biography'],120,"&hellip;");?>
</p> 

                                	</div> 

                            	</div>

                            </div>

                            

                            

                            	

                            

                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>
</div>

                    </li>

               

           <?php }
}
