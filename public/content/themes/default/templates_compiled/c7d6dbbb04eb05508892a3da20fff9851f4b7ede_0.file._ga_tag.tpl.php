<?php
/* Smarty version 3.1.31, created on 2018-08-31 12:48:39
  from "/var/app/current/content/themes/default/templates/_ga_tag.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b893927a5a615_02769731',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c7d6dbbb04eb05508892a3da20fff9851f4b7ede' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/_ga_tag.tpl',
      1 => 1527699518,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b893927a5a615_02769731 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<?php echo '<script'; ?>
 async src="https://www.googletagmanager.com/gtag/js?id=UA-119903360-1"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
 
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  

  gtag('config', 'UA-119903360-1');
<?php echo '</script'; ?>
>
<?php }
}
