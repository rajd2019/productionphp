<?php
/* Smarty version 3.1.31, created on 2018-08-31 12:49:00
  from "/var/app/current/content/themes/default/templates/broadcast_list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b89393c779826_29393942',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '278943870b35389d1d6dd86be9ccc3f4c21066ef' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/broadcast_list.tpl',
      1 => 1530123165,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header_broadcast_list.tpl' => 1,
    'file:_footer_login.tpl' => 1,
  ),
),false)) {
function content_5b89393c779826_29393942 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header_broadcast_list.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


  <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet"> 
  <link href="https://unpkg.com/video.js/dist/video-js.css" rel="stylesheet">
    <?php echo '<script'; ?>
 src="https://unpkg.com/video.js/dist/video.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://unpkg.com/videojs-contrib-hls/dist/videojs-contrib-hls.js"><?php echo '</script'; ?>
>
    <link rel="stylesheet" href="/includes/assets/css/font-awesome/css/custom.css">
   <style>
   body {
       background:#ffffff!important;
   } 
   .autominheight {
       min-height:500px;
   }
   .dotclass {
       font-weight: bold;
        font-size: 25px;
        margin-right: 10px;
        margin-left: 10px;
   }
   </style><?php echo '<?php ';?>exit; <?php echo '?>';?>
    <div class="twitter-main" style="margin-top: 20px;">
            <div class="slider-main-box">
                <div class="banner-section">
                       
              </div>
              <div class="banner-text-section">
              <div class="container">
                  <div class="row">
                     <div class="col-sm-12 col-md-12 col-lg-12 autominheight">
						<div class="banner-text-inner">
							<!--div class="broadcast-live-videos">
								<h2><?php echo __("Video");?>
</h2>
								
								<video width="100%" controls="controls" preload="auto">
								 <source src="https://stage.guo.media/content/uploads/videos/boss.mp4" type="video/mp4">
								

								</video>
							</div-->
						</div>
                        <div class="banner-text-inner">
                            <div class="broadcast-live-videos">
                                <h2><?php echo __("Browse all available broadcasts");?>
</h2>
								
								<!--iframe style="width:100%" id="ls_embed_1524493967" src="https://livestream.com/accounts/27235681/events/8177404/player?width=960&height=540&enableInfoAndActivity=true&defaultDrawer=feed&autoPlay=true&mute=false"  height="540" frameborder="0" scrolling="no" allowfullscreen> </iframe><?php echo '<script'; ?>
 type="text/javascript" data-embed_id="ls_embed_1524493967" src="https://livestream.com/assets/plugins/referrer_tracking.js"><?php echo '</script'; ?>
!-->
								<iframe style="width:100%" id="ls_embed_1525861673" src="https://livestream.com/accounts/27235681/events/8197481/player?width=960&height=540&enableInfoAndActivity=true&defaultDrawer=feed&autoPlay=true&mute=false"  height="540" frameborder="0" scrolling="no" allowfullscreen> </iframe><?php echo '<script'; ?>
 type="text/javascript" data-embed_id="ls_embed_1525861673" src="https://livestream.com/assets/plugins/referrer_tracking.js"><?php echo '</script'; ?>
>
                <!--iframe id="ls_embed_1524752004" src="https://livestream.com/accounts/27235681/events/8177404/player?width=960&height=540&enableInfoAndActivity=true&defaultDrawer=feed&autoPlay=true&mute=false" width="960" height="540" frameborder="0" scrolling="no" allowfullscreen> </iframe><?php echo '<script'; ?>
 type="text/javascript" data-embed_id="ls_embed_1524752004" src="https://livestream.com/assets/plugins/referrer_tracking.js"><?php echo '</script'; ?>
-->
                                <!--div class="no-videos">
                                    
                                </div>
                                <div class="boss col-sm-12" style="float: left;">
                                    
                                </div>
                                <div class="others col-sm-12" style='clear: both;float: left;margin-top:10px'>
                                    
                                </div>
                                <div class="other-title-bar">
                                    <h4><?php echo __("How to broadcast in Guo Media?");?>
</h4>
                                    <p><?php echo __("We welcome everyone to express their viewpoints freely in our platform. Currently, we allow users to do broadcast via our mobile apps while audience can watch in both website and mobile apps. Please download our apps in App Store to become one of our broadcasters today! In the future, we will allow users to broadcast via website as well. Thank you so much for your support, everything is just beginning ️! ");?>

</p>

                                </div-->
                            </div>
                        </div>
                        
                        </div>

                      <div class="login_footer"> 
                      
<!-- footer -->
<div class="container">
   <img src="/content/themes/default/images/live.png" style="display:none">
  <div class="row footer">
    <div class="col-lg-6 col-md-6 col-sm-6">
      &copy; <?php echo date('Y');?>
 <?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 · <span class="text-link" data-toggle="modal" data-url="#translator"><span class="flag-icon flag-icon-<?php echo $_smarty_tpl->tpl_vars['system']->value['language']['flag_icon'];?>
"></span>&nbsp;<?php echo $_smarty_tpl->tpl_vars['system']->value['language']['title'];?>
</span>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-6 links">
      <?php if (count($_smarty_tpl->tpl_vars['static_pages']->value) > 0) {?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['static_pages']->value, 'static_page', true);
$_smarty_tpl->tpl_vars['static_page']->iteration = 0;
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['static_page']->value) {
$_smarty_tpl->tpl_vars['static_page']->iteration++;
$_smarty_tpl->tpl_vars['static_page']->last = $_smarty_tpl->tpl_vars['static_page']->iteration == $_smarty_tpl->tpl_vars['static_page']->total;
$__foreach_static_page_0_saved = $_smarty_tpl->tpl_vars['static_page'];
?>
          <a href="/static/<?php echo $_smarty_tpl->tpl_vars['static_page']->value['page_url'];?>
">
            <?php echo __(((string)$_smarty_tpl->tpl_vars['static_page']->value['page_title']));?>

          </a><?php if (!$_smarty_tpl->tpl_vars['static_page']->last) {?> · <?php }?>
        <?php
$_smarty_tpl->tpl_vars['static_page'] = $__foreach_static_page_0_saved;
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['system']->value['contact_enabled']) {?>
         · 
        <a href="/contacts"><?php echo __("Contacts");?>
</a>
      <?php }?>
       · 
        <a href="/message_to_miles"><?php echo __("Guo");?>
</a>
      
      <?php if ($_smarty_tpl->tpl_vars['system']->value['market_enabled']) {?>
                 · 
                <a href="/market"><?php echo __("Market");?>
</a>
            <?php }?>
    </div>
  </div>
</div>
             </div>
<!-- footer -->
                     </div>
                     
                      </div>
                  
                  </div>
                
              </div>
           </div>
    </div>
<div class="overlay"></div>
<?php $_smarty_tpl->_subTemplateRender('file:_footer_login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- page content -->
<?php echo '<script'; ?>
 type="text/javascript">
jQuery(document).ready(function(){
        updateLiveVideo();
        /*setInterval(function(){
            updateLiveVideo();
        },8000)*/
    });
    
    jQuery(".autominheight").css("min-height",jQuery(window).height()-(jQuery(".main-header").height()+jQuery(".footer").height() + 90));
    
    jQuery(document).ready(function(){
    setTimeout(function(){
     jQuery('body').find("div.live_video").slice(0, 3).show();        
    },2000);
    jQuery(function () {
    jQuery('body').on('click','div#loadMore',function (e) {
        e.preventDefault();
        jQuery(".live_video:hidden").slice(0, 3).slideDown();
        if (jQuery(".live_video:hidden").length == 0) {
            jQuery("#load").fadeOut('slow');
        }
    });
    
});
});
function updateLiveVideo(){
  var settings = {
      "async": true,
      "url": "/list.php",
      "method": "POST",
      "headers": {
        "accept": "application/json",
        "content-type": "application/json"
      }
    };
    //loaded block
    $( ".boss" ).append( "<div class='loader'><span><?php echo __('Miles Guo’s Videos');?>
</span></div>" );
    $('.loader').show();
    $( ".others" ).append( "<div class='loader'><span><?php echo __('Other’s Videos');?>
</span></div>" );
    $('.loader').show();
    jQuery.ajax(settings).done(function (res) {
        
        var bossArray = res.boss;
        var othersArray = res.others;
        
        var player = Array();
        if(bossArray.length > 0) {
            jQuery(".boss").append("<div class='post_name'><?php echo __('Miles Guo’s Videos');?>
</div>");
            for(i=0;i<bossArray.length;i++) {
                jQuery(".boss").append('<div class="live_video"><div class="col-sm-4 video_broadcas="video-js vjs-default-skin" preload="auto" width="300px"st col-xs-12"><video id="video_'+bossArray[i].id+'" clas height="150px"><source src="'+bossArray[i].broadcast_url+'" type="application/x-mpegURL"></video> </div><div class="description col-sm-7 col-xs-12"><div class="video-title"><img src="/content/themes/default/images/live.png" style="width:30px;height: auto;margin-right: 5px;padding-bottom: 5px;"><a href="/video_post?post_id='+bossArray[i].post_id+'" target="_parent">'+bossArray[i].broadcast_name+'</a></div><div class="video-view"><a href="/'+bossArray[i].user_name+'">'+bossArray[i].name+'</a> <span class="dotclass">.</span>'+bossArray[i].views+' <?php echo __("Views");?>
</div><div class="video-des">'+bossArray[i].broadcast_detail+'</div></div></div>');
                     player[i] = videojs('video_'+bossArray[i].id);
                     player[i].play();
                     player[i].volume(0);
                 if(i==0) {
                    $('.loader').hide();
                 }    
            }
            if(bossArray.length > 5) {
             jQuery(".others").append("<div id='loadMore'><?php echo __('Show More');?>
</div>");
            }
            setTimeout(function(){
                 for(i=0;i<bossArray.length;i++) {
                      player[i].pause();
                 }
            },4000);
            
        } else {
            //loaded none
            jQuery(".boss").css('display','block');
            jQuery(".boss").html('<?php echo __("Currently there is no available livestream from Guo.");?>
');
        }
        
        var other_player = Array();;
        if(othersArray.length > 0) {
            jQuery(".others").append("<div class='post_name'><?php echo __('Other’s Videos');?>
</div>");
             for(i=0;i<othersArray.length;i++) {
                 var videototal = othersArray.length; 
                jQuery(".others").append('<div class="live_video"><div class="col-sm-4 video_broadcast col-xs-12"><video id="video_other_'+othersArray[i].id+'" class="video-js vjs-default-skin" preload="auto" width="300px" height="150px"><source src="'+othersArray[i].broadcast_url+'" type="application/x-mpegURL"></video></div><div class="description col-sm-7 col-xs-12"><div class="video-title"><img src="/content/themes/default/images/live.png" style="width:30px; height: auto;margin-right: 5px;padding-bottom: 5px;"><a href="/video_post?post_id='+othersArray[i].post_id+'" target="_parent">'+othersArray[i].broadcast_name+'</a></div><div class="video-view"><a href="/'+othersArray[i].user_name+'">'+othersArray[i].name+'</a> <span class="dotclass">.</span>'+othersArray[i].views+' <?php echo __("Views");?>
</div><div class="video-des">'+othersArray[i].broadcast_detail+'</div></div></div>');
                    other_player[i] = videojs('video_other_'+othersArray[i].id);
                    other_player[i].play();
                    other_player[i].volume(0);
                if(i==0) {
                    $('.loader').hide();
                 } 
            }
            if(othersArray.length > 5) {
             jQuery(".others").append("<div id='loadMore'><?php echo __("Show More");?>
</div>");
            }
            setTimeout(function(){
                 for(i=0;i<othersArray.length;i++) {
                      other_player[i].pause();
                 }
            },8000);
        } else {
             $('.loader').hide();
            jQuery(".others").css('display','block');
            jQuery(".others").html('<?php echo __("Currently there is no available livestream from others.");?>
');
        }
    });  
}
<?php echo '</script'; ?>
><?php }
}
