<style>
li.take-photo {
    background: #5a656b;
    border-radius: 5px;
    margin: 6px 8px 0 0;
}
li.take-video {
    background: #5a656b;
    border-radius: 5px;
    margin: 6px 0 0 0;
}
.take-photo a.take-photo {
    padding: 3px 5px;
    display: block;
    border-radius: 21px !important;
    color: #fff;
    margin: 0 0 0;
	font-size: 12px;
}
.take-video a.take-video {
    padding: 3px 5px;
    display: block;
    border-radius: 21px !important;
    color: #fff;
    margin: 0 0 0;
	font-size: 12px;
}
@media (min-width: 768px){
.main-header .navbar-form .form-control {
    height: 36px;
    line-height: 20px;
    padding-bottom: 5px;
    padding-top: 5px;
    padding-right: 35px;
    border-radius: 21px;
    background: #fafafa;
    color: #5a656b;
    font-size: 12px;
}
.nav-search {
    position: absolute;
    border: none;
    right: 4px;
    top: 19px;
    outline: none !important;
}
.inner_link .top-menues i{
    margin: 12px 11px 0 0 !important;
    float: left;
    line-height: 22px;
	width:24px;
	height:24px;
}
.inner_link .top-menues .hand_icon {
    top: auto;
    position: static;
}
.main-header .header-right .nav{
	margin:6px 0 0 0;
}

.navbar-container span.text-link.dev {
    border: 1px solid #e6ecf0;
    line-height: 22px;
    padding-bottom: 5px;
    padding-top: 5px;
    /* padding-right: 35px; */
    border-radius: 21px;
    background: #fafafa;
    color: #5a656b;
    font-size: 12px !important;
    text-align: center !important;
    padding: 7px 15px;
    margin: 12px 2px 0 5px;
    color: #7697b7;
}

}
@media screen and (max-width:767px){
.top-menues.support-popup i {    
	 width: 21px;
    height: 21px;
    display: inline-block;
    background-position: 0 0;
    background-size: 100%;
    margin: 3px 0 0 4px;
}

ul.nav.navbar-nav {
    float: left;
    width: auto;
}
.navbar-container span.text-link.dev.menu_bfr_login{
  margin: -1px 0 0 7px !important;
}
.mobile-menu span.text-link.dev.pull-right {
	margin: -4px 0 0 7px !important;
}
.navbar-container span.text-link.dev {
    border: 1px solid #e6ecf0;
    line-height: 22px;
    padding-bottom: 5px;
    padding-top: 5px;
    /* padding-right: 35px; */
    border-radius: 21px;
    background: #fafafa;
    color: #5a656b;
    font-size: 13px !important;
    text-align: center !important;
    padding: 5px 15px;
    margin: 5px 2px 0 5px !important;
    color: #7697b7;
    float: right;
}
.top-menues.support-popup img {
	width:100%;
	background: #5a656b;
}
li.take-photo, li.take-video{
	margin-top:2px;
}
}
@supports (-webkit-overflow-scrolling: touch) {
.modal-dialog{
	transform: none !important;
}
}
</style>
	<script type="text/javascript">
      $(document).ready(function(){

          if( $(window).width() > 767){
            $('#desktop-live-msg-id').on('click', function(){

                $('#mobile-live-direct-msgid').css('display','block !important');

                $('#mobile-live-direct-msgid').removeClass('visible-xs');

                $('#mobile-live-direct-msgid > ul.navbar-nav > li').css('display','none');

                $('#myModal').modal('show');

            });
          }else{
               //$('#mobile-live-direct-msgid').addClass('visible-xs');
               //$('#mobile-live-direct-msgid > ul.navbar-nav > li').css('display','block');
          }
      });
    </script>

{if !$user->_logged_in}
    <!--<div class="send-user-message" data-toggle="modal" data-url="posts/compose.php?do=create">
        <div class="add">
            <i class="fa fa-plus-circle"></i>
        </div>
    </div>
    <button class="send-user-message">Send Message</button>-->
    <body data-hash-tok="{$session_hash['token']}" data-hash-pos="{$session_hash['position']}" class="visitor n_chat">
{else}
    <body data-hash-tok="{$session_hash['token']}" data-hash-pos="{$session_hash['position']}" data-chat-enabled="{$user->_data['user_chat_enabled']}" class="{if !$system['chat_enabled']}n_chat{/if}{if $system['activation_enabled'] && !$user->_data['user_activated']} n_activated{/if}{if !$system['system_live']} n_live{/if}">

	<!--<button class="send-user-message">Send Message</button>-->
    <div class="send-user-message" data-toggle="modal" data-url="posts/compose.php?do=create">
        <div class="add">
            <i class="fa fa-plus-circle"></i>
        </div>
    </div>
    <input type="hidden" id="currnt_post_id" value="">
{/if}
    <!-- main wrapper -->
    <div class="main-wrapper">
        <div class="search_field">
            <form id="header-search">
                <input id="search-input" type="text" class="form-control" placeholder='{__("Search for everything")}' required autocomplete="off">
                <button type="submit" class="Icon Icon--medium Icon--search nav-search" tabindex="-1"></button>

                <div id="search-results" class="dropdown-menu dropdown-widget dropdown-search js_dropdown-keepopen">
                    <div class="dropdown-widget-header">
                        {__("Search Results")}
                    </div>
                    <div class="dropdown-widget-body">
                        <div class="loader loader_small ptb10"></div>
                    </div>
                    <a class="dropdown-widget-footer" id="search-results-all" href="/search/">{__("See All Results")}</a>
                </div>
                {if $user->_logged_in && count($user->_data['search_log']) > 0}
                    <div id="search-history" class="dropdown-menu dropdown-widget dropdown-search js_dropdown-keepopen">
                        <div class="dropdown-widget-header">
                            <span class="text-link pull-right flip js_clear-searches">
                                {__("Clear")}
                            </span>
                            <i class="fa fa-clock-o"></i> {__("Recent Searches")}
                        </div>
                        <div class="dropdown-widget-body">
                            {include file='ajax.search.tpl' results=$user->_data['search_log']}
                        </div>
                        <a class="dropdown-widget-footer" id="search-results-all" href="/search/">{__("Advanced Search")}</a>
                    </div>
                {/if}
            </form>
        </div>
        {if $user->_logged_in}
            <div class="sticky_header_second">
                <ul>
                    <li><a {if $last_dir == 'home'} class="active" {/if}  href="/"><i class="Icon Icon--home Icon--large"></i></a></li>
                    <li><a id="search_toggle" href="javascript:void(0)"><i class="Icon Icon--medium Icon--search"></i></a></li>
                    <li>
                        <a href="/notifications" {if $last_dir == 'notifications' || $last_dir == 'mentions'} class="active" {/if}>
                            <span class="label {if $user->_data['user_live_notifications_counter'] == 0}hidden{/if}">
                                {$user->_data['user_live_notifications_counter']}
                            </span>
                            <i class="Icon Icon--notifications Icon--large"></i>
                        </a>
                    </li>
                    <li class="dropdown js_live-messages top-menues" id="mobile-live-msg-id">
                        <a class="left-nav-channel-title title-messaging"><i class="Icon Icon--dm Icon--large"></i></a>
                        <!-- {if $user->_logged_in}
                            <div class="dropdown-menu dropdown-widget with-arrow live-chat-massage">
                                <div class="dropdown-widget-header">
                                    {__("Messages")}
                                    <a class="pull-right flip text-link js_chat-start" href="/messages/new">{__("Send a New Message")}</a>
                                </div>
                                <div class="dropdown-widget-body">
                                <div class="js_scroller">
                                    {if count($user->_data['conversations']) > 0}
                                        <ul>
                                        {foreach $user->_data['conversations'] as $conversation}
                                        {include file='__feeds_conversation.tpl'}
                                        {/foreach}
                                        </ul>
                                    {else}
                                        <p class="text-center text-muted mt10">
                                            {__("No messages")}
                                        </p>
                                    {/if}
                                </div>
                            </div>
                            <a class="dropdown-widget-footer" href="/messages">{__("See All")}</a>
                            </div>
                        {/if}-->
                    </li>
					{if $user->_logged_in && !$user->_is_admin}
						<li class="top-menues support-popup">
							<a href="javascript:void(0)" title="Support">
								<!--<i class="Icon Icon--info Icon--large"></i>     -->
								<i class="">
								  <img src="/content/themes/default/images/technical-support.png?v={$system['FileMicroTime']}" alt="Support" >
								</i>
							</a>
						</li>
					 {/if}

					 <li class="top-menues">
						<a href="javascript:void(0)" title="Info">
							<i class="Icon Icon--info Icon--large"></i>
						</a>
					</li>
                    <li><!--a href="/broadcasts" {if $last_dir == 'broadcasts'} class="active" {/if}><i class="Icon Icon--cameraVideo Icon--large"></i></a-->
                    <div class="dropdown custom-dropdown-btn">
                         <a href="/broadcasts" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown">
                            <i class="Icon Icon--cameraVideo Icon--large mobile-broadcast"></i></a>
                         <div class="dropdown-menu menu-custom-button" aria-labelledby="dropdownMenuButton">
                           <a class="dropdown-item" href="/broadcasts">{__("Watch Broadcasts")}</a>

                         </div>
                       </div>
                    </li>
                </ul>
            </div>
        {/if}
        {if $user->_logged_in && $system['activation_enabled'] && !$user->_data['user_activated']}
            <!-- top-bar -->
            <div class="top-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 hidden-xs">
                            {if $system['activation_type'] == "email"}
                                {__("Please go to")} <span class="text-primary">{$user->_data['user_email']}</span> {__("to complete the sign-up process")}.
                            {else}
                                {__("Please check the SMS on your phone")} <strong>{$user->_data['user_phone']}</strong> {__("to complete the sign-up process")}.
                            {/if}
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            {if $system['activation_type'] == "email"}
                                <span class="text-link" data-toggle="modal" data-url="core/activation_email_resend.php">
                                    {__("Resend Activation Email")}
                                </span>
                                 -
                                <span class="text-link" data-toggle="modal" data-url="#activation-email-reset">
                                    {__("Change Email")}
                                </span>
                            {else}
                                <span class="btn btn-info btn-sm mr10" data-toggle="modal" data-url="#activation-phone">{__("Enter Code")}</span>
                                {if $user->_data['user_phone']}
                                    <span class="text-link" data-toggle="modal" data-url="core/activation_phone_resend.php">
                                        {__("Resend SMS")}
                                    </span>
                                     -
                                {/if}
                                <span class="text-link" data-toggle="modal" data-url="#activation-phone-reset">
                                    {__("Change Phone Number")}
                                </span>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
            <!-- top-bar -->
        {/if}

        {if !$system['system_live']}
            <!-- top-bar alert-->
            <div class="top-bar alert-bar">
                <div class="container">
                    <i class="fa fa-exclamation-triangle fa-lg pr5"></i>
                    <span class="hidden-xs">{__("The system has been shutted down")}.</span>
                    <span>{__("Turn it on from")}</span> <a href="/admincp/settings">{__("Admin Panel")}</a>
                </div>
            </div>
            <!-- top-bar alert-->
        {/if}

        <div class="main-header">
            <div class="container header-container">
                  {include file='_header.messages.tpl'}
                <!-- navbar-container -->
                <div class="navbar-container pull-left">
                  <!-- messages -->

                  <!-- messages -->

                   <div class="mobile-menu visible-xs" id="mobile-live-direct-msgid">
                    <ul class="nav navbar-nav">
                        {if $user->_logged_in}
                          {assign var="dirs" value="/"|explode:$smarty.server.REQUEST_URI}

                              <!-- home -->
                              <li class="top-menues">
                                  <a href="/" {if {$dirs[$dirs|@count-1]} == ''}class="active"{/if}>
                                      <i class="Icon Icon--home Icon--large"></i>
                                      <span>{__("Home")}</span>
                                  </a>
                              </li>
                              <!-- home -->

                              <!-- friend requests -->
                              {include file='_header.friend_requests.tpl'}
                              <!-- friend requests -->

                              <!-- notifications -->
                              {include file='_header.notifications.tpl'}
                              <!-- notifications -->

                              <!-- messages -->

                              <!-- messages -->

                              <!-- search -->
                              <li class="visible-xs-block top-menues">
                                  <a href="/search">
                                      <i class="fa fa-search fa-lg"></i>
                                  </a>
                              </li>

                              <!-- search -->
                        {else}
                          <a class="mobile-signin" href="/signin">
                            <i class="Icon Icon--home Icon--large"></i>
                            <span class="text-link dev pull-right">
                              {__("Sign in")}
                            </span>
                          </a>
                        {/if}

                    </ul>
                    </div>

                </div>
                <!-- navbar-container -->

                    <div class="brand-container  {if $user->_logged_in}user_logout_brand{/if}">
                        <!-- brand -->
                        <a href="/" class="brand">
                            {if SYSTEM_LOGO}
                                <img width="60" src="{SYSTEM_LOGO}" alt="{$system['system_title']}" title="{$system['system_title']}">
                            {else}
                                {$system['system_title']}
                            {/if}
                        </a>
                        <!-- brand -->
                    </div>

                    <div class="inner_link">
                      <ul>
                        <li>


                          {if $user->_logged_in}
                           {if $system['REQUEST_URI'] == '/'}
								<a href="/" class="top-nav-active">
								  <i class="Icon Icon--home Icon--large"></i>
								  {__("Home")}
								</a>
							   {else}
							    <a href="/">
								  <i class="Icon Icon--home Icon--large"></i>
								  {__("Home")}
								</a>
								{/if}
                          {else}
                            <a href="/signin">
                              <i class="Icon Icon--home Icon--large"></i>
                              {__("Sign in")}
                            </a>
                          {/if}


                        </li>
                        <li><!--Link to Guo's page -->
                          {if $system['REQUEST_URI'] == '/milesguo'}							
							  <a href="/milesguo" class="top-nav-active">
								<i class="hand_icon">
								  <img src="/content/themes/default/images/hand.png?v={$system['FileMicroTime']}" alt="hand" width="19">
								</i>
								{__("Guo")}
							  </a>
						   {else}
							<a href="/milesguo">
								<i class="hand_icon">
								  <img src="/content/themes/default/images/hand.png?v={$system['FileMicroTime']}" alt="hand" width="19">
								</i>
								{__("Guo")}
							  </a>
							{/if}
                        </li>

						 <!-- Support -->
						 {if $user->_logged_in && !$user->_is_admin}
                            <li class="top-menues support-popup">
                                <a href="javascript:void(0)">
                                    <i class="">
									  <img src="/content/themes/default/images/technical-support.png?v={$system['FileMicroTime']}" alt="" width="21">
									</i>
									{__("Support")}
                                </a>
                            </li>

							<li class="dropdown js_live-messages top-menues" id="desktop-live-msg-id">
							<a style="cursor: pointer;"  class="left-nav-channel-title title-messaging"><i class="Icon Icon--dm Icon--large"></i>{__("Messages")}</a></li>
						 {/if}
                            <!-- Support -->
                     </ul>
                   </div>



               <!-- navbar-collapse -->
                <div class="header-right {if $user->_logged_in} mobile_user_head {/if}">
                    {if $user->_logged_in}
                        <!-- search -->
                        {include file='_header.search.tpl'}
                        <!-- search -->
                    {/if}
                    <!-- navbar-container -->
                    <div class="navbar-container">
                       <div class="mobile-menu">
                        <ul class="nav navbar-nav">
                            {if $user->_logged_in}

                                <!-- search -->

                                <!-- user-menu -->
                                <li class="dropdown">
                                    <a class="dropdown-toggle user-menu">
                                        <img src="{$user->_data['user_picture']}" alt="">
                                    </a>
                                    <ul class="dropdown-menu">
                                    <span class="caret-outer"></span>
                                    <span class="caret-inner"></span>
                                     <li><div class="user_name_id">

                                     <b class="fullname"><a href="/{$user->_data['user_name']}">{$user->_data['user_firstname']} {$user->_data['user_lastname']}</a></b>
                                     <p class="name"><span class="username u-dir" dir="ltr"><a href="/{$user->_data['user_name']}">@{$user->_data['user_name']}</a></span></p>
                                     </div>
                                     </li>
                                     <li class="divider"></li>
                                        <li>
                                            <a href="/{$user->_data['user_name']}"><span class="DashUserDropdown-linkIcon Icon Icon--medium Icon--me"></span>{__("Profile")}</a>
                                        </li>
                                        <li>
                                            <a href="/settings/privacy"><span class="DashUserDropdown-linkIcon Icon Icon--medium Icon--promotedStroked"></span>{__("Privacy")}</a>
                                        </li>
                                        <li>
                                            <a href="/settings"><span class="DashUserDropdown-linkIcon Icon Icon--medium Icon--cog"></span>{__("Settings")}</a>
                                        </li>
                                        {if $user->_is_admin}
                                            <li class="divider"></li>
                                            <li>
                                                <a href="/admincp">{__("Admin Panel")}</a>
                                            </li>
                                        {/if}
                                        <li class="divider"></li>
                                        <li>
                                            <a href="/signout">{__("Log Out")}</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="visible-xs-block">
                                    <a href="#" data-toggle="offcanvas" data-target=".sidebar-nav">
                                        <i class="fa fa-bars fa-lg"></i>
                                    </a>
                                </li>
                                <!-- user-menu -->
                            {/if}
                        </ul>
                        {if !$user->_logged_in}
                        <span class="text-link dev menu_bfr_login translate_button" data-toggle="modal" data-url="#translator">
                        {else}
                        <span class="text-link dev menu_aftr_login translate_button" data-toggle="modal" data-url="#translator">
                        {/if}

						 {$system['language']['title']}</span>
             {if !$user->_logged_in}
						 <!--<span class="contact-icon watch_broadcasts">
                            <a href="/broadcasts">
                                  <span class="Icon icon_custom Icon--cameraVideo Icon--small"></span>{__("GuoTV")}
                            </a>
                        </span>


                        <a href="/broadcasts" {if $last_dir == 'broadcasts'} class="active" {/if}><span class="broadcasts_icon Icon Icon--cameraVideo Icon--small"></span></a>-->
						<a href="/broadcasts"  class="active"><span style="font-size:23px !important;" class="broadcasts_icon Icon Icon--cameraVideo Icon--large"></span></a>
						<span class="contact-icon">
							<div class="dropdown">
                                {if $user->_broadcast_status}
								<span id="blink-tv">
									<a href="/broadcasts" class="btn btn-secondary dropdown-toggle blink-custom" type="button" id="dropdownMenuButton">
										<span class="Icon icon_custom Icon--cameraVideo Icon--small "></span>{__("GuoTV")}
									</a>
								</span>
								{else}
								<span id="blink-tv">
									 <a href="/broadcasts" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" id="blink_tv">
										<span class="Icon icon_custom Icon--cameraVideo Icon--small "></span>{__("GuoTV")}
									</a>
								</span>
								{/if}

								<div class="dropdown-menu menu-custom-button" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="/broadcasts">{__("Broadcasts")}</a>

								</div>
                            </div>
						</span>
                        {/if}
                        {if $user->_logged_in}
                         <span class="contact-icon">
                            <div class="dropdown">
							{if $user->_broadcast_status}
							<span id="blink-tv">
                                <a href="/broadcasts" class="btn btn-secondary dropdown-toggle blink-custom" type="button" id="dropdownMenuButton">
                                    <span class="Icon icon_custom Icon--cameraVideo Icon--small "></span>{__("GuoTV")}
                                </a>
							</span>
							{else}
							<span id="blink-tv">
								 <a href="/broadcasts" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" id="blink_tv">
                                    <span class="Icon icon_custom Icon--cameraVideo Icon--small "></span>{__("GuoTV")}
                                </a>
							</span>
							{/if}
                                <!--<div class="dropdown-menu menu-custom-button" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="/broadcasts">{__("Watch Broadcasts")}</a>
                                    <a class="dropdown-item" data-toggle="modal" data-target="#do_broad" href="javascript:void(0)">{__("Start Broadcast")}</a>
                                </div>-->

								<div class="dropdown-menu menu-custom-button" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="/broadcasts">{__("Broadcasts")}</a>

								{if ($user->_data['user_name'] == "milesguo" || $user->_data['user_name'] == "MilesGuo") || ($user->_data['user_name'] == "test17" || $user->_data['user_name'] == "Test17")}
									{if !$user->_broadcast_status}

									{else}
										<span id="broad_start_stop"><button type="button" id="flash-button" class="flash-button-stop" onclick="BroadCastStop()">Stop Broadcast</button></span>
									{/if}
								{/if}
								</div>
                            </div>
                        </span>
                         {/if}
						</div>

                    </div>
                    <!-- navbar-container -->
                </div>
                <!-- navbar-collapse -->

            </div>
        </div>

        <!-- ads -->
        {include file='_ads.tpl' _ads=$ads_master['header'] _master=true}
        <!-- ads -->
