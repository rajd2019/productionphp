<ul>
    {foreach $users as $_user}
    <li>
      {if $_user['user_firstname'] == '' || $_user['user_lastname']==''}

      <div class="data-container clickable small js_tag-add" data-uid="{$_user['user_id']}" data-name="{$_user['user_name']}">

      {else}
      <div class="data-container clickable small js_tag-add" data-uid="{$_user['user_id']}" data-name="{$_user['user_firstname']} {$_user['user_lastname']}">
        
      {/if}
          <img class="data-avatar" src="{$_user['user_picture']}" alt="{$_user['user_firstname']} {$_user['user_lastname']}">
            <div class="data-content">
                <div><strong>
                  {if $_user['user_firstname'] == '' || $_user['user_lastname']==''}
                  {$_user['user_name']}
                  {else}
                  {$_user['user_firstname']} {$_user['user_lastname']}
                  {/if}
                </strong>
              </div>
            </div>
        </div>
    </li>
    {/foreach}
</ul>