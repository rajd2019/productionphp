<div class="modal-body plr0 ptb0">

    <div class="x-form publisher mini">



       

		<div class="panel-heading clearfix">

			<button type="button" class="close mr10 pull-right flip" data-dismiss="modal" aria-hidden="true">×</button>	

			<div class="pull-right flip"> 

				<a class="btn btn-primary  " data-toggle="modal" data-url="chat/user_search.php">  <i class="fa fa-comment-o"></i>

					{__("New Message")}

				</a> 

				 

			</div> 

			<div class="mt5"> <span title="Alex, Ajay">{__("Direct Messages")}</span>  </div> 

		</div>

		

       <div class="js_scroller panel panel-default">

                {if count($user->_data['conversations']) > 0}

                    <ul>

                        {foreach $user->_data['conversations'] as $conversation}

                        {include file='__feeds_conversation.tpl'}

                        {/foreach}

                    </ul>

                {else}

                    <div class="modal-body custom-start-conversations">

                       <h4> {__("Send a message, get a message")}</h4>

						<p>{__("Direct Messages are private conversations between you and other people on Twitter. Share Tweets, media, and more!")}</p>

						<a class="btn btn-primary  " data-toggle="modal" data-url="chat/user_search.php">  {__("Start a conversation")}</a>

                    </div>

                {/if}

            </div>

      



       

    </div>

</div>