{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="search-result-heading">
    <div class="container">
        {$query}
    </div>
</div>


<div class="container mt20 offcanvas">
    <div class="row">
    
        
        <div class="col-sm-11 col-md-12 offcanvas-mainbar">
            <div class="row">
                <!-- left panel -->
                <div class="col-sm-12">
                    <!-- search form -->
                    
                    <!-- search form -->


                    {if $query}
                    <!-- search results -->
                     <div class="panel-body tab-content">
                        <div class="tab-pane {if $follow == ''} active {/if}" id="top">
                            
                            {if count($results['limit_user']) > 0}
                            <div class="custom-view"><p class="mt10 mb10"><strong>{__("People")}</strong></p>
                            {if count($results['users']) > 8}
                            <span class="people-view-all"><a href="#users" data-toggle="tab">{__("View All")}</a></span>
                            {/if}
                            </div>
                                
                                <div class="row">
                                    {foreach $results['limit_user'] as $k=>$_user}
                                    <div class="col-sm-3">
                                     <div class="site_bar_new custom-site-bar-new">
                                         <div class="profile-bg-thumb"> 
                                        {if $_user['user_cover']}
                                            <img src="{$system['system_uploads']}/{$_user['user_cover']}">
                                        {/if}
                                        </div>
                                        <div class="side_profile">
                                        <div class="pro_thumb"> <a href="/{$_user['user_name']}{if $_search}?ref=qs{/if}"><img src="{$_user['user_picture']}" alt="{$_user['user_firstname']} {$_user['user_lastname']}"></a></div>
                                        
                                        <div class="admin_detail">
                                        <span> 
                                        
                                        {if $results['custom_login_user_id'] != $_user['user_id'] }
                                        
                                        {if $_user['following'] == 0}
                                            <a class="js_follow follow_btn 1122{$_user['following']}" data-uid="{$_user['user_id']}" href="#">{__("Follow")}</a>
                                        {else}  
                                            <a class="btn btn-default js_unfollow" data-uid="{$_user['user_id']}" href="#"><i class="fa fa-check"></i>{__("Following")}</a>
                                        {/if}
                                        
                                        {/if}
                                        
                                        </span>
                                        <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                                            <a href="/{$_user['user_name']}{if $_search}?ref=qs{/if}"><span>{$_user['user_firstname']}{$_user['user_lastname']} </span> <span class="user-at-btn">@{$_user['user_name']}</span></a>
                                        </span>
                                        
                                        
                                        </div>
                                        <p>{$_user['user_biography']}</p>
                                        </div>
                                    </div>
                                    </div>
                                        {if ($k+1)%4 == 0}
                                            <div class="clearfix"></div>
                                        {/if}
                                 {/foreach}
                                </div>

                                {/if}

                               
                                
                               
                                
                                
                                
                                
                                
                                
                                
                                
                              
                                
                        </div>


                        

                        <div class="tab-pane active" id="users">
                            
                                
                                <div class="row">
                                {if count($results['users']) > 8 }
                                {foreach $results['users'] as $_user}
                                    <div class="col-sm-3">
                                     <div class="site_bar_new custom-site-bar-new">
                                        <div class="profile-bg-thumb"> 
                                        {if $_user['user_cover']}
                                            <img src="{$system['system_uploads']}/{$_user['user_cover']}">
                                        {/if}
                                        </div>
                                        <div class="side_profile">
                                        <div class="pro_thumb"> <a href="/{$_user['user_name']}{if $_search}?ref=qs{/if}"><img src="{$_user['user_picture']}" alt="{$_user['user_firstname']} {$_user['user_lastname']}"></a></div>
                                        
                                        <div class="admin_detail">
                                        <span> 
                                        {if $results['custom_login_user_id'] != $_user['user_id'] }
                                        
                                        {if $_user['following'] == 0}
                                            <a class="js_follow follow_btn 22" data-uid="{$_user['user_id']}" href="#">{__("Follow")}</a>
                                        {else}  
                                            <a class="btn btn-default js_unfollow" data-uid="{$_user['user_id']}" href="#"><i class="fa fa-check"></i>{__("Following")}</a>
                                        {/if}
                                         {/if}
                                        
                                        </span>
                                        <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                                            <a href="/{$_user['user_name']}{if $_search}?ref=qs{/if}"><span>{$_user['user_firstname']}{$_user['user_lastname']} </span> <span class="user-at-btn">@{$_user['user_name']}</span></a>
                                        </span>
                                        
                                        
                                        </div>
                                        <p>{$_user['user_biography']}</p>
                                        </div>
                                    </div>
                                    </div>
                                    {/foreach}
                                </div>
                                
                                {else}
                               {* <div class="text-center x-muted mtb10">
                                    <p class="mt10 mb10"><strong>{__("No results to show")}</strong></p>
                                </div>*}
                                {/if}
                        </div>
                            
                            
                           
                            
                           
                    <!-- search results -->
                    {/if}
                </div>
                <!-- left panel -->

                <!-- right panel -->
                <div class="col-sm-4">
                    {include file='_ads.tpl'}
                    {include file='_widget.tpl'}
                </div>
                <!-- right panel -->
            </div>
        </div>
        

        
    </div>

<!-- page content -->

{include file='_footer.tpl'}