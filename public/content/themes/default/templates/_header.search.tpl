<style>
@media (min-width: 768px){
.main-header .navbar-form .form-control {
    height: 37px;
}
.nav-search {
    top: 20px;
}

.main-header .navbar-form {
    width: 220px;
    height: 46px;
    padding: 5px 0 0;
}
}
</style>
<form class="navbar-form pull-left flip hidden-xs top-menues">

    <input id="search-input" type="text" class="form-control" placeholder='{__("Search for everything")}' required="required" autocomplete="off">

    <button type="submit" class="Icon Icon--medium Icon--search nav-search" tabindex="-1"></button>
	<input id="search-input-value" type="hidden" value="">

    <div id="search-results" class="dropdown-menu dropdown-widget dropdown-search js_dropdown-keepopen">

        <div class="dropdown-widget-header">

            {__("Search Results")}

        </div>

        <div class="dropdown-widget-body">

            <div class="loader loader_small ptb10"></div>

        </div>

        <a class="dropdown-widget-footer" id="search-results-all" style="cursor:pointer;">{__("See All Results")}</a>

    </div>

    {if $user->_logged_in && count($user->_data['search_log']) > 0}

        <div id="search-history" class="dropdown-menu dropdown-widget dropdown-search js_dropdown-keepopen">

            <div class="dropdown-widget-header">

                <span class="text-link pull-right flip js_clear-searches">

                    {__("Clear")}

                </span>

                <i class="fa fa-clock-o"></i> {__("Recent Searches")}

            </div>

            <div class="dropdown-widget-body">

                {include file='ajax.search.tpl' results=$user->_data['search_log']}

            </div>

            <a class="dropdown-widget-footer" id="search-results-all" href="/search/">{__("Advanced Search")}</a>

        </div>

    {/if}

</form>