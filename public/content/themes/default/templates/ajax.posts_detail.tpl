<div class="modal-body post-hover-content" >
    <div class="js_scroller_popup" id="js_scroller_popup" onscroll="viewMoreComments(this)">
		{include file='__feeds_post_hover.tpl' standalone=true}
	</div>
</div>

<script>

function viewMoreComments(o) {
    var busy= false;
    if(o.offsetHeight + o.scrollTop == o.scrollHeight && (!busy)){

      if($( "#more-comments-id" ).length == 1){
          load_more_comments($('.js_see-more-comments'));
          busy = true;
      }
    }
}
</script>
