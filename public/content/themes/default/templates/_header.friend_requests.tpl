<li class="dropdown js_live-requests top-menues">

    <div class="dropdown-menu dropdown-widget with-arrow js_dropdown-keepopen">
        <div class="dropdown-widget-header">
            {__("Friend Requests")}
        </div>
        <div class="dropdown-widget-body">
            <div class="js_scroller">
                {if count($user->_data['friend_requests']) > 0}
                    <ul>
                        {foreach $user->_data['friend_requests'] as $_user}
                        {include file='__feeds_user.tpl' _connection="request"}
                        {/foreach}
                    </ul>
                {else}
                    <p class="text-center text-muted mt10">
                        {__("No new requests")}
                    </p>
                {/if}
                
                <!-- People You May Know -->
                <div class="title">
                    {__("Who to follow")}
                </div>
                
                {if count($user->_data['new_people']) > 0}
                    <ul>
                        {foreach $user->_data['new_people'] as $_user}
                        {include file='__feeds_user.tpl' _connection="add"}
                        {/foreach}
                    </ul>
                {else}
                    <p class="text-center text-muted mt10">
                        {__("No people available")}
                    </p>
                {/if}
                <!-- People You May Know -->
            </div>
        </div>
        <a class="dropdown-widget-footer" href="/people/friend_requests">{__("See All")}</a>
    </div>
</li>