<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $sub_view == "edit"}
            <div class="pull-right flip">
                <a target="_blank" href="/{$data['user_name']}" class="btn btn-info">
                    <i class="fa fa-television fa-fw mr5"></i>{__("View Profile")}
                </a>
            </div>
        {elseif $sub_view == "banned"}
            <div class="pull-right flip">
                <a href="/admincp/banned_ips" class="btn btn-danger">
                    <i class="fa fa-user-times"></i> {__("Manage Banned IPs")}
                </a>
            </div>
        {/if}
        <i class="fa fa-user pr5 panel-icon"></i>
        <strong>{__("Support Query List")}</strong>
        {if $sub_view != "" && $sub_view != "edit"} &rsaquo; <strong>{__($sub_view|capitalize)}</strong>{/if}
        {if $sub_view == "edit"} &rsaquo; <strong>{$data['user_firstname']} {$data['user_lastname']}</strong>{/if}
    </div>
		{if $sub_view == ""}
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>{__("ID")}</th>
                            <th>{__("Name")}</th>
                            <th>{__("Username")}</th>
                            <th>{__("Subject")}</th>
                            <th>{__("Message")}</th>
                            <th>{__("Reply")}</th>
							<th>{__("Date")}</th>
							<th>{__("Actions")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $rows as $row}
                        <tr>
                            <td width="5%"><a href="/{$row['user_name']}" target="_blank">{$row['user_id']}</a></td>
                            <td width="15%">
                                <a target="_blank" href="/{$row['user_name']}">
                                   
                                    {$row['user_firstname']} {$row['user_lastname']}
                                </a>
                            </td>
                            <td width="15%">
                                <a href="/{$row['user_name']}" target="_blank">
                                    {$row['user_name']}
                                </a>
                            </td>
							<td width="20%">{$row['subject']}</td>
							<td width="20%">{$row['message']}</td>
							<td width="5%">{if $row['reply'] == '0'} <span style="color:red">Not Sent</span>{/if}{if !$row['reply'] == '1'}<span style="color:green">Sent</span>{/if}</td>
							<td width="10%">{$row['created']|date_format:"%e %B %Y"}</td>
                            
                            <td width="10%">
                                <button data-toggle="tooltip" data-placement="top" title='{__("Delete")}' class="btn btn-xs btn-danger js_admin-deleter" data-handle="support_email" data-id="{$row['id']}">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                                <a data-toggle="tooltip" data-placement="top" title='{__("Reply")}' href="/admincp/support/reply/{$row['id']}" class="btn btn-xs btn-primary">
                                    <i class="fa fa-reply"></i>
                                </a>
                            </td>
                        </tr>
                        {/foreach}
                    </tbody>
					
                </table>
				{if $total_pages > 1}
				<div class='pagination' style="display: block;
    padding-left: 32px;margin:1px;0;">   
				  {$html}	
				 							
				</div> 
				{/if} 
            </div>
        </div>
    
	{elseif $sub_view == "reply"}
	<!-- Support Reply Start -->
			<div class="panel-body">
            <div class="row">
                <div class="col-xs-offset-3 col-xs-6 col-sm-offset-0 col-sm-2 mb10">
                    <img class="img-responsive img-thumbnail" src="{$data['user_picture']}">
                </div>
                <div class="col-xs-12 col-sm-5 mb10">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="badge">{$data['user_id']}</span>
                            {__("User ID")}
                        </li>
                        <li class="list-group-item">
                            <span class="badge">{$data['user_registered']|date_format:"%e %B %Y"}</span>
                            {__("Joined")}
                        </li>
                        <li class="list-group-item">
                            <span class="badge">{$data['user_last_login']|date_format:"%e %B %Y"}</span>
                            {__("Last Login")}
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-5 mb10">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="badge">{$data['friends']}</span>
                            {__("Friends")}
                        </li>
                        <li class="list-group-item">
                            <span class="badge">{$data['followings']}</span>
                            {__("Followings")}
                        </li>
                        <li class="list-group-item">
                            <span class="badge">{$data['followers']}</span>
                            {__("Followers")}
                        </li>
                    </ul>
                </div>
            </div>
			
			<!-- tabs nav -->
            <ul class="nav nav-tabs mb20">
                <li class="active">
                    <a href="#support-reply" data-toggle="tab">
                        <i class="fa fa-cog fa-fw mr5"></i><strong class="pr5">{__("Reply to User")}</strong>
                    </a>
                </li>
                
            </ul>
            <!-- tabs nav -->	
            
            <!-- tabs content -->
            <div class="tab-content">
                <!-- account tab -->
                <div class="tab-pane active" id="support-reply">
					<form class="js_ajax-forms form-horizontal" data-url="admin/users.php?id={$data['user_id']}&do=send_reply">
						<br><br>
						<input type="hidden" name="from_id" value="{$data['id']}" >
						<input type="hidden" name="user_id" value="{$data['user_id']}" >
						<input type="hidden" name="hidden_user_email" value="{$data['user_email']}" >
						<div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Username")}
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="user_name" value="{$data['user_name']}" disabled>
                            </div>
                        </div>
						
						<div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Email Address")}
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="user_email" value="{$data['user_email']}" disabled>
                            </div>
                        </div>
						
						<div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("User Subject")}
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="user_subject" value="{$data['subject']}" disabled>
                            </div>
                        </div>
						
						<div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("User Message")}
                            </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="user_message" disabled rows="5">{$data['message']}</textarea>
                            </div>
                        </div>
						
						<div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Reply Subject")}
                            </label>
                            <div class="col-sm-9">
                               <input type="text" class="form-control" name="reply_subject" value="">
                            </div>
                        </div>
						
						<div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Support Reply")}
                            </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="support_message" rows="8"></textarea>
                            </div>
                        </div>
						
						<div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary">{__("Send Message")}</button>
                                
                            </div>
                        </div>
					</form>
				</div>
			</div>	
        </div>
		{/if}	
	<!-- Support Reply End -->	
</div>