<div class="modal-body plr0 ptb0">
    <div class="x-form publisher mini">
        <div class="DMActivity-header">
            <div class="DMActivity-navigation">
                <button type="button" class="DMActivity-back u-textUserColorHover" onclick="loadmainModal()">
                    <span class="Icon Icon--caretLeft u-linkComplex-target Icon--medium"></span>
                    <span class="u-hiddenVisually">Back to inbox</span>
                </button>
            </div>
            <h2 class="DMActivity-title js-ariaTitle" id="dm_dialog-header">
                {__("New Message")}
            </h2>
            <div class="DMActivity-toolbar">
                <button type="button" class="DMActivity-close u-textUserColorHover newchat_close" id="chat-close" data-dismiss="modal" aria-hidden="true">
                    <span class="Icon Icon--close Icon--medium"></span>
                    <span class="u-hiddenVisually">Close</span>
                </button>
            </div>
        </div>
			<!--	  <div class="popup_newchart">
                <div class="chat-conversations js_scroller"></div>
                    <div class="chat-to_new clearfix js_autocomplete">
                        <div class="to">{__("To")}:</div>
                        <ul class="tags"></ul>
                        <div class="typeahead">
                            <input type="text" size="20" autofocus placeholder="Enter a name">
                        </div>
                    </div>
                    <div class="chat-attachments attachments clearfix x-hidden" style="display: none;"> <ul> <li class="loading" style="display: none;"> <div class="loader loader_small"></div> </li> </ul> </div>
                    <div class="x-form chat-form x-visible">
                        <div class="chat-form-message">
                            <textarea class="js_autosize  js_post-message_new" dir="auto" rows="1" placeholder='{__("Write a message")}'></textarea>
                        </div>
                        <div class="x-form-tools">
                            <div class="x-form-tools-attach">
                                <i class="fa fa-camera js_x-uploader" data-handle="chat"></i>
                            </div>
                            <div class="x-form-tools-emoji js_emoji-menu-toggle">
                                <i class="fa fa-smile-o fa-lg"></i>
                            </div>
                            {include file='_emoji-menu.tpl'}
                        </div>
                    </div>
                </div>- -->
		<div class="js_conversation-container" id="chat-box">
		    <div class="panel panel-default panel-messages fresh">
                <div class="panel-body panel-new-msg">
                    <div class="chat-conversations js_scroller" data-slimScroll-height="367px"></div>
                    <div class="chat-to clearfix js_autocomplete custom_autocomplete">
                        <div class="to">{__("Send message to")}:</div>
                        <div class="typeahead">
                            <div class="TokenizedMultiselect-inputContainer">
                                <ul class="tags"></ul>
                                <input type="text" size="1" autofocus>
                           </div>
                            <div class="DMTypeaheadHeader"><!-- <span class="show">Recent</span><span class="show">People</span> --></div>
                        </div>
                    </div>
                    <div class="chat-attachments attachments clearfix x-hidden" style="display: none;">
                        <ul>
                            <li class="loading">
                                <div class="loader loader_small"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="chat-form-container" style="display: none;">
                        <div class="x-form chat-form chat_search_area">
                            <div class="chat-form-message custom-chat-form">
                                <textarea class="js_autosize js_post-message msg3" placeholder='{__("Write a message")}'></textarea>
                            </div>
                            <div class="x-form-tools">
                                <div class="x-form-tools-attach">
                                    <i class="fa fa-camera js_x-uploader" data-handle="chat"></i>
                                </div>
                                <div class="x-form-tools-emoji js_emoji-menu-toggle">
                                    <i class="fa fa-smile-o fa-lg"></i>
                                </div>
                                {include file='_emoji-menu.tpl'}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
    </div>
</div>

<!--<div class="modal-footer custom-model-footer">
    <button type="submit" class="btn btn-primary post-chat" onclick="aaaaaa()" >{__("Send")} </button>
</div>-->
<div class="modal-footer DMActivity-footer u-emptyHide" style="padding: 0px !important;">
    <div class="DMButtonBar">
        <button type="button" class="EdgeButton EdgeButton--primary dm-initiate-conversation" onclick="aaaaaa()">
            {__("Next")}
        </button>
    </div>
</div>

<script type="text/javascript">
function aaaaaa(){
    var all = new Array();
    $('.senbird_user_listings').each(function() {
        all.push($(this).attr('data-uid'));
    });
    toConenctUid = $('#user-userId').val();
    all.push(toConenctUid);
    console.log(JSON.stringify(all));
    //sendMultiple(all,true);
}

function loadmainModal(){
    $('#myModal').modal('show');
    $('#modal').modal('hide');
}
$('#modal .newchat_close').click(function() {
    $('.custom-overlay').modal('hide');
});
// $('.new_chat_popup').click(function(){
//     alert('clicked');
//    $('#modal').show();
//  });
</script>