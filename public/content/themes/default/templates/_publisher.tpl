<style type="text/css">
.mce-tinymce{
	box-shadow: none;
}
.mobileHide {
    display: inline;
} 

.text-counter {
	
	display:inline-block !important;
}
/* Smartphone Portrait and Landscape */

@media only screen and (min-device-width: 320px) and (max-device-width: 480px) {
    .mobileHide {
        display: none!important
    }
}

@media (max-width:767px) {
	.post_custom_mobile #postBoxDiv .post_area_custom {
		padding-left: 60px;
		padding-top: 2px;
		padding-bottom: 2px;
	}

	.post_custom_mobile .post-avatar {
		width: 36px;
	}

	.post_custom_mobile .post-avatar-picture {
		min-width: 36px;
		min-height: 36px;
	}
	.mce-tinymce {
		-webkit-box-shadow: none;
		-moz-box-shadow: none;
		box-shadow: none;
	}
	.post_custom_mobile #postBoxDiv ~ .publisher-footer .publisher-tools li {
		padding: 0 5px 0 0;
	}
	.x-uploader .fa {
		color: #337ab7;
	}
	.publisher-tools-attach .fa.fa-camera.fa-fw.js_x-uploader {
		font-size: 26px;
		line-height: 26px;
	}
	.publisher-tools-attach .x-uploader .fa {
		font-size: 26px;
	}
	
	.publisher-tools-attach .fa .fa-paperclip .fa-fw .js_x-uploader {
		font-size: 26px;
	}
	
	.publisher-tools-attach .fa.fa-smile-o {
		color: #337ab7;
		font-size: 26px;
		line-height: 26px;
	}
	.text-counter {
	 position: absolute;
	 top: 17px;
	 right: 125px;
	 float:right;
	 width: 7%;
	}
}

@media (max-width: 1024px) {
	
	.post_custom_mobile #postBoxDiv .post_area_custom {
		padding-left: 60px;
		padding-top: 2px;
		padding-bottom: 2px;
	}

	.post_custom_mobile .post-avatar {
		width: 36px;
	}

	.post_custom_mobile .post-avatar-picture {
		min-width: 36px;
		min-height: 36px;
	}
	.mce-tinymce {
		-webkit-box-shadow: none;
		-moz-box-shadow: none;
		box-shadow: none;
	}
	.post_custom_mobile #postBoxDiv ~ .publisher-footer .publisher-tools li {
		padding: 0 5px 0 0;
	}
	.x-uploader .fa {
		color: #337ab7;
	}
	.publisher-tools-attach .fa.fa-camera.fa-fw.js_x-uploader {
		font-size: 26px;
		line-height: 26px;
	}
	.publisher-tools-attach .x-uploader .fa {
		font-size: 26px;
	}
	.publisher-tools-attach .fa .fa-paperclip .fa-fw .js_x-uploader {
		font-size: 26px;
	}
	
	.publisher-tools-attach .fa.fa-smile-o {
		color: #337ab7;
		font-size: 26px;
		line-height: 26px;
	}
	.text-counter {
		 position: absolute;
		 top: 17px;
		 right: 125px;
		 float:right;
		 width: 7%;
	}
}
.mce-stack-layout-item {
    width: 100%;
}	
</style>
<div class="post_custom post_custom_mobile">
    <div class="max-600">
        <div class="post-avatar" style="float:left;">
            <a class="post-avatar-picture" href="{$_post['post_author_url']}" style="background-image:url({$user->_data.user_picture});"></a>
        </div>
        <div class="x-form publisher" data-handle="{$_handle}" {if $_id}data-id="{$_id}" {/if}>
            <!-- publisher loader -->
            <div class="publisher-loader">
                <div class="loader loader_small"></div>
            </div>
            <!-- publisher loader -->
            <!-- publisher tabs -->
            <!-- publisher tabs -->
            <!-- post product -->
            <div class="publisher-meta top" data-meta="product">
                <i class="fa fa-tag fa-fw"></i>
                <input name="name" type="text" placeholder='{__("What are you selling?")}'>
            </div>
            <div class="publisher-meta top" data-meta="product">
                <i class="fa fa-money fa-fw"></i>
                <input name="price" type="text" placeholder='{__("Add price")} ({$system["system_currency"]})'>
            </div>
            <div class="publisher-meta top" data-meta="product">
                <i class="fa fa-shopping-basket fa-fw"></i>
                <select name="category_id">
                    {foreach $market_categories as $category}
                    <option value="{$category['category_id']}">{__($category['category_name'])}</option>
                    {/foreach}
                </select>
            </div>
            <div class="publisher-meta top" data-meta="product">
                <i class="fa fa-map-marker fa-fw"></i>
                <input name="location" class="js_geocomplete" id="js_geocomplete" type="text" placeholder='{__("Add Location (optional)")}'>
            </div>
            <!-- post product -->
            <!-- post message -->
            <div class="relative focused" id="postBoxDiv">
                <div class="post_area_custom">
                    <div class="post-avatar" style="float:left;">
                        <a class="post-avatar-picture" href="{$_post['post_author_url']}" style="background-image:url({$user->_data.user_picture});"></a>
                    </div>
                    <textarea dir="auto" style="height:60px;overflow: hidden;" id="postBox" class="js_mention js_publisher-scraper postBox emojiable-question post_area_custom_textarea" data-init-placeholder='' maxlength="400"></textarea>
                    {*
                    <button id="toggle">select emoji</button>*}
                    <span class="js_emoji-menu-toggle" style="float:right;width: 7%; display:none;" data-toggle="tooltip" data-placement="top" title='{__("Insert an emoji")}'><!--i class="fa fa-smile-o fa-fw"--></i></span>{include file='_emoji-menu.tpl'}
                     {if $system['photos_enabled']}
                    <span class="publisher-tools-attach js_publisher-photos" style="float:right;width: 7%; height:30px; padding-top:0px;">

                        <!--i class="fa fa-camera fa-fw js_x-uploader" data-handle="publisher" data-multiple="true"></i-->

                    </span> {/if}
                </div>

                <!-- publisher scraper -->
                <div class="publisher-scraper"></div>
                <!-- publisher scraper -->
                <!-- post attachments -->
                <div class="publisher-attachments attachments clearfix x-hidden">
                    <ul id="sortable"></ul>
                </div>
                <!-- post attachments -->
				
				<!-- Please wait Loader ---->
				<div style="font-size:14px;padding: 0px 10px 9px 18px;background: #f5f8fa;color: #726969;display:none;" id="internal-processing">Internal processing please wait....</div>
				
                <!-- post album -->
                <div class="publisher-meta" data-meta="album">
                    <i class="fa fa-picture-o fa-fw"></i>
                    <input type="text" placeholder='{__("Album title")}'>
                </div>
                <!-- post album -->
                <!-- post poll -->
                <div class="publisher-meta remove_all_poll" data-meta="poll">
                    <button type="button" class="btn btn-primary remove_poll">{__("Remove Poll")}</button>
                </div>
                <div class="publisher-meta custom_poll" data-meta="poll">
                    <i class="fa fa-plus fa-fw"></i>
                    <input type="text" placeholder='{__("Add an option")}...'>
                </div>
                <div class="publisher-meta custom_poll" data-meta="poll">
                    <i class="fa fa-plus fa-fw"></i>
                    <input type="text" placeholder='{__("Add an option")}...'>
                </div>
                <!-- post poll -->
                <!-- post video -->
                <div class="publisher-meta remove-media" data-meta="video">
                    <i class="fa fa-video-camera fa-fw"></i> {__("Video uploaded successfully")}
                    <p id="video-name"></p>
                    <button type="button" class="close js_publisher-media-remover" title="Remove"><span>×</span></button>
                </div>
                <!-- post video -->
                <!-- post audio -->
                <div class="publisher-meta remove-media" data-meta="audio">
                    <i class="fa fa-music fa-fw"></i> {__("Audio uploaded successfully")}
                    <p id="audio-name"></p>
                    <button type="button" class="close js_publisher-media-remover" title="Remove"><span>×</span></button>
                </div>
                <!-- post audio -->
                <!-- post file -->
                <div class="publisher-meta" data-meta="file">
                    <i class="fa fa-file-text-o fa-fw"></i> {__("File uploaded successfully")}
                </div>
                <!-- post file -->
                <!-- post location -->
                <div class="publisher-meta" data-meta="location">
                    <i class="fa fa-map-marker fa-fw"></i>
                    <input class="js_geocomplete" type="text" placeholder='{__("Where are you?")}'>
                </div>
                <!-- post location -->
                <!-- post feelings -->
                <div class="publisher-meta feelings" data-meta="feelings">
                    <div id="feelings-menu-toggle" data-init-text='{__("What are you doing?")}'>{__("What are you doing?")}</div>
                    <div id="feelings-data" style="display: none">
                        <input type="text" placeholder='{__("What are you doing?")}'>
                        <span></span>
                    </div>
                    <div id="feelings-menu" class="dropdown-menu dropdown-widget">
                        <div class="dropdown-widget-body ptb5">
                            <div class="js_scroller">
                                <ul class="feelings-list">
                                    {foreach $feelings as $feeling}
                                    <li class="feeling-item js_feelings-add" data-action="{$feeling['action']}" data-placeholder="{__($feeling['placeholder'])}">
                                        <div class="icon">
                                            <i class="twa twa-3x twa-{$feeling['icon']}"></i>
                                        </div>
                                        <div class="data">
                                            {__($feeling['text'])}
                                        </div>
                                    </li>
                                    {/foreach}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div id="feelings-types" class="dropdown-menu dropdown-widget">
                        <div class="dropdown-widget-body ptb5">
                            <div class="js_scroller">
                                <ul class="feelings-list">
                                    {foreach $feelings_types as $type}
                                    <li class="feeling-item js_feelings-type" data-type="{$type['action']}" data-icon="{$type['icon']}">
                                        <div class="icon">
                                            <i class="twa twa-3x twa-{$type['icon']}"></i>
                                        </div>
                                        <div class="data">
                                            {__($type['text'])}
                                        </div>
                                    </li>
                                    {/foreach}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- post feelings -->
            </div>
            <!-- post message -->
            <!-- publisher-footer -->
            <div class="publisher-footer clearfix" style="display:block;">



                <!-- publisher-tools -->
                <ul class="publisher-tools">
                    <li data-toggle="tooltip" data-placement="top" title='{__("Create Post")}'>
                        <span class="js_publisher-tab" data-tab="post">

                        <i class="fa fa-pencil fa-fw"></i> <span class="hidden-xs"></span>
                        </span>
                    </li>
                    {if $system['photos_enabled']}
                    <!--li >
                        <span class="publisher-tools-attach js_publisher-photos newalbumClass" style="float:right;width: 7%; height:30px; padding-top:0px;">

                                <i class="fa fa-camera fa-fw js_x-uploader" data-handle="publisher" data-multiple="true"></i>

                            </span>
                    </li-->
                    <li >
                        <span class="publisher-tools-attach js_publisher-photos newalbumClass" style="float:right;width: 7%; height:30px; padding-top:0px;">

                                <i class="fa fa-camera fa-fw js_x-uploader" data-handle="publisher" data-multiple="true"></i>

                            </span>
                    </li>
                    {/if} {if $system['geolocation_enabled']}
                    <!--<li data-toggle="tooltip" data-placement="top" title='{__("Check In")}'>

                        <span class="js_publisher-location">

                            <i class="fa fa-map-marker fa-fw"></i>

                        </span>

                    </li>
            -->
                    {/if}
                    <!--            <li class="relative" data-toggle="tooltip" data-placement="top" title='{__("Add Feelings & Activity")}'>

                    <span class="js_publisher-feelings">

                        <i class="fa fa-smile-o"></i>

                    </span>

                </li>

               <li class="relative" data-toggle="tooltip" data-placement="top" title='{__("Create Poll")}'>

                    <span class="js_publisher-tab" data-tab="poll">

                        <i class="fa fa-pie-chart fa-fw"></i>

                    </span>

                </li>-->
                    {if $system['market_enabled'] && $_handle != "group" && $_handle != "event"}
                    <li class="relative" data-toggle="tooltip" data-placement="top" title='{__("Sell Something")}'>
                        <span class="js_publisher-tab" data-tab="product">

                        <i class="fa fa-tag fa-fw"></i>

                    </span>
                    </li>
                    {/if} {if $system['blogs_enabled'] && $_handle != "group" && $_handle != "event"}
                    <!--           <li class="relative" data-toggle="tooltip" data-placement="top" title='{__("Write Article")}'>

                    <span class="js_publisher-tab" data-tab="blog">

                    <a href="/blogs/new">

                        <i class="fa fa-file-text fa-fw"></i>

                    </a>

                    </span>

                </li>-->
                    {/if} {if $system['videos_enabled']}
                    <li id="video-select" class="relative" >
                        <span class="publisher-tools-attach js_publisher-tab" data-tab="video">

                        <i class="fa fa-video-camera fa-fw js_x-uploader" data-handle="publisher" data-type="video"></i>

                    </span>
                    </li>
                    {/if} 
					
					{if $system['file_enabled']}
						{if ($user->_data['user_name'] == "milesguo" || $user->_data['user_name'] == "Milesguo") }
						<li>
							<span class="publisher-tools-attach js_publisher-tab" data-tab="file">
								<i class="fa fa-paperclip fa-fw js_x-uploader" data-handle="publisher" data-type="file"></i> 
							</span>
						</li>
						{/if}
					{/if}
					
					{if $system['audio_enabled']}
                     <li id="audio-select" class="relative" >

						<span class="publisher-tools-attach js_publisher-tab" data-tab="audio">

							<i class="fa fa-music fa-fw js_x-uploader" data-handle="publisher" data-type="audio"></i>

						</span>

					</li> 
                    {/if}
                    <li data-toggle="tooltip" class="" data-placement="top">
                        <span class="publisher-tools-attach js_publisher-photos newalbumClass picker" style="overflow:visible">
                        <i class="fa fa-smile-o fa-fw"></i>
                    </span>
                    </li>
					
				{if $user->_data['user_name'] == "apatle" }		
					<li data-toggle="tooltip" class="take-photo" data-placement="top">
                        <a href="javascript:void(0)" class="take-photo">Take Photo</a>
                    
                    </li>
				{/if}
				{if $user->_data['user_name'] == "apatle" }		
					<li data-toggle="tooltip" class="take-video" data-placement="top">
                        <a href="javascript:void(0)" class="take-video">Take Video</a>
                    
                    </li>
				{/if}
                    <script>
                    
                        $(function() {
                            /*setTimeout(function(){
                                $('#postBox').emojiPicker({
                                    width: '300px',
                                    height: '280px',
                                    button: false
                                });
                                $('#toggle').click(function(e) {
                                    e.preventDefault();
                                    $('#postBox').emojiPicker('toggle');
                                });
                            },1000);*/
							
							$('.picker').lsxEmojiPicker({
								closeOnSelect: true,
								twemoji: true,
								onSelect: function(emoji){
									
									tinymce.activeEditor.execCommand('mceInsertContent', false, emoji.value)
								}
							});
							
                        });
                   
                    </script>
                </ul>
                <!-- publisher-tools -->
                <!-- publisher-buttons -->
                <div class="pull-right flip mt5">
                    <div class="max-600">
                        <a href="javascript:void(0)" id="close-update-popup"><i class="fa fa-times"></i></a>
                        {if $_privacy}
                        <!-- privacy -->
                        <div class="btn-group js_publisher-privacy" data-toggle="tooltip" data-placement="top" data-value="public" title='{__("Shared with: Public")}'>
                        </div>
                        <!-- privacy -->
                        {/if}
						<span class="text-counter" id="count_char" style="display:inline-block;"></span>
						
                        <button type="button" class="btn btn-primary js_publisher">{__("Post")}</button>
                    </div>
                </div>
                <!-- publisher-buttons -->
            </div>
            <!-- publisher-footer -->
        </div>
    </div>
</div>
<a href="javascript:void(0)" id="post-update-btn">
    <img src="/content/themes/default/images/post-icon.svg">
</a>
<div class="discard-popup">
    <div class="discard-container">
        <h3>Discard Post?</h3>
        <p>You can't undo this and you'll lose your draft.</p>
        <a href="javascript:void(0)" class="btn btn-primary discard-confirm">Yes, discard it</a>
        <a href="javascript:void(0)" id="discard-cancel">Cancel</a>
    </div>
</div>
{literal}
<script type="text/javascript">
var ta = document.getElementById('postBox');

var dv = document.getElementById('postBoxDiv');





ta.addEventListener('focus', function() {

    autosize(ta);



    document.querySelector('.js_emoji-menu-toggle').style.display = 'block';

    document.querySelector('.text-counter').style.display = 'block';

    document.querySelector('.js_publisher-photos').style.display = 'none';

    //document.querySelector('.publisher-footer').style.display = 'block';

    //dv.classList.add('focused');



    /*var ht = parseInt(ta.style.height, 10) + 22;*/

    //alert(ht)

    //dv.style.height = ht+'px';

});

ta.addEventListener('blur', function() {

    if (ta.value == '') {



    }

});



document.body.addEventListener('click', function(evt) {





    if (!$(evt.target).is('i.fa') && !$(evt.target).is('textarea')) {

        autosize.destroy(ta)

        document.querySelector('.js_emoji-menu-toggle').style.display = 'none';

        document.querySelector('.text-counter').style.display = 'block';

        document.querySelector('.js_publisher-photos').style.display = 'block';
        //document.querySelector('.js_publisher-tab').style.display = 'block';

        if($(".post_custom_mobile .post_area_custom textarea").val() == "" && $(".post_custom_mobile .publisher-attachments ul").html() == "" && $(".post_custom_mobile .publisher-meta[data-meta='video']").css("display") == "none") {
            //document.querySelector('.publisher-footer').style.display = 'none';
            //dv.classList.remove('focused');
        }

    }

});



ta.addEventListener('keypress', function() {

    /*var ht = parseInt(ta.style.height, 10) + 22;*/

    //alert(ht);

    //dv.style.height = ht+'px';

});
</script>
{/literal}