{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20 {if $user->_logged_in}offcanvas{/if}">
  <div class="row">

        <div class="col-xs-12  {if $user->_logged_in}offcanvas-mainbar {else}video_post_logout{/if}">
          
          <div class="row ">
            <!-- left panel -->
            <div class="col-sm-12 col-md-12">
            <div class="col-sm-12 col-md-8 video_main">
                {include file='_video_post.tpl' standalone=true}
            </div>
        <!-- left panel -->

        <!-- right panel -->
        
            <div class="col-sm-12 col-md-4 chat-live">
                       {if $user->_logged_in} 
                       
                            <script src="https://sample.sendbird.com/livechat/SendBird.min.js"></script>
                            <script src="https://sample.sendbird.com/livechat/build/liveChat.SendBird.js"></script>
                           <div class="content-chat" id="sb_chat"></div>
                           <script>
                              var appId = 'E2401D42-320A-48DA-8CD8-6DE8EBDC8644';
                              liveChat.start(appId, document.getElementById('channel_id').value);
                              jQuery(".chat-board").css("display","none");
                              jQuery(".chat-board").find('.top').text('');
                              jQuery(".chat-board").find('.top').text('Live Chat');
                              var timestamp = Math.round((new Date()).getTime() / 1000);
                              jQuery(".user-id").find('input').val((document.getElementById('user_firstname').value)+"_"+timestamp);
                              //jQuery(".user-id").find('input').val(document.getElementById('user_firstname').value);
                              jQuery(".nickname").find('input').val(document.getElementById('user_firstname').value);
                              jQuery(".login-board").find('div.btn').removeClass("disabled");
                              // jQuery(".login-board").find('div.btn').trigger("click");
                              setTimeout(function(){ 
                                  jQuery(".login-board").find('div.btn').trigger("click");
                                  jQuery(".chat-board").css("display","block");
                              }, 2500);
                              
                              setInterval(function(){
                                  jQuery.post("{SYS_URL_API}/tokbox/getViewCount.php", { 'post_id':document.getElementById('post_id').value }, function(res){
                                           if(res.success == true) {
                                               jQuery(".update_count").html();
                                               jQuery(".update_count").html(res.data);
                                           } 
                                    });
                              },60000);
                              
                           </script>
                       {/if}
                    </div>
                </div>
        <!-- right panel -->
          </div>
        </div>

  </div>
</div>
<!-- page content -->

{include file='_footer.tpl'}