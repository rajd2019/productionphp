var appId = 'E2401D42-320A-48DA-8CD8-6DE8EBDC8644';
var currScrollHeight = 0;
var MESSAGE_TEXT_HEIGHT = 27;

var nickname = null;
var userId = null;
var channelListPage = 0;
var currChannelUrl = null;
var currChannelInfo = null;
var leaveChannelUrl = '';
var leaveMessagingChannelUrl = '';
var hideChannelUrl = '';
var userListToken = '';
var userListNext = 0;
var targetAddGroupChannel = null;

var isOpenChat = false;
var memberList = [];

var site_photos_path = 'https://d57iplyuvntm7.cloudfront.net/uploads/';
var site_video_path = 'https://d57iplyuvntm7.cloudfront.net/uploads/';

// 3.0.x
var currentUser;

$('#guide_create').click(function() {
    $('.modal-guide-create').hide();
});

/***********************************************
 *  Send message in the input box to the other members in the chat
 **********************************************/
function getBase64(file, callBack) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        callBack('', reader.result);
    };
    reader.onerror = function (error) {
        callBack(error);
    };
}

$( '#custom-editor' ).focusin(function() {

        $('.DMComposer-send button').attr('aria-disabled', false);
        $('.DMComposer-send button').removeClass('disabled');

})

function DMConversation_sendChatMessage() {
    var chatTextAR = $('#custom-editor').val();
    if($.trim(chatText)!=''){
        $('.DMConversation-sendingStateIndicator').css('display', 'block');
        $('.DMConversation-blankmsgStateIndicator').css('display', 'none');
    }else{
        $('.DMComposer-send button').attr('aria-disabled', true);
        $('.DMComposer-send button').addClass('disabled');
        $('.DMConversation-blankmsgStateIndicator').css('display', 'block');
    }

    if ($('.DMComposer-attachment .ComposerThumbnails').html() != '') {
        if ($('.DMComposer-attachment .ComposerThumbnail').hasClass('ComposerThumbnail--video')) {
            if ($('.DMComposer-attachment .ComposerThumbnail.ComposerThumbnail--video').html() != '') {
                var selected_file = $('.DMComposer-gifSearch input[type="file"]')[0].files[0];
                getBase64(selected_file, function(error, result) {
                    if (error) {
                        alert("Sending Video file failed!");
                        return;
                    }
                    currChannelInfo.sendFileMessage(result, '', 'videoFile', SendMessageHandler);
                });
            }
        } else {
            var selected_file = $('.DMComposer-mediaPicker input[type="file"]')[0].files[0];
            getBase64(selected_file, function(error, result) {
                if (error) {
                    alert("Sending Photo file failed!");
                    return;
                }
                currChannelInfo.sendFileMessage(result, '', 'photoFile', SendMessageHandler);
            });
            //currChannelInfo.sendFileMessage(selected_file, '', 'photoFile', SendMessageHandler);
        }
    }

    /*var chatText = $('.DMComposer-editor.rich-editor div').html();
    if (chatText != '<br>') {
        var newLine = String.fromCharCode(13, 10);
        chatText = chatText.replace(/<br>/g, newLine);
        currChannelInfo.sendUserMessage($.trim(chatText), '', SendMessageHandler);
        $('.DMComposer-editor.rich-editor').html('<div><br></div>');
    }*/

	/*** Code Edited By Abhishek ******/
  var chatText = $('#custom-editor').val();
  if ($(".DMComposer-attachment .DMComposer-tweet").is(":visible") && $('.DMComposer-attachment .DMComposer-tweet').html() != '') {
    var result1 = $('#org_post_detail').html();

    var postDetails = $('#org_post_detail').attr('org-post-id');
    currChannelInfo.sendUserMessage($.trim(chatText), postDetails,'postDetail', SendMessageHandler);
    //alert('*');
  }else{
    if($.trim(chatText)!=''){
        currChannelInfo.sendUserMessage($.trim(chatText), '', SendMessageHandler);
    }
  }
    $('#custom-editor').val('');
	/*** Code Edited By Abhishek End ****/

    scrollPositionBottom();
}

/***********************************************
 *  Close DMConversation Live Chat Dialog
 **********************************************/
function DMConversation_closeLiveChatDiag() {
    $('#chatModal').on('hidden.bs.modal', function () {
    });
    $('#chatModal').modal('hide');
    currChannelUrl = null;
    currChannelInfo = null;
}

/***********************************************
 *  Close DMInbox Dialog
 **********************************************/
function DMInbox_closeDiag() {
    $('#chatModal').on('hidden.bs.modal', function () {
    });
    $('#myModal').modal('hide');
    currChannelUrl = null;
    currChannelInfo = null;
}

/***********************************************
 *  Close DMConversation Live Chat Dialog and Back to the Main Dialog
 **********************************************/
function DMConversation_backToMainModal() {
    $('#chatModal').on('hidden.bs.modal', function () {
        $('body').addClass('modal-open');
    })
    $('#chatModal').modal('hide');
    $('#myModal').modal('show');
    currChannelUrl = null;
    currChannelInfo = null;
}

/***********************************************
 *  Remove the selected message in the chat channel
 **********************************************/
function DMConversation_deleteChatMessage(messageId) {
    if($('.DMNotice.DMDeleteMessage').attr('style') == '') {
        $('.DMNotice.DMDeleteMessage').slideUp();
    }
    $('.DMNotice.DMDeleteMessage').slideDown(function() {
        $('.DMNotice.DMDeleteMessage').attr('style', '');
        $('.DMNotice.DMDeleteMessage .DMDeleteMessage-cancel').click(function () {
            $('.DMNotice.DMDeleteMessage').slideUp();
        });
        console.log(messageId);
        $('.DMNotice.DMDeleteMessage .DMDeleteMessage-confirm').on('click', function() {
            $('.DMNotice.DMDeleteMessage .DMDeleteMessage-confirm').off('click');
            var channelUrl = currChannelInfo.url;
            console.log(channelMessageList[channelUrl]);
            console.log(channelMessageList[channelUrl][messageId]);
            currChannelInfo.deleteMessage(channelMessageList[channelUrl][messageId]['message'], function(response, error){
                if (!error) {
                    delete(channelMessageList[channelUrl][messageId]);
                    $('.DMConversation-content li[data-message-id="' + messageId + '"]').remove();
                }

                $('.DMNotice.DMDeleteMessage').slideUp();
            });
        });
    });
}

// Select the video to be uploaded
$('.DMComposer-gifSearch input[type="file"]').change(function () {
    if ($('.DMComposer-gifSearch input[type="file"]').val().trim().length == 0) return;

    var extension = $('.DMComposer-gifSearch input[type="file"]').val().split('.').pop();
    switch(extension.toLowerCase()) {
        case 'mp4':
        case 'm4v':
        case 'mov':
            var selected_file = $('.DMComposer-gifSearch input[type="file"]')[0].files[0];
            var fr = new FileReader();
            fr.onload = function () {
                var uploadImageStr = '' +
                    '<div role="button" tabindex="0" aria-label="Image 1" class="ComposerThumbnail ComposerThumbnail--video" data-file-id="1" style="height: 206px; width: 330px;">\
                        <div class="ComposerThumbnail-imageContainer" style="height: 206px; width: 330px;">\
                            <video controls>\
                                <source src="%selected_video%">\
                            </video>\
                        </div>\
                        <div class="ComposerThumbnail-overlay ComposerThumbnail-remove">\
                            <div class="Icon Icon--close Icon--smallest"></div>\
                        </div>\
                    </div>';
                uploadImageStr = uploadImageStr.replace('%selected_video%', fr.result);
                $('.DMComposer-attachment .ComposerThumbnails').html(uploadImageStr);
                $('.DMComposer.tweet-form').addClass('has-thumbnail');

                // Disable the attachment buttons
                $('.DMComposer-mediaPicker button').addClass('disabled');
                $('.DMComposer-mediaPicker button').attr('disabled', true);
                $('.DMComposer-mediaPicker input[type="file"]').attr('disabled', true);
                $('.DMComposer-gifSearch button').addClass('disabled');
                $('.DMComposer-gifSearch button').attr('disabled', true);
                $('.DMComposer-gifSearch input[type="file"]').attr('disabled', true);

                // Enable Send Button
                $('.DMComposer-send button').attr('aria-disabled', false);
                $('.DMComposer-send button').removeClass('disabled');

                $('.DMComposer-attachment .ComposerThumbnails .ComposerThumbnail-remove').click(function () {
                    $('.DMComposer-attachment .ComposerThumbnails').html('');
                    $('.DMComposer.tweet-form').removeClass('has-thumbnail');
                    $('.DMComposer-mediaPicker input[type="file"]').val('');
                    $('.DMComposer-gifSearch input[type="file"]').val('');

                    // Enable Attachment Buttons
                    $('.DMComposer-mediaPicker button').removeClass('disabled');
                    $('.DMComposer-mediaPicker button').attr('disabled', false);
                    $('.DMComposer-mediaPicker input[type="file"]').attr('disabled', false);
                    $('.DMComposer-gifSearch button').removeClass('disabled');
                    $('.DMComposer-gifSearch button').attr('disabled', false);
                    $('.DMComposer-gifSearch input[type="file"]').attr('disabled', false);

                    // Check Send Button Status
                    console.log($('.DMComposer-attachment .ComposerThumbnails').html());
                    if ($('.DMComposer-editor.rich-editor').html() == '<div><br></div>') {
                        $('.DMComposer-send button').attr('aria-disabled', true);
                        $('.DMComposer-send button').addClass('disabled');
                    }
                });
            };
            fr.readAsDataURL(selected_file);
            break;
        default:
            alert('Invalid Video Type');
            $('.DMComposer-gifSearch input[type="file"]').val('');
            break;
    }
});

// Select the image to be uploaded
$('.DMComposer-mediaPicker input[type="file"]').change(function () {
    if ($('.DMComposer-mediaPicker input[type="file"]').val().trim().length == 0) return;

    var extension = $('.DMComposer-mediaPicker input[type="file"]').val().split('.').pop();
    switch (extension.toLowerCase()) {
        case 'gif':
        case 'jpg':
        case 'jpeg':
        case 'png':
            var selected_file = $('.DMComposer-mediaPicker input[type="file"]')[0].files[0];
            var fr = new FileReader();
            fr.onload = function () {
                var uploadImageStr = '' +
                    '<div role="button" tabindex="0" aria-label="Image 1" class="ComposerThumbnail" data-file-id="2">\
                        <div class="ComposerThumbnail-imageContainer">\
                            <img class="ComposerThumbnail-image" src="%selected_image%" style="height: 113px; width: 200px; margin-left: -44px; margin-top: 0px;" draggable="false">\
                        </div>\
                        <div class="ComposerThumbnail-overlay ComposerThumbnail-remove">\
                            <div class="Icon Icon--close Icon--smallest"></div>\
                        </div>\
                    </div>';
                uploadImageStr = uploadImageStr.replace('%selected_image%', fr.result);
                $('.DMComposer-attachment .ComposerThumbnails').html(uploadImageStr);
                $('.DMComposer.tweet-form').addClass('has-thumbnail');

                // Disable the attachment buttons
                $('.DMComposer-mediaPicker button').addClass('disabled');
                $('.DMComposer-mediaPicker button').attr('disabled', true);
                $('.DMComposer-mediaPicker input[type="file"]').attr('disabled', true);
                $('.DMComposer-gifSearch button').addClass('disabled');
                $('.DMComposer-gifSearch button').attr('disabled', true);
                $('.DMComposer-gifSearch input[type="file"]').attr('disabled', true);

                // Enable Send Button
                $('.DMComposer-send button').attr('aria-disabled', false);
                $('.DMComposer-send button').removeClass('disabled');

                $('.DMComposer-attachment .ComposerThumbnails .ComposerThumbnail-remove').click(function () {
                    $('.DMComposer-attachment .ComposerThumbnails').html('');
                    $('.DMComposer.tweet-form').removeClass('has-thumbnail');
                    $('.DMComposer-mediaPicker input[type="file"]').val('');
                    $('.DMComposer-gifSearch input[type="file"]').val('');

                    // Enable Attachment Buttons
                    $('.DMComposer-mediaPicker button').removeClass('disabled');
                    $('.DMComposer-mediaPicker button').attr('disabled', false);
                    $('.DMComposer-mediaPicker input[type="file"]').attr('disabled', false);
                    $('.DMComposer-gifSearch button').removeClass('disabled');
                    $('.DMComposer-gifSearch button').attr('disabled', false);
                    $('.DMComposer-gifSearch input[type="file"]').attr('disabled', false);

                    // Check Send Button Status
                    console.log($('.DMComposer-attachment .ComposerThumbnails').html());
                    if ($('.DMComposer-editor.rich-editor').html() == '<div><br></div>') {
                        $('.DMComposer-send button').attr('aria-disabled', true);
                        $('.DMComposer-send button').addClass('disabled');
                    }
                });
            };
            fr.readAsDataURL(selected_file);
            break;
        default:
            alert("Invalid Image Type");
            $('.DMComposer-mediaPicker input[type="file"]').val('');
            break;
    }
});

/***********************************************
 *                DMComposer Editor
 **********************************************/
$('.DMComposer-editor.rich-editor').keydown(function(event) {
    if (event.keyCode == 13) {
        if (event.shiftKey) {
            event.stopPropagation();
        } else {
            event.preventDefault();
            DMConversation_sendChatMessage();
        }
    }
});

function DMComposer_Editor_PasteAction() {
    // Cancel Paste
    event.preventDefault();

    // Get text representation of clipboard
    var text = event.clipboardData.getData('text/plain');

    // Insert text manually
    document.execCommand("insertHTML", false, text);
}

$('.DMComposer-editor.rich-editor').keyup(function() {
	$('.DMConversation-blankmsgStateIndicator').css('display', 'none');
    var htmlText = $('.DMComposer-editor.rich-editor').html();
    if (htmlText == '<br>' || htmlText == '') {
        $('.DMComposer-editor.rich-editor').html('<div><br></div>');
        htmlText = '<div><br></div>';
    }
    if (htmlText == '<div><br></div>' && $('.DMComposer-attachment .ComposerThumbnails').html() == '') {
        $('.DMComposer-send button').attr('aria-disabled', true);
        $('.DMComposer-send button').addClass('disabled');
    } else {
        $('.DMComposer-send button').attr('aria-disabled', false);
        $('.DMComposer-send button').removeClass('disabled');
    }
});

$('.DMConversation-composer .DMComposer-send').click(function () {
    if ($('.DMConversation-composer .DMComposer-send button').attr('aria-disabled') == 'false') {
        DMConversation_sendChatMessage();
    }
});

/***********************************************
 *                OPEN CHAT
 **********************************************/

$('#btn_open_chat').click(function() {
    popupInit();
    $('#myModal').modal('hide');
    $('.modal-guide-create').hide();
    $('.left-nav-button-guide').hide();
    $('.modal-messaging').hide();
    $('#btn_messaging_chat').removeClass('left-nav-messaging--active');

    if ($(this).hasClass('left-nav-open--active')) {
        $('.right-section__modal-bg').hide();
        $(this).removeClass('left-nav-open--active');
        $('.modal-open-chat').hide();
    } else {
        $('.right-section__modal-bg').show();
        $(this).addClass('left-nav-open--active');
        getChannelList(true);
    }
});

$('.modal-open-chat-more').click(function() {
    getChannelList(false);
});

// Create OpenChannel
$('#btn_create_open_channel').click(function(){
    modalInput("Create Open Channel", "", function(channelName){
        sb.OpenChannel.createChannel(channelName, '', '', function(channel, error){
            joinChannel(channel.url);
        });
    });
});

$(document).on('click', '.chat-canvas__list-name', function(e){
    var userId = $(this).data('userid');
    if (isCurrentUser(userId)) {
        console.log('can not block, current user');
        return;
    }

    modalConfirm('Are you Sure?', 'Do you want to block this user?', function(){
        sb.blockUserWithUserId(userId, function(response, error){
            console.log(response, error);
        });
    });
});

function modalConfirm(title, desc, submit, close){
    $('.modal-confirm-title').html(title);
    $('.modal-confirm-desc').html(desc);

    // $('.modal-confirm-submit').unbind('click');
    // $('.modal-confirm-close').unbind('click');

    $('.modal-confirm-close').click(function(){
        if (close) {
            close();
        }
        $('.modal-confirm').hide();
        $('.modal-confirm-close').unbind('click');
    });

    $('.modal-confirm-submit').click(function(){
        if (submit) {
            submit();
        }
        $('.modal-confirm').hide();
        $('.modal-confirm-submit').unbind('click');
    });

    $('.modal-confirm').show();
}

function modalInput(title, desc, submit, close){
    $('.modal-input-title').html(title);
    $('.modal-input-desc').html(desc);

    // $('.modal-confirm-submit').unbind('click');
    // $('.modal-confirm-close').unbind('click');

    $('.modal-input-box-elem').keydown(function(e){
        var keyCode = e.which;
        switch(keyCode) {
            case 13: // enter
                if (submit) {
                  submit($('.modal-input-box-elem').val());
                }
                $('.modal-input').hide();
                $('.modal-input-close').unbind('click');
                $('.modal-input-box-elem').unbind('keydown');
                break;
            case 27: // esc
                if (close) {
                    close();
                }
                $('.modal-input').hide();
                $('.modal-input-close').unbind('click');
                $('.modal-input-box-elem').unbind('keydown');
                break;
        }
    });

    $('.modal-input-close').click(function(){
        if (close) {
            close();
        }
        $('.modal-input').hide();
        $('.modal-input-close').unbind('click');
    });

    $('.modal-input-submit').click(function(){
        if (submit) {
            submit($('.modal-input-box-elem').val());
        }
        $('.modal-input').hide();
        $('.modal-input-submit').unbind('click');
    });

    $('.modal-input').show(0, function(){
        $('.modal-input-box-elem').focus();
    });
}

function getChannelList(isFirstPage) {
    if(isFirstPage) {
        $('.modal-open-chat-list').html('');
        OpenChannelListQuery = sb.OpenChannel.createOpenChannelListQuery();
    }

    if (OpenChannelListQuery.hasNext) {
        OpenChannelListQuery.next(function(channels, error){
            if (error) {
                return;
            }

            $('.modal-open-chat-list').append(createChannelList(channels));
            channelListPage = channels['page'];
            if (channels['next'] != 0) {
                $('.modal-open-chat-more').show();
            } else {
                $('.modal-open-chat-more').hide();
            }
            $('.modal-open-chat').show();
        });
    }
}

function createChannelList(channels) {
    var channelListHtml = '';

    for (var i in channels) {
        var channel = channels[i];
        var item = '<div class="modal-open-chat-list__item" onclick="joinChannel(\'%channelUrl%\')">%channelImage% &nbsp;%channelName%</div>';
        item = item.replace(/%channelUrl%/, channel.url).replace(/%channelName%/, xssEscape(channel.name));
        item = item.replace(/%channelImage%/, '<img src="'+channel.coverUrl+'" /> ');

        channelListHtml += item;
    }
    return channelListHtml;
}

function joinChannel(channelUrl) {
    if (channelUrl == currChannelUrl) {
        navInit();
        popupInit();
        return false;
    }

    PreviousMessageListQuery = null;
    sb.OpenChannel.getChannel(channelUrl, function(channel, error){
        if (error) {
            return;
        }

        channel.enter(function(response, error){
            if (error) {
                if (error.code == 900100) {
                    alert('Oops...You got banned out from this channel.');
                }
                return;
            }

            $('.chat-top__button-hide').hide();

            currChannelInfo = channel;
            currChannelUrl = channelUrl;

            $('.chat-empty').hide();
            initChatTitle(xssEscape(currChannelInfo.name), 0);

            $('.chat-canvas').html('');
            $('.chat-input-text__field').val('');
            $('.chat').show();

            navInit();
            popupInit();

            isOpenChat = true;
            loadMoreChatMessage(scrollPositionBottom);
            setWelcomeMessage(xssEscape(currChannelInfo.name));
            addChannel();
        });
    });
}

function addChannel() {
    if ($('.left-nav-channel-open').length == 0) {
        $('.left-nav-channel-empty').hide();
    }

    $.each($('.left-nav-channel'), function(index, channel) {
        $(channel).removeClass('left-nav-channel-open--active');
        $(channel).removeClass('left-nav-channel-messaging--active');
        $(channel).removeClass('left-nav-channel-group--active');
    });

    var addFlag = true;
    $.each($('.left-nav-channel-open'), function(index, channel) {
        if (currChannelUrl == $(channel).data('channel-url')) {
            $(channel).addClass('left-nav-channel-open--active');
            addFlag = false;
        }
    });

    if(addFlag) {
        $('#open_channel_list').append(
            '<div class="left-nav-channel left-nav-channel-open left-nav-channel-open--active" ' +
            '     onclick="joinChannel(\'' + currChannelInfo.url + '\')"' +
            '     data-channel-url="' + currChannelInfo.url + '"' +
            '>' +
            (currChannelInfo.name.length > 12 ? xssEscape(currChannelInfo.name.substring(0, 12)) + '...' : xssEscape(currChannelInfo["name"])) +
            '</div>'
        );
    }

    $('.modal-guide-create').hide();
    $('.left-nav-button-guide').hide();
}

function leaveChannel(channel, obj) {
    popupInit();

    leaveChannelUrl = channel['channel_url'];

    if($('.chat-top__button-invite').is(':visible')) {
        $('.modal-leave-channel-desc').html('Do you want to leave this messaging channel?');
    } else {
        $('.modal-leave-channel-desc').html('Do you want to leave this channel?');
    }

    $('.modal-leave-channel').show();
    return false;
}

$('.chat-top__button-leave').click(function() {
    if($('.chat-top__button-invite').is(':visible')) {
        endMessaging(currChannelInfo, $(this));
    } else {
        leaveChannel(currChannelInfo, $(this));
    }
});

$('.chat-top__button-hide').click(function() {
    if (currChannelInfo.isOpenChannel()) {
        return;
    }

    hideChannel(currChannelInfo);
});

$('.chat-top__button-member').click(function() {
    if ($('.modal-member').is(":visible")) {
        $(this).removeClass('chat-top__button-member--active');
        $('.modal-member').hide();
    } else {
        popupInit();
        $(this).addClass('chat-top__button-member--active');
        getMemberList(currChannelInfo);
        $('.modal-member').show();
    }
});

$('.chat-top__button-invite').click(function() {
    if ($('.modal-invite').is(":visible")) {
        $(this).removeClass('chat-top__button-invite--active');
        $('.modal-invite').hide();
    } else {
        popupInit();
        $(this).addClass('chat-top__button-invite--active');
        getUserList();
        $('.modal-invite').show();
    }
});

function getMemberList(channel) {
    if (channel.isOpenChannel()) {
        OpenChannelParticipantListQuery = channel.createParticipantListQuery(channel);
        OpenChannelParticipantListQuery.next(function (members, error) {
            if (error) {
                return;
            }

            $('.modal-member-list').html('');
            var memberListHtml = '';
            members.forEach(function (member) {
                memberListHtml += '' +
                    '<div class="modal-member-list__item">' +
                    '<div class="modal-member-list__icon modal-member-list__icon--online"></div>' +
                    '  <div class="modal-member-list__name">' +
                    (member.nickname.length > 13 ? xssEscape(member.nickname.substring(0, 12)) + '...' : xssEscape(member.nickname)) +
                    '  </div>' +
                    '</div>';
            });
            $('.modal-member-list').html(memberListHtml);
        });
    }

    if (channel.isGroupChannel()) {
        var members = channel.members;
        $('.modal-member-list').html('');

        var memberListHtml = '';
        members.forEach(function (member) {
            if (member.connectionStatus == 'online') {
                var isOnline = 'online';
                var dateTimeString = '';
            } else {
                var isOnline = '';
                var dateTime = new Date(member.lastSeenAt);
                var dateTimeString = (dateTime.getMonth()+1) + '/' + dateTime.getDate() + ' ' + dateTime.getHours() + ':' + dateTime.getMinutes();
            }

            memberListHtml += '' +
                '<div class="modal-member-list__item">' +
                '<div class="modal-member-list__icon"><img src="'+member.profileUrl+'" /></div>' +
                '  <div class="modal-member-list__name '+isOnline+'">' +
                (member.nickname.length > 13 ? xssEscape(member.nickname.substring(0, 12)) + '...' : xssEscape(member.nickname)) +
                '  </div>' +
                '<div class="modal-member-list__lastseenat">'+dateTimeString+'</div>' +
                '</div>';
        });
        $('.modal-member-list').html(memberListHtml);
    }
}

$('.modal-leave-channel-close').click(function() {
    $('.modal-leave-channel').hide();
    leaveChannelUrl = '';

    return false;
});

$('.modal-leave-channel-submit').click(function() {
    $('#open_channel_list').removeClass('chat-top__button-leave--active');
    leaveCurrChannel();
});

$('.modal-hide-channel-close').click(function() {
    $('.modal-hide-channel').hide();
    leaveChannelUrl = '';
    return false;
});

$('.modal-hide-channel-submit').click(function() {
    // $('#open_channel_list').removeClass('chat-top__button-leave--active');
    // leaveCurrChannel();
    hideCurrChannel();
});
/***********************************************
 *              // END OPEN CHAT
 **********************************************/


/***********************************************
 *                MESSAGING
 **********************************************/
/*$('.title-messaging').click(function() {
  $('.modal-dialog').show();
});*/

$('#btn_messaging_chat').click(function() {
    popupInit();
    $('#myModal').modal('hide');
    $('#modal-dialog').hide();
    $('.modal-guide-create').hide();
    $('.left-nav-button-guide').hide();
    $('.modal-open-chat').hide();
    $('#btn_open_chat').removeClass('left-nav-open--active');

    if ($(this).hasClass('left-nav-messaging--active')) {
        $('.right-section__modal-bg').hide();
        $(this).removeClass('left-nav-messaging--active');
        $('.modal-messaging').hide();
    } else {
        $('#myModal').modal('hide');
        $('#modal-dialog').hide();
        $('.right-section__modal-bg').show();
        $('.modal-guide-create').hide();
        $('.left-nav-button-guide').hide();
        $('.modal-open-chat').hide();
        // $(this).addClass('left-nav-messaging--active');
        userListToken = '';
        userListNext = 0;
        $('.modal-messaging-list').html('');
        getUserList(true);
        $('#myModal').modal('hide');
        $('#modal-dialog').hide();
        $('.modal-messaging').show();
    }
});

$('.shared_messaging_chat').click(function(event) {
    /*  var url = $(".shared_messaging_chat").attr('data-url');
    $(".chat-input-text__field").val(url);*/
    event.stopPropagation();
    event.stopImmediatePropagation();
    // alert("here123123");
    // $(".right-section").show();
    popupInit();
    $('.modal-guide-create').hide();
    $('.left-nav-button-guide').hide();
    $('.modal-open-chat').hide();
    $('#btn_open_chat').removeClass('left-nav-open--active');
    $('.modal-messaging').show();
    if ($(this).hasClass('left-nav-messaging--active')) {
        $('.right-section__modal-bg').hide();
        $(this).removeClass('left-nav-messaging--active');
        //$('.modal-messaging').hide();
    } else {
        $('.right-section__modal-bg').show();
        $(this).addClass('left-nav-messaging--active');
        userListToken = '';
        userListNext = 0;
        $('.modal-messaging-list').html('');
        getUserList(true);
        //$('.modal-messaging').show();
    }
});

$('#btn_messaging_chatz').click(function() {
    // alert("here");return;
    $('#myModal').modal('hide');
    $(".chat-widget").hide();
    popupInit();
    $('.modal-messaging').show();
    $('.modal-guide-create').hide();
    $('.left-nav-button-guide').hide();
    $('.modal-open-chat').hide();
    $('#btn_open_chat').removeClass('left-nav-open--active');

    if ($(this).hasClass('left-nav-messaging--active')) {
        $('.right-section__modal-bg').hide();
        $(this).removeClass('left-nav-messaging--active');
        //$('.modal-messaging').hide();
    } else {
        $('.right-section__modal-bg').show();
        $(this).addClass('left-nav-messaging--active');
        userListToken = '';
        userListNext = 0;
        $('.modal-messaging-list').html('');
        getUserList(true);
        //$('.modal-messaging').show();
    }
});

function getUserList(isFirstPage) {
    if (isFirstPage) {
        $('.modal-messaging-list').html('');
        UserListQuery = sb.createUserListQuery();
    }

    if (UserListQuery.hasNext) {
        UserListQuery.next(function(userList, error){
            if (error) {
                return;
            }

            var users = userList;
            console.log(users);
            $('.modal-messaging-more').remove();
            // getAjaxuserList();
            $.each(users, function(index, user) {
                UserList[user.userId] = user;
                if (!isCurrentUser(user.userId)) {
                    $('.modal-messaging-list').append(
                        '<div class="modal-messaging-list__item" onclick="userClick($(this))">' +
                        (user.nickname.length > 12 ? xssEscape(user.nickname.substring(0, 12)) + '...' : xssEscape(user.nickname)) +
                        '  <div class="modal-messaging-list__icon" data-guest-id="' + user.userId + '"></div>' +
                        '</div>'
                );
            }
        });

        if (UserListQuery.hasNext) {
            $('.modal-messaging-list').append(
                '<div class="modal-messaging-more" onclick="userListLoadMore()">MORE<div class="modal-messaging-more__icon"></div></div>'
            );
        } else {
            $('.modal-messaging-more').remove();
        }
    });
    }
}


function channel(){
    sb = new SendBird({
        appId: appId
    });

    var channelListQuery = sb.GroupChannel.createMyGroupChannelListQuery();
    channelListQuery.includeEmpty = true;
    channelListQuery.limit = 100; // pagination limit could be set up to 100

    if (channelListQuery.hasNext) {
        channelListQuery.next(function(channelList, error){
            if (error) {
                console.error(error);
                return;
            }
            console.log(channelList);
        });
    }
}

function sendMultiple(uuidArray) {
    // toConenctUid = $('#user-userId').val();
    console.log("send sendMultiple");
    console.log(uuidArray);

    sb = new SendBird({
        appId: appId
    });

    sb.GroupChannel.createChannelWithUserIds(uuidArray, true, function(createdChannel, error){
        if (error) {
            console.error(error);
            alert("Error occurred while fetching user!");
            return;
        } else {
            console.log("createdChannel");
            console.log(createdChannel);
            $('#newMessagesModal').on('hidden.bs.modal', function () {
                $('body').addClass('modal-open');
            });
            $('#newMessagesModal').modal('hide');

            var channelMemberList = '';
            var members = createdChannel.members;

            members.forEach(function(member){
                if (currentUser.userId != member.userId) {
                    channelMemberList += xssEscape(member.nickname) + ', ';
                }
            });

            channelMemberList = channelMemberList.slice(0, -2);
            addGroupChannel(true, channelMemberList, createdChannel);

            //joinPersonalChannel(createdChannel.url);
            joinGroupChannel(createdChannel.url);
        }
    });
}


function test(){
    currentUserId = $('#current_user_sb_at').val();
    toConenctUid = $('#loggedin_user_sb_at').val();
    userId = toConenctUid;
    sb = new SendBird({
        appId: appId
    });
    sb.connect(userId, function(user, error) {
        if(!error){
            var startMessagingProcess = function(){
                console.log(JSON.stringify(user));
                PreviousMessageListQuery = null;
                console.log("currentUserId");
                console.log(currentUserId);
                sb.GroupChannel.createChannelWithUserIds([user.userId,currentUserId], true, function(createdChannel, error){
                    if (error) {
                        console.error(error);
                        return;
                    } else {
                        console.log("createdChannel");
                        console.log(createdChannel);
                        joinPersonalChannel(createdChannel.url,currentUserId);
                    }
                });
            };

            startMessagingProcess();
            return;
        } else {
            console.log("testFunction::"+error);
        }
    });
}

function joinPersonalChannel(channelUrl,currentUserId, callback) {
    if (channelUrl == currChannelUrl) {
        navInit();
        popupInit();
        $.each($('.left-nav-channel'), function(index, channel) {
            if ($(channel).data('channel-url') == channelUrl) {
                $(channel).find('div[class="left-nav-channel__unread"]').remove();
            }
        });
        return false;
    }

    PreviousMessageListQuery = null;
    sb.GroupChannel.getChannel(channelUrl, function(channel, error){
        console.log("channel-data:::");
        console.log(channel);
        if (error) {
            console.error(error);
            return;
        }

        channel.markAsRead();

        currChannelInfo = channel;
        currChannelUrl = channelUrl;
        console.log("channel.members");
        console.log(channel.members);
        var members = channel.members;
        var channelTitle = '';

        channel.refresh(function(){
            // TODO
        });

        $.each(members, function(index, member) {
            if (!isCurrentUser(member.userId)) {
                channelTitle += xssEscape(member.nickname) + ', ';
            }
        });

        channelTitle = channelTitle.slice(0,-2);
        var channelMemberList = channelTitle;
        if (channelTitle.length > 20) {
            channelTitle = channelTitle.substring(0, 20);
            channelTitle += '...';
        }
        var titleType = 1;
        var isGroup = false;
        if (members.length > 2) {
            channelTitle += '({})'.format(members.length-1);
            titleType = 2;
            isGroup = true;
        }

        $('.chat-empty').hide();
        console.log('channelTitle:'+channelTitle);
        console.log('titleType:'+titleType);
        initChatTitle(channelTitle, titleType);
        $('.chat-canvas').html('');
        $('.chat-input-text__field').val('');
        $('.chat').show();
        $('.message-popup').show();

        navInit();
        popupInit();
        makeMemberList(members);

        isOpenChat = false;
        loadMoreChatMessage(scrollPositionBottom);
        setWelcomeMessage('Group Channel');
        addGroupChannel(isGroup, channelMemberList, currChannelInfo);
        $('.chat-input-text__field').attr('disabled', false);

        $('.chat-top__button-hide').show();
        if (callback) {
            callback();
        }
    });
    return;
}

function getAjaxuserList() {
    var accessToken = $("#userAccessToken").val();
    var action = "http://demo2.ongraph.com/demo/twitter/getUserList.php";
    $.ajax({
        url:action,
        data: {'accessToken':accessToken,
            'user_privacy_message':"public"},
        type: 'Post',

        success: function(data) {
            var res = jQuery.parseJSON(data);
            // var html = '';
            console.log(res);
            if(res.success == true) {
                console.log(res);

                /*$.each(res, function (i) {
                    alert(res[i].user_id);return;
                });*/
                $.each(res, function(i,user) {
                    UserList[user.uuid] = user;
                    if (!isCurrentUser(user.uuid)) {
                        $('.modal-messaging-list').append(
                            '<div class="modal-messaging-list__item" onclick="userClick($(this))">' +
                            (res[i].user_name.length > 12 ? xssEscape(res[i].user_name.substring(0, 12)) + '...' : xssEscape(res[i].user_name)) +
                            '  <div class="modal-messaging-list__icon" data-guest-id="' + res[i].uuid + '"></div>' +
                            '</div>'
                        );
                    }
                });
            }
        }
    });
}

function userListLoadMore() {
    getUserList(false);
}

function userClick(obj) {
    var el = obj.find('div');
    if (el.hasClass('modal-messaging-list__icon--select')) {
        el.removeClass('modal-messaging-list__icon--select');
    } else {
        el.addClass('modal-messaging-list__icon--select');
    }

    var selectCount = $('.modal-messaging-list__icon--select').length;
    if (selectCount > 1) {
        $('.modal-messaging-top__title').html('Group Chat ({})'.format(selectCount));
    } else {
        $('.modal-messaging-top__title').html('Group Channel');
    }
}

function startMessaging() {
    if ($('.modal-messaging-list__icon--select').length == 0) {
        alert('Please select user');
        return false;
    }

    var startMessagingProcess = function(){
        var users = [];
        $.each($('.modal-messaging-list__icon--select'), function(index, user) {
            console.log($(user).data("guest-id"));
            users.push(UserList[$(user).data("guest-id")]);
        });

        PreviousMessageListQuery = null;
        console.log(users);
        sb.GroupChannel.createChannel(users, isDistinct, 'test_name', '', '', function(channel, error){
            if (error) {
                return;
            }

            currChannelInfo = channel;
            currChannelUrl = channel.url;

            var members = channel.members;
            var channelTitle = '';

            $.each(members, function(index, member) {
                if (!isCurrentUser(member.userId)) {
                    channelTitle += xssEscape(member.nickname) + ', ';
                }
            });

            channelTitle = channelTitle.slice(0,-2);
            var channelMemberList = channelTitle;
            if (channelTitle.length > 20) {
                channelTitle = channelTitle.substring(0, 20);
                channelTitle += '...'
            }
            var titleType = 1;
            var isGroup = true;
            if(members.length > 2) {
                channelTitle += '({})'.format(members.length);
                titleType = 2;
            }

            $('.chat-empty').hide();
            initChatTitle(channelTitle, titleType);
            $('.chat-canvas').html('');
            $('.chat-input-text__field').val('');
            $('.chat').show();
            $(".message-popup").show();

            navInit();
            popupInit();
            makeMemberList(members);

            isOpenChat = false;
            loadMoreChatMessage(scrollPositionBottom);
            setWelcomeMessage('Messaging Channel');
            addGroupChannel(true, channelMemberList, currChannelInfo);
            $('.chat-input-text__field').attr('disabled', false);
        });
    };

    var isDistinct;
    modalConfirm('Create Channel', 'Do you want to create distinct channel?', function(){
        isDistinct = true;
        startMessagingProcess();
    }, function(){
        isDistinct = false;
        startMessagingProcess();
    });

    return;
}

function deleteChannel(channel) {
    var channelUrl = channel.url;

    if (channel.isGroupChannel()) {
        $('.left-nav-channel-group[data-channel-url='+channelUrl+']').remove();
        delete(groupChannelLastMessageList[channelUrl]);
    }

    if (channel.isOpenChannel()) {
        $('.left-nav-channel-open[data-channel-url='+channelUrl+']').remove();
    }

    try {
        delete(channelMessageList[channelUrl]);
    } catch (e) {
        // pass
    }

    if (channel == currChannelInfo) {
        leaveCurrChannel();
    }
}

function hideCurrChannel(){
    console.log('hideCurrChannel called');
    if (currChannelInfo.isGroupChannel()) {
        currChannelInfo.hide(function(response, error){
            if (error) {
                return;
            }

            // $('.chat-canvas').html('');
            $('.chat-input-text__field').val('');
            $('.chat').hide();
            initChatTitle('', -1);
            $('.chat-empty').show();

            $('.left-nav-channel-messaging--active').remove();
            $('.left-nav-channel-group--active').remove();

            $('.modal-hide-channel').hide();
            channelListPage = 0;
            currChannelUrl = null;
            currChannelInfo = null;
            leaveMessagingChannelUrl = '';

            $('.chat-input-text__field').attr('disabled', true);
        });
    }
}

function leaveCurrChannel(){
    console.log('leaveCurrChannel called');
    if (currChannelInfo.isOpenChannel()) {
        currChannelInfo.exit(function(response, error){
            if (error) {
                return;
            }
            $('.chat-input-text__field').val('');
            $('.chat').hide();
            initChatTitle('', -1);
            $('.chat-empty').show();

            $('.left-nav-channel-open--active').remove();

            if ($('.left-nav-channel-open').length == 0) {
                $('.left-nav-channel-empty').show();
            }

            $('.modal-leave-channel').hide();
            channelListPage = 0;
            currChannelUrl = null;
            currChannelInfo = null;
            leaveChannelUrl = '';

            $('.chat-input-text__field').attr('disabled', true);
        });
    } else if (currChannelInfo.isGroupChannel()) {
        currChannelInfo.leave(function(response, error){
            if (error) {
                return;
            }

            $('#conversationSettingsModal').on('hidden.bs.modal', function () {
                $('body').addClass('modal-open');
            });
            $('#conversationSettingsModal').modal('hide');

            /*$('.left-nav-channel-messaging--active').remove();
            $('.left-nav-channel-group--active').remove();*/
            $('.DMInbox-conversationItem .DMInboxItem[data-channel-url="' + currChannelUrl + '"]').parent().remove();
            channelListPage = 0;
            currChannelUrl = null;
            currChannelInfo = null;
            leaveMessagingChannelUrl = '';

            $('#myModal').modal('show');
        });
    }
}


function moveToTopGroupChat(channelUrl) {
    $('.left-nav-channel-group[data-channel-url='+channelUrl+']').prependTo('#messaging_channel_list');
}

function updateGroupChannelLastMessage(message) {
    var lastMessageDateString = '';
    var lastMessageImg = '';
    if (message) {
        var calcSeconds = (new Date().getTime() - message.createdAt)/1000;
        var parsedValue;

        if (calcSeconds < 60) {
            parsedValue = parseInt(calcSeconds);
            if (parsedValue == 1) {
                lastMessageDateString = parsedValue + ' sec ago';
            } else {
                lastMessageDateString = parsedValue + ' secs ago';
            }
        } else if (calcSeconds / 60 < 60) {
            parsedValue = parseInt(calcSeconds/60);
            if (parsedValue == 1) {
                lastMessageDateString = parsedValue + ' min ago';
            } else {
                lastMessageDateString = parsedValue + ' mins ago';
            }
        } else if (calcSeconds / (60*60) < 24) {
            parsedValue = parseInt(calcSeconds / (60*60));
            if (parsedValue == 1) {
                lastMessageDateString = parsedValue + ' hour ago';
            } else {
                lastMessageDateString = parsedValue + ' hours ago';
            }
        } else {
            parsedValue = parseInt(calcSeconds/(60*60*24));
            if (parsedValue == 1) {
                lastMessageDateString = parsedValue + ' day ago';
            } else {
                lastMessageDateString = parsedValue + ' days ago';
            }
        }

        if (lastMessageDateString) {
            lastMessageDateString = '<div> <span> ' + lastMessageDateString + '</span></div>';
        }

        if (message.messageType == 'user') {
            $('.DMInboxItem[data-channel-url=' + message.channelUrl + '] .u-posRelative').html(
                '<p class="DMInboxItem-snippet">' +
                    message._sender.nickname + ":&nbsp;" + xssEscape(message.message) +
                '</p>'
            );
        } else if (message.messageType == 'file') {
            if (message.customType == 'videoFile') {
                var videoMsg = '' +
                    '<div class="DMInboxItem-media">\
                        <div class="Sensitive">\
                            <div class="Sensitive-content">\
                                <div class="Media u-chromeOverflowFix">\
                                    <div class="Media-photo u-chromeOverflowFix">\
                                        <a href="#" class="dm-attached-media">\
                                            <div class="FlexEmbed u-borderRadiusInherit" style="padding-bottom: 100%">\
                                                <div class="FlexEmbed-item u-borderRadiusInherit">\
                                                    <video controls>\
                                                        <source src="%lastVideoMsg%" />\
                                                    </video>\
                                                </div>\
                                            </div>\
                                        </a>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <p class="DMInboxItem-snippet is-generated">%lastMessage%</p>';
                videoMsg = videoMsg.replace('%lastVideoMsg%', message.url);
                videoMsg = videoMsg.replace('%lastMessage%', message._sender.nickname + 'sent a photo');
                $('.DMInboxItem[data-channel-url=' + message.channelUrl + '] .u-posRelative').html(videoMsg);
            } else if (message.customType == 'photoFile') {
                var imgMsg = '' +
                    '<div class="DMInboxItem-media">\
                        <div class="Sensitive">\
                            <div class="Sensitive-content">\
                                <div class="Media u-chromeOverflowFix">\
                                    <div class="Media-photo u-chromeOverflowFix">\
                                        <a href="#" class="dm-attached-media">\
                                            <div class="FlexEmbed u-borderRadiusInherit" style="padding-bottom: 100%">\
                                                <div class="FlexEmbed-item u-borderRadiusInherit">\
                                                    <img src="%lastImgMsg%"> \
                                                </div>\
                                            </div>\
                                        </a>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <p class="DMInboxItem-snippet is-generated">%lastMessage%</p>';
                imgMsg = imgMsg.replace('%lastImgMsg%', message.url);
                imgMsg = imgMsg.replace('%lastMessage%', message._sender.nickname + 'sent a photo');
                $('.DMInboxItem[data-channel-url=' + message.channelUrl + '] .u-posRelative').html(imgMsg);
            }
        }

        $('.DMInboxItem[data-channel-url='+message.channelUrl+'] .DMInboxItem-header').html(
            '<div class="DMInboxItem-timestamp>' +
                '<span class="_timestamp js-relative-timestamp" data-include-sec="true" aria-hidden="true">' +
                    lastMessageDateString +
                '</span>' +
            '</div>'
        );
    }
}

function updateGroupChannelListAll(){
    for (var i in groupChannelLastMessageList) {
        var message = groupChannelLastMessageList[i];
        updateGroupChannelLastMessage(message);
    }
}


function addGroupChannel(isGroup, channelMemberList, targetChannel) {
    console.log(targetChannel);
    console.log(channelMemberList);
    if (isGroup) {
        groupChannelLastMessageList[targetChannel.url] = targetChannel.lastMessage;
    }

    $.each($('.left-nav-channel'), function(index, channel) {
        $(channel).removeClass('left-nav-channel-open--active');
        $(channel).removeClass('left-nav-channel-messaging--active');
        $(channel).removeClass('left-nav-channel-group--active');
    });

    var addFlag = true;
    $.each($('.left-nav-channel-messaging'), function(index, channel) {
        if (currChannelUrl == $(channel).data('channel-url')) {
            $(channel).addClass('left-nav-channel-messaging--active');
            $(channel).find('div[class="left-nav-channel__unread"]').remove();
            addFlag = false;
        }
    });
    $.each($('.left-nav-channel-group'), function(index, channel) {
        if (currChannelUrl == $(channel).data('channel-url')) {
            $(channel).addClass('left-nav-channel-group--active');
            $(channel).find('div[class="left-nav-channel__unread"]').remove();
            addFlag = false;
        }
    });

    if (channelMemberList.length > 9) {
        channelMemberList = channelMemberList.substring(0, 9) + '...';
    }

    targetAddGroupChannel = targetChannel;
    if (addFlag && !isGroup) {
        $('#messaging_channel_list').append(
            '<div class="left-nav-channel left-nav-channel-messaging left-nav-channel-messaging--active" ' +
            '     onclick="joinGroupChannel(\'' + targetChannel.url + '\')"' +
            '     data-channel-url="' + targetChannel.url + '"' +
            '><img src="https://sendbird.com/main/img/profiles/profile_17_512px.png" ><div class="user-details">'+
            channelMemberList +
            '</div></div>'
        );

        groupChannelListMembersAndProfileImageUpdate(targetChannel);
    } else if (addFlag && isGroup) {
        var channelCoverUrl = '/content/themes/default/images/blank_group.jpg';
        var members = targetChannel.members;
        $.each(members, function(index, member) {
            if (!isCurrentUser(member.userId)) {
                if (members.length == 2) {
                    channelCoverUrl = member.profileUrl;
                }
            }
        });

        var messaging_channel_list_html =
            '<li class="DMInbox-conversationItem" ' +
            'data-channel-url="' + targetChannel.url + '">' +
            '<div class="DMInboxItem"' +
                'onclick="joinGroupChannel(\'' + targetChannel.url + '\')"' +
                'data-channel-url="' + targetChannel.url + '">' +
                '<div class="DMInboxItem-avatar">' +
                    '<a href="#">' +
                        '<div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">' +
                            '<span class="DMAvatar-container">' +
                                '<img class="DMAvatar-image" src="' + channelCoverUrl + '">' +
                            '</span>' +
                        '</div>' +
                    '</a>' +
                '</div>' +
                '<div class="DMInboxItem-title account-group">' + channelMemberList +'</div>' +

                '<div class="DMInboxItem-header"></div>' +
                '<div class="u-posRelative">' +
                    '<p class="DMInboxItem-snippet">&nbsp;</p>'
                '</div>' +
            '</div>' +
            '</li>';
        $('ul.DMInbox-conversations').append(messaging_channel_list_html);
        targetAddGroupChannel = null;
        groupChannelListMembersAndProfileImageUpdate(targetChannel);
    }

    $('.modal-guide-create').hide();
    $('.left-nav-button-guide').hide();
}

function groupChannelListMembersAndProfileImageUpdate(targetChannel){
    // select profileImage
    var members = targetChannel.members;
    // console.log(members);

    var membersProfileImageUrl = [];
    var membersNickname = '';
    for (var i in members){
        var member = members[i];
        if (sb.currentUser.userId != member.userId) {
            membersProfileImageUrl.push(member.profileUrl);
            membersNickname += xssEscape(member.nickname) + ', ';
        }
    }
    membersNickname = membersNickname.substring(0, membersNickname.length - 2);

    if (membersNickname.length > 22) {
        membersNickname = membersNickname.substring(0, 22) + '...';
    }

    // console.log(membersProfileImageUrl);
    var selectSequence = parseInt(Math.random() * membersProfileImageUrl.length);
    // console.log(selectSequence);
    var selectedProfileImageUrl = membersProfileImageUrl[selectSequence];
    // console.log(selectedProfileImageUrl);

    var targetElem = $('.DMInboxItem[data-channel-url='+targetChannel.url+']');

    //targetElem.css('background-image', 'url('+selectedProfileImageUrl+')');

    // member nickname update
    targetElem.find('.DMInboxItem-title').html(
        '<b class="fullname">' + membersNickname + '</b>' +
        '<span class="UserBadges"></span>' +
        '<span class="UserNameBreak">&nbsp;</span>'
    );
}

function joinGroupChannel(channelUrl, callback) {
    //console.log('joinGroupChannel:', currChannelUrl, ' : ', channelUrl);

	$('#custom-editor').val('');

	$('html').css({'position':'fixed'});

    $('#myModal').on('hidden.bs.modal', function () {
        $('body').addClass('modal-open');
    });
    $('#myModal').modal('hide');

    if (!$('.main-header .nav>li.js_live-messages>a .label').hasClass('hidden')) {
        $('.main-header .nav>li.js_live-messages>a .label').html('0');
        $('.main-header .nav>li.js_live-messages>a .label').addClass('hidden');
    }
    $('.DMInbox-conversationItem .DMInboxItem[data-channel-url="' + channelUrl + '"]').removeClass('is--unread');

    if (channelUrl == currChannelUrl) {
        $('#chatModal').modal('show');
        navInit();
        popupInit();
        $.each($('.left-nav-channel'), function(index, channel) {
            if ($(channel).data('channel-url') == channelUrl) {
                $(channel).find('div[class="left-nav-channel__unread"]').remove();
            }
        });
        return false;
    }

    PreviousMessageListQuery = null;
    sb.GroupChannel.getChannel(channelUrl, function(channel, error){
        if (error) {
            console.error(error);
            return;
        }
        channel.markAsRead();

        currChannelInfo = channel;
        currChannelUrl = channelUrl;

        var members = channel.members;
        var channelTitle = '';
        var chatTitleUrl = '/content/themes/default/images/blank_group.jpg';

        channel.refresh(function(){ });

        $.each(members, function(index, member) {
            if (!isCurrentUser(member.userId)) {
                channelTitle += xssEscape(member.nickname) + ', ';
                if (members.length == 2) {
                    chatTitleUrl = member.profileUrl;
                }
            }
        });

        channelTitle = channelTitle.slice(0,-2);
        var channelMemberList = channelTitle;
        if (channelTitle.length > 20) {
            channelTitle = channelTitle.substring(0, 20);
            channelTitle += '...';
        }
        var titleType = 1;
        var isGroup = false;
        if (members.length > 2) {
            channelTitle += '({})'.format(members.length-1);
            titleType = 2;
            isGroup = true;
        }
        $('.chat-empty').hide();
        initChatTitle(channelTitle, titleType, chatTitleUrl);
        $('.DMConversation-content').html('');
        $('.DMComposer-editor.rich-editor').html('<div><br></div>');
        $('#chatModal').modal('show');
        $('body').addClass('modal-open');
        loadMoreChatMessage(scrollPositionBottom);
        /*
        navInit();
        popupInit();
        makeMemberList(members);

        isOpenChat = false;
        loadMoreChatMessage(scrollPositionBottom);
        setWelcomeMessage('Group Channel');
        addGroupChannel(isGroup, channelMemberList, currChannelInfo);
        $('.chat-input-text__field').attr('disabled', false);

        $('.chat-top__button-hide').show();
        if (callback) {
            callback();
        }*/
    });

    return;
}

function endMessaging(channel, obj) {
    popupInit();
    leaveMessagingChannelUrl = channel.url;
    $('.modal-leave-channel-desc').html('Do you want to leave this messaging channel?');
    $('.modal-leave-channel').show();
    return false;
}

function hideChannel(channel) {
    popupInit();
    hideChannelUrl = channel.url;
    $('.modal-hide-channel-desc').html('Do you want to hide this channel?');
    $('.modal-hide-channel').show();
    return false;
}

function inviteMember() {
    if ($('.modal-messaging-list__icon--select').length == 0) {
        alert('Please select user');
        return false;
    }

    var userIds = [];
    $.each($('.modal-messaging-list__icon--select'), function(index, user) {
        if ($(user).data("guest-id")) {
            // console.log("here");return;
            console.log($(user).data("guest-id"));
            userIds.push($(user).data("guest-id"));
        }
    });

    currChannelInfo.inviteWithUserIds(userIds, function(response, error) {
        if (error) {
            return;
        }

        popupInit();
    });
}

function getGroupChannelList() {
    GroupChannelListQuery.next(function(channels, error){
        if (error) {
            return;
        }

        channels.forEach(function(channel){
            PreviousMessageListQuery = channel.createPreviousMessageListQuery();
            PreviousMessageListQuery.load(1, false, function(messages, error) {
                if (error) {
                    return;
                }

                if (messages.length == 0) return;

                var channelMemberList = '';
                var members = channel.members;

                members.forEach(function(member){
                    if (currentUser.userId != member.userId) {
                        channelMemberList += xssEscape(member.nickname) + ', ';
                    }
                });

                channelMemberList = channelMemberList.slice(0, -2);
                addGroupChannel(true, channelMemberList, channel);

                $.each($('.left-nav-channel'), function(index, item) {
                    $(item).removeClass('left-nav-channel-messaging--active');
                    $(item).removeClass('left-nav-channel-group--active');
                });

                var targetUrl = channel.url;
                //var unread = channel.unreadMessageCount > 9 ? '9+' : channel.unreadMessageCount;
                if (channel.unreadMessageCount > 0) {
                    $.each($('.DMInbox-conversationItem .DMInboxItem'), function(index, item) {
                        if ($(item).data("channel-url") == targetUrl) {
                            $(item).addClass('is--unread');
                        }
                    });
                }
            });
        });

        notifyTotalUnreadCount();
    });
}

function makeMemberList(members) {
    var item = {};
    // Clear memberList before updating it
    memberList = [];
    $.each(members, function(index, member) {
        item = {};
        if (!isCurrentUser(member['user_id'])) {
            item["user_id"] = member["user_id"];
            item["name"] = xssEscape(member["name"]);
            memberList.push(item);
        }
    });
}
/***********************************************
 *            // END MESSAGING
 **********************************************/


/***********************************************
 *            SendBird Settings
 **********************************************/
var sb;

var GroupChannelListQuery;
var OpenChannelListQuery;
var OpenChannelParticipantListQuery;

var UserListQuery;
var SendMessageHandler;

var UserList = {};
var isInit = false;

var channelMessageList = {};
var groupChannelLastMessageList = {};

function startSendBird(userId,accessToken, nickName) {
    sb = new SendBird({
        appId: appId
    });

    sb.connect(userId,accessToken, function(user, error) {
        if (error) {
            return;
        } else {
            initPage(user);
        }
    });

    var initPage = function(user){
        isInit = true;
        $('.init-check').hide();

        currentUser = user;
        sb.updateCurrentUserInfo(nickName, '', function(response, error) {
            // console.log(response, error);
        });

        GroupChannelListQuery = sb.GroupChannel.createMyGroupChannelListQuery();
        OpenChannelListQuery = sb.OpenChannel.createOpenChannelListQuery();
        UserListQuery = sb.createUserListQuery();

        GroupChannelListQuery.limit = 100;
        GroupChannelListQuery.includeEmpty = true;
        OpenChannelListQuery.limit = 100;

        UserListQuery.limit = 100;

        getGroupChannelList();

        setTimeout(function(){
            updateGroupChannelListAll();
            setInterval(function(){
                updateGroupChannelListAll();
            }, 1000);
        }, 500);
    };

    var ConnectionHandler = new sb.ConnectionHandler();
    ConnectionHandler.onReconnectStarted = function(id) {
        console.log('onReconnectStarted');
    };

    ConnectionHandler.onReconnectSucceeded = function(id) {
        console.log('onReconnectSucceeded');
        if (!isInit) {
            initPage();
        }

        // OpenChannel list reset
        if ($('.right-section__modal-bg').is(':visible')) {
            var withoutCache = true;
            getChannelList(withoutCache);
        }

        // GroupChannel list reset
        GroupChannelListQuery = sb.GroupChannel.createMyGroupChannelListQuery();
        $('#messaging_channel_list').html('');
        getGroupChannelList();

        setTimeout(function(){
            updateGroupChannelListAll();
            setInterval(function(){
                updateGroupChannelListAll();
            }, 1000);
        }, 500);
    };

    ConnectionHandler.onReconnectFailed = function(id) {
        console.log('onReconnectFailed');
    };
    sb.addConnectionHandler('uniqueID', ConnectionHandler);

    var ChannelHandler = new sb.ChannelHandler();
    ChannelHandler.onMessageReceived = function(channel, message){
        console.log('onMessageReceived');
        var isCurrentChannel = false;

        if (currChannelInfo == channel) {
            isCurrentChannel = true;
        }

        channel.refresh(function(){
        });

        // Move channel to top.
        var curr = $('.DMInbox-conversationItem[data-channel-url=' + message.channelUrl + ']');
        curr.prependTo(curr.parent());

        // update last message
        if (channel.isGroupChannel()) {
            groupChannelLastMessageList[channel.url] = message;
            updateGroupChannelLastMessage(message);
            moveToTopGroupChat(channel.url);
        }

        if (isCurrentChannel && channel.isGroupChannel()) {
            channel.markAsRead();
        } else {
            if (channel.isGroupChannel()) {
                unreadCountUpdate(channel);
            }
        }

        if (!document.hasFocus()) {
            notifyMessage(channel, message);
        }

        if (message.isUserMessage() && isCurrentChannel) {
            setChatMessage(message);
        }

        if (message.isFileMessage() && isCurrentChannel) {
            if (message.customType == 'photoFile') {
                setImageMessage(message);
            } else if (message.customType == 'videoFile') {
                setFileMessage(message);
            }
        }

        if (message.isAdminMessage() && isCurrentChannel) {
            setBroadcastMessage(message);
        }
    };

    SendMessageHandler = function(message, error) {
        if (error) {
            if (error.code == 900050) {
                setSysMessage({'message': 'This channel is freeze.'});
                alert('This channel is freeze.');
            } else if(error.code == 900041) {
                setSysMessage({'message': 'You are muted.'});
                alert('You are muted.');
            } else if (error.code == 800180) {
                setSysMessage({'message': 'Video is too large.'});
                alert('Video is too large.');
            }
            console.log('error');
            $('.DMComposer-attachment .ComposerThumbnails').html('');
            $('.DMComposer.tweet-form').removeClass('has-thumbnail');
            $('.DMComposer-mediaPicker input[type="file"]').val('');
            $('.DMComposer-gifSearch input[type="file"]').val('');

            // Enable Attachment Buttons
            $('.DMComposer-mediaPicker button').removeClass('disabled');
            $('.DMComposer-mediaPicker button').attr('disabled', false);
            $('.DMComposer-mediaPicker input[type="file"]').attr('disabled', false);
            $('.DMComposer-gifSearch button').removeClass('disabled');
            $('.DMComposer-gifSearch button').attr('disabled', false);
            $('.DMComposer-gifSearch input[type="file"]').attr('disabled', false);

            $('.DMComposer-send button').attr('aria-disabled', true);
            $('.DMComposer-send button').addClass('disabled');

            $('.DMConversation-sendingStateIndicator').css('display', 'none');
            return;
        }

        // update last message
        console.log(message);
        if (groupChannelLastMessageList.hasOwnProperty(message.channelUrl)) {
            groupChannelLastMessageList[message.channelUrl] = message;
            updateGroupChannelLastMessage(message);
        }

        // Add Message to ChannelMessageList
        updateChannelMessageCache(currChannelInfo, message);

        if (message.isUserMessage()) {
            setChatMessage(message);
        }

        if (message.isFileMessage()) {
            if (message.customType == 'photoFile') {
                setImageMessage(message);
            } else if (message.customType == 'videoFile') {
                setFileMessage(message);
            }
            $('.DMComposer-attachment .ComposerThumbnails').html('');
            $('.DMComposer.tweet-form').removeClass('has-thumbnail');
            $('.DMComposer-mediaPicker input[type="file"]').val('');
            $('.DMComposer-gifSearch input[type="file"]').val('');

            // Enable Attachment Buttons
            $('.DMComposer-mediaPicker button').removeClass('disabled');
            $('.DMComposer-mediaPicker button').attr('disabled', false);
            $('.DMComposer-mediaPicker input[type="file"]').attr('disabled', false);
            $('.DMComposer-gifSearch button').removeClass('disabled');
            $('.DMComposer-gifSearch button').attr('disabled', false);
            $('.DMComposer-gifSearch input[type="file"]').attr('disabled', false);
        }

        $('.DMComposer-send button').attr('aria-disabled', true);
        $('.DMComposer-send button').addClass('disabled');

        $('.DMConversation-sendingStateIndicator').css('display', 'none');
    };

    ChannelHandler.onMessageDeleted = function (channel, messageId) {
        console.log('ChannelHandler.onMessageDeleted: ', channel, messageId);
        delete(channelMessageList[channel.url][messageId]);
        if (channel.url == currChannelInfo.url) {
            $('.DMConversation-content li[data-message-id="' + messageId + '"]').remove();
        }
    };

    ChannelHandler.onReadReceiptUpdated = function (channel) {
        console.log('ChannelHandler.onReadReceiptUpdated: ', channel);
        updateChannelMessageCacheAll(channel);
    };

    ChannelHandler.onTypingStatusUpdated = function (channel) {
        console.log('ChannelHandler.onTypingStatusUpdated: ', channel);

        if (channel == currChannelInfo) {
            showTypingUser(channel);
        }
    };

    ChannelHandler.onUserJoined = function (channel, user) {
        console.log('ChannelHandler.onUserJoined: ', channel, user);
    };

    ChannelHandler.onUserLeft = function (channel, user) {
        console.log('ChannelHandler.onUserLeft: ', channel, user);
        setSysMessage({'message': '"' + xssEscape(user.nickname) + '" user is left.'});

        if (channel.isGroupChannel()){
            groupChannelListMembersAndProfileImageUpdate(channel);
        }
    };

    ChannelHandler.onUserEntered = function (channel, user) {
        console.log('ChannelHandler.onUserEntered: ', channel, user);
    };

    ChannelHandler.onUserExited = function (channel, user) {
        console.log('ChannelHandler.onUserExited: ', channel, user);
    };

    ChannelHandler.onUserMuted = function (channel, user) {
        console.log('ChannelHandler.onUserMuted: ', channel, user);
    };

    ChannelHandler.onUserUnmuted = function (channel, user) {
        console.log('ChannelHandler.onUserUnmuted: ', channel, user);
    };

    ChannelHandler.onUserBanned = function (channel, user) {
        console.log('ChannelHandler.onUserBanned: ', channel, user);
        if (isCurrentUser(user.userId)) {
            alert('Oops...You got banned out from this channel.');
            navInit();
            popupInit();
        } else {
            setSysMessage({'message': '"' + xssEscape(user.nickname) + '" user is banned.'});
        }
    };

    ChannelHandler.onUserUnbanned = function (channel, user) {
        console.log('ChannelHandler.onUserUnbanned: ', channel, user);
        setSysMessage({'message': '"' + xssEscape(user.nickname) + '" user is unbanned.'});
    };

    ChannelHandler.onChannelFrozen = function (channel) {
        console.log('ChannelHandler.onChannelFrozen: ', channel);
    };

    ChannelHandler.onChannelUnfrozen = function (channel) {
        console.log('ChannelHandler.onChannelUnfrozen: ', channel);
    };

    ChannelHandler.onChannelChanged = function (channel) {
        console.log('ChannelHandler.onChannelChanged: ', channel);
        if (channel.isGroupChannel()){
            groupChannelListMembersAndProfileImageUpdate(channel);
        }
        if (currChannelUrl == channel.url) return;
        unreadCountUpdate(channel);
        if ($('#myModal').css('display') == 'none') {
            notifyTotalUnreadCount();
        }
    };

    ChannelHandler.onChannelDeleted = function (channel) {
        console.log('ChannelHandler.onChannelDeleted: ', channel);
        deleteChannel(channel);
    };

    sb.addChannelHandler('channel', ChannelHandler);
}

var showTypingUser = function(channel){
    if (!channel.isGroupChannel()) {
        return;
    }

    if (!channel.isTyping()) {
        $('.chat-input-typing').hide();
        $('.chat-input-typing').html('');
        return;
    }

    var typingUser = channel.getTypingMembers();

    var nicknames = '';
    for (var i in typingUser) {
        var nickname = xssEscape(typingUser[i].nickname);
        nicknames += nickname + ', ';
    }
    if (nicknames.length > 2) {
        nicknames = nicknames.substring(0, nicknames.length-2);
        $('.chat-input-typing').html('{} typing...'.format(nicknames));
        $('.chat-input-typing').show();
    } else {
        $('.chat-input-typing').hide();
        $('.chat-input-typing').html('');
    }
};


/***********************************************
 *          // END SendBird Settings
 **********************************************/


/***********************************************
 *              Common Function
 **********************************************/
function initChatTitle(title, index, coverUrl) {
    $('.DMConversation .DMUpdateAvatar .DMUpdateAvatar-avatar').html(
        '<div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">' +
        '<span class="DMAvatar-container">' +
        '<img class="DMAvatar-image" src="' + coverUrl + '" alt="" title="">' +
        '</span>' +
        '</div>'
    );
    $('.DMConversation .DMUpdateName .DMUpdateName-name').text(title);
    /*if  (index == -1) {
        $('.chat-top__title').hide();
        $('.chat-top-button').hide();
    } else if (index == 0) {
        $('.chat-top__button-member').removeClass('chat-top__button-member__all');
        $('.chat-top__button-member').addClass('chat-top__button-member__right');
    } else {
        if (index == 1) {
            $('.chat-top__title').addClass('chat-top__title--messaging');
        } else {
            $('.chat-top__title').addClass('chat-top__title--group');
        }
        $('.chat-top__button-member').removeClass('chat-top__button-member__right');
        $('.chat-top__button-member').addClass('chat-top__button-member__all');
        $('.chat-top__button-invite').show();
    }*/
}

var scrollPositionBottom = function() {
    var scrollHeight = $('.DMConversation-content')[0].scrollHeight;
    $('.DMConversation-scrollContainer.js-modal-scrollable').scrollTop(scrollHeight);
    currScrollHeight = scrollHeight;
};

function afterImageLoad(obj) {
  $('.chat-canvas')[0].scrollTop = $('.chat-canvas')[0].scrollTop + obj.height + $('.chat-canvas__list').height();
}

function setChatMessage(message) {
    $('.DMConversation-content').append(messageList(message));

    /*updateChannelMessageCache(currChannelInfo, message);*/
    scrollPositionBottom();
}

var PreviousMessageListQuery = null;
function loadMoreChatMessage(func) {
    if (!PreviousMessageListQuery) {
        PreviousMessageListQuery = currChannelInfo.createPreviousMessageListQuery();
    }

    PreviousMessageListQuery.load(30, false, function(messages, error){
        if (error) {
            return;
        }

        var moreMessage = messages;
        var msgList = '';
        messages.forEach(function(message){
            switch (message.MESSAGE_TYPE) {
                case message.MESSAGE_TYPE_USER:
                    msgList += messageList(message);
                    break;
                case message.MESSAGE_TYPE_FILE:
                    $('.chat-input-file').removeClass('file-upload');
                    $('#chat_file_input').val('');

                    if (message.customType == 'photoFile') {
                        msgList += imageMessageList(message);
                    } else if (message.customType == 'videoFile') {
                        msgList += fileMessageList(message);
                    }
                    break;
                default:
            }
        });
        /*$('.chat-canvas').prepend(msgList);
        $('.chat-canvas')[0].scrollTop = (moreMessage.length * MESSAGE_TEXT_HEIGHT);*/
        $('.DMConversation-content').prepend(msgList);
        $('.DMConversation-content')[0].scrollTop = (moreMessage.length * MESSAGE_TEXT_HEIGHT);

        for (var i in messages) {
            var message = messages[i];
            updateChannelMessageCache(currChannelInfo, message);
        }

        if (func) {
            func();
        }
    });
}

/*
* Set the width and height of the chat image after loaded.
* */
function loadChatImage(imgObj) {
    //console.log(abc.width + ' : ' + abc.height);
    var percentage = imgObj.height / parseFloat(imgObj.width) * 100;
    $(imgObj).parent().parent().attr('style', 'padding-bottom: ' + percentage + '%;');
}

function messageList(message) {
    var msgList = '';
    var user = message.sender;
    console.log(message.messageType);
    console.log(message);
    var channel = currChannelInfo;

    if (isCurrentUser(user.userId)) {
        if(message.messageType=="file") {
            if (message.customType == 'videoFile') {
                var messageTime = moment(message.createdAt).fromNow();
                var readReceiptHtml = '  <label class="chat-canvas__list-readreceipt"></label>';
                var msg = '' +
                    '<li class="DirectMessage DirectMessage--sent clearfix dm js-dm-item" data-message-id="%messageId%">\
                        <div class="DirectMessage-container">\
                            <div class="DirectMessage-avatar">\
                                <a href="#" class="js-action-profile js-user-profile-link">\
                                    <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">\
                                        <span class="DMAvatar-container">\
                                            <img class="DMAvatar-image" src="%userAvatar%" alt="" title="">\
                                        </span>\
                                    </div>\
                                </a>\
                            </div>\
                            <div class="DirectMessage-message with-media dm-message u-chromeOverflowFix">\
                                <div class="DirectMessage-attachmentContainer">\
                                    <div class="DirectMessage-media u-borderRadiusInherit u-chromeOverflowFix">\
                                        <div class="Sensitive">\
                                            <div class="Sensitive-content">\
                                                <div class="Media u-chromeOverflowFix">\
                                                    <div class="PlayableMedia PlayableMedia--video watched playable-media-loaded"> \
                                                    <div class="PlayableMedia-container">\
                                                        <div class="PlayableMedia-player" style="padding-bottom: 62.5%;">\
                                                            <div class="PlayableMedia-reactWrapper">\
                                                                <div style="cursor: pointer; height: 100%; width: 100%; position: relative; color: rgba(255, 255, 255, 0.85); font-size: 13px; font-weight: 400; font-family: &quot;helvetica neue&quot;, arial; line-height: normal; transform: translateZ(0px);">\
                                                                    <div style="height: 100%; width: 100%;">\
                                                                        <div style="position: relative; width: 100%; height: 100%; background-color: black;">\
                                                                            <video style="width: 100%; height: 100%; position: absolute; transform: rotate(0deg) scale(1);" controls>\
                                                                                    <source src="%imgMessageUrl%">\
                                                                                </video>\
                                                                            </div>\
                                                                        </div>\
                                                                    <div>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="DirectMessage-contentContainer"></div>\
                            </div>\
                            <div class="DirectMessage-actions">\
                                <span class="DirectMessage-action">\
                                    <button type="button" class="DMReportMessageAction js-tooltip transparent-no-border-button" aria-hidden="true">\
                                        <span class="Icon Icon--report"></span>\
                                    </button>\
                                    <button type="button" class="DMReportMessageAction u-hiddenVisually transparent-no-border-button"></button>\
                                </span>\
                                <span class="DirectMessage-action">\
                                    <button type="button" class="DMDeleteMessageAction js-tooltip transparent-no-border-button" aria-hidden="true" onclick="DMConversation_deleteChatMessage(%messageId%)">\
                                        <span class="Icon Icon--delete"></span>\
                                    </button>\
                                    <button type="button" class="DMDeleteMessageAction u-hiddenVisually transparent-no-border-button" onclick="DMConversation_deleteChatMessage(%messageId%)"></button>\
                                </span>\
                            </div>\
                        </div>\
                        <div class="DirectMessage-footer">\
                            <div class="DirectMessage-footerItem">\
                                <div class="DMReadReceipt">\
                                    <div class="DMReadReceipt-statusMessage">\
                                        <span class="DMReadReceipt-defaultView">\
                                            <span class="_timestamp js-relative-timestamp">%sentMessageTime%</span>\
                                        </span>\
                                        <span class="DMReadReceipt-statusView">Sent</span>\
                                    </div>\
                                    <span class="DMReadReceipt-check">\
                                        <span class="Icon Icon--checkLight"></span>\
                                    </span>\
                                </div>\
                            </div>\
                        </div>\
                        <div class="DirectMessage-footer">\
                        </div>\
                    </li>';
                msg = msg.replace('%userAvatar%', xssEscape(user.profileUrl));
                msg = msg.replace(/%imgMessageUrl%/g, xssEscape(message.url));
                msg = msg.replace('%sentMessageTime%', messageTime);
                msg = msg.replace(/%messageId%/g, message.messageId);
                msgList += msg;
            } else if (message.customType == 'photoFile') {
                var messageTime = moment(message.createdAt).fromNow();
                var readReceiptHtml = '  <label class="chat-canvas__list-readreceipt"></label>';
                var msg = '' +
                    '<li class="DirectMessage DirectMessage--sent clearfix dm js-dm-item" data-message-id="%messageId%">\
                        <div class="DirectMessage-container">\
                            <div class="DirectMessage-avatar">\
                                <a href="#" class="js-action-profile js-user-profile-link">\
                                    <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">\
                                        <span class="DMAvatar-container">\
                                            <img class="DMAvatar-image" src="%userAvatar%" alt="" title="">\
                                        </span>\
                                    </div>\
                                </a>\
                            </div>\
                            <div class="DirectMessage-message with-media dm-message u-chromeOverflowFix">\
                                <div class="DirectMessage-attachmentContainer">\
                                    <div class="DirectMessage-media u-borderRadiusInherit u-chromeOverflowFix">\
                                        <div class="Sensitive">\
                                            <div class="Sensitive-content">\
                                                <div class="Media u-chromeOverflowFix">\
                                                    <div class="Media-photo u-chromeOverflowFix">\
                                                        <a href="%imgMessageUrl%" class="dm-attached-media">\
                                                            <div class="FlexEmbed u-borderRadiusInherit">\
                                                                <div class="FlexEmbed-item u-borderRadiusInherit">\
                                                                    <img src="%imgMessageUrl%" onload="loadChatImage(this)" />\
                                                                </div>\
                                                            </div>\
                                                        </a>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="DirectMessage-contentContainer"></div>\
                            </div>\
                            <div class="DirectMessage-actions">\
                                <span class="DirectMessage-action">\
                                    <button type="button" class="DMReportMessageAction js-tooltip transparent-no-border-button" aria-hidden="true">\
                                        <span class="Icon Icon--report"></span>\
                                    </button>\
                                    <button type="button" class="DMReportMessageAction u-hiddenVisually transparent-no-border-button"></button>\
                                </span>\
                                <span class="DirectMessage-action">\
                                    <button type="button" class="DMDeleteMessageAction js-tooltip transparent-no-border-button" aria-hidden="true" onclick="DMConversation_deleteChatMessage(%messageId%)">\
                                        <span class="Icon Icon--delete"></span>\
                                    </button>\
                                    <button type="button" class="DMDeleteMessageAction u-hiddenVisually transparent-no-border-button" onclick="DMConversation_deleteChatMessage(%messageId%)"></button>\
                                </span>\
                            </div>\
                        </div>\
                        <div class="DirectMessage-footer">\
                            <div class="DirectMessage-footerItem">\
                                <div class="DMReadReceipt">\
                                    <div class="DMReadReceipt-statusMessage">\
                                        <span class="DMReadReceipt-defaultView">\
                                            <span class="_timestamp js-relative-timestamp">%sentMessageTime%</span>\
                                        </span>\
                                        <span class="DMReadReceipt-statusView">Sent</span>\
                                    </div>\
                                    <span class="DMReadReceipt-check">\
                                        <span class="Icon Icon--checkLight"></span>\
                                    </span>\
                                </div>\
                            </div>\
                        </div>\
                        <div class="DirectMessage-footer">\
                        </div>\
                    </li>';
                msg = msg.replace('%userAvatar%', xssEscape(user.profileUrl));
                msg = msg.replace(/%imgMessageUrl%/g, xssEscape(message.url));
                msg = msg.replace('%sentMessageTime%', messageTime);
                msg = msg.replace(/%messageId%/g, message.messageId);
                msgList += msg;
            }
        }else if(message.customType == 'postDetail'){
          var msg_post_id = message.data;
          $('#showSlowLoaderPostId_'+message.messageId).show();
          var messageTime = moment(message.createdAt).fromNow();
          //console.log(message.data);

          $.ajax({
            type: "GET",
            dataType: "json",
            url: site_path+'/api.php?query=2y10goLM7YVrc.T2G6AvkMdfS.XUF9Wej5O5XPLwL/H2S59z9yvM6W.AG&get=get_post&post_id='+msg_post_id, //Relative or absolute path to response.php file
            success: function(data) {
              console.log('postDetail');
                  console.log(data.response);

                  //$('#showSlowLoaderPostId_'+message.messageId).hide();

                  if(data.status == 'SUCCESS'){
                    $('#org_post_detail_sent_'+message.messageId).attr('org-post-id',data.response.post_id);
                    $('#postMsgBoxId_'+message.messageId).show();
                    $('#post_author_nameId_'+message.messageId).text(data.response.post_author_name);
                    $('#post_author_usernameId_'+message.messageId).html('@'+data.response.post_user_name);
                    var postTextREc1 = '';

                    if(data.response.post_type == 'shared'){
                      postTextREc1 = data.response.origin.text.substr(0, 25);
                      if(data.response.origin.post_type == 'video'){
                        $('#videogridId1_'+message.messageId).show();
                        $('#videogridId11_src_'+message.messageId).attr('src',site_video_path+data.response.origin.video.source);
                      }
                      if(data.response.origin.post_type == 'photos'){
                        if(data.response.origin.photos_num == 1){
                            $('#photogridId1_'+message.messageId).show();
                            $('#photogridId11_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[0].source);
                        }else if(data.response.origin.photos_num == 2){
                            $('#photogridId2_'+message.messageId).show();
                            $('#photogridId21_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[0].source);
                            $('#photogridId22_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[1].source);
                        }else if(data.response.origin.photos_num == 3){
                            $('#photogridId3_'+message.messageId).show();
                            console.log(data.response.origin.photos_num);
                            $('#photogridId31_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[0].source);
                            $('#photogridId32_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[1].source);
                            $('#photogridId33_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[2].source);
                        }else if(data.response.origin.photos_num == 4){
                            $('#photogridId4_'+message.messageId).show();
                            $('#photogridId41_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[0].source);
                            $('#photogridId42_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[1].source);
                            $('#photogridId43_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[2].source);
                            $('#photogridId44_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[3].source);
                        }else{
                          $('#photogridId4_'+message.messageId).show();
                          $('#photogridId41_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[0].source);
                          $('#photogridId42_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[1].source);
                          $('#photogridId43_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[2].source);
                          $('#photogridId44_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[3].source);
                        }
                      }


                    }else{
                      postTextREc1 = data.response.text.substr(0, 25);
                      if(data.response.post_type == 'video'){
                        $('#videogridId1_'+message.messageId).show();

                        $('#videogridId11_src_'+message.messageId).attr('src',site_video_path+data.response.video.source);
                      }
                      if(data.response.post_type == 'photos'){
                        if(data.response.photos_num == 1){
                            $('#photogridId1_'+message.messageId).show();
                            $('#photogridId11_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[0].source);
                        }else if(data.response.photos_num == 2){
                            $('#photogridId2_'+message.messageId).show();
                            $('#photogridId21_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[0].source);
                            $('#photogridId22_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[1].source);
                        }else if(data.response.photos_num == 3){
                            $('#photogridId3_'+message.messageId).show();
                            console.log(data.response.photos);
                            $('#photogridId31_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[0].source);
                            $('#photogridId32_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[1].source);
                            $('#photogridId33_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[2].source);
                        }else if(data.response.photos_num == 4){
                            $('#photogridId4_'+message.messageId).show();
                            $('#photogridId41_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[0].source);
                            $('#photogridId42_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[1].source);
                            $('#photogridId43_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[2].source);
                            $('#photogridId44_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[3].source);
                        }else{
                          $('#photogridId4_'+message.messageId).show();
                          $('#photogridId41_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[0].source);
                          $('#photogridId42_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[1].source);
                          $('#photogridId43_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[2].source);
                          $('#photogridId44_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[3].source);
                        }
                      }

                    }

                    $('#postText_'+message.messageId).text(postTextREc1);
                    $('#postDMAvatar_'+message.messageId).attr('src',data.response.post_author_picture);


                  }else{

                  }
            }
          });
          //console.log($.parseJSON(message.data));
          var readReceiptHtml = '  <label class="chat-canvas__list-readreceipt"></label>';
          var msg = '<div class="loader loader_medium x-hidden" id="showSlowLoaderPostId_%messageId%" style="display:none;text-align:center"></div><div id="postMsgBoxId_%messageId%" style="display:none">' +
              '<li class="DirectMessage DirectMessage--sent clearfix dm js-dm-item" data-message-id="%messageId%">\
              <div class="DMComposer-tweet org_post_detail_cls" id="org_post_detail_sent_%messageId%"  org-post-id="" onclick="get_post_detail_box(%messagePostId%)">\
                  <div class="DirectMessage-container">\
                      <div class="DirectMessage-avatar">\
                          <a href="#" class="js-action-profile js-user-profile-link">\
                              <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">\
                                <span class="DMAvatar-container">\
                                    <img class="DMAvatar-image" src="%userAvatar%" alt="" title="">\
                                </span>\
                              </div>\
                          </a>\
                      </div>\
                      <div class="DirectMessage-message with-text dm-message u-chromeOverflowFix">\
                          <div class="DirectMessage-contentContainer">\
                              <div class="DirectMessage-text" style="">\
								<div style="float:left;">\
                <div class="grid-container1 grid-width" id="videogridId1_%messageId%" style="display:none">\
                  <div class="item1">\
                  <video style="width: 100%; height: 100%; position: absolute; transform: rotate(0deg) scale(1);" controls>\
                          <source src="" id="videogridId11_src_%messageId%">\
                      </video>\
                </div>\
                </div>\
									<div class="grid-container1 grid-width" id="photogridId1_%messageId%" style="display:none">\
										<div class="item1"><img src="" id="photogridId11_src_%messageId%"></div>\
									</div>\
									<div class="grid-container2 grid-width" id="photogridId2_%messageId%" style="display:none">\
										<div class="item1"><img src="" id="photogridId21_src_%messageId%"></div>\
										  <div class="item2"><img src="" id="photogridId22_src_%messageId%"></div>\
									</div>\
									<div class="grid-container3 grid-width" id="photogridId3_%messageId%" style="display:none">\
										<div class="item1"><img src="" id="photogridId31_src_%messageId%"></div>\
										  <div class="item2"><img src="" id="photogridId32_src_%messageId%"></div>\
										  <div class="item3"><img src="" id="photogridId33_src_%messageId%"></div>  \
									</div>\
									<div class="grid-container4 grid-width" id="photogridId4_%messageId%" style="display:none">\
										<div class="item1"><img src="" id="photogridId41_src_%messageId%"></div>\
										  <div class="item2"><img src="" id="photogridId42_src_%messageId%"></div>\
										  <div class="item3"><img src="" id="photogridId43_src_%messageId%"></div>  \
										  <div class="item4"><img src="" id="photogridId44_src_%messageId%"></div>\
									</div>\
								</div>\
                                  <div class="QuoteTweet-originalAuthor u-cf u-textTruncate stream-item-header account-group js-user-profile-link">\
                                  <b class="QuoteTweet-fullname u-linkComplex-target" id="post_author_nameId_%messageId%"></b>\
                                  <span class="UserNameBreak">&nbsp;</span><span class="username u-dir u-textTruncate" dir="ltr" id="post_author_usernameId_%messageId%"></span>\
                                  </div>\
                                  <div class="js-tweet-text-container">\
                                      <p class="TweetTextSize js-tweet-text tweet-text" lang="" data-aria-label-part="0" id="postText_%messageId%"></p>\
                                  </div>\
                              </div>\
                          </div>\
                      </div>\
                      </div><div class="DirectMessage-contentContainer"></div>\
                  ';
                      if(message.message==''){
                        msg+='<div class="DirectMessage-footer"><div class="DirectMessage-actions">\
                            <span class="DirectMessage-action">\
                                <button type="button" class="DMReportMessageAction js-tooltip transparent-no-border-button" aria-hidden="true">\
                                    <span class="Icon Icon--report"></span>\
                                </button>\
                                <button type="button" class="DMReportMessageAction u-hiddenVisually transparent-no-border-button"></button>\
                            </span>\
                            <span class="DirectMessage-action">\
                                <button type="button" class="DMDeleteMessageAction js-tooltip transparent-no-border-button" aria-hidden="true" onclick="DMConversation_deleteChatMessage(%messageId%)">\
                                    <span class="Icon Icon--delete"></span>\
                                </button>\
                                <button type="button" class="DMDeleteMessageAction u-hiddenVisually transparent-no-border-button" onclick="DMConversation_deleteChatMessage(%messageId%)"></button>\
                            </span>\
                        </div>\
                    </div></div>\
                    <div class="DirectMessage-footer">\
                        <div class="DirectMessage-footerItem">\
                            <div class="DMReadReceipt">\
                                <div class="DMReadReceipt-statusMessage">\
                                    <span class="DMReadReceipt-defaultView">\
                                        <span class="_timestamp js-relative-timestamp">%sentMessageTime%</span>\
                                    </span>\
                                    <span class="DMReadReceipt-statusView">Sent</span>\
                                </div>\
                                <span class="DMReadReceipt-check">\
                                    <span class="Icon Icon--checkLight"></span>\
                                </span>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="DirectMessage-footer">\
                    </div>';
                      }
                      msg+='</li>';
                    if(message.message!='')  {
                  msg += '<li class="DirectMessage DirectMessage--sent" data-message-id="%messageId%">\
                      <div class="DirectMessage-container">\
                          <div class="DirectMessage-avatar">\
                              <a href="#" class="js-action-profile js-user-profile-link">\
                                  <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">\
                                      <span class="DMAvatar-container">\
                                          <img class="DMAvatar-image" src="%userAvatar%" alt="" title="">\
                                      </span>\
                                  </div>\
                              </a>\
                          </div>\
                          <div class="DirectMessage-message with-text dm-message u-chromeOverflowFix">\
                              <div class="DirectMessage-contentContainer">\
                                  <div class="DirectMessage-text">\
                                      <div class="js-tweet-text-container">\
                                          <p class="TweetTextSize js-tweet-text tweet-text" lang="" data-aria-label-part="0">%userSentMessage%</p>\
                                      </div>\
                                  </div>\
                              </div>\
                          </div>\
                          <div class="DirectMessage-actions">\
                              <span class="DirectMessage-action">\
                                  <button type="button" class="DMReportMessageAction js-tooltip transparent-no-border-button" aria-hidden="true">\
                                      <span class="Icon Icon--report"></span>\
                                  </button>\
                                  <button type="button" class="DMReportMessageAction u-hiddenVisually transparent-no-border-button"></button>\
                              </span>\
                              <span class="DirectMessage-action">\
                                  <button type="button" class="DMDeleteMessageAction js-tooltip transparent-no-border-button" aria-hidden="true" onclick="DMConversation_deleteChatMessage(%messageId%)">\
                                      <span class="Icon Icon--delete"></span>\
                                  </button>\
                                  <button type="button" class="DMDeleteMessageAction u-hiddenVisually transparent-no-border-button" onclick="DMConversation_deleteChatMessage(%messageId%)"></button>\
                              </span>\
                          </div>\
                      </div>\
                      <div class="DirectMessage-footer">\
                          <div class="DirectMessage-footerItem">\
                              <div class="DMReadReceipt">\
                                  <div class="DMReadReceipt-statusMessage">\
                                      <span class="DMReadReceipt-defaultView">\
                                          <span class="_timestamp js-relative-timestamp">%sentMessageTime%</span>\
                                      </span>\
                                      <span class="DMReadReceipt-statusView">Sent</span>\
                                  </div>\
                                  <span class="DMReadReceipt-check">\
                                      <span class="Icon Icon--checkLight"></span>\
                                  </span>\
                              </div>\
                          </div>\
                      </div>\
                      <div class="DirectMessage-footer">\
                      </div></li>';
                    }
                      msg += '</div>';
          msg = msg.replace('%userAvatar%', xssEscape(user.profileUrl));
          msg = msg.replace('%userSentMessage%', convertLinkMessage(xssEscape(message.message)));

          msg = msg.replace('%sentMessageTime%', messageTime);
          msg = msg.replace(/%messageId%/g, message.messageId);
          msg = msg.replace(/%messagePostId%/g, message.data);

          msgList += msg;
        } else {
            var messageTime = moment(message.createdAt).fromNow();
            var readReceiptHtml = '  <label class="chat-canvas__list-readreceipt"></label>';

            var msg = '' +
                '<li class="DirectMessage DirectMessage--sent" data-message-id="%messageId%">\
                    <div class="DirectMessage-container">\
                        <div class="DirectMessage-avatar">\
                            <a href="#" class="js-action-profile js-user-profile-link">\
                                <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">\
                                    <span class="DMAvatar-container">\
                                        <img class="DMAvatar-image" src="%userAvatar%" alt="" title="">\
                                    </span>\
                                </div>\
                            </a>\
                        </div>\
                        <div class="DirectMessage-message with-text dm-message u-chromeOverflowFix">\
                            <div class="DirectMessage-contentContainer">\
                                <div class="DirectMessage-text">\
                                    <div class="js-tweet-text-container">\
                                        <p class="TweetTextSize js-tweet-text tweet-text" lang="" data-aria-label-part="0">%userSentMessage%</p>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                        <div class="DirectMessage-actions">\
                            <span class="DirectMessage-action">\
                                <button type="button" class="DMReportMessageAction js-tooltip transparent-no-border-button" aria-hidden="true">\
                                    <span class="Icon Icon--report"></span>\
                                </button>\
                                <button type="button" class="DMReportMessageAction u-hiddenVisually transparent-no-border-button"></button>\
                            </span>\
                            <span class="DirectMessage-action">\
                                <button type="button" class="DMDeleteMessageAction js-tooltip transparent-no-border-button" aria-hidden="true" onclick="DMConversation_deleteChatMessage(%messageId%)">\
                                    <span class="Icon Icon--delete"></span>\
                                </button>\
                                <button type="button" class="DMDeleteMessageAction u-hiddenVisually transparent-no-border-button" onclick="DMConversation_deleteChatMessage(%messageId%)"></button>\
                            </span>\
                        </div>\
                    </div>\
                    <div class="DirectMessage-footer">\
                        <div class="DirectMessage-footerItem">\
                            <div class="DMReadReceipt">\
                                <div class="DMReadReceipt-statusMessage">\
                                    <span class="DMReadReceipt-defaultView">\
                                        <span class="_timestamp js-relative-timestamp">%sentMessageTime%</span>\
                                    </span>\
                                    <span class="DMReadReceipt-statusView">Sent</span>\
                                </div>\
                                <span class="DMReadReceipt-check">\
                                    <span class="Icon Icon--checkLight"></span>\
                                </span>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="DirectMessage-footer">\
                    </div>\
                </li>';
            msg = msg.replace('%userAvatar%', xssEscape(user.profileUrl));
            msg = msg.replace('%userSentMessage%', convertLinkMessage(xssEscape(message.message)));
            msg = msg.replace('%sentMessageTime%', messageTime);
            msg = msg.replace(/%messageId%/g, message.messageId);
            msgList += msg;
        }
    } else {
        if(message.messageType=="file") {
            if (message.customType == 'videoFile') {
                var messageTime = moment(message.createdAt).fromNow();
                var msg = '' +
                    '<li class="DirectMessage DirectMessage--received clearfix dm js-dm-item" data-message-id="%messageId%">\
                        <div class="DirectMessage-container">\
                            <div class="DirectMessage-avatar">\
                                <a href="#" class="js-action-profile js-user-profile-link">\
                                    <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">\
                                        <span class="DMAvatar-container">\
                                            <img class="DMAvatar-image" src="%userAvatar%" />\
                                        </span>\
                                    </div>\
                                </a>\
                            </div>\
                            <div class="DirectMessage-message with-media dm-message u-chromeOverflowFix">\
                                <div class="DirectMessage-attachmentContainer">\
                                    <div class="DirectMessage-media u-borderRadiusInherit u-chromeOverflowFix">\
                                        <div class="Sensitive">\
                                            <div class="Sensitive-content">\
                                                <div class="Media u-chromeOverflowFix">\
                                                    <div class="PlayableMedia PlayableMedia--video watched playable-media-loaded"> \
                                                    <div class="PlayableMedia-container">\
                                                        <div class="PlayableMedia-player" style="padding-bottom: 62.5%;">\
                                                            <div class="PlayableMedia-reactWrapper">\
                                                                <div style="cursor: pointer; height: 100%; width: 100%; position: relative; color: rgba(255, 255, 255, 0.85); font-size: 13px; font-weight: 400; font-family: &quot;helvetica neue&quot;, arial; line-height: normal; transform: translateZ(0px);">\
                                                                    <div style="height: 100%; width: 100%;">\
                                                                        <div style="position: relative; width: 100%; height: 100%; background-color: black;">\
                                                                            <video style="width: 100%; height: 100%; position: absolute; transform: rotate(0deg) scale(1);" controls>\
                                                                                    <source src="%imgMessageUrl%">\
                                                                                </video>\
                                                                            </div>\
                                                                        </div>\
                                                                    <div>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="DirectMessage-contentContainer"></div>\
                            </div>\
                        </div>\
                        <div class="DirectMessage-footer">\
                            <span class="DirectMessage-footerItem">%senderUserName%</span>\
                            <span class="DirectMessage-footerItem">\
                                <span class="_timestamp">%messageTime%</span>\
                            </span>\
                        </div>\
                        <div class="DirectMessage-footer">\
                            <div class="DMReadReceiptUserList DirectMessage-footerItem u-hidden">\
                                <div class="DMReadReceiptUserList-truncated"></div>\
                                <div class="DMReadReceiptUserList-expanded"></div>\
                            </div>\
                        </div>\
                    </li>';
                msg = msg.replace('%senderUserName%', message._sender.nickname + '&nbsp;●&nbsp;');
                msg = msg.replace('%messageTime%', messageTime);
                msg = msg.replace('%userAvatar%', xssEscape(user.profileUrl));
                msg = msg.replace('%userReceivedMessage%', convertLinkMessage(xssEscape(message.message)));
                msg = msg.replace(/%imgMessageUrl%/g, xssEscape(message.url));
                msgList += msg;
            } else if (message.customType == 'photoFile') {
                var messageTime = moment(message.createdAt).fromNow();
                var msg = '' +
                    '<li class="DirectMessage DirectMessage--received clearfix dm js-dm-item" data-message-id="%messageId%">\
                        <div class="DirectMessage-container">\
                            <div class="DirectMessage-avatar">\
                                <a href="#" class="js-action-profile js-user-profile-link">\
                                    <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">\
                                        <span class="DMAvatar-container">\
                                            <img class="DMAvatar-image" src="%userAvatar%" />\
                                        </span>\
                                    </div>\
                                </a>\
                            </div>\
                            <div class="DirectMessage-message with-media dm-message u-chromeOverflowFix">\
                                <div class="DirectMessage-attachmentContainer">\
                                    <div class="DirectMessage-media u-borderRadiusInherit u-chromeOverflowFix">\
                                        <div class="Sensitive">\
                                            <div class="Sensitive-content">\
                                                <div class="Media u-chromeOverflowFix">\
                                                    <div class="Media-photo u-chromeOverflowFix">\
                                                        <a href="%imgMessageUrl%" class="dm-attached-media">\
                                                            <div class="FlexEmbed u-borderRadiusInherit">\
                                                                <div class="FlexEmbed-item u-borderRadiusInherit">\
                                                                    <img src="%imgMessageUrl%" onload="loadChatImage(this)" />\
                                                                </div>\
                                                            </div>\
                                                        </a>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="DirectMessage-contentContainer"></div>\
                            </div>\
                        </div>\
                        <div class="DirectMessage-footer">\
                            <span class="DirectMessage-footerItem">%senderUserName%</span>\
                            <span class="DirectMessage-footerItem">\
                                <span class="_timestamp">%messageTime%</span>\
                            </span>\
                        </div>\
                        <div class="DirectMessage-footer">\
                            <div class="DMReadReceiptUserList DirectMessage-footerItem u-hidden">\
                                <div class="DMReadReceiptUserList-truncated"></div>\
                                <div class="DMReadReceiptUserList-expanded"></div>\
                            </div>\
                        </div>\
                    </li>';
                msg = msg.replace('%senderUserName%', message._sender.nickname + '&nbsp;●&nbsp;');
                msg = msg.replace('%messageTime%', messageTime);
                msg = msg.replace('%userAvatar%', xssEscape(user.profileUrl));
                msg = msg.replace('%userReceivedMessage%', convertLinkMessage(xssEscape(message.message)));
                msg = msg.replace(/%imgMessageUrl%/g, xssEscape(message.url));
                msgList += msg;
            }
        }else if(message.customType == 'postDetail'){
          var msg_post_id = message.data;
          $('#showSlowLoaderPostreceivedId_'+message.messageId).show();
          var messageTime = moment(message.createdAt).fromNow();
          //console.log(message.data);

          $.ajax({
            type: "GET",
            dataType: "json",
            url: site_path+'/api.php?query=1234&get=get_post&post_id='+msg_post_id, //Relative or absolute path to response.php file
            success: function(data) {
                  console.log(data.response);

                  // var postText = data.response.text;
                  // var post_author_name = data.response.post_author_name;
                  // var post_author_url = data.response.post_author_url;
                  // var post_user_id= data.response.user_id;
                  // var post_author_picture  = data.response.post_author_picture;
                  // var post_user_name  = data.response.post_user_name;
                  $('#showSlowLoaderPostreceivedId_'+message.messageId).hide();

                  if(data.status == 'SUCCESS'){
                    $('#org_post_detail_rec_'+message.messageId).attr('org-post-id',data.response.post_id);
                    $('#postMsgBoxId_'+message.messageId).show();
                    $('#post_author_nameId_'+message.messageId).text(data.response.post_author_name);
                    $('#post_author_usernameId_'+message.messageId).html('@'+data.response.post_user_name);
                    var postTextREc = '';
                    if(data.response.post_type == 'shared'){
                      postTextREc = data.response.origin.text.substr(0, 25);
                      if(data.response.origin.post_type == 'video'){
                        $('#videogridId1_'+message.messageId).show();
                        console.log(data.response.origin.video.source);
                        $('#videogridId11_src_'+message.messageId).attr('src',site_video_path+data.response.origin.video.source);
                      }
                      if(data.response.origin.post_type == 'photos'){
                        if(data.response.origin.photos_num == 1){
                            $('#photogridId1_'+message.messageId).show();
                            $('#photogridId11_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[0].source);
                        }else if(data.response.origin.photos_num == 2){
                            $('#photogridId2_'+message.messageId).show();
                            $('#photogridId21_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[0].source);
                            $('#photogridId22_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[1].source);
                        }else if(data.response.origin.photos_num == 3){
                            $('#photogridId3_'+message.messageId).show();
                            console.log(data.response.origin.photos_num);
                            $('#photogridId31_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[0].source);
                            $('#photogridId32_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[1].source);
                            $('#photogridId33_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[2].source);
                        }else if(data.response.origin.photos_num == 4){
                            $('#photogridId4_'+message.messageId).show();
                            $('#photogridId41_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[0].source);
                            $('#photogridId42_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[1].source);
                            $('#photogridId43_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[2].source);
                            $('#photogridId44_src_'+message.messageId).attr('src',site_photos_path+data.response.origin.photos[3].source);
                        }else{

                        }
                      }


                    }else{
                      postTextREc = data.response.text.substr(0, 25);
                      if(data.response.post_type == 'video'){
                        console.log(data.response.video.source);
                        $('#videogridId1_'+message.messageId).show();
                        $('#videogridId11_src_'+message.messageId).attr('src',site_photos_path+data.response.video.source);
                      }
                      if(data.response.post_type == 'photos'){
                        if(data.response.photos_num == 1){
                            $('#photogridId1_'+message.messageId).show();
                            $('#photogridId11_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[0].source);
                        }else if(data.response.photos_num == 2){
                            $('#photogridId2_'+message.messageId).show();
                            $('#photogridId21_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[0].source);
                            $('#photogridId22_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[1].source);
                        }else if(data.response.photos_num == 3){
                            $('#photogridId3_'+message.messageId).show();
                            console.log(data.response.photos);
                            $('#photogridId31_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[0].source);
                            $('#photogridId32_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[1].source);
                            $('#photogridId33_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[2].source);
                        }else if(data.response.photos_num == 4){
                            $('#photogridId4_'+message.messageId).show();
                            $('#photogridId41_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[0].source);
                            $('#photogridId42_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[1].source);
                            $('#photogridId43_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[2].source);
                            $('#photogridId44_src_'+message.messageId).attr('src',site_photos_path+data.response.photos[3].source);
                        }else{

                        }
                      }

                    }

                    $('#postText_'+message.messageId).text(postTextREc);
                    $('#postDMAvatar_'+message.messageId).attr('src',data.response.post_author_picture);



                  }else{

                  }
            }
          });
          //console.log($.parseJSON(message.data));
          var readReceiptHtml = '  <label class="chat-canvas__list-readreceipt"></label>';
          var msg = '<div class="loader loader_medium x-hidden" id="showSlowLoaderPostreceivedId_%messageId%" style="display:none;text-align:center"></div><div id="postMsgBoxId_%messageId%" style="display:none">' +
              '<li class="DirectMessage DirectMessage--received is-rapidFire clearfix dm js-dm-item" data-message-id="%messageId%">\
                  <div class="DMComposer-tweet org_post_detail_cls" id="org_post_detail_rec_%messageId%"  org-post-id="" onclick="get_post_detail_box(%messagePostId%)"><div class="DirectMessage-container">\
                  <div class="DirectMessage-avatar">\
                          <a href="#" class="js-action-profile js-user-profile-link">\
                              <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">\
                                <span class="DMAvatar-container">\
                                    <img class="DMAvatar-image" src="%userAvatar%" alt="" title="">\
                                </span>\
                              </div>\
                          </a>\
                      </div>\
                      <div class="DirectMessage-message with-text dm-message u-chromeOverflowFix">\
                          <div class="DirectMessage-contentContainer">\
                              <div class="DirectMessage-text" style="">\
                              <div style="float:left;">\
                              <div class="grid-container1 grid-width" id="videogridId1_%messageId%" style="display:none">\
                                <div class="item1">\
                                <video style="width: 100%; height: 100%; position: absolute; transform: rotate(0deg) scale(1);" controls>\
                                        <source src="" id="videogridId11_src_%messageId%">\
                                    </video>\
                              </div>\
                              </div>\
                                <div class="grid-container1 grid-width" id="photogridId1_%messageId%" style="display:none">\
                                  <div class="item1"><img src="" id="photogridId11_src_%messageId%"></div>\
                                </div>\
                                <div class="grid-container2 grid-width" id="photogridId2_%messageId%" style="display:none">\
                                  <div class="item1"><img src="" id="photogridId21_src_%messageId%"></div>\
                                    <div class="item2"><img src="" id="photogridId22_src_%messageId%"></div>\
                                </div>\
                                <div class="grid-container3 grid-width" id="photogridId3_%messageId%" style="display:none">\
                                  <div class="item1"><img src="" id="photogridId31_src_%messageId%"></div>\
                                    <div class="item2"><img src="" id="photogridId32_src_%messageId%"></div>\
                                    <div class="item3"><img src="" id="photogridId33_src_%messageId%"></div>  \
                                </div>\
                                <div class="grid-container4 grid-width" id="photogridId4_%messageId%" style="display:none">\
                                  <div class="item1"><img src="" id="photogridId41_src_%messageId%"></div>\
                                    <div class="item2"><img src="" id="photogridId42_src_%messageId%"></div>\
                                    <div class="item3"><img src="" id="photogridId43_src_%messageId%"></div>  \
                                    <div class="item4"><img src="" id="photogridId44_src_%messageId%"></div>\
                                </div>\
                              </div>\
                                  <div class="QuoteTweet-originalAuthor u-cf u-textTruncate stream-item-header account-group js-user-profile-link">\
                                  <b class="QuoteTweet-fullname u-linkComplex-target" id="post_author_nameId_%messageId%"></b>\
                                  <span class="UserNameBreak">&nbsp;</span><span class="username u-dir u-textTruncate" dir="ltr" id="post_author_usernameId_%messageId%"></span>\
                                  </div>\
                                  <div class="js-tweet-text-container">\
                                      <p class="TweetTextSize js-tweet-text tweet-text" lang="" data-aria-label-part="0" id="postText_%messageId%"></p>\
                                  </div>\
                              </div>\
                          </div>\
                      </div>\
                  </div>\
                  </div>\
                  </li>';
                  if(message.message !=''){
                  msg += '<li class="DirectMessage DirectMessage--received is-rapidFire clearfix dm js-dm-item" data-message-id="%messageId%">\
                  <div class="DirectMessage-container">\
                      <div class="DirectMessage-avatar">\
                          <a href="#" class="js-action-profile js-user-profile-link">\
                              <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">\
                                  <span class="DMAvatar-container">\
                                      <img class="DMAvatar-image" src="%userAvatar%" />\
                                  </span>\
                              </div>\
                          </a>\
                      </div>\
                      <div class="DirectMessage-message with-text dm-message u-chromeOverflowFix">\
                          <div class="DirectMessage-contentContainer">\
                              <div class="DirectMessage-text">\
                                  <div class="js-tweet-text-container">\
                                      <p class="TweetTextSize js-tweet-text tweet-text">%userReceivedMessage%</p>\
                                  </div>\
                              </div>\
                          </div>\
                      </div>\
                  </div>\
                  <div class="DirectMessage-footer">\
                      <span class="DirectMessage-footerItem">%senderUserName%</span>\
                      <span class="DirectMessage-footerItem">\
                          <span class="_timestamp">%messageTime%</span>\
                      </span>\
                  </div>\
                  <div class="DirectMessage-footer">\
                      <div class="DMReadReceiptUserList DirectMessage-footerItem u-hidden">\
                          <div class="DMReadReceiptUserList-truncated"></div>\
                          <div class="DMReadReceiptUserList-expanded"></div>\
                      </div>\
                  </div>\
              </li>';
            }
            msg += '</div>';
          msg = msg.replace('%userAvatar%', xssEscape(user.profileUrl));
          msg = msg.replace('%userReceivedMessage%', convertLinkMessage(xssEscape(message.message)));
          msg = msg.replace('%senderUserName%', message._sender.nickname + '&nbsp;●&nbsp;');
          msg = msg.replace('%messageTime%', messageTime);
          msg = msg.replace(/%messageId%/g, message.messageId);
          msg = msg.replace(/%messagePostId%/g, message.data);
          msgList += msg;
        } else {
            var messageTime = moment(message.createdAt).fromNow();
            var msg = '' +
                '<li class="DirectMessage DirectMessage--received is-rapidFire clearfix dm js-dm-item" data-message-id="%messageId%">\
                    <div class="DirectMessage-container">\
                        <div class="DirectMessage-avatar">\
                            <a href="#" class="js-action-profile js-user-profile-link">\
                                <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">\
                                    <span class="DMAvatar-container">\
                                        <img class="DMAvatar-image" src="%userAvatar%" />\
                                    </span>\
                                </div>\
                            </a>\
                        </div>\
                        <div class="DirectMessage-message with-text dm-message u-chromeOverflowFix">\
                            <div class="DirectMessage-contentContainer">\
                                <div class="DirectMessage-text">\
                                    <div class="js-tweet-text-container">\
                                        <p class="TweetTextSize js-tweet-text tweet-text">%userReceivedMessage%</p>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="DirectMessage-footer">\
                        <span class="DirectMessage-footerItem">%senderUserName%</span>\
                        <span class="DirectMessage-footerItem">\
                            <span class="_timestamp">%messageTime%</span>\
                        </span>\
                    </div>\
                    <div class="DirectMessage-footer">\
                        <div class="DMReadReceiptUserList DirectMessage-footerItem u-hidden">\
                            <div class="DMReadReceiptUserList-truncated"></div>\
                            <div class="DMReadReceiptUserList-expanded"></div>\
                        </div>\
                    </div>\
                </li>';
            msg = msg.replace('%senderUserName%', message._sender.nickname + '&nbsp;●&nbsp;');
            msg = msg.replace('%messageTime%', messageTime);
            msg = msg.replace('%userAvatar%', xssEscape(user.profileUrl));
            msg = msg.replace('%userReceivedMessage%', convertLinkMessage(xssEscape(message.message)));
            msg = msg.replace(/%messageId%/g, message.messageId);
            msgList += msg;
        }
    }
    return msgList;
}

function updateChannelMessageCache(channel, message) {
    var readReceipt = -1;
    if (channel.isGroupChannel()) {
        readReceipt = channel.getReadReceipt(message);
    }
    if (!channelMessageList.hasOwnProperty(channel.url)) {
        channelMessageList[channel.url] = {};
    }

    if (!channelMessageList[channel.url].hasOwnProperty(message.messageId)) {
        channelMessageList[channel.url][message.messageId] = {};
    }

    channelMessageList[channel.url][message.messageId]['message'] = message;

    if (channel.isGroupChannel() && readReceipt >= 0) {
        channelMessageList[channel.url][message.messageId]['readReceipt'] = readReceipt;

        var elemString = '.chat-canvas__list-text[data-messageid='+message.messageId+']';
        var elem = $(elemString).next();
        if (readReceipt == 0) {
            elem.html('').hide();
        } else {
            elem.html(readReceipt);
            if (!elem.is(':visible')) {
                elem.show();
            }
        }
    } else {
        return;
    }
}

function updateChannelMessageCacheAll(channel) {
    for (var i in channelMessageList[channel.url]) {
        var message = channelMessageList[channel.url][i]['message'];
        updateChannelMessageCache(channel, message);
    }
}

function fileMessageList(obj) {
    console.log("filr message list is " + obj);
    var msgList = '';
    var message = obj;
    var user = message.sender;

    if (isCurrentUser(user.userId)) {
        var messageTime = moment(message.createdAt).fromNow();
        var readReceiptHtml = '  <label class="chat-canvas__list-readreceipt"></label>';
        var msg = '' +
            '<li class="DirectMessage DirectMessage--sent clearfix dm js-dm-item" data-message-id="%messageId%">\
                <div class="DirectMessage-container">\
                    <div class="DirectMessage-avatar">\
                        <a href="#" class="js-action-profile js-user-profile-link">\
                            <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">\
                                <span class="DMAvatar-container">\
                                    <img class="DMAvatar-image" src="%userAvatar%" alt="" title="">\
                                </span>\
                            </div>\
                        </a>\
                    </div>\
                    <div class="DirectMessage-message with-media dm-message u-chromeOverflowFix">\
                        <div class="DirectMessage-attachmentContainer">\
                            <div class="DirectMessage-media u-borderRadiusInherit u-chromeOverflowFix">\
                                <div class="Sensitive">\
                                    <div class="Sensitive-content">\
                                        <div class="Media u-chromeOverflowFix">\
                                            <div class="PlayableMedia PlayableMedia--video watched playable-media-loaded"> \
                                            <div class="PlayableMedia-container">\
                                                <div class="PlayableMedia-player" style="padding-bottom: 62.5%;">\
                                                    <div class="PlayableMedia-reactWrapper">\
                                                        <div style="cursor: pointer; height: 100%; width: 100%; position: relative; color: rgba(255, 255, 255, 0.85); font-size: 13px; font-weight: 400; font-family: &quot;helvetica neue&quot;, arial; line-height: normal; transform: translateZ(0px);">\
                                                            <div style="height: 100%; width: 100%;">\
                                                                <div style="position: relative; width: 100%; height: 100%; background-color: black;">\
                                                                    <video style="width: 100%; height: 100%; position: absolute; transform: rotate(0deg) scale(1);" controls>\
                                                                            <source src="%imgMessageUrl%">\
                                                                        </video>\
                                                                    </div>\
                                                                </div>\
                                                            <div>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                        <div class="DirectMessage-contentContainer"></div>\
                    </div>\
                    <div class="DirectMessage-actions">\
                        <span class="DirectMessage-action">\
                            <button type="button" class="DMReportMessageAction js-tooltip transparent-no-border-button" aria-hidden="true">\
                                <span class="Icon Icon--report"></span>\
                            </button>\
                            <button type="button" class="DMReportMessageAction u-hiddenVisually transparent-no-border-button"></button>\
                        </span>\
                        <span class="DirectMessage-action">\
                            <button type="button" class="DMDeleteMessageAction js-tooltip transparent-no-border-button" aria-hidden="true" onclick="DMConversation_deleteChatMessage(%messageId%)">\
                                <span class="Icon Icon--delete"></span>\
                            </button>\
                            <button type="button" class="DMDeleteMessageAction u-hiddenVisually transparent-no-border-button" onclick="DMConversation_deleteChatMessage(%messageId%)"></button>\
                        </span>\
                    </div>\
                </div>\
                <div class="DirectMessage-footer">\
                    <div class="DirectMessage-footerItem">\
                        <div class="DMReadReceipt">\
                            <div class="DMReadReceipt-statusMessage">\
                                <span class="DMReadReceipt-defaultView">\
                                    <span class="_timestamp js-relative-timestamp">%sentMessageTime%</span>\
                                </span>\
                                <span class="DMReadReceipt-statusView">Sent</span>\
                            </div>\
                            <span class="DMReadReceipt-check">\
                                <span class="Icon Icon--checkLight"></span>\
                            </span>\
                        </div>\
                    </div>\
                </div>\
                <div class="DirectMessage-footer">\
                </div>\
            </li>';
        msg = msg.replace('%userAvatar%', xssEscape(user.profileUrl));
        msg = msg.replace(/%imgMessageUrl%/g, xssEscape(message.url));
        msg = msg.replace('%sentMessageTime%', messageTime);
        msg = msg.replace(/%messageId%/g, message.messageId);
        msgList += msg;
    } else {
        var messageTime = moment(message.createdAt).fromNow();
        var msg = '' +
            '<li class="DirectMessage DirectMessage--received clearfix dm js-dm-item" data-message-id="%messageId%">\
                <div class="DirectMessage-container">\
                    <div class="DirectMessage-avatar">\
                        <a href="#" class="js-action-profile js-user-profile-link">\
                            <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">\
                                <span class="DMAvatar-container">\
                                    <img class="DMAvatar-image" src="%userAvatar%" />\
                                </span>\
                            </div>\
                        </a>\
                    </div>\
                    <div class="DirectMessage-message with-media dm-message u-chromeOverflowFix">\
                        <div class="DirectMessage-attachmentContainer">\
                            <div class="DirectMessage-media u-borderRadiusInherit u-chromeOverflowFix">\
                                <div class="Sensitive">\
                                    <div class="Sensitive-content">\
                                        <div class="Media u-chromeOverflowFix">\
                                            <div class="PlayableMedia PlayableMedia--video watched playable-media-loaded"> \
                                            <div class="PlayableMedia-container">\
                                                <div class="PlayableMedia-player" style="padding-bottom: 62.5%;">\
                                                    <div class="PlayableMedia-reactWrapper">\
                                                        <div style="cursor: pointer; height: 100%; width: 100%; position: relative; color: rgba(255, 255, 255, 0.85); font-size: 13px; font-weight: 400; font-family: &quot;helvetica neue&quot;, arial; line-height: normal; transform: translateZ(0px);">\
                                                            <div style="height: 100%; width: 100%;">\
                                                                <div style="position: relative; width: 100%; height: 100%; background-color: black;">\
                                                                    <video style="width: 100%; height: 100%; position: absolute; transform: rotate(0deg) scale(1);" controls>\
                                                                            <source src="%imgMessageUrl%">\
                                                                        </video>\
                                                                    </div>\
                                                                </div>\
                                                            <div>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                        <div class="DirectMessage-contentContainer"></div>\
                    </div>\
                </div>\
                <div class="DirectMessage-footer">\
                    <span class="DirectMessage-footerItem">%senderUserName%</span>\
                    <span class="DirectMessage-footerItem">\
                        <span class="_timestamp">%messageTime%</span>\
                    </span>\
                </div>\
                <div class="DirectMessage-footer">\
                    <div class="DMReadReceiptUserList DirectMessage-footerItem u-hidden">\
                        <div class="DMReadReceiptUserList-truncated"></div>\
                        <div class="DMReadReceiptUserList-expanded"></div>\
                    </div>\
                </div>\
            </li>';
        msg = msg.replace('%senderUserName%', message._sender.nickname + '&nbsp;●&nbsp;');
        msg = msg.replace('%messageTime%', messageTime);
        msg = msg.replace('%userAvatar%', xssEscape(user.profileUrl));
        msg = msg.replace('%userReceivedMessage%', convertLinkMessage(xssEscape(message.message)));
        msg = msg.replace(/%imgMessageUrl%/g, xssEscape(message.url));
        msgList += msg;
    }

    return msgList;
}

function imageMessageList(obj) {
    console.log("filr message list is "+obj);
    var msgList = '';
    var message = obj;
    var user = message.sender;

    if (isCurrentUser(user.userId)) {
        var messageTime = moment(message.createdAt).fromNow();
        var readReceiptHtml = '  <label class="chat-canvas__list-readreceipt"></label>';
        var msg = '' +
            '<li class="DirectMessage DirectMessage--sent clearfix dm js-dm-item" data-message-id="%messageId%">\
                <div class="DirectMessage-container">\
                    <div class="DirectMessage-avatar">\
                        <a href="#" class="js-action-profile js-user-profile-link">\
                            <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">\
                                <span class="DMAvatar-container">\
                                    <img class="DMAvatar-image" src="%userAvatar%" alt="" title="">\
                                </span>\
                            </div>\
                        </a>\
                    </div>\
                    <div class="DirectMessage-message with-media dm-message u-chromeOverflowFix">\
                        <div class="DirectMessage-attachmentContainer">\
                            <div class="DirectMessage-media u-borderRadiusInherit u-chromeOverflowFix">\
                                <div class="Sensitive">\
                                    <div class="Sensitive-content">\
                                        <div class="Media u-chromeOverflowFix">\
                                            <div class="Media-photo u-chromeOverflowFix">\
                                                <a href="%imgMessageUrl%" class="dm-attached-media">\
                                                    <div class="FlexEmbed u-borderRadiusInherit">\
                                                        <div class="FlexEmbed-item u-borderRadiusInherit">\
                                                            <img src="%imgMessageUrl%" onload="loadChatImage(this)" />\
                                                        </div>\
                                                    </div>\
                                                </a>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                        <div class="DirectMessage-contentContainer"></div>\
                    </div>\
                    <div class="DirectMessage-actions">\
                        <span class="DirectMessage-action">\
                            <button type="button" class="DMReportMessageAction js-tooltip transparent-no-border-button" aria-hidden="true">\
                                <span class="Icon Icon--report"></span>\
                            </button>\
                            <button type="button" class="DMReportMessageAction u-hiddenVisually transparent-no-border-button"></button>\
                        </span>\
                        <span class="DirectMessage-action">\
                            <button type="button" class="DMDeleteMessageAction js-tooltip transparent-no-border-button" aria-hidden="true" onclick="DMConversation_deleteChatMessage(%messageId%)">\
                                <span class="Icon Icon--delete"></span>\
                            </button>\
                            <button type="button" class="DMDeleteMessageAction u-hiddenVisually transparent-no-border-button" onclick="DMConversation_deleteChatMessage(%messageId%)"></button>\
                        </span>\
                    </div>\
                </div>\
                <div class="DirectMessage-footer">\
                    <div class="DirectMessage-footerItem">\
                        <div class="DMReadReceipt">\
                            <div class="DMReadReceipt-statusMessage">\
                                <span class="DMReadReceipt-defaultView">\
                                    <span class="_timestamp js-relative-timestamp">%sentMessageTime%</span>\
                                </span>\
                                <span class="DMReadReceipt-statusView">Sent</span>\
                            </div>\
                            <span class="DMReadReceipt-check">\
                                <span class="Icon Icon--checkLight"></span>\
                            </span>\
                        </div>\
                    </div>\
                </div>\
                <div class="DirectMessage-footer">\
                </div>\
            </li>';
        msg = msg.replace('%userAvatar%', xssEscape(user.profileUrl));
        msg = msg.replace(/%imgMessageUrl%/g, xssEscape(message.url));
        msg = msg.replace('%sentMessageTime%', messageTime);
        msg = msg.replace(/%messageId%/g, message.messageId);
        msgList += msg;
    } else {
        var messageTime = moment(message.createdAt).fromNow();
        var msg = '' +
            '<li class="DirectMessage DirectMessage--received clearfix dm js-dm-item" data-message-id="%messageId%">\
                <div class="DirectMessage-container">\
                    <div class="DirectMessage-avatar">\
                        <a href="#" class="js-action-profile js-user-profile-link">\
                            <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">\
                                <span class="DMAvatar-container">\
                                    <img class="DMAvatar-image" src="%userAvatar%" />\
                                </span>\
                            </div>\
                        </a>\
                    </div>\
                    <div class="DirectMessage-message with-media dm-message u-chromeOverflowFix">\
                        <div class="DirectMessage-attachmentContainer">\
                            <div class="DirectMessage-media u-borderRadiusInherit u-chromeOverflowFix">\
                                <div class="Sensitive">\
                                    <div class="Sensitive-content">\
                                        <div class="Media u-chromeOverflowFix">\
                                            <div class="Media-photo u-chromeOverflowFix">\
                                                <a href="%imgMessageUrl%" class="dm-attached-media">\
                                                    <div class="FlexEmbed u-borderRadiusInherit">\
                                                        <div class="FlexEmbed-item u-borderRadiusInherit">\
                                                            <img src="%imgMessageUrl%" onload="loadChatImage(this)" />\
                                                        </div>\
                                                    </div>\
                                                </a>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                        <div class="DirectMessage-contentContainer"></div>\
                    </div>\
                </div>\
                <div class="DirectMessage-footer">\
                    <span class="DirectMessage-footerItem">%senderUserName%</span>\
                    <span class="DirectMessage-footerItem">\
                        <span class="_timestamp">%messageTime%</span>\
                    </span>\
                </div>\
                <div class="DirectMessage-footer">\
                    <div class="DMReadReceiptUserList DirectMessage-footerItem u-hidden">\
                        <div class="DMReadReceiptUserList-truncated"></div>\
                        <div class="DMReadReceiptUserList-expanded"></div>\
                    </div>\
                </div>\
            </li>';
        msg = msg.replace('%senderUserName%', message._sender.nickname + '&nbsp;●&nbsp;');
        msg = msg.replace('%messageTime%', messageTime);
        msg = msg.replace('%userAvatar%', xssEscape(user.profileUrl));
        msg = msg.replace('%userReceivedMessage%', convertLinkMessage(xssEscape(message.message)));
        msg = msg.replace(/%imgMessageUrl%/g, xssEscape(message.url));
        msgList += msg;
    }

    return msgList;
}

function setWelcomeMessage(name) {
// $('.chat-canvas').append(
//   '<div class="chat-canvas__list-notice">' +
//   '  <label class="chat-canvas__list-system">' +
//   'Unread Messages' +
//   '  </label>' +
//   '</div>'
// );
}


$('#chat_file_input').change(function() {
    if ($('#chat_file_input').val().trim().length == 0) return;
    var file = $('#chat_file_input')[0].files[0];
    $('.chat-input-file').addClass('file-upload');

    currChannelInfo.sendFileMessage(file, SendMessageHandler);
});

function setImageMessage(obj) {
    $('.DMConversation-content').append(imageMessageList(obj));
    scrollPositionBottom();
}

function setFileMessage(obj) {
    $('.DMConversation-content').append(fileMessageList(obj));
    scrollPositionBottom();
}

$('#chatModal .DMConversation-scrollContainer').on('scroll', function() {
    setTimeout(function() {
        var currHeight = $('#chatModal .DMConversation-scrollContainer').scrollTop();
        if (currHeight == 0) {
            if ($('#chatModal .DMConversation-scrollContainer')[0].scrollHeight > $('#chatModal .DMConversation-scrollContainer').height()) {
                loadMoreChatMessage();
            }
        }
    }, 200);
});

function setSysMessage(obj) {
    $('.chat-canvas').append(
        '<div class="chat-canvas__list-notice">' +
        '  <label class="chat-canvas__list-system">' +
        xssEscape(obj['message']) +
        '  </label>' +
        '</div>'
    );
    scrollPositionBottom();
}

function setBroadcastMessage(obj) {
    $('.chat-canvas').append(
        '<div class="chat-canvas__list">' +
        '  <label class="chat-canvas__list-broadcast">' +
        xssEscape(obj['message']) +
        '  </label>' +
        '</div>'
    );
    scrollPositionBottom();
}

function unreadCountUpdate(channel) {
    var targetUrl = channel.url;

    var callAdd = true;
    var unread = channel.unreadMessageCount > 9 ? '9+' : channel.unreadMessageCount;
    //if (unread > 0 || unread == '9+') {
    if (channel.unreadMessageCount > 0) {
        $.each($('.DMInbox-conversationItem .DMInboxItem'), function(index, item) {
            if ($(item).data("channel-url") == targetUrl) {
                //addUnreadCount(item, unread, targetUrl);
                $(item).addClass('is--unread');
                callAdd = false;
            }
        });

        if (callAdd) {
            showChannel(channel, unread, targetUrl);
        }
    } else {
        showChannel(channel, unread, targetUrl);
    }
}

function addUnreadCount(item, unread, targetUrl) {
    if (targetUrl == currChannelUrl) {
        if (document.hasFocus()) {
            return;
        }
    }

    if ($(item).find('div[class="left-nav-channel__unread"]').length != 0) {
        $(item).find('div[class="left-nav-channel__unread"]').html(unread);
    } else {
        $('<div class="left-nav-channel__unread">' + unread + '</div>')
            .prependTo('.left-nav-channel-group[data-channel-url='+targetUrl+']');
    }
}

function showChannel(channel, unread, targetUrl) {
    var members = channel.members;
    var channelMemberList = '';
    $.each(members, function(index, member) {
        if (currentUser.userId != member.userId) {
            channelMemberList += xssEscape(member.nickname) + ', ';
        }
    });
    channelMemberList = channelMemberList.slice(0, -2);
    //addGroupChannel(true, channelMemberList, channel);

    if (unread != 0) {
        $.each($('.left-nav-channel'), function(index, item) {
            if ($(item).data("channel-url") == targetUrl) {
                addUnreadCount(item, unread, targetUrl);
            }
        });
    }
}
/***********************************************
 *          // END Common Function
 **********************************************/


$('.right-section__modal-bg').click(function() {
    navInit();
    popupInit();
});

function navInit() {
    $('.right-section__modal-bg').hide();

    // OPEN CHAT
    $('#btn_open_chat').removeClass('left-nav-open--active');
    $('.modal-open-chat').hide();

    // MESSAGING
    $('#btn_messaging_chat').removeClass('left-nav-messaging--active');
    $('.modal-messaging').hide();
}

function popupInit() {
    $('.modal-member').hide();
    $('.chat-top__button-member').removeClass('chat-top__button-member--active');
    $('.modal-invite').hide();
    $('.chat-top__button-invite').removeClass('chat-top__button-invite--active');
}

function init() {
    // userId = decodeURI(decodeURIComponent(getUrlVars()['userid']));
    // nickname = decodeURI(decodeURIComponent(getUrlVars()['nickname']));
    var accessToken = $("#user-accessToken").val();
    nickname = $("#userNickName").val();
    userId = $("#user-userId").val();
    userId = checkUserId(userId);
    $('.init-check').show();
    startSendBird(userId,accessToken, nickname);
    $('.left-nav-user-nickname').html(xssEscape(nickname));
}

// Add unread message count to header messages toolbox.
function notifyTotalUnreadCount() {
    var unreadCount = 0;
    $.each($('.DMInboxItem.is--unread'), function(index, item) {
        unreadCount ++;
    });

    unreadCount = (unreadCount > 9)? '9+' : unreadCount;
    if (unreadCount == 0) {
        if (!$('.main-header .nav>li.js_live-messages>a .label').hasClass('hidden')) {
            $('.main-header .nav>li.js_live-messages>a .label').addClass('hidden');
        }
    } else if (unreadCount > 0) {
        if ($('.main-header .nav>li.js_live-messages>a .label').hasClass('hidden')) {
            $('.main-header .nav>li.js_live-messages>a .label').removeClass('hidden');
        }

        $('.main-header .nav>li.js_live-messages>a .label').text(unreadCount);
    }
}

$(document).ready(function() {
    // notifyMe();
    init();
});

window.onfocus = function() {
    if (currChannelInfo && !currChannelInfo.isOpenChannel()) {
        currChannelInfo.markAsRead();
    }
    $.each($('.left-nav-channel'), function(index, item) {
        if ($(item).data("channel-url") == currChannelUrl) {
            $(item).find('div[class="left-nav-channel__unread"]').remove();
        }
    });
};

/***********************************************
 *  Set the action for mark-all-read button
 **********************************************/
$('button.mark-all-read').click(function () {
    var channelListQuery = sb.GroupChannel.createMyGroupChannelListQuery();
    channelListQuery.includeEmpty = true;
    channelListQuery.limit = 100; // pagination limit could be set up to 100

    if (channelListQuery.hasNext) {
        channelListQuery.next(function(channelList, error){
            if (error) {
                console.error(error);
                return;
            }
            channelList.forEach(function(channel) {
                channel.markAsRead();
                $('li.DMInbox-conversationItem .DMInboxItem[data-channel-url="' + channel.url + '"]').removeClass('is--unread');
            });
        });
    }
});

/***********************************************
 *  Open Dialog for New Channel
 **********************************************/
$('#myModal .DMComposeButton').click(function() {
    $('#myModal').on('hidden.bs.modal', function () {
        $('body').addClass('modal-open');
    });
    $('#myModal').modal('hide');
    $('#newMessagesModal').modal('show');
    $('#newMessagesModal').on('shown.bs.modal', function () {
        $('body').addClass('modal-open');
    });
    //$('body').addClass('modal-open');

    setRecentListToNewDialog();
});

/***********************************************
 *  Name TextArea Action on New Dialog
 **********************************************/
$('.TokenizedMultiselect-input').keyup(function() {
    /*if (event.keyCode == 13) {
        alert();
    }*/
});

/***********************************************
 *  Recent Channels on New Dialog
 **********************************************/
function setRecentListToNewDialog() {
}

/***********************************************
 *  Back Button Action for New Dialog
 **********************************************/
$('#newMessagesModal .DMActivity-back').click(function () {
    $('#newMessagesModal').on('hidden.bs.modal', function () {
        $('body').addClass('modal-open');
    })
    $('#newMessagesModal').modal('hide');
    //$('body').removeClass('modal-open');
    $('#myModal').modal('show');
    $('body').addClass('modal-open');

});

/***********************************************
 *  Close Button Action for New Dialog
 **********************************************/
$('#newMessagesModal .DMActivity-close').click(function () {
    $('#newMessagesModal').on('hidden.bs.modal', function () {
    });
    $('#newMessagesModal').modal('hide');
    $('body').removeClass('modal-open');
    // $('.custom-overlay').hide();
});

$('#myModal .DMActivity-close').click(function () {

    $('body').removeClass('modal-open');
    // $('.custom-overlay').hide();
});

/***********************************************
 *  Show Emoji Dialog
 **********************************************/
$('button.EmojiPicker-trigger').click(function () {
    if ($('.EmojiPicker-dropdownMenu').css('display') == 'block') {
        $('.EmojiPicker-dropdownMenu').hide();
    } else {
        $('.EmojiPicker-dropdownMenu').show();
    }
});

/***********************************************
 *  Emoji Icon Action
 **********************************************/
$('.EmojiPicker-dropdownMenu .EmojiPicker-item').click(function () {
    var emoticon = twemoji.parse(
        twemoji.convert.fromCodePoint($(this).data('key')),
        function(icon, options, variant) { return; }
    );
    insertTextAtCursor(emoticon);
});

function insertTextAtCursor(text) {
    var sel, range, html;
    $('.DMComposer-editor.rich-editor').focus();
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();
            range.insertNode( document.createTextNode(text) );
            // Enable Send Button
            $('.DMComposer-send button').attr('aria-disabled', false);
            $('.DMComposer-send button').removeClass('disabled');
        }
    } else if (document.selection && document.selection.createRange) {
        console.log(document.selection);
        document.selection.createRange().text = text;
        $('.DMComposer-send button').attr('aria-disabled', false);
        $('.DMComposer-send button').removeClass('disabled');
    }
}

/***********************************************
 *  Show Conversation Settings Dialog
 **********************************************/
$('.DMConversation .DMConversation-convoSettings').click(function() {
    $('#chatModal').on('hidden.bs.modal', function () {
        $('body').addClass('modal-open');
    });
    $('#chatModal').modal('hide');

    // Set conversation information
    $('#conversationSettingsModal .DMUpdateName-name').text($('#chatModal .DMUpdateName-name').text());
    var avatarUrl = $('#chatModal .DMUpdateAvatar-avatar img.DMAvatar-image').attr('src');
    $('#conversationSettingsModal .DMAvatar-image').attr('src', avatarUrl);
    $('#conversationSettingsModal').modal('show');
});

/***********************************************
 *  Leave Conversation Action on Conversation Settings Dialog
 **********************************************/
$('#conversationSettingsModal .js-actionDeleteConversation').click(function() {
    leaveCurrChannel();
});

/***********************************************
 *  Close Conversation Settings Dialog
 **********************************************/
$('#conversationSettingsModal .DMActivity-close').click(function() {
    $('#chatModal').on('hidden.bs.modal', function () {
    });
    $('#conversationSettingsModal').modal('hide');
});

/***********************************************
 *  Back to Chat Dialog from Conversation Settings Dialog
 **********************************************/
$('#conversationSettingsModal .DMActivity-back').click(function() {
    $('#conversationSettingsModal').on('hidden.bs.modal', function () {
    });
    $('#conversationSettingsModal').modal('hide');
    $('#chatModal').modal('show');
    $('body').addClass('modal-open');
});

/***********************************************
 *  Keyboard event about New Dialog Name Search
 **********************************************/

var autocomplete_direct_chat_ajx = 'ToCancelPrevUsrSearchReq';

$('#newMessagesModal textarea.twttr-directmessage-input').keyup(function() {

	$('#DMComposeTypeaheadSuggestions').html('<li class="loading" id="user_loading_id" style="display: none"><div class="loader loader_medium"></div></li>');
	$('#user_loading_id').show();
    var action = "/includes/ajax/users/message_autocomplete.php";
    autocomplete_direct_chat_ajx =  $.ajax({
        url: action,
        data: {
            'query': $('#newMessagesModal textarea.twttr-directmessage-input').val(),
            'skipped_ids': '[]'
        },
        type: 'post',
		beforeSend : function() {
            if(autocomplete_direct_chat_ajx != 'ToCancelPrevUsrSearchReq') {
                  autocomplete_direct_chat_ajx.abort();
            }
        },
        success: function(data) {
			$('#user_loading_id').hide();
			if(data['autocomplete'] != 0){
				$('#DMComposeTypeaheadSuggestions').html(data['autocomplete']);
			}
			else{
				$('#DMComposeTypeaheadSuggestions').html('<li class="DMTypeaheadHeader"><span style="display:block;">Sorry! No Record Found</span></li>');
			}
        }
    });
});

/***********************************************
 *  Add new user to the new group channel on New Dialog
 **********************************************/
function addNewMemberOnNewDialog(fullname, tokenId) {
    var newUser = '<li class="InputToken" onclick="$(this).remove();" tabindex="-1" data-token-text="' + fullname +
        '" data-token-id="' + tokenId +
        '" aria-label="' + fullname + '">' + fullname +
        '<span class="InputToken-delete Icon Icon--close" aria-hidden="true"></span>';
    $('#TokenizedMultiselect-root').prepend(newUser);

    $('#newMessagesModal textarea.twttr-directmessage-input').val('');
    $('#newMessagesModal textarea.twttr-directmessage-input').focus();
    $('#DMComposeTypeaheadSuggestions').html('');
}

/***********************************************
 *  Create new group channel on New Dialog
 **********************************************/
$('#newMessagesModal .dm-initiate-conversation').click(function () {
	var all = new Array();

    if($( "#TokenizedMultiselect-root > li" ).hasClass( "InputToken" )){
        console.log('check InputToken2');
        $('#TokenizedMultiselect-root li').each(function() {
            all.push($(this).attr('data-token-id'));
        });
        toConenctUid = $('#user-userId').val();
        all.push(toConenctUid);
        console.log(JSON.stringify(all));
        sendMultiple(all,true);
    }else{
        return false;
    }
});
