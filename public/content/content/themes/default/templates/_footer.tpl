<!-- ads -->
{include file='_ads.tpl' _ads=$ads_master['footer'] _master=true}
<!-- ads -->

<!-- footer -->
<div class="container">

	<div class="row footer">
		<div class="col-lg-6 col-md-6 col-sm-6">
			&copy; {'Y'|date} {$system['system_title']} · <span class="text-link" data-toggle="modal" data-url="#translator"></span>&nbsp;{$system['language']['title']}</span>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 links">
			{if count($static_pages) > 0}
				{foreach $static_pages as $static_page}
					<a href="/static/{$static_page['page_url']}">
						{__("{$static_page['page_title']}")}
					</a>{if !$static_page@last} · {/if}
				{/foreach}
			{/if}
			{if $system['contact_enabled']}
				 ·
				<a href="/contacts">
					{__("Contacts")}
				</a>
			{/if}
			·
			<a href="/message_to_miles">
					{__("Message to Guo")}
				</a>
				{if $user->_data['user_id']==296}
				<a data-toggle="modal" data-target="#do_broad" href="javascript:void(0)">Do-Broadcast</a>
				{/if}
			{*if $system['directory_enabled']}
				 ·
				<a href="/directory">
					{__("Directory")}
				</a>

			{/if*}
			{if $system['market_enabled']}

            {/if}
		</div>
	</div>
</div>
<!-- footer -->

</div>
<!-- main wrapper -->
    <div  id="do_broad" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
          <iframe style="border:none;" src="/do_broadcast" height="280px;" width="100%"></iframe>

      </div>
    </div>

  </div>
</div>
<!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] -->
{if !$user->_logged_in}
<link rel="stylesheet" href="/includes/assets/css/font-awesome/css/font-awesome.min.css">
{/if}
<link rel="stylesheet" href="/includes/assets/css/twemoji-awesome/twemoji-awesome.min.css">
<link rel="stylesheet" href="/includes/assets/css/flag-icon/css/flag-icon.min.css">
<!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] -->

<script>
$("document").ready(function(){	
   // alert('h22');
});
</script>

<!-- JS Files -->
{include file='_js_files.tpl'}
<!-- JS Files -->

<!-- JS Templates -->
{include file='_js_templates.tpl'}
<!-- JS Templates -->

{if $user->_logged_in}
	<!-- Notification -->
	 <audio id="notification_sound">
		<source src="/includes/assets/sounds/notification.mp3" type="audio/mpeg">
	</audio>
	<!-- Notification -->
	<!-- Chat -->
	<audio id="chat_sound">
		<source src="/includes/assets/sounds/chat.mp3" type="audio/mpeg">
	</audio>
	<!-- Chat -->
	<!-- Call -->
	<audio id="call_sound">
		<source src="/includes/assets/sounds/call.mp3" type="audio/mpeg">
	</audio>
	<!-- Call -->
	<!-- Video -->
	<audio id="video_sound">
		<source src="/includes/assets/sounds/video.mp3" type="audio/mpeg">
	</audio>
	<!-- Video -->
{/if}

{if $user->_data['theme_color'] != ''} 
	{include file='theme_color.tpl'}
{/if}
<script type="text/javascript">
    $('.cross_dismiss').click(function () {
        location.reload('/notifications');
    });
</script>
</body>
</html>