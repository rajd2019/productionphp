<!-- ads -->
{include file='_ads.tpl' _ads=$ads_master['footer'] _master=true}
<!-- ads -->
<!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] -->
<link rel="stylesheet" href="/includes/assets/css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/includes/assets/css/twemoji-awesome/twemoji-awesome.min.css">
<link rel="stylesheet" href="/includes/assets/css/flag-icon/css/flag-icon.min.css">
<script src="{$system['system_url']}/includes/assets/js/jquery/jquery-3.2.1.min.js" ></script>
<script src="/includes/assets/js/jquery/jquery-3.2.1.min.js" ></script>
<!-- JS Files -->
{include file='_js_files.tpl'}
<!-- JS Templates -->
{include file='_js_templates.tpl'}
<!-- JS Templates -->
<script type="text/javascript">
$(document).ready(function() {
	$("#signup_show").click(function() {
	  $(".twitter-main").addClass("signup-block");
	});
	$("#back").click(function() {
	  $(".twitter-main").removeClass("signup-block");
	});
});
</script>
<!-- Analytics Code -->
{if $system['analytics_code']}{html_entity_decode($system['analytics_code'], ENT_QUOTES)}{/if}
<!-- Analytics Code -->
</body>
</html>