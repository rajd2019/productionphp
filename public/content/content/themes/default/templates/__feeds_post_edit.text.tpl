<style>
.post-replace .post-text{
border-bottom:0px !important;
line-height:normal;
white-space: normal;
}
.post-replace .post-text p {
    margin-bottom: 0px !important;
    line-height: 22px !important;
}
.btn-translate {
    ffloat: none;
	display: block !important;
	margin: 0 0 10px;
	text-align: right;
}
a[data-readmore-toggle="rmjs-3"] {
    position: absolute;
}
.btn-translate span:hover {
    color: #2a78c2;
    text-decoration: underline;
}
.btn-translate span {
    color: #657786;
    text-decoration: none;
}
span.trans_by {
    font-size: 12px;
    margin: 9px 0 0;
    display: block;
    color: #000;
	font-family: 'Segoe UI',SegoeUI,"Helvetica Neue",Helvetica,Arial,sans-serif;
    font-weight: bold;
}
span.trans_by img {
    width: 14px;
    margin: -4px 0 0;
}
</style>
<div class="post-replace post-listing" data-id="{$post['post_id']}" >

    {if $_post['broadcast_name'] != ""}

    <div class="post-text js_readmore" dir="auto">{$_post['broadcast_name']}</div>

    {/if}

    <div class="post-text js_readmore rrr" dir="auto" data-id="{$post['post_id']}">{$post['text_plain']|unescape:'html'}</div>

    <div class="post-text-plain hidden" id="hidden-post-{$post['post_id']}">{$post['text_plain']}</div>

</div>