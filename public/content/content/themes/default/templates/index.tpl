{include file='_head.tpl'}
<style type="text/css">

	#newFeatureModal {
		background: rgba(0,0,0,0.5);
	}
	#newFeatureModal .modal-dialog {
		max-width: 90%;
		margin: auto !important;
		transform: translateY(-50%);
		top: 50%;
	}
	#newFeatureModal li {
	   position: relative;
	}
	#newFeatureModal ol li::before {
	   content: "\f0da";
	   display: block;
	   height: 0;
	   width: 0;
	   left: -1em;
	   font-family: FontAwesome;
	   font-size: 20px;
	   line-height: 20px;
	   position: absolute;
	}
    /*8oct*/
    .upload_share {
        width: 100%;
        float: left; 
        position:relative; 
        overflow:hidden;
    }
    .upload_share .x-uploader {
        overflow: visible;  
        width: 100%;  
        float: left; 
        position: static;
    }
    .upload_share .x-uploader input[type=file] { 
        font-size: 0;
    }
    .upload_share .publisher-tools-attach { 
        position: absolute; 
        overflow: visible; 
        left: 0; 
        top: 0; 
        width: 100%; 
        opacity: 0; 
        height: 100%;
    }
    .upload_share i.fa-video-camera {
         display:none;
    }
    .upload_share .publisher-tools-attach {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
	</style>
{if !$user->_logged_in}

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet"> 
	<link rel="stylesheet" href="/content/themes/default/css/login/style.css">
	<link rel="stylesheet" href="/content/themes/default/css/login/jquery.bxslider.css">
	 
	  <div class="twitter-main">
            <div class="twitter-header">
               <div class="container">
                  <div class="top-box">
              <div class="left-section">
              <a href="/" class="logo"><img src="/content/themes/default/images/brand.svg" alt="guo.media" title="guo.media" width="135"></a>
               <ul>
                   
                  <li><a href=""><i class="Icon Icon--home Icon--large"></i>{__("Home")}</a></li>
                  <!--<li><a href="/static/about">About</a></li>-->
                  <li><a href="/milesguo"><i class="hand_icon"><img src="/content/themes/default/images/hand.svg" alt="hand" width="21"></i>{__("Guo")}</a></li>
               
               </ul>
               </div>
              <div class="right-section">
			  {if !$user->_logged_in}
              
                        <span class="text-link" data-toggle="modal" data-url="#translator">
                        <!-- <span class="flag-icon flag-icon-{$system['language']['flag_icon']}"></span> -->
						 {$system['language']['title']}</span>
						 <span class="contact-icon watch_broadcasts_logout">
                            <a href="/broadcasts">
                                 <span class="Icon icon_custom Icon--cameraVideo Icon--small"></span> {__("GuoTV")}
                            </a>
                        </span>
                        <a href="/broadcasts" {if $last_dir == 'broadcasts'} class="active" {/if}><span class="broadcasts_icon Icon Icon--cameraVideo Icon--large"></span></a>
                        {/if}
               
              </div>
              </div>
               </div>
            </div>
            
            <!--<div class="twitter-header twitter_header_mobile">
               <div class="container">
                  <div class="top-box">
                  
                  <div class="left-section">
                   <ul>
                       
                      <li class="back"><a href="javascript:void(0)" id="back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a></li>
                      <li  class="none_link"><a href="javascript:void(0)" id="signup_show">Sign up</a></li>
                      <li  class="none_link"><a href="/milesguo" id="signup_show">Miles Guo</a></li>
                   
                   </ul>
                   </div>
                   
                  <a href="/" class="logo"><img src="/content/themes/default/images/logo.jpg" alt="guo.media" title="guo.media" width="80"></a>
                   
                  <div class="right-section">
                  {if !$user->_logged_in}
                            <span class="text-link" data-toggle="modal" data-url="#translator">
                            <span class="flag-icon flag-icon-{$system['language']['flag_icon']}"></span>
                             {$system['language']['title']}</span>
                            {/if}
                   
                  </div>
                  
              </div>
               </div>
            </div>-->
            
            
            <div class="slider-main-box">
                <div class="banner-section">
                       <div class="main-slider">
                            <div class="slide">
                               <div class="featured-img"><img src="/content/uploads/files/GUO MEDIA 1.jpg" alt="GUO MEDIA 1"></div>
                             </div>
                           
							<div class="slide">
                               <div class="featured-img"><img  src="/content/uploads/files/GUO MEDIA 2.jpg" alt="GUO MEDIA 2"></div>
                             </div>
                             <div class="slide">
                               <div class="featured-img"><img  src="/content/uploads/files/GUO MEDIA 3.jpg"  alt="GUO MEDIA 3"></div>
                             </div> 
                        </div>
              </div>
              <div class="banner-text-section">
              <div class="container">
                  <div class="row">
                     <div class="col-sm-12 col-md-6 col-lg-8">
                        <div class="banner-text-inner">
						
						   {if $system['language']['code']=='zn-tw'}
                           <h1>郭媒體 GUO.MEDIA</h1>
						   {else}
						    <h1>郭媒体 GUO.MEDIA</h1>
						   {/if}
                            <h2>{__("Everything is just beginning")} <i class="fa fa-hand-grab-o" aria-hidden="true"></i> <i class="fa fa-hand-grab-o" aria-hidden="true"></i> <i class="fa fa-hand-grab-o" aria-hidden="true"></i> !</h2>
           <p>{__("GUO.MEDIA is now launching our beta version. We strive to provide a free social networking and livestreaming platform where everyone can express their views and opinions freely.")}</p>
                        
                        
                        </div>
                       
                      <div class="login_footer"> 
                     	
<!-- footer -->
<div class="container">

	<div class="row footer">
		<div class="col-lg-6 col-md-6 col-sm-6">
			&copy; {'Y'|date} {$system['system_title']} · <span class="text-link" data-toggle="modal" data-url="#translator"><span class="flag-icon flag-icon-{$system['language']['flag_icon']}"></span>&nbsp;{$system['language']['title']}</span>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 links">
			{if count($static_pages) > 0}
				{foreach $static_pages as $static_page}
					<a href="/static/{$static_page['page_url']}">
						{__("{$static_page['page_title']}")}
					</a>{if !$static_page@last} · {/if}
				{/foreach}
			{/if}
			{if $system['contact_enabled']}
				 · 
				<a href="/contacts">
					{__("Contacts")}
				</a>
			{/if}
			{*if $system['directory_enabled']}
				 · 
				<a href="/directory">
					{__("Directory")}
				</a>
			{/if*}
			{if $system['market_enabled']}
                 · 
                <a href="/market">
                    {__("Market")}
                </a>
            {/if}
		</div>
	</div>
</div>
						 </div>
<!-- footer -->
                     </div>
                     <div class="col-sm-12 col-md-6 col-lg-4">
					 <form class="js_ajax-forms" data-url="core/signin.php" method="post">
                        <div class="login-section">
                           <div class="form-box">
							    <input name="username_email" type="text" placeholder='{__("Email")} {__("or")} {__("Username")}'  value="{if $username_email}{$username_email}{/if}" required>
                              <input name="password" type="password" placeholder='{__("Password")}' value="" required>
							  <input type="submit" value='{__("Login")}'/>
                           </div>
                          <div class="check-box">

                          <input name="remember"  type="checkbox" {if $username_email}checked{/if}>
                          <label for="remember">{__("Remember me")}</label>
                              | <a href="/reset">{__("Forgotten password?")}</a> </div>
                        </div>
						<!-- error -->
						<div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
						<!-- error -->
                        </form>

                        <div class="sign-section">
                           <h4>{__("Sign up")}</h4>
                           <div class="form-box">
						     <form class="js_ajax-forms" data-url="core/signup.php" method="post">
							 {*<div class="full-name">*}
                               {*<input type="text" value="" placeholder="{__("Full name")}" name="first_name" required/>*}
							 {*</div>
							 <div class="user-name">
								<input type="text" value="" placeholder='{__("Last name")}' name="last_name" />
							</div>*}
                               <input type="text" value="" placeholder='{__("Username")}' name="username" />
                             <!--   <input type="email" placeholder='{__("Email")}'  name="email" required/> -->
							{if $system['activation_enabled'] && $system['activation_type'] == "sms"}
							   <input name="phone" type="text" placeholder='{__("Phone number (eg. +905...)")}' required>
							  {/if}
                              <input name="password" type="password" placeholder='{__("Password")}' required/>
							  {if $system['reCAPTCHA_enabled']}                                 
                                        
									<div class="col-sm-12" style="margin-bottom:12px;">
										<!-- reCAPTCHA -->
										<script src='https://www.google.com/recaptcha/api.js'></script>
										<div class="g-recaptcha" data-sitekey="{$system['reCAPTCHA_site_key']}"></div>
										<!-- reCAPTCHA -->
									</div>
                                  
                                {/if}
							   <!-- error -->
								<div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
								<!-- error -->
                              <input type="submit" value='{__("Sign Up")}'/>
							  </form>
                              
                              <div class="befound"><p>{__("By signing up, you agree to our")} <a href="/static/terms">{__("Terms of Service")}</a> {__("and")} <a href="/static/privacy">{__("Privacy Policy")}</a>, {__("and")} <a href="/static/privacy#cookie">{__("Cookie Use")}</a> </p></div>
                              
                           </div>
                          
                        </div>
                        
                        <div class="google_link mobile_google_link">
                        <a href="https://play.google.com/store/apps/details?id=com.media.guo"><img  src="/content/themes/default/images/google-play.png" alt="google-play"></a>
                        
                        </div>
                       
                     </div>
                      </div>
                  
                  </div>
                
              </div>
           </div>
    </div>
	<div class="overlay"></div>
	{include file='_footer_login.tpl'}
    <!-- page content -->
	
{else}
	{include file='_header.tpl'}
    <!-- page content -->
    <div class="container mt20 offcanvas">
        <div class="row">

            <!-- left panel -->
            <div class="col-sm-4 col-md-3 offcanvas-sidebar">
                {include file='_sidebar.tpl'}
            </div>
            <!-- left panel -->

            <div class="col-sm-8 col-md-9 offcanvas-mainbar">
                <div class="row">
                    <!-- center panel -->
                    <div class="col-sm-12 col-md-8">

                        <!-- announcments -->
                        {include file='_announcements.tpl'}
                        <!-- announcments -->

                        {if $view == ""}

                         
                            <!-- stories -->

                            <!-- publisher -->
                            {include file='_publisher.tpl' _handle="me" _privacy=true}
                            <!-- publisher -->
                             
                            <!-- boosted post -->
                            {if $boosted_post}
                                {include file='_boosted_post.tpl' post=$boosted_post}
                            {/if}
                            <!-- boosted post -->

                            {if $pinned_post} 
                                {include file='_pinned_post.tpl' post=$pinned_post} 
                            {/if}
                            
                            <!-- posts stream -->
                            {include file='_posts.tpl' _get="newsfeed"}
                            <!-- posts stream -->

                        {elseif $view == "saved"}
                            <!-- saved posts stream -->
                            {include file='_posts.tpl' _get="saved" _title=__("Saved Posts")}
                            <!-- saved posts stream -->

                        {elseif $view == "articles"}
                            <!-- saved posts stream -->
                            {include file='_posts.tpl' _get="posts_profile" _id=$user->_data['user_id'] _filter="article" _title=__("My Articles")}
                            <!-- saved posts stream -->

                        {elseif $view == "products"}
                            <!-- saved posts stream -->
                            {include file='_posts.tpl' _get="posts_profile" _id=$user->_data['user_id'] _filter="product" _title=__("My Products")}
                            <!-- saved posts stream -->

                        {/if}
                    </div>
                    <!-- center panel -->

                    <!-- right panel -->
                    <div class="col-sm-12 col-md-4">
                        <!-- pro members -->
                        {if count($pro_members) > 0}
                            <div class="panel panel-default panel-friends">
                                <div class="panel-heading">
                                    {*if $system['packages_enabled'] && !$user->_data['user_subscribed']}
                                        <!--<div class="pull-right flip">
                                            <small><a href="/packages">{__("Upgrade")}</a></small>
                                        </div>-->
                                    {/if*}
                                    <strong class="text-primary"><i class="fa fa-bolt"></i> {__("Pro Users")}</strong>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        {foreach $pro_members as $_member}
                                            <div class="col-xs-4">
                                                <a class="friend-picture" href="/{$_member['user_name']}" style="background-image:url({$_member['user_picture']});" >
                                                    <span class="friend-name">{$_member['user_firstname']} {$_member['user_lastname']}@{$_member['user_name']}</span>
                                                </a>
                                            </div>
                                        {/foreach}
                                    </div>
                                </div>
                            </div>
                        {/if}
                        <!-- pro members -->

                        <!-- boosted pages -->
                        {if count($promoted_pages) > 0}
                            <div class="panel panel-default panel-friends">
                                <div class="panel-heading">
                                    {if $system['packages_enabled'] && !$user->_data['user_subscribed']}
                                        <div class="pull-right flip">
                                            <small><a href="/packages">{__("Upgrade")}</a></small>
                                        </div>
                                    {/if}
                                    <strong class="text-primary"><i class="fa fa-bullhorn fa-fw"></i> {__("Pro Pages")}</strong>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        {foreach $promoted_pages as $_page}
                                            <div class="col-xs-4">
                                                <a class="friend-picture" href="/pages/{$_page['page_name']}" style="background-image:url({$_page['page_picture']});" >
                                                    <span class="friend-name">{$_page['page_title']}</span>
                                                </a>
                                            </div>
                                        {/foreach}
                                    </div>
                                </div>
                            </div>
                        {/if}
                         <!-- boosted pages -->

                        {include file='_ads.tpl'}
                        {include file='_widget.tpl'}

                        <!-- people you may know -->
                        
                            <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <div class="pull-right flip">
                                        <small><a href="/people">{__("View All")}</a></small>
                                    </div>
                                    <strong class="wtf-module">{__("Who to follow")}</strong>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                    {if count($user->_data['new_people']) > 0}
                                        {foreach $user->_data['new_people'] as $_user}
                                        {include file='__feeds_user.tpl' _connection="add" _small=true}
                                        {/foreach}
                                     {/if}
                                     {if count($user->_data['new_people']) <= 0}
                                     <p class="text-center text-muted">
                                        {__("No people available")}
                                     </p>
                                     {/if}
                                    </ul>
                                </div>
                            </div>
                        
                        <!-- Upload Video/Images added by yasir 08-10-2018 --->
                        <!--
                        <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <strong class="wtf-module">Youtube</strong>
                                </div>
                                <div class="panel-body">
                                    <p class="text-center text-muted upload_share">Upload/Share
                                       <span class="publisher-tools-attach js_publisher-tab" data-tab="video">
                                            <i class="fa fa-video-camera fa-fw js_x-uploader" data-handle="publisher"  data-type="video"></i>
                                        </span>
                                    </p>
                                </div>
                        </div>
                        <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <strong class="wtf-module">Twitter</strong>
                                </div>
                                <div class="panel-body">
                                    <p class="text-center text-muted upload_share">Upload/Share
                                        <span class="publisher-tools-attach js_publisher-tab" data-tab="video">
                                            <i class="fa fa-video-camera fa-fw js_x-uploader" data-handle="publisher"  data-type="video"></i>
                                        </span>
                                    </p>
                                </div>
                        </div>
                        -->
                        <!-- mini footer -->
                        {if count($user->_data['new_people']) > 0 || count($new_pages) > 0 || count($new_groups) > 0 || count($new_events) > 0}
                            <div class="row plr10 hidden-xs">
                                <div class="col-xs-12 mb5">
                                    {if count($static_pages) > 0}
                                        {foreach $static_pages as $static_page}
                                            <a href="/static/{$static_page['page_url']}">
                                                {__("{$static_page['page_title']}")}
                                            </a>{if !$static_page@last} · {/if}
                                        {/foreach}
                                    {/if}
                                    {if $system['contact_enabled']}
                                         · 
                                        <a href="/contacts">
                                            {__("Contacts")}
                                        </a>
                                    {/if}
                                    {if $system['directory_enabled']}
                                         · 
                                        <a href="/directory">
                                            {__("Directory")}
                                        </a>
                                    {/if}
                                    {if $system['market_enabled']}
                                         · 
                                        <a href="/market">
                                            {__("Market")}
                                        </a>
                                    {/if}
                                </div>
                                <div class="col-xs-12">
                                    &copy; {'Y'|date} {$system['system_title']} · <span class="text-link dev" data-toggle="modal" data-url="#translator">{$system['language']['title']}</span>
                                    
{*<div id="google_translate_element"></div>*}
                                </div>
                            </div>
                        {/if}
                        <!-- mini footer -->
                    </div>
                    <!-- right panel -->
                </div>
            </div>

        </div>
		<!--Start: new Features updates pop-up-->
        <div id="newFeatureModal" class="modal fade">

            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					{if $website_current_lang == 'en_us'}
                        <h4 class="modal-title">New Features Update</h4>
					{else}
						<h4 class="modal-title">新功能更新</h4>	
					{/if}	
                    </div>

                    <div class="modal-body">

                        {if $website_current_lang == 'en_us'}
                        <ol >
                          <li >User friendly text editor</li>
                          <li >Technical email support features</li>
                          <li >Image zooming and swiping facility</li>
                          <li >Optimized App performance</li>                         
                          <li >Protect posted pictures and prevent video download</li>
                          <li >Water marking on images and videos</li>
                        </ol>
                        {else}
                         <ol >
                          <li >分享发文</li>
                          <li >用户有好的文本编辑</li>
                          <li >技术支持功能</li>
                          <li >图像缩放</li>
                          <li >APP的优化性能</li>
                          <li >上传照片的安全性</li>
                          <li >图片的水印</li>
                        </ol>
                        {/if}

                    </div>

                </div>

            </div>

        </div>
        <!--end-->

       
       {if $current_user_feature_status == 1}
            <input type="hidden" id="show_feature_popup" name="show_feature_popup" value="1">
            {else}
            <input type="hidden" id="show_feature_popup" name="show_feature_popup" value="0">
            {/if}
    </div>
    <!-- page content -->
  
{literal}
      <script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script>
$(document).ready(function(){
    $(".nav.nav-tabs li a").click(function(){
        $(".alert.alert-success.mb0.mt10.x-hidden").remove();
    });
	
	var show_feature_popup = $('#show_feature_popup').val();
   

    if(show_feature_popup == 1){
        $('#newFeatureModal').modal('show');

        setTimeout(function() {
          closeFeaturePopup();
        }, 10000);
    }
});
</script>
{/literal}
{include file='_footer.tpl'}
{/if}



