<!-- post body -->
<div class="post-body {if $_lightbox}pt0{/if}">

    <!-- post header -->
    <div class="post-header">
        <!-- post picture -->
        <div class="post-avatar">
            <a class="post-avatar-picture" href="{$post['post_author_url']}" style="background-image:url({$post['post_author_picture']});">
            </a>
        </div>
        <!-- post picture -->

        <!-- post meta -->
        <div class="post-meta">
            <!-- post menu & author name & type meta & feeling -->
            <div>
                <!-- post author name -->
                <span class="js_user-popover" data-type="{$post['user_type']}" data-uid="{$post['user_id']}">
                    <a href="{$post['post_author_url']}">{$post['post_author_name']}</a>
                </span>
                {if $post['post_author_verified']}
                    {if $post['user_type'] == "user"}
                    <i data-toggle="tooltip" data-placement="top" title='{__("Verified User")}' class="fa fa-check-circle fa-fw verified-badge"></i>
                    {else}
                    <i data-toggle="tooltip" data-placement="top" title='{__("Verified Page")}' class="fa fa-check-circle fa-fw verified-badge"></i>
                    {/if}
                {/if}
                {if $post['user_subscribed']}
                <i data-toggle="tooltip" data-placement="top" title='{__("Pro User")}' class="fa fa-bolt fa-fw pro-badge"></i>
                {/if}
                <!-- post author name -->
            </div>
            <!-- post menu & author name & type meta & feeling -->

            <!-- post time & location & privacy -->
            <div class="post-time">
                <a href="/posts/{$post['post_id']}" class="js_moment" data-time="{$post['time']}">{$post['time']}</a>

                {if $post['location']}
                ·
                <i class="fa fa-map-marker"></i> <span>{$post['location']}</span>
                {/if}

                 
               {* {if $post['privacy'] == "me"}
                    <i class="fa fa-lock" data-toggle="tooltip" data-placement="top" title='{__("Shared with")} {__("Only Me")}'></i>
                {elseif $post['privacy'] == "friends"}
                    <i class="fa fa-users" data-toggle="tooltip" data-placement="top" title='{__("Shared with")} {__("Friends")}'></i>
                {elseif $post['privacy'] == "public"}
                    <i class="fa fa-globe" data-toggle="tooltip" data-placement="top" title='{__("Shared with")} {__("Public")}'></i>
                {elseif $post['privacy'] == "custom"}
                    <i class="fa fa-cog" data-toggle="tooltip" data-placement="top" title='{__("Shared with")} {__("Custom People")}'></i>
                {/if} *}
            </div>
            <!-- post time & location & privacy -->
        </div>
        <!-- post meta -->
    </div>
    <!-- post header -->

    <!-- photo -->
    {if !$_lightbox}
        <div class="mt10 clearfix">
            <div class="pg_wrapper">
                <div class="pg_1x">
                    <a href="/photos/{$photo['photo_id']}" class="js_lightbox"  data-id="{$photo['photo_id']}" data-image="{$system['system_uploads']}/{$photo['source']}" data-context="{if $photo['is_single']}album{else}post{/if}">
                        <div class="text" style=" position: absolute; width: 595px; height: 100%;"></div><img src="{$system['system_uploads']}/{$photo['source']}">
                    </a>
                </div>
            </div>
        </div>
    {/if}
    <!-- photo -->

    <!-- post actions -->
    <div class="post-actions">
                {if $user->_logged_in}
                    <!-- comment -->
                    <span class="text-clickable mr20 js-custom-share-comment" data-id="{$post['post_id']}" data-handle="post">
                        <i class="Icon Icon--medium Icon--reply"></i> <span><!--{__("Comment")}&nbsp;-->{$post['comments']}</span>
                    </span>
                    <!-- comment -->

                    <!-- share -->
                    {if $post['privacy'] == "public"}
                        <span class="text-clickable mr20 share-tweet-post" data-id="{$post['post_id']}">
                            <i class="Icon Icon--medium Icon--retweet"></i> <span><!--{__("Share")}&nbsp;-->{$post['shares']}</span>
                        </span>
                    {/if}
                    <!-- share -->
                    <!-- like -->
                    <span class="text-clickable {if $post['i_like']}text-active js_unlike-post{else}js_like-post{/if}">
                        <i class="Icon Icon--heart Icon--medium"></i><i class="Icon Icon--heartBadge Icon--medium"></i> <span class="span-counter"><!--{__("Like")}&nbsp;-->{$post['likes']}</span>
                    </span>
                    <!-- like -->

                {else}
                    <a href="/signin">{__("Please log in to like, share and comment!")}</a>
                {/if}
				<div class="sharethis-inline-share-buttons" data-url="{$system['system_url']}/posts/{$post['post_id']}"></div>
        </div>
    <!-- post actions -->
</div>

<!-- post footer -->
<div class="post-footer custom-photo-comment">
    <!-- social sharing -->
    {include file='__feeds_post.social.tpl'}
    <!-- social sharing -->

    <!-- comments -->
    {include file='__feeds_post.comments.tpl' _is_photo=(!$photo['is_single'])?true:false}
    <!-- comments -->
</div>
<!-- post footer -->