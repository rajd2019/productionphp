{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="page-title">
    {__("Reset Password")}
</div>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="modal-header">           

            <h5 class="modal-title">{__("Reset Password Confirmation Code")}</h5>

        </div>

        <form class="js_ajax-forms" data-url="core/forget_password_confirm.php">

            <div class="modal-body">

                <div class="mb20">

                    {__("Check your email")} - {__("We sent you an email with a six-digit confirmation code. Enter it below to continue to reset your password")}.

                </div>

                <div class="row">

                    <div class="col-md-6">

                        <div class="form-group">

                            <input name="reset_key" type="text" class="form-control" placeholder="######" required autofocus>

                        </div>



                        <!-- error -->

                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                        <!-- error -->

                    </div>

                    <!--<div class="col-md-6">

                        <label class="mb0">{__("We sent your code to")}</label> {literal}{{email}}{/literal}

                    </div>-->

                </div>

            </div>

            <div class="modal-footer">

                <input name="email" type="hidden" value="{$r_email}">

                <button type="submit" class="btn btn-primary">{__("Continue")}</button>

                <button type="button" class="btn btn-default" data-dismiss="modal">{__("Cancel")}</button>

            </div>

        </form>
        </div>
    </div>
</div>
<!-- page content -->

{literal}
<script type="text/javascript">
	$(document).ready(function(e){
				
	});	
</script>
{/literal}

{include file='_footer.tpl'}