<div class="modal-body plr0 ptb0">
    <div class="x-form publisher mini">

         <!-- publisher close -->
        <button type="button" class="close mr10 pull-right flip" data-dismiss="modal" aria-hidden="true">×</button>
        <!-- publisher close -->

        <!-- publisher tabs -->
        <ul class="publisher-tabs clearfix">
            <li>
                <span class="active">
                    <i class="fa fa-picture-o fa-fw"></i> {__("Create a Post")}
                </span>
            </li>
        </ul>

        <!-- publisher -->
        {include file='_publisher.tpl' _handle="me_ajax"  _privacy=true}
        <!-- publisher -->
    </div>
</div>