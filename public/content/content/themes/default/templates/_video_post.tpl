  <script src="/content/themes/default/templates/static/js/jquery-1.11.3.min.js"></script>
  <link rel="stylesheet" href="/content/themes/default/templates/static_shared/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="/content/themes/default/templates/static_shared/css/sample-chat.css">
   <link href='https://fonts.googleapis.com/css?family=Exo+2:400,900italic,900,800italic,800,700italic,700,600italic,600,500italic,500,400italic,300italic,200italic,200,100italic,100,300'
        rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,900italic,900,800italic,800,700italic,700,600italic,600,500italic,500,400italic,300italic,200italic,200,100italic,100,300'
        rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="/includes/assets/css/font-awesome/css/custom.css">
  <link href="https://unpkg.com/video.js/dist/video-js.css" rel="stylesheet">
  <script src="https://unpkg.com/video.js/dist/video.js"></script>
  <script src="https://unpkg.com/videojs-contrib-hls/dist/videojs-contrib-hls.js"></script>
  
  <style>
      .video-title, .video-des, .video-view {
          padding:5px;
      }
      .description {
          margin-top:10px;
      }
  </style>
  <div class="col-sm-12" style="margin-top:20px">
{if !$standalone}<li>{/if}
    <!-- post -->
    <div class="post {if $boosted}boosted{/if}" data-id="{$post['post_id']}">

        {if $standalone && $pinned}
            <div class="pin-icon" data-toggle="tooltip" title="{__('Pinned Post')}">
                <i class="fa fa-bookmark"></i>
            </div>
        {/if}

        {if $standalone && $boosted}
            <div class="boosted-icon" data-toggle="tooltip" title="{__('Promoted')}">
                <i class="fa fa-bullhorn"></i>
            </div>
        {/if}

       {if !$_shared && $user->_logged_in}
      <div class="pull-right flip dropdown custom-dropdown-profile"> 
        <i class="fa fa-chevron-down dropdown-toggle" data-toggle="dropdown"></i>
        <ul class="dropdown-menu post-dropdown-menu">
         {if $post['user_id']==$user->_data['user_id']}
          <li> <a href="#" class="" data-toggle="modal" data-url="chat/direct_share.php?id={$post['post_id']}">
            <div class="action no-desc"> <i class="fa fa-bookmark fa-fw"></i> <span>{__("Share via Direct Message ")}</span> </div>
            </a> </li>
            <li><span id="p{$post['user_id']}" style="display:none;"></span> <a href="javascript:void(0)"  data-clipboard-text="{$system['system_url']}/posts/{$post['post_id']}" class="copy_link">
            <div class="action no-desc"> <i class="fa fa-bolt fa-fw"></i> <span>{__("Copy Link to Post")}</span> </div>
            </a> </li>
            <li> {if $post['pinned']} <a href="#" class="js_unpin-post">
            <div class="action no-desc"> <i class="fa fa-thumb-tack fa-fw"></i> <span>{__("Unpin Post")}</span> </div>
            </a> {else} <a href="#" class="js_pin-post">
            <div class="action no-desc"> <i class="fa fa-thumb-tack fa-fw"></i> <span>{__("Pin Post")}</span> </div>
            </a> {/if} </li>
             <li> <a href="#" class="js_delete-post">
            <div class="action no-desc"> <i class="fa fa-trash-o fa-fw"></i> {__("Delete Post")} </div>
            </a> </li>
          {else}
           <li><span id="p{$post['user_id']}" style="display:none;">/includes/ajax/posts/product_editor.php?post_id={$post['post_id']}</span> <a href="javascript:void(0)"  data-clipboard-text="{$system['system_url']}/posts/{$post['post_id']}" class="copy_link">
            <div class="action no-desc"> <i class="fa fa-bolt fa-fw"></i> <span>{__("Copy Link to Post")}</span> </div>
            </a> </li>

    <li class="sendbird-chat-cls-shared">
    <input type="hidden" name="accessToken" value="{$user->_data['accessToken']}">
      <div class="init-check"></div>
    
      <div class="sample-body">
    
        <!-- left nav -->
          <a href="//sendbird.com" target="_blank"><div class="left-nav-icon"></div></a>
          <div class="left-nav-channel-select">
            <!-- <button type="button" class="left-nav-button left-nav-open" id="btn_open_chat">
              OPEN CHANNEL
              <div class="left-nav-button-guide"></div>
            </button> -->
            <input type="hidden" name="post-url" id="post-url" value="http://demo2.ongraph.com/demo/chat/direct_share.php?id={$post['post_id']}">
            <a href="#" class="shared_messaging_chat">
            <!-- <button type="button" class="left-nav-button left-nav-messaging shared_messaging_chat" > -->
              <div class="action no-desc"> <i class="fa fa-bookmark fa-fw"></i> <span>{__("Share via Direct Message ")}</span> </div>
              <div class="left-nav-button-guide"></div>
           </a>
            <!-- </button> -->
          </div>
      </div>
    </li>
        
    <li> <a href="#" class="js_hide-post">
      <div class="action"> <i class="fa fa-eye-slash fa-fw"></i> {__("Mute Post")} </div>
      <div class="action-desc">{__("See fewer posts like this")}</div>
      </a> </li>
    <li> <a href="#" class="js_block-comment" data-id="{$post['user_id']}">
      <div class="action"> <i class="fa fa-ban fa-fw"></i> {__("Block")} {$post['post_author_name']}</div>
      <div class="action-desc">{__("Block this user")}</div>
      </a> </li>
    <li> <a href="#" class="js_report" data-handle="post" data-id="{$post['post_id']}">
      <div class="action no-desc"> <i class="fa fa-flag fa-fw"></i> {__("Report post")} </div>
      </a> </li>

     {/if}
   
    </ul>
  </div>
  {/if}

        <!-- post body -->
         {if $post['post_id']!='a'}
        <div class="post-body" style="padding-bottom: 50px;" data-id="{$post['post_id']}">
            
            {include file='__video_post.body.tpl' _post=$post _shared=false}

            <!-- post stats -->
            
            <!-- post stats -->

            <!-- post actions -->
            
            <!-- post actions -->

        </div>
        {/if}
        <div class="custom-post-actions" style="float: none !important;display: block !important;">
        <div class="post-actions">
                {if $user->_logged_in}
                <!-- comment -->
                <span class="text-clickable js-custom-share-comment mr20" data-id="{$post['post_id']}" data-handle="post">
                    <i class="Icon Icon--medium Icon--reply" data-tooltip="tooltip" data-placement="top" title="" data-original-title="{__("Reply")}"></i> <span><!--{__("Comment")}-->{$post['comments']}</span>
                </span>
                <!-- comment -->

                <!-- share -->

                    <span class="text-clickable mr20 share-tweet-post" data-id="{$post['post_id']}">
                        <i class="Icon Icon--medium Icon--retweet" data-tooltip="tooltip" data-placement="top" title="" data-original-title="{__("Repost")}"></i> <span><!--{__("Share")}-->{$post['shares']}</span>
                    </span>

                <!-- share -->
                <!-- like -->
                <span class="text-clickable {if $post['i_like']}text-active js_unlike-post{else}js_like-post{/if}">
                    <i class="Icon Icon--heart Icon--medium" data-tooltip="tooltip" data-placement="top" title="" data-original-title="{__("Like")}"></i> <i class="Icon Icon--heartBadge Icon--medium"></i>
                    <span class="span-counter_{$post['post_id']}">{$post['likes']}</span>
                </span>
                <!-- like -->

                <!-- Direct Message -->

                {if $user->_data['user_id'] !=$post['author_id']}
                <span class="text-clickable direct_message_frm_post" data-id="{$post['post_id']}">
                    <i class="Icon Icon--envelope Icon--medium" data-tooltip="tooltip" data-placement="top" title="" data-original-title="{__("Direct message")}"></i>

                </span>
                {/if}
                <!-- Direct Message -->
                {else}
                    <a href="/signin">{__("Please log in to like, share and comment!")}</a>
                {/if}
				<div class="sharethis-inline-share-buttons" data-url="{$system['system_url']}/posts/{$post['post_id']}"></div>
        </div>
        
        </div>
        <div style="float: right; padding: 0 15px 15px 15px;position: relative;    width: auto; margin-top: -38px; display: inline-block;">
            <span class="total_views"><span class="update_count">{$totalviews}</span> {__(Views)}</span>
        </div>
        <!-- post body -->

        <!-- post footer -->
        <div class="post-footer {if !$standalone}x-hidden{/if}">
            <!-- social sharing -->
            {include file='__feeds_post.social.tpl'}
            <!-- social sharing -->

            <!-- comments -->
            
            <!-- comments -->
        </div>
        <!-- post footer -->

    </div>
    <!-- post -->
{if !$standalone}</li>{/if}

<!--div class="other-title-bar video-page" style="margin-top:30px;">
    <h4>How to broadcast in Guo Media?</h4>
    <p>We welcome everyone to express their viewpoints freely in our platform. Currently, we allow users to do broadcast via our mobile apps while audience can watch in both website and mobile apps. Please download our apps in App Store to become one of our broadcasters today! In the future, we will allow users to broadcast via website as well. Thank you so much for your support, everything is just beginning ️️️! 
</p>
<div class="google_link"> <a href="#"><img src="/content/themes/default/images/google-play.png" alt="google-play"></a> <a href="#"><img src="/content/themes/default/images/apple-store.png" alt="apple-store"></a> </div>
</div-->
</div>


  <script src="/content/themes/default/templates/static_shared/lib/SendBird.min.js"></script>
  <script src="/content/themes/default/templates/static_shared/js/util.js"></script>
  <!-- <script src="/content/themes/default/templates/static_shared/js/chat.js"></script> -->