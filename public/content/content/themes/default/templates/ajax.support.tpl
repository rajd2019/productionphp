<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 class="modal-title">{__("Support")}</h5>
</div>
    
	<div id="response-div">
		
		<input type="hidden" name="user_id" id="user_id" value="{$user_id}">
		
		<span class="DMTypeaheadHeader">{__("To:")}</span>
		<div class="post_custom">
			
			<div class="form-group">
				<input type="text" name="support-to" id="support-top" class="form-control" value="support@guo.media.com" disabled required autofocus>
			</div>
		</div>
		
		<span class="DMTypeaheadHeader">{__("From:")}</span>
		<div class="post_custom">
			
			<div class="form-group">
				<input type="text" name="support-from" id="support-from" class="form-control" disabled value="{$username}" required autofocus>
			</div>
		</div>
		
		<span class="DMTypeaheadHeader">{__("Your Email:")}</span>
		<div class="post_custom">
			
			<div class="form-group">
				<input type="text" name="user-email" id="user-email" class="form-control" value="{$user_email}" required autofocus>
			</div>
		</div>
		
		<span class="DMTypeaheadHeader">{__("Subject:")}</span>
		<div class="post_custom">
			
			<div class="form-group">
				<input type="text" name="support-subject" id="support-subject" class="form-control" required autofocus>
			</div>
		</div>
		
		<span class="DMTypeaheadHeader">{__("Message:")}</span>
		<div class="post_custom">
			
			<div class="form-group">
				<textarea class="expand replyComment  replyComm_chk post_comment form-control" rows="5" cols="50" value="" placeholder='{__("Add a message")}....' name="support-message" id="support-message" required autofocus></textarea>
			</div>
		</div>
		<div class="modal-footer">        
			<a class="btn btn-primary submit_support" data-id="" data-handle="">{__("Submit")}</a>
		</div>
	</div>
<script type="text/javascript">
   $(function() {


text_comment_reply();
}); 

$(document).on('keyup', '.replyComm_chk', function(event) {
   
        
        text_comment_reply()
    });


    function text_comment_reply()
    {
     

        var reply_comm=$('textarea.replyComm_chk').val();
        var comm_leng=reply_comm.length;
            text_counter= comm_leng;       
        
            if(text_counter==0)
            {
                $("a.reply_com_btn").attr("disabled",true);
                return true;
            }
            else
            {
                $("a.reply_com_btn").attr("disabled",false);
                return false;
            }
    }
</script>