<style type="text/css">
.body{
	position:fixed;	
}
.modal{
	position:fixed;	
}
</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 class="modal-title">{__("Reply Comment")}</h5>
</div>
    <div class="modal-body post-hover-content">
        {foreach $posts as $_user}
        <div class="post " data-id="{$_user['post_id']}"> 
            <div class="post-section">
                <div class="post-body"> 
                    <div class="post-header"> 
                        <div class="post-avatar"><img src="{$_user['user_picture']}" /></div> 
                        <div class="post-meta">
                        <div>
                        <span>
                            <span class="first-name-custom">{$_user["name"]}</span> 
                            <span class="second-name-custom">@{$_user["user_name"]}</span>
                        </span>
                         <div class="post-time"> <span class="js_moment" data-time="{$_user['time']}">{$_user["time"]}</span>
                         </div>
                         <span class="post-title"> </span> 
                        <div class="post-replace">
                            
                            <div class="post-text js_readmore" dir="auto" data-id="{$_user['post_id']}">{$_user['text']|unescape:'html'}</div>
                            <div class="post-text-plain hidden">{$_user['text_plain']}</div>
                        </div>  
                        </div>
                        </div>
                         
                    </div>
                </div>
            </div>
        </div>
        {/foreach}
    </div>

    <div class="post_custom">
        <div class="form-group">
            <input type="hidden" name="do" value="share">
            <input type="hidden" name="id" value="{$id}" class="postId">
            
            <div id="replaceTextarea"><textarea class="expand replyComment  replyComm_chk post_comment form-control" rows="1" cols="50" value="{$album['title']}" placeholder='{__("Add a comment")}....' name="comment" id="post_comment_box_id"></textarea></div>
			
			<span class="publisher-tools-attach js_publisher-photos newalbumClass picker2" style="overflow:visible">
            <i class="fa fa-smile-o fa-fw"></i>
			</span>
        </div>
    </div>
    <div class="modal-footer">        
        <a class="btn btn-primary js_comment-reply reply_com_btn" data-id="{$id}" data-handle="{$_handle}">{__("Reply")}</a>
    </div>
<script type="text/javascript">

jQuery.fn.putCursorAtEnd = function() {

          return this.each(function() {

            $(this).focus()

            // If this function exists...
            if (this.setSelectionRange) {
              // ... then use it (Doesn't work in IE)

              // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
              var len = $(this).val().length * 2;

              this.setSelectionRange(len, len);

            } else {
            // ... otherwise replace the contents with itself
            // (Doesn't work in Google Chrome)

              $(this).val($(this).val());

            }

            // Scroll to the bottom, in case we're in a tall textarea
            // (Necessary for Firefox and Google Chrome)
            this.scrollTop = 999999;

          });

};




$(document).on('keyup', '.replyComm_chk', function(event) {
    text_comment_reply();
});


function text_comment_reply(){
    var reply_comm=$('textarea.replyComm_chk').val();
        
    var comm_leng=reply_comm.length;
    text_counter= comm_leng;       
        
    if(text_counter==0){
        $("a.reply_com_btn").attr("disabled",true);
        return true;
    }else{
        $("a.reply_com_btn").attr("disabled",false);
        return false;
    }
}
	

$(function() {
    text_comment_reply();

	$('.picker2').lsxEmojiPicker({
		closeOnSelect: true,
		twemoji: true,
		onSelect: function(emoji){
				
			var cursorPosition = $(".replyComm_chk")[0].selectionStart;
			var FirstPart = $(".replyComm_chk").val().substring(0, cursorPosition);
			var NewText = emoji.value;
			var SecondPart = $(".replyComm_chk").val().substring(cursorPosition + 1, $(".replyComm_chk").val().length);
				
				
			var txt_placeholder = $(".replyComm_chk").attr('placeholder');
				
			$("#replaceTextarea").html('<textarea class="expand replyComment  replyComm_chk post_comment form-control" rows="1" cols="50" value="" placeholder="'+txt_placeholder+'" name="comment" id="post_comment_box_id">'+FirstPart+NewText+SecondPart+'</textarea>');

            $("#post_comment_box_id").putCursorAtEnd();
            text_comment_reply();
		}
	});		
});
</script>