{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="search-result-heading">
    <div class="container">
        {$query}
    </div>
</div>

<div class="search-result-custom">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="view-top {if $follow == ''} active {/if}">
                <a href="#top" data-toggle="tab">
                    <strong class="pr5">{__("Top")}</strong>
                </a>
            </li>
            <li class="view-posts">
                <a href="#posts" data-toggle="tab">
                    <strong class="pr5">{__("Posts")}</strong>
                </a>
            </li>
            <li class="view-people {if {$follow} == 'follow'} active {/if}">
                <a href="#users" data-toggle="tab">
                    <strong class="pr5">{__("People")}</strong>
                </a>
            </li>
            <li class="view-trends">
                <a href="#trends" data-toggle="tab">
                    <strong class="pr5">{__("Trends")}</strong>
                </a>
            </li>
            <li class="view-media">
                <a href="#media" data-toggle="tab">
                    <strong class="pr5">{__("Media")}</strong>
                </a>
            </li>
            <li class="view-mentions">
                <a href="#mentions" data-toggle="tab">
                    <strong class="pr5">{__("Mentions")}</strong>
                </a>
            </li>
            <li class="view-articles">
                <a href="#articles" data-toggle="tab">
                    <strong class="pr5">{__("Articles")}</strong>
                </a>
            </li>
            <li class="view-polls">
                <a href="#polls" data-toggle="tab">
                    <strong class="pr5">{__("Polls")}</strong>
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="container mt20 offcanvas">
    <div class="row">
    
        <!-- side panel -->
        <div class="col-sm-4 col-md-3 offcanvas-sidebar">
            
            <div class="panel panel-default panel-widget">
        
            
                        <div class="panel-heading">
                            <div class="mt5">
                                <strong>{__("Search")}</strong><span class="show-hide"><a href="javascript:void(0)">{__("Show")}</a></span> <span class="hide-show" style="display:none"><a href="javascript:void(0)">{__("hide")}</a></span>
                            </div>
                        </div>
                        <div class="panel-body search-panel-body" style="display:none">
                            <form class="form-horizontal js_search-form">
                                <div class="form-group">
                                    <div class="col-sm-8 mb5">
                                        <input type="text" name="query" class="form-control" value="{$query}" placeholder='{__("Search for people, pages and #hashtags")}' required>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="submit" name="submit" class="btn btn-primary">{__("Search")}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                   
                </div>
                
               <div class="panel panel-default panel-widget">
                    <div class="panel-heading">
                        <div class="pull-right flip">
                            <small><a href="/people">{__("View All")}</a></small>
                        </div>
                        <strong class="wtf-module">{__("Who to follow")}</strong>
                    </div>
                    <div class="panel-body">
                        <ul>
                        {if count($user->_data['new_people']) > 0}
                            {foreach $user->_data['new_people'] as $_user}
                            {include file='__feeds_user.tpl' _connection="add" _small=true}
                            {/foreach}
                         {/if}
                         {if count($user->_data['new_people']) <= 0}
                         <p class="text-center text-muted">
                            {__("No people available")}
                         </p>
                         {/if}
                        </ul>
                    </div>
                </div>    


            
            <div class="site_bar_new">
                <!-- Disable weekily trends -->
                <!--<div class="sidebar_trends">
                <h3>{__("Weekly Trends for you")}</h3>
                
                {if $user->_data.popular_tag != 'No Record Found'}
                {foreach $user->_data.popular_tag as $custom_field}
                <div class="fb_post_rep">
                <h4><a href="/trend_post.php?tags={$custom_field['url_tags']}">{$custom_field['tags']}</a></h4>
                <p>{$custom_field['count']} {__("Posts")}</p>
                </div>
                {/foreach}
                {else}
                <h4>{__("No Trends for you.")}</h4>
                {/if}
                
                </div>
            </div>-->
        </div>
        <!-- side panel -->
        </div>
        <div class="col-sm-8 col-md-9 offcanvas-mainbar">
            <div class="row">
                <!-- left panel -->
                <div class="col-sm-12">
                    <!-- search form -->
                    
                    <!-- search form -->

                    {if $query}
                    <!-- search results -->
                     <div class="panel-body tab-content">
                        <div class="tab-pane {if $follow == ''} active {/if}" id="top">
                            
                            {if count($results['limit_user']) > 0}
                            <div class="custom-view"><p class="mt10 mb10"><strong>{__("People")}</strong></p><span class="people-view-all"><a href="#users" data-toggle="tab">{__("View All")}</a></span></div>
                                
                                <div class="row">
                                    {foreach $results['limit_user'] as $k=>$_user}
                                    <div class="col-sm-4">
                                     <div class="site_bar_new custom-site-bar-new">
                                         <div class="profile-bg-thumb"> 
                                        {if $_user['user_cover']}
                                            <img src="{$system['system_uploads']}/{$_user['user_cover']}">
                                        {/if}
                                        </div>
                                        <div class="side_profile">
                                        <div class="pro_thumb"> <a href="/{$_user['user_name']}{if $_search}?ref=qs{/if}"><img src="{$_user['user_picture']}" alt="{$_user['user_firstname']} {$_user['user_lastname']}"></a></div>
                                        
                                        <div class="admin_detail">
                                        <span> 
                                        
										{if $results['custom_login_user_id'] != $_user['user_id'] }
										
                                        {if $_user['following'] == 0}
                                            <a class="js_follow follow_btn 1122{$_user['following']}" data-uid="{$_user['user_id']}" href="#">{__("Follow")}</a>
                                        {else}  
                                            <a class="btn btn-default js_unfollow" data-uid="{$_user['user_id']}" href="#"><i class="fa fa-check"></i>{__("Following")}</a>
                                        {/if}
										
										{/if}
                                        
                                        </span>
                                        <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                                            <a href="/{$_user['user_name']}{if $_search}?ref=qs{/if}"><span>{$_user['user_firstname']}{$_user['user_lastname']} </span> <span class="user-at-btn">@{$_user['user_name']}</span></a>
                                        </span>
                                        
                                        
                                        </div>
                                        <p>{$_user['user_biography']}</p>
                                        </div>
                                    </div>
                                    </div>
                                        {if ($k+1)%3 == 0}
                                            <div class="clearfix"></div>
                                        {/if}
                                 {/foreach}
                                </div>

                                {/if}

                               {if count($trend_results['trends_result']) > 0}
                                 <div class="custom-view"><p class="mt10 mb10"><strong>{__("Trends")}</strong></p><span class="trends-view-all"><a href="#trends" data-toggle="tab">{__("View All")}</a></span></div>
                                <ul>
                                    {foreach $trend_results['trends_result'] as $post}
                                    {include file='__trend_feeds_post.tpl' _tpl="list"}
                                    {/foreach}
                                </ul>
                                {/if}
                                
                               
                                
                                <!--<div class="custom-view"><p class="mt10 mb10"><strong>{__("Posts")}</strong></p><span><a href="#posts" data-toggle="tab">View all</a></span></div>-->
                             
                                
                                {if count($trend_results['limit_media']) > 0 && count($trend_results['limit_photos']) > 0}
                                <div class="custom-view"><p class="mt10 mb10"><strong>{__("Media")}</strong></p><span class="media-view-all"><a href="#media" data-toggle="tab">{__("View All")}</a></span></div>
                                <ul>
                                    {foreach $results['limit_media'] as $post}
                                    {include file='__feeds_post.tpl'}
                                    {/foreach}
                                </ul>
                                
                                <ul>
                                    {foreach $results['limit_photos'] as $post}
                                    {include file='__feeds_post.tpl'}
                                    {/foreach}
                                </ul>
                                {/if}
                                
                                {if count($results['limit_mention']) > 0}
                                <div class="custom-view"><p class="mt10 mb10"><strong>{__("Mentions")}</strong></p><span class="mentions-view-all"><a href="#mentions" data-toggle="tab">{__("View All")}</a></span></div>
                                
                                <ul>
                                    {foreach $results['limit_mention'] as $post}
                                    {include file='__feeds_post.tpl'}
                                    {/foreach}
                                </ul>
                                {/if}
                                
                                {if count($results['limit_articles']) > 0}
                                <div class="custom-view"><p class="mt10 mb10"><strong>{__("Articles")}</strong></p><span class="articles-view-all"><a href="#articles" data-toggle="tab">{__("View All")}</a></span></div>
                                
                                <ul>
                                    {foreach $results['limit_articles'] as $post}
                                    {include file='__feeds_post.tpl'}
                                    {/foreach}
                                </ul>
                                {/if}
                                
                                {if count($results['limit_polls']) > 0}
                                <div class="custom-view"><p class="mt10 mb10"><strong>{__("Polls")}</strong></p><span class="polls-view-all"><a href="#polls" data-toggle="tab">{__("View All")}</a></span></div>
                                
                                <ul>
                                    {foreach $results['limit_polls'] as $post}
                                    {include file='__feeds_post.tpl'}
                                    {/foreach}
                                </ul>
                                {/if}
                              {if count($results['posts']) > 0}
                                <ul id="toptab">
                                    {foreach $results['posts'] as $post}
                                    {include file='__feeds_post.tpl'  _get=newsfeed}
                                    {/foreach}
          
                                </ul>
  <div class="alert alert-post see-more mb10 custom_js_see-more {if $user->_logged_in}custom_js_see-more-infinite{/if}" data-get="newsfeed1" data-filter="{if $_filter}{$_filter}{else}all{/if}" {if $_id}data-id="{$_id}"{/if} data-query="{$query}">
			<span>{__("More Stories")}</span>
			<div class="loader loader_small x-hidden"></div>
		</div>
                                
                                {else}
                                {* <div class="text-center x-muted mtb10">
                                    <p class="mt10 mb10"><strong>{__("No results to show")}</strong></p>
                                </div> *}
                                {/if}
                                
                        </div>


                        <div class="tab-pane" id="posts">
                            
                 
                               {if count($results['posts']) > 0}
                                <ul id="post-tab">
                                    {foreach $results['posts'] as $post}
                                   {include file='__feeds_post.tpl' _get=newsfeed}
                                    {/foreach}
                                </ul>
                         <!-- see-more -->
                          <div class="alert alert-post see-more mb10 post_js_see-more {if $user->_logged_in}post_js_see-more-infinite{/if}" data-get="newsfeed1" data-filter="{if $_filter}{$_filter}{else}all{/if}" {if $_id}data-id="{$_id}"{/if} data-query="{$query}">
                                <span>{__("More Stories")}</span>
                                <div class="loader loader_small x-hidden"></div>
                            </div>
		<!-- see-more -->
                                {/if} 
                           
                                
                                
                        </div>

                        <div class="tab-pane {if $follow != ''} active {/if}" id="users">
                            
                                
                                <div class="row">
                                {if count($results['users']) > 0}
                                {foreach $results['users'] as $_user}
                                    <div class="col-sm-4">
                                     <div class="site_bar_new custom-site-bar-new">
                                        <div class="profile-bg-thumb"> 
                                        {if $_user['user_cover']}
                                            <img src="{$system['system_uploads']}/{$_user['user_cover']}">
                                        {/if}
                                        </div>
                                        <div class="side_profile">
                                        <div class="pro_thumb"> <a href="/{$_user['user_name']}{if $_search}?ref=qs{/if}"><img src="{$_user['user_picture']}" alt="{$_user['user_firstname']} {$_user['user_lastname']}"></a></div>
                                        
                                        <div class="admin_detail">
                                        <span> 
										{if $results['custom_login_user_id'] != $_user['user_id'] }
                                        
                                        {if $_user['following'] == 0}
                                            <a class="js_follow follow_btn 22" data-uid="{$_user['user_id']}" href="#">{__("Follow")}</a>
                                        {else}  
                                            <a class="btn btn-default js_unfollow" data-uid="{$_user['user_id']}" href="#"><i class="fa fa-check"></i>{__("Following")}</a>
                                        {/if}
										 {/if}
                                        
                                        </span>
                                        <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                                            <a href="/{$_user['user_name']}{if $_search}?ref=qs{/if}"><span>{$_user['user_firstname']}{$_user['user_lastname']} </span> <span class="user-at-btn">@{$_user['user_name']}</span></a>
                                        </span>
                                        
                                        
                                        </div>
                                        <p>{$_user['user_biography']}</p>
                                        </div>
                                    </div>
                                    </div>
                                    {/foreach}
                                </div>
                                
                                {else}
                               {* <div class="text-center x-muted mtb10">
                                    <p class="mt10 mb10"><strong>{__("No results to show")}</strong></p>
                                </div>*}
                                {/if}
                        </div>
                            <div class="tab-pane" id="trends">
                                {if count($results['trends']) > 0}
                                <ul>
                                    {foreach $results['trends'] as $post}
                                    {include file='__trend_feeds_post.tpl'}
                                    {/foreach}
                                </ul>
                                {else}
                                {* <div class="text-center x-muted mtb10">
                                    <p class="mt10 mb10"><strong>{__("No results to show")}</strong></p>
                                </div>*}
                                {/if}
                            </div>
                            <div class="tab-pane" id="media">
                               {if count($results['media']) > 0}
                                <ul>
                                    {foreach $results['media'] as $post}
                                        {if $post['post_id']!='a'}
                                            {include file='__feeds_post.tpl'}
                                        {/if}
                                    {/foreach}
                                </ul>
                               {else}
                                {* <div class="text-center x-muted mtb10">
                                    <p class="mt10 mb10"><strong>{__("No results to show")}</strong></p>
                                </div>*}
                                {/if}
                                
                                <ul>
                                    {foreach $results['photos'] as $post}
                                    {include file='__feeds_post.tpl'}
                                    {/foreach}
                                </ul>
                                

                            </div>
                            <div class="tab-pane" id="mentions">
                                {if count($results['mention']) > 0}
                                <ul>
                                    {foreach $results['mention'] as $post}
                                    {include file='__feeds_post.tpl'}
                                    {/foreach}
                                </ul>
                                {else}
                                {* <div class="text-center x-muted mtb10">
                                    <p class="mt10 mb10"><strong>{__("No results to show")}</strong></p>
                                </div>*}
                                {/if}
                            </div>
                            <div class="tab-pane" id="articles">
                                {if count($results['article']) > 0}
                                <ul>
                                    {foreach $results['article'] as $post}
                                    {include file='__trend_feeds_post.tpl'}
                                    {/foreach}
                                </ul>
                                {else}
                                {* <div class="text-center x-muted mtb10">
                                    <p class="mt10 mb10"><strong>{__("No results to show")}</strong></p>
                                </div>*}
                                {/if}
                            </div>
                            <div class="tab-pane" id="polls">
                                {if count($results['poll']) > 0}
                                <ul>
                                    {foreach $results['poll'] as $post}
                                    {include file='__trend_feeds_post.tpl'}
                                    {/foreach}
                                </ul>
                                {else}
                                   {if count($results['poll'])==0}
                                        {* <div class="text-center x-muted mtb10">
                                    <p class="mt10 mb10"><strong>{__("No results to show")}</strong></p>
                                </div>*}
                                   {/if}
                                {/if}
                            </div>
                    <!-- search results -->
                    {/if}
                </div>
                <!-- left panel -->

                <!-- right panel -->
                <div class="col-sm-4">
                    {include file='_ads.tpl'}
                    {include file='_widget.tpl'}
                </div>
                <!-- right panel -->
            </div>
        </div>
        

        
    </div>

<!-- page content -->

{include file='_footer.tpl'}