<html>
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.cloudflare.com/cdn-cgi/scripts/9014afdb/cloudflare-static/rocket.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/content/themes/default/css/do-broadcast.css">
  </head>
  <body>
    <div id="do_broad">
      <h4 class="modal-title">{$title}</h4>
      <div class="modal-body">
        <div class="error" class="do-broadcast-error">
          {$error}
        </div>
        <form action="//{$http_host}/do_broadcast.php" method="post" class="do-broadcast">
          <label>
            {$bname} :
          </label>
          <input type="text" value="" name="broadcastName">
          <label>
            {$bdetails}:
          </label>
          <input type="text" value="" name="broadcastDetails">
          <input class="do-broadcast-submit" type="submit" value="{$start}" name="submit">
        </form>
      </div>
    </div>
  </body>
</html>
