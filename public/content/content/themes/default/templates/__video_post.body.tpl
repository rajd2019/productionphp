<!-- post header -->



<div class="post-header ">

  <!-- post picture -->

  <div class="post-avatar"> <a class="post-avatar-picture" href="{$_post['post_author_url']}" style="background-image:url({$_post['post_author_picture']});"> </a> </div>

  <!-- post picture -->

  <!-- post meta -->

  <div class="post-meta">

    <!-- post menu & author name & type meta & feeling -->

    <div>

      <!-- post menu -->

      

      <!-- post menu -->

      <!-- post author name -->

      <span class="js_user-popover" data-type="{$_post['user_type']}" data-uid="{$_post['user_id']}"> <a href="{$_post['post_author_url']}"><span class="first-name-custom">{if $_post['post_author_name']}{$_post['post_author_name']}{/if}</span> <span class="second-name-custom">@{$_post['post_user_name']}</span></a> </span>

      {if $_post['post_author_verified']}

      

      {if $_post['user_type'] == "user"} <i data-toggle="tooltip" data-placement="top" title='{__("Verified User")}' class="fa fa-check-circle fa-fw verified-badge"></i> {else} <i data-toggle="tooltip" data-placement="top" title='{__("Verified Page")}' class="fa fa-check-circle fa-fw verified-badge"></i> {/if}

      

      {/if}

      <!-- post time & location & privacy -->

      <div class="post-time"> <a href="/posts/{$_post['post_id']}" class="js_moment" data-time="{$_post['time']}"> - {$_post['time']}</a> {if $_post['location']}

        

        路 <i class="fa fa-map-marker"></i> <span>{$_post['location']}</span> {/if} </div>

      

      

      {if $_post['user_subscribed']} <i data-toggle="tooltip" data-placement="top" title='{__("Pro User")}' class="fa fa-bolt fa-fw pro-badge"></i> {/if}

      <!-- post author name -->

      <!-- post type meta -->

      <!-- post feeling -->

      {if $_post['feeling_action']} <span class="post-title"> {__("is")} <i class="twa twa-lg twa-{$_post['feeling_icon']}"></i> {__($_post["feeling_action"])} {__($_post["feeling_value"])} </span> {/if}

      <!-- post feeling -->

    </div>

    <!-- post menu & author name & type meta & feeling -->

    <!-- post time & location & privacy -->

  </div>

  <!-- post meta -->

</div>

<!-- post header -->

<!-- product -->

{if $_post['post_type'] == "product" && $_post['product']}



    {if $_post['product']['available']}

<div class="mb5 text-lg"> <strong>{$_post['product']['name']}</strong> </div>

{else}

<div class="mb5 text-lg x-muted"> <strong>({__("SOLD")}) {$_post['product']['name']}</strong> </div>

{/if}

<div class="mb5 text-bg"> <i class="fa fa-money fa-fw"></i> <span class="green">{$system['system_currency_symbol']}{$_post['product']['price']}</span> ({$system['system_currency']}) </div>

{if $_post['product']['location']}

<div class="mb10 text-muted"> <i class="fa fa-map-marker fa-fw"></i> {$_post['product']['location']} </div>

{/if}



{/if}

<!-- product -->

<!-- post text -->

{if !$_shared}



    {include file='__feeds_post.text.tpl'}



{else}



<div class="post-text js_readmore" dir="auto">{$_post['text']}</div>

{/if}

<!-- post text -->

{if $_post['post_type'] == "album" || ($_post['post_type'] == "product" && $_post['photos_num'] > 0) || $_post['post_type'] == "photos" || $_post['post_type'] == "profile_picture" || $_post['post_type'] == "profile_cover" || $_post['post_type'] == "page_picture" || $_post['post_type'] == "page_cover"}

<div class="mt10 clearfix">

  <div class="pg_wrapper"> {if $_post['photos_num'] == 1}

    <div class="pg_1x"> <a href="/photos/{$_post['photos'][0]['photo_id']}" class="js_lightbox" data-id="{$_post['photos'][0]['photo_id']}" data-image="{$system['system_uploads']}/{$_post['photos'][0]['source']}" data-context="{if $_post['post_type'] == 'product'}post{else}album{/if}"> <img src="{$system['system_uploads']}/{$_post['photos'][0]['source']}"> </a> </div>

    {elseif $_post['photos_num'] == 2}

    

    {foreach $_post['photos'] as $photo}

    <div class="pg_2x"> <a href="/photos/{$photo['photo_id']}" class="js_lightbox" data-id="{$photo['photo_id']}" data-image="{$system['system_uploads']}/{$photo['source']}" data-context="post" style="background-image:url('{$system['system_uploads']}/{$photo['source']}');"></a> </div>

    {/foreach}

    

    {elseif $_post['photos_num'] == 3}

    <div class="pg_3x">

      <div class="pg_2o3">

        <div class="pg_2o3_in"> <a href="/photos/{$_post['photos'][0]['photo_id']}" class="js_lightbox" data-id="{$_post['photos'][0]['photo_id']}" data-image="{$system['system_uploads']}/{$_post['photos'][0]['source']}" data-context="post" style="background-image:url('{$system['system_uploads']}/{$_post['photos'][0]['source']}');"></a> </div>

      </div>

      <div class="pg_1o3">

        <div class="pg_1o3_in"> <a href="/photos/{$_post['photos'][1]['photo_id']}" class="js_lightbox" data-id="{$_post['photos'][1]['photo_id']}" data-image="{$system['system_uploads']}/{$_post['photos'][1]['source']}" data-context="post" style="background-image:url('{$system['system_uploads']}/{$_post['photos'][1]['source']}');"></a> </div>

        <div class="pg_1o3_in"> <a href="/photos/{$_post['photos'][2]['photo_id']}" class="js_lightbox" data-id="{$_post['photos'][2]['photo_id']}" data-image="{$system['system_uploads']}/{$_post['photos'][2]['source']}" data-context="post" style="background-image:url('{$system['system_uploads']}/{$_post['photos'][2]['source']}');"></a> </div>

      </div>

    </div>

    {else}

    <div class="pg_4x">

      <div class="pg_2o3">

        <div class="pg_2o3_in"> <a href="/photos/{$_post['photos'][0]['photo_id']}" class="js_lightbox" data-id="{$_post['photos'][0]['photo_id']}" data-image="{$system['system_uploads']}/{$_post['photos'][0]['source']}" data-context="post" style="background-image:url('{$system['system_uploads']}/{$_post['photos'][0]['source']}');"></a> </div>

      </div>

      <div class="pg_1o3">

        <div class="pg_1o3_in"> <a href="/photos/{$_post['photos'][1]['photo_id']}" class="js_lightbox" data-id="{$_post['photos'][1]['photo_id']}" data-image="{$system['system_uploads']}/{$_post['photos'][1]['source']}" data-context="post" style="background-image:url('{$system['system_uploads']}/{$_post['photos'][1]['source']}');"></a> </div>

        <div class="pg_1o3_in"> <a href="/photos/{$_post['photos'][2]['photo_id']}" class="js_lightbox" data-id="{$_post['photos'][2]['photo_id']}" data-image="{$system['system_uploads']}/{$_post['photos'][2]['source']}" data-context="post" style="background-image:url('{$system['system_uploads']}/{$_post['photos'][2]['source']}');"></a> </div>

        <div class="pg_1o3_in"> <a href="/photos/{$_post['photos'][3]['photo_id']}" class="js_lightbox" data-id="{$_post['photos'][3]['photo_id']}" data-image="{$system['system_uploads']}/{$_post['photos'][3]['source']}" data-context="post" style="background-image:url('{$system['system_uploads']}/{$_post['photos'][3]['source']}');"> {if $_post['photos_num'] > 4} <span class="more">+{$_post['photos_num']-4}</span> {/if} </a> </div>

      </div>

    </div>

    {/if} </div>

</div>

{elseif $_post['post_type'] == "media" && $_post['media']}

<div class="mt10"> 
  {if $_post['media']['source_type'] == "photo"}

  <div class="post-media">

    <div class="post-media-image">

      <div style="background-image:url('{$_post['media']['source_url']}');"></div>

    </div>

    <div class="post-media-meta">

      <div class="source"><a target="_blank" href="{$_post['media']['source_url']}">{$_post['media']['source_provider']}</a></div>

    </div>

  </div>

  {else}

  

  {if $_post['media']['source_provider'] == "YouTube"}

  

  {if $system['smart_yt_player']}

  

  {$_post['media']['vidoe_id'] = get_youtube_id($_post['media']['source_html'])}

  <div class="youtube-player youtube-video" data-id="{$_post['media']['vidoe_id']}"> <img src="https://i.ytimg.com/vi/{$_post['media']['vidoe_id']}/hqdefault.jpg">

    <div class="play"></div>

  </div>

  <div class="post-media-meta"> <a class="title mb5" href="{$_post['media']['source_url']}" target="_blank">{$_post['media']['source_title']}</a>

    <div class="text mb5"></div>

    <div class="source">{$_post['media']['source_text']}</div>

  </div>

  {else}

  <div class="post-media">

    <div class="embed-responsive embed-responsive-16by9"> {html_entity_decode($_post['media']['source_html'], ENT_QUOTES)} </div>

  </div>

  <div class="post-media-meta"> <a class="title mb5" href="{$_post['media']['source_url']}" target="_blank">{$_post['media']['source_title']}</a>

    <div class="text mb5">{$_post['media']['source_text']}</div>

    <div class="source">{$_post['media']['source_provider']}</div>

  </div>

  {/if}

  

  

  

  {elseif $_post['media']['source_provider'] == "Vimeo" || $_post['media']['source_provider'] == "SoundCloud" || $_post['media']['source_provider'] == "Vine"}

  <div class="post-media">

    <div class="embed-responsive embed-responsive-16by9"> {html_entity_decode($_post['media']['source_html'], ENT_QUOTES)} </div>

  </div>

  <div class="post-media-meta"> <a class="title mb5" href="{$_post['media']['source_url']}" target="_blank">{$_post['media']['source_title']}</a>

    <div class="text mb5">{$_post['media']['source_text']}</div>

    <div class="source">{$_post['media']['source_provider']}</div>

  </div>

  {else}

  <div class="embed-ifram-wrapper"> {html_entity_decode($_post['media']['source_html'], ENT_QUOTES)} </div>

  {/if}

  

  {/if} </div>

{elseif $_post['post_type'] == "link" && $_post['link']}

<div class="mt10">

  <div class="post-media"> {if $_post['link']['source_thumbnail']}

    <div class="post-media-image">

      <div style="background-image:url('{$_post['link']['source_thumbnail']}');"></div>

    </div>

    {/if}

    <div class="post-media-meta"> <a class="title mb5" href="{$_post['link']['source_url']}" target="_blank">{$_post['link']['source_title']}</a>

      <div class="text mb5">{$_post['link']['source_text']}</div>

      <div class="source">{$_post['link']['source_host']|upper}</div>

    </div>

  </div>

</div>

{elseif $_post['post_type'] == "poll" && $_post['poll']}

<div class="poll-options mt10" data-poll-votes="{$_post['poll']['votes']}"> {foreach $_post['poll']['options'] as $option}

  <div class="mb5">

    <div class="poll-option js_poll-vote" data-id="{$option['option_id']}" data-option-votes="{$option['votes']}">

      <div class="percentage-bg" {if $_post['poll']['votes'] > 0} style="width: {($option['votes']/$_post['poll']['votes'])*100}%"{/if}></div>

      <div class="radio radio-primary radio-inline">

        <input type="radio" name="poll_{$_post['poll']['poll_id']}" id="option_{$option['option_id']}" {if $option['checked']}checked{/if}>

        <label for="option_{$option['option_id']}">{$option['text']}</label>

      </div>

    </div>

    <div class="poll-voters">

      <div class="more" data-toggle="modal" data-url="posts/who_votes.php?option_id={$option['option_id']}"> {$option['votes']} </div>

    </div>

  </div>

  {/foreach} </div>

{elseif $_post['post_type'] == "article" && $_post['article']}

<div class="mt10">

  <div class="post-media"> {if $_post['article']['cover']} <a class="post-media-image" href="/blogs/{$_post['post_id']}/{$_post['article']['title_url']}">

    <div style="padding-top: 50%; background-image:url('{$system['system_uploads']}/{$_post['article']['cover']}');"></div>

    </a> {/if}

    <div class="post-media-meta"> <a class="title mb5" href="/blogs/{$_post['post_id']}/{$_post['article']['title_url']}">{$_post['article']['title']}</a>

      <div class="text mb5">{$_post['article']['text_snippet']|truncate:400}</div>

    </div>

  </div>

</div>

{elseif $_post['post_type'] == "video" && $_post['video']}

<div class="video_custom_margin">

  
    {if $_post['is_broadcast'] == 1}
      {$is_mobile = check_is_mobile()}
      {if ($is_mobile == 'true') }
        <video width="320" height="240" class="js_mediaelementplayer custom-video" controls="controls" preload="auto">
      {else}
        <video width="640" height="480" class="js_mediaelementplayer custom-video" controls="controls" preload="auto">
      {/if}
        <source src="{BROADCAST_URL}/{$_post['video']['source']}" type="video/mp4">
        <source src="{BROADCAST_URL}/{$_post['video']['source']}" type="video/webm">
       </video>

    {/if}
    
    {if $_post['is_broadcast'] != 1}
    <video width="100%" class="js_mediaelementplayer custom-video" controls="controls" preload="auto">
    <source src="{$system['system_uploads']}/{$_post['video']['source']}" type="video/mp4">

    <source src="{$system['system_uploads']}/{$_post['video']['source']}" type="video/webm">
    </video>
    {/if}
 
</div>

{elseif $_post['post_type'] == "live"}

<div class="video_custom_margin">
  {$is_mobile = check_is_mobile()}
  {if ($is_mobile == 'true') }
    <video id="video_{$_post['post_id']}" class="video-js vjs-default-skin" controls preload="auto" width="320" height="240">
        <source src="{$_post['broadcast_url']}" type="application/x-mpegURL">
    </video>
  {/if}
  {if ($is_mobile == 'false') }
    <video id="video_{$_post['post_id']}" class="video-js vjs-default-skin" controls preload="auto" width="640" height="480">
        <source src="{$_post['broadcast_url']}" type="application/x-mpegURL">
    </video>
  {/if}
    <script>
        var player = videojs("video_{$_post['post_id']}");
        player.on('error', function(event) {
            if(player.error().code == 4) {
                jQuery("#sb_chat").css('display','none');
                jQuery(".custom-dropdown-profile").css('display','none');
                jQuery(".text-clickable").css('display','none');
            }
        });
        player.play();
    </script>
</div>
<input type="hidden" value="{$_post['post_id']}" id="post_id"/>
<input type="hidden" value="{$_post['channel_url']}" id="channel_id"/>
<input type="hidden" value="{$user->_data['user_firstname']}" id="user_firstname"/>
<input type="hidden" value="{$user->_data['uuid']}" id="uuid"/>
{elseif $_post['post_type'] == "audio" && $_post['audio']}

<div>

  <audio width="100%" class="js_mediaelementplayer">

    <source src="{$system['system_uploads']}/{$_post['audio']['source']}">

    {__("Your browser does not support HTML5 audio")} </audio>

</div>

{elseif $_post['post_type'] == "file" && $_post['file']}

<div class="post-downloader">

  <div class="icon"> <i class="fa fa-file-text-o fa-2x"></i> </div>

  <div class="info"> <strong>{__("File Type")}</strong>: {get_extension({$_post['file']['source']})}

    <div class="mt10"> <a class="btn btn-primary btn-sm" href="{$system['system_uploads']}/{$_post['file']['source']}">{__("Download")}</a> </div>

  </div>

</div>

{elseif !$_shared && $_post['post_type'] == "shared" && $_post['origin']}



    {if $_snippet} <span class="text-link js_show-attachments">{__("Show Attachments")}</span> {/if}

<div class="mt10 {if $_snippet}x-hidden{/if}">

  <div class="post-media">

    <div class="post-media-meta"> {include file='__feeds_post.body.tpl' _post=$_post['origin'] _shared=true} </div>

  </div>

</div>

{elseif $_post['post_type'] == "map"}

<div class="post-map"> <img src="https://maps.googleapis.com/maps/api/staticmap?center={$_post['location']}&amp;zoom=13&amp;size=600x250&amp;maptype=roadmap&amp;markers=color:red%7C{$_post['location']}&amp;key={$system['geolocation_key']}" width="100%"> </div>

{/if}

<!-- product -->

{if $_post['post_type'] == "product" && $_post['product'] && $_post['author_id'] != $user->_data['user_id'] }

<div class="mt10 clearfix">

  <button type="button" class="btn btn-primary btn-sm pull-right flip js_chat-start" data-uid="{$_post['author_id']}" data-name="{if $_post['post_author_name']}{$_post['post_author_name']}{/if}"><i class="fa fa-comments-o"></i> {__("Contact Seller")}</button>

</div>

{/if}

<!-- product -->