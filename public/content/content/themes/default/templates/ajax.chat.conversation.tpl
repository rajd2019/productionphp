<div class=" panel-messages" data-cid={$conversation['conversation_id']}>
    <div class="panel-heading clearfix custom_msg_heading">
	<!-- <button type="button" data-dismiss="modal" aria-hidden="true" class="close mr10 pull-right flip">  x</button> -->
		  <div class="mt5">
            {if !$conversation['multiple_recipients']}
               <!--  {$conversation['name_html']} -->
			     <span title="{$conversation['name_list']}">{$conversation['user_name']} {$conversation['name']}</span>
            {else}
                <span title="{$conversation['name_list']}">{$conversation['user_name']} {$conversation['name']}</span>
            {/if}
        </div>
        <div class="pull-right flip">
         <!--   <a class="btn btn-primary js_chat-start" href="/messages/new">
                <i class="fa fa-comment-o"></i>
                {__("New Message")}
            </a>-->
			
			
			<!--<a class="btn btn-primary  " data-toggle="modal" data-url="chat/user_search.php">  <i class="fa fa-comment-o"></i>
					New Message
				</a> -->
			
            <a href="#" class="custom-button-delete js_delete-conversation">
                <span class="Icon Icon--delete Icon--medium"></span>
            </a>
        </div>
			<a class="custom_back_btn"  data-toggle="modal" data-url="chat/open_conversation.php">
					<span class="Icon Icon--caretLeft u-linkComplex-target Icon--medium"></span>
				</a>
      
    </div>
	<div class="panel panel-default">
    <div class="  panel-body  ">
        <div class="chat-conversations js_scroller" data-slimScroll-height="367px" data-slimScroll-start="bottom">
            {include file='ajax.chat.conversation.messages.tpl'}
        </div>
        <div class="chat-attachments attachments clearfix x-hidden">
            <ul>
                <li class="loading">
                    <div class="loader loader_small"></div>
                </li>
            </ul>
        </div>
        <div class="chat-form-container custom-form-container">
            <div class="x-form chat-form">
                <div class="chat-form-message">
                    <textarea class="js_autosize js_post-message" placeholder='{__("Write a message")}'></textarea>
                </div>
                <div class="x-form-tools">
                    <div class="x-form-tools-attach">
                        <i class="fa fa-camera js_x-uploader" data-handle="chat"></i>
                    </div>
                    <div class="x-form-tools-emoji js_emoji-menu-toggle">
                        <i class="fa fa-smile-o fa-lg"></i>
                    </div>
                    {include file='_emoji-menu.tpl'}
                </div>
            </div>
        </div>
    </div>
	</div>
</div>