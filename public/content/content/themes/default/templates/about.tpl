{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="page-title">
    {__("About GUO.MEDIA")}
</div>

<div class="container offcanvas">
    <div class="row">

	    <!-- side panel -->
	    <div class="col-xs-12 visible-xs-block offcanvas-sidebar mt20">
	        {include file='_sidebar.tpl'}
	    </div>
	    <!-- side panel -->

	    <div class="col-xs-12 offcanvas-mainbar">
			<div class="row">
		        <div class="col-xs-10 col-xs-offset-1 text-readable ptb10">
		            <p>
                	{__("GUO.MEDIA is now launching our beta version. We need your support.We are looking forward to your feedback in order to improve our services.We strive to provide a free social networking and livestreaming platform where everyone can express their views and opinions freely.Please join us in promoting judicial and media independence, democracy and religious freedom in China! Everything is just beginning!")}
            		</p>
                    {*<h3 class="text-info">{__("Social networking")}</h3>
                    <p>
                	{__("GUO.MEDIA is an all-in-one social networking platform available in both website and mobile app versions, where users can build their own user profiles, create their own posts and follow each other. This is a platform for people to share their views freely on everything.")}
            		</p>
                    <h3 class="text-info">{__("Livestream broadcast")}</h3>
                    <p>
                    {__("Users can broadcast a live video via our app. Your followers will receive notifications when live streaming has started, to let everyone also able to watch your broadcast.")}
            		</p>
					 <h3 class="text-info">{__("More to come!")}</h3>
                    <p>
                    {__("This is just a beta version of GUO.MEDIA . We value and appreciate your suggestions in order to improve our services. More exciting features will be launched in due course to facilitate freedom of expression and democracy development in China. Everything is just beginning!")}
            		</p>*}
		        </div>
		    </div>
	    </div>
	    
	</div>
</div>
<!-- page content -->

{include file='_footer.tpl'}