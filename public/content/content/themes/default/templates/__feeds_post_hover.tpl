{if !$standalone}<li>{/if}
    <!-- post -->
    <div class="post {if $boosted}boosted{/if}" data-id="{$post['post_id']}">

        {if $standalone && $pinned}
            <div class="pin-icon" data-toggle="tooltip" title="{__('Pinned Post')}">
                <i class="fa fa-bookmark"></i>
            </div>
        {/if}

        {if $standalone && $boosted}
            <div class="boosted-icon" data-toggle="tooltip" title="{__('Promoted')}">
                <i class="fa fa-bullhorn"></i>
            </div>
        {/if}

        <!-- post body -->
        <div class="">
            
            {include file='__feeds_post.body.tpl' _post=$post _shared=false onhover='detail'}

            <!-- post stats -->
            <div class="post-stats">
				{if $post['post_type'] == "video" && $post['video']}
                 {$user->video_view_manager({$post['post_id']})}
                    <!-- views -->
                        <span class="js_post-views-num">{$user->views_format({$post['views']})}</span>
                    <!-- views -->
                {/if}
                 <!-- views 
                <span class="text-clickable" data-toggle="modal" data-url="posts/who_likes.php?post_id={$post['post_id']}">
                    <i class="fa fa-thumbs-o-up"></i> 
                    <span class="js_post-likes-num">
                        {$post['views']} views
                    </span>
                </span>
                views -->
                <!-- likes -->
                <!--
                <span class="text-clickable" data-toggle="modal" data-url="posts/who_likes.php?post_id={$post['post_id']}">
                    <i class="fa fa-thumbs-o-up"></i> 
                    <span class="js_post-likes-num">
                        {$post['likes']}
                    </span>
                </span>
                -->
                <!-- likes -->
				<!--
                <span class="pull-right flip">
                -->
                    <!-- comments -->
                    <!--
                    <span class="text-clickable js_comments-toggle">
                        <i class="fa fa-comments"></i> {$post['comments']} {__("Comments")}
                    </span>
                    -->
                    <!-- comments -->

                    <!-- shares -->
                    <!--
                    <span class="text-clickable ml10 {if $post['shares'] == 0}x-hidden{/if}" data-toggle="modal" data-url="posts/who_shares.php?post_id={$post['post_id']}">
                        <i class="fa fa-share"></i> {$post['shares']} {__("Shares")}
                    </span>
                    -->
                    <!-- shares -->
                <!--</span>-->
            </div>
            <!-- post stats -->

            <!-- post actions -->
            <div class="post-actions">
                {if $user->_logged_in}
                    <!-- comment -->
                    <span class="text-clickable mr20" id="detail-hover-comment">
                        <i class="Icon Icon--medium Icon--reply"></i> <span id="span-comments-counter-{$post['post_id']}"><!--{__("Comment")}&nbsp;-->{$post['comments']}</span>
                    </span>
                    <!-- comment -->

                    <!-- share -->
                    {if $post['privacy'] == "public"}
                        <span class="text-clickable mr20 share-tweet-post" data-id="{$post['post_id']}">
                            <i class="Icon Icon--medium Icon--retweet"></i> <span id="span-share-counter-{$post['post_id']}"><!--{__("Share")}&nbsp;-->{$post['shares']}</span>
                        </span>
                    {/if}
                    <!-- share -->
                    <!-- like -->
                    <span class="text-clickable {if $post['i_like']}text-active js_unlike-post{else}js_like-post{/if}">
                        <i class="Icon Icon--heart Icon--medium"></i><i class="Icon Icon--heartBadge Icon--medium"></i> 
                        <span class="span-counter_{$post['post_id']}">{$post['likes']}</span>
                    </span>
                    <!-- like -->

                {else}
                    <a href="/signin">{__("Please log in to like, share and comment!")}</a>
                {/if}
				<div class="sharethis-inline-share-buttons" data-url="{$system['system_url']}/posts/{$post['post_id']}"></div>
            </div>
            <!-- post actions -->

        </div>
        <!-- post body -->

        <!-- post footer -->
        <div class="{if !$standalone}x-hidden{/if}">
            <!-- social sharing -->
            {include file='__feeds_post.social.tpl'}
            <!-- social sharing -->

            <!-- comments -->
            {include file='__feeds_post.comments.tpl'}
            <!-- comments -->
        </div>
        <!-- post footer -->

    </div>
    <!-- post -->
{if !$standalone}</li>{/if}