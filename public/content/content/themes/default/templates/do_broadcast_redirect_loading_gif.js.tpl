<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.cloudflare.com/cdn-cgi/scripts/9014afdb/cloudflare-static/rocket.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/content/themes/default/css/do-broadcast.css">
</head>
<body>
<div id="do_broad">
    <div class='loader loading-gif'>
        <img src='https://gifimage.net/wp-content/uploads/2017/09/blue-loading-gif-transparent-13.gif' />
        {$redirecting}
    </div>
    <script type="text/javascript">
      setTimeout(function(){
        window.parent.location.href = "{$redirect_url}";
      }, 5000);
    </script>
</div>
</body>
</html>

