<style>
	.comment-outer .pull-right{
		float:none !important;
	}
	.comment-edit .x-form.comment-form div.edit-textarea, .post_custom #replaceTextarea, div#replaceTextarea_cmt_frm {
		padding: 10px 31px 10px 10px;
		border: 1px solid #ccc;
		-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.1);
		-moz-box-shadow: inset 0 1px 1px rgba(0,0,0,0.1);
		box-shadow: inset 0 1px 1px rgba(0,0,0,0.1);
		border-radius: 4px;
		background:#fff;
	}
	.comment-edit .x-form.comment-form div.edit-textarea textarea, #replaceTextarea textarea#post_comment_box_id, .comment-form textarea#wwww {
		height: auto !important;
		overflow-y: auto !important;
		border: 0;
		padding: 0;
		box-shadow: none;
	}
	 .comment-outer .x-form-tools-emoji.js_emoji-menu-toggle.picker3 {
		bottom: 13px;
		top: auto !important;
	}
	.comment-edit .x-form.comment-form .x-form-tools-emoji.js_emoji-menu-toggle{
		bottom: 4px;
	}
	 .comment-outer .x-form-tools {
		top: 5px;
	}
	.comment-edit .x-form.comment-form .x-form-tools-attach{
		top: 15px;
	}
	.comment-outer .lsx-emojipicker-container {
		top: auto !important;
		right: 0px;
		bottom: 32px !important;
	}
	.modal-dialog .post_custom .form-group {
		margin: 0;
		position: relative;
	}
	.post_custom .publisher-tools-attach.js_publisher-photos.newalbumClass.picker2 {
		position: absolute;
		right: 6px;
		bottom: 4px;
		display: inline-block;
		width: auto !important;
	}
	.form-group .fa.fa-smile-o.fa-fw {
		width: auto;
		font-size: 21px;
	}
	form[data-url="/data/report.php"] .form-group {
		padding: 15px;
		padding-bottom: 0;
	}
	.custom-post-comment {
		margin: 0 -15px !important;
	}
	#modal p {
		padding: 15px;
		padding-bottom: 0;
		margin-bottom: 0;
	}
	.slimScrollBar {
		background: #ccc !important;
	}
	.comment-text{
		line-height:20px;
	}
	.comment-btn.dropdown .tooltip.top {
		left: auto !important;
		right: 0;
	}
	.comment-btn.dropdown .tooltip-arrow {
		right: 2px !important;
		left: auto !important;
	}
	@media screen and (min-width:768px){
		div#modal {
			margin: 0 0 0 -17px;
		}
		/*.modal-body.post-hover-content {
			max-height: calc(100vh - 61px);
			overflow-y: auto !important;
		}
		.modal-body.post-hover-content .slimScrollDiv {
			height: calc(100vh - 61px);
		}
		.modal-body.post-hover-content .js_scroller{
			height: calc(100vh - 61px);
		}*/
	}
	@media screen and (max-width:767px){
		.parent-comment-section .comment {
			padding-bottom: 0;
		}
		.comment .post-actions {
			margin: 0;
		}
	}
</style>
<div class="custom-post-comment">

<div class="post-comments">



    {if $_is_photo}



         <!-- post a comment -->

        {if $user->_logged_in}

            <div class="modal-body">

					<div class="comment-outer">

						{include file='__feeds_comment.form.tpl' _handle='photo' _id=$photo['photo_id']}

						<div class="pull-right flip mt5 mr10 custom-comment-button" style="display: none;">

							<button type="button" id="detail-hover-submitComment"  class="btn btn-primary" disabled="disabled">{__("Post")}</button>

						</div>
					</div>

            </div>

        {/if}

        <!-- post a comment -->



        <!-- comments -->

        <div class="parent-comment-section">

        <ul class="js_comments">

            {if $photo['comments'] > 0}

                {foreach $photo['photo_comments'] as $comment}

                {include file='__feeds_comment.tpl' _comment=$comment}

                {/foreach}

            {/if}

        </ul>

        </div>

        <!-- comments -->
 <!-- previous comments -->

        {if $photo['comments'] >= $system['min_results']}

            <div class="pb10 text-center js_see-more-comments" id="more-comments-id" data-get="photo_comments" data-id="{$photo['photo_id']}" data-remove="true" style="display:block">

                <span class="text-link">

                    <i class="fa fa-comment-o"></i>

                    {__("View previous comments")}

                </span>

                <div class="loader loader_small x-hidden"></div>

            </div>

        {/if}

        <!-- previous comments -->




    {else}



        <!-- post a comment -->

        {if $user->_logged_in}

            <div class="modal-body">
				<div class="comment-outer">

					{include file='__feeds_comment.form.tpl' _handle='post' _id=$post['post_id']}

					<div class="pull-right flip mt5 mr10 custom-comment-button" style="display: none;">

					   <!--<button type="button" id="detail-hover-submitComment"  class="btn btn-primary" disabled="disabled">{__("Post")}</button>-->

					   <button type="button" id="detail-hover-submitComment"  class="btn btn-primary profile-comment">{__("Post")}</button>

					</div>
				</div>
            </div>




        {/if}



        <!-- post a comment -->






        <!-- comments -->

        <div class="parent-comment-section">
        <div class="loading" id="post_comments_loading_id" style="display: none">
                <div class="loader loader_medium"></div>
        </div>

        <ul class="js_comments">

            {if $post['comments'] > 0}

                {foreach $post['post_comments'] as $comment}

                {include file='__feeds_comment.tpl' _comment=$comment}

                {/foreach}

            {/if}

        </ul>

        </div>

        <!-- comments -->



         <!-- previous comments -->


        {if count($post['post_comments']) >= $system['min_results']}

            <div class="pb10 text-center js_see-more-comments" id="more-comments-id" data-get="post_comments" data-id="{$post['post_id']}" data-remove="true" style="display:block">

                <span class="text-link">

                    <i class="fa fa-comment-o"></i>

                    {__("View previous comments")}

                </span>

                <div class="loader loader_small x-hidden"></div>

            </div>

        {/if}

        <!-- previous comments -->




    {/if}

</div>

</div>
<style type="text/css">
    #post_comments_loading_id .loader.loader_medium {

    display: inherit;

}
</style>
