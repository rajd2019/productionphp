{if $type == "user"}

    <!-- user popover -->

    <div class="user-popover-content user-hover-detail {if $user->_data['user_id'] == $profile['user_id']}author-profile{/if}">

        <div class="user-card">

            <div class="user-card-cover" {if $profile['user_cover']}style="background-image:url('{$system['system_uploads']}/{$profile['user_cover']}');"{/if}>

            </div>

            <div class="user-card-avatar">

                <img src="{$profile['user_picture']}" alt="{$profile['user_firstname']} {$profile['user_lastname']}">

            </div>

            



        </div>

        <div class="user-card-meta">

            <!-- mutual friends -->

            

            <!-- mutual friends -->

            

            {if $user->_data['user_id'] != $profile['user_id']}

                <div class="btn-group">

                    {if $profile['i_follow']}

                        <button type="button" class="btn btn-default js_unfollow" data-uid="{$profile['user_id']}">

                            <i class="fa fa-check"></i>

                            {__("Following")}

                        </button>

                    {else}

                        <button type="button" class="btn btn-default js_follow" data-uid="{$profile['user_id']}">

                            <i class="fa fa-rss"></i>

                            {__("Follow")}

                        </button>

                    {/if}

                </div>

            {/if}

            <div class="user-card-info">

                    <a class="name" href="/{$profile['user_name']}">

                    <span class="first-name-custom">{$profile['user_firstname']} {$profile['user_lastname']}</span>

                    <span class="second-name-custom">@{$profile['user_name']}</span>

                    </a>

                    {if $profile['user_verified']}

                        <i data-toggle="tooltip" data-placement="top" title='{__("Verified User")}' class="fa fa-check-circle fa-fw verified-badge"></i>

                    {/if}

            </div>

        </div>



        <div class="following side_profile">

            <ul>

            <li><a href="/{$profile['user_name']}">{__("Posts")}<span>{if $profile['posts_count']}{$profile['posts_count']}{else}0{/if}</span></a></li>

            <li><a href="/followings.php?username={$profile['user_name']}">{__("Followings")}<span>

            {if $profile['followings_count']}{$profile['followings_count']}{else}0{/if}</span></a></li>

            <li><a href="/followers.php?username={$profile['user_name']}">{__("Followers")}<span>{if $user->_data.followers_count}{$profile['followers_count']}{else}0{/if}</span></a></li>

            </ul>

        </div>

        

        <!--<div class="footer">

            {if $user->_data['user_id'] != $profile['user_id']}

                {if $profile['we_friends']}

                    <div class="btn btn-default btn-delete js_friend-remove" data-uid="{$profile['user_id']}">

                        <i class="fa fa-check fa-fw"></i> {__("Friends")}

                    </div>

                {elseif $profile['he_request']}

                    <div class="btn btn-primary js_friend-accept" data-uid="{$profile['user_id']}">{__("Confirm")}</div>

                    <div class="btn btn-default js_friend-decline" data-uid="{$profile['user_id']}">{__("Delete Request")}</div>

                {elseif $profile['i_request']}

                    <div class="btn btn-default btn-sm js_friend-cancel" data-uid="{$profile['user_id']}">

                        <i class="fa fa-user-plus"></i> {__("Friend Request Sent")}

                    </div>

                {else}

                    <div class="btn btn-success btn-sm js_friend-add" data-uid="{$profile['user_id']}">

                        <i class="fa fa-user-plus"></i> {__("Add Friend")}

                    </div>

                {/if}



                

            {else}

                <a href="/settings/profile" class="btn btn-default">

                    <i class="fa fa-pencil"></i> {__("Update Info")}

                </a>

            {/if}

        </div>

    </div>-->

    <!-- user popover -->

{else}

    <!-- page popover -->

    <div class="user-popover-content">

        <div class="user-card">

            <div class="user-card-cover" {if $profile['page_cover']}style="background-image:url('{$system['system_uploads']}/{$profile['page_cover']}');"{/if}></div>
            
           

            <div class="user-card-avatar">

                <img class="img-responsive" src="{$profile['page_picture']}" alt="{$profile['page_title']}">

            </div>

            <div class="user-card-info">

                <a class="name" href="/pages/{$profile['page_name']}">{$profile['page_title']}</a>

                {if $profile['page_verified']}

                    <i data-toggle="tooltip" data-placement="top" title='{__("Verified User")}' class="fa fa-check-circle fa-fw verified-badge"></i>

                {/if}

                <div class="info">{$profile['page_likes']} {__("Likes")}</div>

            </div>

        </div>

        <div class="footer">

            {if $profile['i_like']}

                <button type="button" class="btn btn-default js_unlike-page" data-id="{$profile['page_id']}">

                    <i class="fa fa-thumbs-o-up"></i>

                    {__("Unlike")}

                </button>

            {else}

                <button type="button" class="btn btn-primary js_like-page" data-id="{$profile['page_id']}">

                    <i class="fa fa-thumbs-o-up"></i>

                    {__("Like")}

                </button>

            {/if}

        </div>

    </div>

    <!-- page popover -->

{/if}