{include file='_head.tpl'}
{include file='_header.tpl'}
<style>
/** Default Css **/
body {
    margin:0; 
    background:#e6ecf0;
    }
ul {
    padding:0; 
    list-style:none; 
    margin:0;
    }

/** Custom Css **/

.ico_active {
    display:none;
    }
.sidebar_lft {
    width:100%;
    }
.sidebar_lft li { 
    margin-bottom:16px;
    }
.sidebar_lft li a { 
    border:1px solid #ded7d7; 
    color:#000000; 
    font-size:18px; 
    font-weight:normal; 
    background:#fff; padding:5px 20px; 
    display:block;
    border-radius:2px; 
    height:54px; 
    line-height:2.3; 
    position:relative;
    }
.sidebar_lft li a:hover, .sidebar_lft li a:focus {
    text-decoration:none;
    } 
.icon_default {
    display:inline-block;
    }
.icon_active {
    display:none;
    }
.sidebar_lft li i { 
    margin-right:10px;
    }
.sidebar_lft .youtube_btn.active a { 
    background:#ff0000; 
    color:#fff; 
    border:1px solid #ff0000;
    }
.sidebar_lft .twitter_btn.active a { 
    background:#21c7ff; 
    color:#fff; 
    border:1px solid #21c7ff;
    }
.sidebar_lft li.active a:after {
	content:'';
	position:absolute;
	top:48%;
	right:-10px;
	-ms-transform:translateY(-50%);	
	-moz-transform:translateY(-50%);	
	-webkit-transform:translateY(-50%);	
	transform:translateY(-50%);	
	border-top: 10px solid transparent;
	border-bottom: 10px solid transparent;  
	border-left: 10px solid #21c7ff;	
}
.sidebar_lft .youtube_btn.active a:after {
	border-left: 10px solid #ff0000;	
}
ul.sidebar_lft li.active .icon_default {
    display:none;
    }
ul.sidebar_lft li.active .icon_active { 
    display:inline-block;
    }

.sidebar_rht {
    text-align:center; 
    background:#ffffff;
    }
.sidebar_rht h5 {
    text-align:center; 
    margin:0; 
    color:#2a2a2a; 
    font-size:20px; 
    border-bottom:1px solid #e6ecf0; 
    padding:20px 0;
    }
.sidebar_rht h5 i{
    margin-right:7px;
    }
.btn_group { 
    padding:30px 20px;
    }
.upload_btn, .share_btn { 
    background:#5a656b; 
    display:block; 
    border-radius:21px;
     color:#fff; 
     font-size:16px; 
     font-weight:400; 
     width:170px; 
     position:relative; 
     min-height:42px; 
     vertical-align:middle; 
     margin:0 auto 13px; 
     line-height:2.6; 
     cursor:pointer; 
     border:0; 
     outline:0;
     }
.upload_btn i, .share_btn i { 
    display:inline-block; 
    margin-right:7px;
    }
.upload_btn input[type="file"]{
    cursor:pointer; 
    width:100%; 
    position:absolute;
    top:0; 
    left:0;
    height:100%; 
    opacity:0;
    }
.upload_btn:hover, .share_btn:hover {
    background:#333;
    }
.share_btn {
    margin-bottom:0;
    }
</style>
<!-- page content -->
<div class="container mt20 offcanvas">
	
     
    <div class="row">
        <aside class="col-md-3 col-xs-12">
					<ul class="sidebar_lft">
						<li class="youtube_btn">
							<a href="#">
								<i><img src="/content/themes/default/images/youtube_icon.png" alt="Youtube" class="icon_default"/>
									<img src="/content/themes/default/images/youtube_white_icon.png" alt="Youtube" class="icon_active"/></i>
								YouTube Posts
							</a>
						</li>
						<li class="twitter_btn active">
							<a href="#">
								<i><img src="/content/themes/default/images/Twitter_icon.png" alt="Twitter" class="icon_default"/>
									<img src="/content/themes/default/images/Twitter_white_icon.png" alt="Twitter" class="icon_active"/></i>
								Twitter Posts
							</a>
						</li>
					</ul>
				</aside>

				<div class="col-md-6 col-xs-12">
                     {include file="/var/app/current/twitter_profile/346/index.html"} 
                    {*<h1>Content coming soon..</h1>*}
                </div>

				<aside class="col-md-3 col-xs-12 pull-right">
					<div class="sidebar_rht">
						<h5><i><img src="/content/themes/default/images/Twitter_icon.png" alt="Twitter" /></i> My Twitter Upload</h5>
						<div class="btn_group">
							<div class="upload_btn">
								<input type="file">
								<i><img src="/content/themes/default/images/upload_icon.png" alt="upload" /></i> Upload
							</div>
							{* <button class="share_btn">							
								<i><img src="/content/themes/default/images/share-icon.png" alt="Share" /></i> Share
							</button> *}
						</div>
					</div>
				</aside>
    </div>
</div>
<!-- page content -->
{include file='_footer.tpl'}