<li>
    <div class="comment {if $_is_reply}reply{/if}" data-id="{$_comment['comment_id']}" id="comment_{$_comment['comment_id']}">
        <!-- comment avatar -->
        <div class="comment-avatar">
            <a class="comment-avatar-picture" href="{$_comment['author_url']}" style="background-image:url({$_comment['author_picture']});">
            </a>
        </div>
        <!-- comment avatar -->

        <!-- comment body -->
        <div class="comment-data">
            <!-- comment menu -->
            {if $user->_logged_in}
                {if !$_comment['edit_comment'] && !$_comment['delete_comment'] }
                  

                    <div class="comment-btn dropdown pull-right flip">
                        <i class="fa fa-chevron-down dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" data-placement="top" title='{__("Report or Block User")}'></i>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#" class="js_report" data-handle="comment" data-id="{$_comment['comment_id']}">{__("Report")}</a>
                            </li>
                            <li>
                                <a href="#" class="js_block-comment" data-id="{$_comment['user_id']}">{__("Block User")}</a>
                            </li>
                        </ul>
                    </div>
                     
                 {elseif !$_comment['edit_comment'] && $_comment['delete_comment']}
               


                    <div class="comment-btn dropdown pull-right flip">
                        <i class="fa fa-chevron-down dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" data-placement="top" title='{__("Report or Block User")}'></i>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#" class="js_delete-comment" >{__("Delete Comment")}</a>
                            </li>
                            <li>
                                <a href="#" class="js_block-comment" data-id="{$_comment['user_id']}">{__("Block User")}</a>
                            </li>
                        </ul>
                    </div>
                {else}
                    <div class="comment-btn dropdown pull-right flip">
                        <i class="fa fa-times dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" data-placement="top" title='{__("Edit or Delete")}'></i>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#" class="js_edit-comment">{__("Edit Comment")}</a>
                            </li>
                            <li>
                                <a href="#" class="js_delete-comment">{__("Delete Comment")}</a>
                            </li>
                        </ul>
                    </div>
                     
                {/if}
            {/if}
            <!-- comment menu -->

            <!-- comment author & text  -->
            <div class="mb5 js_notifier-flasher">
                <!-- author -->
                <span class="text-semibold js_user-popover" data-type="{$_comment['user_type']}" data-uid="{$_comment['user_id']}">
                    <a href="{$_comment['author_url']}"  >
                        <span class="first-name-custom">{$_comment['author_name']}</span>
                        <span class="second-name-custom">@{$_comment['user_name']}</span>
                    </a>
                </span>
                {if $_comment['author_verified']}
                <i data-toggle="tooltip" data-placement="top" title='{__("Verified User")}' class="fa fa-check-circle fa-fw verified-badge"></i>
                {/if}
                <!-- author -->

                <!-- text -->
                {include file='__feeds_comment.text.tpl'}
                <!-- text -->
            </div>
            <!-- comment author & text  -->

            <!-- comment actions & time  -->
            <div>
                <!-- actions -->          
                <!-- actions -->
                <!-- time  -->
                <small class="text-x-muted js_moment" data-time="{$_comment['time']}">{$_comment['time']}</small>
                <!-- time  -->
            </div>

            <div class="custom-post-actions">
                <div class="post-actions">
                        {if $user->_logged_in}
                            <!-- comment -->
                            <span class="text-clickable mr20 js-custom-share-comment" data-id="{$_comment['comment_id']}" data-target="#modal-comment-reply" data-handle="comment">
                                <i class="Icon Icon--medium Icon--reply"></i> <span id="span-share-counter-inside-{$_comment['comment_id']}">{$_comment['replies']}</span>
                            </span>
                            <!-- comment -->
                            <!-- like -->
                            <span class="text-clickable {if $_comment['i_like']}text-active js_unlike-comment{else}js_like-comment{/if}">
                                <i class="Icon Icon--heart Icon--medium"></i><i class="Icon Icon--heartBadge Icon--medium"></i> 
                                <span class="span-counter_{$_comment['comment_id']}">{$_comment['likes']}</span>
                            </span>
                            <!-- like -->

                            {if !$_is_reply}
                                {if !$standalone && $_comment['replies'] > 0}
                                <!-- <div class="ptb10 plr10 js_replies-toggle"> -->
                                    <span class="text-link ptb10 plr10 js_replies-toggle">
                                        <i class="fa fa-comments-o"></i>
                                        {$_comment['replies']} {__("Replies")}
                                    </span>
                                <!-- </div> -->
                                {/if}
                            {/if}

                        {else}
                            <a href="/signin">{__("Please log in to like, share and comment!")}</a>
                        {/if}
						<div class="sharethis-inline-share-buttons" data-url="{$system['system_url']}/posts/{$post['post_id']}"></div>
                </div>
            </div>
            <!-- comment actions & time  -->

            <!-- comment replies  -->
            {if !$_is_reply}
                
                <div style="clear: both"></div>
                <div class="loading rply_loader_cls" id="post_comments_loading_id_{$_comment['comment_id']}" style="display: none">
                    <div class="loader loader_medium"></div>
                </div>
                <div class="comment-replies {if !$standalone}x-hidden{/if}">
                    <!-- previous replies -->
                    {if $_comment['replies'] >= $system['min_results']}
                        <div class="pb10 text-center js_see-more" data-get="comment_replies" data-id="{$_comment['comment_id']}" data-remove="true">
                            <span class="text-link">
                                <i class="fa fa-comment-o"></i>
                                {__("View previous replies")}
                            </span>
                            <div class="loader loader_small x-hidden"></div>
                        </div>
                    {/if}
                    <!-- previous replies -->

                    <!-- replies -->
                    <ul class="js_replies">
                        {if $_comment['replies'] > 0}
                            {foreach $_comment['comment_replies'] as $reply}
                            {include file='__feeds_comment.tpl' _comment=$reply _is_reply=true}
                            {/foreach}
                        {/if}
                    </ul>
                    <!-- replies -->

                    <!-- post a reply -->
                    {if $user->_logged_in}
                        <div class="x-hidden js_reply-form">
                            <div class="x-form comment-form">
                                <textarea dir="auto" class="js_autosize js_mention js_post-reply" rows="1" placeholder='{__("Write a reply")}'></textarea>
                                <div class="x-form-tools">
                                    <div class="x-form-tools-post js_post-reply">
                                        <i class="fa fa-paper-plane-o"></i>
                                    </div>
                                    <div class="x-form-tools-attach">
                                        <i class="fa fa-camera js_x-uploader" data-handle="comment"></i>
                                    </div>
                                    <div class="x-form-tools-emoji js_emoji-menu-toggle">
                                        <i class="fa fa-smile-o fa-lg"></i>
                                    </div>
                                    {include file='_emoji-menu.tpl'}
                                </div>
                            </div>
                            <div class="comment-attachments attachments clearfix x-hidden">
                                <ul>
                                    <li class="loading">
                                        <div class="loader loader_small"></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    {/if}
                    <!-- post a reply -->
                </div>
            {/if}
            <!-- comment replies  -->
        </div>
        <!-- comment body -->
    </div>
</li>
<style type="text/css">
    .rply_loader_cls .loader.loader_medium {

    display: inherit;

}
</style>