<div id="conversationSettingsModal" class="modal fade" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="DMActivity DMConversationSettings js-ariaDocument u-chromeOverflowFix DMActivity--open" role="document">
                    <div class="DMActivity-header">
                        <div class="DMActivity-navigation">
                            <button type="button" class="DMActivity-back u-textUserColorHover transparent-no-border-button" to-convo="true">
                                <span class="Icon Icon--caretLeft u-linkComplex-target Icon--medium"></span>
                                <span class="u-hiddenVisually">{__("Back to inbox")}</span>
                            </button>
                        </div>
                        <h2 class="DMActivity-title js-ariaTitle" id="dm_dialog-header">{__("Group info")}</h2>
                        <div class="DMActivity-toolbar">
                            <div class="DMConversationSettings-dropdown u-posRelative u-textLeft u-textUserColorHover">
                                <div class="DMPopover DMPopover--left">
                                    <button class="DMPopover-button transparent-no-border-button" aria-haspopup="true">
                                        <span class="u-hiddenVisually">{__("More")}</span>
                                        <span class="Icon Icon--dots Icon--medium"></span>
                                    </button>
                                    <div class="DMPopover-content Caret Caret--top Caret--stroked ">
                                        <ul class="DMPopoverMenu js-focus-on-open u-dropdownUserColor" tabindex="-1" role="menu">
                                            <li class="js-actionEditGroupName">
                                                <button type="button" class="DMPopoverMenu-button">{__("Edit group name")}</button>
                                            </li>
                                            <li class="js-actionChangeAvatar">
                                                <button type="button" class="DMPopoverMenu-button">{__("Upload new photo")}</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="DMActivity-close js-close u-textUserColorHover transparent-no-border-button">
                                <span class="Icon Icon--close Icon--medium"></span>
                                <span class="u-hiddenVisually">{__("Close")}</span>
                            </button>
                        </div>
                    </div>
                    <div class="DMActivity-container">
                        <div class="DMActivity-notice">
                            <div class="DMNotice DMNotice--error DMErrorBar" style="display: none;">
                                <div class="DMNotice-message">
                                    <div class="DMErrorBar-text"></div>
                                </div>
                                <div class="DMNotice-actions u-emptyHide"></div>
                                <button type="button" class="DMNotice-dismiss">
                                    <span class="Icon Icon--close"></span>
                                    <span class="u-hiddenVisually">{__("Dismiss")}</span>
                                </button>
                            </div>
                            <div class="DMNotice DMNotice--toast " style="display: none;">
                                <div class="DMNotice-message"></div>
                                <div class="DMNotice-actions u-emptyHide"></div>
                                <button type="button" class="DMNotice-dismiss">
                                    <span class="Icon Icon--close"></span>
                                    <span class="u-hiddenVisually">{__("Dismiss")}</span>
                                </button>
                            </div>
                            <div class="DMNotice DMDeleteConversation" style="display: none;">
                                <div class="DMNotice-message">
                                    <span class="DMDeleteConversation-message">
                                        {__("This conversation history will be deleted from your inbox.")}
                                    </span>
                                </div>
                                <div class="DMNotice-actions u-emptyHide">
                                    <button type="button" class="DMDeleteConversation-cancel EdgeButton EdgeButton--tertiary">{__("Cancel")}</button>
                                    <button type="button" class="DMDeleteConversation-confirm EdgeButton EdgeButton--danger js-initial-focus">{__("Leave")}</button>
                                </div>
                            </div>
                        </div>
                        <div class="DMActivity-body js-ariaBody DMConversationSettings-container js-initial-focus flex-module u-scrollY">
                            <div class="DMConversationSettings-avatar">
                                <div class="DMUpdateAvatar has-defaultAvatar" aria-haspopup="true" data-has-custom-avatar="false">
                                    <div class="DMPopover DMPopover--center">
                                        <button class="transparent-no-border-button DMPopover-button" aria-haspopup="true">
                                            <span class="u-hiddenVisually">{__("Update group photo.")}</span>
                                            <div class="DMUpdateAvatar-avatar">
                                                <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">
                                                    <span class="DMAvatar-container">
                                                        <img class="DMAvatar-image" src="" alt="" title="">
                                                    </span>
                                                </div>
                                            </div>
                                        </button>
                                        <div class="DMPopover-content Caret Caret--top Caret--stroked ">
                                            <ul class="DMPopoverMenu u-textCenter js-focus-on-open u-dropdownUserColor" tabindex="-1" role="menu">
                                                <li class="DMUpdateAvatar-view">
                                                    <button type="button" class="DMPopoverMenu-button">{__("View photo")}</button>
                                                </li>
                                                <li class="DMUpdateAvatar-change">
                                                    <button type="button" class="DMPopoverMenu-button">{__("Upload photo")}</button>
                                                </li>
                                                <li class="DMUpdateAvatar-remove">
                                                    <button type="button" class="DMPopoverMenu-button">{__("Remove")}</button>
                                                </li>
                                            </ul>
                                            <div class="DMUpdateAvatar-photoSelector photo-selector" tabindex="-1" aria-hidden="true">
                                                <div class="image-selector">
                                                    <input type="hidden" name="media_file_name" class="file-name">
                                                    <input type="hidden" name="media_data_empty" class="file-data">
                                                    <label class="t1-label">
                                                        <span class="u-hiddenVisually">{__("Add Photo")}</span>
                                                        <input type="file" name="media[]" class="file-input js-tooltip" accept="image/*" tabindex="-1" title="Add Photo">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="DMConversationSettings-name">
                                <div class="DMUpdateName u-textTruncate">
                                    <div class="DMUpdateName-header account-group">
                                        <span class="DMUpdateName-name u-textTruncate edit-allowed js-tooltip" title="Click to edit group name">Double Bear, Daniel Lee</span>
                                        <span class="UserBadges">
                                            <span class="Icon Icon--verified js-verified hidden">
                                                <span class="u-hiddenVisually">{__("Verified account")}</span>
                                            </span>
                                            <span class="Icon Icon--protected js-protected hidden">
                                                <span class="u-hiddenVisually">{__("Protected Tweets")}</span>
                                            </span>
                                        </span>
                                    </div>
                                    <div class="DMUpdateName-screenName u-textTruncate"></div>
                                    <div class="DMUpdateName-controls">
                                        <span class="DMUpdateName-spinner DMSpinner u-hidden"></span>
                                        <div class="DMUpdateName-form input-group u-hidden">
                                            <input type="text" class="DMUpdateName-input" aria-label="Edit group name">
                                            <button class="DMUpdateName-confirm u-textUserColorLight">
                                                <span class="Icon Icon--check"></span>
                                                <span class="u-hiddenVisually">{__("Save group name")}</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="DMConversationSettings-notifications">
                                <h3>Notifications</h3>
                                <label class="t1-label checkbox">
                                    <input type="checkbox" name="dm[toggle_notifications]">{__("Mute notifications")}
                                </label>
                                <p class="DMConversationSettings-notificationsFooter t1-infotext">{__("Disable all notifications from this conversation. This will not remove you from the group.")}</p>
                            </div>
                            <div class="DMConversationSettings-mentions u-hidden">
                                <label class="t1-label checkbox">
                                    <input type="checkbox" name="dm[toggle_mentions]">{__("Mentions")}
                                </label>
                                <p class="DMConversationSettings-mentionsFooter t1-infotext">{__("Receive notifications when people mention you in this group.")}</p>
                            </div>
                            <div class="DMConversationSettings-subscriptions u-hidden">
                                <h3>Subscriptions</h3>
                                <label class="t1-label checkbox">
                                    <input type="checkbox" name="dm[toggle_subscriptions]">{__("Subscribe to updates")}
                                </label>
                                <p class="DMConversationSettings-subscriptionsFooter t1-infotext"></p>
                            </div>
                        </div>
                        <div class="DMActivity-footer u-emptyHide">
                            <div class="DMConversationSettings-footer u-flexRow u-bgUserColorLightest">
                                <!--<button type="button" class="EdgeButton EdgeButton--secondary js-actionReportConversation">Report conversation</button>-->
                                <button type="button" class="EdgeButton EdgeButton--danger js-actionDeleteConversation">{__("Leave conversation")}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--<div class="modal-overlay custom-overlay"></div>-->

<!--<div class="modal-overlay custom-overlay"></div>-->
{literal} 
<script type="text/javascript">
    $('.DMConversation-convoSettings.dm-to-convoSettings').click(function () {
        $('.right-section.message-popup').css('display', 'none');
        //$('.DMConversationSettings').addClass('DMActivity--open');
	
    });
$('.title-messaging').on('click', function(){
    //$('.custom-overlay').show();
    $('#myModal').modal('show');
    $('body').addClass('modal-open');
	$('body').css({'position':'fixed'});
	$('.modal-dialog').css({'position':'fixed'});
	
});
$('.DMInbox-toolbar').click(function () {
        
	$('body').addClass('modal-open');
	$('body').css({'position':'fixed','-webkit-overflow-scrolling':'touch !important','height':'100% !important'});
	$('.modal-dialog').css({'position':'fixed','-webkit-overflow-scrolling':'touch !important','height':'100% !important'});
	alert('bbbbbbbbbbbb')
	
    });

</script>
{/literal} 