{include file='_head.tpl'}
<div class="profile_header_custom">
{include file='_header.tpl'}
<style>
#postsocial .lsx-emojipicker-container {
    top: -75px !important;
    left: 35px;
}
    .mobileHide {
        display: inline;
    }
    .mce-tinymce{
        box-shadow: none;
    }
@media (max-width: 767px){
.visible-xs-block {
    display: block;
}
 #mobilefeedphoto{
     display: none;
}
#mobileview{
    margin-left: 16%;
}
.panel-default {
        text-align: left;
      }
    .offcanvas-sidebar{
    width:100%
    }
}

@media (max-width:767px) {
    .post_custom_mobile #postBoxDiv .post_area_custom {
        padding-left: 60px;
        padding-top: 2px;
        padding-bottom: 2px;
    }

    .post_custom_mobile .post-avatar {
        width: 36px;
    }

    .post_custom_mobile .post-avatar-picture {
        min-width: 36px;
        min-height: 36px;
    }
    .mce-tinymce {
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
    }
    .post_custom_mobile #postBoxDiv ~ .publisher-footer .publisher-tools li {
        padding: 0 5px 0 0;
    }
    .x-uploader .fa {
        color: #337ab7;
    }
    .publisher-tools-attach .fa.fa-camera.fa-fw.js_x-uploader {
        font-size: 26px;
        line-height: 26px;
    }
    .publisher-tools-attach .x-uploader .fa {
        font-size: 26px;
    }

    .publisher-tools-attach .fa .fa-paperclip .fa-fw .js_x-uploader {
        font-size: 26px;
    }

    .publisher-tools-attach .fa.fa-smile-o {
        color: #337ab7;
        font-size: 26px;
        line-height: 26px;
    }
    .text-counter {
     position: absolute;
     top: 17px;
     right: 125px;
    }
}

@media (max-width: 1024px) {

    .post_custom_mobile #postBoxDiv .post_area_custom {
        padding-left: 60px;
        padding-top: 2px;
        padding-bottom: 2px;
    }

    .post_custom_mobile .post-avatar {
        width: 36px;
    }

    .post_custom_mobile .post-avatar-picture {
        min-width: 36px;
        min-height: 36px;
    }
    .mce-tinymce {
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
    }
    .post_custom_mobile #postBoxDiv ~ .publisher-footer .publisher-tools li {
        padding: 0 5px 0 0;
    }
    .x-uploader .fa {
        color: #337ab7;
    }
    .publisher-tools-attach .fa.fa-camera.fa-fw.js_x-uploader {
        font-size: 26px;
        line-height: 26px;
    }
    .publisher-tools-attach .x-uploader .fa {
        font-size: 26px;
    }
    .publisher-tools-attach .fa .fa-paperclip .fa-fw .js_x-uploader {
        font-size: 26px;
    }
    .publisher-tools-attach .fa.fa-smile-o {
        color: #337ab7;
        font-size: 26px;
        line-height: 26px;
    }
    .text-counter {
         position: absolute;
         top: 17px;
         right: 125px;
    }
}
.mce-stack-layout-item {
    width: 100%;
}

.about-list{
  margin-top:10px !important;
  margin-left:5px !important;
  margin-bottom: 10px !important;
}
.about-list-item .fa{
  top:2px !important;
}
.about-list-item{
  text-align: left  !important;
}
</style>
</div>
 <!-- profile-header -->

            <div class="profile-header">

                <!-- profile-cover -->

                <div {if $profile['user_cover_id']} class="profile-cover-wrapper js_lightbox" data-id="{$profile['user_cover_id']}" data-image="{$system['system_uploads']}/{$profile['user_cover']}" data-context="album" {else} class="profile-cover-wrapper" {/if}  {if $profile['user_cover']} style="background-image:url('{$system['system_uploads']}/{$profile['user_cover']}');" {/if}>

                    {if $profile['user_id'] == $user->_data['user_id']}

                        <div class="profile-cover-change">

                            <i class="fa fa-camera js_x-uploader" data-handle="cover-user"></i>

                        </div>

                        <div class="profile-cover-delete {if !$profile['user_cover']}x-hidden{/if}">

                            <i class="fa fa-trash js_delete-cover" data-handle="cover-user" title='{__("Delete Cover")}'></i>

                        </div>

                        <div class="profile-cover-change-loader">

                            <div class="loader loader_large"></div>

                        </div>

                    {/if}

                </div>

                <!-- profile-cover -->

                <div class="container {if $user->_logged_in}offcanvas{/if}">

                    <div class="row">

                        <!-- profile-avatar -->

                        <div class="profile-avatar-wrapper">

                            <img {if $profile['user_picture_id']} class="js_lightbox" data-id="{$profile['user_picture_id']}" data-image="{$profile['user_picture']}" data-context="album" {/if} src="{$profile['user_picture']}" alt="{$profile['user_firstname']} {$profile['user_lastname']}">

                            {if $profile['user_id'] == $user->_data['user_id']}

                                <div class="profile-avatar-change">

                                    <i class="fa fa-camera js_x-uploader" data-handle="picture-user"></i>

                                </div>

                                <div class="profile-avatar-delete {if $profile['user_picture_default']}x-hidden{/if}">

                                    <i class="fa fa-trash js_delete-picture" data-handle="picture-user" title='{__("Delete Picture")}'></i>

                                </div>

                                <div class="profile-avatar-change-loader">

                                    <div class="loader loader_medium"></div>

                                </div>

                            {/if}

                        </div>

                        <!-- profile-avatar -->



                        <!-- profile-name -->

                        <div class="profile-name-wrapper">

                            <!--<a href="/{$profile['user_name']}">{$profile['user_firstname']} {$profile['user_lastname']}</a>-->



                        </div>

                        <!-- profile-name -->



                        <!-- profile-buttons -->

                        <div class="profile-buttons-wrapper">

                            {if $user->_logged_in}

                                 {if $user->_data['user_id'] != $profile['user_id']}

                                    {if $profile['we_friends']}

                                        <!--<div class="btn btn-default btn-delete js_friend-remove" data-uid="{$profile['user_id']}">

                                            <i class="fa fa-check fa-fw"></i> {__("Friends")}

                                        </div>-->

                                    {elseif $profile['he_request']}

                                        <!--<div class="btn btn-primary js_friend-accept" data-uid="{$profile['user_id']}">{__("Confirm")}</div>

                                        <div class="btn btn-default js_friend-decline" data-uid="{$profile['user_id']}">{__("Delete Request")}</div>-->

                                    {elseif $profile['i_request']}

                                        <!--<div class="btn btn-default btn-sm js_friend-cancel" data-uid="{$profile['user_id']}">

                                            <i class="fa fa-user-plus"></i> {__("Friend Request Sent")}

                                        </div>-->

                                    {else}

                                        <!--<button type="button" class="btn btn-success js_friend-add" data-uid="{$profile['user_id']}">

                                            <i class="fa fa-user-plus"></i> {__("Add Friend")}

                                        </button>-->

                                    {/if}



                                    <div class="btn-group profile_unfollow_btn">

                                        {if $profile['i_follow']}

                                            <button type="button" class="btn btn-default" data-uid="{$profile['user_id']}">

                                                <i class="fa fa-check"></i>

                                                {__("Following")}

                                            </button>

                                        {else}

                                            <button type="button" class="btn btn-default js_follow" data-uid="{$profile['user_id']}">

                                                <!--<i class="fa fa-rss"></i>-->

                                                {__("Follow")}

                                            </button>

                                        {/if}

                                        <!--<button type="button" class="btn btn-default js_chat-start" data-name="{$profile['user_firstname']} {$profile['user_lastname']}" data-uid="{$profile['user_id']}">

                                            <i class="fa fa-comments-o"></i> {__("Message")}

                                        </button>-->

                                        <div class="btn-group">

                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">

                                                <span class="user-dropdown-icon Icon Icon--dotsVertical Icon--medium"></span>

                                            </button>

                                            <ul class="dropdown-menu follow_btn_toggle">

                                            <span class="caret-outer"></span>

                                            <span class="caret-inner"></span>

                                                <li><a href="#" class="js_report" data-handle="user" data-id="{$profile['user_id']}"><i class="fa fa-flag fa-fw"></i> {__("Report")}</a></li>

                                                <li><a href="#" class="js_block-user" data-uid="{$profile['user_id']}"><i class="fa fa-minus-circle fa-fw"></i> {__("Block")}</a></li>

                                            </ul>

                                        </div>

                                    </div>

                                {else}

                                    <a href="/settings/profile" class="btn btn-default">

                                        <i class="fa fa-pencil"></i> <span>{__("Update Info")}</span>

                                    </a>
                                     <a href="#postsocial" data-toggle="modal" data-backdrop="static" data-keyboard="false" class="btn btn-default hand_icon">
                                        <img src="/content/themes/default/images/add_post.png" alt="add_post" width="15"><span> {__("Add New Post")}</span>
                                    </a>

                                {/if}

                            {/if}

                        </div>

                        <!-- profile-buttons -->



                        <!-- profile-tabs -->

                        <div class="profile-tabs-wrapper">

                            <ul class="nav">

                                <li>

                                    <a href="/{$profile['user_name']}">

                                        {__("Posts")}

                                         <small class="text-muted">

                                                <span class="text-underline">{if $profile['posts_count']}{$profile['posts_count']}{else}0{/if}</span>

                                            </small>

                                    </a>

                                </li>

                                <li class="middle-tabs">

                                    <a href="/followings.php?username={$profile['user_name']}" class="active">

                                        {__("Following")}

                                            <small class="text-muted">

                                                <span class="text-underline">{$profile['followings_count']} {* {if $user->_data.followings_count}{$user->_data.followings_count}{else}0{/if} *}</span>

                                            </small>



                                    </a>

                                </li>



                                <li class="middle-tabs">

                                    <a href="/followers.php?username={$profile['user_name']}">

                                        {__("Followers")}

                                         <small class="text-muted">

                                                <span class="text-underline">{$profile['followers_count']} {* {if $user->_data.followers_count}{$user->_data.followers_count}{else}0{/if} *}</span>

                                            </small>

                                    </a>

                                </li>

								{if $profile['user_name'] eq 'milesguo'} 
								<li class="middle-tabs">
									<a href="javascript:;">
													 {__("Users")} 
													 <small class="text-muted">
															<span class="text-underline">{__("111M")}</span>
														</small>
												</a>
								</li>
								{/if}

                                <li class="middle-tabs">

                                    <a href="/likes.php?username={$profile['user_name']}">

                                        {__("Likes")}



                                         <small class="text-muted">

                                                <span class="text-underline" id="current_like_count" data-toggle="modal">{$profile['likes_count']}</span>

                                            </small>

                                    </a>

                                </li>

                                <li class="middle-tabs">
                                    <a href="/post_broadcast.php?username={$profile['user_name']}">
                                        {__("Broadcasts")}

                                         <small class="text-muted">
                                                <span class="text-underline">{$profile['broadcast_count']}</span>
                                            </small>
                                    </a>
                                </li>


                                <li class="dropdown" style="display:none;">

                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                        {__("More")}

                                        <i class="caret"></i>

                                    </a>

                                    <ul class="dropdown-menu">

                                        <li class="middle-tabs-alt">

                                            <a href="/{$profile['user_name']}/friends">{__("Friends")} ({if $profile['friends_count']}{$profile['friends_count']}{else}0{/if})</a>

                                        </li>

                                        <li class="middle-tabs-alt">

                                            <a href="/{$profile['user_name']}/photos">{__("Photos")} ({if $profile['photos_count']}{$profile['photos_count']}{else}0{/if})</a>

                                        </li>

                                          <li class="middle-tabs-alt">

                                            <a href="/{$profile['user_name']}/followers">{__("Followers")} ({if $profile['followers_count']}{$profile['followers_count']}{else}0{/if})</a>

                                        </li>

                                        <li>

                                            <a href="/{$profile['user_name']}/likes">{__("Likes")}</a>

                                        </li>

                                        <li>

                                            <a href="/{$profile['user_name']}/groups">{__("Groups")}</a>

                                        </li>

                                        <li>

                                            <a href="/{$profile['user_name']}/events">{__("Events")}</a>

                                        </li>

                                    </ul>

                                </li>

                            </ul>

                        </div>

                        <!-- profile-tabs -->

                    </div>

                </div>

            </div>

            <!-- profile-header -->

<!-- page content -->

<div class="container {if $user->_logged_in}offcanvas{/if}">

    <div class="row">



        <!-- side panel -->

        {if $user->_logged_in}

            <div class="col-xs-12 visible-xs-block offcanvas-sidebar mt20">

                {include file='_sidebar.tpl'}

            </div>

        {/if}

        <!-- side panel -->



        <div class="col-xs-12 {if $user->_logged_in}offcanvas-mainbar{/if}">





            <!-- profile-content -->

            <div class="row">



                <!-- profile-buttons alt -->

                {if $user->_logged_in && $user->_data['user_id'] != $profile['user_id']}

                    <!--<div class="col-sm-12">

                        <div class="panel panel-default profile-buttons-wrapper-alt">

                            <div class="panel-body">

                                {if $user->_logged_in && $user->_data['user_id'] != $profile['user_id']}

                                    {if $profile['we_friends']}

                                        <div class="btn btn-default btn-delete js_friend-remove" data-uid="{$profile['user_id']}">

                                            <i class="fa fa-check fa-fw"></i> {__("Friends")}

                                        </div>

                                    {elseif $profile['he_request']}

                                        <div class="btn btn-primary js_friend-accept" data-uid="{$profile['user_id']}">{__("Confirm")}</div>

                                        <div class="btn btn-default js_friend-decline" data-uid="{$profile['user_id']}">{__("Delete Request")}</div>

                                    {elseif $profile['i_request']}

                                        <div class="btn btn-default btn-sm js_friend-cancel" data-uid="{$profile['user_id']}">

                                            <i class="fa fa-user-plus"></i> {__("Friend Request Sent")}

                                        </div>

                                    {else}

                                        <button type="button" class="btn btn-success js_friend-add" data-uid="{$profile['user_id']}">

                                            <i class="fa fa-user-plus"></i> {__("Add Friend")}

                                        </button>

                                    {/if}



                                    <div class="btn-group pull-right flip">

                                        {if $profile['i_follow']}

                                        <button type="button" class="btn btn-default js_unfollow" data-uid="{$profile['user_id']}">

                                            <i class="fa fa-check"></i>

                                            {__("Following")}

                                        </button>

                                        {else}

                                        <button type="button" class="btn btn-default js_follow" data-uid="{$profile['user_id']}">

                                            <i class="fa fa-rss"></i>

                                            {__("Follow")}

                                        </button>

                                        {/if}

                                        <button type="button" class="btn btn-default js_chat-start" data-name="{$profile['user_firstname']} {$profile['user_lastname']}" data-uid="{$profile['user_id']}">

                                            <i class="fa fa-comments-o"></i> {__("Message")}

                                        </button>

                                        <div class="btn-group">

                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">

                                                <i class="fa fa-bars"></i>

                                            </button>

                                            <ul class="dropdown-menu">

                                                <li><a href="#" class="js_report" data-handle="user" data-id="{$profile['user_id']}"><i class="fa fa-flag fa-fw"></i> {__("Report")}</a></li>

                                            <li><a href="#" class="js_block-user" data-uid="{$profile['user_id']}"><i class="fa fa-minus-circle fa-fw"></i>{__("Block")}</a></li>

                                            </ul>

                                        </div>

                                    </div>

                                {else}

                                    <a href="/settings/profile" class="btn btn-default">

                                        <i class="fa fa-pencil"></i> {__("Update Info")}

                                    </a>

                                {/if}

                            </div>

                        </div>

                    </div>-->

                {/if}

                <!-- profile-buttons alt -->



                <!-- view content -->

                {if $view == ""}

                    <div class="col-sm-3">

                        <!-- about -->

                        <div class="panel panel-default profile-rightbar">

                            <div class="panel-body">

                               {* {if !is_empty($profile['user_biography'])} *}

                                <div class="about-bio">
                                    <div class="username_and_tick">
                                    	<p class="user-name">{$profile['user_firstname']} {$profile['user_lastname']}</p>
                                    	  {if $profile['user_verified']}

                                            <i data-toggle="tooltip" data-placement="top" title='{__("Verified User")}' class="fa fa-check-circle fa-fw verified-badge"></i>

                                        {/if}

                                        {if $profile['user_subscribed']}

                                            <i data-toggle="tooltip" data-placement="top" title='{__("Pro User")}' class="fa fa-bolt fa-fw pro-badge"></i>

                                        {/if}
                                    </div>

                                        <p class="main-username">@{$profile['user_name']}</p>

                                        {$profile['user_biography']}

                                </div>

                               {* {/if} *}

                                <ul class="about-list">

                                    {if $profile['user_subscribed']}

                                        <li class="package" {if $profile['package_color']} style="background: {$profile['package_color']}" {/if}>

                                            <i class="fa fa-bolt fa-fw"></i> {$profile['package_name']} {__("Member")}

                                        </li>

                                    {/if}



                                    {if $profile['user_work_title']}

                                        {if $profile['user_id'] == $user->_data['user_id'] || $profile['user_privacy_work'] == "public" || ($profile['user_privacy_work'] == "friends" && $profile['we_friends'])}

                                            <li>

                                                <div class="about-list-item">

                                                    <i class="fa fa-briefcase fa-fw fa-lg"></i>

                                                    {$profile['user_work_title']} {__("at")} <span class="text-link">{$profile['user_work_place']}</span>

                                                </div>

                                            </li>

                                        {/if}

                                    {/if}



                                    {if $profile['user_edu_major']}

                                        {if $profile['user_id'] == $user->_data['user_id'] || $profile['user_privacy_education'] == "public" || ($profile['user_privacy_education'] == "friends" && $profile['we_friends'])}

                                            <li>

                                                <div class="about-list-item">

                                                    <i class="fa fa-graduation-cap fa-fw fa-lg"></i>

                                                    {__("Studied")} {$profile['user_edu_major']}

                                                    {__("at")}  <span class="text-link">{$profile['user_edu_school']}</span>

                                                    <div class="details">

                                                        Class of {$profile['user_edu_class']}

                                                    </div>

                                                </div>

                                            </li>

                                        {/if}

                                    {/if}



                                    {if $profile['user_current_city']}

                                        {if $profile['user_id'] == $user->_data['user_id'] || $profile['user_privacy_location'] == "public" || ($profile['user_privacy_location'] == "friends" && $profile['we_friends'])}

                                            <li>

                                                <div class="about-list-item">

                                                    <i class="fa fa-home fa-fw fa-lg"></i>

                                                    {__("Lives in")} <span class="text-link">{$profile['user_current_city']}</span>

                                                </div>

                                            </li>

                                        {/if}

                                    {/if}



                                    {if $profile['user_hometown']}

                                        {if $profile['user_id'] == $user->_data['user_id'] || $profile['user_privacy_location'] == "public" || ($profile['user_privacy_location'] == "friends" && $profile['we_friends'])}

                                            <li>

                                                <div class="about-list-item">

                                                    <i class="fa fa-map-marker fa-fw fa-lg"></i>

                                                    {__("From")} <span class="text-link">{$profile['user_hometown']}</span>

                                                </div>

                                            </li>

                                        {/if}

                                    {/if}



                                   {*<li>

                                        <div class="about-list-item">

                                            {if $profile['user_gender'] == "male"}

                                                <i class="fa fa-male fa-fw fa-lg"></i>

                                                {__("Male")}

                                            {else}

                                                <i class="fa fa-female fa-fw fa-lg"></i>

                                                {__("Female")}

                                            {/if}



                                        </div>

                                    </li>*}



                                    {if $profile['user_relationship']}

                                        {if $profile['user_id'] == $user->_data['user_id'] || $profile['user_privacy_relationship'] == "public" || ($profile['user_privacy_relationship'] == "friends" && $profile['we_friends'])}

                                            <li>

                                                <div class="about-list-item">

                                                    <i class="fa fa-heart fa-fw fa-lg"></i>

                                                    {if $profile['user_relationship'] == "relationship"}

                                                        {__("In a relationship")}

                                                    {elseif $profile['user_relationship'] == "complicated"}

                                                        {__("It's complicated")}

                                                    {else}

                                                        {__($profile['user_relationship']|ucfirst)}

                                                    {/if}

                                                </div>

                                            </li>

                                        {/if}

                                    {/if}



                                    {if $profile['user_birthdate'] != null}



                                    <li>
                                        <div class="about-list-item">
                                            <i class="fa fa-calendar fa-fw fa-lg"></i>
                                            {if $profile['user_id'] == $user->_data['user_id']
                                            || $profile['user_privacy_birthdate'] == "public" ||
                                            ($profile['user_privacy_birthdate'] == "friends" && $profile['we_friends'])
                                            ||($profile['user_privacy_birthdate'] == "following" && $profile['i_follow'])||
                                            ($profile['user_privacy_birthdate'] == "followers" && $profile['he_follow'])
                                            ||($profile['user_privacy_birthdate'] == "followers_following" && ($profile['he_follow'] && $profile['i_follow']))
                                            }
                                            {assign var="showBirthdatAsMd" value='md'}

                                            {/if}

                                            {if $profile['user_id'] == $user->_data['user_id']
                                            || $profile['user_privacy_birthyear'] == "public" ||
                                            ($profile['user_privacy_birthyear'] == "friends" && $profile['we_friends'])
                                            ||($profile['user_privacy_birthyear'] == "following" && $profile['i_follow'])
                                            ||($profile['user_privacy_birthyear'] == "followers" && $profile['he_follow'])
                                            ||($profile['user_privacy_birthyear'] == "followers_following" && ($profile['he_follow'] && $profile['i_follow']))
                                            }
                                            {assign var="showBirthdatAsY" value='y'}

                                            {/if}

                                            {if $showBirthdatAsMd == 'md' && $showBirthdatAsY == 'y'}
                                            {__("Born on")} {$profile['user_birthdate']|date_format:"%d/%m/%Y"}
                                            {elseif $showBirthdatAsMd == 'md'}
                                            {__("Born on")} {$profile['user_birthdate']|date_format:"%d/%m"}
                                            {elseif $showBirthdatAsY == 'y'}
                                            {__("Born on")} {$profile['user_birthdate']|date_format:"%Y"}
                                            {else}
                                            {/if}

                                        </div>
                                   </li>

                                    {/if}

                                    {if $profile['user_location']}

                                    <li>
                                        <div class="about-list-item">
                                            <i class="fa fa-map-marker fa-fw"></i>
                                            {$profile['user_location']}
                                        </div>
                                    </li>
                                    {/if}



                                    {if $profile['user_website']}

                                        <li>

                                            <div class="about-list-item">

                                                <i class="fa fa-globe fa-fw fa-lg"></i>

                                                <a target="_blank" href="{$profile['user_website']}">{$profile['user_website']}</a>

                                            </div>

                                        </li>

                                    {/if}


                                    <!--
                                    <li>

                                        <div class="about-list-item">

                                            <i class="fa fa-rss fa-fw fa-lg"></i>

                                            {__("Followed by")}

                                            <a href="/followers.php?username={$profile['user_name']}">{$profile['followers_count']} {__("people")}</a>

                                        </div>

                                    </li> -->

                                </ul>
                                {if $user->_data['user_id'] != $profile['user_id']}

                                <!--<button type="button" class="btn btn-default user-message" data-toggle="modal" data-url="chat/user_search.php?user_id={$profile['user_id']}&user_name=@{$profile['user_name']}&name={$profile['user_firstname']} {$profile['user_lastname']}">

                                    {__(" Send Message")}

                                </button>-->



                                {/if}

                            </div>

                        </div>

                        <!-- about -->



                        <!-- custom fields -->

                        {if $custom_fields['basic']}

                        {if $profile['user_id'] == $user->_data['user_id'] || $profile['user_privacy_basic'] == "public" || ($profile['user_privacy_basic'] == "friends" && $profile['we_friends'])}

                            <div class="panel panel-default">

                                <div class="panel-heading">

                                    <i class="fa fa-user fa-fw text-info"></i> {__("Basic Info")}

                                </div>

                                <div class="panel-body">

                                    <ul class="about-list">

                                        {foreach $custom_fields['basic'] as $custom_field}

                                            {if $custom_field['value']}

                                                <li>

                                                    <strong>{$custom_field['label']}</strong><br>

                                                    {$custom_field['value']}

                                                </li>

                                            {/if}

                                        {/foreach}

                                    </ul>

                                </div>

                            </div>

                        {/if}

                        {/if}

                        {if $custom_fields['work']}

                        {if $profile['user_id'] == $user->_data['user_id'] || $profile['user_privacy_work'] == "public" || ($profile['user_privacy_work'] == "friends" && $profile['we_friends'])}

                            <div class="panel panel-default">

                                <div class="panel-heading">

                                    <i class="fa fa-briefcase fa-fw text-info"></i> {__("Work Info")}

                                </div>

                                <div class="panel-body">

                                    <ul class="about-list">

                                        {foreach $custom_fields['work'] as $custom_field}

                                            {if $custom_field['value']}

                                                <li>

                                                    <strong>{$custom_field['label']}</strong><br>

                                                    {$custom_field['value']}

                                                </li>

                                            {/if}

                                        {/foreach}

                                    </ul>

                                </div>

                            </div>

                        {/if}

                        {/if}

                        {if $custom_fields['location']}

                        {if $profile['user_id'] == $user->_data['user_id'] || $profile['user_privacy_location'] == "public" || ($profile['user_privacy_location'] == "friends" && $profile['we_friends'])}

                            <div class="panel panel-default">

                                <div class="panel-heading">

                                    <i class="fa fa-home fa-fw text-info"></i> {__("Location Info")}

                                </div>

                                <div class="panel-body">

                                    <ul class="about-list">

                                        {foreach $custom_fields['location'] as $custom_field}

                                            {if $custom_field['value']}

                                                <li>

                                                    <strong>{$custom_field['label']}</strong><br>

                                                    {$custom_field['value']}

                                                </li>

                                            {/if}

                                        {/foreach}

                                    </ul>

                                </div>

                            </div>

                        {/if}

                        {/if}

                        {if $custom_fields['education']}

                        {if $profile['user_id'] == $user->_data['user_id'] || $profile['user_privacy_education'] == "public" || ($profile['user_privacy_education'] == "friends" && $profile['we_friends'])}

                            <div class="panel panel-default">

                                <div class="panel-heading">

                                    <i class="fa fa-graduation-cap fa-fw text-info"></i> {__("Education Info")}

                                </div>

                                <div class="panel-body">

                                    <ul class="about-list">

                                        {foreach $custom_fields['education'] as $custom_field}

                                            {if $custom_field['value']}

                                                <li>

                                                    <strong>{$custom_field['label']}</strong><br>

                                                    {$custom_field['value']}

                                                </li>

                                            {/if}

                                        {/foreach}

                                    </ul>

                                </div>

                            </div>

                        {/if}

                        {/if}

                        {if $custom_fields['other']}

                        {if $profile['user_id'] == $user->_data['user_id'] || $profile['user_privacy_other'] == "public" || ($profile['user_privacy_other'] == "friends" && $profile['we_friends'])}

                            <div class="panel panel-default">

                                <div class="panel-heading">

                                    <i class="fa fa-info-circle fa-fw text-info"></i> {__("Other Info")}

                                </div>

                                <div class="panel-body">

                                    <ul class="about-list">

                                        {foreach $custom_fields['other'] as $custom_field}

                                            {if $custom_field['value']}

                                                <li>

                                                    <strong>{$custom_field['label']}</strong><br>

                                                    {$custom_field['value']}

                                                </li>

                                            {/if}

                                        {/foreach}

                                    </ul>

                                </div>

                            </div>

                        {/if}

                        {/if}

                        <!-- custom fields -->



                        <!-- social links -->

                        {if $profile['user_social_facebook'] || $profile['user_social_twitter'] || $profile['user_social_google'] || $profile['user_social_youtube'] || $profile['user_social_instagram'] || $profile['user_social_linkedin'] || $profile['user_social_vkontakte']}

                            <div class="panel panel-default">

                                <div class="panel-heading">

                                    <i class="fa fa-share-alt fa-fw text-info"></i> {__("Social Links")}

                                </div>

                                <div class="panel-body text-center">

                                    {if $profile['user_social_facebook']}

                                        <a target="_blank" href="{$profile['user_social_facebook']}" class="btn btn-social-icon btn-facebook">

                                            <i class="fa fa-facebook"></i>

                                        </a>

                                    {/if}

                                    {if $profile['user_social_twitter']}

                                        <a target="_blank" href="{$profile['user_social_twitter']}" class="btn btn-social-icon btn-twitter">

                                            <i class="fa fa-twitter"></i>

                                        </a>

                                    {/if}

                                    {if $profile['user_social_google']}

                                        <a target="_blank" href="{$profile['user_social_google']}" class="btn btn-social-icon btn-google">

                                            <i class="fa fa-google"></i>

                                        </a>

                                    {/if}

                                    {if $profile['user_social_youtube']}

                                        <a target="_blank" href="{$profile['user_social_youtube']}" class="btn btn-social-icon btn-pinterest">

                                            <i class="fa fa-youtube"></i>

                                        </a>

                                    {/if}

                                    {if $profile['user_social_instagram']}

                                        <a target="_blank" href="{$profile['user_social_instagram']}" class="btn btn-social-icon btn-instagram">

                                            <i class="fa fa-instagram"></i>

                                        </a>

                                    {/if}

                                    {if $profile['user_social_linkedin']}

                                        <a target="_blank" href="{$profile['user_social_linkedin']}" class="btn btn-social-icon btn-linkedin">

                                            <i class="fa fa-linkedin"></i>

                                        </a>

                                    {/if}

                                    {if $profile['user_social_vkontakte']}

                                        <a target="_blank" href="{$profile['user_social_vkontakte']}" class="btn btn-social-icon btn-vk">

                                            <i class="fa fa-vk"></i>

                                        </a>

                                    {/if}

                                </div>

                            </div>

                        {/if}

                        <!-- social links -->



                        <!-- friends -->

                        {if $profile['friends_count'] > 0}

                        	{if $user->_data['user_id'] != $profile['user_id']}

                            	<div class="panel panel-default panel-friends profile-rightbar">

                                <div class="panel-heading">

                                    <i class="fa fa-users fa-fw text-info"></i> <a href="/{$profile['user_name']}/friends">{$profile['friends_count']} {__("Follower you know")}</a

                                    <!--<small>{$profile['friends_count']}</small> -->

                                    {if $profile['mutual_friends_count'] && $profile['mutual_friends_count'] > 0}

                                     <!--<small>

                                            (<span class="text-underline" data-toggle="modal" data-url="users/mutual_friends.php?uid={$profile['user_id']}">{$profile['mutual_friends_count']} {__("mutual friends")}</span>)

                                        </small>-->

                                    {/if}

                                </div>

                                <div class="panel-body">

                                    <div class="row">

                                        {foreach $profile['friends'] as $_friend}

                                            <div class="col-xs-2 col-sm-2">

                                                <a class="friend-picture" href="/{$_friend['user_name']}" style="background-image:url({$_friend['user_picture']});" >

                                                    <!--<span class="friend-name">{$_friend['user_firstname']} {$_friend['user_lastname']}</span>-->

                                                </a>

                                            </div>

                                        {/foreach}

                                    </div>

                                </div>

                            </div>

                            {/if}

                        {/if}

                        <!-- friends -->



                        <!-- photos -->

                        {if count($profile['photos']) > 0}

                            <div class="panel panel-default panel-photos profile-rightbar">

                                <div class="panel-heading">

                                    <i class="fa fa-picture-o fa-fw text-info"></i> <a href="/media.php?username={$profile['user_name']}">{__("Photos & Videos")}</a>

                                </div>

                                <div class="panel-body">

                                    <div class="row">

                                        {foreach $profile['photos'] as $photo}

                                            {include file='__feeds_photo.tpl' _context="photos" _small=true}

                                        {/foreach}

                                    </div>

                                </div>

                            </div>

                        {/if}

                        <!-- photos -->



                        <!-- pages -->

                        {if count($profile['pages']) > 0}

                            <!--<div class="panel panel-default panel-friends profile-rightbar">

                                <div class="panel-heading">

                                    <i class="fa fa-flag fa-fw text-info"></i> <a href="/{$profile['user_name']}/likes">{__("Likes")}</a>

                                </div>

                                <div class="panel-body">

                                    <div class="row">

                                        {foreach $profile['pages'] as $_page}

                                            <div class="col-xs-3 col-sm-4">

                                                <a class="friend-picture" href="/pages/{$_page['page_name']}" style="background-image:url({$_page['page_picture']});" >

                                                    <span class="friend-name">{$_page['page_title']}</span>

                                                </a>

                                            </div>

                                        {/foreach}

                                    </div>

                                </div>

                            </div>-->

                        {/if}

                        <!-- pages -->



                        <!-- groups -->

                        {if count($profile['groups']) > 0}

                            <div class="panel panel-default panel-friends profile-rightbar">

                                <div class="panel-heading">

                                    <i class="fa fa-users fa-fw text-info"></i> <a href="/{$profile['user_name']}/groups">{__("Groups")}</a>

                                </div>

                                <div class="panel-body">

                                    <div class="row">

                                        {foreach $profile['groups'] as $_group}

                                            <div class="col-xs-3 col-sm-4">

                                                <a class="friend-picture" href="/groups/{$_group['group_name']}" style="background-image:url({$_group['group_picture']});" >

                                                    <span class="friend-name">{$_group['group_title']}</span>

                                                </a>

                                            </div>

                                        {/foreach}

                                    </div>

                                </div>

                            </div>

                        {/if}

                        <!-- groups -->



                        <!-- events -->

                        {if count($profile['events']) > 0}

                            <!--<div class="panel panel-default panel-friends profile-rightbar">

                                <div class="panel-heading">

                                    <i class="fa fa-calendar fa-fw text-info"></i> <a href="/{$profile['user_name']}/events">{__("Events")}</a>

                                </div>

                                <div class="panel-body">

                                    <div class="row">

                                        {foreach $profile['events'] as $_event}

                                            <div class="col-xs-3 col-sm-4">

                                                <a class="friend-picture" href="/events/{$_event['event_id']}" style="background-image:url({$_event['event_picture']});" >

                                                    <span class="friend-name">{$_event['event_title']}</span>

                                                </a>

                                            </div>

                                        {/foreach}

                                    </div>

                                </div>

                            </div>-->

                        {/if}

                        <!-- events -->

                    </div>



                   <div class="col-md-8">



                    <div class="panel panel-default">



                        {if $view == ""}







                            <div class="panel-body">



                                {if count($people) > 0}









                                        {include file='__feeds_followings.tpl'}










                                {else}



                                    <p class="text-center text-muted">



                                        {__("No people available")}



                                    </p>



                                {/if}







                                <!-- see-more -->



                                {if count($people) >= $system['min_results']}



                                    <div class="alert alert-info see-more mb10 js_see-more {if $user->_logged_in} js_see-more-infinite{/if}" data-get="followings" data-uid="{$profile['user_id']}" data-remove="true">



                                        <span>{__("See More")}</span>



                                        <div class="loader loader_small x-hidden"></div>



                                    </div>



                                {/if}



                                <!-- see-more -->



                            </div>



                        {elseif $view == "find"}



                            <div class="panel-heading light">



                                <strong>{__("Search Results")}</strong>



                            </div>



                            <div class="panel-body">



                                {if count($people) > 0}



                                    <ul>



                                        {foreach $people as $_user}



                                        {include file='__feeds_followers.tpl' _connection=$_user['connection']}



                                        {/foreach}



                                    </ul>



                                {else}



                                    <p class="text-center text-muted">



                                        {__("No people available for your search")}



                                    </p>



                                {/if}



                            </div>



                        {elseif $view == "friend_requests"}



                            <div class="panel-heading light">



                                <strong>{__("Respond to Your Friend Request")}</strong>



                            </div>



                            <div class="panel-body">



                                {if $user->_data['friend_requests_count'] > 0}



                                    <ul>



                                        {foreach $user->_data['friend_requests'] as $_user}



                                        {include file='__feeds_followers.tpl' _connection="request"}



                                        {/foreach}



                                    </ul>



                                {else}



                                    <p class="text-center text-muted">



                                        {__("No new requests")}



                                    </p>



                                {/if}







                                <!-- see-more -->



                                {if $user->_data['friend_requests_count'] >= $system['max_results']}



                                    <div class="alert alert-info see-more js_see-more" data-get="friend_requests">



                                        <span>{__("See More")}</span>



                                        <div class="loader loader_small x-hidden"></div>



                                    </div>



                                {/if}



                                <!-- see-more -->



                            </div>



                        {elseif $view == "sent_requests"}



                            <div class="panel-heading light">



                                <strong>{__("Friend Requests Sent")}</strong>



                            </div>



                            <div class="panel-body">



                                {if $user->_data['friend_requests_sent_count'] > 0}



                                    <ul>



                                        {foreach $user->_data['friend_requests_sent'] as $_user}



                                        {include file='__feeds_followers.tpl' _connection="cancel"}



                                        {/foreach}



                                    </ul>



                                {else}



                                    <p class="text-center text-muted">



                                        {__("No new requests")}



                                    </p>



                                {/if}







                                <!-- see-more -->



                                {if $user->_data['friend_requests_sent_count'] >= $system['max_results']}



                                    <div class="alert alert-info see-more js_see-more" data-get="friend_requests_sent">



                                        <span>{__("See More")}</span>



                                        <div class="loader loader_small x-hidden"></div>



                                    </div>



                                {/if}



                                <!-- see-more -->



                            </div>



                        {/if}



                    </div>



                </div>



                {elseif $view == "friends"}

                    <!-- friends -->

                    <div class="col-xs-12">

                        <div class="panel panel-default">

                            <div class="panel-heading with-icon with-nav">

                                <!-- friend requests -->

                                {if $user->_logged_in && $user->_data['user_id'] == $profile['user_id']}

                                    <div class="pull-right flip">

                                        <a href="/people/friend_requests" class="btn btn-default btn-sm">

                                            {__("Friend Requests")}

                                        </a>

                                    </div>

                                {/if}

                                <!-- friend requests -->



                                <!-- panel title -->

                                <div class="mb20">

                                    <i class="fa fa-users pr5 panel-icon"></i>

                                    <strong>{__("Friends")}</strong>

                                </div>

                                <!-- panel title -->



                                <!-- panel nav -->

                                <ul class="nav nav-tabs">

                                    <li class="active">

                                        <a href="/{$profile['user_name']}/friends">

                                            <strong class="pr5">{__("Friends")}</strong>

                                            <span class="label label-info ml5">{$profile['friends_count']}</span>

                                        </a>

                                    </li>

                                    <li>

                                        <a href="/{$profile['user_name']}/followers">{__("Followers")}</a>

                                    </li>

                                    <li>

                                        <a href="/{$profile['user_name']}/followings">{__("Followings")}</a>

                                    </li>

                                </ul>

                                <!-- panel nav -->

                            </div>

                            <div class="panel-body">

                                {if $profile['friends_count'] > 0}

                                    <ul class="row">

                                        {foreach $profile['friends'] as $_user}

                                        {include file='__feeds_user.tpl' _connection=$_user["connection"] _parent="profile"}

                                        {/foreach}

                                    </ul>

                                    {if count($profile['friends']) >= $system['min_results_even']}

                                        <!-- see-more -->

                                        <div class="alert alert-info see-more js_see-more" data-get="friends" data-uid="{$profile['user_id']}">

                                            <span>{__("See More")}</span>

                                            <div class="loader loader_small x-hidden"></div>

                                        </div>

                                        <!-- see-more -->

                                    {/if}

                                {else}

                                    <p class="text-center text-muted mt10">

                                        {$profile['user_firstname']} {__("doesn't have friends")}

                                    </p>

                                {/if}

                            </div>

                        </div>

                    </div>

                    <!-- friends -->

                {elseif $view == "photos"}

                    <!-- photos -->

                    <div class="col-xs-12">

                        <div class="panel panel-default panel-photos">

                            <div class="panel-heading with-icon with-nav">

                                <!-- panel title -->

                                <div class="mb20">

                                    <i class="fa fa-picture-o pr5 panel-icon"></i>

                                    <strong>{__("Photos")}</strong>

                                </div>

                                <!-- panel title -->



                                <!-- panel nav -->

                                <ul class="nav nav-tabs">

                                    <li class="active">

                                        <a href="/{$profile['user_name']}/photos">

                                            <strong class="pr5">{__("Photos")}</strong>

                                        </a>

                                    </li>

                                    <li>

                                        <a href="/{$profile['user_name']}/albums">{__("Albums")}</a>

                                    </li>

                                </ul>

                                <!-- panel nav -->

                            </div>

                            <div class="panel-body">

                                {if count($profile['photos']) > 0}

                                    <ul class="row">

                                        {foreach $profile['photos'] as $photo}

                                            {include file='__feeds_photo.tpl' _context="photos"}

                                        {/foreach}

                                    </ul>

                                    {if count($profile['photos']) >= $system['min_results_even']}

                                        <!-- see-more -->

                                        <div class="alert alert-info see-more js_see-more" data-get="photos" data-id="{$profile['user_id']}" data-type='user'>

                                            <span>{__("See More")}</span>

                                            <div class="loader loader_small x-hidden"></div>

                                        </div>

                                        <!-- see-more -->

                                    {/if}

                                {else}

                                    <p class="text-center text-muted mt10">

                                        {$profile['user_firstname']} {__("doesn't have photos")}

                                    </p>

                                {/if}

                            </div>

                        </div>

                    </div>

                    <!-- photos -->

                {elseif $view == "albums"}

                    <!-- albums -->

                    <div class="col-xs-12">

                        <div class="panel panel-default panel-albums">

                            <div class="panel-heading with-icon with-nav">

                                <!-- panel title -->

                                <div class="mb20">

                                    <i class="fa fa-picture-o pr5 panel-icon"></i>

                                    <strong>{__("Photos")}</strong>

                                </div>

                                <!-- panel title -->



                                <!-- panel nav -->

                                <ul class="nav nav-tabs">

                                    <li>

                                        <a href="/{$profile['user_name']}/photos">{__("Photos")}</a>

                                    </li>

                                    <li class="active">

                                        <a href="/{$profile['user_name']}/albums">

                                            <strong class="pr5">{__("Albums")}</strong>

                                        </a>

                                    </li>

                                </ul>

                                <!-- panel nav -->

                            </div>

                            <div class="panel-body">

                                {if count($profile['albums']) > 0}

                                    <ul class="row">

                                        {foreach $profile['albums'] as $album}

                                        {include file='__feeds_album.tpl'}

                                        {/foreach}

                                    </ul>

                                    {if count($profile['albums']) >= $system['max_results_even']}

                                        <!-- see-more -->

                                        <div class="alert alert-info see-more js_see-more" data-get="albums" data-id="{$profile['user_id']}" data-type='user'>

                                            <span>{__("See More")}</span>

                                            <div class="loader loader_small x-hidden"></div>

                                        </div>

                                        <!-- see-more -->

                                    {/if}

                                {else}

                                    <p class="text-center text-muted mt10">

                                        {$profile['user_firstname']} {__("doesn't have albums")}

                                    </p>

                                {/if}

                            </div>

                        </div>

                    </div>

                    <!-- albums -->

                {elseif $view == "album"}

                    <!-- albums -->

                    <div class="col-xs-12">

                        <div class="panel panel-default panel-albums">

                            <div class="panel-heading with-icon with-nav">

                                <!-- back to albums -->

                                <div class="pull-right flip">

                                    <a href="/{$profile['user_name']}/albums" class="btn btn-default btn-sm">

                                        <i class="fa fa-arrow-circle-left"></i> {__("Back to Albums")}

                                    </a>

                                </div>

                                <!-- back to albums -->



                                <!-- panel title -->

                                <div class="mb20">

                                    <i class="fa fa-picture-o pr5 panel-icon"></i>

                                    <strong>{__("Photos")}</strong>

                                </div>

                                <!-- panel title -->



                                <!-- panel nav -->

                                <ul class="nav nav-tabs">

                                    <li>

                                        <a href="/{$profile['user_name']}/photos">{__("Photos")}</a>

                                    </li>

                                    <li class="active">

                                        <a href="/{$profile['user_name']}/albums">

                                            <strong class="pr5">{__("Albums")}</strong>

                                        </a>

                                    </li>

                                </ul>

                                <!-- panel nav -->

                            </div>

                            <div class="panel-body">

                            {include file='_album.tpl'}

                            </div>

                        </div>

                    </div>

                    <!-- albums -->

                {elseif $view == "followers"}

                    <!-- followers -->

                    <div class="col-xs-12">

                        <div class="panel panel-default">

                            <div class="panel-heading with-icon with-nav">

                                {if $user->_logged_in && $user->_data['user_id'] == $profile['user_id']}

                                <!-- friend requests -->

                                <div class="pull-right flip">

                                    <a href="/people/friend_requests" class="btn btn-default btn-sm">

                                        {__("Friend Requests")}

                                    </a>

                                </div>

                                <!-- friend requests -->

                                {/if}



                                <!-- panel title -->

                                <div class="mb20">

                                    <i class="fa fa-users pr5 panel-icon"></i>

                                    <strong>{__("Friends")}</strong>

                                </div>

                                <!-- panel title -->



                                <!-- panel nav -->

                                <ul class="nav nav-tabs">

                                    <li>

                                        <a href="/{$profile['user_name']}/friends">{__("Friends")}</a>

                                    </li>

                                    <li class="active">

                                        <a href="/{$profile['user_name']}/followers">

                                            <strong class="pr5">{__("Followers")}</strong>

                                            <span class="label label-info ml5">{$profile['followers_count']}</span>

                                        </a>

                                    </li>

                                    <li>

                                        <a href="/{$profile['user_name']}/followings">{__("Followings")}</a>

                                    </li>

                                </ul>

                                <!-- panel nav -->

                            </div>

                            <div class="panel-body">

                                {if $profile['followers_count'] > 0}

                                    <ul class="row">

                                        {foreach $profile['followers'] as $_user}

                                            {include file='__feeds_user.tpl' _connection=$_user["connection"] _parent="profile"}

                                        {/foreach}

                                    </ul>



                                    {if count($profile['followers']) >= $system['min_results_even']}

                                    <!-- see-more -->

                                    <div class="alert alert-info see-more js_see-more" data-get="followers" data-uid="{$profile['user_id']}">

                                        <span>{__("See More")}</span>

                                        <div class="loader loader_small x-hidden"></div>

                                    </div>

                                    <!-- see-more -->

                                    {/if}



                                {else}

                                    <p class="text-center text-muted mt10">

                                        {$profile['user_firstname']} {__("doesn't have followers")}

                                    </p>

                                {/if}

                            </div>

                        </div>

                    </div>

                    <!-- followers -->

                {elseif $view == "followings"}

                    <!-- followings -->

                    <div class="col-xs-12">

                        <div class="panel panel-default">

                            <div class="panel-heading with-icon with-nav">

                                {if $user->_logged_in && $user->_data['user_id'] == $profile['user_id']}

                                <!-- friend requests -->

                                <div class="pull-right flip">

                                    <a href="/people/friend_requests" class="btn btn-default btn-sm">

                                        {__("Friend Requests")}

                                    </a>

                                </div>

                                <!-- friend requests -->

                                {/if}



                                <!-- panel title -->

                                <div class="mb20">

                                    <i class="fa fa-users pr5 panel-icon"></i>

                                    <strong>{__("Friends")}</strong>

                                </div>

                                <!-- panel title -->



                                <!-- panel nav -->

                                <ul class="nav nav-tabs">

                                    <li>

                                        <a href="/{$profile['user_name']}/friends">{__("Friends")}</a>

                                    </li>

                                    <li>

                                        <a href="/{$profile['user_name']}/followers">{__("Followers")}</a>

                                    </li>

                                    <li class="active">

                                        <a href="/{$profile['user_name']}/followings">

                                            <strong class="pr5">{__("Followings")}</strong>

                                            <span class="label label-info ml5">{$profile['followings_count']}</span>

                                        </a>

                                    </li>

                                </ul>

                                <!-- panel nav -->

                            </div>

                            <div class="panel-body">

                                {if $profile['followings_count'] > 0}

                                    <ul class="row">

                                        {foreach $profile['followings'] as $_user}

                                            {include file='__feeds_user.tpl' _connection=$_user["connection"] _parent="profile"}

                                        {/foreach}

                                    </ul>



                                    {if count($profile['followings']) >= $system['min_results_even']}

                                    <!-- see-more -->

                                    <div class="alert alert-info see-more js_see-more" data-get="followings" data-uid="{$profile['user_id']}">

                                        <span>{__("See More")}</span>

                                        <div class="loader loader_small x-hidden"></div>

                                    </div>

                                    <!-- see-more -->

                                    {/if}

                                {else}

                                    <p class="text-center text-muted mt10">

                                        {$profile['user_firstname']} {__("doesn't have followings")}

                                    </p>

                                {/if}

                            </div>

                        </div>

                    </div>

                    <!-- followings -->

                {elseif $view == "followings"}

                    <!-- likes -->

                    <div class="col-xs-12">

                        <div class="panel panel-default">

                            <div class="panel-heading with-icon">

                                <!-- panel title -->

                                <i class="fa fa-thumbs-o-up pr5 panel-icon"></i>

                                <strong>{__("Likes")}</strong>

                                <!-- panel title -->

                            </div>

                            <div class="panel-body">

                                {if $profile['user_id'] == $user->_data['user_id'] || $profile['user_privacy_pages'] == "public" || ($profile['user_privacy_pages'] == "friends" && $profile['we_friends'])}

                                    {if count($profile['pages']) > 0}

                                        <ul class="row">

                                            {foreach $profile['pages'] as $_page}

                                                {include file='__feeds_page.tpl' _tpl="profile_pages"}

                                            {/foreach}

                                        </ul>



                                        {if count($profile['pages']) >= $system['max_results_even']}

                                        <!-- see-more -->

                                        <div class="alert alert-info see-more js_see-more" data-get="profile_pages" data-uid="{$profile['user_id']}">

                                            <span>{__("See More")}</span>

                                            <div class="loader loader_small x-hidden"></div>

                                        </div>

                                        <!-- see-more -->

                                        {/if}

                                    {else}

                                        <p class="text-center text-muted mt10">

                                            {__("No pages to show")}

                                        </p>

                                    {/if}

                                {else}

                                    <p class="text-center text-muted mt10">

                                        {__("No pages to show")}

                                    </p>

                                {/if}





                            </div>

                        </div>

                    </div>

                    <!-- likes -->

                {elseif $view == "groups"}

                    <!-- groups -->

                    <div class="col-xs-12">

                        <div class="panel panel-default">

                            <div class="panel-heading with-icon">

                                <!-- panel title -->

                                <i class="fa fa-users pr5 panel-icon"></i>

                                <strong>{__("Groups")}</strong>

                                <!-- panel title -->

                            </div>

                            <div class="panel-body">

                                {if $profile['user_id'] == $user->_data['user_id'] || $profile['user_privacy_groups'] == "public" || ($profile['user_privacy_groups'] == "friends" && $profile['we_friends'])}

                                    {if count($profile['groups']) > 0}

                                        <ul class="row">

                                            {foreach $profile['groups'] as $_group}

                                                {include file='__feeds_group.tpl' _tpl="profile_groups"}

                                            {/foreach}

                                        </ul>



                                        {if count($profile['groups']) >= $system['max_results_even']}

                                            <!-- see-more -->

                                            <div class="alert alert-info see-more js_see-more" data-get="profile_groups" data-uid="{$profile['user_id']}">

                                                <span>{__("See More")}</span>

                                                <div class="loader loader_small x-hidden"></div>

                                            </div>

                                            <!-- see-more -->

                                        {/if}

                                    {else}

                                        <p class="text-center text-muted mt10">

                                            {__("No groups to show")}

                                        </p>

                                    {/if}

                                {else}

                                    <p class="text-center text-muted mt10">

                                        {__("No groups to show")}

                                    </p>

                                {/if}



                            </div>

                        </div>

                    </div>

                    <!-- groups -->

                {elseif $view == "events"}

                    <!-- events -->

                    <div class="col-xs-12">

                        <div class="panel panel-default">

                            <div class="panel-heading with-icon">

                                <!-- panel title -->

                                <i class="fa fa-calendar pr5 panel-icon"></i>

                                <strong>{__("Events")}</strong>

                                <!-- panel title -->

                            </div>

                            <div class="panel-body">

                                {if $profile['user_id'] == $user->_data['user_id'] || $profile['user_privacy_events'] == "public" || ($profile['user_privacy_events'] == "friends" && $profile['we_friends'])}

                                    {if count($profile['events']) > 0}

                                        <ul class="row">

                                            {foreach $profile['events'] as $_event}

                                                {include file='__feeds_event.tpl' _tpl="profile_events"}

                                            {/foreach}

                                        </ul>



                                        {if count($profile['events']) >= $system['max_results_even']}

                                            <!-- see-more -->

                                            <div class="alert alert-info see-more js_see-more" data-get="profile_events" data-uid="{$profile['user_id']}">

                                                <span>{__("See More")}</span>

                                                <div class="loader loader_small x-hidden"></div>

                                            </div>

                                            <!-- see-more -->

                                        {/if}

                                    {else}

                                        <p class="text-center text-muted mt10">

                                            {__("No events to show")}

                                        </p>

                                    {/if}

                                {else}

                                    <p class="text-center text-muted mt10">

                                        {__("No events to show")}

                                    </p>

                                {/if}



                            </div>

                        </div>

                    </div>

                    <!-- events -->

                {/if}

                <!-- view content -->

            </div>

            <!-- profile-content -->

        </div>



    </div>

</div>

<!-- page content -->

<!--Model-->
    <!-- post message -->
{include file='_addNewPost.tpl'}
<!--end-->
{if $profile['user_id'] == $user->_data['user_id']}
<a href="#postsocial" id="post-update-btn">
    <img src="/content/themes/default/images/post-icon.svg">
</a>
<div class="discard-popup">
    <div class="discard-container">
        <h3>Discard Post?</h3>
        <p>You can't undo this and you'll lose your draft.</p>
        <a href="javascript:void(0)" class="btn btn-primary discard-confirm">Yes, discard it</a>
        <a href="javascript:void(0)" id="discard-cancel">Cancel</a>
    </div>
</div>

<div class="modal fade" id="discard-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Discard this Post?</h3>
                <button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to discard this Tweet?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary discard-confirm">Discard</button>
            </div>
        </div>
    </div>
</div>

    {/if}
{literal}
<script type="text/javascript">

var ta = document.getElementById('postBox');
var dv = document.getElementById('postBoxDiv');


ta.addEventListener('focus', function() {
    autosize(ta);

    document.querySelector('.js_emoji-menu-toggle').style.display = 'block';
    document.querySelector('.text-counter').style.display = 'inline-block';
    document.querySelector('.js_publisher-photos').style.display = 'none';
    //document.querySelector('.publisher-footer').style.display = 'block';

    //var ht = parseInt(ta.style.height, 10) + 22;

});
ta.addEventListener('blur', function() {
    if (ta.value == '') {

    }
});

document.body.addEventListener('click', function(evt) {


    if (!$(evt.target).is('i.fa') && !$(evt.target).is('textarea')) {
        autosize.destroy(ta)
        //document.querySelector('.js_emoji-menu-toggle').style.display = 'none';
        document.querySelector('.text-counter').style.display = 'inline-block';
        document.querySelector('.js_publisher-photos').style.display = 'block';
        //document.querySelector('.publisher-footer').style.display = 'none';
    }
});

ta.addEventListener('keypress', function(){
 	//var ht = parseInt(ta.style.height, 10)+22;
	//alert(ht);
  	//dv.style.height = ht+'px';
});

$('#postsocial').on('shown.bs.modal', function (e) {
    $(".text-counter").css({"float":"none", "display":"inline-block"});
  tinymce.init({
                  selector:'#postBox',
                  theme: 'modern',
                  toolbar: false,
                  menubar: false,
                  branding: false,
                  statusbar: false,
                  forced_root_block : "",
                  setup: function (ed) {
                            ed.on('keyup', function (e) {
                                var count = CountCharacters();

                                if(count > 1000){
                                    document.getElementById("count_char").innerHTML = '<span style="color:red;">-'+count+'</span>'+'/1000';
                                }
                                else{
                                    document.getElementById("count_char").innerHTML = count+'/1000';
                                }

                            });

                            /*ed.on('focus',function(){
                              $('iframe').contents().find('#imThePlaceholder').remove();
                            });*/

                            ed.on( 'keypress', function(e) {
                                var count = CountCharacters();

                                if(count >= 1000){
                                    document.getElementById("count_char").innerHTML = '<span style="color:red;">-'+count+'</span>'+'/1000';
                                    tinymce.dom.Event.cancel(e);
                                }
                            } );


                            setInterval(function(e) {
                              var count = CountCharacters();

                                if(count >= 1000){
                                    document.getElementById("count_char").innerHTML = '<span style="color:red;">-'+count+'</span>'+'/1000';
                                    tinymce.dom.Event.cancel(e);
                                }

                                if(count > 1000){
                                    document.getElementById("count_char").innerHTML = '<span style="color:red;">-'+count+'</span>'+'/1000';
                                }
                                else{
                                    document.getElementById("count_char").innerHTML = count+'/1000';
                                }
                            }, 1000);
                        }

                    });
});

</script>
{/literal}

{include file='_footer.tpl'}
