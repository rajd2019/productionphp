<style type="text/css">
#modal2 .modal-body{
	overflow:unset;	
}
.lsx-emojipicker-container{
	top:0px;
	right:0px;
}
</style>
<div class="comment" data-handle="{$_handle}" data-id="{$_id}">

    <div class="comment-avatar">

        <a class="comment-avatar-picture" href="/{$user->_data['user_name']}" style="background-image:url({$user->_data['user_picture']});">

            </a>

    </div>

    <div class="comment-data">

        <div class="x-form comment-form">

            <div id="replaceTextarea_cmt_frm"><textarea dir="auto" class="js_autosize js_mention js_post-comment replyComment post_comment" id="wwww" name="commentBox" rows="1" placeholder='{__("Write a comment")}'></textarea></div>
            
            <div class="x-form-tools">

                <div class="x-form-tools-attach">

                    <i class="fa fa-camera js_x-uploader" data-handle="comment"></i>

                </div>

                <div class="x-form-tools-emoji js_emoji-menu-toggle picker3" style="overflow:visible">

                    <i class="fa fa-smile-o fa-lg"></i>

                </div>

                
            </div>

        </div>

        <div class="comment-attachments attachments clearfix x-hidden">

            <ul>

                <li class="loading">

                    <div class="loader loader_small"></div>

                </li>

            </ul>

        </div>

    </div>

</div>
<script type="text/javascript">



jQuery.fn.putCursorAtEndComment = function() {

          return this.each(function() {

            $(this).focus()

            // If this function exists...
            if (this.setSelectionRange) {
              // ... then use it (Doesn't work in IE)

              // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
              var len = $(this).val().length * 2;

              this.setSelectionRange(len, len);

            } else {
            // ... otherwise replace the contents with itself
            // (Doesn't work in Google Chrome)

              $(this).val($(this).val());

            }

            // Scroll to the bottom, in case we're in a tall textarea
            // (Necessary for Firefox and Google Chrome)
            this.scrollTop = 999999;

          });

    };
   
	$(function() {
		
		$('.picker3').lsxEmojiPicker({
			closeOnSelect: true,
			twemoji: true,
			onSelect: function(emoji){
				
				var cursorPosition = $("#wwww")[0].selectionStart;
				var FirstPart = $("#wwww").val().substring(0, cursorPosition);
				var NewText = emoji.value;
				var SecondPart = $("#wwww").val().substring(cursorPosition + 1, $("#wwww").val().length);
				
				//$(".replyComm_chk").html(FirstPart+NewText+SecondPart);
				
				var txt_placeholder = $("#wwww").attr('placeholder');
				
				$("#replaceTextarea_cmt_frm").html('<textarea dir="auto" class="js_autosize js_mention js_post-comment replyComment post_comment" id="wwww" name="commentBox" rows="1" placeholder="'+txt_placeholder+'">'+FirstPart+NewText+SecondPart+'</textarea>');
				$("#wwww").putCursorAtEndComment();
			}
		})

		

		
		
		
		   
        
          

	});

// $(function() {

// 	$('.picker33').lsxEmojiPicker({
			
// 			closeOnSelect: true,
// 			twemoji: true,
// 			onSelect: function(emoji){
				
				
// 			}
// 		});	

// });
	

				
</script>