<style>
.post-replace .post-text{
border-bottom:0px !important;
line-height:normal;
white-space: normal;
}
.post-replace .post-text p {
    margin-bottom: 0px !important;
    line-height: 22px !important;
}
.btn-translate {
    ffloat: none;
	display: block !important;
	margin: 0 0 10px;
	text-align: right;
}
a[data-readmore-toggle="rmjs-3"] {
    position: absolute;
}
.btn-translate span:hover {
    color: #2a78c2;
    text-decoration: underline;
}
.btn-translate span {
    color: #657786;
    text-decoration: none;
}
span.trans_by {
    font-size: 12px;
    margin: 9px 0 0;
    display: block;
    color: #000;
	font-family: 'Segoe UI',SegoeUI,"Helvetica Neue",Helvetica,Arial,sans-serif;
    font-weight: bold;
}
span.trans_by img {
    width: 12px;
    margin: -2px 0 0;
}
</style>
<div class="post-replace post-listing" data-id="{$post['post_id']}" >
    
    {if $_post['broadcast_name'] != ""}

    <div class="post-text js_readmore" dir="auto">{$_post['broadcast_name']}</div>
    
    {/if}

    <div class="post-text js_readmore rrr" dir="auto" data-id="{$post['post_id']}">{$post['text_plain']|unescape:'html'}</div>

    <div class="post-text-plain hidden" id="hidden-post-{$post['post_id']}">{$post['text_plain']}</div>

</div>

{if $is_ajax_post != 'Yes'}
<div class="btn-translate" data-id-translate="{$post['post_id']}"><i class="fa fa-globe" aria-hidden="true"></i> <span onclick="TranslateText({$post['post_id']})">Translate</span></div>
<div class="text-translate" style="display:none;" id="t-translate-{$post['post_id']}">
	<div class="post-media">
		<div class="post-media-meta">
		<!-- Start Translate Text Via Microsoft Translate API -->
			<div class=".post-text" id="translated-text-{$post['post_id']}" style="text-align:justify;"></div>			
		<!-- End Translate Text Via Microsoft Translate API -->	
		<span class="trans_by" id="translated-by-{$post['post_id']}" style="display:none;"><span style="font-weight:normal;">Translated by</span> <img src="/content/themes/default/images/microsoft_PNG20.png"> Microsoft</span>
		<div class="loader loader_middium" id="translate-loader-{$post['post_id']}" style="display:none;"></div>
		</div>
	</div>
</div>
<input type="hidden" id="translate-toggle-val-{$post['post_id']}" value="0">
{/if}