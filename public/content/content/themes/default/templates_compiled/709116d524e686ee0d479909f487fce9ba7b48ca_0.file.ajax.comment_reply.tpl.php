<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:48:07
  from "/var/app/current/content/themes/default/templates/ajax.comment_reply.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c587ad72d0b83_73770903',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '709116d524e686ee0d479909f487fce9ba7b48ca' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/ajax.comment_reply.tpl',
      1 => 1536745028,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c587ad72d0b83_73770903 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style type="text/css">
.body{
	position:fixed;	
}
.modal{
	position:fixed;	
}
</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 class="modal-title"><?php echo __("Reply Comment");?>
</h5>
</div>
    <div class="modal-body post-hover-content">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['posts']->value, '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
        <div class="post " data-id="<?php echo $_smarty_tpl->tpl_vars['_user']->value['post_id'];?>
"> 
            <div class="post-section">
                <div class="post-body"> 
                    <div class="post-header"> 
                        <div class="post-avatar"><img src="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
" /></div> 
                        <div class="post-meta">
                        <div>
                        <span>
                            <span class="first-name-custom"><?php echo $_smarty_tpl->tpl_vars['_user']->value["name"];?>
</span> 
                            <span class="second-name-custom">@<?php echo $_smarty_tpl->tpl_vars['_user']->value["user_name"];?>
</span>
                        </span>
                         <div class="post-time"> <span class="js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['_user']->value['time'];?>
"><?php echo $_smarty_tpl->tpl_vars['_user']->value["time"];?>
</span>
                         </div>
                         <span class="post-title"> </span> 
                        <div class="post-replace">
                            
                            <div class="post-text js_readmore" dir="auto" data-id="<?php echo $_smarty_tpl->tpl_vars['_user']->value['post_id'];?>
"><?php echo htmlspecialchars_decode($_smarty_tpl->tpl_vars['_user']->value['text'], ENT_QUOTES);?>
</div>
                            <div class="post-text-plain hidden"><?php echo $_smarty_tpl->tpl_vars['_user']->value['text_plain'];?>
</div>
                        </div>  
                        </div>
                        </div>
                         
                    </div>
                </div>
            </div>
        </div>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </div>

    <div class="post_custom">
        <div class="form-group">
            <input type="hidden" name="do" value="share">
            <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" class="postId">
            
            <div id="replaceTextarea"><textarea class="expand replyComment  replyComm_chk post_comment form-control" rows="1" cols="50" value="<?php echo $_smarty_tpl->tpl_vars['album']->value['title'];?>
" placeholder='<?php echo __("Add a comment");?>
....' name="comment" id="post_comment_box_id"></textarea></div>
			
			<span class="publisher-tools-attach js_publisher-photos newalbumClass picker2" style="overflow:visible">
            <i class="fa fa-smile-o fa-fw"></i>
			</span>
        </div>
    </div>
    <div class="modal-footer">        
        <a class="btn btn-primary js_comment-reply reply_com_btn" data-id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" data-handle="<?php echo $_smarty_tpl->tpl_vars['_handle']->value;?>
"><?php echo __("Reply");?>
</a>
    </div>
<?php echo '<script'; ?>
 type="text/javascript">

jQuery.fn.putCursorAtEnd = function() {

          return this.each(function() {

            $(this).focus()

            // If this function exists...
            if (this.setSelectionRange) {
              // ... then use it (Doesn't work in IE)

              // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
              var len = $(this).val().length * 2;

              this.setSelectionRange(len, len);

            } else {
            // ... otherwise replace the contents with itself
            // (Doesn't work in Google Chrome)

              $(this).val($(this).val());

            }

            // Scroll to the bottom, in case we're in a tall textarea
            // (Necessary for Firefox and Google Chrome)
            this.scrollTop = 999999;

          });

};




$(document).on('keyup', '.replyComm_chk', function(event) {
    text_comment_reply();
});


function text_comment_reply(){
    var reply_comm=$('textarea.replyComm_chk').val();
        
    var comm_leng=reply_comm.length;
    text_counter= comm_leng;       
        
    if(text_counter==0){
        $("a.reply_com_btn").attr("disabled",true);
        return true;
    }else{
        $("a.reply_com_btn").attr("disabled",false);
        return false;
    }
}
	

$(function() {
    text_comment_reply();

	$('.picker2').lsxEmojiPicker({
		closeOnSelect: true,
		twemoji: true,
		onSelect: function(emoji){
				
			var cursorPosition = $(".replyComm_chk")[0].selectionStart;
			var FirstPart = $(".replyComm_chk").val().substring(0, cursorPosition);
			var NewText = emoji.value;
			var SecondPart = $(".replyComm_chk").val().substring(cursorPosition + 1, $(".replyComm_chk").val().length);
				
				
			var txt_placeholder = $(".replyComm_chk").attr('placeholder');
				
			$("#replaceTextarea").html('<textarea class="expand replyComment  replyComm_chk post_comment form-control" rows="1" cols="50" value="" placeholder="'+txt_placeholder+'" name="comment" id="post_comment_box_id">'+FirstPart+NewText+SecondPart+'</textarea>');

            $("#post_comment_box_id").putCursorAtEnd();
            text_comment_reply();
		}
	});		
});
<?php echo '</script'; ?>
><?php }
}
