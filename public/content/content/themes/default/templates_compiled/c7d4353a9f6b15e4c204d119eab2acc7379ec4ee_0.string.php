<?php
/* Smarty version 3.1.31, created on 2019-02-04 18:08:58
  from "c7d4353a9f6b15e4c204d119eab2acc7379ec4ee" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c587fba3381d7_50652306',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c587fba3381d7_50652306 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="content_static">
  <h3 class="text-info"> Personal Data (Privacy) Policy Statement (“Privacy Policy”) </h3>
  <p>China Golden Spring Group (Hong Kong) Limited (“Company”) is based in Hong Kong and pledges to meet fully with the requirements of the Personal Data (Privacy) Ordinance, Chapter 486 of the Laws of Hong Kong. In doing so, the Company will ensure compliance by its staff to the strictest standards of security and confidentiality in respect of all personal information and data submitted by users via GUO.MEDIA and its sub-domains (collectively, “Sites”) and the Company will not release such information to anyone without the prior consent of the relevant user(s) of the Sites (whether registered or not) (“User(s)”) except to the authorized persons listed under paragraph 3 below. </p>
  <p>Users are strongly recommended to read this Privacy Policy carefully to have an understanding of the Company’s policy and practices with regard to the treatment of personal information and data provided by the Users on the Sites. This Privacy Policy is applicable to both registered and non-registered Users, and the terms contained herein may be updated, revised, varied and/or amended from time to time.</p>
  <p>If Users have questions or concerns regarding this Privacy Policy, they should email to support@guo.media.</p>
  <ol type="1">
    <li>
      <h3 class="text-info">Purpose of Collection of Personal Data</h3>
      <p>In the course of using the Sites, Users may disclose or be asked to provide personal information and/or data. In order to have the benefit of and enjoy various services offered by the Sites, it may be necessary for Users to provide the Company with their personal information and/or data. Although Users are not obliged to provide the information and/or data as requested on the Sites, the Company will not be able to render certain services on the Sites in the event that Users fail to do so.</p>
      <p>The Company’s purposes for collection of information and data on the Sites include but not limited to the following:</p>
      <ol type="a">
        <li>for the daily operation of the services provided to Users;</li>
        <li>to identify Users who have posted advertisements, materials, messages, photos, views or comments or such other information (collectively “Information”) on the Sites;</li>
        <li>to identify Users who have viewed the Information posted on the Sites;</li>
        <li>to provide Users with marketing and promotional materials for their enjoyment of benefits as members of the Sites (for further details, please refer to paragraph 4 headed “Subscription of Newsletter/Promotional Materials/Marketing Materials” below);</li>
        <li>to identify Users who have enjoyed their benefits as members of the Sites by receiving and using marketing and promotional materials;</li>
        <li>to provide Users with a platform and forum for posting photos, sharing and discussing their insights;</li>
        <li>to allow members of the Sites to enjoy their benefits as members by enrolling for special events hosted by the Company and/or its affiliates; </li>
        <li>to design and provide products and services to Users in relation to the above purposes; </li>
        <li>to compile aggregate statistics about the Users and to analyze the service usage of the Sites for the Company’s internal use; and</li>
        <li>to facilitate the Company and/or its affiliates to use the Users’ personal data for purposes relating to the provision of services offered by the Company and marketing services, special events and/or promotions of the Company, its affiliates and/or their respective clients.</li>
      </ol>
      <p>If the User is under the age of 13, the Company would strongly recommend him/her to seek prior consent from a person with parental responsibility for him/her, e.g. parent or guardian, who may contact the responsible personnel of the Company at support@guo.media for registering the User as member of the Sites.</p>
      <p>The Company strives to only collect personal data of Users which is necessary for the purposes set out hereinabove.</p>
      <p>The Company may use the Users’ personal data for a purpose other than those set out hereinabove. The Company will obtain consent from the Users for such use prior to the use.  If User is a minor, the prescribed consent should be given by his/her parent or guardian.</p>
    </li>
    <li>
      <h3 class="text-info">Collection of Personal Data</h3>
      <p>The Company may collect personal information and/or data about a User such as his/her name, log-in ID and password, address, email address, phone number, age, sex, date of birth, country of residence, nationality, education level and work experience that is/are not otherwise publicly available. Occasionally, the Company may also collect additional personal information and/or data from a User in connection with contests, surveys, or special offers.</p>
      <p>Only duly authorized staff of the Company will be permitted to access the Users’ personal information and data, and the Company shall not release such personal information and data to any third parties save and except for the circumstances listed out under the Paragraph 3. entitled “Disclosure or Transfer of Data”.</p>
    </li>
    <li>
      <h3 class="text-info">Disclosure or Transfer of Data</h3>
      <p>The Company agrees to take all practicable steps to keep all personal information and data of Users confidential and/or undisclosed, subject to the following.</p>
      <p>Generally speaking, the Company will only disclose and/or transfer Users’ personal information and/or data to the Company’s personnel and staff for the purpose of providing services to Users. However, the Company may disclose and/or transfer such information and/or data to third parties under the following circumstances:</p>
      <ol type="a">
        <li>where the information and/or data is disclosed and/or transferred to any third party suppliers or external service providers who have been duly authorized by the Company to use such information and/or data and who will facilitate the services on the Sites, under a duty of confidentiality;</li>
        <li>where the information and/or data is disclosed and/or transferred to any agents, affiliates or associates of the Company who have been duly authorized by the Company to use such information and/or data;</li>
        <li>where the Company needs to protect and defend its rights and property;</li>
        <li>where the Company considers necessary to do so in order to comply with the applicable laws and regulations, including without limitation compliance with a judicial proceeding, court order, or legal process served on the Sites; and</li>
        <li>where the Company deems necessary in order to maintain and improve the services on the Sites.</li>
      </ol>
      <p>Personal data collected via the Sites may be transferred, stored and processed in any country in which the Company or its affiliates operate. By using the Sites, Users are deemed to have agreed to, consented to and authorized the Company to disclose and/or transfer their personal information and data under the circumstances stated above, as well as to any transfer of information (including the Information) outside of the Users’ country.</p>
    </li>
    <li>
      <h3 class="text-info">Subscription of Newsletter/Promotional Materials/Marketing Materials</h3>
      <p>The Company and its affiliates may from time to time send to members and Users of the Sites newsletters, promotional materials and marketing materials based on the personal information and data that they have provided to the Company. The Company may use Users’ data in direct marketing and the Company requires the Users’ consent (which includes an indication of no objection) for that purpose. In this connection, please note that:</p>
      <ol type="a">
        <li>the name, log-in ID and password, contact details, age, sex, date of birth, country of residence, nationality, education level and work experience of Users held by the Company from time to time may be used by the Company and/or its authorised personnel or staff in direct marketing;</li>
        <li>the following classes of services, products and subjects may be marketed:
          <ol type="i">
            <li>special events hosted by the Company and its affiliates for members and Users, including but not limited to courses, workshops, and competitions;</li>
            <li>reward, loyalty or privileges programmes and related products and services;</li>
            <li>special offers including coupons, discounts, group purchase offers and promotional campaigns;</li>
            <li>products and services offered by the Company’s affiliates and advertisers (the names of such affiliates and advertisers can be found in the relevant advertisements and/or promotional or marketing materials for the relevant products and services, as the case may be);</li>
            <li>donations and contributions for charitable and/or non-profit making purposes;</li>
          </ol>
        </li>
        <li>The above products, services and subjects may be provided or (in the case of donations and contributions) solicited by the Company and/or:
          <ol type="i">
            <li>the Company’s affiliates;</li>
            <li>third party service providers providing the products, services and subjects listed in paragraph (b) above; and</li>
            <li>charitable or non-profit marking organizations;</li>
            <li>in addition to marketing the above services, products and subject itself, the Company also intends to provide the data described in paragraph (a) above to all or any of the persons described in paragraph (c) above for use by them in marketing those services, products and subjects, and the Company requires the Users’ written consent (which includes an indication of no objection) for that purpose;</li>
            <li>the Company may receive money or other property in return for providing the data to the other persons in paragraph (d) above and, when requesting the Users’ written consent as described in paragraph (d) above, the Company will inform the Users if the Company receives any money or other property in return for providing the data to the other persons.</li>
          </ol>
          Suitable measures are implemented to make available to such members the options to “opt-out” of receiving such materials. In this regard, Users may choose to sign up or unsubscribe for such materials by logging into the registration or user account maintenance webpage, or clicking on the automatic link appearing in each newsletter/message, or contact us by sending email to support@guo.media. </li>
      </ol>
    </li>
    <li>
      <h3 class="text-info">Access</h3>
      <p>Any User is entitled to request access to or make amendments to his/her own personal information and data kept with the Company by contacting us via email to support@guo.media. </p>
      <p>In the event that a User wishes to access or amend his/her personal information and data, the Company may request him/her to provide personal details in order to verify and confirm his/her identity. HKID card number or passport number or business registration certificate number cannot be amended unless such data is proved to be inaccurate. The Company is required to respond to a User’s requests within 40 days of his/her request and will endeavor to do so wherever possible.</p>
    </li>
    <li id="cookie">
      <h3 class="text-info"> Cookies and Log Files</h3>
      <p>The Company does not collect any personally identifiable information from any Users whilst they visit and browse the Sites, save and except where such information of the Users is expressly requested. When Users access the Sites, the Company records their visits only and do not collect their personal information or data. The Sites’ server software will also record the domain name server address and track the pages the Users visit and store such information in “cookies”, and gather and store information like internet protocol (IP) addresses, browser type, referring/exit pages, operating system, date/time stamp, and clickstream data in log files. All these are done without the Users being aware that they are occurring.</p>
      <p>The Company does not link the information and data automatically collected in the above manner to any personally identifiable information. The Company generally uses such automatically collected information and data to estimate the audience size of the Sites, gauge the popularity of various parts of the Sites, track Users’ movements and number of entries in the Company’s promotional activities and special events, measure Users’ traffic patterns and administer the Sites. Such automatically collected information and data will not be disclosed save and except in accordance with the Paragraph 3 entitled “Disclosure or Transfer of Data”. </p>
    </li>
    <li>
      <h3 class="text-info">Links to Other Websites</h3>
      <p>The Sites may provide links to other websites which are not owned or controlled by the Company. Personal information and data from Users may be collected on these other websites when Users visit such websites and make use of the services provided therein. Where and when Users decide to click on any advertisement or hyperlink on the Sites which grants Users access to another website, the protection of Users’ personal information and data which are deemed to be private and confidential may be exposed in these other websites.</p>
      <p>Non-registered Users who gain access to the Sites via their accounts in online social networking tools (including but not limited to Facebook, Twitter, etc.) are deemed to have consented to the terms of this Privacy Policy, and such Users’ personal data which they have provided to those networking tools may be obtained by the Company and be used by the Company and its authorized persons in and outside of the User’s country for the purpose of providing services and marketing materials to the Users. The Company and its authorized personnel may gain access to and use these Users’ personal data so obtained, subject to the other provisions of this Privacy Policy.</p>
      <p>This Privacy Policy is only applicable to the Sites. Users are reminded that this Privacy Policy grants no protection to Users’ personal information and data that may be exposed on websites other than the Sites, and the Company is not responsible for the privacy practices of such other websites. Users are strongly recommended to refer to the privacy policy of such other websites.</p>
    </li>
    <li>
      <h3 class="text-info">Security</h3>
      <p>The security of Users’ personal information and data is important to the Company. The Company will always strive to ensure that Users’ personal information and data will be protected against unauthorized access. The Company has implemented appropriate electronic and managerial measures in order to safeguard, protect and secure Users’ personal information and data.</p>
      <p>All personal information and data provided by Users are only accessible by the authorized personnel of the Company or its authorized third parties, and such personnel shall be instructed to observe the terms of this Privacy Policy when accessing such personal information and data. Users may rest assured that their personal information and data will only be kept for as long as is necessary to fulfill the purpose for which it is collected.  Registered Users should safeguard his/her unique Username and Password by keeping it secret and confidential and never share these details with anyone.</p>
      <p>The Company uses third party payment gateway service providers to facilitate electronic transactions on the Sites.  Regarding sensitive information provided by Users, such as credit card number for completing any electronic transactions, the web browser and third party payment gateway communicate such information using secure socket layer technology (SSL).</p>
      <p>The Company follows generally accepted industry standards to protect the personal information and data submitted by Users to the Sites, both during transmission and once the Company receives it. However, no method of transmission over the Internet, or method of electronic storage, is 100% secure. Therefore, while the Company strives to protect Users’ personal information and data against unauthorized access, the Company cannot guarantee its absolute security.</p>
    </li>
    <li>
      <h3 class="text-info">Retention of Personal Data</h3>
      <p>Once the Company has obtained a User’s personal information and/or data, it will be maintained securely in the Company’s system. Subject to legal requirements, the personal information and/or data of Users will be retained by the Company after deactivation of the relevant service until the User requests the Company in writing to erase his/her own personal information and/or data from the Company's database or to terminate his/her membership of the Sites.</p>
    </li>
    <li>
      <h3 class="text-info">Changes in this Privacy Policy </h3>
      <p>The Company reserves the right to update, revise, modify or amend this Privacy Policy in the following manner at any time as the Company deems necessary and Users are strongly recommended to review this Privacy Policy frequently. If the Company decides to update, revise, modify or amend this Privacy Policy, the Company will post those changes to this webpage and/or other places the Company deems appropriate so that Users would be aware of what information the Company collects, how the Company uses it, and under what circumstances, if any, the Company discloses it.</p>
      <p>If the Company makes material changes to this Privacy Policy, the Company will notify Users on this webpage, by email, or by means of a notice on the home page of the Company.</p>
      <p>For any query, please contact us by sending email to support@guo.media.</p>
      <p>Effective: 23rd December 2017</p>
    </li>
  </ol>
</div><?php }
}
