<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:12:16
  from "/var/app/current/content/themes/default/templates/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c587270899380_06068017',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6decd337ac509acbf51706cb9b77c19b9039a928' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/index.tpl',
      1 => 1543292066,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_footer_login.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:_sidebar.tpl' => 1,
    'file:_announcements.tpl' => 1,
    'file:_publisher.tpl' => 1,
    'file:_boosted_post.tpl' => 1,
    'file:_posts.tpl' => 4,
    'file:_ads.tpl' => 1,
    'file:_widget.tpl' => 1,
    'file:__feeds_user.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_5c587270899380_06068017 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<style type="text/css">

	#newFeatureModal {
		background: rgba(0,0,0,0.5);
	}
	#newFeatureModal .modal-dialog {
		max-width: 90%;
		margin: auto !important;
		transform: translateY(-50%);
		top: 50%;
	}
	#newFeatureModal li {
	   position: relative;
	}
	#newFeatureModal ol li::before {
	   content: "\f0da";
	   display: block;
	   height: 0;
	   width: 0;
	   left: -1em;
	   font-family: FontAwesome;
	   font-size: 20px;
	   line-height: 20px;
	   position: absolute;
	}
    /*8oct*/
    .upload_share {
        width: 100%;
        float: left; 
        position:relative; 
        overflow:hidden;
    }
    .upload_share .x-uploader {
        overflow: visible;  
        width: 100%;  
        float: left; 
        position: static;
    }
    .upload_share .x-uploader input[type=file] { 
        font-size: 0;
    }
    .upload_share .publisher-tools-attach { 
        position: absolute; 
        overflow: visible; 
        left: 0; 
        top: 0; 
        width: 100%; 
        opacity: 0; 
        height: 100%;
    }
    .upload_share i.fa-video-camera {
         display:none;
    }
    .upload_share .publisher-tools-attach {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
	</style>
<?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet"> 
	<link rel="stylesheet" href="/content/themes/default/css/login/style.css">
	<link rel="stylesheet" href="/content/themes/default/css/login/jquery.bxslider.css">
	 
	  <div class="twitter-main">
            <div class="twitter-header">
               <div class="container">
                  <div class="top-box">
              <div class="left-section">
              <a href="/" class="logo"><img src="/content/themes/default/images/brand.svg" alt="guo.media" title="guo.media" width="135"></a>
               <ul>
                   
                  <li><a href=""><i class="Icon Icon--home Icon--large"></i><?php echo __("Home");?>
</a></li>
                  <!--<li><a href="/static/about">About</a></li>-->
                  <li><a href="/milesguo"><i class="hand_icon"><img src="/content/themes/default/images/hand.svg" alt="hand" width="21"></i><?php echo __("Guo");?>
</a></li>
               
               </ul>
               </div>
              <div class="right-section">
			  <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
              
                        <span class="text-link" data-toggle="modal" data-url="#translator">
                        <!-- <span class="flag-icon flag-icon-<?php echo $_smarty_tpl->tpl_vars['system']->value['language']['flag_icon'];?>
"></span> -->
						 <?php echo $_smarty_tpl->tpl_vars['system']->value['language']['title'];?>
</span>
						 <span class="contact-icon watch_broadcasts_logout">
                            <a href="/broadcasts">
                                 <span class="Icon icon_custom Icon--cameraVideo Icon--small"></span> <?php echo __("GuoTV");?>

                            </a>
                        </span>
                        <a href="/broadcasts" <?php if ($_smarty_tpl->tpl_vars['last_dir']->value == 'broadcasts') {?> class="active" <?php }?>><span class="broadcasts_icon Icon Icon--cameraVideo Icon--large"></span></a>
                        <?php }?>
               
              </div>
              </div>
               </div>
            </div>
            
            <!--<div class="twitter-header twitter_header_mobile">
               <div class="container">
                  <div class="top-box">
                  
                  <div class="left-section">
                   <ul>
                       
                      <li class="back"><a href="javascript:void(0)" id="back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a></li>
                      <li  class="none_link"><a href="javascript:void(0)" id="signup_show">Sign up</a></li>
                      <li  class="none_link"><a href="/milesguo" id="signup_show">Miles Guo</a></li>
                   
                   </ul>
                   </div>
                   
                  <a href="/" class="logo"><img src="/content/themes/default/images/logo.jpg" alt="guo.media" title="guo.media" width="80"></a>
                   
                  <div class="right-section">
                  <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                            <span class="text-link" data-toggle="modal" data-url="#translator">
                            <span class="flag-icon flag-icon-<?php echo $_smarty_tpl->tpl_vars['system']->value['language']['flag_icon'];?>
"></span>
                             <?php echo $_smarty_tpl->tpl_vars['system']->value['language']['title'];?>
</span>
                            <?php }?>
                   
                  </div>
                  
              </div>
               </div>
            </div>-->
            
            
            <div class="slider-main-box">
                <div class="banner-section">
                       <div class="main-slider">
                            <div class="slide">
                               <div class="featured-img"><img src="/content/uploads/files/GUO MEDIA 1.jpg" alt="GUO MEDIA 1"></div>
                             </div>
                           
							<div class="slide">
                               <div class="featured-img"><img  src="/content/uploads/files/GUO MEDIA 2.jpg" alt="GUO MEDIA 2"></div>
                             </div>
                             <div class="slide">
                               <div class="featured-img"><img  src="/content/uploads/files/GUO MEDIA 3.jpg"  alt="GUO MEDIA 3"></div>
                             </div> 
                        </div>
              </div>
              <div class="banner-text-section">
              <div class="container">
                  <div class="row">
                     <div class="col-sm-12 col-md-6 col-lg-8">
                        <div class="banner-text-inner">
						
						   <?php if ($_smarty_tpl->tpl_vars['system']->value['language']['code'] == 'zn-tw') {?>
                           <h1>郭媒體 GUO.MEDIA</h1>
						   <?php } else { ?>
						    <h1>郭媒体 GUO.MEDIA</h1>
						   <?php }?>
                            <h2><?php echo __("Everything is just beginning");?>
 <i class="fa fa-hand-grab-o" aria-hidden="true"></i> <i class="fa fa-hand-grab-o" aria-hidden="true"></i> <i class="fa fa-hand-grab-o" aria-hidden="true"></i> !</h2>
           <p><?php echo __("GUO.MEDIA is now launching our beta version. We strive to provide a free social networking and livestreaming platform where everyone can express their views and opinions freely.");?>
</p>
                        
                        
                        </div>
                       
                      <div class="login_footer"> 
                     	
<!-- footer -->
<div class="container">

	<div class="row footer">
		<div class="col-lg-6 col-md-6 col-sm-6">
			&copy; <?php echo date('Y');?>
 <?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 · <span class="text-link" data-toggle="modal" data-url="#translator"><span class="flag-icon flag-icon-<?php echo $_smarty_tpl->tpl_vars['system']->value['language']['flag_icon'];?>
"></span>&nbsp;<?php echo $_smarty_tpl->tpl_vars['system']->value['language']['title'];?>
</span>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 links">
			<?php if (count($_smarty_tpl->tpl_vars['static_pages']->value) > 0) {?>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['static_pages']->value, 'static_page', true);
$_smarty_tpl->tpl_vars['static_page']->iteration = 0;
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['static_page']->value) {
$_smarty_tpl->tpl_vars['static_page']->iteration++;
$_smarty_tpl->tpl_vars['static_page']->last = $_smarty_tpl->tpl_vars['static_page']->iteration == $_smarty_tpl->tpl_vars['static_page']->total;
$__foreach_static_page_0_saved = $_smarty_tpl->tpl_vars['static_page'];
?>
					<a href="/static/<?php echo $_smarty_tpl->tpl_vars['static_page']->value['page_url'];?>
">
						<?php echo __(((string)$_smarty_tpl->tpl_vars['static_page']->value['page_title']));?>

					</a><?php if (!$_smarty_tpl->tpl_vars['static_page']->last) {?> · <?php }?>
				<?php
$_smarty_tpl->tpl_vars['static_page'] = $__foreach_static_page_0_saved;
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['system']->value['contact_enabled']) {?>
				 · 
				<a href="/contacts">
					<?php echo __("Contacts");?>

				</a>
			<?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['system']->value['market_enabled']) {?>
                 · 
                <a href="/market">
                    <?php echo __("Market");?>

                </a>
            <?php }?>
		</div>
	</div>
</div>
						 </div>
<!-- footer -->
                     </div>
                     <div class="col-sm-12 col-md-6 col-lg-4">
					 <form class="js_ajax-forms" data-url="core/signin.php" method="post">
                        <div class="login-section">
                           <div class="form-box">
							    <input name="username_email" type="text" placeholder='<?php echo __("Email");?>
 <?php echo __("or");?>
 <?php echo __("Username");?>
'  value="<?php if ($_smarty_tpl->tpl_vars['username_email']->value) {
echo $_smarty_tpl->tpl_vars['username_email']->value;
}?>" required>
                              <input name="password" type="password" placeholder='<?php echo __("Password");?>
' value="" required>
							  <input type="submit" value='<?php echo __("Login");?>
'/>
                           </div>
                          <div class="check-box">

                          <input name="remember"  type="checkbox" <?php if ($_smarty_tpl->tpl_vars['username_email']->value) {?>checked<?php }?>>
                          <label for="remember"><?php echo __("Remember me");?>
</label>
                              | <a href="/reset"><?php echo __("Forgotten password?");?>
</a> </div>
                        </div>
						<!-- error -->
						<div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
						<!-- error -->
                        </form>

                        <div class="sign-section">
                           <h4><?php echo __("Sign up");?>
</h4>
                           <div class="form-box">
						     <form class="js_ajax-forms" data-url="core/signup.php" method="post">
							 
                               <input type="text" value="" placeholder="<?php echo __("Full name");?>
" name="first_name" required/>
							 
                               <input type="text" value="" placeholder='<?php echo __("Username");?>
' name="username" />
                             <!--   <input type="email" placeholder='<?php echo __("Email");?>
'  name="email" required/> -->
							<?php if ($_smarty_tpl->tpl_vars['system']->value['activation_enabled'] && $_smarty_tpl->tpl_vars['system']->value['activation_type'] == "sms") {?>
							   <input name="phone" type="text" placeholder='<?php echo __("Phone number (eg. +905...)");?>
' required>
							  <?php }?>
                              <input name="password" type="password" placeholder='<?php echo __("Password");?>
' required/>
							  <?php if ($_smarty_tpl->tpl_vars['system']->value['reCAPTCHA_enabled']) {?>                                 
                                        
									<div class="col-sm-12" style="margin-bottom:12px;">
										<!-- reCAPTCHA -->
										<?php echo '<script'; ?>
 src='https://www.google.com/recaptcha/api.js'><?php echo '</script'; ?>
>
										<div class="g-recaptcha" data-sitekey="<?php echo $_smarty_tpl->tpl_vars['system']->value['reCAPTCHA_site_key'];?>
"></div>
										<!-- reCAPTCHA -->
									</div>
                                  
                                <?php }?>
							   <!-- error -->
								<div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
								<!-- error -->
                              <input type="submit" value='<?php echo __("Sign Up");?>
'/>
							  </form>
                              
                              <div class="befound"><p><?php echo __("By signing up, you agree to our");?>
 <a href="/static/terms"><?php echo __("Terms of Service");?>
</a> <?php echo __("and");?>
 <a href="/static/privacy"><?php echo __("Privacy Policy");?>
</a>, <?php echo __("including");?>
 <a href="/static/privacy#cookie"><?php echo __("Cookie Use");?>
</a> </p></div>
                              
                           </div>
                          
                        </div>
                        
                        <div class="google_link mobile_google_link">
                        <a href="https://play.google.com/store/apps/details?id=com.media.guo"><img  src="/content/themes/default/images/google-play.png" alt="google-play"></a>
                        
                        </div>
                       
                     </div>
                      </div>
                  
                  </div>
                
              </div>
           </div>
    </div>
	<div class="overlay"></div>
	<?php $_smarty_tpl->_subTemplateRender('file:_footer_login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <!-- page content -->
	
<?php } else { ?>
	<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <!-- page content -->
    <div class="container mt20 offcanvas">
        <div class="row">

            <!-- left panel -->
            <div class="col-sm-4 col-md-3 offcanvas-sidebar">
                <?php $_smarty_tpl->_subTemplateRender('file:_sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
            <!-- left panel -->

            <div class="col-sm-8 col-md-9 offcanvas-mainbar">
                <div class="row">
                    <!-- center panel -->
                    <div class="col-sm-12 col-md-8">

                        <!-- announcments -->
                        <?php $_smarty_tpl->_subTemplateRender('file:_announcements.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                        <!-- announcments -->

                        <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>

                         
                            <!-- stories -->

                            <!-- publisher -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_publisher.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_handle'=>"me",'_privacy'=>true), 0, false);
?>

                            <!-- publisher -->
                             
                            <!-- boosted post -->
                            <?php if ($_smarty_tpl->tpl_vars['boosted_post']->value) {?>
                                <?php $_smarty_tpl->_subTemplateRender('file:_boosted_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('post'=>$_smarty_tpl->tpl_vars['boosted_post']->value), 0, false);
?>

                            <?php }?>
                            <!-- boosted post -->

                            <!-- posts stream -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_posts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>"newsfeed"), 0, false);
?>

                            <!-- posts stream -->

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "saved") {?>
                            <!-- saved posts stream -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_posts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>"saved",'_title'=>__("Saved Posts")), 0, true);
?>

                            <!-- saved posts stream -->

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "articles") {?>
                            <!-- saved posts stream -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_posts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>"posts_profile",'_id'=>$_smarty_tpl->tpl_vars['user']->value->_data['user_id'],'_filter'=>"article",'_title'=>__("My Articles")), 0, true);
?>

                            <!-- saved posts stream -->

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "products") {?>
                            <!-- saved posts stream -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_posts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>"posts_profile",'_id'=>$_smarty_tpl->tpl_vars['user']->value->_data['user_id'],'_filter'=>"product",'_title'=>__("My Products")), 0, true);
?>

                            <!-- saved posts stream -->

                        <?php }?>
                    </div>
                    <!-- center panel -->

                    <!-- right panel -->
                    <div class="col-sm-12 col-md-4">
                        <!-- pro members -->
                        <?php if (count($_smarty_tpl->tpl_vars['pro_members']->value) > 0) {?>
                            <div class="panel panel-default panel-friends">
                                <div class="panel-heading">
                                    
                                    <strong class="text-primary"><i class="fa fa-bolt"></i> <?php echo __("Pro Users");?>
</strong>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pro_members']->value, '_member');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_member']->value) {
?>
                                            <div class="col-xs-4">
                                                <a class="friend-picture" href="/<?php echo $_smarty_tpl->tpl_vars['_member']->value['user_name'];?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['_member']->value['user_picture'];?>
);" >
                                                    <span class="friend-name"><?php echo $_smarty_tpl->tpl_vars['_member']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['_member']->value['user_lastname'];?>
@<?php echo $_smarty_tpl->tpl_vars['_member']->value['user_name'];?>
</span>
                                                </a>
                                            </div>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </div>
                                </div>
                            </div>
                        <?php }?>
                        <!-- pro members -->

                        <!-- boosted pages -->
                        <?php if (count($_smarty_tpl->tpl_vars['promoted_pages']->value) > 0) {?>
                            <div class="panel panel-default panel-friends">
                                <div class="panel-heading">
                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['packages_enabled'] && !$_smarty_tpl->tpl_vars['user']->value->_data['user_subscribed']) {?>
                                        <div class="pull-right flip">
                                            <small><a href="/packages"><?php echo __("Upgrade");?>
</a></small>
                                        </div>
                                    <?php }?>
                                    <strong class="text-primary"><i class="fa fa-bullhorn fa-fw"></i> <?php echo __("Pro Pages");?>
</strong>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['promoted_pages']->value, '_page');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_page']->value) {
?>
                                            <div class="col-xs-4">
                                                <a class="friend-picture" href="/pages/<?php echo $_smarty_tpl->tpl_vars['_page']->value['page_name'];?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['_page']->value['page_picture'];?>
);" >
                                                    <span class="friend-name"><?php echo $_smarty_tpl->tpl_vars['_page']->value['page_title'];?>
</span>
                                                </a>
                                            </div>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </div>
                                </div>
                            </div>
                        <?php }?>
                         <!-- boosted pages -->

                        <?php $_smarty_tpl->_subTemplateRender('file:_ads.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:_widget.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


                        <!-- people you may know -->
                        
                            <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <div class="pull-right flip">
                                        <small><a href="/people"><?php echo __("View All");?>
</a></small>
                                    </div>
                                    <strong class="wtf-module"><?php echo __("Who to follow");?>
</strong>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                    <?php if (count($_smarty_tpl->tpl_vars['user']->value->_data['new_people']) > 0) {?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['user']->value->_data['new_people'], '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
                                        <?php $_smarty_tpl->_subTemplateRender('file:__feeds_user.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_connection'=>"add",'_small'=>true), 0, true);
?>

                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                     <?php }?>
                                     <?php if (count($_smarty_tpl->tpl_vars['user']->value->_data['new_people']) <= 0) {?>
                                     <p class="text-center text-muted">
                                        <?php echo __("No people available");?>

                                     </p>
                                     <?php }?>
                                    </ul>
                                </div>
                            </div>
                        
                        <!-- Upload Video/Images added by yasir 08-10-2018 --->
                        <!--
                        <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <strong class="wtf-module">Youtube</strong>
                                </div>
                                <div class="panel-body">
                                    <p class="text-center text-muted upload_share">Upload/Share
                                       <span class="publisher-tools-attach js_publisher-tab" data-tab="video">
                                            <i class="fa fa-video-camera fa-fw js_x-uploader" data-handle="publisher"  data-type="video"></i>
                                        </span>
                                    </p>
                                </div>
                        </div>
                        <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <strong class="wtf-module">Twitter</strong>
                                </div>
                                <div class="panel-body">
                                    <p class="text-center text-muted upload_share">Upload/Share
                                        <span class="publisher-tools-attach js_publisher-tab" data-tab="video">
                                            <i class="fa fa-video-camera fa-fw js_x-uploader" data-handle="publisher"  data-type="video"></i>
                                        </span>
                                    </p>
                                </div>
                        </div>
                        -->
                        <!-- mini footer -->
                        <?php if (count($_smarty_tpl->tpl_vars['user']->value->_data['new_people']) > 0 || count($_smarty_tpl->tpl_vars['new_pages']->value) > 0 || count($_smarty_tpl->tpl_vars['new_groups']->value) > 0 || count($_smarty_tpl->tpl_vars['new_events']->value) > 0) {?>
                            <div class="row plr10 hidden-xs">
                                <div class="col-xs-12 mb5">
                                    <?php if (count($_smarty_tpl->tpl_vars['static_pages']->value) > 0) {?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['static_pages']->value, 'static_page', true);
$_smarty_tpl->tpl_vars['static_page']->iteration = 0;
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['static_page']->value) {
$_smarty_tpl->tpl_vars['static_page']->iteration++;
$_smarty_tpl->tpl_vars['static_page']->last = $_smarty_tpl->tpl_vars['static_page']->iteration == $_smarty_tpl->tpl_vars['static_page']->total;
$__foreach_static_page_4_saved = $_smarty_tpl->tpl_vars['static_page'];
?>
                                            <a href="/static/<?php echo $_smarty_tpl->tpl_vars['static_page']->value['page_url'];?>
">
                                                <?php echo __(((string)$_smarty_tpl->tpl_vars['static_page']->value['page_title']));?>

                                            </a><?php if (!$_smarty_tpl->tpl_vars['static_page']->last) {?> · <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['static_page'] = $__foreach_static_page_4_saved;
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['contact_enabled']) {?>
                                         · 
                                        <a href="/contacts">
                                            <?php echo __("Contacts");?>

                                        </a>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['directory_enabled']) {?>
                                         · 
                                        <a href="/directory">
                                            <?php echo __("Directory");?>

                                        </a>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['market_enabled']) {?>
                                         · 
                                        <a href="/market">
                                            <?php echo __("Market");?>

                                        </a>
                                    <?php }?>
                                </div>
                                <div class="col-xs-12">
                                    &copy; <?php echo date('Y');?>
 <?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 · <span class="text-link dev" data-toggle="modal" data-url="#translator"><?php echo $_smarty_tpl->tpl_vars['system']->value['language']['title'];?>
</span>
                                    

                                </div>
                            </div>
                        <?php }?>
                        <!-- mini footer -->
                    </div>
                    <!-- right panel -->
                </div>
            </div>

        </div>
		<!--Start: new Features updates pop-up-->
        <div id="newFeatureModal" class="modal fade">

            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<?php if ($_smarty_tpl->tpl_vars['website_current_lang']->value == 'en_us') {?>
                        <h4 class="modal-title">New Features Update</h4>
					<?php } else { ?>
						<h4 class="modal-title">新功能更新</h4>	
					<?php }?>	
                    </div>

                    <div class="modal-body">

                        <?php if ($_smarty_tpl->tpl_vars['website_current_lang']->value == 'en_us') {?>
                        <ol >
                          <li >User friendly text editor</li>
                          <li >Technical email support features</li>
                          <li >Image zooming and swiping facility</li>
                          <li >Optimized App performance</li>                         
                          <li >Protect posted pictures and prevent video download</li>
                          <li >Water marking on images and videos</li>
                        </ol>
                        <?php } else { ?>
                         <ol >
                          <li >分享发文</li>
                          <li >用户有好的文本编辑</li>
                          <li >技术支持功能</li>
                          <li >图像缩放</li>
                          <li >APP的优化性能</li>
                          <li >上传照片的安全性</li>
                          <li >图片的水印</li>
                        </ol>
                        <?php }?>

                    </div>

                </div>

            </div>

        </div>
        <!--end-->

       
       <?php if ($_smarty_tpl->tpl_vars['current_user_feature_status']->value == 1) {?>
            <input type="hidden" id="show_feature_popup" name="show_feature_popup" value="1">
            <?php } else { ?>
            <input type="hidden" id="show_feature_popup" name="show_feature_popup" value="0">
            <?php }?>
    </div>
    <!-- page content -->
  

      <?php echo '<script'; ?>
 type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
$(document).ready(function(){
    $(".nav.nav-tabs li a").click(function(){
        $(".alert.alert-success.mb0.mt10.x-hidden").remove();
    });
	
	var show_feature_popup = $('#show_feature_popup').val();
   

    if(show_feature_popup == 1){
        $('#newFeatureModal').modal('show');

        setTimeout(function() {
          closeFeaturePopup();
        }, 10000);
    }
});
<?php echo '</script'; ?>
>

<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }?>



<?php }
}
