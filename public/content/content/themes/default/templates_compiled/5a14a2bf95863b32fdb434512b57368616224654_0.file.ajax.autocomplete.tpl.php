<?php
/* Smarty version 3.1.31, created on 2019-02-05 07:08:47
  from "/var/app/current/content/themes/default/templates/ajax.autocomplete.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c59367fa5c731_00266065',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5a14a2bf95863b32fdb434512b57368616224654' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/ajax.autocomplete.tpl',
      1 => 1536745030,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c59367fa5c731_00266065 (Smarty_Internal_Template $_smarty_tpl) {
?>
<ul>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['users']->value, '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
    <li>
      <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_firstname'] == '' || $_smarty_tpl->tpl_vars['_user']->value['user_lastname'] == '') {?>

      <div class="data-container clickable small js_tag-add" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
">

      <?php } else { ?>
      <div class="data-container clickable small js_tag-add" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>
">
        
      <?php }?>
          <img class="data-avatar" src="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>
">
            <div class="data-content">
                <div><strong>
                  <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_firstname'] == '' || $_smarty_tpl->tpl_vars['_user']->value['user_lastname'] == '') {?>
                  <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>

                  <?php } else { ?>
                  <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>

                  <?php }?>
                </strong>
              </div>
            </div>
        </div>
    </li>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</ul><?php }
}
