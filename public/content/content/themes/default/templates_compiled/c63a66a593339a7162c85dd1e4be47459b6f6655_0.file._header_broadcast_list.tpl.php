<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:12:15
  from "/var/app/current/content/themes/default/templates/_header_broadcast_list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c58726f8ea6f1_01627471',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c63a66a593339a7162c85dd1e4be47459b6f6655' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/_header_broadcast_list.tpl',
      1 => 1536745014,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:__feeds_conversation.tpl' => 1,
    'file:_header.friend_requests.tpl' => 1,
    'file:_header.notifications.tpl' => 1,
    'file:_header.messages.tpl' => 1,
    'file:_header.search.tpl' => 1,
    'file:_ads.tpl' => 1,
  ),
),false)) {
function content_5c58726f8ea6f1_01627471 (Smarty_Internal_Template $_smarty_tpl) {
if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
	<!--<div class="send-user-message" data-toggle="modal" data-url="posts/compose.php?do=create">
        <div class="add">
            <i class="fa fa-plus-circle"></i>
        </div>
    </div> 
	<button class="send-user-message">Send Message</button>-->
    <body data-hash-tok="<?php echo $_smarty_tpl->tpl_vars['session_hash']->value['token'];?>
" data-hash-pos="<?php echo $_smarty_tpl->tpl_vars['session_hash']->value['position'];?>
" class="visitor n_chat">
<?php } else { ?>
		    <body data-hash-tok="<?php echo $_smarty_tpl->tpl_vars['session_hash']->value['token'];?>
" data-hash-pos="<?php echo $_smarty_tpl->tpl_vars['session_hash']->value['position'];?>
" data-chat-enabled="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_chat_enabled'];?>
" class="<?php if (!$_smarty_tpl->tpl_vars['system']->value['chat_enabled']) {?>n_chat<?php }
if ($_smarty_tpl->tpl_vars['system']->value['activation_enabled'] && !$_smarty_tpl->tpl_vars['user']->value->_data['user_activated']) {?> n_activated<?php }
if (!$_smarty_tpl->tpl_vars['system']->value['system_live']) {?> n_live<?php }?>">

	<!--<button class="send-user-message">Send Message</button>-->
    <div class="send-user-message" data-toggle="modal" data-url="posts/compose.php?do=create">
        <div class="add">
            <i class="fa fa-plus-circle"></i>
        </div>
    </div>
<?php }?>
    
    <!-- main wrapper -->
    
   <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?> 
                <div class="sticky_header_second boardcast_header">
                    
                    <ul>
                        
                        <li><a <?php if ($_smarty_tpl->tpl_vars['last_dir']->value == 'home') {?> class="active" <?php }?>  href="/"><i class="Icon Icon--home Icon--large"></i></a></li>
                        
                        <li><a id="search_toggle" href="javascript:void(0)"><i class="Icon Icon--medium Icon--search"></i></a></li>
                        
                        <li>
                            
                            <a href="/notifications" <?php if ($_smarty_tpl->tpl_vars['last_dir']->value == 'notifications' || $_smarty_tpl->tpl_vars['last_dir']->value == 'mentions') {?> class="active" <?php }?>>
                                <span class="label <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_live_notifications_counter'] == 0) {?>hidden<?php }?>">

                                    <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_live_notifications_counter'];?>

                                    
                                </span>
                                <i class="Icon Icon--notifications Icon--large"></i>
                                
                            </a></li>
                            
                            
                            <li class="dropdown js_live-messages top-menues">
                                <a class="left-nav-channel-title title-messaging"><i class="Icon Icon--dm Icon--large"></i></a>
               <!-- <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                <div class="dropdown-menu dropdown-widget with-arrow live-chat-massage">
                    <div class="dropdown-widget-header">
                        <?php echo __("Messages");?>

                        <a class="pull-right flip text-link js_chat-start" href="/messages/new"><?php echo __("Send a New Message");?>
</a>
                    </div>
                    <div class="dropdown-widget-body">
                        <div class="js_scroller">
                            <?php if (count($_smarty_tpl->tpl_vars['user']->value->_data['conversations']) > 0) {?>
                                <ul>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['user']->value->_data['conversations'], 'conversation');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['conversation']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_conversation.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                            <?php } else { ?>
                                <p class="text-center text-muted mt10">
                                    <?php echo __("No messages");?>

                                </p>
                            <?php }?>
                        </div>
                    </div>
                    <a class="dropdown-widget-footer" href="/messages"><?php echo __("See All");?>
</a>
                </div>
                <?php }?>-->
            </li>
            <li><!--a href="/broadcasts" <?php if ($_smarty_tpl->tpl_vars['last_dir']->value == 'broadcasts') {?> class="active" <?php }?>><i class="Icon Icon--cameraVideo Icon--large"></i></a></li-->
            <div class="">
                <a class="active" href="/broadcasts">
                <i class="Icon Icon--cameraVideo Icon--large mobile-broadcast"></i></a>
            </div>
            </li>
        </ul>
        
        
    </div>
    <?php }?>
    
  <div class="main-header">
            <div class="container header-container">
                <!-- navbar-container -->
                <div class="navbar-container pull-left">
                    <!--<div class="brand-container mobile-logo">
                
                                <a href="/" class="brand">
                                        <img width="60" src="<?php echo SYSTEM_LOGO;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
">
                                </a>
                                
                   </div>-->
                   <div class="mobile-menu">
                    <ul class="nav navbar-nav">
                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                        <?php $_smarty_tpl->_assignInScope('dirs', explode("/",$_SERVER['REQUEST_URI']));
?>
                            
                            <!-- home -->
                            <li class="top-menues">
                                <a href="/" <?php ob_start();
echo $_smarty_tpl->tpl_vars['dirs']->value[count($_smarty_tpl->tpl_vars['dirs']->value)-1];
$_prefixVariable1=ob_get_clean();
if ($_prefixVariable1 == '') {?>class="active"<?php }?>>
                                    <i class="Icon Icon--home Icon--large"></i>
                                    <span><?php echo __("Home");?>
</span>
                                </a>
                            </li>
                            <!-- home -->
                            
                            <!-- friend requests -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_header.friend_requests.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                            <!-- friend requests -->
    
                            <!-- notifications -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_header.notifications.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                            <!-- notifications -->
    
                            <!-- messages -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_header.messages.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                            <!-- messages -->
                            
                            <!-- search -->
                            <li class="visible-xs-block top-menues">
                                <a href="/search">
                                    <i class="fa fa-search fa-lg"></i>
                                </a>
                            </li>
                            
                            <!-- search -->
                       
                        <?php }?>
                        
                    </ul>
                    </div>
                    
                </div>
                <!-- navbar-container -->
    			 <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                <div class="brand-container">
                    <!-- brand -->
                    <a href="/" class="brand">
                        <?php if (SYSTEM_LOGO) {?>
                            <img width="60" src="<?php echo SYSTEM_LOGO;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
">
                        <?php } else { ?>
                            <?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>

                        <?php }?>
                    </a>
                    <!-- brand -->
                </div>
                <?php } else { ?>
                <div class="brand-container user_logout_brand">
                    <!-- brand -->
                    <a href="/" class="brand">
                        <?php if (SYSTEM_LOGO) {?>
                            <img width="60" src="<?php echo SYSTEM_LOGO;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
">
                        <?php } else { ?>
                            <?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>

                        <?php }?>
                    </a>
                    <!-- brand -->
                    
                </div>
                <div class="inner_link">
                    <ul>
                   
                      <li><a href="/"><i class="Icon Icon--home Icon--large"></i><?php echo __("Home");?>
</a></li>
                      <!--<li><a href="/static/about">About</a></li>-->
                      <li><a href="/milesguo"><i class="hand_icon"><img src="/content/themes/default/images/hand.svg" alt="hand" width="21"></i><?php echo __("Miles Guo");?>
</a></li>
                   
                   </ul>
                   </div>
                <?php }?>

                 
                
               <!-- navbar-collapse -->
                <div class="header-right <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?> mobile_user_head <?php }?>">

                   
                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                        <!-- search -->
                        <?php $_smarty_tpl->_subTemplateRender('file:_header.search.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                        <!-- search -->
                    <?php }?>

                    <!-- navbar-container -->
                    <div class="navbar-container">
                    	<!--<div class="brand-container mobile-logo">
                    
                                    <a href="/" class="brand">
                                            <img width="60" src="<?php echo SYSTEM_LOGO;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
">
                                    </a>
                                    
                       </div>-->
                       <div class="mobile-menu">
                        <ul class="nav navbar-nav">
                            <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                                
                                <!-- search -->

                                <!-- user-menu -->
                                <li class="dropdown">
                                    <a class="dropdown-toggle user-menu" data-toggle="dropdown">
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_picture'];?>
" alt="">
                                      
                                        
                                    </a>
                                    <ul class="dropdown-menu">
                                    <span class="caret-outer"></span>
                                    <span class="caret-inner"></span>
                                     <li><div class="user_name_id">
                                      
                                     <b class="fullname"><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_lastname'];?>
</a></b>
                                     <p class="name"><span class="username u-dir" dir="ltr"><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
">@<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
</a></span></p>
                                     </div>
                                     </li>
                                     <li class="divider"></li>
                                        <?php if ($_smarty_tpl->tpl_vars['system']->value['packages_enabled'] && !$_smarty_tpl->tpl_vars['user']->value->_data['user_subscribed']) {?>
                                            <!--<li>
                                                <a href="/packages"><span class="DashUserDropdown-linkIcon Icon Icon--medium Icon--me"></span><?php echo __("Upgrade to Pro");?>
</a>
                                            </li>
                                            <li class="divider"></li>-->
                                        <?php }?>
                                        <li>
                                            <a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><span class="DashUserDropdown-linkIcon Icon Icon--medium Icon--me"></span><?php echo __("Profile");?>
</a>
                                        </li>
                                        <li>
                                            <a href="/settings/privacy"><span class="DashUserDropdown-linkIcon Icon Icon--medium Icon--promotedStroked"></span><?php echo __("Privacy");?>
</a>
                                        </li>
                                        <li>
                                            <a href="/settings"><span class="DashUserDropdown-linkIcon Icon Icon--medium Icon--cog"></span><?php echo __("Settings");?>
</a>
                                        </li>
                                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_is_admin) {?>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="/admincp"><?php echo __("Admin Panel");?>
</a>
                                            </li>
                                        <?php }?>
                                       
                                        <li class="divider"></li>
                                        <li>
                                            <a href="/signout"><?php echo __("Log Out");?>
</a>
                                        </li>
                                    </ul>
                                </li>
                                
                                <li class="visible-xs-block">
                                    <a href="#" data-toggle="offcanvas" data-target=".sidebar-nav">
                                        <i class="fa fa-bars fa-lg"></i>
                                    </a>
                                </li>
                                <!-- user-menu -->
                            <?php } else { ?>
                                <!--<li>
                                    <a href="/">
                                        <span><?php echo __("Join");?>
</span>
                                    </a>
                                </li>-->
                            <?php }?>
                        </ul>
                        <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                        <span class="text-link dev" data-toggle="modal" data-url="#translator">
                        
						 <?php echo $_smarty_tpl->tpl_vars['system']->value['language']['title'];?>
</span>
						 <span class="contact-icon">
                            <a href="/broadcasts">
                                 <span class="Icon icon_custom Icon--cameraVideo Icon--small"></span> <?php echo __("GuoTV");?>

                            </a>
                        </span>
                        <a href="/broadcasts" <?php if ($_smarty_tpl->tpl_vars['last_dir']->value == 'broadcasts') {?> class="active" <?php }?>><span class="broadcasts_icon Icon Icon--cameraVideo Icon--small"></span></a>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                          <span class="contact-icon watch_broadcasts">
                            <div class="">
                                 <a href="/broadcasts">
                                 <span class="Icon icon_custom Icon--cameraVideo Icon--small"></span><?php echo __("GuoTV");?>

                              </a>
                            </div>
                        </span>
                         <?php }?>
						</div>
                        
                    </div>
                    <!-- navbar-container -->
                </div>
                <!-- navbar-collapse -->

            </div>
        </div>
        
        <!-- ads -->
        <?php $_smarty_tpl->_subTemplateRender('file:_ads.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_ads'=>$_smarty_tpl->tpl_vars['ads_master']->value['header'],'_master'=>true), 0, false);
?>

        <!-- ads --><?php }
}
