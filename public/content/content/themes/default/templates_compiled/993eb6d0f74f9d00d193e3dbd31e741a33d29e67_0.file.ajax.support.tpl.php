<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:35:04
  from "/var/app/current/content/themes/default/templates/ajax.support.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c5877c842a885_84726135',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '993eb6d0f74f9d00d193e3dbd31e741a33d29e67' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/ajax.support.tpl',
      1 => 1536745008,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c5877c842a885_84726135 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 class="modal-title"><?php echo __("Support");?>
</h5>
</div>
    
	<div id="response-div">
		
		<input type="hidden" name="user_id" id="user_id" value="<?php echo $_smarty_tpl->tpl_vars['user_id']->value;?>
">
		
		<span class="DMTypeaheadHeader">To:</span>
		<div class="post_custom">
			
			<div class="form-group">
				<input type="text" name="support-to" id="support-top" class="form-control" value="support@guo.media.com" disabled required autofocus>
			</div>
		</div>
		
		<span class="DMTypeaheadHeader">From:</span>
		<div class="post_custom">
			
			<div class="form-group">
				<input type="text" name="support-from" id="support-from" class="form-control" disabled value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" required autofocus>
			</div>
		</div>
		
		<span class="DMTypeaheadHeader">Your Email:</span>
		<div class="post_custom">
			
			<div class="form-group">
				<input type="text" name="user-email" id="user-email" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['user_email']->value;?>
" required autofocus>
			</div>
		</div>
		
		<span class="DMTypeaheadHeader">Subject:</span>
		<div class="post_custom">
			
			<div class="form-group">
				<input type="text" name="support-subject" id="support-subject" class="form-control" required autofocus>
			</div>
		</div>
		
		<span class="DMTypeaheadHeader">Message:</span>
		<div class="post_custom">
			
			<div class="form-group">
				<textarea class="expand replyComment  replyComm_chk post_comment form-control" rows="5" cols="50" value="" placeholder='<?php echo __("Add a message");?>
....' name="support-message" id="support-message" required autofocus></textarea>
			</div>
		</div>
		<div class="modal-footer">        
			<a class="btn btn-primary submit_support" data-id="" data-handle=""><?php echo __("Submit");?>
</a>
		</div>
	</div>	
<?php echo '<script'; ?>
 type="text/javascript">
   $(function() {


text_comment_reply();
}); 

$(document).on('keyup', '.replyComm_chk', function(event) {
   
        
        text_comment_reply()
    });


    function text_comment_reply()
    {
     

        var reply_comm=$('textarea.replyComm_chk').val();
        var comm_leng=reply_comm.length;
            text_counter= comm_leng;       
        
            if(text_counter==0)
            {
                $("a.reply_com_btn").attr("disabled",true);
                return true;
            }
            else
            {
                $("a.reply_com_btn").attr("disabled",false);
                return false;
            }
    }
<?php echo '</script'; ?>
><?php }
}
