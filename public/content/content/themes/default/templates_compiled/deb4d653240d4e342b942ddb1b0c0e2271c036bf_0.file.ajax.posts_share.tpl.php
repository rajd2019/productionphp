<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:20:03
  from "/var/app/current/content/themes/default/templates/ajax.posts_share.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c587443244d35_09130663',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'deb4d653240d4e342b942ddb1b0c0e2271c036bf' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/ajax.posts_share.tpl',
      1 => 1536745020,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c587443244d35_09130663 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 class="modal-title"><?php echo __("Share Post");?>
</h5>
</div>
        
    <div class="post_custom">
        <div class="form-group">
            <input type="hidden" name="do" value="share">
            <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" class="postId">
            <textarea class="expand postComment post_com_en" rows="1" cols="50" value="<?php echo $_smarty_tpl->tpl_vars['album']->value['title'];?>
" class="form-control" placeholder='<?php echo __("Add a comment");?>
....' name="comment"></textarea>
        </div>
    </div>

    <div class="modal-body">
        
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['posts']->value, '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
        <div class="post-section">
            <div class="post-body"> 
                <div class="post-header"> 
                    <div class="post-avatar"><img src="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
" /></div> 
                    <div class="post-meta">
                    <div>
                    <span>
                        <span class="first-name-custom"><?php echo $_smarty_tpl->tpl_vars['_user']->value["name"];?>
</span> 
                        <span class="second-name-custom">@<?php echo $_smarty_tpl->tpl_vars['_user']->value["user_name"];?>
</span>
                    </span>
                     <div class="post-time"> <span class="js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['_user']->value['time'];?>
"><?php echo $_smarty_tpl->tpl_vars['_user']->value["time"];?>
</span>
                     </div>
                     <span class="post-title"> </span> 
                     <div class="post-replace">
                        <div class="post-text js_readmore" dir="auto"><?php echo htmlspecialchars_decode($_smarty_tpl->tpl_vars['_user']->value["text"], ENT_QUOTES);?>
</div>
                        <div class="post-text-plain hidden"><?php echo $_smarty_tpl->tpl_vars['_user']->value["text_plain"];?>
</div> 
                    </div>  
                    </div>
                    </div>
                     
                </div>
        </div>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </div>

    <div class="modal-footer">        
        <button type="submit" class="btn btn-primary post-share btn_share_post"><?php echo __("Share");?>
</button>
    </div>

    <?php echo '<script'; ?>
 type="text/javascript">
   $(function() {


//text_post_share();
}); 

$(document).on('keyup', '.post_com_en', function(event) {
   
        
      //  text_post_share()
    });


    function text_post_share()
    {
     

        var post_com=$('textarea.post_com_en').val();
        var post_leng=post_com.length;
            text_counter= post_leng;       
        
            if(text_counter==0)
            {
                $("button.btn_share_post").attr("disabled",true);
                return true;
            }
            else
            {
                $("button.btn_share_post").attr("disabled",false);
                return false;
            }
    }
<?php echo '</script'; ?>
>
<?php }
}
