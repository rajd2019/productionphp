<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:17:08
  from "/var/app/current/content/themes/default/templates/theme_color.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c5873945e4934_89496732',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4c0c14e3d2d2c4698d155161b884e7f7b20ccb6e' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/theme_color.tpl',
      1 => 1536745020,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c5873945e4934_89496732 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style>
.navbar-container span.text-link.dev{
border: 1px solid <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
background: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
span.text-link.dev.translate_button{
color:#fff;
}
.profile-bg-thumb{
	background: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}

.side_profile ul li span{
	color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}

.side_profile ul li a:hover{
color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}

.side-nav{
    background: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
1a;
}
.side-nav a, .side-nav .static{
	color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.panel-body.with-nav{
	background:#fff;
}
.side-nav>li.active>a{
background: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
color:#fff;
}

.side-nav a:hover{
background: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
color:#fff;
}

.btn-primary, .btn-primary:active{
background: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
d9;
border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
d9;
}
.btn-primary:hover, .btn-primary:active:hover{
background: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
 !important;
border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
 !important;
}
.publisher-footer .x-uploader .fa{
	color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
d9;
}
.publisher-footer .fa-fw{
	color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
d9;
}

.js_user-popover a:hover .first-name-custom{
    color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}

.post-meta .dropdown .dropdown-toggle:hover, .dropdown.custom-dropdown-profile .dropdown-toggle:hover {
background: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
40;
color:#fff;
}
.post-time a:hover{
color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>

}
.inner_link li a:hover img{
background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.panel-heading a{
	color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.dropdown-menu>li>a:hover, .dropdown-menu>li>a:focus{
	background: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
	color:#fff;
}

a.hover, a:hover, a.hover:hover{
color:#000;
}

.inner_link li a:hover{
color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
border-bottom: 2px solid <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}

.btn-translate span:hover {
    color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}

.btn-translate:hover span:hover,.btn-translate:hover span, .btn-translate:hover i.fa.fa-globe {
    color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}

.post-actions span.text-clickable:hover i {
    color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}

/*--never change this--*/
.post-actions span.text-clickable.js_like-post:hover i {
    color: #ed4956;
}


/*---GUO Page---*/
.profile-cover-wrapper{
	background-color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
    border-bottom: 1px solid #e8faf2;
}

div.panel-heading#mobilefeedphoto, 
.profile-rightbar .panel-heading{
background-color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
color:#fff;
}
div.panel-heading#mobilefeedphoto a, 
.profile-rightbar .panel-heading a{
background-color: ;
color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.profile-sidebar .panel-default>.panel-heading, .panel-default>.panel-heading{
background-color:transparent !important;
}
.profile-sidebar .panel-default>.panel-heading, .panel-default>.panel-heading{
	background-color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}

.custom-search-follow{
	background: #e8faf2;
}

.js_user-popover a:hover, .data-content .name a:hover{
	color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
 !important;
}
.js_user-popover a:hover, .data-content .name a.user-at-btn:hover{
	color: #000 !important;
}
.data-content .name a:hover span {
    border-bottom: 1px solid <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.user-card-cover{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
	border-bottom : #e8faf2;
}
.user-card-meta .fa{
	color:#fff;
}
.post_custom{
	background-color:#fff;
	background-image: linear-gradient(<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
1a, <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
1a);
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
1a;
}
.input-group-addon{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
40;
}
.profile-tabs-wrapper>ul>li>a:hover, .profile-tabs-wrapper>ul>li>a.active, .profile-tabs-wrapper>ul>li>a:focus{
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.profile-tabs-wrapper>ul>li>a:hover span, .profile-tabs-wrapper>ul>li>a.active span, .profile-tabs-wrapper>ul>li>a:focus span{
	color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.EdgeButton--secondary{
	border-color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
    color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.EdgeButton--secondary:hover, .EdgeButton--secondary:focus, .EdgeButton--secondary:active{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
1a;
	color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.EdgeButton--secondary:active {
    box-shadow: 0 0 0 2px #FFFFFF, 0 0 0 4px <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
d9;
}
button.transparent-no-border-button:hover {
    color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
 !important;
}
#postBoxDiv.focused{
	box-shadow: 0 0 0 1px <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
66;
	border-color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
66;
}
.EdgeButton--primary, .EdgeButton--primary:focus{
	background-color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
d9;
	border-color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
d9;
}

.EdgeButton--primary:hover, .EdgeButton--primary:active{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>

}

.EdgeButton--primary:hover{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>

}
.EdgeButton--primary:active{
	    box-shadow: 0 0 0 2px #FFFFFF, 0 0 0 4px <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
d9;
}
.u-textUserColorHover:hover, .u-textUserColorHover:focus{
	color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
 !important;
}
.TokenizedMultiselect-inputContainer{
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.DMActivity-footer .DMButtonBar{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
1a;
}
.Icon--verified{
	color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}

.DMTypeaheadItem-title .fullname:hover {
    color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.InputToken{
	background-color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}

.u-bgUserColorLightest{
	background-color: #e8faf2;
}

.DMConversation-composer{
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
1a !important;
}
  
.navbar-nav .open.attachment .TweetBoxExtras-item{
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
	background-color:#e8faf2;
}


.offcanvas-sidebar .panel-default{
	background-color:#e8faf2;
}

.side-nav>li.active>a {
    background-color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
    color: #fff;
}

.panel-heading.with-nav .nav>li.active>a {
    color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
    border: 0;
    border-bottom: solid 3px <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
    padding-bottom: 9px;
}

.panel-heading.with-nav .nav>li.active>a strong {
    color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.panel-heading.with-nav .nav>li a:hover {
    color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
    border-color: transparent;
    border-bottom: solid 3px transparent;    
    background: no-repeat;
	padding-bottom:9px;
}
.panel-heading.with-nav .nav>li.active a:hover {
    color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
    border-color: transparent;
    border-bottom: solid 3px <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;    
    background: no-repeat;
	padding-bottom:9px;
}
.panel-heading.with-nav .nav>li a:hover strong{
	color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.ProfileHeading-toggleItem .ProfileHeading-toggleLink, .fb_post_rep h4 a{
	color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.btn-group.profile_unfollow_btn .js_unfollow, .feeds-item .btn.btn-default.js_unfollow, .js_unfollow{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
d9 !important;
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
d9 !important;
	color: #fff !important;
}
.btn-group.profile_unfollow_btn .js_unfollow:hover,.feeds-item .btn.btn-default.js_unfollow:hover,.js_unfollow:hover{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
 !important;
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
 !important;
	color: #fff !important;
}
.btn-group.profile_unfollow_btn .js_unfollow, .feeds-item .btn.btn-default.js_unfollow, a.js_unfollow{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
d9;
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
d9 !important;
	color:#fff !important;
}
.btn-group.profile_unfollow_btn .js_unfollow:focus, .btn-group.profile_unfollow_btn .js_unfollow:hover, .feeds-item .btn.btn-default.js_unfollow:focus, .feeds-item .btn.btn-default.js_unfollow:hover, a.js_unfollow:focus, a.js_unfollow:hover{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
 !important;
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
 !important;
	color:#fff !important;
}

.btn-group.profile_unfollow_btn .js_follow, .btn-group.profile_unfollow_btn .js_follow:active, .feeds-item .btn.btn-default.js_follow, .feeds-item .btn.btn-default.js_follow:active, .custom-site-bar-new .js_follow, a.js_follow:active, a.js_follow{
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
 !important;
	color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
 !important;
}
.btn-group.profile_unfollow_btn .js_follow:focus, .btn-group.profile_unfollow_btn .js_follow:hover, .feeds-item .btn.btn-default.js_follow:focus, .feeds-item .btn.btn-default.js_follow:hover, .custom-site-bar-new .js_follow:hover, a.js_follow:focus, a.js_follow:hover{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
1a !important;
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
 !important;
	color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
 !important;
}
.translator-language:hover {
    background: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
    color: #fff;
}


.inner_link ul > li > a.top-nav-active{
	color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
	border-bottom: 2px solid <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.inner_link ul > li > a.top-nav-active img{	
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.sticky_header_second li a:hover, .sticky_header_second li a.active{
	color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
	border-bottom: 2px solid <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.sticky_header_second li a:hover img{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
#close-modal-popup, #discard-modal .modal-header .close{
	color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.dropdown-widget-footer:hover, .dropdown-widget-footer{
	color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.alert-info{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
1a;
	border-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
1a;
	color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.alert-info span{
	color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
#discard-modal .modal-footer .btn-primary:hover, #discard-modal .modal-footer .btn-primary:focus; #postsocial .btn-primary:hover, #postsocial .btn-primary:focus{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
 !important;
}
li.take-photo {
    background: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
d9;
}
li.take-video {
    background: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
d9;
}
li.take-photo:hover {
    background: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
li.take-video:hover {
    background: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
#close-update-popup, #close-modal-popup, #discard-modal .modal-header .close, .publisher-tools-attach .fa.fa-smile-o{
	color: <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
a#post-update-btn{
	background-color:<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
;
}
.modal-dialog button#play, .modal-dialog button#snapshot, .modal-dialog button#download, .modal-dialog button.popup-close{
	
}
</style><?php }
}
