<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:12:16
  from "/var/app/current/content/themes/default/templates/_header.notifications.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c5872708c4f78_69920971',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '373e04e313ae09ee7659e003300889a852cad428' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/_header.notifications.tpl',
      1 => 1536745026,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:__feeds_notification.tpl' => 1,
  ),
),false)) {
function content_5c5872708c4f78_69920971 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('dirs', explode("/",$_SERVER['REQUEST_URI']));
?>

<li class="dropdown top-menues">

    <a href="/notifications" <?php ob_start();
echo $_smarty_tpl->tpl_vars['dirs']->value[count($_smarty_tpl->tpl_vars['dirs']->value)-1];
$_prefixVariable1=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['dirs']->value[count($_smarty_tpl->tpl_vars['dirs']->value)-1];
$_prefixVariable2=ob_get_clean();
if ($_prefixVariable1 == 'notifications' || $_prefixVariable2 == 'mentions') {?>class="active"<?php }?>>

        <i class="Icon Icon--notifications Icon--large"></i>

        <span> <?php echo __("Notifications");?>
</span>

        <span class="label <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_live_notifications_counter'] == 0) {?>hidden<?php }?>">

            <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_live_notifications_counter'];?>


        </span>

    </a>

    <div class="dropdown-menu dropdown-widget with-arrow js_dropdown-keepopen notification-popup">

        <div class="dropdown-widget-header">

            <?php echo __("Notifications");?>




            <label class="switch sm pull-right flip" for="notifications_sound">

                <input type="checkbox" class="js_notifications-sound-toggle" name="notifications_sound" id="notifications_sound" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['notifications_sound']) {?>checked<?php }?>>

                <span class="slider round"></span>

            </label>

            <div class="pull-right flip mr5 fw-normal">

                <?php echo __("Alert Sound");?>


            </div>

        </div>

        <div class="dropdown-widget-body">

            <div class="js_scroller" style="height: 280px;">

                <?php if (count($_smarty_tpl->tpl_vars['user']->value->_data['notifications']) > 0) {?>

                    <ul>

                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['user']->value->_data['notifications'], 'notification');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['notification']->value) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:__feeds_notification.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                    </ul>

                <?php } else { ?>

                    <p class="text-center text-muted mt10">

                        <?php echo __("No notifications");?>


                    </p>

                <?php }?>

            </div>

        </div>

        <a class="dropdown-widget-footer" href="/notifications"><?php echo __("See All");?>
</a>

    </div>

</li><?php }
}
