<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:12:14
  from "/var/app/current/content/themes/default/templates/_ga_tag.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c58726ee817a8_17623128',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c7d6dbbb04eb05508892a3da20fff9851f4b7ede' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/_ga_tag.tpl',
      1 => 1536745024,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c58726ee817a8_17623128 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<?php echo '<script'; ?>
 async src="https://www.googletagmanager.com/gtag/js?id=UA-119903360-1"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
 
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  

  gtag('config', 'UA-119903360-1');
<?php echo '</script'; ?>
>
<?php }
}
