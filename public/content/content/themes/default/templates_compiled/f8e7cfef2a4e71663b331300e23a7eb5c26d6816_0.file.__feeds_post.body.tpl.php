<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:12:15
  from "/var/app/current/content/themes/default/templates/__feeds_post.body.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c58726fd1dca2_85860481',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f8e7cfef2a4e71663b331300e23a7eb5c26d6816' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/__feeds_post.body.tpl',
      1 => 1536745032,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:__feeds_post.text.tpl' => 1,
    'file:__feeds_post.body.tpl' => 2,
  ),
),false)) {
function content_5c58726fd1dca2_85860481 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_photo_thumb')) require_once '/var/app/current/vendor/smarty/smarty/libs/plugins/function.photo_thumb.php';
if (!is_callable('smarty_modifier_truncate')) require_once '/var/app/current/vendor/smarty/smarty/libs/plugins/modifier.truncate.php';
if ($_smarty_tpl->tpl_vars['_post']->value['user_name'] != 'milesguo') {?>
<style>
.js_lightbox::before {
    width: 100%;
    height: 100%;
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    background-image: url(/content/themes/default/images/logo.png);
	background-repeat:repeat;
	    opacity: 0.04;
}
.loader-on span.loader {
    position: absolute;
    top: 0;
    height: 100%;
    background: rgba(0, 0, 0, 0.5);
}
/*.lg-outer.lg-use-transition-for-zoom .lg-item.lg-complete .lg-img-wrap:after{
	width: 100%;
	height: 100%;
	content: "";
	position: absolute;
	top: 0;
	left: 0;
	background-image: url(/content/themes/default/images/logo.png);
	background-repeat:repeat;
	opacity: 0.04;
}*/
.ld-hide{
display:none;
}
</style>
<?php } else { ?>
	<style>
	.loader-on span.loader {
		position: absolute;
		top: 0;
		height: 100%;
		background: rgba(0, 0, 0, 0.5);
	}
	/*.lg-outer.lg-use-transition-for-zoom .lg-item.lg-complete .lg-img-wrap:after{
		width: 100%;
		height: 100%;
		content: "";
		position: absolute;
		top: 0;
		left: 0;
		background-image: url(/content/themes/default/images/logo.png);
		background-repeat:repeat;
		opacity: 0.04;
	}*/
	.ld-hide{
	display:none;
	}
	</style>
<?php }?>
<!-- post header -->


<div class="post-header">
    <!-- post picture -->
    <div class="post-avatar">
        <a  class="post-avatar-picture"
            href="<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_author_url'];?>
"
            style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_author_picture'];?>
);"></a>
    </div>
    <!-- post picture -->

    <!-- post meta -->
    <div class="post-meta">
        <!-- post menu & author name & type meta & feeling -->
        <div>
            <!-- post author name -->
            <span class="js_user-popover" data-type="<?php echo $_smarty_tpl->tpl_vars['_post']->value['user_type'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['_post']->value['user_id'];?>
">
               <a href="<?php echo $_smarty_tpl->tpl_vars['_post']->value['user_name'];?>
">
                  <span class="first-name-custom"><?php if ($_smarty_tpl->tpl_vars['_post']->value['user_firstname']) {
echo $_smarty_tpl->tpl_vars['_post']->value['user_firstname'];
}?></span>
                  <span class="second-name-custom">@<?php echo $_smarty_tpl->tpl_vars['_post']->value['user_name'];?>
</span>
              </a>
            </span>

            <?php if ($_smarty_tpl->tpl_vars['_post']->value['post_author_verified']) {?>
                <?php if ($_smarty_tpl->tpl_vars['_post']->value['user_type'] == "user") {?>
                  <?php $_smarty_tpl->_assignInScope('type_copy', __("Verified User"));
?>
                <?php } else { ?>
                  <?php $_smarty_tpl->_assignInScope('type_copy', __("Verified Page"));
?>
                <?php }?>

                <i  data-toggle="tooltip" data-placement="top" title='<?php echo $_smarty_tpl->tpl_vars['type_copy']->value;?>
'
                    class="fa fa-check-circle fa-fw verified-badge">
                </i>
            <?php }?>

            <!-- post time & location & privacy -->

            <div class="post-time">
              <a href="/posts/<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_id'];?>
" class="js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['_post']->value['time'];?>
" data-toggle="tooltip" data-placement="right" title="<?php echo $_smarty_tpl->tpl_vars['_post']->value['formatted_time'];?>
">
                <?php echo $_smarty_tpl->tpl_vars['_post']->value['time'];?>

              </a>
              <?php if ($_smarty_tpl->tpl_vars['_post']->value['location']) {?>
                    路
                    <i class="fa fa-map-marker"></i>
                    <span><?php echo $_smarty_tpl->tpl_vars['_post']->value['location'];?>
</span>
                <?php }?>
            </div>
            <!-- post time & location & privacy -->


            <?php if ($_smarty_tpl->tpl_vars['_post']->value['user_subscribed']) {?>
                <i  data-toggle="tooltip" data-placement="top"
                    title='<?php echo __("Pro User");?>
' class="fa fa-bolt fa-fw pro-badge">
                </i>
            <?php }?>

            <?php if ($_smarty_tpl->tpl_vars['_post']->value['feeling_action']) {?>
                <span class="post-title"> <?php echo __("is");?>
 <i
                            class="twa twa-lg twa-<?php echo $_smarty_tpl->tpl_vars['_post']->value['feeling_icon'];?>
"></i> <?php echo __($_smarty_tpl->tpl_vars['_post']->value["feeling_action"]);?>
 <?php echo __($_smarty_tpl->tpl_vars['_post']->value["feeling_value"]);?>
 </span>
            <?php }?>

        </div>
        <!-- post menu & author name & type meta & feeling -->
    </div>

    <!-- post meta -->

</div>

<!-- post header -->

<!-- product -->

<?php if ($_smarty_tpl->tpl_vars['_post']->value['post_type'] == "product" && $_smarty_tpl->tpl_vars['_post']->value['product']) {?>



    <?php if ($_smarty_tpl->tpl_vars['_post']->value['product']['available']) {?>
        <div class="mb5 text-lg"><strong><?php echo $_smarty_tpl->tpl_vars['_post']->value['product']['name'];?>
</strong></div>
    <?php } else { ?>
        <div class="mb5 text-lg x-muted"><strong>(<?php echo __("SOLD");?>
) <?php echo $_smarty_tpl->tpl_vars['_post']->value['product']['name'];?>
</strong></div>
    <?php }?>
    <div class="mb5 text-bg"><i class="fa fa-money fa-fw"></i> <span
                class="green"><?php echo $_smarty_tpl->tpl_vars['system']->value['system_currency_symbol'];
echo $_smarty_tpl->tpl_vars['_post']->value['product']['price'];?>
</span>
        (<?php echo $_smarty_tpl->tpl_vars['system']->value['system_currency'];?>
)
    </div>
    <?php if ($_smarty_tpl->tpl_vars['_post']->value['product']['location']) {?>
        <div class="mb10 text-muted"><i class="fa fa-map-marker fa-fw"></i> <?php echo $_smarty_tpl->tpl_vars['_post']->value['product']['location'];?>
 </div>
    <?php }?>



<?php }?>

<!-- product -->

<!-- post text -->

<?php if (!$_smarty_tpl->tpl_vars['_shared']->value) {?>



    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.text.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>




<?php } else { ?>

    <div class="post-text js_readmore post-listing" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" dir="auto"><?php echo htmlspecialchars_decode($_smarty_tpl->tpl_vars['_post']->value['text'], ENT_QUOTES);?>
</div>

<?php }?>

<!-- post text -->

<?php if ($_smarty_tpl->tpl_vars['_post']->value['post_type'] == "album" || ($_smarty_tpl->tpl_vars['_post']->value['post_type'] == "product" && $_smarty_tpl->tpl_vars['_post']->value['photos_num'] > 0) || $_smarty_tpl->tpl_vars['_post']->value['post_type'] == "photos" || $_smarty_tpl->tpl_vars['_post']->value['post_type'] == "profile_picture" || $_smarty_tpl->tpl_vars['_post']->value['post_type'] == "profile_cover" || $_smarty_tpl->tpl_vars['_post']->value['post_type'] == "page_picture" || $_smarty_tpl->tpl_vars['_post']->value['post_type'] == "page_cover") {?>
    <div class="mt10 clearfix">

        <div class="pg_wrapper"> <?php if ($_smarty_tpl->tpl_vars['_post']->value['photos_num'] == 1) {?>
                <div class="pg_1x"><a href="/photos/<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][0]['photo_id'];?>
"
                                      class="js_lightbox loader-on" data-id="<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][0]['photo_id'];?>
"
                                      data-image=""
                                      data-context="<?php if ($_smarty_tpl->tpl_vars['_post']->value['post_type'] == 'product') {?>post<?php } else { ?>album<?php }?>" data-post-id = "<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_id'];?>
">
									  <div class="text" style=" position: absolute; width: 595px; height: 100%;"></div>
                        <img class="lazy" data-src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][0]['source'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo smarty_function_photo_thumb(array('param1'=>$_smarty_tpl->tpl_vars['_post']->value['photos'][0]['source']),$_smarty_tpl);?>
" /> <span class="loader loader_middium ld-hide"></span></a></div>
            <?php } elseif ($_smarty_tpl->tpl_vars['_post']->value['photos_num'] == 2) {?>



                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['_post']->value['photos'], 'photo');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['photo']->value) {
?>
                    <div class="pg_2x"><a href="/photos/<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" class="js_lightbox loader-on"
                                          data-id="<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
"
                                          data-image="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['photo']->value['source'];?>
"
                                          data-context="post" data-post-id = "<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_id'];?>
" 
                                          style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo smarty_function_photo_thumb(array('param1'=>$_smarty_tpl->tpl_vars['photo']->value['source']),$_smarty_tpl);?>
');"><span class="loader loader_middium ld-hide"></span></a>
                    </div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>




            <?php } elseif ($_smarty_tpl->tpl_vars['_post']->value['photos_num'] == 3) {?>				
                <div class="pg_3x">
					
                    <div class="pg_2o3">

                        <div class="pg_2o3_in"><a
                                    href="/photos/<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][0]['photo_id'];?>
"
                                    class="js_lightbox loader-on" data-id="<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][0]['photo_id'];?>
"
                                    data-image=""
                                    data-context="post" data-post-id = "<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_id'];?>
" 
                                    style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo smarty_function_photo_thumb(array('param1'=>$_smarty_tpl->tpl_vars['_post']->value['photos'][0]['source']),$_smarty_tpl);?>
');"><span class="loader loader_middium ld-hide"></span></a>
                        </div>

                    </div>

                    <div class="pg_1o3">

                        <div class="pg_1o3_in"><a
                                    href="/photos/<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][1]['photo_id'];?>
"
                                    class="js_lightbox loader-on" data-id="<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][1]['photo_id'];?>
"
                                    data-image=""
                                    data-context="post" data-post-id = "<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_id'];?>
"
                                    style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo smarty_function_photo_thumb(array('param1'=>$_smarty_tpl->tpl_vars['_post']->value['photos'][1]['source']),$_smarty_tpl);?>
');"><span class="loader loader_middium ld-hide"></span></a>
                        </div>

                        <div class="pg_1o3_in"><a
                                    href="/photos/<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][2]['photo_id'];?>
"
                                    class="js_lightbox loader-on" data-id="<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][2]['photo_id'];?>
"
                                    data-image=""
                                    data-context="post" data-post-id = "<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_id'];?>
" 
                                    style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo smarty_function_photo_thumb(array('param1'=>$_smarty_tpl->tpl_vars['_post']->value['photos'][2]['source']),$_smarty_tpl);?>
');"><span class="loader loader_middium ld-hide"></span></a>
                        </div>

                    </div>

                </div>
            <?php } else { ?>
                <div class="pg_4x">

                    <div class="pg_2o3">

                        <div class="pg_2o3_in"><a
                                    href="/photos/<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][0]['photo_id'];?>
"
                                    class="js_lightbox loader-on" data-id="<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][0]['photo_id'];?>
"
                                    data-image=""
                                    data-context="post" data-post-id = "<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_id'];?>
" 
                                    style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo smarty_function_photo_thumb(array('param1'=>$_smarty_tpl->tpl_vars['_post']->value['photos'][0]['source']),$_smarty_tpl);?>
');"><span class="loader loader_middium ld-hide"></span></a>
                        </div>

                    </div>

                    <div class="pg_1o3">

                        <div class="pg_1o3_in"><a
                                    href="/photos/<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][1]['photo_id'];?>
"
                                    class="js_lightbox loader-on" data-id="<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][1]['photo_id'];?>
"
                                    data-image=""
                                    data-context="post" data-post-id = "<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_id'];?>
" 
                                    style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo smarty_function_photo_thumb(array('param1'=>$_smarty_tpl->tpl_vars['_post']->value['photos'][1]['source']),$_smarty_tpl);?>
');"><span class="loader loader_middium ld-hide"></span></a>
                        </div>

                        <div class="pg_1o3_in"><a
                                    href="/photos/<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][2]['photo_id'];?>
"
                                    class="js_lightbox loader-on" data-id="<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][2]['photo_id'];?>
"
                                    data-image=""
                                    data-context="post" data-post-id = "<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_id'];?>
" 
                                    style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo smarty_function_photo_thumb(array('param1'=>$_smarty_tpl->tpl_vars['_post']->value['photos'][2]['source']),$_smarty_tpl);?>
');"><span class="loader loader_middium ld-hide"></span></a>
                        </div>

                        <div class="pg_1o3_in"><a
                                    href="/photos/<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][3]['photo_id'];?>
"
                                    class="js_lightbox loader-on" data-id="<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos'][3]['photo_id'];?>
"
                                    data-image=""
                                    data-context="post" data-post-id = "<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_id'];?>
" 
                                    style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo smarty_function_photo_thumb(array('param1'=>$_smarty_tpl->tpl_vars['_post']->value['photos'][3]['source']),$_smarty_tpl);?>
');"> <?php if ($_smarty_tpl->tpl_vars['_post']->value['photos_num'] > 4) {?>
                                    <span class="more">+<?php echo $_smarty_tpl->tpl_vars['_post']->value['photos_num']-4;?>
</span>
                                <?php }?> <span class="loader loader_middium ld-hide"></span></a></div>

                    </div>

                </div>
            <?php }?> </div>

    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['_post']->value['post_type'] == "media" && $_smarty_tpl->tpl_vars['_post']->value['media']) {?>
    <div class="mt10"> <?php if ($_smarty_tpl->tpl_vars['_post']->value['media']['source_type'] == "photo") {?>
            <div class="post-media">

                <div class="post-media-image">

                    <div style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['source_url'];?>
');"></div>

                </div>

                <div class="post-media-meta">

                    <div class="source"><a target="_blank"
                                           href="<?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['source_url'];?>
"><?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['source_provider'];?>
</a>
                    </div>

                </div>

            </div>
        <?php } else { ?>



            <?php if ($_smarty_tpl->tpl_vars['_post']->value['media']['source_provider'] == "YouTube") {?>



                <?php if ($_smarty_tpl->tpl_vars['system']->value['smart_yt_player']) {?>



                    <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['_post']) ? $_smarty_tpl->tpl_vars['_post']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['media']['vidoe_id'] = get_youtube_id($_smarty_tpl->tpl_vars['_post']->value['media']['source_html']);
$_smarty_tpl->_assignInScope('_post', $_tmp_array);
?>
                    <div class="youtube-player youtube-video" data-id="<?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['vidoe_id'];?>
"><img
                                src="https://i.ytimg.com/vi/<?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['vidoe_id'];?>
/hqdefault.jpg">

                        <div class="play"></div>

                    </div>
                    <div class="post-media-meta"><a class="title mb5" href="<?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['source_url'];?>
"
                                                    target="_blank"><?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['source_title'];?>
</a>

                        <div class="text mb5"></div>

                        <div class="source"><?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['source_text'];?>
</div>

                    </div>
                <?php } else { ?>
                    <div class="post-media">

                        <div class="embed-responsive embed-responsive-16by9"> <?php echo html_entity_decode($_smarty_tpl->tpl_vars['_post']->value['media']['source_html'],ENT_QUOTES);?>
 </div>

                    </div>
                    <div class="post-media-meta"><a class="title mb5" href="<?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['source_url'];?>
"
                                                    target="_blank"><?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['source_title'];?>
</a>

                        <div class="text mb5"><?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['source_text'];?>
</div>

                        <div class="source"><?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['source_provider'];?>
</div>

                    </div>
                <?php }?>







            <?php } elseif ($_smarty_tpl->tpl_vars['_post']->value['media']['source_provider'] == "Vimeo" || $_smarty_tpl->tpl_vars['_post']->value['media']['source_provider'] == "SoundCloud" || $_smarty_tpl->tpl_vars['_post']->value['media']['source_provider'] == "Vine") {?>
                <div class="post-media">

                    <div class="embed-responsive embed-responsive-16by9"> <?php echo html_entity_decode($_smarty_tpl->tpl_vars['_post']->value['media']['source_html'],ENT_QUOTES);?>
 </div>

                </div>
                <div class="post-media-meta"><a class="title mb5" href="<?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['source_url'];?>
"
                                                target="_blank"><?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['source_title'];?>
</a>

                    <div class="text mb5"><?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['source_text'];?>
</div>

                    <div class="source"><?php echo $_smarty_tpl->tpl_vars['_post']->value['media']['source_provider'];?>
</div>

                </div>
            <?php } else { ?>
                <div class="embed-ifram-wrapper"> <?php echo html_entity_decode($_smarty_tpl->tpl_vars['_post']->value['media']['source_html'],ENT_QUOTES);?>
 </div>
            <?php }?>



        <?php }?> </div>
<?php } elseif ($_smarty_tpl->tpl_vars['_post']->value['post_type'] == "link" && $_smarty_tpl->tpl_vars['_post']->value['link']) {?>
    <div class="mt10">

        <div class="post-media"> <?php if ($_smarty_tpl->tpl_vars['_post']->value['link']['source_thumbnail']) {?>
                <div class="post-media-image">

                    <div style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['_post']->value['link']['source_thumbnail'];?>
');"></div>

                </div>
            <?php }?>

            <div class="post-media-meta"><a class="title mb5" href="<?php echo $_smarty_tpl->tpl_vars['_post']->value['link']['source_url'];?>
"
                                            target="_blank"><?php echo $_smarty_tpl->tpl_vars['_post']->value['link']['source_title'];?>
</a>

                <div class="text mb5"><?php echo $_smarty_tpl->tpl_vars['_post']->value['link']['source_text'];?>
</div>

                <div class="source"><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['_post']->value['link']['source_host'], 'UTF-8');?>
</div>

            </div>

        </div>

    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['_post']->value['post_type'] == "poll" && $_smarty_tpl->tpl_vars['_post']->value['poll']) {?>
    <div class="poll-options mt10"
         data-poll-votes="<?php echo $_smarty_tpl->tpl_vars['_post']->value['poll']['votes'];?>
"> <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['_post']->value['poll']['options'], 'option');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['option']->value) {
?>
            <div class="mb5">

                <div class="poll-option js_poll-vote" data-id="<?php echo $_smarty_tpl->tpl_vars['option']->value['option_id'];?>
"
                     data-option-votes="<?php echo $_smarty_tpl->tpl_vars['option']->value['votes'];?>
">

                    <div class="percentage-bg" <?php if ($_smarty_tpl->tpl_vars['_post']->value['poll']['votes'] > 0) {?> style="width: <?php echo ($_smarty_tpl->tpl_vars['option']->value['votes']/$_smarty_tpl->tpl_vars['_post']->value['poll']['votes'])*100;?>
%"<?php }?>></div>

                    <div class="radio radio-primary radio-inline">

                        <input type="radio" name="poll_<?php echo $_smarty_tpl->tpl_vars['_post']->value['poll']['poll_id'];?>
" id="option_<?php echo $_smarty_tpl->tpl_vars['option']->value['option_id'];?>
"
                               <?php if ($_smarty_tpl->tpl_vars['option']->value['checked']) {?>checked<?php }?>>

                        <label for="option_<?php echo $_smarty_tpl->tpl_vars['option']->value['option_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['option']->value['text'];?>
</label>

                    </div>

                </div>

                <div class="poll-voters">

                    <div class="more" data-toggle="modal"
                         data-url="posts/who_votes.php?option_id=<?php echo $_smarty_tpl->tpl_vars['option']->value['option_id'];?>
"> <?php echo $_smarty_tpl->tpl_vars['option']->value['votes'];?>
 </div>

                </div>

            </div>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>
 </div>
<?php } elseif ($_smarty_tpl->tpl_vars['_post']->value['post_type'] == "article" && $_smarty_tpl->tpl_vars['_post']->value['article']) {?>
    <div class="mt10">

        <div class="post-media"> <?php if ($_smarty_tpl->tpl_vars['_post']->value['article']['cover']) {?>
                <a class="post-media-image"
                   href="/blogs/<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_id'];?>
/<?php echo $_smarty_tpl->tpl_vars['_post']->value['article']['title_url'];?>
">

                    <div style="padding-top: 50%; background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_post']->value['article']['cover'];?>
');"></div>

                </a>
            <?php }?>

            <div class="post-media-meta"><a class="title mb5"
                                            href="/blogs/<?php echo $_smarty_tpl->tpl_vars['_post']->value['post_id'];?>
/<?php echo $_smarty_tpl->tpl_vars['_post']->value['article']['title_url'];?>
"><?php echo $_smarty_tpl->tpl_vars['_post']->value['article']['title'];?>
</a>

                <div class="text mb5"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['_post']->value['article']['text_snippet'],400);?>
</div>

            </div>

        </div>

    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['_post']->value['post_type'] == "video" && $_smarty_tpl->tpl_vars['_post']->value['video']) {?>
    <div class="video_broadcast" onmouseout="video_stop(this)" onmouseover="video_play(this)">
	<?php if ($_smarty_tpl->tpl_vars['_post']->value['user_id'] != '356') {?>
		<div class="text" style=" position: absolute; width: 94% !important; height: 50% !important; z-index:1;"></div>
	<?php } else { ?>	
		
	<?php }?>
        <video width="100%" class="js_mediaelementplayer custom-video" controls="controls" preload="auto">
             <?php if ($_smarty_tpl->tpl_vars['_post']->value['is_broadcast'] == 1) {?>
                <source src="<?php echo BROADCAST_URL;?>
/<?php echo $_smarty_tpl->tpl_vars['_post']->value['video']['source'];?>
" type="video/mp4">
                <source src="<?php echo BROADCAST_URL;?>
/<?php echo $_smarty_tpl->tpl_vars['_post']->value['video']['source'];?>
" type="video/webm">
            <?php } else { ?>
                <source src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_post']->value['video']['source'];?>
" type="video/mp4">
                <source src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_post']->value['video']['source'];?>
" type="video/webm">
            <?php }?>

        </video>

    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['_post']->value['post_type'] == "audio" && $_smarty_tpl->tpl_vars['_post']->value['audio']) {?>
    <div>

        <audio width="100%" class="js_mediaelementplayer">

            <source src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_post']->value['audio']['source'];?>
">

            <?php echo __("Your browser does not support HTML5 audio");?>
 </audio>

    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['_post']->value['post_type'] == "file" && $_smarty_tpl->tpl_vars['_post']->value['file']) {?>
    <div class="post-downloader">

        <div class="icon"><i class="fa fa-file-text-o fa-2x"></i></div>

        <div class="info"><strong><?php echo __("File Type");?>
</strong>: <?php ob_start();
echo $_smarty_tpl->tpl_vars['_post']->value['file']['source'];
$_prefixVariable4=ob_get_clean();
echo get_extension($_prefixVariable4);?>


            <div class="mt10"><a class="btn btn-primary btn-sm"
                                 href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_post']->value['file']['source'];?>
"><?php echo __("Download");?>
</a>
            </div>

        </div>

    </div>
<?php } elseif (!$_smarty_tpl->tpl_vars['_shared']->value && $_smarty_tpl->tpl_vars['_post']->value['post_type'] == "shared" && $_smarty_tpl->tpl_vars['_post']->value['origin']) {?>



    <?php if ($_smarty_tpl->tpl_vars['_snippet']->value) {?><span class="text-link js_show-attachments"><?php echo __("Show Attachments");?>
</span><?php }?>
    <div class="mt10 <?php if ($_smarty_tpl->tpl_vars['_snippet']->value) {?>x-hidden<?php }?>">

        <div class="post-media">

            <div class="post-media-meta"> <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.body.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_post'=>$_smarty_tpl->tpl_vars['_post']->value['origin'],'_shared'=>true), 0, true);
?>
 </div>

        </div>

    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['_post']->value['post_type'] == "map") {?>
    <div class="post-map"><img
                src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $_smarty_tpl->tpl_vars['_post']->value['location'];?>
&amp;zoom=13&amp;size=600x250&amp;maptype=roadmap&amp;markers=color:red%7C<?php echo $_smarty_tpl->tpl_vars['_post']->value['location'];?>
&amp;key=<?php echo $_smarty_tpl->tpl_vars['system']->value['geolocation_key'];?>
"
                width="100%"></div>
<?php }?>

<!-- product -->

<?php if ($_smarty_tpl->tpl_vars['_post']->value['post_type'] == "product" && $_smarty_tpl->tpl_vars['_post']->value['product'] && $_smarty_tpl->tpl_vars['_post']->value['author_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
    <div class="mt10 clearfix">

        <button type="button" class="btn btn-primary btn-sm pull-right flip js_chat-start"
                data-uid="<?php echo $_smarty_tpl->tpl_vars['_post']->value['author_id'];?>
"
                data-name="<?php if ($_smarty_tpl->tpl_vars['_post']->value['post_author_name']) {
echo $_smarty_tpl->tpl_vars['_post']->value['post_author_name'];
}?>"><i
                    class="fa fa-comments-o"></i> <?php echo __("Contact Seller");?>
</button>

    </div>
<?php }?>

<!-- product -->

<?php echo '<script'; ?>
>
$('.loader-on').click(function(){
	$(this).find('.loader.loader_middium').removeClass('ld-hide');
});
 function video_play(obj){
        //alert(obj);
        //obj.getElementsByClassName('mejs__overlay-play')[0].click();
        //obj.getElementsByTagName('video')[0].autoplay = true;      
        //obj.getElementsByClassName('mejs__overlay-button')[0].click();
        obj.getElementsByTagName('video')[0].play();
        //alert();mejs__overlay-play
    }

function video_stop(obj){
    obj.getElementsByTagName('video')[0].pause();
}
<?php echo '</script'; ?>
>
<?php }
}
