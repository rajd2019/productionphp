<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:12:15
  from "/var/app/current/content/themes/default/templates/__feeds_post.comments.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c58726fc548f6_15586916',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '03765850da7fc7f81171c50282fe6406dcffa25d' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/__feeds_post.comments.tpl',
      1 => 1536745018,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:__feeds_comment.form.tpl' => 2,
    'file:__feeds_comment.tpl' => 2,
  ),
),false)) {
function content_5c58726fc548f6_15586916 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style>
	.comment-outer .pull-right{
		float:none !important;
	}
	.comment-edit .x-form.comment-form div.edit-textarea, .post_custom #replaceTextarea, div#replaceTextarea_cmt_frm {
		padding: 10px 31px 10px 10px;
		border: 1px solid #ccc;
		-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.1);
		-moz-box-shadow: inset 0 1px 1px rgba(0,0,0,0.1);
		box-shadow: inset 0 1px 1px rgba(0,0,0,0.1);
		border-radius: 4px;
		background:#fff;
	}
	.comment-edit .x-form.comment-form div.edit-textarea textarea, #replaceTextarea textarea#post_comment_box_id, .comment-form textarea#wwww {
		height: auto !important;
		overflow-y: auto !important;
		border: 0;
		padding: 0;
		box-shadow: none;
	}
	 .comment-outer .x-form-tools-emoji.js_emoji-menu-toggle.picker3 {
		bottom: 13px;
		top: auto !important;
	}
	.comment-edit .x-form.comment-form .x-form-tools-emoji.js_emoji-menu-toggle{
		bottom: 4px;
	}
	 .comment-outer .x-form-tools {
		top: 5px;
	}
	.comment-edit .x-form.comment-form .x-form-tools-attach{
		top: 15px;
	}
	.comment-outer .lsx-emojipicker-container {
		top: auto !important;
		right: 0px;
		bottom: 32px !important;
	}
	.modal-dialog .post_custom .form-group {
		margin: 0;
		position: relative;
	}
	.post_custom .publisher-tools-attach.js_publisher-photos.newalbumClass.picker2 {
		position: absolute;
		right: 6px;
		bottom: 4px;
		display: inline-block;
		width: auto !important;
	}
	.form-group .fa.fa-smile-o.fa-fw {
		width: auto;
		font-size: 21px;
	}
	form[data-url="/data/report.php"] .form-group {
		padding: 15px;
		padding-bottom: 0;
	}
	.custom-post-comment {
		margin: 0 -15px !important;
	}
	#modal p {
		padding: 15px;
		padding-bottom: 0;
		margin-bottom: 0;
	}
	.slimScrollBar {
		background: #ccc !important;
	}
	.comment-text{
		line-height:20px;
	}
	.comment-btn.dropdown .tooltip.top {
		left: auto !important;
		right: 0;
	}
	.comment-btn.dropdown .tooltip-arrow {
		right: 2px !important;
		left: auto !important;
	}
	@media screen and (min-width:768px){
		div#modal {
			margin: 0 0 0 -17px;
		}
		/*.modal-body.post-hover-content {
			max-height: calc(100vh - 61px);
			overflow-y: auto !important;
		}
		.modal-body.post-hover-content .slimScrollDiv {
			height: calc(100vh - 61px);
		}
		.modal-body.post-hover-content .js_scroller{
			height: calc(100vh - 61px);
		}*/
	}
	@media screen and (max-width:767px){
		.parent-comment-section .comment {
			padding-bottom: 0;
		}
		.comment .post-actions {
			margin: 0;
		}
	}
</style>
<div class="custom-post-comment">

<div class="post-comments">



    <?php if ($_smarty_tpl->tpl_vars['_is_photo']->value) {?>



         <!-- post a comment -->

        <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>

            <div class="modal-body">

					<div class="comment-outer">

						<?php $_smarty_tpl->_subTemplateRender('file:__feeds_comment.form.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_handle'=>'photo','_id'=>$_smarty_tpl->tpl_vars['photo']->value['photo_id']), 0, false);
?>


						<div class="pull-right flip mt5 mr10 custom-comment-button" style="display: none;">

							<button type="button" id="detail-hover-submitComment"  class="btn btn-primary" disabled="disabled"><?php echo __("Post");?>
</button>

						</div>
					</div>

            </div>

        <?php }?>

        <!-- post a comment -->



        <!-- comments -->

        <div class="parent-comment-section">

        <ul class="js_comments">

            <?php if ($_smarty_tpl->tpl_vars['photo']->value['comments'] > 0) {?>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['photo']->value['photo_comments'], 'comment');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->value) {
?>

                <?php $_smarty_tpl->_subTemplateRender('file:__feeds_comment.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_comment'=>$_smarty_tpl->tpl_vars['comment']->value), 0, true);
?>


                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


            <?php }?>

        </ul>

        </div>

        <!-- comments -->
 <!-- previous comments -->

        <?php if ($_smarty_tpl->tpl_vars['photo']->value['comments'] >= $_smarty_tpl->tpl_vars['system']->value['min_results']) {?>

            <div class="pb10 text-center js_see-more-comments" id="more-comments-id" data-get="photo_comments" data-id="<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" data-remove="true" style="display:block">

                <span class="text-link">

                    <i class="fa fa-comment-o"></i>

                    <?php echo __("View previous comments");?>


                </span>

                <div class="loader loader_small x-hidden"></div>

            </div>

        <?php }?>

        <!-- previous comments -->




    <?php } else { ?>



        <!-- post a comment -->

        <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>

            <div class="modal-body">
				<div class="comment-outer">

					<?php $_smarty_tpl->_subTemplateRender('file:__feeds_comment.form.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_handle'=>'post','_id'=>$_smarty_tpl->tpl_vars['post']->value['post_id']), 0, true);
?>


					<div class="pull-right flip mt5 mr10 custom-comment-button" style="display: none;">

					   <!--<button type="button" id="detail-hover-submitComment"  class="btn btn-primary" disabled="disabled"><?php echo __("Post");?>
</button>-->

					   <button type="button" id="detail-hover-submitComment"  class="btn btn-primary profile-comment"><?php echo __("Post");?>
</button>

					</div>
				</div>
            </div>




        <?php }?>



        <!-- post a comment -->






        <!-- comments -->

        <div class="parent-comment-section">
        <div class="loading" id="post_comments_loading_id" style="display: none">
                <div class="loader loader_medium"></div>
        </div>

        <ul class="js_comments">

            <?php if ($_smarty_tpl->tpl_vars['post']->value['comments'] > 0) {?>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['post']->value['post_comments'], 'comment');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->value) {
?>

                <?php $_smarty_tpl->_subTemplateRender('file:__feeds_comment.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_comment'=>$_smarty_tpl->tpl_vars['comment']->value), 0, true);
?>


                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


            <?php }?>

        </ul>

        </div>

        <!-- comments -->



         <!-- previous comments -->


        <?php if (count($_smarty_tpl->tpl_vars['post']->value['post_comments']) >= $_smarty_tpl->tpl_vars['system']->value['min_results']) {?>

            <div class="pb10 text-center js_see-more-comments" id="more-comments-id" data-get="post_comments" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" data-remove="true" style="display:block">

                <span class="text-link">

                    <i class="fa fa-comment-o"></i>

                    <?php echo __("View previous comments");?>


                </span>

                <div class="loader loader_small x-hidden"></div>

            </div>

        <?php }?>

        <!-- previous comments -->




    <?php }?>

</div>

</div>
<style type="text/css">
    #post_comments_loading_id .loader.loader_medium {

    display: inherit;

}
</style>
<?php }
}
