<?php
/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {html_image} function plugin
 * Type:     function<br>
 * Name:     html_image<br>
 * Date:     Feb 24, 2003<br>
 * Purpose:  format HTML tags for the image<br>
 * Examples: {html_image file="/images/masthead.gif"}<br>
 * Output:   <img src="/images/masthead.gif" width=400 height=23><br>
 * Params:
 * <pre>
 * - file        - (required) - file (and path) of image
 * - height      - (optional) - image height (default actual height)
 * - width       - (optional) - image width (default actual width)
 * - basedir     - (optional) - base directory for absolute paths, default is environment variable DOCUMENT_ROOT
 * - path_prefix - prefix for path output (optional, default empty)
 * </pre>
 *
 * @link    http://www.smarty.net/manual/en/language.function.html.image.php {html_image}
 *          (Smarty online manual)
 * @author  Monte Ohrt <monte at ohrt dot com>
 * @author  credits to Duda <duda@big.hu>
 * @version 1.0
 *
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 *
 * @throws SmartyException
 * @return string
 * @uses    smarty_function_escape_special_chars()
 */
require_once(ABSPATH . 'includes/class-custom-user.php'); 
 
function smarty_function_get_photo($params)
{    
	$user = new CustomUser();
	global $db, $system, $date;
	
	$photo_id = $params['param1'];
	$full_details = false;
	$get_gallery = false;
	$context = 'album';
	
	/* get photo */
        $get_photo = $db->query(sprintf("SELECT * FROM posts_photos WHERE photo_id = %s", secure($photo_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_photo->num_rows == 0) {
            return false;
        }
        $photo = $get_photo->fetch_assoc();
        /* get post */
        $post = $user->_check_post($photo['post_id'], false, $full_details);
        if (!$post) {
            return false;
        }
        $check_like = $db->query(sprintf("SELECT * FROM posts_likes WHERE user_id = %s AND post_id = %s", secure($user->_data['user_id'], 'int'), secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $photo['i_like'] = ($check_like->num_rows > 0) ? true : false;
        /* check if photo can be deleted */
        if ($post['in_group']) {
            /* check if (cover|profile) photo */
            $photo['can_delete'] = (($photo_id == $post['group_picture_id']) or ($photo_id == $post['group_cover_id'])) ? false : true;
        } elseif ($post['in_event']) {
            /* check if (cover) photo */
            $photo['can_delete'] = (($photo_id == $post['event_cover_id'])) ? false : true;
        } elseif ($post['user_type'] == "user") {
            /* check if (cover|profile) photo */
            $photo['can_delete'] = (($photo_id == $post['user_picture_id']) or ($photo_id == $post['user_cover_id'])) ? false : true;
        } elseif ($post['user_type'] == "page") {
            /* check if (cover|profile) photo */
            $photo['can_delete'] = (($photo_id == $post['page_picture_id']) or ($photo_id == $post['page_cover_id'])) ? false : true;
        }
        /* check photo type [single|mutiple] */
        $check_single = $db->query(sprintf("SELECT * FROM posts_photos WHERE post_id = %s", secure($photo['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $photo['is_single'] = ($check_single->num_rows > 1) ? false : true;
        /* get full details */
        if ($full_details) {
            if ($photo['is_single']) {
                /* single photo => get (likes|comments) of post */
                /* check if viewer likes this post */
                if ($user->_logged_in && $post['likes'] > 0) {
                    $check_like = $db->query(sprintf("SELECT * FROM posts_likes WHERE user_id = %s AND post_id = %s", secure($user->_data['user_id'], 'int'), secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    $photo['i_like'] = ($check_like->num_rows > 0) ? true : false;
                }
                /* get post comments */
                if ($post['comments'] > 0) {
                    $post['post_comments'] = $user->get_comments($post['post_id'], 0, true, true, $post);
                }
            } else {
                /* mutiple photo => get (likes|comments) of photo */
                /* check if viewer user likes this photo */
                if ($user->_logged_in && $photo['likes'] > 0) {
                    $check_like = $db->query(sprintf("SELECT * FROM posts_photos_likes WHERE user_id = %s AND photo_id = %s", secure($user->_data['user_id'], 'int'), secure($photo['photo_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    $photo['i_like'] = ($check_like->num_rows > 0) ? true : false;
                }
                /* get post comments */
                if ($photo['comments'] > 0) {
                    $photo['photo_comments'] = $user->get_comments($photo['photo_id'], 0, false, true, $post);
                }
            }
        }
        /* get gallery */
        if ($get_gallery) {
            switch ($context) {
                case 'post':
                    $get_post_photos = $db->query(sprintf("SELECT photo_id, source FROM posts_photos WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    while ($post_photo = $get_post_photos->fetch_assoc()) {
                        $post_photos[$post_photo['photo_id']] = $post_photo;
                    }
					
                    $c_val = (int) count($post_photos);
					
					if(empty($post_photos[get_array_key($post_photos, $photo['photo_id'], 1)])){
						$photo['next'] = $post_photos[get_array_key($post_photos, $photo['photo_id'], -($c_val - 1))];
					}
					else{
						$photo['next'] = $post_photos[get_array_key($post_photos, $photo['photo_id'], 1)];
					}
					
					if(empty($post_photos[get_array_key($post_photos, $photo['photo_id'], 1)])){
						$photo['prev'] = $post_photos[get_array_key($post_photos, $photo['photo_id'], -($c_val - 1))];
					}
					else{
						$photo['prev'] = $post_photos[get_array_key($post_photos, $photo['photo_id'], 1)];
					}
					
                    //$photo['next'] = $post_photos[get_array_key($post_photos, $photo['photo_id'], 1)];
                    //$photo['prev'] = $post_photos[get_array_key($post_photos, $photo['photo_id'], 1)];
                    break;
                case 'album':
                    $get_album_photos = $db->query(sprintf("SELECT photo_id, source FROM posts_photos WHERE album_id = %s", secure($photo['album_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    while ($album_photo = $get_album_photos->fetch_assoc()) {
                        $album_photos[$album_photo['photo_id']] = $album_photo;
                    }
					
					$c_val = (int) count($album_photos);
					
					if(empty($album_photos[get_array_key($album_photos, $photo['photo_id'], 1)])){
						$photo['next'] = $album_photos[get_array_key($album_photos, $photo['photo_id'], -($c_val - 1))];
					}
					else{
						$photo['next'] = $album_photos[get_array_key($album_photos, $photo['photo_id'], 1)];
					}
					
					if(empty($album_photos[get_array_key($album_photos, $photo['photo_id'], 1)])){
						$photo['prev'] = $album_photos[get_array_key($album_photos, $photo['photo_id'], -($c_val - 1))];
					}
					else{
						$photo['prev'] = $album_photos[get_array_key($album_photos, $photo['photo_id'], 1)];
					}
					
                    //$photo['next'] = $album_photos[get_array_key($album_photos, $photo['photo_id'], -1)];
                    //$photo['prev'] = $album_photos[get_array_key($album_photos, $photo['photo_id'], 1)];
                    break;
                case 'photos':
                    if ($post['in_group']) {
                        $get_target_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts INNER JOIN posts_photos ON posts.post_id = posts_photos.post_id WHERE posts.in_group = '1' AND posts.group_id = %s", secure($post['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    } elseif ($post['in_event']) {
                        $get_target_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts INNER JOIN posts_photos ON posts.post_id = posts_photos.post_id WHERE posts.in_event = '1' AND posts.event_id = %s", secure($post['event_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    } elseif ($post['user_type'] == "page") {
                        $get_target_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts INNER JOIN posts_photos ON posts.post_id = posts_photos.post_id WHERE posts.user_type = 'page' AND posts.user_id = %s", secure($post['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    } elseif ($post['user_type'] == "user") {
                        $get_target_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts INNER JOIN posts_photos ON posts.post_id = posts_photos.post_id WHERE posts.user_type = 'user' AND posts.user_id = %s", secure($post['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    }
                    while ($target_photo = $get_target_photos->fetch_assoc()) {
                        $target_photos[$target_photo['photo_id']] = $target_photo;
                    }
					
					$c_val = (int) count($target_photos);
					
					if(empty($target_photos[get_array_key($target_photos, $photo['photo_id'], 1)])){
						$photo['next'] = $target_photos[get_array_key($target_photos, $photo['photo_id'], -($c_val - 1))];
					}
					else{
						$photo['next'] = $target_photos[get_array_key($target_photos, $photo['photo_id'], 1)];
					}
					
					if(empty($target_photos[get_array_key($target_photos, $photo['photo_id'], 1)])){
						$photo['prev'] = $target_photos[get_array_key($target_photos, $photo['photo_id'], -($c_val - 1))];
					}
					else{
						$photo['prev'] = $target_photos[get_array_key($target_photos, $photo['photo_id'], 1)];
					}
					
                    //$photo['next'] = $target_photos[get_array_key($target_photos, $photo['photo_id'], -1)];
                    //$photo['prev'] = $target_photos[get_array_key($target_photos, $photo['photo_id'], 1)];
                    break;
            }
        }
        /* return post array with photo */
        $photo['post'] = $post;
				
        //return $photo;
		
		//$smarty->assign('plug_photo_comments', $photo['post']['comments']);
		//$smarty->assign('plug_photo_likes', $photo['post']['likes']);
		//$smarty->assign('plug_photo_shares', $photo['post']['shares']);
		//$smarty->assign('plug_photo_text', html_entity_decode($photo['post']['text']));
		
		$return_text = "<p>".$photo['post']['text']."</p><p><span class='text-clickable mr20 js-custom-share-comment' ><i class='Icon Icon--medium Icon--reply'></i> <span >".$photo['post']['comments']."</span></span><span class='text-clickable mr20 share-tweet-post' ><i class='Icon Icon--medium Icon--retweet'></i> <span >".$photo['post']['shares']."</span></span><span class='text-clickable js_like-post' ><i class='Icon Icon--heartBadge Icon--medium'></i> <span >".$photo['post']['likes']."</span></span></p>";
		
		return $return_text;
	   
	}

