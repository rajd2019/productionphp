<?php
require_once('../../../bootstrap.php');

$sql = "SHOW TABLES LIKE `migration_settings`";
$currentVersion = 0;

if (mysqli_query($db, $sql)) {
    $select = "SELECT `current_version` FROM `migration_settings`";
    $currentVersion = mysqli_query($db, $select);
} else {
    $create = "CREATE TABLE `migration_settings` (`current_version` INT NOT NULL)";
    mysqli_query($db, $create);
    $insert = "INSERT INTO `migration_settings`(`current_version`) VALUES ('0')";
    mysqli_query($db, $insert);
}

$dir = new DirectoryIterator(dirname('../..'));
foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot() && !$fileinfo->isDir()
        && preg_match('/Migration(.+?).php/', $fileinfo->getFilename(), $matches)
        && $matches[1] > $currentVersion) {
        require_once ('../' . $fileinfo->getFilename());
        $migrationName = basename($fileinfo->getFilename(), '.php');
        $cls = 'Migrations\\' . $migrationName;
        $migration = new $cls($db);
        mysqli_query($db, $migration->up());
        $insertCurrentVersion = "INSERT INTO `migration_settings`(`current_version`) VALUES ('$matches[1]')";
        mysqli_query($db, $insertCurrentVersion);
    }
}