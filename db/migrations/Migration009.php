<?php

namespace Migrations;

require_once ('classes/KwokMigration.php');

class Migration009 extends KwokMigration
{
    public function up() {
        return 'CREATE TABLE `video_streams` (
            `video_stream_id` int(11) NOT NULL AUTO_INCREMENT,
            `user_id` int(11) DEFAULT NULL,
            `time` datetime DEFAULT NULL,
            `title` varchar(255) DEFAULT NULL,
            `description` longtext,
            `dacast_id` int(11) DEFAULT NULL,
            `dacast_login` varchar(255) DEFAULT NULL,
            `dacast_password` varchar(255) DEFAULT NULL,
            `dacast_publishing_point` varchar(255) DEFAULT NULL,
            `dacast_stream_name` varchar(255) DEFAULT NULL,
            PRIMARY KEY (`video_stream_id`)
        );       
       ';
    }

    public function down() {

    }
}
