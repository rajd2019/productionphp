<?php

namespace Migrations;

require_once ('classes/KwokMigration.php');

class Migration003 extends KwokMigration
{
    public function up() {
        return 'ALTER TABLE `users` ADD `ga_secret` VARCHAR(255) NULL DEFAULT NULL';
    }

    public function down() {

    }
}