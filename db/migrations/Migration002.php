<?php

namespace Migrations;

require_once ('classes/KwokMigration.php');

class Migration002 extends KwokMigration
{
    public function up() {
        return 'ALTER TABLE `users` ADD `login_verification` VARCHAR(255) NULL DEFAULT NULL';
    }

    public function down() {

    }
}