<?php

namespace Migrations;

require_once ('classes/KwokMigration.php');

class Migration001 extends KwokMigration
{
    public function up() {
        return 'ALTER TABLE `system_options` ADD `video_streaming_enabled` ENUM(\'0\',\'1\') NOT NULL DEFAULT \'0\'';
    }

    public function down() {

    }
}