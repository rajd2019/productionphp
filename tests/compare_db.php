<?php
// set system version
define('SYS_VER', '2.5.1');

// set absolut & base path
define('ABSPATH', dirname(__FILE__).'/../');
define('BASEPATH', dirname($_SERVER['PHP_SELF']));

// get system configurations
require_once(ABSPATH . 'includes/config.php');

// connect to the database
$db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$db->set_charset('utf8mb4');
if(mysqli_connect_error()) {
    _error(DB_ERROR);
}

//read structure from database

$structure_db = [];
$result_tables = $db->query("show tables");
if($result_tables->num_rows == 0) {
    die("Can't read list of tables");
}

while($t = $result_tables->fetch_array()) {
    $t_name = $t[0];
    $fields = [];
    $result_fields = $db->query("SHOW COLUMNS FROM $t_name");
    while($f = $result_fields->fetch_assoc()) {
        array_push($fields, $f['Field']);
    }

    $structure_db[$t_name] = $fields;
}

//read structure from install file

$structure = file_get_contents(ABSPATH . 'db/db_structure.sql');

$structure_install = [];
$table_name = '';
$fields = [];

$rows = explode("\n", $structure);
foreach ($rows as $row){
    if(preg_match("#CREATE TABLE\s+`(.*)`#", $row, $out)){
        $table_name = $out[1];
    }
    elseif(strpos($row, ';') > 0){
        if(!empty($table_name)){
            $structure_install[$table_name] = $fields;
        }
        $table_name = "";
        $fields = [];
    }
    elseif(preg_match("#`(.+)`\s+#", $row, $out)){
        array_push($fields, $out[1]);
    }
}

//compare tables
$tables_install = array_keys($structure_install);
$tables_db = array_keys($structure_db);
$tables_diff = array_unique(array_merge(array_diff($tables_db, $tables_install), array_diff($tables_install, $tables_db)));

//compare fields
$fields_diff = [];
$tables_all = array_unique(array_merge($tables_install, $tables_db));
foreach ($tables_all as $table_name){
    if(!array_key_exists($table_name, $structure_db) || !array_key_exists($table_name, $structure_install)){
        continue;
    }

    $structure_db_fields = $structure_db[$table_name];
    $structure_install_fields = $structure_install[$table_name];
    $f_diff = array_unique(array_merge(array_diff($structure_db_fields, $structure_install_fields), array_diff($structure_install_fields, $structure_db_fields)));
    if(sizeof($f_diff) > 0) {
        $fields_diff[$table_name] = $f_diff;
    }
}

echo "Tables is same?: " . ((sizeof($tables_diff) > 0) ? "NO" : "YES") . "\n";
echo "Fields is same?: " . ((sizeof($fields_diff) > 0) ? "NO" : "YES") . "\n";

echo "Tables diff: \n";
print_r($tables_diff);

echo "Fields diff: \n";
print_r($fields_diff);
