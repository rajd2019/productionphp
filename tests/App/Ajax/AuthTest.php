<?php

namespace Tests\App\Ajax;

use \Tests\App\BaseTest;

class AuthTest extends BaseTest
{
  public function testAjaxSignin()
  {
    $response = $this->http->request(
      'POST',
      '/includes/ajax/core/signin.php',
      [
        'headers' => [
          'X-Requested-With' => 'XMLHttpRequest',
        ],
        'form_params' => [
          'username_email' => 'wrong@email',
          'password' => 'wrong@password'
        ]
      ]
    );
    $this->assertEquals(200, $response->getStatusCode());
    $array = json_decode($response->getBody()->getContents(), true);
    $this->assertArrayHasKey('error', $array);
    $this->assertArrayHasKey('message', $array);
    $this->assertTrue($array['error']);
  }
}
