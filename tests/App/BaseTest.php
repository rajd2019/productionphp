<?php

namespace Tests\App;

use PHPUnit\Framework\TestCase;

abstract class BaseTest extends TestCase
{
  protected $http;
  protected $jar;

  public function setUp()
  {
    $this->jar = new \GuzzleHttp\Cookie\CookieJar();
    $this->http = new \GuzzleHttp\Client(['base_uri' => TEST_SYS_URL, 'exceptions' => false, 'debug' => false, 'cookie' => true]);
  }

  public function tearDown()
  {
    $this->http = null;
  }

  public function logIn()
  {
    $this->http->request('POST', '/includes/ajax/core/signin.php', [
      'cookies' => $this->jar,
      'headers' => [
        'X-Requested-With' => 'XMLHttpRequest'
      ],
      'form_params' => [
        'username_email' => TEST_USER_EMAIL,
        'password' => TEST_USER_PASSWORD
      ]
    ]);
  }
}
