<?php

namespace Tests\App\Web;

use Tests\App\BaseTest;

class DoBroadcastsTest extends BaseTest
{
  public function testDoBroadcastUser()
  {
    $this->login();
    $response = $this->http->request('GET', '/do_broadcast', ['cookies' => $this->jar]);
    $this->assertEquals(200, $response->getStatusCode());
  }

  public function testDoBroadcastVisitor()
  {
    $response = $this->http->request('GET', '/do_broadcast');
    $this->assertEquals(404, $response->getStatusCode());
  }
}
