<?php
/**
 * Created by PhpStorm.
 * User: vitaliy
 * Date: 3/8/18
 * Time: 5:26 PM
 */
namespace Tests\App\Web;

use Tests\App\BaseTest;

class HomeTest extends BaseTest
{
    public function testHome()
    {
        $response = $this->http->request('GET', '/');
        $this->assertEquals(200, $response->getStatusCode());
    }
}
