<?php

namespace Tests\App\Web;

use Tests\App\BaseTest;

class UserTest extends BaseTest
{
  public function testUserPosts()
  {
    $response = $this->http->request('GET', '/' . TEST_USER_EMAIL);
    $this->assertEquals(200, $response->getStatusCode());
  }

  public function testUserFollowings()
  {
    $response = $this->http->request('GET', '/followings.php?username=' . TEST_USER_EMAIL);
    $this->assertEquals(200, $response->getStatusCode());
  }
  public function testUserFollowers()
  {
    $response = $this->http->request('GET', '/followers.php?username=' . TEST_USER_EMAIL);
    $this->assertEquals(200, $response->getStatusCode());
  }
  public function testUserLikes()
  {
    $response = $this->http->request('GET', '/likes.php?username=' . TEST_USER_EMAIL);
    $this->assertEquals(200, $response->getStatusCode());
  }
  public function testUserBroadcasts()
  {
    $response = $this->http->request('GET', '/post_broadcast.php?username=' . TEST_USER_EMAIL);
    $this->assertEquals(200, $response->getStatusCode());
  }

  public function testUserMedia()
  {
    $response = $this->http->request('GET', '/media.php?username=' . TEST_USER_EMAIL);
    $this->assertEquals(200, $response->getStatusCode());
  }
}
