<?php

namespace Tests\App\Web;

use \Tests\App\BaseTest;

class AuthTest extends BaseTest
{
  public function testPasswordReset()
  {
    $response = $this->http->request('GET', '/reset');
    $this->assertEquals(200, $response->getStatusCode());
  }

  public function testSignin()
  {
    $response = $this->http->request('GET', '/signin');
    $this->assertEquals(200, $response->getStatusCode());
  }
}
