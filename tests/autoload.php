<?php
$_SERVER['SERVER_NAME'] = "localhost:8888";
$_SERVER['REQUEST_SCHEME']  = 'http';

require_once __DIR__ . '/../includes/config.php';
include_once __DIR__.'/../vendor/autoload.php';

$classLoader = new \Composer\Autoload\ClassLoader();
$classLoader->addPsr4('Tests\\App\\', __DIR__.'/App', true);
$classLoader->register();
