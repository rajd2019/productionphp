<?php
declare(strict_types=1);

require_once(__DIR__ . "/../includes/config.php");
require_once(__DIR__ . "/../includes/class-aws-s3.php");

use PHPUnit\Framework\TestCase;

final class AwsS3Test extends TestCase
{
    public function testBucketIsPresent(): void
    {
        $this->assertTrue(AwsS3::is_present_bucket(S3_BUCKET));
    }

    public function testBucketNotPresent(): void
    {
        $this->assertFalse(AwsS3::is_present_bucket(time()));
    }

    public function testUploadAndDeleteImage(): void
    {
        $this->assertTrue(AwsS3::upload(__DIR__ . "/assets/test.png", 'test.png'));
        $this->assertTrue(AwsS3::delete_object('test.png'));
    }
}
