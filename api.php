<?php
ini_set("display_errors", 0);
/**
 * api
 * 
 * @package Sngine
 * @author Zamblek
 */
//var_dump($_POST);
//var_dump($_GET);
//var_dump($_REQUEST);
$json = file_get_contents('php://input');
$input = json_decode($json);

// fetch bootstrap
require_once(__DIR__ . '/bootstrap.php');
$_GET['query'] = $input->query;
$_GET['get'] = $input->get;
// valid inputs
if(!isset($_GET['query']) || is_empty($_GET['query'])) {
	return_json( array('error' => true, 'message' => "Bad Request, query is missing") );
}
// get data
try {

	// initialize the return array
	$return = array();

	switch ($_GET['get']) {
		case 'users':
			/* get users */
			$get_users = $db->query(sprintf('SELECT user_id, user_name, user_firstname, user_gender, user_picture, user_cover, user_registered, user_verified FROM users WHERE user_name LIKE %1$s OR user_firstname LIKE %1$s ORDER BY user_firstname ASC LIMIT %2$s', secure($_GET['query'], 'search'), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
			if($get_users->num_rows > 0) {
				while($user = $get_users->fetch_assoc()) {
					$user['user_picture'] = User::get_picture($user['user_picture'], $user['user_gender']);
					$return[] = $user;
				}
			}
			break;

		case 'pages':
			/* get pages */
			$get_pages = $db->query(sprintf('SELECT * FROM pages WHERE page_name LIKE %1$s OR page_title LIKE %1$s ORDER BY page_title ASC LIMIT %2$s', secure($_GET['query'], 'search'), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
			if($get_pages->num_rows > 0) {
				while($page = $get_pages->fetch_assoc()) {
					$page['page_picture'] = User::get_picture($page['page_picture'], 'page');
					$return[] = $page;
				}
			}
			break;

		case 'groups':
			/* get groups */
			$get_groups = $db->query(sprintf('SELECT * FROM groups WHERE group_name LIKE %1$s OR group_title LIKE %1$s ORDER BY group_title ASC LIMIT %2$s', secure($_GET['query'], 'search'), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
			if($get_groups->num_rows > 0) {
				while($group = $get_groups->fetch_assoc()) {
					$group['group_picture'] = User::get_picture($group['group_picture'], 'group');
					$return[] = $group;
				}
			}
			break;
			
		case 'login':
			/* get login */
			echo json_encode($user->sign_in($input->username, $input->password, 'mobile'));//
			exit;
			break;
			
		case 'signup':
			/* get signup  */ // email, first_name, last_name, username, password
			$args = array('email' => $input->email, 'api' => true, 'first_name' => $input->first_name, 'last_name' => $input->last_name, 'username' => $input->username, 'password' => $input->password);
			$signup = $user->sign_up($args);
			
			if(isset($signup)){
				echo json_encode($signup);
			}
			else{
				$ar = $user->sign_in($input->username, $input->password, 'mobile');
				$user->settings_api('email', $args = array('user_id' => $ar['response']['user_id'], 'email' => $input->email));
				echo json_encode($user->sign_in($input->username, $input->password, 'mobile'));
			}
			exit;
			break;
			
		case 'forget_password':
			/* get forget_password */
			echo json_encode($user->forget_password($input->email, 'mobile'));
			exit;
			break;

		case 'get_posts':
			/* get posts */
			if(!isset($input->limit)){
				$input->limit = 10;
			}
			if(!isset($input->page)){
				$input->page = 1;
			}
			if(!isset($input->me_id)){
				$input->me_id = '';
			}
			$arr = $user->_get_posts_api($input->user_id, $input->me_id, $input->limit, $input->page);
			//var_dump($arr);
			$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;

		case 'get_broadcasts':
			/* get broadcasts */
			$arr = $user->_get_broadcast_api($input->user_id);
			//var_dump($arr);
			$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;

		case 'like_post':
			/* like post */
			$arr = $user->like_post_api($input->post_id, $input->user_id);
			//var_dump($arr);
			//$response = array('status'=>'SUCCESS', 'message'=>'post like successfully.', 'response' => $arr);
			echo json_encode($arr);
			exit;
			break;

		case 'like_comment':
			/* like post */
			$arr = $user->like_comment_api($input->comment_id, $input->user_id);
			//var_dump($arr);
			//$response = array('status'=>'SUCCESS', 'message'=>'post like successfully.', 'response' => $arr);
			echo json_encode($arr);
			exit;
			break;
			

		case 'unlike_post':
			/* like post */
			$arr = $user->unlike_post_api($input->post_id, $input->user_id);
			//var_dump($arr);
			//$response = array('status'=>'SUCCESS', 'message'=>'post unlike successfully.', 'response' => $arr);
			echo json_encode($arr);
			exit;
			break;

		case 'unlike_comment':
			/* like post */
			$arr = $user->unlike_comment_api($input->comment_id, $input->user_id);
			//var_dump($arr);
			//$response = array('status'=>'SUCCESS', 'message'=>'post unlike successfully.', 'response' => $arr);
			echo json_encode($arr);
			exit;
			break;
			
		case 'get_followers':
			/* get_followers */
			if(!isset($input->limit)){
				$input->limit = 10;
			}
			if(!isset($input->page)){
				$input->page = 1;
			}
			if(!isset($input->me_id)){
				$input->me_id = '';
			}
			if(!isset($input->keyword)){
				$arr = $user->get_followers_api($input->user_id, $input->me_id, $input->limit, $input->page);
			}
			else{
				$arr = $user->get_followers_api($input->user_id, $input->me_id, '-1', $input->page);
				$arr2 = array();
				foreach($arr['response'] as $key => $value){
					if(strpos(strtolower(json_encode($value)), strtolower($input->keyword))){
						$arr2[] = $value;
					}
				}
				$arr['response'] = $arr2;
			}
			//var_dump($arr);
			//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($arr);
			exit;
			break;

		case 'get_followings':
			/* get followings */
			if(!isset($input->limit)){
				$input->limit = 10;
			}
			if(!isset($input->page)){
				$input->page = 1;
			}
			if(!isset($input->me_id)){
				$input->me_id = '';
			}

			if(!isset($input->keyword)){
				$arr = $user->get_followings_api($input->user_id, $input->me_id, $input->limit, $input->page);
			}
			else{
				$arr = $user->get_followings_api($input->user_id, $input->me_id, '-1', $input->page);
				$arr2 = array();
				foreach($arr['response'] as $key => $value){
					if(strpos(strtolower(json_encode($value)), strtolower($input->keyword))){
						$arr2[] = $value;
					}
				}
				$arr['response'] = $arr2;
			}
			echo json_encode($arr);
			exit;
			break;

		case 'follow':
			/* follow */
			$arr = $user->follow_api($input->user_id, $input->following_id);
			//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($arr);
			exit;
			break;
			
		case 'unfollow':
			/* unfollow */
			$arr = $user->unfollow_api($input->user_id, $input->following_id);
			//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($arr);
			exit;
			break;
			
		case 'notification':
			/* Notification list */
			if(!isset($input->limit)){
				$input->limit = 10;
			}
			if(!isset($input->page)){
				$input->page = 1;
			}
			$arr = $user->get_notifications_api($input->user_id, $input->limit, $input->page);
			$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;
			
		case 'comments':
			/* comments */
			$arr = $user->comment_api($input->user_id, $input->handle, $input->node_id, $input->message, $input->photo);
			//$response = array('status'=>'SUCCESS', 'message'=>'Comment posted successfully.', 'response' => $arr);
			echo json_encode($arr);
			exit;
			break;
			
		case 'share':
			/* share post */
			$arr = $user->share_api($input->user_id, $input->post_id, $input->comment);
			//$response = array('status'=>'SUCCESS', 'message'=>'Comment posted successfully.', 'response' => $arr);
			echo json_encode($arr);
			exit;
			break;
			
		case 'edit_info':
			/* edit info */
			if($input->user_id && $input->about_me){
				$response = $user->settings_api('basic', $args = array('user_id' => $input->user_id, 'firstname' => $input->firstname, 'lastname' => $input->lastname, 'biography'=>$input->about_me));
				if($response['status'] != 'ERROR'){
					if(isset($input->email)){
						$response = $user->settings_api('email', $args = array('user_id' => $input->user_id, 'email' => $input->email));
						if($response['status'] == 'ERROR'){
							echo json_encode($response);
							exit;
						}
					}
					if(isset($input->username)){
						$response = $user->settings_api('username', $args = array('user_id' => $input->user_id, 'username' => $input->username));
						if($response['status'] == 'ERROR'){
							echo json_encode($response);
							exit;
						}
					}
				}
			}
			else{
				$response = array('status'=>'ERROR', 'message'=>'Please check all parameters.', 'response' => $arr);
			}
			echo json_encode($response);
			exit;
			break;

		case 'change_email':
			/* change email */
			if($input->user_id && $input->email){
				$response = $user->settings_api('email', $args = array('user_id' => $input->user_id, 'email' => $input->email));
			}
			echo json_encode($response);
			exit;
			break;
			
		case 'change_password':
			/* change password */
			if($input->user_id && $input->new && $input->confirm){
				$response = $user->settings_api('password', $args = array('user_id' => $input->user_id, 'new' => $input->new, 'confirm' => $input->confirm));
			}
			echo json_encode($response);
			exit;
			break;
			
		case 'change_username':
			/* change username */
			if($input->user_id && $input->username){
				$response = $user->settings_api('username', $args = array('user_id' => $input->user_id, 'username' => $input->username));
			}
			echo json_encode($response);
			exit;
			break;
			
		case 'privacy':
			/* change username */
			//if($input->user_id && $input->username){
				$response = $user->settings_api('privacy', $args = array('user_id' => $input->user_id, 'user_privacy_message' => $input->user_privacy_message));
			//}
			echo json_encode($response);
			exit;
			break;
			
		case 'get_blocked_users':
			/* get_blocked_users */
			//if($input->user_id && $input->username){
				$response = $user->blocked_user_list_api($input->user_id);
			//}
			echo json_encode($response);
			exit;
			break;
			
		case 'get_post':
			/* post detail */
			if(!isset($input->me_id) || $input->me_id == ''){
				$input->me_id = '';
			}
			if($input->post_id){
				$response = $user->get_post_api($input->post_id, $input->me_id);
				if(isset($response['status'])){
					echo json_encode($response);
					exit;
				}
				else{
					$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $response);
				}
			}
			else{
				$response = array('status'=>'ERROR', 'message'=>'Post id is missing.', 'response' => null);
			}
			
			echo json_encode($response);
			exit;
			break;
			
		case 'get_likes_posts':
			/* get_likes_posts_api */
			if(!isset($input->limit)){
				$input->limit = 10;
			}
			if(!isset($input->page)){
				$input->page = 1;
			}
			$response = $user->get_likes_posts_api($input->user_id, $input->limit, $input->page);
			//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;
			
		case 'delete_post':
			/* delete_post */
			$response = $user->delete_post_api($input->post_id);
			//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;

		case 'mute_post':
			/* mute_post */
			$response = $user->hide_post_api($input->user_id, $input->post_id);
			//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;			
			
		case 'unmute_post':
			/* unmute_post */
			$response = $user->unhide_post_api($input->user_id, $input->post_id);
			//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;
			
		case 'change_picture':
			/* unmute_post */
			$response = $user->change_picture_api($input->user_id, $input->picture, $input->type);
			//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;
			
		case 'get_user_profile':
			/* unmute_post */
			if(!isset($input->me_id) || $input->me_id == ''){
				$input->me_id = '';
			}
			$response = $user->get_user_api($input->user_id, $input->me_id);
			//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;
			
		case 'block_user':
			/* unmute_post */
			$response = $user->connect_api('block', $input->user_id, $input->block_id);
			//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;
			
		case 'unblock_user':
			/* unmute_post */
			$response = $user->connect_api('unblock', $input->user_id, $input->block_id);
			//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;

		case 'report_post':
			/* report_api */
			$response = $user->report_api($input->user_id, $input->post_id, 'post', $input->comments);
			//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;

		case 'get_counts':
			/* get_counts */
			$response = $user->get_counts_api($input->user_id);
			//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;			
		case 'add_post':
			/* get_counts */
			$response = $user->add_post_api($input->user_id, $input->post_text, $input->media_files);
			//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;

		case 'search':
			if(!isset($input->limit)){
				$input->limit = 10;
			}
			if(!isset($input->page)){
				$input->page = 1;
			}

			if(isset($input->type) && $input->type=='users'){
				$response = $user->get_users_api($input->keyword, $input->user_id, $input->limit, $input->page);
			}
			elseif(isset($input->type) && $input->type=='posts'){
				$response = $user->_search_posts_api($input->user_id, 'posts', $input->keyword, $input->limit, $input->page);
				if(count($response) == 0){
					$response = array('status'=>'SUCCESS', 'message'=>"No message.", 'response' => $response);
					//exit;
				}
				else{
					$response = array('status'=>'SUCCESS', 'message'=>"No message.", 'response' => $response);
					//exit;
				}
			}
			elseif(isset($input->type) && $input->type=='video'){
				$response = $user->_search_posts_api($input->user_id, 'video', $input->keyword, $input->limit, $input->page);
				if(count($response) == 0){
					$response = array('status'=>'SUCCESS', 'message'=>"No message.", 'response' => $response);
					//exit;
				}
				else{
					$response = array('status'=>'SUCCESS', 'message'=>"No message.", 'response' => $response);
					//exit;
				}
			}
			elseif(isset($input->type) && $input->type=='photos'){
				$response = $user->_search_posts_api($input->user_id, 'photos', $input->keyword, $input->limit, $input->page);
				if(count($response) == 0){
					$response = array('status'=>'SUCCESS', 'message'=>"No message.", 'response' => $response);
					//exit;
				}
				else{
					$response = array('status'=>'SUCCESS', 'message'=>"No message.", 'response' => $response);
					//exit;
				}
			}
			elseif(isset($input->type) && $input->type=='audios'){
				$response = $user->_search_posts_api($input->user_id, 'audio', $input->keyword, $input->limit, $input->page);
				if(count($response) == 0){
					$response = array('status'=>'SUCCESS', 'message'=>"No message.", 'response' => $response);
					//exit;
				}
				else{
					$response = array('status'=>'SUCCESS', 'message'=>"No message.", 'response' => $response);
					//exit;
				}
			}
			elseif(isset($input->type) && $input->type=='global'){
				$response = $user->_search_posts_api($input->user_id, 'global', $input->keyword, $input->limit, $input->page);
				if(count($response) == 0){
					$response = array('status'=>'SUCCESS', 'message'=>"No message.", 'response' => $response);
					//exit;
				}
				else{
					$response = array('status'=>'SUCCESS', 'message'=>"No message.", 'response' => $response);
					//exit;
				}
			}
			elseif(isset($input->type) && $input->type=='mymedia'){
				$response = $user->_search_posts_api($input->user_id, 'mymedia', $input->keyword, $input->limit, $input->page);
				if(count($response) == 0){
					$response = array('status'=>'SUCCESS', 'message'=>"No message.", 'response' => $response);
					//exit;
				}
				else{
					$response = array('status'=>'SUCCESS', 'message'=>"No message.", 'response' => $response);
					//exit;
				}
			}
			elseif(isset($input->type) && $input->type=='media'){
				$response = $user->_search_posts_api($input->user_id, 'media', $input->keyword, $input->limit, $input->page);
				if(count($response) == 0){
					$response = array('status'=>'SUCCESS', 'message'=>"No message.", 'response' => $response);
					//exit;
				}
				else{
					$response = array('status'=>'SUCCESS', 'message'=>"No message.", 'response' => $response);
					//exit;
				}
			}
			else{
				$response = array('status'=>'ERROR', 'message'=>'parameter `type` is missing.', 'response' => null);
			}//$response = array('status'=>'SUCCESS', 'message'=>'no message', 'response' => $arr);
			echo json_encode($response);
			exit;
			break;
			
		case 'delete_user':
			/* delete user */
			if($input->user_id && $input->username && $input->password){
				$arr = $user->sign_in($input->username, $input->password, 'mobile');
				if($arr['status'] == "SUCCESS" && $arr['response']['user_id'] == $input->user_id){
					$response = $user->delete_user_api($input->user_id);
				}
				else{
					$response = $arr;
				}
			}
			echo json_encode($response);
			exit;
			break;

		case 'update_push_token':
			/* delete user */
			if($input->user_id && $input->device_token && $input->device_type){
				$arr = $user->update_device_token($input->user_id, $input->device_token, $input->device_type);
				$response = $arr;
			}
			echo json_encode($response);
			exit;
			break;

		case 'logout':
			/* delete user */
			if($input->user_id && $input->device_token && $input->device_type){
				$arr = $user->delete_device_token($input->user_id, $input->device_token, $input->device_type);
				$response = $arr;
			}
			echo json_encode($response);
			exit;
			break;

		case 'send_push':
			/* send push */
		/*	$msg = array
			(
			    'message'   => 'here is a message. message',
			    'title'     => 'Testing here chetan',
			    'subtitle'  => 'This is a subtitle. subtitle',
			    'tickerText'    => 'Ticker text here...Ticker text here...Ticker text here',
			    'vibrate'   => 1,
			    'sound'     => 1,
			    'largeIcon' => 'https://www.google.co.in/logos/doodles/2018/world-cup-2018-day-22-5384495837478912-law.gif',
			    'smallIcon' => 'https://www.google.co.in/logos/doodles/2018/world-cup-2018-day-22-5384495837478912-law.gif'
			); */
			$response = $user->send_push_notifications(array($input->device_token), $input->msg);
			echo json_encode($response);
			exit;
			break;

		default:
			return_json( array('error' => true, 'message' => "Bad Request, not valid parameters.") );
			break;
	}

	// return & exit
	return_json($return);

} catch (Exception $e) {
	return_json( array('error' => true, 'message' => $e->getMessage()) );
}

?>