<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
       <style>
	     * {
         box-sizing: border-box;
         }
		.favorite-progress {
			background: rgba(0,0,0,.5);
			height: 100%;
			left: 0;
			 position: fixed;
			top: 0;
			width: 100%;
			z-index: 9998;
		}
		#img-loader{
			margin-top:15%;
		}
         body {
         background-color: #f1f1f1;
         }
         #regForm {
         background-color: #ffffff;
         margin: 100px auto;
         font-family: Raleway;
         padding: 40px;
         width: 70%;
         min-width: 300px;
         }
         h1 {
         text-align: center;  
         }
         input {
         padding: 10px;
         width: 100%;
         font-size: 17px;
         font-family: Raleway;
         border: 1px solid #aaaaaa;
         }
         /* Mark input boxes that gets an error on validation: */
         input.invalid {
         background-color: #ffdddd;
         }
         /* Hide all steps by default: */
         .tab {
         display: none;
         }
         button {
         background-color: #4CAF50;
         color: #ffffff;
         border: none;
         padding: 10px 21px;
         font-size: 17px;
         font-family: Raleway;
         cursor: pointer;
		 white-space:nowrap
         }
         button:hover {
         opacity: 0.8;
         }
         #prevBtn {
         background-color: #bbbbbb;
         }
         /* Make circles that indicate the steps of the form: */
         .step {
         height: 15px;
         width: 15px;
         margin: 0 2px;
         background-color: #bbbbbb;
         border: none;  
         border-radius: 50%;
         display: inline-block;
         opacity: 0.5;
         }
         .step.active {
         opacity: 1;
         }
         /* Mark the steps that are finished and valid: */
         .step.finish {
         background-color: #4CAF50;
         }	

.contBlock label {
    font-size: 16px;
}
textarea {
    overflow-y: scroll;
    height: 100px;
    resize: none;
}	
.block-container{
display:flex;
flex-wrap:wrap;
    margin: 15px 0;
}
	
@media only screen and (max-device-width: 991px){
.container {
    width: 90%;
}

.block-container div
{
width:100%;
    margin: 15px 0;
 text-align:center;
 
}
.block-container div:first-child{
    order: 2;
}
}
@media only screen and (min-device-width: 468px){
.container {
    width: 90%;
}
}
option {
    font-size: 14px;
}
	   </style>
   </head>
   <body>
      <div class="container">      
		 <div id="container" class="row">
		 <br/><br/>		
         <input type="hidden" id="user_id" name="user_id" value="{$profile['user_id']}">
		 <input type="hidden" id="other_user_id" name="other_user_id" value="{$profile['other_user_id']}">
		 <input type="hidden" id="lang_code" name="lang_code" value="{$profile['lang_code']}">
         <div class="tab col-md-6 col-md-offset-3 contBlock" style="background: #e8e6f3;
    padding: 30px;
    box-shadow: 0px 4px 3px #888888;
    margin-bottom: 20px;  margin-top:10px">
            <p style="font-size:18px; padding:10px 0">{__("Help us understand the problem. What issue with")} <strong>@{$profile['user_name']}</strong> {__("are you reporting?")}</p>
            <div class="form-group">
               <label>{__("Select Reason:")}</label>
				<select id="report_option" name="report_option" class="form-control">
				  {foreach $parent_options as $options}	
				  <option value="{$options['id']}">{__($options['option'])}</option>
				  {/foreach}
				</select>              
            </div>
            <div class="form-group"> <label for="comment">{__("Comment:")}</label> 
			<textarea name="comment" value="" class="form-control" rows="5" id="comment" placeholder="{__('Enter comment here...')}"></textarea> 
			</div>
         </div>
         <div class="tab col-md-6 col-md-offset-3 contBlock" style="background: #e8e6f3;
			padding: 30px;
			box-shadow: 0px 4px 3px #888888;
			margin-bottom: 20px; margin-top:10px">
            <div class="reportFlowHeader-web web-reporting-flow">
               <div class="list-title web-item text-center fntSize16">
                  {__("Thanks for letting us know.")}
               </div>
               <br>				
               <div class="list-explanation success-page">			   
                  <div style="margin-top:20px;"></div>
                  <table class="report-success-table">
                     <colgroup>
                        <col width="100%">
                       
                     </colgroup>
                     <tbody>
                        <tr style="border-bottom:1px solid #908e87">
                           <td  class="description-btn">
						   <div class="col-md-12 block-container">
<div class="col-md-4">
							{if $profile['mute_status'] == true}
							<button type="button" id="unmute-btn" class="mute-btn" onclick="unmute({$profile['user_id']}, {$profile['other_user_id']})">{__("Un-Mute")}</button>
							{else}
						   <button type="button" id="mute-btn" class="mute-btn" onclick="mute({$profile['user_id']}, {$profile['other_user_id']})">{__("Mute")}</button>
							{/if}
							</div>
					<div class="col-md-8">
                              {__("Since you are not following")} <strong>@{$profile['user_name']}</strong>, {__("you will no longer see posts or notifications from")} <strong>@{$profile['user_name']}</strong>.
							  </div>
                         
						   </div>
						     </td>
                        </tr>
                        <tr>
                           <td  class="description-btn">
						   <div class="col-md-12 block-container">
						   <div class="col-md-4">
							{if $profile['block_status'] == true}
							<button type="button" id="unblock-btn" class="block-btn" style="background-color: red" onclick="unblock({$profile['user_id']}, {$profile['other_user_id']})">{__("Un-Block")}</button>
							{else}
						   <button type="button" id="block-btn" class="block-btn" style="background-color: red" onclick="block({$profile['user_id']}, {$profile['other_user_id']})">{__("Block")}</button>
							{/if}
						   </div>
						  <div class="col-md-8">
                          <strong>@{$profile['user_name']}</strong> {__("will not be able to follow you or view your posts, and you will not see posts or notifications from")} <strong>@{$profile['user_name']}</strong>.             
                          </div>
						   </div>
						   </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
        
            <div class="btn-section col-md-6 col-md-offset-3 text-center" style="margin-bottom:20px"> 
			<button type="button" id="prevBtn" onclick="nextPrev(-1)" style="display: none;">{__("Previous")}</button>
			<button type="button" id="nextBtn" onclick="nextPrev(1)">{__("Next")}</button>
	
			</div>
        
         <div style="text-align:center;margin-top:40px;display:none;"> <span class="step active"></span> <span class="step"></span> </div>
      </div> 

	  <div id="loader" class="text-center favorite-progress" style="display: none">
		<img id="img-loader" src="/content/themes/default/images/loader.gif">
	   </div>
      <div id="thanks" class="text-center" style="display:none;
		padding-top: 27%;
		font-size: 22px;">
         <h2 id="message"></h2>
      </div>
	  
	   </div>
	  
      <script>
         var currentTab = 0; // Current tab is set to be the first tab (0)
         showTab(currentTab); // Display the current tab
         
         function showTab(n) {
			var lang_code = $("#lang_code").val();
			 if(lang_code == 'zh_cn'){
				var next = "下一个";
				var submit = "提交";
			 }else{
				var next = "Next";
				var submit = "Submit";
			 }
           // This function will display the specified tab of the form...
           var x = document.getElementsByClassName("tab");
           x[n].style.display = "block";
           //... and fix the Previous/Next buttons:
           if (n == 0) {
             document.getElementById("prevBtn").style.display = "none";
           } else {
             document.getElementById("prevBtn").style.display = "inline";
           }
           if (n == (x.length - 1)) {
             document.getElementById("nextBtn").innerHTML = submit;
           } else {
             document.getElementById("nextBtn").innerHTML = next;
           }
           //... and run a function that will display the correct step indicator:
           fixStepIndicator(n)
         }
         
         function nextPrev(n) {
           // This function will figure out which tab to display
           var x = document.getElementsByClassName("tab");
           // Exit the function if any field in the current tab is invalid:
           if (n == 1 && !validateForm()) return false;
           // Hide the current tab:
           x[currentTab].style.display = "none";
           // Increase or decrease the current tab by 1:
           currentTab = currentTab + n;
           // if you have reached the end of the form...
           if (currentTab >= x.length) {
				currentTab--;
				x[currentTab].style.display = "block";
				$(".btn-section").hide();
         		submitReport();				
             // ... the form gets submitted:
             //document.getElementById("regForm").submit();
             return false;
           }
           // Otherwise, display the correct tab:
           showTab(currentTab);
         }
         
         function validateForm() {
           // This function deals with validation of the form fields
           var x, y, i, valid = true;
           x = document.getElementsByClassName("tab");
           y = x[currentTab].getElementsByTagName("input");
           // A loop that checks every input field in the current tab:
           for (i = 0; i < y.length; i++) {
             // If a field is empty...
             if (y[i].value == "") {
               // add an "invalid" class to the field:
               y[i].className += " invalid";
               // and set the current valid status to false
               valid = false;
             }
           }
           // If the valid status is true, mark the step as finished and valid:
           if (valid) {
             document.getElementsByClassName("step")[currentTab].className += " finish";
           }
           return valid; // return the valid status
         }
         
         function fixStepIndicator(n) {
           // This function removes the "active" class of all steps...
           var i, x = document.getElementsByClassName("step");
           for (i = 0; i < x.length; i++) {
             x[i].className = x[i].className.replace(" active", "");
           }
           //... and adds the "active" class on the current step:
           x[n].className += " active";
         }
         
         function mute(user_id, other_user_id){
			var lang_code = $("#lang_code").val();
			 if(lang_code == 'zh_cn'){
				var unmute = "取消静音";
			 }else{
				var unmute = "Un-Mute";
			 }
         	$.ajax({
             type: "GET",
             url: "https://guo.media/api.php?get=mute_user&query=2y10goLM7YVrc.T2G6AvkMdfS.XUF9Wej5O5XPLwL/H2S59z9yvM6W.AG&user_id="+user_id+"&mute_user_id="+other_user_id,
			 beforeSend: function() {
				$("#loader").show();			 				
			 },
             success: function(result){
         		console.log(result);
         		var result = JSON.parse(result);
				$("#mute-btn").attr("onclick","unmute("+user_id+", "+other_user_id+")").html(unmute);
				$("#unmute-btn").attr("onclick","unmute("+user_id+", "+other_user_id+")").html(unmute);
             },
			 complete: function() {
				$("#loader").hide();
			 }
           });
         }
		 
		 function unmute(user_id, other_user_id){
			var lang_code = $("#lang_code").val();
			 if(lang_code == 'zh_cn'){
				var mute = "静音";
			 }else{
				var mute = "Mute";
			 }
         	$.ajax({
             type: "GET",
             url: "https://guo.media/api.php?get=unmute_user&query=2y10goLM7YVrc.T2G6AvkMdfS.XUF9Wej5O5XPLwL/H2S59z9yvM6W.AG&user_id="+user_id+"&mute_user_id="+other_user_id,
			 beforeSend: function() {
				$("#loader").show();			 				
			 },
             success: function(result){
         		console.log(result);
         		var result = JSON.parse(result);
				$("#mute-btn").attr("onclick","mute("+user_id+", "+other_user_id+")").html(mute);
				$("#unmute-btn").attr("onclick","mute("+user_id+", "+other_user_id+")").html(mute);
             },
			 complete: function() {
				$("#loader").hide();
			 }
           });
         }
         
         function block(user_id, other_user_id){
			var lang_code = $("#lang_code").val();
			 if(lang_code == 'zh_cn'){
				var unblock = "取消阻止";
			 }else{
				var unblock = "Un-Block";
			 }
         	$.ajax({
             type: "GET",
             url: "https://guo.media/api.php?get=block_user&query=2y10goLM7YVrc.T2G6AvkMdfS.XUF9Wej5O5XPLwL/H2S59z9yvM6W.AG&user_id="+user_id+"&block_id="+other_user_id,
			 beforeSend: function() {
				$("#loader").show();			 				
			 },
             success: function(result){
         		console.log(result);
         		var result = JSON.parse(result);
         		$("#block-btn").attr("onclick","unblock("+user_id+", "+other_user_id+")").html(unblock);
				$("#unblock-btn").attr("onclick","unblock("+user_id+", "+other_user_id+")").html(unblock);
             },
			 complete: function() {
				$("#loader").hide();
			 }
           });
         }
		 
		 function unblock(user_id, other_user_id){
			var lang_code = $("#lang_code").val();
			 if(lang_code == 'zh_cn'){
				var block = "块";
			 }else{
				var block = "Block";
			 }
         	$.ajax({
             type: "GET",
             url: "https://guo.media/api.php?get=unblock_user&query=2y10goLM7YVrc.T2G6AvkMdfS.XUF9Wej5O5XPLwL/H2S59z9yvM6W.AG&user_id="+user_id+"&block_id="+other_user_id,
			 beforeSend: function() {
				$("#loader").show();			 				
			 },
             success: function(result){
         		console.log(result);
         		var result = JSON.parse(result);
         		$("#block-btn").attr("onclick","block("+user_id+", "+other_user_id+")").html(block);
				$("#unblock-btn").attr("onclick","block("+user_id+", "+other_user_id+")").html(block);
             },
			 complete: function() {
				$("#loader").hide();
			 }
           });
         }
         
         function submitReport(){
         	var user_id = $("#user_id").val();
         	var other_user_id = $("#other_user_id").val();
         	var report_option = $("#report_option").val();
         	var comment = $("#comment").val();
			
         	$.ajax({
             type: "GET",
             url: "https://guo.media/api.php?get=report_user&query=2y10goLM7YVrc.T2G6AvkMdfS.XUF9Wej5O5XPLwL/H2S59z9yvM6W.AG&user_id="+user_id+"&other_user_id="+other_user_id+"&report_option="+report_option+"&comment="+comment,
			 beforeSend: function() {
				$("#loader").show();			 				
			 },
             success: function(result){
         		console.log(result);
         		var result = JSON.parse(result);
         		$("#container").hide();
				if(result.status == "SUCCESS"){
					$('#thanks').css("color", "green");
				}else{
					$('#thanks').css("color", "red");
				}
         		$("#thanks").show();
         		$("#message").html(result.message);
             },
			 complete: function() {
				$("#loader").hide();
			 }
           });
         }
		 
         history.pushState(null, null, document.URL);
         window.addEventListener('popstate', function () {
             history.pushState(null, null, document.URL);
         });
      </script>  
   </body>
</html>