{if !$standalone}<li>{/if}
    <!-- post -->
    <div class="post {if $boosted}boosted{/if}" data-id="{$post['post_id']}">

        {if $standalone && $pinned}
            <div class="pin-icon" data-toggle="tooltip" title="{__('Pinned Post')}">
                <i class="fa fa-bookmark"></i>
            </div>
        {/if}

        {if $standalone && $boosted}
            <div class="boosted-icon" data-toggle="tooltip" title="{__('Promoted')}">
                <i class="fa fa-bullhorn"></i>
            </div>
        {/if}

       {if !$_shared && $user->_logged_in}
      <div class="pull-right flip dropdown custom-dropdown-profile"> 
        <i class="fa fa-chevron-down dropdown-toggle" data-toggle="dropdown"></i>
        <ul class="dropdown-menu post-dropdown-menu">
         {if $post['user_id']==$user->_data['user_id']}
          <li> <a href="#" class="" data-toggle="modal" data-url="chat/direct_share.php?id={$post['post_id']}">
            <div class="action no-desc"> <i class="fa fa-bookmark fa-fw"></i> <span>{__("Share via Direct Message ")}</span> </div>
            </a> </li>
            <li><span id="p{$post['user_id']}" style="display:none;"></span> <a href="javascript:void(0)"  data-clipboard-text="{$system['system_url']}/posts/{$post['post_id']}" class="copy_link">
            <div class="action no-desc"> <i class="fa fa-bolt fa-fw"></i> <span>{__("Copy Link to Post")}</span> </div>
            </a> </li>
            <li> {if $post['pinned']} <a href="#" class="js_unpin-post">
            <div class="action no-desc"> <i class="fa fa-thumb-tack fa-fw"></i> <span>{__("Unpin Post")}</span> </div>
            </a> {else} <a href="#" class="js_pin-post">
            <div class="action no-desc"> <i class="fa fa-thumb-tack fa-fw"></i> <span>{__("Pin Post")}</span> </div>
            </a> {/if} </li>
            <li> <a href="javascript:void(0)" class="post_editing message js_edit-post" data-handle="post" data-id="{$post['post_id']}">
            <div class="action no-desc"> <i class="fa fa-pencil-square-o"></i><span class="edit-post"> {__("Edit Post")} </span></div>
            </a> </li>
             <li> <a href="#" class="js_delete-post">
            <div class="action no-desc"> <i class="fa fa-trash-o fa-fw"></i> {__("Delete Post")} </div>
            </a> </li>
          {else}
           <li><span id="p{$post['user_id']}" style="display:none;">/includes/ajax/posts/product_editor.php?post_id={$post['post_id']}</span> <a href="javascript:void(0)"  data-clipboard-text="{$system['system_url']}/posts/{$post['post_id']}" class="copy_link">
            <div class="action no-desc"> <i class="fa fa-bolt fa-fw"></i> <span>{__("Copy Link to Post")}</span> </div>
            </a> </li>
               <li> <a href="#" class="js_hide-post">
            <div class="action"> <i class="fa fa-eye-slash fa-fw"></i> {__("Mute Post")} </div>
            <div class="action-desc">{__("See fewer posts like this")}</div>
            </a> </li>
          <li> <a href="#" class="js_block-comment" data-id="{$post['user_id']}">
            <div class="action"> <i class="fa fa-ban fa-fw"></i> {__("Block")} {$post['post_author_name']}</div>
            <div class="action-desc">{__("Block this user")}</div>
            </a> </li>
          <li> <a href="#" class="js_report" data-handle="post" data-id="{$post['post_id']}">
            <div class="action no-desc"> <i class="fa fa-flag fa-fw"></i> {__("Report post")} </div>
            </a> </li>
         {/if}
       
        </ul>
      </div>
      {/if}

        <!-- post body -->
         {if $post['post_id']!='a'}
        <div class="post-body" data-id="{$post['post_id']}" style="padding-bottom: 5px;">
            
            {include file='__feeds_post.body.tpl' _post=$post _shared=false}

            <!-- post stats -->
            
            <!-- post stats -->

            <!-- post actions -->
            
            <!-- post actions -->

        </div>
        {/if}
         <div class="custom-post-actions bottom-icons" style="float:none;">
            {if $post['post_type'] == "video" && $post['video'] && $post['is_broadcast']}
                <div>
                {$user->video_view_manager({$post['post_id']})}
                       <!-- views -->
                           <span class="js_post-views-num" style="font-size: 11px;color: #999;">{$user->views_format({$post['views']})}</span>
                       <!-- views -->
           </div>
            {/if}
        <div class="post-actions">
                {if $user->_logged_in}
                    <!-- comment -->
                    <span class="text-clickable mr20" data-id="{$post['post_id']}" data-handle="post">
                        <i class="Icon Icon--medium Icon--reply post_comment_evnt" data-id="{$post['post_id']}" data-tooltip="tooltip" data-placement="top" title="" data-original-title="{__("Reply")}"></i> <span id="span-comments-counter_{$post['post_id']}"><!--{__("Comment")}&nbsp;-->{$post['comments']}</span>
                    </span>
                    <!-- comment -->

                    <!-- share -->
                    {if $post['privacy'] == "public"}
                        <span class="text-clickable mr20 share-tweet-post" data-id="{$post['post_id']}">
                            <i class="Icon Icon--medium Icon--retweet" data-tooltip="tooltip" data-placement="top" title="" data-original-title="{__("Repost")}"></i> <span id="span-share-counter_{$post['post_id']}"><!--{__("Share")}&nbsp;-->{$post['shares']}</span>
                        </span>
                    {/if}
                    <!-- share -->
                    <!-- like -->
                    <span class="text-clickable {if $post['i_like']}text-active js_unlike-post{else}js_like-post{/if} mr20">
                        <i class="Icon Icon--heart Icon--medium" data-tooltip="tooltip" data-placement="top" title="" data-original-title="{__("Like")}"></i>
                        <i class="Icon Icon--heartBadge Icon--medium"></i>
                        <span class="span-counter_{$post['post_id']}"><!--{__("Like")}&nbsp;-->{$post['likes']}</span>
                    </span>
                    <!-- like -->
                    <!-- Direct Message -->

                    {if $user->_data['user_id'] !=$post['author_id']}
                    <span class="text-clickable direct_message_frm_post" data-id="{$post['post_id']}">
                        <i class="Icon Icon--envelope Icon--medium" data-tooltip="tooltip" data-placement="top" title="" data-original-title="{__("Direct message")}"></i>

                    </span>
                    {/if}
                    <!-- Direct Message -->
                    <!-- Download -->
                    {if $post['post_type'] == "video" && $post['video'] && $post['is_broadcast']}
        
                   <span class="download-video-link">
                       <a href="/download_broadcast.php?video_url={BROADCAST_URL}/{$post['video']['source']}" class="download-broadcast"><i class="fa fa-download Icon--medium"></i></a>
                   </span>
                    {/if}
                   <!-- Download --> 

                {else}
                    <a href="/signin">{__("Please log in to like, share and comment!")}</a>
                {/if}
				<div class="sharethis-inline-share-buttons" data-url="{$system['system_url']}/posts/{$post['post_id']}"></div>
        </div>
        </div>
        <!-- post body -->

        <!-- post footer -->
        <div class="post-footer {if !$standalone}x-hidden{/if}">
            <!-- social sharing -->
            {include file='__feeds_post.social.tpl'}
            <!-- social sharing -->

            <!-- comments -->
            
            <!-- comments -->
        </div>
        <!-- post footer -->

    </div>
    <!-- post -->
<div id="openGallery"></div>
<script id="lightbox" type="text/template">

    <div class="lightbox custom-lightbox-container">
      
        <div class="container lightbox-container">
            
            <button data-toggle="tooltip" data-placement="bottom" title='{__("Press Esc to close")}' type="button" class="close lightbox-close js_lightbox-close">
                <span class="Icon Icon--close Icon--large"></span>
            </button>
                
            <div class="lightbox-preview">
                 
                <div id="divLoading" style="display : none;position : fixed;z-index: 100;background-image : url('/content/themes/default/images/loading.gif');background-color:#666;opacity : 0.4;background-repeat : no-repeat;background-position : center;left : 0;bottom : 0;right : 0;top : 0;"></div> 
                

                <div class="post-image"><div style="display: table-cell;vertical-align: middle;" id="test-swap"><img alt="" class="img-responsive" src="{literal}{{image}}{/literal}"></div></div>
                <div class="hover-text">
                </div>
                <div class="test-post-actions"></div>


            </div>

        </div>

    </div>

</script>
    
{if !$standalone}</li>{/if}