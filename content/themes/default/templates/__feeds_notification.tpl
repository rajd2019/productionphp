<li class="feeds-item {if !$notification['seen']}unread{/if}" data-id="{$notification['notification_id']}">

    <a class="data-container" href="{$notification['url']}" target="{if $notification['action'] == 'invite'}_new{/if}">

        <img class="data-avatar" src="{$notification['user_picture']}" alt="">

        <div class="data-content">

            <div><span class="name">{$notification['user_firstname']} {$notification['user_lastname']}</span><div class="time js_moment" data-time="{$notification['time']}">{$notification['time']}</div></div>

            <div><i class="Icon Icon--Icon Icon--medium {$notification['icon']} pr5"></i> {__("{$notification['message']}")}</div>

        </div>

    </a>

</li>