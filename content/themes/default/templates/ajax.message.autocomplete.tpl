<li class="DMTypeaheadHeader">
    <span>People</span>
</li>
{foreach $users as $_user}
{if $_user['user_firstname'] == '' || $_user['user_lastname']==''}

<li class="DMTokenizedMultiselectSuggestion DMTypeaheadSuggestions-item" data-uuid="{$_user['uuid']}" role="option" tabindex="-1" onclick="addNewMemberOnNewDialog('{$_user['user_name']}', '{$_user['uuid']}')">

{else}

<li class="DMTokenizedMultiselectSuggestion DMTypeaheadSuggestions-item" data-uuid="{$_user['uuid']}" role="option" tabindex="-1" onclick="addNewMemberOnNewDialog('{$_user['user_firstname']} {$_user['user_lastname']}', '{$_user['uuid']}')">

{/if}

        <div class="DMTokenizedMultiselectSuggestion-body">
            <div class="DMTypeaheadItem">
                <div class="DMTypeaheadItem-avatar" aria-hidden="true">
                    <div class="DMAvatar DMAvatar--1 u-chromeOverflowFix">
                        <span class="DMAvatar-container">
                            <img class="DMAvatar-image" src="{$_user['user_picture']}" alt="{if $_user['user_firstname'] == '' || $_user['user_lastname']==''}
                            {$_user['user_name']}
                            {else}
                            {$_user['user_firstname']} {$_user['user_lastname']}
                            {/if}">
                        </span>
                    </div>
                </div>
                <div class="DMTypeaheadItem-body">
                    <div class="DMTypeaheadItem-title account-group">
                        <b class="fullname">
                          {if $_user['user_firstname'] == '' || $_user['user_lastname']==''}
                          {$_user['user_name']}
                          {else}
                          {$_user['user_firstname']} {$_user['user_lastname']}
                          {/if}
                        </b>
                        <span class="UserBadges">
                            <span class="Icon Icon--verified">
                                <span class="u-hiddenVisually">Verified account</span>
                            </span>
                        </span>
                        <span class="UserNameBreak">&nbsp;</span>
                        <span class="username u-dir u-textTruncate" dir="ltr">
                            @{$_user['user_name']}
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="DMTokenizedMultiselectSuggestion-state">
            <span class="DMTokenizedMultiselectSuggestion-selectedIndicator Icon Icon--check"></span>
            <span class="DMTokenizedMultiselectSuggestion-preselectedIndicator">In Group</span>
        </div>
    </li>
{/foreach}