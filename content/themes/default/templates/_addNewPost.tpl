
<!--Model-->
                                      <!-- post message -->
                                       
<div class="custom_post_social modal fade" role="dialog" id="postsocial">
    <div class="modal-dialog">
        <div class="modal-content-modal2">
            <div class="modal-header">
                <h3 class="modal-title">Compose new Post</h3>
                <button type="button" id="close-modal-popup" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body post_custom post_custom_mobile">
                <div class="max-600">
                    <div class="post-avatar">
                        <a class="post-avatar-picture" href="{$_post['post_author_url']}" style="background-image:url({$user->_data.user_picture});"></a>
                    </div>
                    <div class="x-form publisher" data-handle="me_ajax" {if $_id}data-id="{$_id}" {/if}>
                            <!-- publisher loader -->
                            <div class="publisher-loader">
                                <div class="loader loader_small"></div>
                            </div>
                            <!-- publisher loader -->
                            <!-- publisher tabs -->
                            <!-- publisher tabs -->
                            <!-- post product -->
                            <div class="publisher-meta top" data-meta="product">
                                <i class="fa fa-tag fa-fw"></i>
                                <input name="name" type="text" placeholder='{__("What are you selling?")}'>
                            </div>
                            <div class="publisher-meta top" data-meta="product">
                                <i class="fa fa-money fa-fw"></i>
                                <input name="price" type="text" placeholder='{__("Add price")} ({$system["system_currency"]})'>
                            </div>
                            <div class="publisher-meta top" data-meta="product">
                                <i class="fa fa-shopping-basket fa-fw"></i>
                                <select name="category_id">
                                    {foreach $market_categories as $category}
                                    <option value="{$category['category_id']}">{__($category['category_name'])}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="publisher-meta top" data-meta="product">
                                <i class="fa fa-map-marker fa-fw"></i>
                                <input name="location" class="js_geocomplete" type="text" placeholder='{__("Add Location (optional)")}'>
                            </div>
                            <!-- post product -->
                            <!-- post message -->
                            <div class="relative focused" id="postBoxDiv">
                                <div class="post_area_custom">
                                    <div class="post-avatar" style="float:left;">
                                        <a class="post-avatar-picture" href="{$_post['post_author_url']}" style="background-image:url({$user->_data.user_picture});"></a>
                                    </div>
                                    <textarea dir="auto" style="height:30px;overflow: hidden;" id="postBox" class="js_mention js_publisher-scraper" ></textarea>

                                    <span class="js_emoji-menu-toggle" style="float:right;width: 7%; display:none;" data-toggle="tooltip" data-placement="top" title='{__("Insert an emoji")}'><!--i class="fa fa-smile-o fa-fw"--></i></span>{include file='_emoji-menu.tpl'}

                                   

                                </div>
                                <!-- publisher scraper -->
                                <div class="publisher-scraper"></div>
                                <!-- publisher scraper -->
                                <!-- post attachments -->
                                <div class="publisher-attachments attachments clearfix x-hidden">
                                    <ul id="sortable"></ul>
                                </div>
                                <!-- post attachments -->
                                <!-- post album -->
                                <div class="publisher-meta" data-meta="album">
                                    <i class="fa fa-picture-o fa-fw"></i>
                                    <input type="text" placeholder='{__("Album title")}'>
                                </div>
                                <!-- post album -->
                                <!-- post poll -->
                                <div class="publisher-meta remove_all_poll" data-meta="poll">
                                    <button type="button" class="btn btn-primary remove_poll">{__("Remove Poll")}</button>
                                </div>
                                <div class="publisher-meta custom_poll" data-meta="poll">
                                    <i class="fa fa-plus fa-fw"></i>
                                    <input type="text" placeholder='{__("Add an option")}...'>
                                </div>
                                <div class="publisher-meta custom_poll" data-meta="poll">
                                    <i class="fa fa-plus fa-fw"></i>
                                    <input type="text" placeholder='{__("Add an option")}...'>
                                </div>
                                <!-- post poll -->
                                <!-- post video -->
                                <div class="publisher-meta" data-meta="video">
                                    <i class="fa fa-video-camera fa-fw"></i> {__("Video uploaded successfully")}
                                </div>
                                <!-- post video -->
                                <!-- post audio -->
                                <div class="publisher-meta" data-meta="audio">
                                    <i class="fa fa-music fa-fw"></i> {__("Audio uploaded successfully")}
                                </div>
                                <!-- post audio -->
                                <!-- post file -->
                                <div class="publisher-meta" data-meta="file">
                                    <i class="fa fa-file-text-o fa-fw"></i> {__("File uploaded successfully")}
                                </div>
                                <!-- post file -->
                                <!-- post location -->
                                <div class="publisher-meta" data-meta="location">
                                    <i class="fa fa-map-marker fa-fw"></i>
                                    <input class="js_geocomplete" type="text" placeholder='{__("Where are you?")}'>
                                </div>
                                <!-- post location -->
                                <!-- post feelings -->
                                <div class="publisher-meta feelings" data-meta="feelings">
                                    <div id="feelings-menu-toggle" data-init-text='{__("What are you doing?")}'>{__("What are you doing?")}</div>
                                    <div id="feelings-data" style="display: none">
                                        <input type="text" placeholder='{__("What are you doing?")}'>
                                        <span></span>
                                    </div>
                                    <div id="feelings-menu" class="dropdown-menu dropdown-widget">
                                        <div class="dropdown-widget-body ptb5">
                                            <div class="js_scroller">
                                                <ul class="feelings-list">
                                                    {foreach $feelings as $feeling}
                                                    <li class="feeling-item js_feelings-add" data-action="{$feeling['action']}" data-placeholder="{__($feeling['placeholder'])}">
                                                        <div class="icon">
                                                            <i class="twa twa-3x twa-{$feeling['icon']}"></i>
                                                        </div>
                                                        <div class="data">
                                                            {__($feeling['text'])}
                                                        </div>
                                                    </li>
                                                    {/foreach}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="feelings-types" class="dropdown-menu dropdown-widget">
                                        <div class="dropdown-widget-body ptb5">
                                            <div class="js_scroller">
                                                <ul class="feelings-list">
                                                    {foreach $feelings_types as $type}
                                                    <li class="feeling-item js_feelings-type" data-type="{$type['action']}" data-icon="{$type['icon']}">
                                                        <div class="icon">
                                                            <i class="twa twa-3x twa-{$type['icon']}"></i>
                                                        </div>
                                                        <div class="data">
                                                            {__($type['text'])}
                                                        </div>
                                                    </li>
                                                    {/foreach}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- post feelings -->
                            </div>
                            <!-- post message -->
                            <!-- publisher-footer -->
            <div class="publisher-footer clearfix" style="display:block;">
                <!-- publisher-tools -->
                <ul class="publisher-tools">
                    <li data-toggle="tooltip" data-placement="top" title='{__("Create Post")}'>
                        <span class="js_publisher-tab" data-tab="post">

                        <i class="fa fa-pencil fa-fw"></i> <span class="hidden-xs"></span>
                        </span>
                    </li>
                    {if $system['photos_enabled']}
                    <li >
                        <span class="publisher-tools-attach js_publisher-photos newalbumClass" style="float:right;width: 7%; height:30px; padding-top:0px;">

                                <i class="fa fa-camera fa-fw js_x-uploader" data-handle="publisher" data-multiple="true"></i>

                            </span>
                    </li>
                    {/if} {if $system['geolocation_enabled']}
                    
                    {/if}
                    
                    {if $system['market_enabled'] && $_handle != "group" && $_handle != "event"}
                    <li class="relative" data-toggle="tooltip" data-placement="top" title='{__("Sell Something")}'>
                        <span class="js_publisher-tab" data-tab="product">

                        <i class="fa fa-tag fa-fw"></i>

                    </span>
                    </li>
                    {/if} {if $system['blogs_enabled'] && $_handle != "group" && $_handle != "event"}
                    
                    {/if} {if $system['videos_enabled']}
                    <li id="video-select" class="relative" data-toggle="tooltip" data-placement="top" title='{__("Add Video")}'>
                        
                    <span class="publisher-tools-attach js_publisher-tab" data-tab="video">

                        <i class="fa fa-video-camera fa-fw js_x-uploader" data-handle="publisher" data-type="video"></i>

                    </span>
                    </li>
                    {/if} 
                    
                    {if $system['file_enabled']}
                        {if ($user->_data['user_name'] == "milesguo" || $user->_data['user_name'] == "Milesguo") }
                        <li>
                            <span class="publisher-tools-attach js_publisher-tab" data-tab="file">
                                <i class="fa fa-paperclip fa-fw js_x-uploader" data-handle="publisher" data-type="file"></i> 
                            </span>
                        </li>
                        {/if}
                    {/if}
                    
                    {if $system['audio_enabled']}
                     <li id="audio-select" class="relative" >

                        <span class="publisher-tools-attach js_publisher-tab" data-tab="audio">

                            <i class="fa fa-music fa-fw js_x-uploader" data-handle="publisher" data-type="audio"></i>

                        </span>

                    </li> 
                    {/if}
                    <li data-toggle="tooltip" class="" data-placement="top">
                        <span class="publisher-tools-attach js_publisher-photos newalbumClass picker_profile" style="overflow:visible">
                        <i class="fa fa-smile-o fa-fw"></i>
                    </span>
                    </li>
            
                    <script>
                    
                        $(function() {
                            
                            $('.picker_profile').lsxEmojiPicker({
                                closeOnSelect: true,
                                twemoji: true,
                                onSelect: function(emoji){
                                    
                                    tinymce.activeEditor.execCommand('mceInsertContent', false, emoji.value)
                                }
                            });
                            
                        });
                   
                    </script>
                </ul>
                <!-- publisher-tools -->
                <!-- publisher-buttons -->
                <div class="pull-right flip mt5">
                    <div class="max-600">
                        <a href="javascript:void(0)" id="close-update-popup"><i class="fa fa-times"></i></a>
                        {if $_privacy}
                        <!-- privacy -->
                        <div class="btn-group js_publisher-privacy" data-toggle="tooltip" data-placement="top" data-value="public" title='{__("Shared with: Public")}'>
                        </div>
                        <!-- privacy -->
                        {/if}
                        <span class="text-counter" id="count_char" style="display:inline-block;"></span>
                        
                        <button type="button" class="btn btn-primary js_publisher">{__("Post")}</button>
                    </div>
                </div>
                <!-- publisher-buttons -->
            </div>
                            <!-- publisher-footer -->
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div> 