{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20 offcanvas">
    <div class="row">

        <!-- side panel -->
        <div class="col-sm-4 col-md-3 offcanvas-sidebar">
            {include file='_notifications-sidebar.tpl'}
        </div>
        <!-- side panel -->

        <div class="col-sm-8 col-md-9 offcanvas-mainbar">
            <div class="row">
                <!-- left panel -->
                <div class="col-sm-12 col-md-8">
                    <!-- notifications -->
                    <ul class="ProfileHeading-toggle"> 
                            <li class="ProfileHeading-toggleItem  u-textUserColor" data-element-term="photos_and_videos_toggle"> <a class="ProfileHeading-toggleLink js-nav" href="/notifications" data-nav="photos_and_videos_toggle">{__("All")}</a></li>
                            <li class="ProfileHeading-toggleItem  is-active" data-element-term="tweets_toggle"> 
                                <span aria-hidden="true">{__("Mentions")}</span> 
                            </li> 
                    </ul>
                    <div class="panel panel-default custom_notification">
                        <div class="">
                            <ul>
                                {foreach $user->_data['mentions'] as $notification}
                                {include file='__feeds_notification.tpl'}
                                {/foreach}
                            </ul>

                            {if count($user->_data['mentions']) >= $system['max_results']}
                            <!-- see-more -->
                            <div class="alert alert-info see-more js_see-more" data-get="notifications">
                                <span>{__("See More")}</span>
                                <div class="loader loader_small x-hidden"></div>
                            </div>
                            <!-- see-more -->
                            {/if}

                        </div>
                    </div>
                    <!-- notifications -->
                </div>
                <!-- left panel -->

                <!-- right panel -->
                    <div class="col-sm-12 col-md-4">
                        <!-- pro members -->
                        {if count($pro_members) > 0}
                            <div class="panel panel-default panel-friends">
                                <div class="panel-heading">
                                    {if $system['packages_enabled'] && !$user->_data['user_subscribed']}
                                        <div class="pull-right flip">
                                            <small><a href="/packages">{__("Upgrade")}</a></small>
                                        </div>
                                    {/if}
                                    <strong class="text-primary"><i class="fa fa-bolt"></i> {__("Pro Users")}</strong>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        {foreach $pro_members as $_member}
                                            <div class="col-xs-4">
                                                <a class="friend-picture" href="/{$_member['user_name']}" style="background-image:url({$_member['user_picture']});" >
                                                    <span class="friend-name">{$_member['user_firstname']} {$_member['user_lastname']}@{$_member['user_name']}</span>
                                                </a>
                                            </div>
                                        {/foreach}
                                    </div>
                                </div>
                            </div>
                        {/if}
                        <!-- pro members -->

                        <!-- boosted pages -->
                        {if count($promoted_pages) > 0}
                            <div class="panel panel-default panel-friends">
                                <div class="panel-heading">
                                    {if $system['packages_enabled'] && !$user->_data['user_subscribed']}
                                        <div class="pull-right flip">
                                            <small><a href="/packages">{__("Upgrade")}</a></small>
                                        </div>
                                    {/if}
                                    <strong class="text-primary"><i class="fa fa-bullhorn fa-fw"></i> {__("Pro Pages")}</strong>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        {foreach $promoted_pages as $_page}
                                            <div class="col-xs-4">
                                                <a class="friend-picture" href="/pages/{$_page['page_name']}" style="background-image:url({$_page['page_picture']});" >
                                                    <span class="friend-name">{$_page['page_title']}</span>
                                                </a>
                                            </div>
                                        {/foreach}
                                    </div>
                                </div>
                            </div>
                        {/if}
                         <!-- boosted pages -->

                        {include file='_ads.tpl'}
                        {include file='_widget.tpl'}

                        <!-- people you may know -->

                            <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <div class="pull-right flip">
                                        <small><a href="/people">{__("View All")}</a></small>
                                    </div>
                                    <strong class="wtf-module">{__("Who to follow")}</strong>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                    {if count($user->_data['new_people']) > 0}
                                        {foreach $user->_data['new_people'] as $_user}
                                        {include file='__feeds_user.tpl' _connection="add" _small=true}
                                        {/foreach}
                                     {/if}
                                     {if count($user->_data['new_people']) <= 0}
                                     <p class="text-center text-muted">
                                        {__("No people available")}
                                     </p>
                                     {/if}
                                    </ul>
                                </div>
                            </div>

                         <!-- people you may know -->

                        <!-- suggested pages
                        {if count($new_pages) > 0}
                            <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <div class="pull-right flip">
                                        <small><a href="/pages">{__("See All")}</a></small>
                                    </div>
                                    <strong>{__("Suggested Pages")}</strong>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                        {foreach $new_pages as $_page}
                                        {include file='__feeds_page.tpl' _tpl="list"}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                        {/if} -->
                        <!-- suggested pages -->

                        <!-- suggested groups
                        {if count($new_groups) > 0}
                            <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <div class="pull-right flip">
                                        <small><a href="/groups">{__("See All")}</a></small>
                                    </div>
                                    <strong>{__("Suggested Groups")}</strong>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                        {foreach $new_groups as $_group}
                                        {include file='__feeds_group.tpl' _tpl="list"}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                        {/if}-->
                        <!-- suggested groups -->

                        <!-- suggested events
                        {if count($new_events) > 0}
                            <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <div class="pull-right flip">
                                        <small><a href="/events">{__("See All")}</a></small>
                                    </div>
                                    <strong>{__("Suggested Events")}</strong>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                        {foreach $new_events as $_event}
                                        {include file='__feeds_event.tpl' _tpl="list" _small=true}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                        {/if}-->
                        <!-- suggested events -->

                        <!-- mini footer -->
                        {if count($user->_data['new_people']) > 0 || count($new_pages) > 0 || count($new_groups) > 0 || count($new_events) > 0}
                            <div class="row plr10 hidden-xs">
                                <div class="col-xs-12 mb5">
                                    {if count($static_pages) > 0}
                                        {foreach $static_pages as $static_page}
                                            <a href="/static/{$static_page['page_url']}">
                                                {$static_page['page_title']}
                                            </a>{if !$static_page@last} · {/if}
                                        {/foreach}
                                    {/if}
                                    {if $system['contact_enabled']}
                                         · 
                                        <a href="/contacts">
                                            {__("Contacts")}
                                        </a>
                                    {/if}
                                    {if $system['directory_enabled']}
                                         · 
                                        <a href="/directory">
                                            {__("Directory")}
                                        </a>
                                    {/if}
                                    {if $system['market_enabled']}
                                         · 
                                        <a href="/market">
                                            {__("Market")}
                                        </a>
                                    {/if}
                                </div>
                                <div class="col-xs-12">
                                    &copy; {'Y'|date} {$system['system_title']} · <span class="text-link dev" data-toggle="modal" data-url="#translator">{$system['language']['title']}</span>
                                </div>
                            </div>
                        {/if}
                        <!-- mini footer -->
                    </div>
                    <!-- right panel -->
            </div>
        </div>

    </div>
</div>
<!-- page content -->

{include file='_footer.tpl'}