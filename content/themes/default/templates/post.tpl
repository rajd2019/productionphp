{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20 {if $user->_logged_in}offcanvas{/if}">
	<div class="row">

        <!-- side panel -->
        {if $user->_logged_in}
            <div class="col-xs-12 visible-xs-block offcanvas-sidebar mt20">
                {include file='_sidebar.tpl'}
            </div>
        {/if}
        <!-- side panel -->
		
        <div class="col-xs-12 {if $user->_logged_in}offcanvas-mainbar{/if}">
        	
        	<div class="row ">
        		<div class="col-sm-4 col-md-3 offcanvas-sidebar custom-mobile-sidebar"> 
                {include file='_sidebar.tpl'}
            	</div>
        		<!-- left panel -->
        		<div class="col-sm-8 col-md-9">
				<div class="col-sm-12 col-md-8">
				{include file='__feeds_post.tpl' standalone=true}
				</div>
				<!-- left panel -->

				<!-- right panel -->
				
				<div class="col-sm-12 col-md-4">
                       

                        {include file='_ads.tpl'}
                        {include file='_widget.tpl'}

                        <!-- people you may know -->
                        
                            <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <div class="pull-right flip">
                                        <small><a href="/people">{__("View All")}</a></small>
                                    </div>
                                    <strong class="wtf-module">{__("Who to follow")}</strong>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                    {if count($user->_data['new_people']) > 0}
                                        {foreach $user->_data['new_people'] as $_user}
                                        {include file='__feeds_user.tpl' _connection="add" _small=true}
                                        {/foreach}
                                     {/if}
                                     {if count($user->_data['new_people']) <= 0}
                                     <p class="text-center text-muted">
                                        {__("No people available")}
                                     </p>
                                     {/if}
                                    </ul>
                                </div>
                            </div>
                        
                         

                        
                    </div>
                   </div>
				<!-- right panel -->
        	</div>
        </div>

	</div>
</div>
<!-- page content -->

{include file='_footer.tpl'}