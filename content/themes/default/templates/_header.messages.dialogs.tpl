<div id="dm_dialog" class="DMDialog modal-container" style="z-index: 4002;">
    <div class="modal is-autoPosition" id="dm_dialog-dialog" role="dialog" aria-labelledby="dm_dialog-header">
        <div class="DMDock">
            <div class="DMDock-conversations">
                <div class="DMActivity DMConversationSettings js-ariaDocument u-chromeOverflowFix DMActivity--open" role="document">
                    <div class="DMActivity-header">
                        <div class="DMActivity-navigation"></div>
                        <h2 class="DMActivity-title js-ariaTitle" id="dm_dialog-header">Group Info</h2>
                        <div class="DMActivity-toolbar">
                            <div class="DMConversationSettings-dropdown u-posRelative u-textLeft u-textUserColorHover">
                                <div class="DMPopover DMPopover--left">
                                    <button class="DMPopover-button" aria-haspopup="true">
                                        <span class="u-hiddenVisually">More</span>
                                        <span class="Icon Icon--dots Icon--medium"></span>
                                    </button>
                                    <div class="DMPopover-content Caret Caret--top Caret--stroked"></div>
                                </div>
                            </div>
                            <button type="button" class="DMActivity-close js-close u-textUserColorHover">
                                <span class="Icon Icon--close Icon--medium"></span>
                                <span class="u-hiddenVisually"></span>
                            </button>
                        </div>
                    </div>
                    <div class="DMActivity-container"></div>
                </div>
            </div>
        </div>
    </div>
</div>