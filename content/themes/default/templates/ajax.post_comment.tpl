<div class="modal-body">

    <div class="x-form publisher mini">



         <!-- publisher close -->

        <button type="button" class="close mr10 pull-right flip" data-dismiss="modal" aria-hidden="true">×</button>

        <!-- publisher close -->



        <!-- publisher tabs -->



        <div class="mt5 custom_title">{__("Add Comment")}</div>

        <!-- publisher -->

        

        <!-- publisher -->

    </div>

</div>



<div class="comment custom_comment_popup" data-handle="post" data-id="{$_id}">

    <div class="modal-body">

    <div class="comment-avatar">

        <a class="comment-avatar-picture" href="/{$user->_data['user_name']}" style="background-image:url({$user->_data['user_picture']});">

            </a>

    </div>

    <div class="comment-data">

        <div class="x-form comment-form">

            <textarea dir="auto" id="commentBox" class="js_autosize js_mention js_post-comment" rows="1" placeholder='{__("Write a comment")}'></textarea>

            <div class="x-form-tools">

                <div class="x-form-tools-post js_post-comment">

                    <i class="fa fa-paper-plane-o"></i>

                </div>

                <div class="x-form-tools-attach">

                    <i class="fa fa-camera js_x-uploader" data-handle="comment"></i>

                </div>

                <div class="x-form-tools-emoji js_emoji-menu-toggle">

                    <i class="fa fa-smile-o fa-lg"></i>

                </div>

                {include file='_emoji-menu.tpl'}

            </div>

        </div>

        <div class="comment-attachments attachments clearfix x-hidden">

            <ul>

                <li class="loading">

                    <div class="loader loader_small"></div>

                </li>

            </ul>

        </div>

    </div>

    <div class="pull-right flip mt5 mr10">

            <button type="button" id="commentButton"  class="btn btn-primary">{__("Post")}</button>

        </div>

        </div>

</div>

