{if !$standalone}<li>{/if}


    <!-- post -->
    <div class="post {if $boosted}boosted{/if}" data-id="{$post['post_id']}">

        {if $standalone && $pinned}
            <div class="pin-icon" data-toggle="tooltip" title="{__('Pinned Post')}">
                <i class="fa fa-bookmark"></i>
            </div>
        {/if}

        {if $standalone && $boosted}
            <div class="boosted-icon" data-toggle="tooltip" title="{__('Promoted')}">
                <i class="fa fa-bullhorn"></i>
            </div>
        {/if}

		    {if !$_shared && $user->_logged_in}
			  <div class="pull-right flip dropdown custom-dropdown-profile" data-id="{$post['post_id']}"> 
				<i class="fa fa-chevron-down dropdown-toggle" data-toggle="dropdown"></i>
				<ul class="dropdown-menu post-dropdown-menu">
				 {if $post['user_id']==$user->_data['user_id']}
				  <li> <a href="#" class="" data-toggle="modal" data-url="chat/direct_share.php?id=={$post['post_id']}">
					<div class="action no-desc"> <i class="fa fa-bookmark fa-fw"></i> <span>{__("Share via Direct Message ")}</span> </div>
					</a> </li>
					<li><span id="p{$post['user_id']}" style="display:none;"></span> <a href="javascript:void(0)"  data-clipboard-text="{$system['system_url']}/posts/{$post['post_id']}" class="copy_link">
					<div class="action no-desc"> <i class="fa fa-bolt fa-fw"></i> <span>{__("Copy Link to Post")}</span> </div>
					</a> </li>
					<li> {if $post['pinned']} <a href="#" class="js_unpin-post">
					<div class="action no-desc"> <i class="fa fa-thumb-tack fa-fw"></i> <span>{__("Unpin Post")}</span> </div>
					</a> {else} <a href="#" class="js_pin-post">
					<div class="action no-desc"> <i class="fa fa-thumb-tack fa-fw"></i> <span>{__("Pin Post")}</span> </div>
					</a> {/if} </li>
					 <li> <a href="#" class="js_delete-post">
					<div class="action no-desc"> <i class="fa fa-trash-o fa-fw"></i> {__("Delete Post")} </div>
					</a> </li>
				  {else}
				   <li><span id="p{$post['user_id']}" style="display:none;">/includes/ajax/posts/product_editor.php?post_id={$post['post_id']}</span> <a href="javascript:void(0)"  data-clipboard-text="{$system['system_url']}/posts/{$post['post_id']}" class="copy_link">
					<div class="action no-desc"> <i class="fa fa-bolt fa-fw"></i> <span>{__("Copy Link to Post")}</span> </div>
					</a> </li>
					   <li> <a href="#" class="js_hide-post">
					<div class="action"> <i class="fa fa-eye-slash fa-fw"></i> {__("Mute Post")} </div>
					<div class="action-desc">{__("See fewer posts like this")}</div>
					</a> </li>
				  <li> <a href="#" class="js_block-comment" data-id="{$post['user_id']}">
					<div class="action"> <i class="fa fa-ban fa-fw"></i> {__("Block")} {$post['post_author_name']}</div>
					<div class="action-desc">{__("Block this user")}</div>
					</a> </li>
				  <li> <a href="#" class="js_report" data-handle="post" data-id="{$post['post_id']}">
					<div class="action no-desc"> <i class="fa fa-flag fa-fw"></i> {__("Report post")} </div>
					</a> </li>
				 {/if}
			   
				</ul>
			   </div>
			{/if}

        <!-- post body -->
        <div class="post-body post-listing" data-id="{$post['post_id']}">            
            {include file='__trend_feeds_post.body.tpl' _post=$post _shared=false}
        </div>
        <!-- post body -->
        <!-- post actions -->

            <!-- post actions -->

        <!-- post footer -->
        <div class="post-footer {if !$standalone}x-hidden{/if}">
            <!-- social sharing -->
            {include file='__feeds_post.social.tpl'}
            <!-- social sharing -->

            <!-- comments -->
            {include file='__feeds_post.comments.tpl'}
            <!-- comments -->
        </div>
        <!-- post footer -->



 
    <!-- post -->
{if !$standalone}
<div class="custom-post-actions" style="float: none !important;display: block !important;">
    <div class="post-actions">
        {if $user->_logged_in}
            <!-- comment -->
            <span class="text-clickable mr20 js-custom-share-comment" data-id="{$post['post_id']}" data-handle="post">
            <i class="Icon Icon--medium Icon--reply"></i> <span id="span-comments-counter_{$post['post_id']}"><!--{__("Comment")}&nbsp;-->{$post['comments']}</span>
            </span>
            <!-- comment -->

            <!-- share -->
            {if $post['privacy'] == "public"}
                <span class="text-clickable mr20 {if $system['social_share_enabled']}share-tweet-post{else}share-tweet-post{/if}" data-id="{$post['post_id']}">
                    <i class="Icon Icon--medium Icon--retweet"></i> <span id="span-share-counter_{$post['post_id']}"><!--{__("Share")}&nbsp;-->{$post['shares']}</span>
                </span>
            {/if}
            <!-- share -->
            <!-- like -->
            <span class="text-clickable {if $post['i_like']}text-active js_unlike-post{else}js_like-post{/if}">
                <i class="Icon Icon--heart Icon--medium"></i><i class="Icon Icon--heartBadge Icon--medium"></i> <span class="span-counter_{$post['post_id']}"><!--{__("Like")}&nbsp;-->{$post['likes']}</span>
            </span>
            <!-- like -->

        {else}
            <a href="/signin">{__("Please log in to like, share and comment!")}</a>
        {/if}
		<div class="sharethis-inline-share-buttons" data-url="{$system['system_url']}/posts/{$post['post_id']}"></div>
    </div>
</div>
   </div>
</li>{/if}