{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20 offcanvas">
    <div class="row">

        <!-- left panel -->
        <div class="col-sm-4 col-md-3 offcanvas-sidebar">
            {include file='_sidebar.tpl'}
        </div>
        <!-- left panel -->

        <!-- right panel -->
        <div class="col-sm-8 col-md-9 offcanvas-mainbar">

            <!-- tabs -->
            <!-- <div class="box-tabs-wrapper clearfix">
                <ul class="nav pull-left flip">
                    <li {if $view == "" || $view == "find"}class="active"{/if}>
                        <a href="/people">{__("Discover")}</a>
                    </li>
                    <li {if $view == "friend_requests"}class="active"{/if}>
                        <a href="/people/friend_requests">
                            {__("Friend Requests")}
                            {if $user->_data['friend_requests_count'] > 0}
                                <span class="label label-danger ml5">{$user->_data['friend_requests_count']}</span>
                            {/if}
                        </a>
                    </li>
                    <li {if $view == "sent_requests"}class="active"{/if}>
                        <a href="/people/sent_requests">
                            {__("Sent Requests")}
                            {if $user->_data['friend_requests_sent_count'] > 0}
                                <span class="label label-info ml5">{$user->_data['friend_requests_sent_count']}</span>
                            {/if}
                        </a>
                    </li>
                </ul>
            </div> -->
            <!-- tabs -->

            <!-- content -->
            <div class="row">
                <!-- left panel -->
                <div class="col-md-4 col-md-push-8 who-to-middle">
                    <!-- search panel -->
					<!-- 
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-search"></i> <strong>{__("Search")}</strong>
                        </div>
                        <div class="panel-body">
                            <form action="/people/find" method="post">
                                <div class="form-group">
                                    <label>{__("Keyword")}</label>
                                    <input type="text" class="form-control" name="query">
                                </div>
                                <div class="form-group">
                                    <label>{__("Gender")}</label>
                                    <select class="form-control" name="gender">
                                        <option value="any">{__("Any")}</option>
                                        <option value="male">{__("Male")}</option>
                                        <option value="female">{__("Female")}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>{__("Relationship")}</label>
                                    <select class="form-control" name="relationship">
                                        <option value="any">{__("Any")}</option>
                                        <option value="single">{__("Single")}</option>
                                        <option value="relationship">{__("In a relationship")}</option>
                                        <option value="married">{__("Married")}</option>
                                        <option value="complicated">{__("It's complicated")}</option>
                                        <option value="separated">{__("Separated")}</option>
                                        <option value="divorced">{__("Divorced")}</option>
                                        <option value="widowed">{__("Widowed")}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>{__("Online Status")}</label>
                                    <select class="form-control" name="status">
                                        <option value="any">{__("Any")}</option>
                                        <option value="online">{__("Online")}</option>
                                        <option value="offline">{__("Offline")}</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary" name="submit">{__("Search")}</button>
                            </form>
                        </div>
                    </div>

					-->
                    <!-- search panel -->

                   	<!--  {include file='_ads.tpl'}
                    {include file='_widget.tpl'} -->
					
                        <div class="panel panel-default panel-widget ">
                            <div class="panel-heading">
                                <div class="pull-right flip">
                                    <small><a href="/people">{__("View All")}</a></small>
                                </div>
                                <strong class="wtf-module">{__("Who to follow")}</strong>
                            </div>
                            <div class="panel-body">
                                <ul>
                                {if count($user->_data['new_people']) > 0}
                                    {foreach $user->_data['new_people'] as $_user}
                                    {include file='__feeds_user.tpl' _connection="add" _small=true}
                                    {/foreach}
                                 {/if}
                                 {if count($user->_data['new_people']) <= 0}
                                 <p class="text-center text-muted">
                                    {__("No people available")}
                                 </p>
                                 {/if}
                                </ul>
                            </div>
                        </div>
				<!--
                       	<div class="site_bar_new">
                            {$user->_data.popular_hash_tag}
                            <div class="sidebar_trends">
                                <h3>Weekly Trends for you<a href="#">Change</a></h3>
                                
                                {if $user->_data.popular_tag != 'No Record Found'}
                                {foreach $user->_data.popular_tag as $custom_field}
                                <div class="fb_post_rep">
                                <h4><a href="/trend_post.php?tags={$custom_field['url_tags']}">{$custom_field['tags']}</a></h4>
                                <p>{$custom_field['count']} {__("Posts")}</p>
                                </div>
                                {/foreach}
                                {else}
                                <h4>No Trends for you.</h4>
                                {/if}
                        	</div>
                        </div>

-->
                </div>
                <!-- left panel -->

                <!-- right panel -->
                <div class="col-md-8 col-md-pull-4 ">
                    <div class="panel panel-default">
                        {if $view == ""}
                        
                        <div class="panel-heading light">
                            <strong>{__("Who to follow")}</strong>
                        </div>
                            
                        	<form class="navbar-form flip hidden-xs top-menues custom-search-follow" id="header-search">
                                <input id="search-input-follow" type="text" class="form-control" placeholder='{__("Search for people")}' required="required" autocomplete="off">
								<input type="hidden" id="search_for_id" value="people">
                                <button type="submit" class="Icon Icon--medium Icon--search nav-follow-search" tabindex="-1"></button>

                            </form>
                        
                            
                            
                            <div class="panel-body">
                                {if count($people) > 0}
                                    <ul>
                                        {foreach $people as $_user}
                                        {include file='__feeds_user.tpl' _connection="add"}
                                        {/foreach}
                                    </ul>
                                {else}
                                    <p class="text-center text-muted">
                                        {__("No people available")}
                                    </p>
                                {/if}

                                <!-- see-more -->
                               {if count($people) >= $system['min_results']}
                                    <div class="alert alert-info see-more js_see-more mb10 js_see-more {if $user->_logged_in} js_see-more-infinite{/if}" data-get="new_people">
                                        <span>{__("See More")}</span>
                                        <div class="loader loader_small x-hidden"></div>
                                    </div>
                                {/if}
                                <!-- see-more -->
                            </div>
                        {elseif $view == "find"}
                            <div class="panel-heading light">
                                <strong>{__("Search Results")}</strong>
                            </div>
                            <div class="panel-body">
                                {if count($people) > 0}
                                    <ul>
                                        {foreach $people as $_user}
                                        {include file='__feeds_user.tpl' _connection=$_user['connection']}
                                        {/foreach}
                                    </ul>
                                {else}
                                    <p class="text-center text-muted">
                                        {__("No people available for your search")}
                                    </p>
                                {/if}
                            </div>
                        {elseif $view == "friend_requests"}
                            <div class="panel-heading light">
                                <strong>{__("Respond to Your Friend Request")}</strong>
                            </div>
                            <div class="panel-body">
                                {if $user->_data['friend_requests_count'] > 0}
                                    <ul>
                                        {foreach $user->_data['friend_requests'] as $_user}
                                        {include file='__feeds_user.tpl' _connection="request"}
                                        {/foreach}
                                    </ul>
                                {else}
                                    <p class="text-center text-muted">
                                        {__("No new requests")}
                                    </p>
                                {/if}

                                <!-- see-more -->
                                {if $user->_data['friend_requests_count'] >= $system['max_results']}
                                    <div class="alert alert-info see-more js_see-more" data-get="friend_requests">
                                        <span>{__("See More")}</span>
                                        <div class="loader loader_small x-hidden"></div>
                                    </div>
                                {/if}
                                <!-- see-more -->
                            </div>
                        {elseif $view == "sent_requests"}
                            <div class="panel-heading light">
                                <strong>{__("Friend Requests Sent")}</strong>
                            </div>
                            <div class="panel-body">
                                {if $user->_data['friend_requests_sent_count'] > 0}
                                    <ul>
                                        {foreach $user->_data['friend_requests_sent'] as $_user}
                                        {include file='__feeds_user.tpl' _connection="cancel"}
                                        {/foreach}
                                    </ul>
                                {else}
                                    <p class="text-center text-muted">
                                        {__("No new requests")}
                                    </p>
                                {/if}

                                <!-- see-more -->
                                {if $user->_data['friend_requests_sent_count'] >= $system['max_results']}
                                    <div class="alert alert-info see-more js_see-more" data-get="friend_requests_sent">
                                        <span>{__("See More")}</span>
                                        <div class="loader loader_small x-hidden"></div>
                                    </div>
                                {/if}
                                <!-- see-more -->
                            </div>
                        {/if}
                    </div>
                </div>
                <!-- right panel -->
            </div>
            <!-- content -->

        </div>
        <!-- right panel -->

    </div>
</div>
<!-- page content -->

{include file='_footer.tpl'}