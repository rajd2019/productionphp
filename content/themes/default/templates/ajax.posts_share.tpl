<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 class="modal-title">{__("Share Post")}</h5>
</div>
        
    <div class="post_custom">
        <div class="form-group">
            <input type="hidden" name="do" value="share">
            <input type="hidden" name="id" value="{$id}" class="postId">
            <textarea class="expand postComment post_com_en" rows="1" cols="50" value="{$album['title']}" class="form-control" placeholder='{__("Add a comment")}....' name="comment"></textarea>
        </div>
    </div>

    <div class="modal-body">
        
        {foreach $posts as $_user}
        <div class="post-section">
            <div class="post-body"> 
                <div class="post-header"> 
                    <div class="post-avatar"><img src="{$_user['user_picture']}" /></div> 
                    <div class="post-meta">
                    <div>
                    <span>
                        <span class="first-name-custom">{$_user["name"]}</span> 
                        <span class="second-name-custom">@{$_user["user_name"]}</span>
                    </span>
                     <div class="post-time"> <span class="js_moment" data-time="{$_user['time']}">{$_user["time"]}</span>
                     </div>
                     <span class="post-title"> </span> 
                     <div class="post-replace">
                        <div class="post-text js_readmore" dir="auto">{$_user["text"]|unescape:'html'}</div>
                        <div class="post-text-plain hidden">{$_user["text_plain"]}</div> 
                    </div>  
                    </div>
                    </div>
                     
                </div>
        </div>
        {/foreach}
    </div>

    <div class="modal-footer">        
        <button type="submit" class="btn btn-primary post-share btn_share_post">{__("Share")}</button>
    </div>

    <script type="text/javascript">
   $(function() {


//text_post_share();
}); 

$(document).on('keyup', '.post_com_en', function(event) {
   
        
      //  text_post_share()
    });


    function text_post_share()
    {
     

        var post_com=$('textarea.post_com_en').val();
        var post_leng=post_com.length;
            text_counter= post_leng;       
        
            if(text_counter==0)
            {
                $("button.btn_share_post").attr("disabled",true);
                return true;
            }
            else
            {
                $("button.btn_share_post").attr("disabled",false);
                return false;
            }
    }
</script>
