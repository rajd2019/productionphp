{include file='_head.tpl'}
{include file='_header_broadcast_list.tpl'}

  <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet"> 
  <link href="https://unpkg.com/video.js/dist/video-js.css" rel="stylesheet">
    <script src="https://unpkg.com/video.js/dist/video.js"></script>
    <script src="https://unpkg.com/videojs-contrib-hls/dist/videojs-contrib-hls.js"></script>
    <link rel="stylesheet" href="/includes/assets/css/font-awesome/css/custom.css">
   <style>
   body {
       background:#ffffff!important;
   } 
   .autominheight {
       min-height:500px;
   }
   .dotclass {
       font-weight: bold;
        font-size: 25px;
        margin-right: 10px;
        margin-left: 10px;
   }
   </style><?php exit; ?>
    <div class="twitter-main" style="margin-top: 20px;">
            <div class="slider-main-box">
                <div class="banner-section">
                       
              </div>
              <div class="banner-text-section">
              <div class="container">
                  <div class="row">
                     <div class="col-sm-12 col-md-12 col-lg-12 autominheight">
						<div class="banner-text-inner">
							<!--div class="broadcast-live-videos">
								<h2>{__("Video")}</h2>
								
								<video width="100%" controls="controls" preload="auto">
								 <source src="https://stage.guo.media/content/uploads/videos/boss.mp4" type="video/mp4">
								

								</video>
							</div-->
						</div>
                        <div class="banner-text-inner">
                            <div class="header-right">
                                <label id="audio_lebel" for="audio">广播:</label>
                                <select name="broadcast_language" onchange="OpenIframe(this.value)">
                                    <option value="zh">中文</option>
                                    <option value="en">English</option>
                                </select>
                            </div>
                            <div class="broadcast-live-videos">
                                <h2 id="broadcast_heading">{__("郭媒体直播")}</h2>
								
								<!--iframe style="width:100%" id="ls_embed_1524493967" src="https://livestream.com/accounts/27235681/events/8177404/player?width=960&height=540&enableInfoAndActivity=true&defaultDrawer=feed&autoPlay=true&mute=false"  height="540" frameborder="0" scrolling="no" allowfullscreen> </iframe><script type="text/javascript" data-embed_id="ls_embed_1524493967" src="https://livestream.com/assets/plugins/referrer_tracking.js"></script!-->
								<iframe style="width:100%" id="ls_embed_1525861673" src="https://livestream.com/accounts/27235681/events/8197481/player?width=960&height=540&enableInfoAndActivity=true&defaultDrawer=feed&autoPlay=true&mute=false"  height="540" frameborder="0" scrolling="no" allowfullscreen> </iframe><script type="text/javascript" data-embed_id="ls_embed_1525861673" src="https://livestream.com/assets/plugins/referrer_tracking.js"></script>
                <!--iframe id="ls_embed_1524752004" src="https://livestream.com/accounts/27235681/events/8177404/player?width=960&height=540&enableInfoAndActivity=true&defaultDrawer=feed&autoPlay=true&mute=false" width="960" height="540" frameborder="0" scrolling="no" allowfullscreen> </iframe><script type="text/javascript" data-embed_id="ls_embed_1524752004" src="https://livestream.com/assets/plugins/referrer_tracking.js"></script-->
                                <!--div class="no-videos">
                                    
                                </div>
                                <div class="boss col-sm-12" style="float: left;">
                                    
                                </div>
                                <div class="others col-sm-12" style='clear: both;float: left;margin-top:10px'>
                                    
                                </div>
                                <div class="other-title-bar">
                                    <h4>{__("How to broadcast in Guo Media?")}</h4>
                                    <p>{__("We welcome everyone to express their viewpoints freely in our platform. Currently, we allow users to do broadcast via our mobile apps while audience can watch in both website and mobile apps. Please download our apps in App Store to become one of our broadcasters today! In the future, we will allow users to broadcast via website as well. Thank you so much for your support, everything is just beginning ️! ")}
</p>

                                </div-->
                            </div>
                        </div>
                        
                        </div>

                      <div class="login_footer"> 
                      
<!-- footer -->
<div class="container">
   <img src="/content/themes/default/images/live.png" style="display:none">
  <div class="row footer">
    <div class="col-lg-6 col-md-6 col-sm-6">
      &copy; {'Y'|date} {$system['system_title']} · <span class="text-link" data-toggle="modal" data-url="#translator"><span class="flag-icon flag-icon-{$system['language']['flag_icon']}"></span>&nbsp;{$system['language']['title']}</span>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-6 links">
      {if count($static_pages) > 0}
        {foreach $static_pages as $static_page}
          <a href="/static/{$static_page['page_url']}">
            {__("{$static_page['page_title']}")}
          </a>{if !$static_page@last} · {/if}
        {/foreach}
      {/if}
      {if $system['contact_enabled']}
         · 
        <a href="/contacts">{__("Contacts")}</a>
      {/if}
       · 
        <a href="/message_to_miles">{__("Guo")}</a>
      {*if $system['directory_enabled']}
         · 
        <a href="/directory">
          {__("Directory")}
        </a>
      {/if*}
      {if $system['market_enabled']}
                 · 
                <a href="/market">{__("Market")}</a>
            {/if}
    </div>
  </div>
</div>
             </div>
<!-- footer -->
                     </div>
                     
                      </div>
                  
                  </div>
                
              </div>
           </div>
    </div>
<div class="overlay"></div>
{include file='_footer_login.tpl'}
<!-- page content -->
<script type="text/javascript">
jQuery(document).ready(function(){
        updateLiveVideo();
        /*setInterval(function(){
            updateLiveVideo();
        },8000)*/
    });
    
    jQuery(".autominheight").css("min-height",jQuery(window).height()-(jQuery(".main-header").height()+jQuery(".footer").height() + 90));
    
    jQuery(document).ready(function(){
    setTimeout(function(){
     jQuery('body').find("div.live_video").slice(0, 3).show();        
    },2000);
    jQuery(function () {
    jQuery('body').on('click','div#loadMore',function (e) {
        e.preventDefault();
        jQuery(".live_video:hidden").slice(0, 3).slideDown();
        if (jQuery(".live_video:hidden").length == 0) {
            jQuery("#load").fadeOut('slow');
        }
    });
    
});
});
function updateLiveVideo(){
  var settings = {
      "async": true,
      "url": "/list.php",
      "method": "POST",
      "headers": {
        "accept": "application/json",
        "content-type": "application/json"
      }
    };
    //loaded block
    $( ".boss" ).append( "<div class='loader'><span>{__('Miles Guo’s Videos')}</span></div>" );
    $('.loader').show();
    $( ".others" ).append( "<div class='loader'><span>{__('Other’s Videos')}</span></div>" );
    $('.loader').show();
    jQuery.ajax(settings).done(function (res) {
        
        var bossArray = res.boss;
        var othersArray = res.others;
        
        var player = Array();
        if(bossArray.length > 0) {
            jQuery(".boss").append("<div class='post_name'>{__('Miles Guo’s Videos')}</div>");
            for(i=0;i<bossArray.length;i++) {
                jQuery(".boss").append('<div class="live_video"><div class="col-sm-4 video_broadcas="video-js vjs-default-skin" preload="auto" width="300px"st col-xs-12"><video id="video_'+bossArray[i].id+'" clas height="150px"><source src="'+bossArray[i].broadcast_url+'" type="application/x-mpegURL"></video> </div><div class="description col-sm-7 col-xs-12"><div class="video-title"><img src="/content/themes/default/images/live.png" style="width:30px;height: auto;margin-right: 5px;padding-bottom: 5px;"><a href="/video_post?post_id='+bossArray[i].post_id+'" target="_parent">'+bossArray[i].broadcast_name+'</a></div><div class="video-view"><a href="/'+bossArray[i].user_name+'">'+bossArray[i].name+'</a> <span class="dotclass">.</span>'+bossArray[i].views+' {__("Views")}</div><div class="video-des">'+bossArray[i].broadcast_detail+'</div></div></div>');
                     player[i] = videojs('video_'+bossArray[i].id);
                     player[i].play();
                     player[i].volume(0);
                 if(i==0) {
                    $('.loader').hide();
                 }    
            }
            if(bossArray.length > 5) {
             jQuery(".others").append("<div id='loadMore'>{__('Show More')}</div>");
            }
            setTimeout(function(){
                 for(i=0;i<bossArray.length;i++) {
                      player[i].pause();
                 }
            },4000);
            
        } else {
            //loaded none
            jQuery(".boss").css('display','block');
            jQuery(".boss").html('{__("Currently there is no available livestream from Guo.")}');
        }
        
        var other_player = Array();;
        if(othersArray.length > 0) {
            jQuery(".others").append("<div class='post_name'>{__('Other’s Videos')}</div>");
             for(i=0;i<othersArray.length;i++) {
                 var videototal = othersArray.length; 
                jQuery(".others").append('<div class="live_video"><div class="col-sm-4 video_broadcast col-xs-12"><video id="video_other_'+othersArray[i].id+'" class="video-js vjs-default-skin" preload="auto" width="300px" height="150px"><source src="'+othersArray[i].broadcast_url+'" type="application/x-mpegURL"></video></div><div class="description col-sm-7 col-xs-12"><div class="video-title"><img src="/content/themes/default/images/live.png" style="width:30px; height: auto;margin-right: 5px;padding-bottom: 5px;"><a href="/video_post?post_id='+othersArray[i].post_id+'" target="_parent">'+othersArray[i].broadcast_name+'</a></div><div class="video-view"><a href="/'+othersArray[i].user_name+'">'+othersArray[i].name+'</a> <span class="dotclass">.</span>'+othersArray[i].views+' {__("Views")}</div><div class="video-des">'+othersArray[i].broadcast_detail+'</div></div></div>');
                    other_player[i] = videojs('video_other_'+othersArray[i].id);
                    other_player[i].play();
                    other_player[i].volume(0);
                if(i==0) {
                    $('.loader').hide();
                 } 
            }
            if(othersArray.length > 5) {
             jQuery(".others").append("<div id='loadMore'>{__("Show More")}</div>");
            }
            setTimeout(function(){
                 for(i=0;i<othersArray.length;i++) {
                      other_player[i].pause();
                 }
            },8000);
        } else {
             $('.loader').hide();
            jQuery(".others").css('display','block');
            jQuery(".others").html('{__("Currently there is no available livestream from others.")}');
        }
    });  
}
   
function OpenIframe(val){
if(val == 'en'){ 
  $('#ls_embed_1525861673').attr('src','https://livestream.com/accounts/27235681/events/8531480/player?width=640&height=360&enableInfoAndActivity=true&defaultDrawer=&autoPlay=true&mute=false');
  $("#broadcast_heading").html('Guo Media Broadcast');
  $("#audio_lebel").html('Broadcast:');  
}else{
  $('#ls_embed_1525861673').attr('src','https://livestream.com/accounts/27235681/events/8197481/player?width=960&height=540&enableInfoAndActivity=true&defaultDrawer=feed&autoPlay=true&mute=false');
  $("#broadcast_heading").html('郭媒体直播');
  $("#audio_lebel").html('广播:');
}
} 
</script>