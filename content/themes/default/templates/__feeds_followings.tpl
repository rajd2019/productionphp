<div class="js_posts_stream" data-get="{$_get}" data-filter="{if $_filter}{$_filter}{else}all{/if}" {if $_id}data-id="{$_id}"{/if}>
		<ul>
			<!-- posts -->
                    <li>
                    <h3 class="AdaptiveSearchPage-moduleTitle">{__("People")}</h3>
                    <div class="row">
                         {foreach $people as $_user}
                    
                        	
                            <div class="col-sm-4">
                            	<div class="site_bar_new custom-site-bar-new"> 

                                    <div class="profile-bg-thumb">
                                    {if $_user['user_cover']}<img src="{$system['system_uploads']}/{$_user['user_cover']}">{/if}
                                    </div>
                                    <div class="side_profile" style="height: 175px;"> 
                                        <div class="pro_thumb"> 
                                            <a href="/{$_user['user_name']}{if $_search}?ref=qs{/if}">
                                                <img class="data-avatar" src="{$_user['user_picture']}" alt="{$_user['user_firstname']} {$_user['user_lastname']}">
                                            </a>
                                        </div> 
                                        <div class="admin_detail"> 
                                             <span>
											 								
											 	{if $_user['connection'] eq 'unfollow'} 
 
											  <a class="btn btn-default js_unfollow" data-uid="{$_user['user_id']}" >
												<i class="fa fa-check"></i>{__("Following")}
											 </a>
											    
                                                
											 {else}

                                       <a class="btn btn-default js_follow" data-uid="{$_user['user_id']}">

                                            <i class="fa fa-rss"></i>

                                            {__("Follow")}

                                        </a>

                                        {/if}
                                        
                                            </span> 
                                            <span class="name js_user-popover" data-uid="{$_user['user_id']}"> 
                                                <a href="/{$_user['user_name']}{if $_search}?ref=qs{/if}">
                                                    <span>{$_user['user_firstname']} {$_user['user_lastname']}</span> 
                                                    <span class="user-at-btn">@{$_user['user_name']}</span>
                                                </a> 
                                            </span> 
                                        </div> 
                                        <p>{$_user['user_biography']|truncate:120:"&hellip;"}</p> 
                                	</div> 
                            	</div>
                            </div>
                            
                            
                            	
                            
                        {/foreach}</div>
                    </li>
                    </ul>
               
           