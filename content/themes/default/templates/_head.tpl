<!DOCTYPE html>

<!--[if IE 8]><html class="ie8"> <![endif]-->
<!--[if IE 9]><html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="gt-ie8 gt-ie9 not-ie" lang="{$system['language']['code']}" {if $system['language']['dir'] == "RTL"} dir="RTL" {/if}>
<!--<![endif]-->

<head>
    {include file='_ga_tag.tpl'}
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="generator" content="Sngine">
    <meta name="version" content="{$system['system_version']}">
    <meta name="google" content="notranslate" />
    <meta name="google" value="notranslate" />
    <!-- Title -->
    <title>郭媒体 GUO.MEDIA</title>
    <!-- Title -->

    <!-- Meta -->
    <meta name="description" content="{$system['system_description']}">
    <meta name="keywords" content="{$system['system_keywords']}">
    <!-- Meta -->

    <!-- OG-Meta -->
    <meta property="og:title" content="{$page_title}"/>
    <meta property="og:description" content="{$system['system_description']}"/>
    <meta property="og:url" content="{$system['system_url']}"/>
    <meta property="og:site_name" content="{$system['system_title']}"/>
    <!-- OG-Meta -->

    <!-- OG-Image -->
    {if $system['system_ogimage']}
        <meta property="og:image" content="{$system['system_uploads']}/{$system['system_ogimage']}"/>
    {elseif $system['system_ogimage_default']}
        <meta property="og:image" content="/content/themes/{$system['theme']}/images/og-image.jpg"/>
    {/if}
    <!-- OG-Image -->

    <!-- Favicon -->
    {if $system['system_favicon_default']}
        <link rel="shortcut icon" href="/content/themes/{$system['theme']}/images/favicon.png" />
    {elseif $system['system_favicon']}
        <link rel="shortcut icon" href="{$system['system_uploads']}/{$system['system_favicon']}" />
    {/if}
    <!-- Favicon -->

    <!-- Dependencies CSS [Bootstrap|Font-Awesome] -->
    <style type="text/css">{include file="../../../../includes/assets/css/bootstrap/css/bootstrap+social.css" caching}</style>
    {if $user->_logged_in}
    <link rel="stylesheet" href="/includes/assets/css/font-awesome/css/font-awesome.min.css">
    {/if}
    <!-- Dependencies CSS [Bootstrap|Font-Awesome] -->

    <!-- CSS -->
    {if $system['language']['dir'] == "LTR"}
        <style type="text/css">{include file="../css/style.css" caching}</style>
    {else}
        <link rel="stylesheet" type='text/css' href="/includes/assets/css/bootstrap/css/bootstrap-rtl.min.css">
        <style type="text/css">{include file="../css/style.rtl.css" caching}</style>
    {/if}

    <!-- CSS -->
	
	{if $user->_data['theme_color'] != ''} 
		{include file='theme_color.tpl'}
	{/if}
    <style type="text/css">
		
		/***** Start Flash Button *******/
			.flash-button-start {
			  background-color: #004A7F;
			  /*-webkit-border-radius: 10px;*/
			  /*border-radius: 10px;*/
			  border: none;
			  color: #FFFFFF;
			  cursor: pointer;
			  display: inline-block;
			  font-family: Arial;
			  font-size: 14px;
			  padding: 5px 10px;
			  text-align: center;
			  text-decoration: none;
			  -webkit-animation: glowing 1500ms infinite;
			  -moz-animation: glowing 1500ms infinite;
			  -o-animation: glowing 1500ms infinite;
			  animation: glowing 1500ms infinite;
			  margin: 10px 0px 15px 23px;
			}
			@-webkit-keyframes glowing {
			  0% { background-color: #0b6623; -webkit-box-shadow: 0 0 3px #0b6623; }
			  50% { background-color: #0b6623; -webkit-box-shadow: 0 0 20px #0b6623; }
			  100% { background-color: #0b6623; -webkit-box-shadow: 0 0 3px #0b6623; }
			}

			@-moz-keyframes glowing {
			  0% { background-color: #0b6623; -moz-box-shadow: 0 0 3px #0b6623; }
			  50% { background-color: #0b6623; -moz-box-shadow: 0 0 20px #0b6623; }
			  100% { background-color: #0b6623; -moz-box-shadow: 0 0 3px #0b6623; }
			}

			@-o-keyframes glowing {
			  0% { background-color: #0b6623; box-shadow: 0 0 3px #0b6623; }
			  50% { background-color: #0b6623; box-shadow: 0 0 20px #0b6623; }
			  100% { background-color: #0b6623; box-shadow: 0 0 3px #0b6623; }
			}

			@keyframes glowing {
			  0% { background-color: #0b6623; box-shadow: 0 0 3px #0b6623; }
			  50% { background-color: #0b6623; box-shadow: 0 0 20px #0b6623; }
			  100% { background-color: #0b6623; box-shadow: 0 0 3px #0b6623; }
			}
		/***** End Flash Button Script *******/
		
		/***** Stop Flash Button *******/
		.flash-button-stop {
		   background-color: #004A7F;
		  /*-webkit-border-radius: 10px;*/
		  /*border-radius: 10px;*/
		  border: none;
		  color: #FFFFFF;
		  cursor: pointer;
		  display: inline-block;
		  font-family: Arial;
		  font-size: 14px;
		  padding: 5px 10px;
		  text-align: center;
		  text-decoration: none;
		  -webkit-animation: glowing2 1500ms infinite;
		  -moz-animation: glowing2 1500ms infinite;
		  -o-animation: glowing2 1500ms infinite;
		  animation: glowing2 1500ms infinite;
		  margin: 10px 0px 15px 23px;
		}
		@-webkit-keyframes glowing2 {
		  0% { background-color: #B20000; -webkit-box-shadow: 0 0 3px #B20000; }
		  50% { background-color: #FF0000; -webkit-box-shadow: 0 0 40px #FF0000; }
		  100% { background-color: #B20000; -webkit-box-shadow: 0 0 3px #B20000; }
		}

		@-moz-keyframes glowing2 {
		  0% { background-color: #B20000; -moz-box-shadow: 0 0 3px #B20000; }
		  50% { background-color: #FF0000; -moz-box-shadow: 0 0 40px #FF0000; }
		  100% { background-color: #B20000; -moz-box-shadow: 0 0 3px #B20000; }
		}

		@-o-keyframes glowing2 {
		  0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
		  50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
		  100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
		}

		@keyframes glowing2 {
		  0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
		  50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
		  100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
		}
		/***** End Flash Button Script *******/
		
          #map{ width:700px; height: 500px; }
          html, body, div, applet, object, iframe,
            h1, h2, h3, h4, h5, h6, p, blockquote, pre,
            a, abbr, acronym, address, big, cite, code,
            del, dfn, em, img, ins, kbd, q, s, samp,
            small, strike, strong, sub, sup, tt, var,
            b, u, center,
            dl, dt, dd, ol, ul, li,
            fieldset, form, label, legend,
            table, caption, tbody, tfoot, thead, tr, th, td,
            article, aside, canvas, details, embed,
            figure, figcaption, footer, header, hgroup,
            menu, nav, output, ruby, section, summary,
            time, mark, audio, video , input  {
              /*font-family :' Apple LiGothic Medium', 'STHeiti', 'LiHei Pro Medium', 'Microsoft JhengHei', 'Noto Sans CJK' !important;*/
              font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;
            }
            .ProfileHeading-toggle {
                border-bottom: 1px solid #ddd;
            }

            .profile-sidebar .panel-default>.panel-heading , .panel-default>.panel-heading {
                color: #333;
                background-color: white !important;
                border-color: white !important;
            }
            .offcanvas-mainbar .col-sm-3 .panel-default{
                border:solid 1px #ddd !important;
            }
            .main-header .nav > li > a span {
                font-size: 15px;
            }
            .right-section ul > li > a, .left-section ul > li > a, .js-current-language {
                    font-size: 15px !important;
                }
            .profile-rightbar .modal-messaging-bottom__button {
                width: 90%;
                padding: 7px;
                border: 1px solid #5a656b;
                margin: 10px auto;
                display: table;
                font-size: 14px;
                margin-left: 0;
                margin-top: 15px;
                background-color: #5a656b;
                border-radius: 26px;
                height: auto;
            }
            .profile-rightbar .modal-messaging-bottom__button:hover, .profile-rightbar .modal-messaging-bottom__button:focus {
                border: 1px solid #000;
                background: #000;
            }
            .inner_link ul > li > a {
                text-decoration: none;
            }
            .inner_link {
                padding: 0px 0 0 160px;
                float: left;
            }
            .contact-icon.watch_broadcasts_logout {
                margin: -8px 0px 0px 15px;
                display: inline-block;
                float: right;
            }
            .download-video-link > a{
                color:#657786;
                margin-left: 10px;
            }
            .download-video-link > a:hover{
                color:#657786;
            }
            .download-video-link > a:active{
                color:#657786;
            }
            @media(max-width:1024px){
                .inner_link {
                    padding: 0px 0 0 0px;
                    float: left;
                }
            }
            @media(max-width:767px){
                  .mobile-menu .navbar-nav .open .dropdown-menu {
                    position: absolute !important;
                    float: left  !important;
                    width: 100%  !important;
                    margin-top: 0  !important;
                    background-color: #fff  !important ;
                    border: 1px solid #ccc  !important;
                    border: 1px solid rgba(0,0,0,.15)  !important;
                    border-top: 0  !important ;
                    -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175)  !important ;
                    box-shadow: 0 6px 12px rgba(0,0,0,.175) !important ;
                }
                .main-header .header-right .nav > li > a {
                    padding: 0px 4px 0 9px;

               }
               .contact-icon.watch_broadcasts{

                       margin: 0px 0px 0px 6px;
               }
               .profile-tabs-wrapper ul {
                   display: table;
                   width: auto;
                   margin: 0 auto;
               }
                .main-header .user-menu>img, .post-avatar-picture {
                  height: 35px;
                  width: 35px;
                }
                /*.sticky_header_second{
                    top:52px;
                }  */
            }

            @media(max-width:480px){
                .contact-icon.watch_broadcasts_logout {
                    margin: -8px 0px 0px 5px;
                    display: inline-block;
                    float: right;
                }
                .contact-icon a {
                    padding: 10px 12px 10px 26px;
                    float: left;
                    font-size: 10px;
                    background-size: 13px;
                }
                .profile-tabs-wrapper > ul > li > a {
                    padding: 12px 3px;
                }

            }
        </style>



    <!-- CSS Customized -->
    {include file='_head.css.tpl'}
    <!-- CSS Customized -->
    <script src="/includes/assets/js/jquery/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.1/jquery-migrate.min.js"></script>
    <script>
        var shiftWindow = function() { scrollBy(0, -50) };
        window.addEventListener("hashchange", shiftWindow);
        function load() { if (window.location.hash) shiftWindow(); }
    </script>

    <link rel="stylesheet" href="/content/themes/default/plugins/jquery-emoji-picker-master/css/jquery.emojipicker.css">
    <script type="text/javascript" src="/content/themes/default/plugins/jquery-emoji-picker-master/js/jquery.emojipicker.js"></script>
    <!-- Emoji Data -->
    <link rel="stylesheet" href="/content/themes/default/plugins/jquery-emoji-picker-master/css/jquery.emojipicker.tw.css">
    <script type="text/javascript" src="/content/themes/default/plugins/jquery-emoji-picker-master/js/jquery.emojis.js"></script>
	<!-- Share This start -->
	<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5b06c150acd3680011c1afe4&product=inline-share-buttons' async='async'></script>
	<!-- Share This end -->
	<script>
      
		function BroadCastStart(){
	
			 var data = {
				  "action": "test"
				};
			
			$('#broad_start_stop').html('');
			//$('.dropdown-menu').show();
			$("document").ready(function(){	
                	
				$.ajax({
				  type: "POST",
				  dataType: "json",
				  url: "https://www.guo.media/start_bcast", //Relative or absolute path to response.php file
				  data: data,
				  success: function(data) {
					/*$(".the-return").html(
					  "Favorite beverage: " + data["favorite_beverage"] + "<br />Favorite restaurant: " + data["favorite_restaurant"] + "<br />Gender: " + data["gender"] + "<br />JSON: " + data["json"]
					);*/
					//alert("Form submitted successfully.\nReturned json: " + data["json"]);
					
					if(data["json"] == 'Success' && data["status_start"] == 'Yes')
					{
						$('#broad_start_stop').html('<button type="button" id="flash-button" class="flash-button-stop" onclick="BroadCastStop()">Stop Broadcast</button>');
						
						//$('#blink-tv').html('<a href="/broadcasts" class="btn btn-secondary dropdown-toggle blink-custom" type="button" id="dropdownMenuButton"><span class="Icon icon_custom Icon--cameraVideo Icon--small "></span>{__("GuoTV")}</a><a href="/broadcasts" class="btn btn-secondary dropdown-toggle blink-custom" type="button" id="dropdownMenuButton"><span class="Icon icon_custom Icon--cameraVideo Icon--small "></span>GuoTV</a>');
						
						//setInterval(blink_text, 1000);
						//$('#flash-button').removeClass('flash-button-start');
						//$('#flash-button').addClass('flash-button-stop');
						
						window.location.reload();
					}	
				  }
				});	
			
			});
		}
		function BroadCastStop(){
	
			 var data = {
				  "action": "test"
				};
			
			$('#broad_start_stop').html('');
			//$('.dropdown-menu').show();
			$("document").ready(function(){				
				$.ajax({
				  type: "POST",
				  dataType: "json",
				  url: "https://www.guo.media/stop_bcast", //Relative or absolute path to response.php file
				  data: data,
				  success: function(data) {
					/*$(".the-return").html(
					  "Favorite beverage: " + data["favorite_beverage"] + "<br />Favorite restaurant: " + data["favorite_restaurant"] + "<br />Gender: " + data["gender"] + "<br />JSON: " + data["json"]
					);*/
					//alert("Form submitted successfully.\nReturned json: " + data["json"]);
					
					if(data["json"] == 'Success' && data["status_start"] == 'No')
					{
						//$('#broad_start_stop').html('<button type="button" id="flash-button" class="flash-button-start" onclick="BroadCastStart()">Start Broadcast</button>');
						
						//$('#blink-tv').html('<a href="/broadcasts" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"><span class="Icon icon_custom Icon--cameraVideo Icon--small "></span>{__("GuoTV")}</a><a href="/broadcasts" class="btn btn-secondary dropdown-toggle blink-custom" type="button" id="dropdownMenuButton"><span class="Icon icon_custom Icon--cameraVideo Icon--small "></span>GuoTV</a>');
						//$('#flash-button').removeClass('flash-button-start');
						//$('#flash-button').addClass('flash-button-stop');
						
						window.location.reload();
					}	
				  }
				});	
			
			});
		}
		/*function blink_text() {
			$('.blink-custom').fadeOut(100);
			$('.blink-custom').fadeIn(100);
		}*/
		/*setInterval(blink_text, 1000);*/
	</script>
</head>
