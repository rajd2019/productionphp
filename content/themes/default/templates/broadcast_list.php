{include file='_head.tpl'}
{include file='_header_broadcast_list.tpl'}

  <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet"> 
  <link href="https://unpkg.com/video.js/dist/video-js.css" rel="stylesheet">
    <script src="https://unpkg.com/video.js/dist/video.js"></script>
    <script src="https://unpkg.com/videojs-contrib-hls/dist/videojs-contrib-hls.js"></script>
    <link rel="stylesheet" href="/includes/assets/css/font-awesome/css/custom.css">
    <link rel="stylesheet" href="/includes/assets/css/tokbox/app.css">
	<script src="https://static.opentok.com/v2/js/opentok.min.js"></script> 
   <style>
   body {
       background:#ffffff!important;
   } 
   .autominheight {
       min-height:500px;
   }
   .dotclass {
       font-weight: bold;
        font-size: 25px;
        margin-right: 10px;
        margin-left: 10px;
   }
   </style>
    <div class="twitter-main" style="margin-top: 20px;">
            <div class="slider-main-box">
                <div class="banner-section">
                       
              </div>
              <div class="banner-text-section">
              <div class="container">
                  <div class="row">
                     <div class="col-sm-12 col-md-12 col-lg-12 autominheight">
                        <div class="banner-text-inner">
                            <div class="broadcast-live-videos">
                                <h2>{__("Browse all available broadcasts")}</h2>
								<div class="webinar">
									<div id="videos">
									  <div id="publisher"></div>
									  <!--<div style="height: 200px; overflow-y: auto;">-->
									  <div id="subscriber"></div>			
									  <!--</div>	-->					
									</div>
								
								<!-- Chat box starts here -->
								  <div id="textchat">
									  <div class="chatBxHdr">Chat Live Window</div>
									  <div class="chatListBx">
										  <p id="history"></p>
									  </div>
										<input type="text" placeholder="Input your text here" id="msgTxt"></input>
								  </div>
								</div>
								<div class="broadcast">
								<?php echo 'hello'; ?>
									<iframe src="https://broadcast-sample.herokuapp.com/host" style="position:fixed; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
										Your browser doesn't support iframes
									</iframe>
								</div>
							  <!-- Chat box ends here -->

								<!--iframe src="//iframe.dacast.com/b/110860/c/471241" width="590" height="431" frameborder="0" scrolling="no" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe-->
                <!--iframe id="ls_embed_1524752004" src="https://livestream.com/accounts/27235681/events/8177404/player?width=960&height=540&enableInfoAndActivity=true&defaultDrawer=feed&autoPlay=true&mute=false" width="960" height="540" frameborder="0" scrolling="no" allowfullscreen> </iframe><script type="text/javascript" data-embed_id="ls_embed_1524752004" src="https://livestream.com/assets/plugins/referrer_tracking.js"></script-->
                                <!--div class="no-videos">
                                    
                                </div>
                                <div class="boss col-sm-12" style="float: left;">
                                    
                                </div>
                                <div class="others col-sm-12" style='clear: both;float: left;margin-top:10px'>
                                    
                                </div>
                                <div class="other-title-bar">
                                    <h4>{__("How to broadcast in Guo Media?")}</h4>
                                    <p>{__("We welcome everyone to express their viewpoints freely in our platform. Currently, we allow users to do broadcast via our mobile apps while audience can watch in both website and mobile apps. Please download our apps in App Store to become one of our broadcasters today! In the future, we will allow users to broadcast via website as well. Thank you so much for your support, everything is just beginning ️! ")}
</p>

                                </div-->
                            </div>
                        </div>
                        
                        </div>

                      <div class="login_footer"> 
  
<!-- footer -->

<div class="container">
   <img src="/content/themes/default/images/live.png" style="display:none">
  <div class="row footer">
    <div class="col-lg-6 col-md-6 col-sm-6">
      &copy; {'Y'|date} {$system['system_title']} · <span class="text-link" data-toggle="modal" data-url="#translator"><span class="flag-icon flag-icon-{$system['language']['flag_icon']}"></span>&nbsp;{$system['language']['title']}</span>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-6 links">
      {if count($static_pages) > 0}
        {foreach $static_pages as $static_page}
          <a href="/static/{$static_page['page_url']}">
            {__("{$static_page['page_title']}")}
          </a>{if !$static_page@last} · {/if}
        {/foreach}
      {/if}
      {if $system['contact_enabled']}
         · 
        <a href="/contacts">{__("Contacts")}</a>
      {/if}
       · 
        <a href="/message_to_miles">{__("Guo")}</a>
      {*if $system['directory_enabled']}
         · 
        <a href="/directory">
          {__("Directory")}
        </a>
      {/if*}
      {if $system['market_enabled']}
                 · 
                <a href="/market">{__("Market")}</a>
            {/if}
    </div>
  </div>
</div>
             

  </div>
<!-- footer -->
                     </div>
                     
                      </div>
                  
                  </div>
                
              </div>
           </div>
    </div>
<div class="overlay"></div>
{include file='_footer_login.tpl'}
<script src="/includes/assets/js/tokbox/config.js"></script>
<script src="/includes/assets/js/tokbox/app.js"></script>