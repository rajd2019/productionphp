<style type="text/css">
    /* for direct share post message*/
.chat-conversations .text {
    width: 100% !important;
    opacity: 1 !important;
}
.chat_search_area .x-form-tools-emoji1.js_emoji-menu-toggle1 {    position: absolute;  bottom: 5px; cursor: pointer; left: -4px;}
.lsx-emojipicker-container {
    right: 0;
}
.chat_search_area .x-form-tools{
    right: 27px !important;
}

#modal .custom_autocomplete .typeahead .TokenizedMultiselect-inputContainer {
    width: auto !important;
    text-align: left !important;
}

#modal .panel-messages .chat-to .to {
    margin-left: 14px !important;
    text-align: left !important;
}

.conversation {        
    padding-right: 14px;    
    padding-left: 16px;
}

/*new css*/
.u-borderUserColorLighter{
	width: -webkit-calc(100% - 179px);
	width: -moz-calc(100% - 179px);
	width: -o-calc(100% - 179px);
	width: calc(100% - 179px);
}
textarea#custom-editor {
    width: 100%;
}
.emoji-selector li {
    margin: 0;
    width: 0;
}
.nav-tabs>li>a {
    margin: 0;
    padding: 0;
}
.modal-content .publisher, .modal-content .panel{
overflow:hidden;
}
button.btn.btn-primary.post-chat {
	display: inline-block;
	margin: 15px 0 0;
	float: right;
}
.modal-footer.custom-model-footer {
	padding: 0 !important;
}
.modal-body span#user_loading_id_fr_post {
    height: 100%;
    width: 100%;
    display: table;
    vertical-align: middle;
}
.js_conversation-container .panel {
	border: 0px;
	margin: 0;
}

.panel-messages .chat-form-message textarea {
    height: 55px !important;
}
.x-form-tools-emoji1 .publisher-tools-attach i.fa.fa-smile-o.fa-fw {
    font-size: 19px;
}
.custom_autocomplete .auto-complete ul{
	margin:0;
}
.chat-form-container{
	text-align:left;
}
.lsx-emojipicker-tabs img.emoji{
	margin:2px 7px;
}
.modal-footer.custom-model-footer {
    padding: 0 !important;
    position: absolute;
    right: 0;
    bottom: 88px;
    top: auto;
}
/*new css end*/
@media (min-width:768px) and (max-width: 3000px) {
    #modal .modal-dialog .modal-footer .btn {
        margin-top: -20px !important;
    }

    #modal .panel-heading {
        background: #f5f8fa;
        border-radius:6px 6px 0 0;
    }
    .x-form.chat-form.chat_search_area {
		width: 500px;
		display: inline-block;
	}
	button.btn.btn-primary.post-chat {
		display: inline-block;
		margin: 15px 0 0;
		float: right;
	}
	.modal-footer.custom-model-footer {
		padding: 0 !important;
	}
	.js_conversation-container .panel {
		border: 0px;
		margin: 0;
	}
	.chat-form-message {
		padding-right: 31px !important;
	}
	.panel-messages .chat-form-message textarea{
		padding:0;
	}
	.modal-footer.custom-model-footer{
		 bottom: 30px;
	}
}


@media (min-width:320px) and (max-width: 767px) {

    #modal .slimScrollDiv {
        height: auto !important;
    }

    .slimScrollDiv ul {
        /*height: -webkit-calc(100vh - 266px) !important;
		height: -moz-calc(100vh - 266px) !important;
		height: -o-calc(100vh - 266px) !important;
		height: calc(100vh - 266px) !important;*/
    }

    #modal .panel-messages .chat-form-message textarea {
        height: 45px !important;
		padding:0;
    }

     #modal .conversation {
        padding-top: 24px;
        margin-bottom: 10px;
     }

     button.close {
        top:20px;
     }

    #modal .chat-conversations {
        height: 390px !important;
        padding: 99px 7px 0 7px;
    }

    .panel-messages .chat-to .to {
        padding-top: 10px;
    }

    #modal .panel-heading {
        padding-bottom: 13px;
        background: #f5f8fa;
    }

    #modal .modal-dialog {
        height: 100%;
        position: relative;
    }

    .chat-form-container {
        position: fixed;
        width: 100%;
		-webkit-bottom: 26px;
        bottom: 114px;
		
    }

    .custom_autocomplete .data-content {   
        padding: 7px 0 0 60px !important;
    }

    .publisher-tools-attach .fa.fa-smile-o {
        font-size: 20px;
    }
    .x-uploader .fa {
        font-size: 16px !important;
    }

    .panel-messages .chat-to {
        padding-top: 0 !important;
        padding-left: 6px !important;
        padding-right: 6px !important;
        padding-bottom: 0 !important;
    }

    .custom_autocomplete .typeahead .TokenizedMultiselect-inputContainer {
        margin-top: 2px;
    }

    .modal-footer {
        height: 90px;
        padding:0 !important;
    } 

    .modal-dialog .modal-footer .btn {
        right: 6px;
        position: relative;
        top:34px;
        border-radius: 10px;
        min-width: 60px;
        padding: 5px 12px;
    }

    .panel-messages .chat-form-message {
        width: 98%;
    }



    .lsx-emojipicker-container {
        position: fixed !important;
		top: auto !important;
		right: 75px !important;
		bottom:156px !important;
    }
	div#modal {
		top: 0;
		height: 100vh;
		background-color: #fff;
		left: 0;
		width: 100%;
		transform: translate(0%, 0%);
		-webkit-transform: translate(0%, 0%);
		-o-transform: translate(0%, 0%);
		-moz-transform: translate(0%, 0%);
	}
	#modal .chat-conversations {
    	height: -webkit-calc(100vh - 154px) !important;
		height: -moz-calc(100vh - 154px) !important;
		height: -o-calc(100vh - 154px) !important;
		height: calc(100vh - 154px) !important;
	}
	.x-form.chat-form.chat_search_area {
		width: -webkit-calc(100% - 54px);
		width: -moz-calc(100% - 54px);
		width: -o-calc(100% - 54px);
		width: calc(100% - 54px);
		display: inline-block;
	}
	.chat_search_area .x-form-tools {
		right: 40px !important;
		bottom:-2px;
	}
	.js_conversation-container .panel-messages.fresh .panel-body {
		max-height: none;
	}
	.chat-attachments {
		position: fixed;
		bottom: 197px;
		width: 100%;
	}
	.js_conversation-container .panel-messages.fresh .panel-body{
		height:100vh;
	}
	.modal-footer.custom-model-footer {
		bottom: 134px;
	}
}

.panel-messages.fresh .panel-body {
    max-height: 400px;
    overflow-y: auto !important;
    /* border: 1px solid; */
}

@media (min-width:414px) and (max-width: 767px) { 
    #modal .modal-dialog {
        height: 100% !important;
        position: relative;
    }

    .modal-dialog .modal-footer .btn {
        top: 40px !important;
    }

    /*#modal .chat-conversations {
        height: 445px !important;
    }*/
 }

@media (min-width:320px) and (max-width: 767px) {
    .panel-messages.fresh .panel-body {
		max-height: 450px;
		overflow-y: auto !important;
		/* border: 1px solid; */
	}
}
@media (max-width: 480px) and (min-width: 320px){
	#modal .panel-heading{
		padding:4px 0 8px;
	}
	.custom-nw-msg {
		font-size: 16px;
	}
	button.close {
		top: 8px;
	}
	.panel-messages .chat-to .to {
		padding-top: 6px;
	}
	#modal .chat-conversations {
		height: 390px !important;
		padding: 81px 7px 0 7px;
	}
	.custom_autocomplete .typeahead {
		padding: 3px 0;
	}
	#modal .chat-conversations {
		height: -webkit-calc(100vh - 131px) !important;
		height: -moz-calc(100vh - 131px) !important;
		height: -o-calc(100vh - 131px) !important;
		height: calc(100vh - 131px) !important;
	}
	.slimScrollDiv ul {
		/*height: -webkit-calc(100vh - 192px) !important;
		height: -moz-calc(100vh - 192px) !important;
		height: -o-calc(100vh - 192px) !important;
		height: calc(100vh - 192px) !important;*/
	}
	.custom_autocomplete .dropdown-menu.auto-complete{
		margin-top:2px !important;	
	}
	.modal-content {
		height: 100%;
	}
}
</style>
<div class="modal-body dir-msg plr0 ptb0">

    <div class="x-form publisher mini">

        <div class="panel-heading clearfix">

            <div class="mt5 custom-nw-msg">New Message  </div> 

            <button type="button" class="close mr10 pull-right flip" data-dismiss="modal" aria-hidden="true">×</button> 

            <div class="pull-left flip"> 

                <a class="custom_back_btn" href="#" data-toggle="modal" data-dismiss="modal" aria-hidden="true">

                    <span class="Icon Icon--caretLeft u-linkComplex-target Icon--medium"></span>

                </a> 

            </div>

        </div>

        <div class="js_conversation-container" id="chat-box">

        <div class="panel panel-default panel-messages fresh">

                   

                    <div class="panel-body panel-new-msg">

                        <div class="chat-conversations js_scroller" data-slimScroll-height="">
						<span class="loading" id="user_loading_id_fr_post" style="display: none">

                                            <span class="loader loader_medium"></span>

                                    </span>
						</div>

                        <div class="chat-to clearfix js_autocomplete custom_autocomplete">

                            <div class="to">{__("Send message to")}:</div>

                            

                            <div class="typeahead">

                            <div class="TokenizedMultiselect-inputContainer">

                               <ul class="tags"></ul> 

                               <input type="text" size="1" autofocus >

                               </div>

                              

                            </div>

                        </div>
						<span class="loading" id="user_loading_id" style="display: none">
                                            <span class="loader loader_small"></span>
                                        </span>

                        <div class="chat-attachments attachments clearfix x-hidden">

                            <ul>

                                <li class="loading">

                                    <div class="loader loader_small"></div>

                                </li>

                            </ul>

                        </div>

                        <div class="chat-form-container">
                            <div class="x-form chat-form chat_search_area">
                                <div class="chat-form-message custom-chat-form" id="replaceTextareaOfPost">
                                    <textarea class="js_autosize direct_share_post_chk" placeholder='{__("Write a message")}' id="direct_share_post_chk_id" style="height: 120px;"></textarea>
                                </div>
                                
                                <input type="hidden" id="post_id_value" value="{$post_id}">
                                <!-- <div class="chat-form-message custom-chat-form">

                                    <textarea class="js_post-message shareDirectPost"  id="shareDirectPost" placeholder='{__("Write a message")}'></textarea>
 
                                </div> -->
                                <div class="x-form-tools">
                                    <div class="x-form-tools-attach">
                                            <i class="fa fa-camera js_x-uploader" data-handle="chat"></i>
                                    </div>
                                    <div class="x-form-tools-emoji1 js_emoji-menu-toggle1">
                                        <span class="publisher-tools-attach js_publisher-photos newalbumClass picker_direct_share" style="overflow:visible">
                                        <i class="fa fa-smile-o fa-fw"></i>
                                        </span>
                                    </div>
                                </div> 
                            </div>
							
                            <!-- <ul class="publisher-tools">
                                <li data-toggle="tooltip" class="" data-placement="top">
                                    <div class="x-form-tools-attach">

                                        <i class="fa fa-camera js_x-uploader" data-handle="chat"></i>

                                    </div>
                                </li>&nbsp;
                                <li data-toggle="tooltip" class="" data-placement="top">
                                    <div class="x-form-tools-emoji1 js_emoji-menu-toggle1">
                                    <span class="pickersharePost" style="overflow:visible">
                                        <i class="fa fa-smile-o fa-fw"></i>
                                    </span>
                                </div>
                                </li>
                            </ul> -->
<!-- <div class="custom-post-actions bottom-icons" style="float:none;"> 
<div class="post-actions"> 

<div class="x-form-tools-attach">

                                        <i class="fa fa-camera js_x-uploader" data-handle="chat"></i>

                                    </div>

                                    <div class="x-form-tools-emoji1 js_emoji-menu-toggle1">
                                    <span class="pickersharePost" style="overflow:visible">
                                        <i class="fa fa-smile-o fa-fw"></i>
                                    </span>
                                </div>
 
</div> 
</div> -->
                            

                        </div>

                    </div>

                </div>

       </div>

    </div>

</div>

<div class="modal-footer custom-model-footer"><button type="submit" class="btn btn-primary post-chat">{__("Send")}</button></div>
<script type="text/javascript">

jQuery.fn.putCursorAtEnd = function() {

          return this.each(function() {

            $(this).focus()

            // If this function exists...
            if (this.setSelectionRange) {
              // ... then use it (Doesn't work in IE)

              // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
              var len = $(this).val().length * 2;

              this.setSelectionRange(len, len);

            } else {
            // ... otherwise replace the contents with itself
            // (Doesn't work in Google Chrome)

              $(this).val($(this).val());

            }

            // Scroll to the bottom, in case we're in a tall textarea
            // (Necessary for Firefox and Google Chrome)
            this.scrollTop = 999999;

          });

    };


    $(function() {
        
        $('.picker_direct_share').lsxEmojiPicker({
            closeOnSelect: true,
            twemoji: true,
            onSelect: function(emoji){
                
                var cursorPosition = $(".direct_share_post_chk")[0].selectionStart;
                var FirstPart = $(".direct_share_post_chk").val().substring(0, cursorPosition);
                var NewText = emoji.value;
                var SecondPart = $(".direct_share_post_chk").val().substring(cursorPosition + 1, $(".direct_share_post_chk").val().length);
                
                $(".direct_share_post_chk").html(FirstPart+NewText+SecondPart);
                
                var txt_placeholder = $(".direct_share_post_chk").attr('placeholder');

                
                //$("#getrplyVal").val(FirstPart+NewText+SecondPart);
                
                $("#replaceTextareaOfPost").html('<textarea class="js_autosize direct_share_post_chk" rows="1" cols="50" value="" placeholder="'+txt_placeholder+'" name="comment" id="direct_share_post_chk_id">'+FirstPart+NewText+SecondPart+'</textarea>');
                
                $("#direct_share_post_chk_id").putCursorAtEnd();

               

                
            }
        });     
    });

/*$(window).on('shown.bs.modal', function() { 
    tinymce.init({
        selector: "#shareDirectPost",
        theme: 'modern',
        toolbar: false,
        menubar: false,
        branding: false,
        statusbar: false,
        forced_root_block : "",
    });

});
    
$(function() {

    

    $('#pickersharePost_'+{$post_id}).lsxEmojiPicker({
            closeOnSelect: true,
            twemoji: true,
            onSelect: function(emoji){
                tinymce.activeEditor.execCommand('mceInsertContent', false, emoji.value)
            }
    });

});*/


</script>