{if !$user->_logged_in}
	<!--<div class="send-user-message" data-toggle="modal" data-url="posts/compose.php?do=create">
        <div class="add">
            <i class="fa fa-plus-circle"></i>
        </div>
    </div> 
	<button class="send-user-message">Send Message</button>-->
    <body data-hash-tok="{$session_hash['token']}" data-hash-pos="{$session_hash['position']}" class="visitor n_chat">
{else}
		    <body data-hash-tok="{$session_hash['token']}" data-hash-pos="{$session_hash['position']}" data-chat-enabled="{$user->_data['user_chat_enabled']}" class="{if !$system['chat_enabled']}n_chat{/if}{if $system['activation_enabled'] && !$user->_data['user_activated']} n_activated{/if}{if !$system['system_live']} n_live{/if}">

	<!--<button class="send-user-message">Send Message</button>-->
    <div class="send-user-message" data-toggle="modal" data-url="posts/compose.php?do=create">
        <div class="add">
            <i class="fa fa-plus-circle"></i>
        </div>
    </div>
{/if}
    
    <!-- main wrapper -->
    
   {if $user->_logged_in} 
                <div class="sticky_header_second boardcast_header">
                    
                    <ul>
                        
                        <li><a {if $last_dir == 'home'} class="active" {/if}  href="/"><i class="Icon Icon--home Icon--large"></i></a></li>
                        
                        <li><a id="search_toggle" href="javascript:void(0)"><i class="Icon Icon--medium Icon--search"></i></a></li>
                        
                        <li>
                            
                            <a href="/notifications" {if $last_dir == 'notifications' || $last_dir == 'mentions'} class="active" {/if}>
                                <span class="label {if $user->_data['user_live_notifications_counter'] == 0}hidden{/if}">

                                    {$user->_data['user_live_notifications_counter']}
                                    
                                </span>
                                <i class="Icon Icon--notifications Icon--large"></i>
                                
                            </a></li>
                            
                            
                            <li class="dropdown js_live-messages top-menues">
                                <a class="left-nav-channel-title title-messaging"><i class="Icon Icon--dm Icon--large"></i></a>
               <!-- {if $user->_logged_in}
                <div class="dropdown-menu dropdown-widget with-arrow live-chat-massage">
                    <div class="dropdown-widget-header">
                        {__("Messages")}
                        <a class="pull-right flip text-link js_chat-start" href="/messages/new">{__("Send a New Message")}</a>
                    </div>
                    <div class="dropdown-widget-body">
                        <div class="js_scroller">
                            {if count($user->_data['conversations']) > 0}
                                <ul>
                                    {foreach $user->_data['conversations'] as $conversation}
                                    {include file='__feeds_conversation.tpl'}
                                    {/foreach}
                                </ul>
                            {else}
                                <p class="text-center text-muted mt10">
                                    {__("No messages")}
                                </p>
                            {/if}
                        </div>
                    </div>
                    <a class="dropdown-widget-footer" href="/messages">{__("See All")}</a>
                </div>
                {/if}-->
            </li>
            <li><!--a href="/broadcasts" {if $last_dir == 'broadcasts'} class="active" {/if}><i class="Icon Icon--cameraVideo Icon--large"></i></a></li-->
            <div class="">
                <a class="active" href="/broadcasts">
                <i class="Icon Icon--cameraVideo Icon--large mobile-broadcast"></i></a>
            </div>
            </li>
        </ul>
        
        
    </div>
    {/if}
    
  <div class="main-header">
            <div class="container header-container">
                <!-- navbar-container -->
                <div class="navbar-container pull-left">
                    <!--<div class="brand-container mobile-logo">
                
                                <a href="/" class="brand">
                                        <img width="60" src="{SYSTEM_LOGO}" alt="{$system['system_title']}" title="{$system['system_title']}">
                                </a>
                                
                   </div>-->
                   <div class="mobile-menu">
                    <ul class="nav navbar-nav">
                        {if $user->_logged_in}
                        {assign var="dirs" value="/"|explode:$smarty.server.REQUEST_URI}
                            
                            <!-- home -->
                            <li class="top-menues">
                                <a href="/" {if {$dirs[$dirs|@count-1]} == ''}class="active"{/if}>
                                    <i class="Icon Icon--home Icon--large"></i>
                                    <span>{__("Home")}</span>
                                </a>
                            </li>
                            <!-- home -->
                            
                            <!-- friend requests -->
                            {include file='_header.friend_requests.tpl'}
                            <!-- friend requests -->
    
                            <!-- notifications -->
                            {include file='_header.notifications.tpl'}
                            <!-- notifications -->
    
                            <!-- messages -->
                            {include file='_header.messages.tpl'}
                            <!-- messages -->
                            
                            <!-- search -->
                            <li class="visible-xs-block top-menues">
                                <a href="/search">
                                    <i class="fa fa-search fa-lg"></i>
                                </a>
                            </li>
                            
                            <!-- search -->
                       
                        {/if}
                        
                    </ul>
                    </div>
                    
                </div>
                <!-- navbar-container -->
    			 {if $user->_logged_in}
                <div class="brand-container">
                    <!-- brand -->
                    <a href="/" class="brand">
                        {if SYSTEM_LOGO}
                            <img width="60" src="{SYSTEM_LOGO}" alt="{$system['system_title']}" title="{$system['system_title']}">
                        {else}
                            {$system['system_title']}
                        {/if}
                    </a>
                    <!-- brand -->
                </div>
                {else}
                <div class="brand-container user_logout_brand">
                    <!-- brand -->
                    <a href="/" class="brand">
                        {if SYSTEM_LOGO}
                            <img width="60" src="{SYSTEM_LOGO}" alt="{$system['system_title']}" title="{$system['system_title']}">
                        {else}
                            {$system['system_title']}
                        {/if}
                    </a>
                    <!-- brand -->
                    
                </div>
                <div class="inner_link">
                    <ul>
                   
                      <li><a href="/"><i class="Icon Icon--home Icon--large"></i>{__("Home")}</a></li>
                      <!--<li><a href="/static/about">About</a></li>-->
                      <li><a href="/milesguo"><i class="hand_icon"><img src="/content/themes/default/images/hand.svg" alt="hand" width="21"></i>{__("Miles Guo")}</a></li>
                   
                   </ul>
                   </div>
                {/if}

                 
                
               <!-- navbar-collapse -->
                <div class="header-right {if $user->_logged_in} mobile_user_head {/if}">

                   
                    {if $user->_logged_in}
                        <!-- search -->
                        {include file='_header.search.tpl'}
                        <!-- search -->
                    {/if}

                    <!-- navbar-container -->
                    <div class="navbar-container">
                    	<!--<div class="brand-container mobile-logo">
                    
                                    <a href="/" class="brand">
                                            <img width="60" src="{SYSTEM_LOGO}" alt="{$system['system_title']}" title="{$system['system_title']}">
                                    </a>
                                    
                       </div>-->
                       <div class="mobile-menu">
                        <ul class="nav navbar-nav">
                            {if $user->_logged_in}
                                
                                <!-- search -->

                                <!-- user-menu -->
                                <li class="dropdown">
                                    <a class="dropdown-toggle user-menu" data-toggle="dropdown">
                                        <img src="{$user->_data['user_picture']}" alt="">
                                      
                                        
                                    </a>
                                    <ul class="dropdown-menu">
                                    <span class="caret-outer"></span>
                                    <span class="caret-inner"></span>
                                     <li><div class="user_name_id">
                                      
                                     <b class="fullname"><a href="/{$user->_data['user_name']}">{$user->_data['user_firstname']} {$user->_data['user_lastname']}</a></b>
                                     <p class="name"><span class="username u-dir" dir="ltr"><a href="/{$user->_data['user_name']}">@{$user->_data['user_name']}</a></span></p>
                                     </div>
                                     </li>
                                     <li class="divider"></li>
                                        {if $system['packages_enabled'] && !$user->_data['user_subscribed']}
                                            <!--<li>
                                                <a href="/packages"><span class="DashUserDropdown-linkIcon Icon Icon--medium Icon--me"></span>{__("Upgrade to Pro")}</a>
                                            </li>
                                            <li class="divider"></li>-->
                                        {/if}
                                        <li>
                                            <a href="/{$user->_data['user_name']}"><span class="DashUserDropdown-linkIcon Icon Icon--medium Icon--me"></span>{__("Profile")}</a>
                                        </li>
                                        <li>
                                            <a href="/settings/privacy"><span class="DashUserDropdown-linkIcon Icon Icon--medium Icon--promotedStroked"></span>{__("Privacy")}</a>
                                        </li>
                                        <li>
                                            <a href="/settings"><span class="DashUserDropdown-linkIcon Icon Icon--medium Icon--cog"></span>{__("Settings")}</a>
                                        </li>
                                        {if $user->_is_admin}
                                            <li class="divider"></li>
                                            <li>
                                                <a href="/admincp">{__("Admin Panel")}</a>
                                            </li>
                                        {/if}
                                       {* {if ($user->_data['user_group'] < 3 || $system['pages_enabled']) || ($user->_data['user_group'] < 3 || $system['groups_enabled'])}
                                            <li class="divider"></li>
                                            {if $user->_data['user_group'] < 3 || $system['pages_enabled']}
                                                <li>
                                                    <a href="/pages/manage">{__("Manage Pages")}</a>
                                                </li>
                                            {/if}
                                            {if $user->_data['user_group'] < 3 || $system['groups_enabled']}
                                                <li>
                                                    <a href="/groups/manage">{__("Manage Groups")}</a>
                                                </li>
                                            {/if}
                                        {/if} *}
                                        <li class="divider"></li>
                                        <li>
                                            <a href="/signout">{__("Log Out")}</a>
                                        </li>
                                    </ul>
                                </li>
                                
                                <li class="visible-xs-block">
                                    <a href="#" data-toggle="offcanvas" data-target=".sidebar-nav">
                                        <i class="fa fa-bars fa-lg"></i>
                                    </a>
                                </li>
                                <!-- user-menu -->
                            {else}
                                <!--<li>
                                    <a href="/">
                                        <span>{__("Join")}</span>
                                    </a>
                                </li>-->
                            {/if}
                        </ul>
                        {if !$user->_logged_in}
                        <span class="text-link dev" data-toggle="modal" data-url="#translator">
                        
						 {$system['language']['title']}</span>
						 <span class="contact-icon">
                            <a href="/broadcasts">
                                 <span class="Icon icon_custom Icon--cameraVideo Icon--small"></span> {__("GuoTV")}
                            </a>
                        </span>
                        <a href="/broadcasts" {if $last_dir == 'broadcasts'} class="active" {/if}><span class="broadcasts_icon Icon Icon--cameraVideo Icon--small"></span></a>
                        {/if}
                        {if $user->_logged_in}
                          <span class="contact-icon watch_broadcasts">
                            <div class="">
                                 <a href="/broadcasts">
                                 <span class="Icon icon_custom Icon--cameraVideo Icon--small"></span>{__("GuoTV")}
                              </a>
                            </div>
                        </span>
                         {/if}
						</div>
                        
                    </div>
                    <!-- navbar-container -->
                </div>
                <!-- navbar-collapse -->

            </div>
        </div>
        
        <!-- ads -->
        {include file='_ads.tpl' _ads=$ads_master['header'] _master=true}
        <!-- ads -->