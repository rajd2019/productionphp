<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:12:15
  from "/var/app/current/content/themes/default/templates/__feeds_photo.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c58726fc22368_02312661',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9f3e2882062b965ba11215c873c5e16a8d8a9a0a' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/__feeds_photo.tpl',
      1 => 1536745036,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c58726fc22368_02312661 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_photo_thumb')) require_once '/var/app/current/vendor/smarty/smarty/libs/plugins/function.photo_thumb.php';
?>
<div class="<?php if ($_smarty_tpl->tpl_vars['_small']->value) {?>col-xs-6 col-sm-4<?php } else { ?>col-xs-6 col-sm-2<?php }?>">
    <a class="pg_photo <?php if (!$_smarty_tpl->tpl_vars['_small']->value) {?>large<?php }?> js_lightbox" href="/photos/<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" data-image="" data-context="<?php echo $_smarty_tpl->tpl_vars['_context']->value;?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo smarty_function_photo_thumb(array('param1'=>$_smarty_tpl->tpl_vars['photo']->value['source']),$_smarty_tpl);?>
);">
    	<?php if ($_smarty_tpl->tpl_vars['_manage']->value || $_smarty_tpl->tpl_vars['photo']->value['manage']) {?>
	    	<div class="pg_photo-btn">
	            <button type="button" class="close js_delete-photo" data-id="<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" data-toggle="tooltip" data-placement="top" title='<?php echo __("Delete");?>
'>
	                <span aria-hidden="true">&times;</span>
	            </button>
	        </div>
        <?php }?>
    </a>
</div><?php }
}
