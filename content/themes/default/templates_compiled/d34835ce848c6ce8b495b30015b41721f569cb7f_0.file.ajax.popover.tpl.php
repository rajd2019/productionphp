<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:16:29
  from "/var/app/current/content/themes/default/templates/ajax.popover.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c58736db93fc1_07252415',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd34835ce848c6ce8b495b30015b41721f569cb7f' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/ajax.popover.tpl',
      1 => 1536745034,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c58736db93fc1_07252415 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['type']->value == "user") {?>

    <!-- user popover -->

    <div class="user-popover-content user-hover-detail <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_id'] == $_smarty_tpl->tpl_vars['profile']->value['user_id']) {?>author-profile<?php }?>">

        <div class="user-card">

            <div class="user-card-cover" <?php if ($_smarty_tpl->tpl_vars['profile']->value['user_cover']) {?>style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_cover'];?>
');"<?php }?>>

            </div>

            <div class="user-card-avatar">

                <img src="<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['profile']->value['user_lastname'];?>
">

            </div>

            



        </div>

        <div class="user-card-meta">

            <!-- mutual friends -->

            

            <!-- mutual friends -->

            

            <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_id'] != $_smarty_tpl->tpl_vars['profile']->value['user_id']) {?>

                <div class="btn-group">

                    <?php if ($_smarty_tpl->tpl_vars['profile']->value['i_follow']) {?>

                        <button type="button" class="btn btn-default js_unfollow" data-uid="<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_id'];?>
">

                            <i class="fa fa-check"></i>

                            <?php echo __("Following");?>


                        </button>

                    <?php } else { ?>

                        <button type="button" class="btn btn-default js_follow" data-uid="<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_id'];?>
">

                            <i class="fa fa-rss"></i>

                            <?php echo __("Follow");?>


                        </button>

                    <?php }?>

                </div>

            <?php }?>

            <div class="user-card-info">

                    <a class="name" href="/<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_name'];?>
">

                    <span class="first-name-custom"><?php echo $_smarty_tpl->tpl_vars['profile']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['profile']->value['user_lastname'];?>
</span>

                    <span class="second-name-custom">@<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_name'];?>
</span>

                    </a>

                    <?php if ($_smarty_tpl->tpl_vars['profile']->value['user_verified']) {?>

                        <i data-toggle="tooltip" data-placement="top" title='<?php echo __("Verified User");?>
' class="fa fa-check-circle fa-fw verified-badge"></i>

                    <?php }?>

            </div>

        </div>



        <div class="following side_profile">

            <ul>

            <li><a href="/<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_name'];?>
"><?php echo __("Posts");?>
<span><?php if ($_smarty_tpl->tpl_vars['profile']->value['posts_count']) {
echo $_smarty_tpl->tpl_vars['profile']->value['posts_count'];
} else { ?>0<?php }?></span></a></li>

            <li><a href="/followings.php?username=<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_name'];?>
"><?php echo __("Followings");?>
<span>

            <?php if ($_smarty_tpl->tpl_vars['profile']->value['followings_count']) {
echo $_smarty_tpl->tpl_vars['profile']->value['followings_count'];
} else { ?>0<?php }?></span></a></li>

            <li><a href="/followers.php?username=<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_name'];?>
"><?php echo __("Followers");?>
<span><?php if ($_smarty_tpl->tpl_vars['user']->value->_data['followers_count']) {
echo $_smarty_tpl->tpl_vars['profile']->value['followers_count'];
} else { ?>0<?php }?></span></a></li>

            </ul>

        </div>

        

        <!--<div class="footer">

            <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_id'] != $_smarty_tpl->tpl_vars['profile']->value['user_id']) {?>

                <?php if ($_smarty_tpl->tpl_vars['profile']->value['we_friends']) {?>

                    <div class="btn btn-default btn-delete js_friend-remove" data-uid="<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_id'];?>
">

                        <i class="fa fa-check fa-fw"></i> <?php echo __("Friends");?>


                    </div>

                <?php } elseif ($_smarty_tpl->tpl_vars['profile']->value['he_request']) {?>

                    <div class="btn btn-primary js_friend-accept" data-uid="<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_id'];?>
"><?php echo __("Confirm");?>
</div>

                    <div class="btn btn-default js_friend-decline" data-uid="<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_id'];?>
"><?php echo __("Delete Request");?>
</div>

                <?php } elseif ($_smarty_tpl->tpl_vars['profile']->value['i_request']) {?>

                    <div class="btn btn-default btn-sm js_friend-cancel" data-uid="<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_id'];?>
">

                        <i class="fa fa-user-plus"></i> <?php echo __("Friend Request Sent");?>


                    </div>

                <?php } else { ?>

                    <div class="btn btn-success btn-sm js_friend-add" data-uid="<?php echo $_smarty_tpl->tpl_vars['profile']->value['user_id'];?>
">

                        <i class="fa fa-user-plus"></i> <?php echo __("Add Friend");?>


                    </div>

                <?php }?>



                

            <?php } else { ?>

                <a href="/settings/profile" class="btn btn-default">

                    <i class="fa fa-pencil"></i> <?php echo __("Update Info");?>


                </a>

            <?php }?>

        </div>

    </div>-->

    <!-- user popover -->

<?php } else { ?>

    <!-- page popover -->

    <div class="user-popover-content">

        <div class="user-card">

            <div class="user-card-cover" <?php if ($_smarty_tpl->tpl_vars['profile']->value['page_cover']) {?>style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['profile']->value['page_cover'];?>
');"<?php }?>></div>
            
           

            <div class="user-card-avatar">

                <img class="img-responsive" src="<?php echo $_smarty_tpl->tpl_vars['profile']->value['page_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['profile']->value['page_title'];?>
">

            </div>

            <div class="user-card-info">

                <a class="name" href="/pages/<?php echo $_smarty_tpl->tpl_vars['profile']->value['page_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['profile']->value['page_title'];?>
</a>

                <?php if ($_smarty_tpl->tpl_vars['profile']->value['page_verified']) {?>

                    <i data-toggle="tooltip" data-placement="top" title='<?php echo __("Verified User");?>
' class="fa fa-check-circle fa-fw verified-badge"></i>

                <?php }?>

                <div class="info"><?php echo $_smarty_tpl->tpl_vars['profile']->value['page_likes'];?>
 <?php echo __("Likes");?>
</div>

            </div>

        </div>

        <div class="footer">

            <?php if ($_smarty_tpl->tpl_vars['profile']->value['i_like']) {?>

                <button type="button" class="btn btn-default js_unlike-page" data-id="<?php echo $_smarty_tpl->tpl_vars['profile']->value['page_id'];?>
">

                    <i class="fa fa-thumbs-o-up"></i>

                    <?php echo __("Unlike");?>


                </button>

            <?php } else { ?>

                <button type="button" class="btn btn-primary js_like-page" data-id="<?php echo $_smarty_tpl->tpl_vars['profile']->value['page_id'];?>
">

                    <i class="fa fa-thumbs-o-up"></i>

                    <?php echo __("Like");?>


                </button>

            <?php }?>

        </div>

    </div>

    <!-- page popover -->

<?php }
}
}
