<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:57:29
  from "/var/app/current/content/themes/default/templates/video_post.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c587d09d90ef7_37240147',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '203ea202365e5aff566bd3fab2265cae13c2a777' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/video_post.tpl',
      1 => 1536745010,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:_video_post.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_5c587d09d90ef7_37240147 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- page content -->
<div class="container mt20 <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>offcanvas<?php }?>">
  <div class="row">

        <div class="col-xs-12  <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>offcanvas-mainbar <?php } else { ?>video_post_logout<?php }?>">
          
          <div class="row ">
            <!-- left panel -->
            <div class="col-sm-12 col-md-12">
            <div class="col-sm-12 col-md-8 video_main">
                <?php $_smarty_tpl->_subTemplateRender('file:_video_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('standalone'=>true), 0, false);
?>

            </div>
        <!-- left panel -->

        <!-- right panel -->
        
            <div class="col-sm-12 col-md-4 chat-live">
                       <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?> 
                       
                            <?php echo '<script'; ?>
 src="https://sample.sendbird.com/livechat/SendBird.min.js"><?php echo '</script'; ?>
>
                            <?php echo '<script'; ?>
 src="https://sample.sendbird.com/livechat/build/liveChat.SendBird.js"><?php echo '</script'; ?>
>
                           <div class="content-chat" id="sb_chat"></div>
                           <?php echo '<script'; ?>
>
                              var appId = 'E2401D42-320A-48DA-8CD8-6DE8EBDC8644';
                              liveChat.start(appId, document.getElementById('channel_id').value);
                              jQuery(".chat-board").css("display","none");
                              jQuery(".chat-board").find('.top').text('');
                              jQuery(".chat-board").find('.top').text('Live Chat');
                              var timestamp = Math.round((new Date()).getTime() / 1000);
                              jQuery(".user-id").find('input').val((document.getElementById('user_firstname').value)+"_"+timestamp);
                              //jQuery(".user-id").find('input').val(document.getElementById('user_firstname').value);
                              jQuery(".nickname").find('input').val(document.getElementById('user_firstname').value);
                              jQuery(".login-board").find('div.btn').removeClass("disabled");
                              // jQuery(".login-board").find('div.btn').trigger("click");
                              setTimeout(function(){ 
                                  jQuery(".login-board").find('div.btn').trigger("click");
                                  jQuery(".chat-board").css("display","block");
                              }, 2500);
                              
                              setInterval(function(){
                                  jQuery.post("<?php echo SYS_URL_API;?>
/tokbox/getViewCount.php", { 'post_id':document.getElementById('post_id').value }, function(res){
                                           if(res.success == true) {
                                               jQuery(".update_count").html();
                                               jQuery(".update_count").html(res.data);
                                           } 
                                    });
                              },60000);
                              
                           <?php echo '</script'; ?>
>
                       <?php }?>
                    </div>
                </div>
        <!-- right panel -->
          </div>
        </div>

  </div>
</div>
<!-- page content -->

<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
