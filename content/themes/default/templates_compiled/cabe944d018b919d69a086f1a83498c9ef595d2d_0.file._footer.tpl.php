<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:12:15
  from "/var/app/current/content/themes/default/templates/_footer.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c58726fc74bb2_67120112',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cabe944d018b919d69a086f1a83498c9ef595d2d' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/_footer.tpl',
      1 => 1539082237,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_ads.tpl' => 1,
    'file:_js_files.tpl' => 1,
    'file:_js_templates.tpl' => 1,
    'file:theme_color.tpl' => 1,
  ),
),false)) {
function content_5c58726fc74bb2_67120112 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- ads -->
<?php $_smarty_tpl->_subTemplateRender('file:_ads.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_ads'=>$_smarty_tpl->tpl_vars['ads_master']->value['footer'],'_master'=>true), 0, false);
?>

<!-- ads -->

<!-- footer -->
<div class="container">

	<div class="row footer">
		<div class="col-lg-6 col-md-6 col-sm-6">
			&copy; <?php echo date('Y');?>
 <?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 · <span class="text-link" data-toggle="modal" data-url="#translator"></span>&nbsp;<?php echo $_smarty_tpl->tpl_vars['system']->value['language']['title'];?>
</span>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 links">
			<?php if (count($_smarty_tpl->tpl_vars['static_pages']->value) > 0) {?>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['static_pages']->value, 'static_page', true);
$_smarty_tpl->tpl_vars['static_page']->iteration = 0;
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['static_page']->value) {
$_smarty_tpl->tpl_vars['static_page']->iteration++;
$_smarty_tpl->tpl_vars['static_page']->last = $_smarty_tpl->tpl_vars['static_page']->iteration == $_smarty_tpl->tpl_vars['static_page']->total;
$__foreach_static_page_6_saved = $_smarty_tpl->tpl_vars['static_page'];
?>
					<a href="/static/<?php echo $_smarty_tpl->tpl_vars['static_page']->value['page_url'];?>
">
						<?php echo __(((string)$_smarty_tpl->tpl_vars['static_page']->value['page_title']));?>

					</a><?php if (!$_smarty_tpl->tpl_vars['static_page']->last) {?> · <?php }?>
				<?php
$_smarty_tpl->tpl_vars['static_page'] = $__foreach_static_page_6_saved;
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['system']->value['contact_enabled']) {?>
				 ·
				<a href="/contacts">
					<?php echo __("Contacts");?>

				</a>
			<?php }?>
			·
			<a href="/message_to_miles">
					<?php echo __("Message to Guo");?>

				</a>
				<?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_id'] == 296) {?>
				<a data-toggle="modal" data-target="#do_broad" href="javascript:void(0)">Do-Broadcast</a>
				<?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['system']->value['market_enabled']) {?>

            <?php }?>
		</div>
	</div>
</div>
<!-- footer -->

</div>
<!-- main wrapper -->
    <div  id="do_broad" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
          <iframe style="border:none;" src="/do_broadcast" height="280px;" width="100%"></iframe>

      </div>
    </div>

  </div>
</div>
<!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] -->
<?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
<link rel="stylesheet" href="/includes/assets/css/font-awesome/css/font-awesome.min.css">
<?php }?>
<link rel="stylesheet" href="/includes/assets/css/twemoji-awesome/twemoji-awesome.min.css">
<link rel="stylesheet" href="/includes/assets/css/flag-icon/css/flag-icon.min.css">
<!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] -->

<?php echo '<script'; ?>
>
$("document").ready(function(){	
   // alert('h22');
});
<?php echo '</script'; ?>
>

<!-- JS Files -->
<?php $_smarty_tpl->_subTemplateRender('file:_js_files.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- JS Files -->

<!-- JS Templates -->
<?php $_smarty_tpl->_subTemplateRender('file:_js_templates.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- JS Templates -->

<?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
	<!-- Notification -->
	 <audio id="notification_sound">
		<source src="/includes/assets/sounds/notification.mp3" type="audio/mpeg">
	</audio>
	<!-- Notification -->
	<!-- Chat -->
	<audio id="chat_sound">
		<source src="/includes/assets/sounds/chat.mp3" type="audio/mpeg">
	</audio>
	<!-- Chat -->
	<!-- Call -->
	<audio id="call_sound">
		<source src="/includes/assets/sounds/call.mp3" type="audio/mpeg">
	</audio>
	<!-- Call -->
	<!-- Video -->
	<audio id="video_sound">
		<source src="/includes/assets/sounds/video.mp3" type="audio/mpeg">
	</audio>
	<!-- Video -->
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '') {?> 
	<?php $_smarty_tpl->_subTemplateRender('file:theme_color.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
echo '<script'; ?>
 type="text/javascript">
    $('.cross_dismiss').click(function () {
        location.reload('/notifications');
    });
<?php echo '</script'; ?>
>
</body>
</html><?php }
}
