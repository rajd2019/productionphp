<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:12:16
  from "/var/app/current/content/themes/default/templates/_header.search.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c5872708d5533_18478667',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a9c098fe9444f4e00f28c26cf0e4eb67e5a64ee8' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/_header.search.tpl',
      1 => 1536745028,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ajax.search.tpl' => 1,
  ),
),false)) {
function content_5c5872708d5533_18478667 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style>
@media (min-width: 768px){
.main-header .navbar-form .form-control {
    height: 37px;
}
.nav-search {
    top: 20px;
}

.main-header .navbar-form {
    width: 220px;
    height: 46px;
    padding: 5px 0 0;
}
}
</style>
<form class="navbar-form pull-left flip hidden-xs top-menues">

    <input id="search-input" type="text" class="form-control" placeholder='<?php echo __("Search for everything");?>
' required="required" autocomplete="off">

    <button type="submit" class="Icon Icon--medium Icon--search nav-search" tabindex="-1"></button>
	<input id="search-input-value" type="hidden" value="">

    <div id="search-results" class="dropdown-menu dropdown-widget dropdown-search js_dropdown-keepopen">

        <div class="dropdown-widget-header">

            <?php echo __("Search Results");?>


        </div>

        <div class="dropdown-widget-body">

            <div class="loader loader_small ptb10"></div>

        </div>

        <a class="dropdown-widget-footer" id="search-results-all" style="cursor:pointer;"><?php echo __("See All Results");?>
</a>

    </div>

    <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in && count($_smarty_tpl->tpl_vars['user']->value->_data['search_log']) > 0) {?>

        <div id="search-history" class="dropdown-menu dropdown-widget dropdown-search js_dropdown-keepopen">

            <div class="dropdown-widget-header">

                <span class="text-link pull-right flip js_clear-searches">

                    <?php echo __("Clear");?>


                </span>

                <i class="fa fa-clock-o"></i> <?php echo __("Recent Searches");?>


            </div>

            <div class="dropdown-widget-body">

                <?php $_smarty_tpl->_subTemplateRender('file:ajax.search.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('results'=>$_smarty_tpl->tpl_vars['user']->value->_data['search_log']), 0, false);
?>


            </div>

            <a class="dropdown-widget-footer" id="search-results-all" href="/search/"><?php echo __("Advanced Search");?>
</a>

        </div>

    <?php }?>

</form><?php }
}
