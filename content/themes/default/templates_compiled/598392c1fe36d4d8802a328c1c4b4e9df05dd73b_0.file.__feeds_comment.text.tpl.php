<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:12:15
  from "/var/app/current/content/themes/default/templates/__feeds_comment.text.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c58726fe139e6_93032530',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '598392c1fe36d4d8802a328c1c4b4e9df05dd73b' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/__feeds_comment.text.tpl',
      1 => 1536745022,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c58726fe139e6_93032530 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="comment-replace">
    <div class="comment-text js_readmore" dir="auto"><?php echo htmlspecialchars_decode($_smarty_tpl->tpl_vars['_comment']->value['text'], ENT_QUOTES);?>
</div>
    <div class="comment-text-plain hidden"><?php echo $_smarty_tpl->tpl_vars['_comment']->value['text_plain'];?>
</div>
    <div class="comment-text-id hidden"><?php echo $_smarty_tpl->tpl_vars['_comment']->value['comment_id'];?>
</div>
    <?php if ($_smarty_tpl->tpl_vars['_comment']->value['image'] != '') {?>
        <span class="text-link js_lightbox-nodata" data-image="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_comment']->value['image'];?>
">
            <img alt="" class="img-responsive" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_comment']->value['image'];?>
">
        </span>
    <?php }?>
</div>
<?php }
}
