<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:41:28
  from "/var/app/current/content/themes/default/templates/settings.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c5879484d6192_93376677',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a83f21e824e22a86a189b7e2d8ec64701688eaf2' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/settings.tpl',
      1 => 1536745008,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:__custom_fields.tpl' => 5,
    'file:__feeds_user.tpl' => 2,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_5c5879484d6192_93376677 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_capitalize')) require_once '/var/app/current/vendor/smarty/smarty/libs/plugins/modifier.capitalize.php';
if (!is_callable('smarty_modifier_date_format')) require_once '/var/app/current/vendor/smarty/smarty/libs/plugins/modifier.date_format.php';
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>



<style>
.bio_emoji .lsx-emojipicker-container {
    top: auto !important;
    bottom: 0px;
    right: -13px;
}
.icon-camera{
display:none;
}
  @media(max-width: 767px)
  {
	  .panel-default {
		text-align: left;
	  }
	.offcanvas-sidebar{
	width:100%;
	 z-index: unset;
	}
  }
  .bio_emoji{
    bottom: 10px;
    right: 22px;
  }
  .bio_emoji i.fa.fa-smile-o.fa-lg {
    font-size: 1.6em;
}
#bio_info_id{
  /*height:auto;
  min-height:90px;*/
}
.pac-container:after {
  background-image: none !important;
  height: 0px;
}
.pac-icon-marker{
  display: none;
}

option.Icon {
    font-family: "edgeicons";
}
button.btn.dropdown-toggle.btn-default[data-id="privacy_birthyear"] {
    width: 57px;
    border: 0 !important;
    box-shadow: none;
    margin: 0 0 0 -21px;
}
button.btn.dropdown-toggle.btn-default[data-id="privacy_birthdate"] {
    width: 57px;
    border: 0 !important;
    box-shadow: none;
    margin: 0 0 0 -21px;
}
.privacy_birthyr_cls .dropdown-menu.open {
    width: 206px;
	z-index:999999;
}
.privacy_birthdate_cls .dropdown-menu.open {
    width: 206px;
}
.btn-group.bootstrap-select.form-control.privacy_birthyr_cls {
    box-shadow: none;
}
.btn-group.bootstrap-select.form-control.privacy_birthdate_cls {
    box-shadow: none;
}
button.btn.dropdown-toggle.btn-default[data-id="privacy_birthdate"]:hover, button.btn.dropdown-toggle.btn-default[data-id="privacy_birthdate"]:focus {
    outline: 0 !important;
    background: transparent;
}
button.btn.dropdown-toggle.btn-default[data-id="privacy_birthyr"]:hover, button.btn.dropdown-toggle.btn-default[data-id="privacy_birthyr"]:focus {
    outline: 0 !important;
    background: transparent;
}
.privacy_birthyr_cls .dropdown-menu.open {
    width: 201px;
    left: -16px;
}
.privacy_birthyr_cls .dropdown-menu.open {
    width: 201px;
    left: -16px;
}

.red.color-box ~ i        { background:#ff691f;color:#ff691f}
.orange.color-box ~ i     { background:#fab81e;color:#fab81e}
.light-green.color-box ~ i{ background:#7fdbb6;color:#7fdbb6}
.green.color-box ~ i      { background:#19cf86;color:#19cf86}
.light-blue.color-box ~ i { background:#91d2fa;color:#91d2fa}
.blue.color-box ~ i       { background:#1b95e0;color:#1b95e0}
.grey.color-box ~ i       { background:#abb8c2;color:#abb8c2}
.amaranth.color-box ~ i   { background:#e81c4f;color:#e81c4f}
.pink.color-box ~ i       { background:#f58ea8;color:#f58ea8}
.purple.color-box ~ i     { background:#981ceb;color:#981ceb}
.color-dropdown li.col-sm-2 {
	text-align: center;
}
.color-dropdown input.color-box {
	position: absolute;
	left: 0;
	right: 0;
	top: 0;
	width: 100%;
	height: 100%;
	z-index:999999999;
	opacity:0;
}
.color-dropdown input.color-box ~ i {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
}	
.color-dropdown li {
    text-align: center;
    height: 28px;
	position:relative;
	overflow:hidden;
	padding:0 8px;
	border-radius: 3px;
	margin-bottom: 9px;
}
.color-dropdown input.color-box ~ i:before {
    color: #fff;
    z-index: 99999;
    position: absolute;
    left: calc(50% - 6px);
    top: calc(50% + -5px);
	opacity:0;
}	
.color-dropdown input.color-box:checked ~ i:before{
	opacity:1;
}

.color-dropdown input#rdOthrClr.color-box:checked ~ input[type="text"] {
	display: block;
    margin-left: 23px;
    width: calc(100% - 23px);
    height: 100%;
    padding: 4px 2px;
    border-radius: 0 4px 4px 0;
    border: solid 1px #ccc;
}
.color-dropdown input#rdOthrClr.color-box ~ input[type="text"] {
	display:none;
	
}
.color-dropdown input#rdOthrClr.color-box:checked ~ i, .color-dropdown input#rdOthrClr.color-box:checked{
	width: 24px;
}
.color-dropdown li.col-sm-4 {
    width: calc(30% + 8px);
}
.color-dropdown input#rdOthrClr.color-box ~ i {
    width: calc(50% - 4px);
	border-radius:3px;
}
.color-dropdown input#rdOthrClr.color-box:checked ~ i {
	border-radius: 0;
}
.color-dropdown input#rdOthrClr.color-box {
    width: 50%;
}
.color-dropdown input#rdOthrClr.color-box ~ i:before{
	opacity:1;
}
.color-dropdown li span {
    position: relative;
    display: block;
    float: left;
    width: 100%;
    height: 100%;
    border-radius: 4px;
    overflow: hidden;
    /* margin-bottom: 82px; */
}
ul.color-dropdown {
    padding: 11px 3px 3px;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    border: 1px solid #ccc;
}
            @media(max-width:1024px){
                .inner_link {
                    padding: 0px 0 0 0px;
                    float: left;
                }
            }
            @media(max-width:767px){
                  .mobile-menu .navbar-nav .open .dropdown-menu {
                    position: absolute !important;
                    float: left  !important;
                    width: 100%  !important;
                    margin-top: 0  !important;
                    background-color: #fff  !important ;
                    border: 1px solid #ccc  !important;
                    border: 1px solid rgba(0,0,0,.15)  !important;
                    border-top: 0  !important ;
                    -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175)  !important ;
                    box-shadow: 0 6px 12px rgba(0,0,0,.175) !important ;
                }
                .main-header .header-right .nav > li > a {
                    padding: 0px 4px 0 9px;

               }
               .contact-icon.watch_broadcasts{

                       margin: 0px 0px 0px 6px;
               }
               .profile-tabs-wrapper ul {
                   display: table;
                   width: auto;
                   margin: 0 auto;
               }
                .main-header .user-menu>img, .post-avatar-picture {
                  height: 35px;
                  width: 35px;
                }
                /*.sticky_header_second{
                    top:52px;
                }  */
            }

            @media(max-width:480px){
                .contact-icon.watch_broadcasts_logout {
                    margin: -8px 0px 0px 5px;
                    display: inline-block;
                    float: right;
                }
                .contact-icon a {
                    padding: 10px 12px 10px 26px;
                    float: left;
                    font-size: 10px;
                    background-size: 13px;
                }
                .profile-tabs-wrapper > ul > li > a {
                    padding: 12px 3px;
                }

            }
</style>


<!-- page content -->

<div class="container mt20 offcanvas">

    <div class="row">



        <!-- left panel -->

        <div class="col-sm-3 offcanvas-sidebar">

        <div class="site_bar_new">
            <div class="profile-bg-thumb">
            <a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
">
			   <?php ob_start();
echo $_smarty_tpl->tpl_vars['user']->value->_data['user_cover'];
$_prefixVariable1=ob_get_clean();
if ($_prefixVariable1 != '') {?>
					<img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_cover'];?>
" >
				<?php }?>

            </a>
            </div>
            <div class="side_profile">
            <div class="pro_thumb">
                <a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_picture'];?>
">
                </a>
            </div>

            <div class="admin_detail">
            <h3><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_firstname'];?>
</a></h3>
            <span><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
">@<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
</a></span>
            </div>
            <ul>
            <li><a href="/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><?php echo __("Posts");?>
<span><?php if ($_smarty_tpl->tpl_vars['user']->value->_data['posts_count']) {
echo $_smarty_tpl->tpl_vars['user']->value->_data['posts_count'];
} else { ?>0<?php }?></span></a></li>
            <li><a href="/followings.php?username=<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><?php echo __("Followings");?>
<span><?php if ($_smarty_tpl->tpl_vars['user']->value->_data['followings_count']) {
echo $_smarty_tpl->tpl_vars['user']->value->_data['followings_count'];
} else { ?>0<?php }?></span></a></li>
            <li><a href="/followers.php?username=<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><?php echo __("Followers");?>
<span><?php if ($_smarty_tpl->tpl_vars['user']->value->_data['followers_count']) {
echo $_smarty_tpl->tpl_vars['user']->value->_data['followers_count'];
} else { ?>0<?php }?></span></a></li>
            </ul>
            </div>

		</div>

            <div class="panel panel-default">

                <div class="panel-body with-nav">

                    <ul class="side-nav">

                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>class="active"<?php }?>>

                            <a href="/settings"><i class="fa fa-cog fa-fw fa-lg pr10"></i> <?php echo __("Account Settings");?>
</a>

                        </li>

                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "profile") {?>class="active"<?php }?>>

                            <a href="/settings/profile"><i class="fa fa-user fa-fw fa-lg pr10"></i> <?php echo __("Edit Profile");?>
</a>

                        </li>

                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "privacy") {?>class="active"<?php }?>>

                            <a href="/settings/privacy"><i class="fa fa-lock fa-fw fa-lg pr10"></i> <?php echo __("Privacy Settings");?>
</a>

                        </li>

                        <!--TODO: Enable IP logging in future
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "security") {?>class="active"<?php }?>>

                            <a href="/settings/security"><i class="fa fa-shield fa-fw fa-lg pr10"></i> <?php echo __("Security Settings");?>
</a>

                        </li>-->

                        <?php if ($_smarty_tpl->tpl_vars['system']->value['email_notifications']) {?>

                            <?php if ($_smarty_tpl->tpl_vars['system']->value['email_post_likes'] || $_smarty_tpl->tpl_vars['system']->value['email_post_comments'] || $_smarty_tpl->tpl_vars['system']->value['email_post_shares'] || $_smarty_tpl->tpl_vars['system']->value['email_wall_posts'] || $_smarty_tpl->tpl_vars['system']->value['email_mentions'] || $_smarty_tpl->tpl_vars['system']->value['email_profile_visits'] || $_smarty_tpl->tpl_vars['system']->value['email_friend_requests']) {?>

                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "notifications") {?>class="active"<?php }?>>

                                    <a href="/settings/notifications"><i class="fa fa-envelope-open-o fa-fw fa-lg pr10"></i> <?php echo __("Email Notifications");?>
</a>

                                </li>

                            <?php }?>

                        <?php }?>

                        <?php if ($_smarty_tpl->tpl_vars['system']->value['social_login_enabled']) {?>

                            <?php if ($_smarty_tpl->tpl_vars['system']->value['facebook_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['twitter_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['google_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['instagram_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['linkedin_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['vkontakte_login_enabled']) {?>

                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "linked") {?>class="active"<?php }?>>

                                    <a href="/settings/linked"><i class="fa fa-share-alt fa-fw fa-lg pr10"></i> <?php echo __("Linked Accounts");?>
</a>

                                </li>

                            <?php }?>

                        <?php }?>


                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_group'] == "2") {?>
                        <!-- Reports -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports") {?>class="active"<?php }?>>
                            <a href="/settings/reports" class="no-border">
                                <i class="fa fa-exclamation-triangle fa-fw fa-lg pr10"></i><?php echo __("Reports");?>

                            </a>
                        </li>
                        <!-- Reports -->
                        <?php }?>


                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "blocking") {?>class="active"<?php }?>>

                            <a href="/settings/blocking"><i class="fa fa-minus-circle fa-fw fa-lg pr10"></i> <?php echo __("Blocking");?>
</a>

                        </li>

                     

                        <?php if ($_smarty_tpl->tpl_vars['system']->value['affiliates_enabled']) {?>

                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "affiliates") {?>class="active"<?php }?>>

                                <a href="/settings/affiliates"><i class="fa fa-exchange fa-fw fa-lg pr10"></i> <?php echo __("Affiliates");?>
</a>

                            </li>

                        <?php }?>

                       

                        <?php if ($_smarty_tpl->tpl_vars['system']->value['delete_accounts_enabled']) {?>

                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "delete") {?>class="active"<?php }?>>

                                <a href="/settings/delete"><i class="fa fa-trash fa-fw fa-lg pr10"></i> <?php echo __("Delete Account");?>
</a>

                            </li>

                        <?php }?>

                    </ul>

                </div>

            </div>

        </div>

        <!-- left panel -->



        <!-- right panel -->

        <div class="col-sm-9 offcanvas-mainbar">

            <div class="panel panel-default">



                <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>

                    <div class="panel-heading with-icon with-nav">

                        <!-- panel title -->

                        <div class="mb20">

                            <i class="fa fa-cog pr5 panel-icon"></i>

                            <strong><?php echo __("Account Settings");?>
</strong>

                        </div>

                        <!-- panel title -->



                        <!-- panel nav -->

                        <ul class="nav nav-tabs">

                            <li class="active">

                                <a href="#username" data-toggle="tab">

                                    <i class="fa fa-cog fa-fw mr5"></i><strong class="pr5"><?php echo __("Username");?>
</strong>

                                </a>

                            </li>

                            <li>

                                <a href="#email" data-toggle="tab">

                                    <i class="fa fa-envelope-o fa-fw mr5"></i><strong class="pr5"><?php echo __("Email");?>
</strong>

                                </a>

                            </li>

                            <li>

                                <a href="#password" data-toggle="tab">

                                    <i class="fa fa-key fa-fw mr5"></i><strong class="pr5"><?php echo __("Password");?>
</strong>

                                </a>

                            </li>


                            <!-- <li>

                        <a href="#verification" data-toggle="tab">

                            <i class="fa fa-user-secret fa-fw mr5"></i><strong class="pr5"><?php echo __("Security");?>
</strong>

                        </a>

                    </li> -->

                        </ul>

                        <!-- panel nav -->

                    </div>

                    <div class="panel-body tab-content">

                        <!-- username tab -->

                        <div class="tab-pane active" id="username">

                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=username">

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Username");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <div class="input-group">

                                            <span class="input-group-addon">/</span>

                                            <input type="text" class="form-control" name="username" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
">

                                        </div>

                                    </div>

                                </div>



                                <div class="form-group">

                                    <div class="col-sm-9 col-sm-offset-3">

                                        <button type="submit" class="btn btn-primary" id="save_changes"><?php echo __("Save Changes");?>
</button>

                                    </div>

                                </div>



                                <!-- success -->

                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>

                                <!-- success -->



                                <!-- error -->

                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                                <!-- error -->

                            </form>

                        </div>

                        <!-- username tab -->



                        <!-- email tab -->

                        <div class="tab-pane" id="email">

                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=email">

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Email Address");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <input type="email" class="form-control" name="email" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_email'];?>
">

                                    </div>

                                </div>



                                <div class="form-group">

                                    <div class="col-sm-9 col-sm-offset-3">

                                        <button type="submit" class="btn btn-primary" id="save_changes"><?php echo __("Save Changes");?>
</button>

                                    </div>

                                </div>



                                <!-- success -->

                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>

                                <!-- success -->



                                <!-- error -->

                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                                <!-- error -->

                            </form>

                        </div>

                        <!-- email tab -->



                        <!-- password tab -->

                        <div class="tab-pane" id="password">

                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=password">

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Current");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <input type="password" class="form-control" name="current">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("New");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <input type="password" class="form-control" name="new">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Re-type new");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <input type="password" class="form-control" name="confirm">

                                    </div>

                                </div>



                                <div class="form-group">

                                    <div class="col-sm-9 col-sm-offset-3">

                                        <button type="submit" class="btn btn-primary" id="save_changes"><?php echo __("Save Changes");?>
</button>

                                    </div>

                                </div>



                                <!-- success -->

                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>

                                <!-- success -->



                                <!-- error -->

                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                                <!-- error -->

                            </form>

                        </div>

                        <!-- password tab -->

                        <!-- security tab -->

                        <div class="tab-pane" id="verification">

                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=verification">

                                <div class="form-group">
									<div class="col-sm-1"></div> <div class="col-sm-11"><p><?php echo __("When you choose to enable two-factor login verification, you will be asked to verify with the selected method every time you log in.");?>
</p></div>

                                    <div class="col-sm-1">

                                        <input <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['login_verification'] != null) {?>checked="checked"<?php }?> type="checkbox" class="form-control" name="enable_verification" value="1">

                                    </div>

                                    <label class="col-sm-11 control-label">

                                        <?php echo __("Enable two-factors login verification");?>


                                    </label>

                                </div>

                                <div class="form-group">

                                    <div class="col-sm-1">

                                        <input <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['login_verification'] == "email") {?>checked="checked"<?php }?> type="radio" class="form-control" name="verification" value="email">
<div class="verification-way">

                                        <i class="fa fa-envelope" aria-hidden="true"></i>

                                    </div>
                                    </div>



                                    <label class="col-sm-11 control-label">

                                        <p class="text-left">

                                            <?php echo __("Email login verification");?>


                                        </p>

                                        <p class="text-left">

                                            <?php echo __("Verification codes are sent by email");?>


                                        </p>

                                        <p class="text-left">

                                            <button onclick="show_verification_edit_email()" class="btn btn-primary" type="button"><?php echo __("Edit email");?>
</button>

                                        </p>

                                        <p id="verification_edit_email" style="display: none;" class="text-left col-sm-4">

                                            <input value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_email'];?>
" type="email" class="form-control" name="verification_email">

                                        </p>


                                    </label>

                                </div>

                                <div class="form-group">

                                    <div class="col-sm-1">

                                        <input <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['login_verification'] == "sms") {?>checked="checked"<?php }?> type="radio" class="form-control" name="verification" value="sms">
<div class="verification-way">

                                        <i class="fa fa-mobile" aria-hidden="true"></i>

                                    </div>
                                    </div>



                                    <label class="col-sm-11 control-label">

                                            <p class="text-left">

                                                <?php echo __("SMS login verification");?>


                                            </p>

                                            <p class="text-left">

                                                <?php echo __("Verification codes are sent by SMS text messages");?>


                                            </p>

                                            <p class="text-left">

                                                <button onclick="show_verification_edit_phone()" class="btn btn-primary" type="button" name="verification_edit_phone"><?php echo __("Edit phone");?>
</button>

                                            </p>

                                            <p id="verification_edit_phone" style="display: none;" class="text-left">

                                                <input value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_phone'];?>
" type="tel" class="form-control" name="verification_phone">

                                            </p>

                                    </label>

                                </div>

                               

                                <div id="ga-content" style="display: none;" class="col-sm-12 text-center">

                                    <div class="row col-sm-12">

                                        <?php echo __("Please scan this code in Google Authenticator mobile app");?>


                                    </div>

                                    <img id="ga-image"/>

                                    <div class="row col-sm-12">

                                        <a href="https://support.google.com/accounts/answer/1066447"><?php echo __("About Google Authenticator");?>
</a>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <div class="col-sm-10 col-sm-offset-1">

                                        <button type="submit" class="btn btn-primary" id="save_changes"><?php echo __("Save Changes");?>
</button>

                                    </div>

                                </div>



                                <!-- success -->

                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>

                                <!-- success -->



                                <!-- error -->

                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                                <!-- error -->

                            </form>

                        </div>

                        <!-- security tab -->

                    </div>

                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "profile") {?>

                    <div class="panel-heading with-icon with-nav">

                        <!-- panel title -->

                        <div class="mb20">

                            <i class="fa fa-user pr5 panel-icon"></i>

                            <strong><?php echo __("Edit Profile");?>
</strong>

                        </div>

                        <!-- panel title -->



                        <!-- panel nav -->

                        <ul class="nav nav-tabs">

                            <li class="active">

                                <a href="#basic" data-toggle="tab">

                                    <i class="fa fa-user fa-fw mr5"></i><strong class="pr5"><?php echo __("Basic");?>
</strong>

                                </a>

                            </li>

                           

                            <!--<li>

                                <a href="#location" data-toggle="tab">

                                    <i class="fa fa-map-marker fa-fw mr5"></i><strong class="pr5"><?php echo __("Location");?>
</strong>

                                </a>

                            </li>-->

                           

                        </ul>

                        <!-- panel nav -->

                    </div>



                    <div class="panel-body tab-content">

                        <!-- basic tab -->

                        <div class="tab-pane active" id="basic">

                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=basic">

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Full name");?>


                                    </label>

                                    <div class="col-sm-6">

                                        <input class="form-control" name="firstname" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_firstname'];?>
">

                                    </div>

                                </div>
                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("I am");?>


                                    </label>

                                    <div class="col-sm-6">

                                        <select name="gender" class="form-control">

                                            <option value="none"><?php echo __("Select");?>
</option>

                                            <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_gender'] == "male") {?>selected<?php }?> value="male"><?php echo __("Male");?>
</option>

                                            <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_gender'] == "female") {?>selected<?php }?> value="female"><?php echo __("Female");?>
</option>

                                        </select>

                                    </div>

                                </div>

                                

                                <!--<div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("I am");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <select name="gender" class="form-control">

                                            <option value="none"><?php echo __("Select Sex");?>
</option>

                                            <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_gender'] == "male") {?>selected<?php }?> value="male"><?php echo __("Male");?>
</option>

                                            <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_gender'] == "female") {?>selected<?php }?> value="female"><?php echo __("Female");?>
</option>

                                        </select>

                                    </div>

                                </div>-->



                               

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Bio");?>


                                    </label>

                                    <div class="col-sm-6">
                                      <input type="hidden" id="bioTextHt" value="">
                                      <div id="replaceTextarea">
                          							<textarea onkeyup="textAreaAdjust(this)" class="form-control bio_textInfo" data-role="none" name="biography" id="bio_info_id" style="padding-right:30px !important;resize: none;overflow:hidden"  ><?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_biography'];?>
</textarea>
                          						</div>

                                      <div class="x-form-tools bio_emoji">
                                          <div class="x-form-tools-emoji js_emoji-menu-toggle picker_bio" style="overflow:visible">

                                              <i class="fa fa-smile-o fa-lg"></i>

                                          </div>
                                      </div>

                                        <!-- <textarea class="form-control" name="biography"><?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_biography'];?>
</textarea> -->

                                    </div>

                                </div>
                                <div class="form-group">

                                     <label class="col-sm-3 control-label">

                                       <?php echo __("Location");?>


                                     </label>



                                     <div class="col-sm-9">

                                         <div class="row">
                                           <div class="col-sm-6">

                                               <input type="text" class="form-control" name="location" id="txtPlaces" onclick="getSomelocation()" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_location'];?>
" value="" placeholder=" ">

                                           </div>
                                           </div>
                                     </div>

                                 </div>
                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Website");?>


                                    </label>

                                    <div class="col-sm-6">

                                        <input type="text" class="form-control" name="website" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_website'];?>
">

                                    </div>

                                </div>

                                <div class="form-group">

                                     <label class="col-sm-3 control-label">

                                         <?php echo __("Birthdate");?>


                                     </label>



                                     <div class="col-sm-9">

                                         <div class="row">

                                             <div class="col-xs-4">

                                                 <select class="form-control" name="birth_month">

                                                     <option value="none"><?php echo __("Select Month");?>
</option>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '1') {?>selected<?php }?> value="1"><?php echo __("Jan");?>
</option>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '2') {?>selected<?php }?> value="2"><?php echo __("Feb");?>
</option>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '3') {?>selected<?php }?> value="3"><?php echo __("Mar");?>
</option>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '4') {?>selected<?php }?> value="4"><?php echo __("Apr");?>
</option>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '5') {?>selected<?php }?> value="5"><?php echo __("May");?>
</option>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '6') {?>selected<?php }?> value="6"><?php echo __("Jun");?>
</option>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '7') {?>selected<?php }?> value="7"><?php echo __("Jul");?>
</option>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '8') {?>selected<?php }?> value="8"><?php echo __("Aug");?>
</option>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '9') {?>selected<?php }?> value="9"><?php echo __("Sep");?>
</option>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '10') {?>selected<?php }?> value="10"><?php echo __("Oct");?>
</option>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '11') {?>selected<?php }?> value="11"><?php echo __("Nov");?>
</option>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '12') {?>selected<?php }?> value="12"><?php echo __("Dec");?>
</option>

                                                 </select>

                                             </div>

                                             <div class="col-xs-4">

                                                 <select class="form-control" name="birth_day">

                                                     <option value="none"><?php echo __("Select Day");?>
</option>

                                                     <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 31+1 - (1) : 1-(31)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['day'] == $_smarty_tpl->tpl_vars['i']->value) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>

                                                     <?php }
}
?>


                                                 </select>

                                             </div>
                                             <div class="col-xs-4">
												<div class="custom-select">
                                                 <select  class="form-control privacy_birthdate_cls" name="privacy_birthdate" id="privacy_birthdate">

                                                   <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthdate'] == "public") {?>selected<?php }?> value="public" data-content="<i class='Icon Icon--visibilityPublic Icon--medium'></i> <?php echo __("Public");?>
">

                                                       <?php echo __("Public");?>


                                                   </option>
                                                   <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthdate'] == "followers") {?>selected<?php }?> value="followers" data-content="<i class='Icon Icon--visibilityFollowers Icon--medium'></i> <?php echo __("Your followers");?>
">

                                                       <?php echo __("Your followers");?>


                                                   </option>
                                                   <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthdate'] == "following") {?>selected<?php }?> value="following" data-content="<i class='Icon Icon--visibilityFollowing Icon--medium'></i> <?php echo __("People you follow");?>
">

                                                       <?php echo __("People you follow");?>


                                                   </option>

                                                   <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthdate'] == "followers_following") {?>selected<?php }?> value="followers_following" data-content="<i class='Icon Icon--visibilityMutual Icon--medium'></i> <?php echo __("You follow each other");?>
">

                                                       <?php echo __("You follow each other");?>


                                                   </option>

                                                   <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthdate'] == "me") {?>selected<?php }?> value="me" data-content="<i class='Icon Icon--visibilityOnlyMe Icon--medium'></i> <?php echo __("Only you");?>
">

                                                       <?php echo __("Only you");?>


                                                   </option>

                                                 </select>
												</div>
                                             </div>
                                           </div>


                                     </div>

                                 </div>



                                  <div class="form-group">

                                       <label class="col-sm-3 control-label">



                                       </label>



                                       <div class="col-sm-9">

                                           <div class="row">




                                               <div class="col-xs-4">

                                                   <select class="form-control" name="birth_year">

                                                       <option value="none"><?php echo __("Select Year");?>
</option>

                                                       <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 2015+1 - (1905) : 1905-(2015)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1905, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>

                                                       <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['year'] == $_smarty_tpl->tpl_vars['i']->value) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>

                                                       <?php }
}
?>


                                                   </select>

                                               </div>


                                               <div class="col-xs-4">
												<div class="custom-select">
                                                 <select class="form-control privacy_birthyr_cls" name="privacy_birthyear" id="privacy_birthyear">

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthyear'] == "public") {?>selected<?php }?> value="public" data-content="<i class='Icon Icon--visibilityPublic Icon--medium'></i> <?php echo __("Public");?>
">

                                                         <?php echo __("Public");?>


                                                     </option>
                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthyear'] == "followers") {?>selected<?php }?> value="followers" data-content="<i class='Icon Icon--visibilityFollowers Icon--medium'></i> <?php echo __("Your followers");?>
">

                                                         <?php echo __("Your followers");?>


                                                     </option>
                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthyear'] == "following") {?>selected<?php }?> value="following" data-content="<i class='Icon Icon--visibilityFollowing Icon--medium'></i> <?php echo __("People you follow");?>
">

                                                         <?php echo __("People you follow");?>


                                                     </option>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthyear'] == "followers_following") {?>selected<?php }?> value="followers_following" data-content="<i class='Icon Icon--visibilityMutual Icon--medium'></i> <?php echo __("You follow each other");?>
">

                                                         <?php echo __("You follow each other");?>


                                                     </option>

                                                     <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthyear'] == "me") {?>selected<?php }?> value="me" data-content="<i class='Icon Icon--visibilityOnlyMe Icon--medium'></i> <?php echo __("Only you");?>
">

                                                         <?php echo __("Only you");?>


                                                     </option>

                                                 </select>
												</div>
                                               </div>
                                             </div>


                                       </div>

                                   </div>

									<div class="form-group"> 
									<label class="col-sm-3 control-label">

                                        <?php echo __("Theme Color");?>


                                    </label>
									<div class="col-sm-9">
										<ul class="color-dropdown col-sm-6">
											<li  class="col-xs-2">
												<span>
													<input type="radio" value="#ff691f" name="theme_color" class="red color-box" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] == '#ff691f') {?>checked="checked" <?php }?>><i class="fa fa-check" aria-hidden="true"></i>
												</span>
											</li>
											<li class="col-xs-2">
												<span>
													<input type="radio" value="#fab81e" name="theme_color" class="orange color-box" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] == '#fab81e') {?>checked="checked" <?php }?>><i class="fa fa-check" aria-hidden="true"></i>
												</span>
											</li>
											<li class="col-xs-2">
												<span>
													<input type="radio" value="#7fdbb6" name="theme_color" class="light-green color-box" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] == '#7fdbb6') {?>checked="checked" <?php }?>><i class="fa fa-check" aria-hidden="true"></i>
												</span>
											</li>
											<li class="col-xs-2">
												<span>
													<input type="radio" value="#19cf86" name="theme_color" class="green color-box" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] == '#19cf86') {?>checked="checked" <?php }?>><i class="fa fa-check" aria-hidden="true"></i>
												</span>
											</li>
											<li class="col-xs-2">
												<span>
													<input type="radio" value="#91d2fa" name="theme_color" class="light-blue color-box" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] == '#91d2fa') {?>checked="checked" <?php }?>><i class="fa fa-check" aria-hidden="true"></i>
												</span>
											</li>
											<li class="col-xs-2">
												<span>
													<input type="radio" value="#1b95e0" name="theme_color" class="blue color-box" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] == '#1b95e0') {?>checked="checked" <?php }?>><i class="fa fa-check" aria-hidden="true"></i>
												</span>
											</li>
											<li class="col-xs-2">
												<span>
													<input type="radio" value="#abb8c2" name="theme_color" class="grey color-box" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] == '#abb8c2') {?>checked="checked" <?php }?>><i class="fa fa-check" aria-hidden="true"></i>
												</span>
											</li>
											<li class="col-xs-2">
												<span>
													<input type="radio" value="#e81c4f" name="theme_color" class="amaranth color-box" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] == '#e81c4f') {?>checked="checked" <?php }?>><i class="fa fa-check" aria-hidden="true"></i>
												</span>
											</li>
											<li class="col-xs-2">
												<span>
													<input type="radio" value="#f58ea8" name="theme_color" class="pink color-box" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] == '#f58ea8') {?>checked="checked" <?php }?>><i class="fa fa-check" aria-hidden="true"></i>
												</span>
											</li>
											<li class="col-xs-2">
												<span>
													<input type="radio" value="#981ceb" name="theme_color" class="purple color-box" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] == '#981ceb') {?>checked="checked" <?php }?>><i class="fa fa-check" aria-hidden="true"></i>
												</span>
											</li>
											<li class="col-xs-4">
												<span>
													<input type="radio" id="rdOthrClr" value="" name="theme_color" class="green color-box" style="" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '') {
if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#ff691f' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#fab81e' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#7fdbb6' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#19cf86' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#91d2fa' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#1b95e0' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#abb8c2' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#e81c4f' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#f58ea8' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#981ceb') {?> checked="checked"<?php }
}?> ><i class="fa fa-plus" aria-hidden="true"></i>
													<input type="text" name="otherColor" id="otherColor" class="dispaly color-text" maxlength="6" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '') {
if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#ff691f' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#fab81e' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#7fdbb6' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#19cf86' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#91d2fa' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#1b95e0' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#abb8c2' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#e81c4f' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#f58ea8' && $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '#981ceb') {?> value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['theme_color'];?>
"<?php }
}?>>
											</li>
											<li class="col-xs-6" style="height:auto;">
												<button type="button" class="btn btn-default col-sm-12"id="cancle_changes" style="display:none;"><?php echo __("Cancel color");?>
</button>
											</li>
									<?php if ($_smarty_tpl->tpl_vars['user']->value->_data['theme_color'] != '') {?>		
											<li class="col-xs-6" style="height:auto;">
												<button type="button" class="btn btn-default col-sm-12" id="default_theme" style=""><?php echo __("Default theme");?>
</button>
											</li>
									<?php }?>		
									
									<div class="clearfix"></div>
										</ul>
									</div> 
								</div>

                                <!-- custom fields -->

                                <?php if ($_smarty_tpl->tpl_vars['custom_fields']->value['basic']) {?>

                                <?php $_smarty_tpl->_subTemplateRender('file:__custom_fields.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_custom_fields'=>$_smarty_tpl->tpl_vars['custom_fields']->value['basic'],'_registration'=>false), 0, false);
?>


                                <?php }?>

                                <!-- custom fields -->

                                <div class="form-group">

                                    <div class="col-sm-9 col-sm-offset-3">

                                        <button type="submit" class="btn btn-primary" id="save_changes"><?php echo __("Save Changes");?>
</button>

                                    </div>

                                </div>



                                <!-- success -->

                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>

                                <!-- success -->



                                <!-- error -->

                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                                <!-- error -->

                            </form>

                        </div>

                        <!-- basic tab -->



                        <!-- work tab -->

                        <div class="tab-pane" id="work">

                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=work">

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Work Title");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <input type="text" class="form-control" name="work_title" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_work_title'];?>
">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Work Place");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <input type="text" class="form-control" name="work_place" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_work_place'];?>
">

                                    </div>

                                </div>

                                <!-- custom fields -->

                                <?php if ($_smarty_tpl->tpl_vars['custom_fields']->value['work']) {?>

                                <?php $_smarty_tpl->_subTemplateRender('file:__custom_fields.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_custom_fields'=>$_smarty_tpl->tpl_vars['custom_fields']->value['work'],'_registration'=>false), 0, true);
?>


                                <?php }?>

                                <!-- custom fields -->

                                <div class="form-group">

                                    <div class="col-sm-9 col-sm-offset-3">

                                        <button type="submit" class="btn btn-primary" id="save_changes"><?php echo __("Save Changes");?>
</button>

                                    </div>

                                </div>



                                <!-- success -->

                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>

                                <!-- success -->



                                <!-- error -->

                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                                <!-- error -->

                            </form>

                        </div>

                        <!-- work tab -->



                        <!-- location tab -->

                        <div class="tab-pane" id="location">

                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=location">

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Current City");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <input type="text" class="form-control js_geocomplete" name="city" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_current_city'];?>
">


                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Hometown");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <input type="text" class="form-control js_geocomplete" name="hometown" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_hometown'];?>
">

                                    </div>

                                </div>

                                <!-- custom fields -->

                                <?php if ($_smarty_tpl->tpl_vars['custom_fields']->value['location']) {?>

                                <?php $_smarty_tpl->_subTemplateRender('file:__custom_fields.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_custom_fields'=>$_smarty_tpl->tpl_vars['custom_fields']->value['location'],'_registration'=>false), 0, true);
?>


                                <?php }?>

                                <!-- custom fields -->

                                <div class="form-group">

                                    <div class="col-sm-9 col-sm-offset-3">

                                        <button id="btnSave" type="submit" class="btn btn-primary" id="save_changes"><?php echo __("Save Changes");?>
</button>

                                    </div>

                                </div>



                                <!-- success -->

                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>

                                <!-- success -->



                                <!-- error -->

                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                                <!-- error -->

                            </form>

                        </div>

                        <!-- location tab -->



                        <!-- education tab -->

                        <div class="tab-pane" id="education">

                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=education">

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("School");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <input type="text" class="form-control" name="edu_school" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_edu_school'];?>
">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Major");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <input type="text" class="form-control" name="edu_major" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_edu_major'];?>
">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Class");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <input type="text" class="form-control" name="edu_class" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_edu_class'];?>
">

                                    </div>

                                </div>

                                <!-- custom fields -->

                                <?php if ($_smarty_tpl->tpl_vars['custom_fields']->value['education']) {?>

                                <?php $_smarty_tpl->_subTemplateRender('file:__custom_fields.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_custom_fields'=>$_smarty_tpl->tpl_vars['custom_fields']->value['education'],'_registration'=>false), 0, true);
?>


                                <?php }?>

                                <!-- custom fields -->

                                <div class="form-group">

                                    <div class="col-sm-9 col-sm-offset-3">

                                        <button type="submit" class="btn btn-primary" id="save_changes"><?php echo __("Save Changes");?>
</button>

                                    </div>

                                </div>



                                <!-- success -->

                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>

                                <!-- success -->



                                <!-- error -->

                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                                <!-- error -->

                            </form>

                        </div>

                        <!-- education tab -->



                        <!-- social tab -->

                        <div class="tab-pane" id="social">

                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=social">

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <i class="fa fa-facebook-square fa-2x" style="color: #3B579D"></i>

                                    </label>

                                    <div class="col-sm-9 mt5">

                                        <input type="text" class="form-control" name="facebook" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_social_facebook'];?>
" placeholder="<?php echo __('Facebook Profile URL');?>
">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <i class="fa fa-twitter-square fa-2x" style="color: #55ACEE"></i>

                                    </label>

                                    <div class="col-sm-9 mt5">

                                        <input type="text" class="form-control" name="twitter" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_social_twitter'];?>
" placeholder="<?php echo __('Twitter Profile URL');?>
">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <i class="fa fa-google-plus-square fa-2x" style="color: #DC4A38"></i>

                                    </label>

                                    <div class="col-sm-9 mt5">

                                        <input type="text" class="form-control" name="google" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_social_google'];?>
" placeholder="<?php echo __('Google+ Profile URL');?>
">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <i class="fa fa-youtube fa-2x" style="color: #E62117"></i>

                                    </label>

                                    <div class="col-sm-9 mt5">

                                        <input type="text" class="form-control" name="youtube" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_social_youtube'];?>
" placeholder="<?php echo __('YouTube Profile URL');?>
">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <i class="fa fa-instagram fa-2x" style="color: #3f729b"></i>

                                    </label>

                                    <div class="col-sm-9 mt5">

                                        <input type="text" class="form-control" name="instagram" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_social_instagram'];?>
" placeholder="<?php echo __('Instagram Profile URL');?>
">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <i class="fa fa-linkedin-square fa-2x" style="color: #1A84BC"></i>

                                    </label>

                                    <div class="col-sm-9 mt5">

                                        <input type="text" class="form-control" name="linkedin" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_social_linkedin'];?>
" placeholder="<?php echo __('LinkedIn Profile URL');?>
">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <i class="fa fa-vk fa-2x" style="color: #527498"></i>

                                    </label>

                                    <div class="col-sm-9 mt5">

                                        <input type="text" class="form-control" name="vkontakte" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_social_vkontakte'];?>
" placeholder="<?php echo __('Vkontakte Profile URL');?>
">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <div class="col-sm-9 col-sm-offset-3">

                                        <button type="submit" class="btn btn-primary" id="save_changes"><?php echo __("Save Changes");?>
</button>

                                    </div>

                                </div>



                                <!-- success -->

                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>

                                <!-- success -->



                                <!-- error -->

                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                                <!-- error -->

                            </form>

                        </div>

                        <!-- social tab -->



                        <?php if ($_smarty_tpl->tpl_vars['custom_fields']->value['other']) {?>

                            <!-- other tab -->

                            <div class="tab-pane" id="other">

                                <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=other">

                                    <!-- custom fields -->

                                    <?php if ($_smarty_tpl->tpl_vars['custom_fields']->value['other']) {?>

                                    <?php $_smarty_tpl->_subTemplateRender('file:__custom_fields.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_custom_fields'=>$_smarty_tpl->tpl_vars['custom_fields']->value['other'],'_registration'=>false), 0, true);
?>


                                    <?php }?>

                                    <!-- custom fields -->

                                    <div class="form-group">

                                        <div class="col-sm-9 col-sm-offset-3">

                                            <button type="submit" class="btn btn-primary" id="save_changes"><?php echo __("Save Changes");?>
</button>

                                        </div>

                                    </div>



                                    <!-- success -->

                                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>

                                    <!-- success -->



                                    <!-- error -->

                                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                                    <!-- error -->

                                </form>

                            </div>

                            <!-- other tab -->

                        <?php }?>

                    </div>

                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "privacy") {?>

                    <div class="panel-heading with-icon">

                        <!-- panel title -->

                        <i class="fa fa-lock pr5 panel-icon"></i>

                        <strong><?php echo __("Privacy Settings");?>
</strong>

                        <!-- panel title -->

                    </div>

                    <div class="panel-body">

                        <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=privacy">

                            <?php if ($_smarty_tpl->tpl_vars['system']->value['chat_enabled']) {?>

                                

                            <?php }?>

                            

                            

                            

                            

                            

                            

                            

                            

                           

                            
                            <div class="form-group">

                                <label class="col-sm-5 control-label" for="user_privacy_message">

                                   <?php echo __("Who can send you message");?>


                                </label>

                                <div class="col-sm-3">

                                    <select class="form-control" name="user_privacy_message" id="user_privacy_message">

                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_message'] == "public") {?>selected<?php }?> value="public">

                                            <?php echo __("Everyone");?>


                                        </option>

                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_message'] == "follow") {?>selected<?php }?> value="follow">

                                            <?php echo __("Follow");?>


                                        </option>
                                    </select>

                                </div>

                            </div>
                           

                            <div class="form-group">

                                <div class="col-sm-7 col-sm-offset-5">

                                    <button type="submit" class="btn btn-primary" id="save_changes"><?php echo __("Save Changes");?>
</button>

                                </div>

                            </div>



                            <!-- success -->

                            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>

                            <!-- success -->



                            <!-- error -->

                            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                            <!-- error -->

                        </form>

                    </div>

                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "security") {?>

                    <div class="panel-heading with-icon">

                        <!-- panel title -->

                        <i class="fa fa-shield pr5 panel-icon"></i>

                        <strong><?php echo __("Security Settings");?>
</strong>

                        <!-- panel title -->

                    </div>

                    <div class="panel-body">

                        <div class="table-responsive">

                            <table class="table table-bordered table-hover">

                                <thead>

                                    <tr>

                                        <th><?php echo __("ID");?>
</th>

                                        <th><?php echo __("Browser");?>
</th>

                                        <th><?php echo __("OS");?>
</th>

                                        <th><?php echo __("Date");?>
</th>

                                        <th><?php echo __("IP");?>
</th>

                                        <th><?php echo __("Actions");?>
</th>

                                    </tr>

                                </thead>

                                <tbody>

                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sessions']->value, 'session');
$_smarty_tpl->tpl_vars['session']->iteration = 0;
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['session']->value) {
$_smarty_tpl->tpl_vars['session']->iteration++;
$__foreach_session_0_saved = $_smarty_tpl->tpl_vars['session'];
?>

                                        <tr <?php if ($_smarty_tpl->tpl_vars['session']->value['session_token'] == $_smarty_tpl->tpl_vars['user']->value->_data['active_session']) {?>class="success"<?php }?>>

                                            <td><?php echo $_smarty_tpl->tpl_vars['session']->iteration;?>
</td>

                                            <td><?php echo $_smarty_tpl->tpl_vars['session']->value['user_browser'];?>
 <?php if ($_smarty_tpl->tpl_vars['session']->value['session_token'] == $_smarty_tpl->tpl_vars['user']->value->_data['active_session']) {?><span class="label label-info"><?php echo __("Active Session");?>
</span><?php }?></td>

                                            <td><?php echo $_smarty_tpl->tpl_vars['session']->value['user_os'];?>
</td>

                                            <td>

                                                <span class="js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['session']->value['session_date'];?>
"><?php echo $_smarty_tpl->tpl_vars['session']->value['session_date'];?>
</span>

                                            </td>

                                            <td><?php echo $_smarty_tpl->tpl_vars['session']->value['user_ip'];?>
</td>

                                            <td>

                                                <button data-toggle="tooltip" data-placement="top" title='<?php echo __("End Session");?>
' class="btn btn-xs btn-danger js_session-deleter" data-id="<?php echo $_smarty_tpl->tpl_vars['session']->value['session_id'];?>
">

                                                    <i class="fa fa-trash-o"></i>

                                                </button>

                                            </td>

                                        </tr>

                                    <?php
$_smarty_tpl->tpl_vars['session'] = $__foreach_session_0_saved;
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                                </tbody>

                            </table>

                        </div>

                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "reports") {?>

                    <div class="panel-heading with-icon">
                    <i class="fa fa-exclamation-triangle pr5 panel-icon"></i>
                    <strong><?php echo __("Reports");?>
</strong>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover js_dataTable">
                            <thead>
                                <tr>
                                    <th><?php echo __("ID");?>
</th>
                                    <th><?php echo __("Node");?>
</th>
                                    <th><?php echo __("Type");?>
</th>
                                    <th><?php echo __("Reporter By");?>
</th>
                                    <th><?php echo __("Time");?>
</th>                       <th><?php echo __("Reason");?>
</th>
                                    <th><?php echo __("Actions");?>
</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['reports']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>

                                    <tr>
                                        <td><?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
</td>
                                        <td>
                                            <?php if ($_smarty_tpl->tpl_vars['row']->value['node_type'] == "user") {?>
                                                <a target="_blank" href="/<?php echo $_smarty_tpl->tpl_vars['row']->value['node']['user_name'];?>
">
                                                    <img class="tbl-image" src="<?php echo $_smarty_tpl->tpl_vars['row']->value['node']['user_picture'];?>
">
                                                    <?php echo $_smarty_tpl->tpl_vars['row']->value['node']['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['row']->value['node']['user_lastname'];?>

                                                </a>
                                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['node_type'] == "page") {?>
                                                <a target="_blank" href="/pages/<?php echo $_smarty_tpl->tpl_vars['row']->value['node']['page_name'];?>
">
                                                    <img class="tbl-image" src="<?php echo $_smarty_tpl->tpl_vars['row']->value['node']['page_picture'];?>
">
                                                    <?php echo $_smarty_tpl->tpl_vars['row']->value['node']['page_title'];?>

                                                </a>
                                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['node_type'] == "group") {?>
                                                <a target="_blank" href="/groups/<?php echo $_smarty_tpl->tpl_vars['row']->value['node']['group_name'];?>
">
                                                    <img class="tbl-image" src="<?php echo $_smarty_tpl->tpl_vars['row']->value['node']['group_picture'];?>
">
                                                    <?php echo $_smarty_tpl->tpl_vars['row']->value['node']['group_title'];?>

                                                </a>
                                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['node_type'] == "event") {?>
                                                <a target="_blank" href="/events/<?php echo $_smarty_tpl->tpl_vars['row']->value['node_id'];?>
">
                                                    <img class="tbl-image" src="<?php echo $_smarty_tpl->tpl_vars['row']->value['node']['event_picture'];?>
">
                                                    <?php echo $_smarty_tpl->tpl_vars['row']->value['node']['event_title'];?>

                                                </a>
                                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['node_type'] == "post") {?>
                                                <a class="btn btn-xs btn-default" href="/posts/<?php echo $_smarty_tpl->tpl_vars['row']->value['node_id'];?>
" target="_blank">
                                                    <i class="fa fa-eye"></i> <?php echo __("View Post");?>

                                                </a>
                                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['node_type'] == "comment") {?>
                                                <a class="btn btn-xs btn-default" href="<?php echo $_smarty_tpl->tpl_vars['row']->value['url'];?>
" target="_blank">
                                                    <i class="fa fa-eye"></i> <?php echo __("View Comment");?>

                                                </a>
                                            <?php }?>
                                        </td>
                                        <td>
                                            <span class="label label-<?php echo $_smarty_tpl->tpl_vars['row']->value['node']['color'];?>
"><?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['row']->value['node_type']);?>
</span>
                                        </td>
                                        <td>
                                            <a target="_blank" href="/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
">
                                                <img class="tbl-image" src="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_picture'];?>
">
                                                <?php echo $_smarty_tpl->tpl_vars['row']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['row']->value['user_lastname'];?>

                                            </a>
                                        </td>
                                        <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['row']->value['time'],"%e %B %Y");?>
</td>                          <td><?php echo $_smarty_tpl->tpl_vars['row']->value['reason'];?>
</td>
                                        <td>
                                            <button data-toggle="tooltip" data-placement="top" title='<?php echo __("Mark as Safe");?>
' class="btn btn-xs btn-success js_admin-deleter-modrator" data-handle="report" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
">
                                                <i class="fa fa-check"></i>
                                            </button>
                                            <button data-toggle="tooltip" data-placement="top" title='<?php echo __("Delete");?>
' class="btn btn-xs btn-danger js_admin-deleter-modrator" data-handle="<?php echo $_smarty_tpl->tpl_vars['row']->value['node_type'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['node_id'];?>
" data-node="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                            <!-- <?php if ($_smarty_tpl->tpl_vars['row']->value['node_type'] == "user") {?>
                                                <a data-toggle="tooltip" data-placement="top" title='<?php echo __("Edit");?>
' target="_blank" href="/admincp/users/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['node_id'];?>
" class="btn btn-xs btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['node_type'] == "page") {?>
                                                <a data-toggle="tooltip" data-placement="top" title='<?php echo __("Edit");?>
' target="_blank" href="/admincp/pages/edit_page/<?php echo $_smarty_tpl->tpl_vars['row']->value['node_id'];?>
" class="btn btn-xs btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['node_type'] == "group") {?>
                                                <a data-toggle="tooltip" data-placement="top" title='<?php echo __("Edit");?>
' target="_blank" href="/admincp/groups/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['node_id'];?>
" class="btn btn-xs btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['node_type'] == "event") {?>
                                                <a data-toggle="tooltip" data-placement="top" title='<?php echo __("Edit");?>
' target="_blank" href="/admincp/events/edit_event/<?php echo $_smarty_tpl->tpl_vars['row']->value['node_id'];?>
" class="btn btn-xs btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            <?php }?> -->
                                        </td>
                                    </tr>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </tbody>
                        </table>
                    </div>
                </div>

                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "notifications") {?>

                    <div class="panel-heading with-icon">

                        <!-- panel title -->

                        <i class="fa fa-envelope-open-o pr5 panel-icon"></i>

                        <strong><?php echo __("Email Notifications");?>
</strong>

                        <!-- panel title -->

                    </div>

                    <div class="panel-body">

                        <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=notifications">

                            <div class="form-group">

                                <label class="col-sm-3 control-label">

                                    <?php echo __("Email Me When");?>


                                </label>

                                <div class="col-sm-9">

                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['email_post_likes']) {?>

                                        <div class="checkbox checkbox-primary">

                                            <input type="checkbox" name="email_post_likes" id="email_post_likes" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['email_post_likes']) {?>checked<?php }?>>

                                            <label for="email_post_likes"><?php echo __("Someone liked my post");?>
</label>

                                        </div>

                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['email_post_comments']) {?>

                                        <div class="checkbox checkbox-primary">

                                            <input type="checkbox" name="email_post_comments" id="email_post_comments" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['email_post_comments']) {?>checked<?php }?>>

                                            <label for="email_post_comments"><?php echo __("Someone commented on my post");?>
</label>

                                        </div>

                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['email_post_shares']) {?>

                                        <div class="checkbox checkbox-primary">

                                            <input type="checkbox" name="email_post_shares" id="email_post_shares" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['email_post_shares']) {?>checked<?php }?>>

                                            <label for="email_post_shares"><?php echo __("Someone shared my post");?>
</label>

                                        </div>

                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['email_wall_posts']) {?>

                                        <div class="checkbox checkbox-primary">

                                            <input type="checkbox" name="email_wall_posts" id="email_wall_posts" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['email_wall_posts']) {?>checked<?php }?>>

                                            <label for="email_wall_posts"><?php echo __("Someone posted on my timeline");?>
</label>

                                        </div>

                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['email_mentions']) {?>

                                        <div class="checkbox checkbox-primary">

                                            <input type="checkbox" name="email_mentions" id="email_mentions" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['email_mentions']) {?>checked<?php }?>>

                                            <label for="email_mentions"><?php echo __("Someone mentioned me");?>
</label>

                                        </div>

                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['email_profile_visits']) {?>

                                        <div class="checkbox checkbox-primary">

                                            <input type="checkbox" name="email_profile_visits" id="email_profile_visits" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['email_profile_visits']) {?>checked<?php }?>>

                                            <label for="email_profile_visits"><?php echo __("Someone visited my profile");?>
</label>

                                        </div>

                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['email_friend_requests']) {?>

                                        <div class="checkbox checkbox-primary">

                                            <input type="checkbox" name="email_friend_requests" id="email_friend_requests" <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['email_friend_requests']) {?>checked<?php }?>>

                                            <label for="email_friend_requests"><?php echo __("Someone sent me/accepted my friend requset");?>
</label>

                                        </div>

                                    <?php }?>

                                </div>

                            </div>



                            <div class="form-group">

                                <div class="col-sm-9 col-sm-offset-3">

                                    <button type="submit" class="btn btn-primary" id="save_changes"><?php echo __("Save Changes");?>
</button>

                                </div>

                            </div>



                            <!-- success -->

                            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>

                            <!-- success -->



                            <!-- error -->

                            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                            <!-- error -->

                        </form>

                    </div>

                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "linked") {?>

                    <div class="panel-heading with-icon">

                        <!-- panel title -->

                        <i class="fa fa-share-alt pr5 panel-icon"></i>

                        <strong><?php echo __("Linked Accounts");?>
</strong>

                        <!-- panel title -->

                    </div>

                    <div class="panel-body">

                        <ul>

                            <?php if ($_smarty_tpl->tpl_vars['system']->value['facebook_login_enabled']) {?>

                                <li class="feeds-item">

                                    <div class="data-container">

                                        <div class="data-avatar">

                                            <i class="fa fa-facebook-square" style="color: #3B579D"></i>

                                        </div>

                                        <div class="data-content">

                                            <div class="pull-right flip">

                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['facebook_connected']) {?>

                                                <a class="btn btn-danger" href="/revoke/facebook"><?php echo __("Disconnect");?>
</a>

                                                <?php } else { ?>

                                                <a class="btn btn-primary" href="/connect/facebook"><?php echo __("Connect");?>
</a>

                                                <?php }?>

                                            </div>

                                            <div>

                                                <div class="name mt5 text-primary">

                                                    <?php echo __("Facebook");?>


                                                </div>

                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['facebook_connected']) {?>

                                                <?php echo __("Your account is connected to");?>
 <?php echo __("Facebook");?>


                                                <?php } else { ?>

                                                <?php echo __("Connect your account to");?>
 <?php echo __("Facebook");?>


                                                <?php }?>

                                            </div>

                                        </div>

                                    </div>

                                </li>

                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['system']->value['twitter_login_enabled']) {?>

                                <li class="feeds-item">

                                    <div class="data-container">

                                        <div class="data-avatar">

                                            <i class="fa fa-twitter-square" style="color: #55ACEE"></i>

                                        </div>

                                        <div class="data-content">

                                            <div class="pull-right flip">

                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['twitter_connected']) {?>

                                                <a class="btn btn-danger" href="/revoke/twitter"><?php echo __("Disconnect");?>
</a>

                                                <?php } else { ?>

                                                <a class="btn btn-primary" href="/connect/twitter"><?php echo __("Connect");?>
</a>

                                                <?php }?>

                                            </div>

                                            <div>

                                                <div class="name mt5 text-primary">

                                                    <?php echo __("Twitter");?>


                                                </div>

                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['twitter_connected']) {?>

                                                <?php echo __("Your account is connected to");?>
 <?php echo __("Twitter");?>


                                                <?php } else { ?>

                                                <?php echo __("Connect your account to");?>
 <?php echo __("Twitter");?>


                                                <?php }?>

                                            </div>

                                        </div>

                                    </div>

                                </li>

                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['system']->value['google_login_enabled']) {?>

                                <li class="feeds-item">

                                    <div class="data-container">

                                        <div class="data-avatar">

                                            <i class="fa fa-google-plus-square" style="color: #DC4A38"></i>

                                        </div>

                                        <div class="data-content">

                                            <div class="pull-right flip">

                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['google_connected']) {?>

                                                <a class="btn btn-danger" href="/revoke/google"><?php echo __("Disconnect");?>
</a>

                                                <?php } else { ?>

                                                <a class="btn btn-primary" href="/connect/google"><?php echo __("Connect");?>
</a>

                                                <?php }?>

                                            </div>

                                            <div>

                                                <div class="name mt5 text-primary">

                                                    <?php echo __("Google");?>


                                                </div>

                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['google_connected']) {?>

                                                <?php echo __("Your account is connected to");?>
 <?php echo __("Google");?>


                                                <?php } else { ?>

                                                <?php echo __("Connect your account to");?>
 <?php echo __("Google");?>


                                                <?php }?>

                                            </div>

                                        </div>

                                    </div>

                                </li>

                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['system']->value['instagram_login_enabled']) {?>

                                <li class="feeds-item">

                                    <div class="data-container">

                                        <div class="data-avatar">

                                            <i class="fa fa-instagram" style="color: #3f729b"></i>

                                        </div>

                                        <div class="data-content">

                                            <div class="pull-right flip">

                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['instagram_connected']) {?>

                                                    <a class="btn btn-danger" href="/revoke/instagram"><?php echo __("Disconnect");?>
</a>

                                                <?php } else { ?>

                                                    <a class="btn btn-primary" href="/connect/instagram"><?php echo __("Connect");?>
</a>

                                                <?php }?>

                                            </div>

                                            <div>

                                                <div class="name mt5 text-primary">

                                                    <?php echo __("Instagram");?>


                                                </div>

                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['instagram_connected']) {?>

                                                    <?php echo __("Your account is connected to");?>
 <?php echo __("Instagram");?>


                                                <?php } else { ?>

                                                    <?php echo __("Connect your account to");?>
 <?php echo __("Instagram");?>


                                                <?php }?>

                                            </div>

                                        </div>

                                    </div>

                                </li>

                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['system']->value['linkedin_login_enabled']) {?>

                                <li class="feeds-item">

                                    <div class="data-container">

                                        <div class="data-avatar">

                                            <i class="fa fa-linkedin-square" style="color: #1A84BC"></i>

                                        </div>

                                        <div class="data-content">

                                            <div class="pull-right flip">

                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['linkedin_connected']) {?>

                                                <a class="btn btn-danger" href="/revoke/linkedin"><?php echo __("Disconnect");?>
</a>

                                                <?php } else { ?>

                                                <a class="btn btn-primary" href="/connect/linkedin"><?php echo __("Connect");?>
</a>

                                                <?php }?>

                                            </div>

                                            <div>

                                                <div class="name mt5 text-primary">

                                                    <?php echo __("Linkedin");?>


                                                </div>

                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['linkedin_connected']) {?>

                                                <?php echo __("Your account is connected to");?>
 <?php echo __("Linkedin");?>


                                                <?php } else { ?>

                                                <?php echo __("Connect your account to");?>
 <?php echo __("Linkedin");?>


                                                <?php }?>

                                            </div>

                                        </div>

                                    </div>

                                </li>

                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['system']->value['vkontakte_login_enabled']) {?>

                                <li class="feeds-item">

                                    <div class="data-container">

                                        <div class="data-avatar">

                                            <i class="fa fa-vk" style="color: #527498"></i>

                                        </div>

                                        <div class="data-content">

                                            <div class="pull-right flip">

                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['vkontakte_connected']) {?>

                                                <a class="btn btn-danger" href="/revoke/vkontakte"><?php echo __("Disconnect");?>
</a>

                                                <?php } else { ?>

                                                <a class="btn btn-primary" href="/connect/vkontakte"><?php echo __("Connect");?>
</a>

                                                <?php }?>

                                            </div>

                                            <div>

                                                <div class="name mt5 text-primary">

                                                    <?php echo __("Vkontakte");?>


                                                </div>

                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['vkontakte_connected']) {?>

                                                <?php echo __("Your account is connected to");?>
 <?php echo __("Vkontakte");?>


                                                <?php } else { ?>

                                                <?php echo __("Connect your account to");?>
 <?php echo __("Vkontakte");?>


                                                <?php }?>

                                            </div>

                                        </div>

                                    </div>

                                </li>

                            <?php }?>

                        </ul>

                    </div>

                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "blocking") {?>

                    <div class="panel-heading with-icon">

                        <!-- panel title -->

                        <i class="fa fa-minus-circle pr5 panel-icon"></i>

                        <strong><?php echo __("Manage Blocking");?>
</strong>

                        <!-- panel title -->

                    </div>

                    <div class="panel-body">

                        <div class="alert alert-info">

                            <i class="fa fa-info-circle fa-lg mr10"></i><?php echo __("Once you block someone, that person can no longer see things you post on your timeline");?>
<br>

                        </div>



                        <?php if (count($_smarty_tpl->tpl_vars['blocks']->value) > 0) {?>

                            <ul>

                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['blocks']->value, '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>

                                <?php $_smarty_tpl->_subTemplateRender('file:__feeds_user.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_connection'=>"blocked"), 0, true);
?>


                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                            </ul>

                        <?php } else { ?>

                            <p class="text-center text-muted">

                                <?php echo __("No blocked users");?>


                            </p>

                        <?php }?>



                        <?php if (count($_smarty_tpl->tpl_vars['blocks']->value) >= $_smarty_tpl->tpl_vars['system']->value['max_results']) {?>

                            <!-- see-more -->

                            <div class="alert alert-info see-more js_see-more" data-get="blocks">

                                <span><?php echo __("See More");?>
</span>

                                <div class="loader loader_small x-hidden"></div>

                            </div>

                            <!-- see-more -->

                        <?php }?>

                    </div>

                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "affiliates") {?>

                    <div class="panel-heading with-icon with-nav">

                        <!-- panel title -->

                        <div class="mb20">

                            <i class="fa fa-exchange pr5 panel-icon"></i>

                            <strong><?php echo __("Affiliates");?>
</strong>

                        </div>

                        <!-- panel title -->



                        <!-- panel nav -->

                        <ul class="nav nav-tabs">

                            <li class="active">

                                <a href="#affiliates" data-toggle="tab">

                                    <i class="fa fa-users fa-fw mr5"></i><strong class="pr5"><?php echo __("My Affiliates");?>
</strong>

                                </a>

                            </li>

                            <li>

                                <a href="#payments" data-toggle="tab">

                                    <i class="fa fa-money fa-fw mr5"></i><strong class="pr5"><?php echo __("Payments");?>
</strong>

                                </a>

                            </li>

                        </ul>

                        <!-- panel nav -->

                    </div>

                    <div class="panel-body tab-content">

                        <!-- affiliates tab -->

                        <div class="tab-pane active" id="affiliates">

                            <div class="alert alert-warning">

                                <div class="icon">

                                    <i class="fa fa-money fa-2x"></i>

                                </div>

                                <div class="text">

                                    <strong><?php echo __("Affiliates System");?>
</strong><br>

                                    <?php echo __("Earn up to");?>
 <?php echo $_smarty_tpl->tpl_vars['system']->value['system_currency_symbol'];
echo number_format($_smarty_tpl->tpl_vars['system']->value['affiliates_per_user'],2);?>
 <?php echo __("For each user your refer");?>
.<br>

                                    <?php echo __("You will be paid when");?>


                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['affiliate_type'] == "registeration") {?>.

                                        <?php echo __("new user registered");?>


                                    <?php } else { ?>

                                        <?php echo __("new user registered & bought a package");?>
.

                                    <?php }?>

                                </div>

                            </div>

                            <div class="text-center text-readable mb20">

                                <?php echo __("Your affiliate link is");?>
<br>

                                <a href="/?ref=<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
" target="_blank"  class="ptb5 plr20" style="border: 1px solid #ddd;">

                                    /?ref=<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>


                                </a>

                            </div>



                            <div class="divider"></div>



                            <?php if (count($_smarty_tpl->tpl_vars['affiliates']->value) > 0) {?>

                                <ul>

                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['affiliates']->value, '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>

                                    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_user.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_connection'=>$_smarty_tpl->tpl_vars['_user']->value["connection"]), 0, true);
?>


                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                                </ul>

                            <?php } else { ?>

                                <p class="text-center text-muted">

                                    <?php echo __("No affiliates");?>


                                </p>

                            <?php }?>



                            <!-- see-more -->

                            <?php if (count($_smarty_tpl->tpl_vars['affiliates']->value) >= $_smarty_tpl->tpl_vars['system']->value['max_results']) {?>

                                <div class="alert alert-info see-more js_see-more" data-uid="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_id'];?>
" data-get="affiliates">

                                    <span><?php echo __("See More");?>
</span>

                                    <div class="loader loader_small x-hidden"></div>

                                </div>

                            <?php }?>

                            <!-- see-more -->

                        </div>

                        <!-- affiliates tab -->



                        <!-- payments tab -->

                        <div class="tab-pane" id="payments">

                            <div class="alert alert-info">

                                <div class="icon">

                                    <i class="fa fa-info-circle fa-2x"></i>

                                </div>

                                <div class="text pt5">

                                    <?php echo __("The minimum withdrawal request amount is");?>
 <?php echo $_smarty_tpl->tpl_vars['system']->value['system_currency_symbol'];
echo $_smarty_tpl->tpl_vars['system']->value['affiliates_min_withdrawal'];?>


                                </div>

                            </div>

                            <form class="js_ajax-forms form-horizontal" data-url="users/withdraw.php">

                                <div class="form-group">

                                    <label class="col-sm-3 control-label"></label>

                                    <div class="col-sm-9 text-lg">

                                        <?php echo __("Your Balance");?>
: <?php echo $_smarty_tpl->tpl_vars['system']->value['system_currency_symbol'];
echo number_format($_smarty_tpl->tpl_vars['user']->value->_data['user_affiliate_balance'],2);?>


                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Email");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <input type="email" class="form-control" name="email" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_affiliate_email'];?>
">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Amount");?>
 (<?php echo $_smarty_tpl->tpl_vars['system']->value['system_currency'];?>
)

                                    </label>

                                    <div class="col-sm-9">

                                        <input type="text" class="form-control" name="amount">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-sm-3 control-label">

                                        <?php echo __("Payment Method");?>


                                    </label>

                                    <div class="col-sm-9">

                                        <?php if ($_smarty_tpl->tpl_vars['system']->value['affiliate_payment_method'] == "both" || $_smarty_tpl->tpl_vars['system']->value['affiliate_payment_method'] == "paypal") {?>

                                            <div class="radio radio-primary radio-inline">

                                                <input type="radio" name="method" id="method_paypal" value="paypal" <?php if ($_smarty_tpl->tpl_vars['system']->value['affiliate_payment_method'] == "paypal") {?>checked<?php }?>>

                                                <label for="method_paypal"><?php echo __("Paypal");?>
</label>

                                            </div>

                                        <?php }?>

                                        <?php if ($_smarty_tpl->tpl_vars['system']->value['affiliate_payment_method'] == "both" || $_smarty_tpl->tpl_vars['system']->value['affiliate_payment_method'] == "skrill") {?>

                                            <div class="radio radio-primary radio-inline">

                                                <input type="radio" name="method" id="method_skrill" value="skrill" <?php if ($_smarty_tpl->tpl_vars['system']->value['affiliate_payment_method'] == "skrill") {?>checked<?php }?>>

                                                <label for="method_skrill"><?php echo __("Skrill");?>
</label>

                                            </div>

                                        <?php }?>

                                    </div>

                                </div>



                                <div class="form-group">

                                    <div class="col-sm-9 col-sm-offset-3">

                                        <button type="submit" class="btn btn-success"><?php echo __("Make a withdrawal");?>
</button>

                                    </div>

                                </div>



                                <!-- success -->

                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>

                                <!-- success -->



                                <!-- error -->

                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

                                <!-- error -->

                            </form>



                            <div class="divider"></div>



                            <h4><?php echo __("Withdrawal History");?>
</h2>

                            <?php if (count($_smarty_tpl->tpl_vars['payments']->value) > 0) {?>

                                <div class="table-responsive mt20">

                                    <table class="table table-striped table-bordered table-hover">

                                        <thead>

                                            <tr>

                                                <th><?php echo __("ID");?>
</th>

                                                <th><?php echo __("Email");?>
</th>

                                                <th><?php echo __("Amount");?>
</th>

                                                <th><?php echo __("Method");?>
</th>

                                                <th><?php echo __("Time");?>
</th>

                                                <th><?php echo __("Status");?>
</th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['payments']->value, 'payment');
$_smarty_tpl->tpl_vars['payment']->iteration = 0;
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['payment']->value) {
$_smarty_tpl->tpl_vars['payment']->iteration++;
$__foreach_payment_4_saved = $_smarty_tpl->tpl_vars['payment'];
?>

                                            <tr>

                                                <td><?php echo $_smarty_tpl->tpl_vars['payment']->iteration;?>
</td>

                                                <td><?php echo $_smarty_tpl->tpl_vars['payment']->value['email'];?>
</td>

                                                <td><?php echo $_smarty_tpl->tpl_vars['system']->value['system_currency_symbol'];
echo number_format($_smarty_tpl->tpl_vars['payment']->value['amount'],2);?>
</td>

                                                <td>

                                                    <?php echo ucfirst($_smarty_tpl->tpl_vars['payment']->value['method']);?>


                                                </td>

                                                <td>

                                                    <span class="js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['payment']->value['time'];?>
"><?php echo $_smarty_tpl->tpl_vars['payment']->value['time'];?>
</span>

                                                </td>

                                                <td>

                                                    <?php if ($_smarty_tpl->tpl_vars['payment']->value['status'] == '0') {?>

                                                        <span class="label label-warning"><?php echo __("Pending");?>
</span>

                                                    <?php } elseif ($_smarty_tpl->tpl_vars['payment']->value['status'] == '1') {?>

                                                        <span class="label label-success"><?php echo __("Approved");?>
</span>

                                                    <?php } else { ?>

                                                        <span class="label label-danger"><?php echo __("Declined");?>
</span>

                                                    <?php }?>

                                                </td>

                                            </tr>

                                            <?php
$_smarty_tpl->tpl_vars['payment'] = $__foreach_payment_4_saved;
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                                        </tbody>

                                    </table>

                                </div>

                            <?php } else { ?>

                                <p class="text-center text-muted">

                                    <?php echo __("No withdrawal history");?>


                                </p>

                            <?php }?>

                        </div>

                        <!-- payments tab -->

                    </div>

                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "verification") {?>

                    <div class="panel-heading with-icon">

                        <!-- panel title -->

                        <i class="fa fa-check-circle pr5 panel-icon"></i>

                        <strong><?php echo __("Verification");?>
</strong>

                        <!-- panel title -->

                    </div>

                    <div class="panel-body">



                        <?php if ($_smarty_tpl->tpl_vars['case']->value == "verified") {?>

                            <div class="text-center">

                                <div class="big-icon success">

                                    <i class="fa fa-thumbs-o-up fa-3x"></i>

                                </div>

                                <h4><?php echo __("Congratulations");?>
</h4>

                                <p class="mt20"><?php echo __("This account is verified");?>
</p>

                            </div>

                        <?php } elseif ($_smarty_tpl->tpl_vars['case']->value == "request") {?>

                            <div class="alert alert-info">

                                <i class="fa fa-info-circle fa-lg mr10"></i><?php echo __("You can send verification request to verify your account");?>
<br>

                            </div>

                            <div class="text-center">

                                <button class="btn btn-success js_verify">

                                    <i class="fa fa-check-circle mr5"></i><?php echo __("Verification Request");?>


                                </button>

                            </div>

                        <?php } elseif ($_smarty_tpl->tpl_vars['case']->value == "pending") {?>

                            <div class="alert alert-info">

                                <i class="fa fa-info-circle fa-lg mr10"></i><?php echo __("Your verification request is still awaiting admin approval");?>
<br>

                            </div>

                            <div class="text-center">

                                <button class="btn btn-warning btn-delete js_unverify"><i class="fa fa-clock-o mr5"></i><?php echo __("Pending");?>
</button>

                            </div>

                        <?php } elseif ($_smarty_tpl->tpl_vars['case']->value == "declined") {?>

                            <div class="text-center">

                                <div class="big-icon error">

                                    <i class="fa fa-meh-o fa-3x"></i>

                                </div>

                                <h4><?php echo __("Sorry");?>
</h4>

                                <p class="mt20"><?php echo __("Your verification request has been declined by the admin");?>
</p>

                            </div>

                        <?php }?>

                    </div>

                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "membership") {?>

                    <div class="panel-heading with-icon">

                        <!-- panel title -->

                        <i class="fa fa-id-card-o pr5 panel-icon"></i>

                        <strong><?php echo __("Membership");?>
</strong>

                        <!-- panel title -->

                    </div>

                    <div class="panel-body">

                        <div class="alert alert-warning">

                            <div class="icon">

                                <i class="fa fa-id-card-o fa-2x"></i>

                            </div>

                            <div class="text">

                                <strong><?php echo __("Membership");?>
</strong><br>

                                <?php echo __("Choose the Plan That's Right for You");?>
, <?php echo __("Check the package from");?>
 <a href="/packages"><?php echo __("Here");?>
</a>

                            </div>

                        </div>



                        <?php if (!$_smarty_tpl->tpl_vars['user']->value->_data['user_subscribed']) {?>

                            <div class="text-center">

                                <a href="/packages" class="btn btn-primary"><i class="fa fa-rocket mr5"></i><?php echo __("Upgrade to Pro");?>
</a>

                            </div>

                        <?php } else { ?>

                            <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_subscribed']) {?>

                                <form class="form-horizontal">

                                    <div class="form-group mb0">

                                        <label class="col-sm-3 control-label text-left">

                                            <?php echo __("Package");?>


                                        </label>

                                        <div class="col-sm-9">

                                            <p class="form-control-static">

                                                <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['name'];?>
 (<?php echo $_smarty_tpl->tpl_vars['system']->value['system_currency_symbol'];
echo $_smarty_tpl->tpl_vars['user']->value->_data['price'];?>


                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['period'] == "life") {
echo __("Life Time");
} else {
echo __("per");?>
 <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['period_num'] != '1') {
echo $_smarty_tpl->tpl_vars['user']->value->_data['period_num'];
}?> <?php echo __(ucfirst($_smarty_tpl->tpl_vars['user']->value->_data['period']));
}?>)

                                            </p>

                                        </div>

                                    </div>

                                    <div class="form-group mb0">

                                        <label class="col-sm-3 control-label text-left">

                                            <?php echo __("Subscription Date");?>


                                        </label>

                                        <div class="col-sm-9">

                                            <p class="form-control-static">

                                                <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['user']->value->_data['user_subscription_date'],"%e %B %Y");?>


                                            </p>

                                        </div>

                                    </div>

                                    <div class="form-group mb0">

                                        <label class="col-sm-3 control-label text-left">

                                            <?php echo __("Expiration Date");?>


                                        </label>

                                        <div class="col-sm-9">

                                            <p class="form-control-static">

                                                <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['user']->value->_data['subscription_end'],"%e %B %Y");?>
 (<?php if ($_smarty_tpl->tpl_vars['user']->value->_data['subscription_timeleft'] > 0) {
echo __("Remining");?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['subscription_timeleft'];?>
 <?php echo __("Days");
} else {
echo __("Expired");
}?>)

                                            </p>

                                        </div>

                                    </div>

                                    <div class="form-group mb0">

                                        <label class="col-sm-3 control-label text-left">

                                            <?php echo __("Boosted Posts");?>


                                        </label>

                                        <div class="col-sm-9">

                                            <p class="form-control-static">

                                                <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_boosted_posts'];?>
/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['boost_posts'];?>


                                            </p>



                                            <div class="progress mb5">

                                                <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="<?php echo ($_smarty_tpl->tpl_vars['user']->value->_data['user_boosted_posts']/$_smarty_tpl->tpl_vars['user']->value->_data['boost_pages'])*100;?>
" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($_smarty_tpl->tpl_vars['user']->value->_data['user_boosted_posts']/$_smarty_tpl->tpl_vars['user']->value->_data['boost_pages'])*100;?>
%"></div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-3 control-label text-left">

                                            <?php echo __("Boosted Pages");?>


                                        </label>

                                        <div class="col-sm-9">

                                            <p class="form-control-static">

                                                <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_boosted_pages'];?>
/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['boost_pages'];?>


                                            </p>



                                            <div class="progress mb5">

                                                <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="<?php echo ($_smarty_tpl->tpl_vars['user']->value->_data['user_boosted_pages']/$_smarty_tpl->tpl_vars['user']->value->_data['boost_pages'])*100;?>
" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($_smarty_tpl->tpl_vars['user']->value->_data['user_boosted_pages']/$_smarty_tpl->tpl_vars['user']->value->_data['boost_pages'])*100;?>
%"></div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-3 control-label text-left">



                                        </label>

                                        <div class="col-sm-9">

                                            <a href="/packages" class="btn btn-primary"><i class="fa fa-rocket mr5"></i><?php echo __("Upgrade Package");?>
</a>

                                        </div>

                                    </div>

                                </form>

                            <?php }?>

                        <?php }?>

                    </div>

                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "delete") {?>

                    <div class="panel-heading with-icon">

                        <!-- panel title -->

                        <i class="fa fa-trash pr5 panel-icon"></i>

                        <strong><?php echo __("Delete Account");?>
</strong>

                        <!-- panel title -->

                    </div>

                    <div class="panel-body">

                        <div class="alert alert-warning">

                            <i class="fa fa-exclamation-triangle fa-lg mr10"></i><?php echo __("Once you delete your account you will no longer can access it again");?>


                        </div>

                        <div class="text-center">

                            <button class="btn btn-danger js_delete-user"><i class="fa fa-trash mr5"></i><?php echo __("Delete My Account");?>
</button>

                        </div>

                    </div>

                <?php }?>



            </div>

        </div>

        <!-- right panel -->



    </div>

</div>

<!-- page content -->

<?php echo '<script'; ?>
 type="text/javascript">

  showGa = function (url) {
    jQuery('#ga-image').attr('src', url);
    jQuery('#ga-content').show();
  };

  show_verification_edit_email = function () {
    jQuery('#verification_edit_email').show();
  };

  show_verification_edit_phone = function () {
    jQuery('#verification_edit_phone').show();
  };

<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
$(document).ready(function(){


  $('#bio_info_id').removeAttr('placeholder');

    $(".nav.nav-tabs li a").click(function(){
     $('.alert.alert-success.mb0.mt10.x-hidden').hide();
    });
    });

    jQuery.fn.putCursorAtEndPostDetail = function() {

              return this.each(function() {

                $(this).focus()

                // If this function exists...
                if (this.setSelectionRange) {
                  // ... then use it (Doesn't work in IE)

                  // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
                  var len = $(this).val().length * 2;

                  this.setSelectionRange(len, len);

                } else {
                // ... otherwise replace the contents with itself
                // (Doesn't work in Google Chrome)

                  $(this).val($(this).val());

                }

                // Scroll to the bottom, in case we're in a tall textarea
                // (Necessary for Firefox and Google Chrome)
                this.scrollTop = 999999;

              });

        };

    	$(function() {

    		$('.picker_bio').lsxEmojiPicker({
    			closeOnSelect: false,
    			twemoji: true,
    			onSelect: function(emoji){
            //alert($("#bio_info_id").height());
            var calTxtarHt = $("#bioTextHt").val();
            //$('#bio_info_id').height(calTxtarHt);

    				var cursorPosition = $("#bio_info_id")[0].selectionStart;
    				var FirstPart = $("#bio_info_id").val().substring(0, cursorPosition);
    				var NewText = emoji.value;
    				var SecondPart = $("#bio_info_id").val().substring(cursorPosition + 1, $("#bio_info_id").val().length);

    				//$(".replyComm_chk").html(FirstPart+NewText+SecondPart);

    				var txt_placeholder = $("#bio_info_id").attr('placeholder');

            //$("textarea#bio_info_id").val(FirstPart+NewText+SecondPart);
    				$("#replaceTextarea").html('<textarea onkeyup="textAreaAdjust(this)" class="form-control bio_textInfo" data-role="none" name="biography" id="bio_info_id" style="padding-right:30px !important;resize: none;overflow:hidden"  >'+FirstPart+NewText+SecondPart+'</textarea>');
            $('#bio_info_id').css("height",calTxtarHt+'px');
            $("#bio_info_id").putCursorAtEndPostDetail();
    			}
    		});

    		if($('.lsx-emojipicker-container').length > 0){
    			$('.lsx-emojipicker-container').css('top','25px');
    		}
    	});


      $(document).mouseup(function(e)
{
    var container = $(".lsx-emojipicker-container");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        container.hide();
    }
});

function textAreaAdjust(o) {
  o.style.height = "2px";
  o.style.height = (13+o.scrollHeight)+"px";
}
  $("#bio_info_id").height( $("#bio_info_id")[0].scrollHeight );
  var calTxtarHt = $("#bio_info_id").height();
  $('#bioTextHt').val(calTxtarHt);
  $('#bio_info_id').height(calTxtarHt);

<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {

//       const myElement = document.getElementById('ELEMENT_ID');
// var autocomplete = new google.maps.places.Autocomplete(myElement, { placeholder: undefined });

      
      $('#txtPlaces').attr('placeholder', '');
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
        $('#txtPlaces').attr('placeholder', '');
        google.maps.event.addListener(places, 'place_changed', function () {

            var place = places.getPlace();
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var mesg = "Address: " + address;
            mesg += "\nLatitude: " + latitude;
            mesg += "\nLongitude: " + longitude;
            //alert(mesg);
        });
    });

    function getSomelocation(){


        // navigator.geolocation.getCurrentPosition(
        //     function( position ){ // success cb
        //         console.log( position );
        //     },
        //     function(){ // fail cb
        //     }
        // );


    }

	$(document).ready(function(){
		$('#privacy_birthdate').selectpicker();
		$('#privacy_birthyear').selectpicker();
	})
<?php echo '</script'; ?>
>


<?php echo '<script'; ?>
>
$(document).ready(function(){
    $(".nav.nav-tabs li a").click(function(){
     $('.alert.alert-success.mb0.mt10.x-hidden').hide();
    });
	
	$('input[type=radio][name=theme_color]').change(function() {
		
		if(this.value == '')
		{
			return false;
		}
		var t_color = this.value;
		
		$('#save_changes').css({'background-color':t_color,'border-color':t_color});
		$('#btnSave').css({'background-color':t_color,'border-color':t_color});
		
		$('div.side_profile > ul > li > a > span').css({'color':t_color});
		
		$("div.side_profile > ul > li > a").hover(function() {
            $(this).css({'color':t_color});
        }).mouseout(function(){
              $(this).css({"color":''});
		});
		
		$('ul.side-nav > li.active > a').css({'color':t_color});
		
		$('ul.side-nav > li > a').hover(function() {
            $(this).css({'color':t_color});
        }).mouseout(function(){
              $(this).css({"color":''});
		});
		
		$('div.panel-heading').find('li.active > a > strong').css({'color':t_color});
		$('div.panel-heading').find('li.active > a > i').css({'color':t_color});
		
		$('div.profile-bg-thumb').css({'background-color':t_color});
		
		$('ul.side-nav > li.active > a').css({'background-color':t_color,'color':'#fff'});
		
		$('ul.side-nav > li.active > a').hover(function() {
            $(this).css({'background-color':t_color,'color':'#fff'});
        }).mouseout(function(){
              $(this).css({'background-color':''});
		});
		
		$('ul.side-nav').css({'background':t_color+"1a"});
		
		$('.side-nav a').css({'color':t_color});
		$('.side-nav>li.active>a').css({'color':'#fff'});
		$('.side-nav a:hover').css({'color':'#fff','background-color':t_color});
		$('.panel-heading.with-nav .nav>li.active>a').css({'border-bottom-color':t_color});
		$('.navbar-container span.text-link.dev').css({'background-color':t_color,'border-color':t_color});
		
		$('.inner_link li a:hover').css({'color':t_color,'border-color':t_color});
		$('.dropdown-menu>li>a:hover').css({'color':'#fff','background-color':t_color});
		$('.dropdown-menu>li>a:focus').css({'color':'#fff','background-color':t_color});
		$('.js_user-popover a:hover').css({'color':t_color});
		$('.data-content .name a:hover').css({'color':t_color});
		$('a.follow_btn').css({'border-color':t_color,'color':t_color});
		$('a.follow_btn:hover').css({'background-color':t_color+"1a"});
		
		$('.btn-primary').css({'background-color':t_color})
		$('.main-header').css({'cursor':'default','pointer-events':'none'});
		$('.main-header .header-container').css({'opacity':'0.3'});
		$('.site_bar_new').css({'cursor':'default','pointer-events':'none'});
		$('.side-nav').css({'cursor':'default','pointer-events':'none'});
		$('.panel-heading.with-nav .nav>li.active>a').css({'cursor':'default','pointer-events':'none'});
		$('#cancle_changes').show();
		
		
		
	});
	
	$("#otherColor").keyup(function(){
		
        var t_color = "#"+this.value;
		
		$('#save_changes').css({'background-color':t_color,'border-color':t_color});
		$('#btnSave').css({'background-color':t_color,'border-color':t_color});
		
		$('div.side_profile > ul > li > a > span').css({'color':t_color});
		
		$("div.side_profile > ul > li > a").hover(function() {
            $(this).css({'color':t_color});
        }).mouseout(function(){
              $(this).css({"color":''});
		});
		
		$('ul.side-nav > li.active > a').css({'color':t_color});
		
		$('ul.side-nav > li > a').hover(function() {
            $(this).css({'color':t_color});
        }).mouseout(function(){
              $(this).css({"color":''});
		});
		
		$('div.panel-heading').find('li.active > a > strong').css({'color':t_color});
		$('div.panel-heading').find('li.active > a > i').css({'color':t_color});
		
		$('div.profile-bg-thumb').css({'background-color':t_color});
		
		$('ul.side-nav > li.active > a').css({'background-color':t_color,'color':'#fff'});
		
		$('ul.side-nav > li.active > a').hover(function() {
            $(this).css({'background-color':t_color,'color':'#fff'});
        }).mouseout(function(){
              $(this).css({'background-color':''});
		});
		
		$('ul.side-nav').css({'background':t_color+"1a"});
		
		$('.side-nav a').css({'color':t_color});
		$('.side-nav>li.active>a').css({'color':'#fff'});
		$('.side-nav a:hover').css({'color':'#fff','background-color':t_color});
		$('.panel-heading.with-nav .nav>li.active>a').css({'border-bottom-color':t_color});
		$('.navbar-container span.text-link.dev').css({'background-color':t_color,'border-color':t_color});
		
		$('.inner_link li a:hover').css({'color':t_color,'border-color':t_color});
		$('.dropdown-menu>li>a:hover').css({'color':'#fff','background-color':t_color});
		$('.dropdown-menu>li>a:focus').css({'color':'#fff','background-color':t_color});
		$('.js_user-popover a:hover').css({'color':t_color});
		$('.data-content .name a:hover').css({'color':t_color});
		$('a.follow_btn').css({'border-color':t_color,'color':t_color});
		$('a.follow_btn:hover').css({'background-color':t_color+"1a"})
		
		$('.btn-primary').css({'background-color':t_color})
		$('.main-header').css({'cursor':'default','pointer-events':'none'});
		$('.main-header .header-container').css({'opacity':'0.3'});
		$('.site_bar_new').css({'cursor':'default','pointer-events':'none'});
		$('.side-nav').css({'cursor':'default','pointer-events':'none'});
		$('.panel-heading.with-nav .nav>li.active>a').css({'cursor':'default','pointer-events':'none'});
		$('#cancle_changes').show();
    });
	
	//$('#otherColor').colorpicker();
	
	 $('#otherColor').colorpicker({'format':'hex'}).on('changeColor', function (e) {
		 
		   var t_color = e.color.toHex();
		
		$('#save_changes').css({'background-color':t_color,'border-color':t_color});
		$('#btnSave').css({'background-color':t_color,'border-color':t_color});
		
		$('div.side_profile > ul > li > a > span').css({'color':t_color});
		
		$("div.side_profile > ul > li > a").hover(function() {
            $(this).css({'color':t_color});
        }).mouseout(function(){
              $(this).css({"color":''});
		});
		
		$('ul.side-nav > li.active > a').css({'color':t_color});
		
		$('ul.side-nav > li > a').hover(function() {
            $(this).css({'color':t_color});
        }).mouseout(function(){
              $(this).css({"color":''});
		});
		
		$('div.panel-heading').find('li.active > a > strong').css({'color':t_color});
		$('div.panel-heading').find('li.active > a > i').css({'color':t_color});
		
		$('div.profile-bg-thumb').css({'background-color':t_color});
		
		$('ul.side-nav > li.active > a').css({'background-color':t_color,'color':'#fff'});
		
		$('ul.side-nav > li.active > a').hover(function() {
            $(this).css({'background-color':t_color,'color':'#fff'});
        }).mouseout(function(){
              $(this).css({'background-color':''});
		});
		
		$('ul.side-nav').css({'background':t_color+"1a"});
		
		$('.side-nav a').css({'color':t_color});
		$('.side-nav>li.active>a').css({'color':'#fff'});
		$('.side-nav a:hover').css({'color':'#fff','background-color':t_color});
		$('.panel-heading.with-nav .nav>li.active>a').css({'border-bottom-color':t_color});
		$('.navbar-container span.text-link.dev').css({'background-color':t_color,'border-color':t_color});
		
		$('.inner_link li a:hover').css({'color':t_color,'border-color':t_color});
		$('.dropdown-menu>li>a:hover').css({'color':'#fff','background-color':t_color});
		$('.dropdown-menu>li>a:focus').css({'color':'#fff','background-color':t_color});
		$('.js_user-popover a:hover').css({'color':t_color});
		$('.data-content .name a:hover').css({'color':t_color});
		$('a.follow_btn').css({'border-color':t_color,'color':t_color});
		$('a.follow_btn:hover').css({'background-color':t_color+"1a"})
		
		$('.btn-primary').css({'background-color':t_color})
		$('.main-header').css({'cursor':'default','pointer-events':'none'});
		$('.main-header .header-container').css({'opacity':'0.3'});
		$('.site_bar_new').css({'cursor':'default','pointer-events':'none'});
		$('.side-nav').css({'cursor':'default','pointer-events':'none'});
		$('.panel-heading.with-nav .nav>li.active>a').css({'cursor':'default','pointer-events':'none'});
		$('#cancle_changes').show();
		 
		
	  });
	  $('#cancle_changes').click(function(){
		  
		$('input[type=radio][name=theme_color]').each(function () { $(this).prop('checked', false); })
		
		$('#save_changes').removeAttr('style');
		$('#btnSave').removeAttr('style');
		
		$('div.side_profile > ul > li > a > span').removeAttr('style');
		
		$("div.side_profile > ul > li > a").hover(function() {
            $(this).css({'color':t_color});
        }).mouseout(function(){
              $(this).css({"color":''});
		});
		
		$('ul.side-nav > li.active > a').removeAttr('style');
		
		$('ul.side-nav > li > a').hover(function() {
            $(this).css({'color':t_color});
        }).mouseout(function(){
              $(this).css({"color":''});
		});
		
		$('div.panel-heading').find('li.active > a > strong').removeAttr('style');
		$('div.panel-heading').find('li.active > a > i').removeAttr('style');
		
		$('div.profile-bg-thumb').removeAttr('style');
		
		$('ul.side-nav > li.active > a').removeAttr('style');
		
		$('ul.side-nav > li.active > a').hover(function() {
            $(this).css({'background-color':t_color,'color':'#fff'});
        }).mouseout(function(){
              $(this).css({'background-color':''});
		});
		
		$('ul.side-nav').removeAttr('style');
		
		$('.side-nav a').removeAttr('style');
		$('.side-nav>li.active>a').removeAttr('style');
		$('.side-nav a:hover').removeAttr('style');
		$('.panel-heading.with-nav .nav>li.active>a').removeAttr('style');
		$('.navbar-container span.text-link.dev').removeAttr('style');
		
		$('.inner_link li a:hover').removeAttr('style');
		$('.dropdown-menu>li>a:hover').removeAttr('style');
		$('.dropdown-menu>li>a:focus').removeAttr('style');
		$('.js_user-popover a:hover').removeAttr('style');
		$('.data-content .name a:hover').removeAttr('style');
		$('a.follow_btn').removeAttr('style');
		$('a.follow_btn:hover').removeAttr('style');
		
		$('.btn-primary').removeAttr('style');
		  
		  
		  $(this).hide();
		  $('.main-header').removeAttr('style');
		$('.main-header .header-container').removeAttr('style');
		$('.site_bar_new').removeAttr('style');
		$('.side-nav').removeAttr('style');
		$('.panel-heading.with-nav .nav>li.active>a').removeAttr('style');
		$('.color-box').attr('checked', false);
	  });
	  
	$("#default_theme").click(function(){
		$('input[type=radio][name=theme_color]').each(function () { $(this).prop('checked', false); })
		$("#save_changes").click();		
	});
});   		
<?php echo '</script'; ?>
>


<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
