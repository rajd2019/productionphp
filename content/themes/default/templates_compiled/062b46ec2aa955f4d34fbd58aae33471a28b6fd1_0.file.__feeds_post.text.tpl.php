<?php
/* Smarty version 3.1.31, created on 2019-07-31 16:11:52
  from "E:\wamp64\www\guo-production\content\themes\default\templates\__feeds_post.text.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5d41bdc8075b58_77500588',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '062b46ec2aa955f4d34fbd58aae33471a28b6fd1' => 
    array (
      0 => 'E:\\wamp64\\www\\guo-production\\content\\themes\\default\\templates\\__feeds_post.text.tpl',
      1 => 1564370770,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d41bdc8075b58_77500588 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style>
.post-replace .post-text{
border-bottom:0px !important;
line-height:normal;
white-space: normal;
}
.post-replace .post-text p {
    margin-bottom: 0px !important;
    line-height: 22px !important;
}
.btn-translate {
    ffloat: none;
	display: block !important;
	margin: 0 0 10px;
	text-align: right;
}
a[data-readmore-toggle="rmjs-3"] {
    position: absolute;
}
.btn-translate span:hover {
    color: #2a78c2;
    text-decoration: underline;
}
.btn-translate span {
    color: #657786;
    text-decoration: none;
}
span.trans_by {
    font-size: 12px;
    margin: 9px 0 0;
    display: block;
    color: #000;
	font-family: 'Segoe UI',SegoeUI,"Helvetica Neue",Helvetica,Arial,sans-serif;
    font-weight: bold;
}
span.trans_by img {
    width: 14px;
    margin: -4px 0 0;
}
</style>
<div class="post-replace post-listing" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" >

    <?php if ($_smarty_tpl->tpl_vars['_post']->value['broadcast_name'] != '') {?>
    <div class="post-text js_readmore" dir="auto"><?php echo $_smarty_tpl->tpl_vars['_post']->value['broadcast_name'];?>
</div>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['_post']->value['post_id'] == 175551) {?>
	<div class="post-text js_readmore rrr" dir="auto" data-id="">亲爱的支持者们：法治基金官网更多功能正在上线中，目前借助&ldquo;郭媒体置顶&rdquo;及时发布信息。一切请以法治基金官网（英文版）、&ldquo;郭媒体置顶&rdquo;协助发布的信息为准，感谢关注！<br /><br />❤ 法制基金官网如下：<br /><a target='_blank' href="https://rolsociety.org\">https://rolsociety.org</a><br /><a target='_blank' href="https://rolfoundation.org\">https://rolfoundation.org</a><br /><br />❤ 法治基金接受捐款的PayPal账户两个如下：<br /><a target='_blank' href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GY3YZQTU5YRJS&source=url">https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GY3YZQTU5YRJS&source=url</a><br /><br /><a target='_blank' href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=WMKKVYANPHXU4&source=url">https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=WMKKVYANPHXU4&source=url</a><br />❤ 法制基金接受捐款的银行账户信息两个如图！</div> 
	<?php } else { ?>
    <div class="post-text js_readmore rrr" dir="auto" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"><?php echo htmlspecialchars_decode($_smarty_tpl->tpl_vars['post']->value['text_plain'], ENT_QUOTES);?>
</div>
    <div class="post-text-plain hidden" id="hidden-post-<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['post']->value['text_plain'];?>
</div>
	<?php }?>

</div>

<?php if ($_smarty_tpl->tpl_vars['is_ajax_post']->value != 'Yes') {?>
<div class="btn-translate" data-id-translate="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"><i class="fa fa-globe" aria-hidden="true"></i> <span onclick="TranslateText(<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
)"><?php echo __("Translate");?>
</span></div>
<div class="text-translate" style="display:none;" id="t-translate-<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
	<div class="post-media">
		<div class="post-media-meta">
		<!-- Start Translate Text Via Microsoft Translate API -->
			<div class=".post-text" id="translated-text-<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" style="text-align:justify;"></div>
		<!-- End Translate Text Via Microsoft Translate API -->
		
		<div class="loader loader_middium" id="translate-loader-<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" style="display:none;"></div>
		</div>
	</div>
</div>
<input type="hidden" id="translate-toggle-val-<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" value="0">
<?php }
}
}
