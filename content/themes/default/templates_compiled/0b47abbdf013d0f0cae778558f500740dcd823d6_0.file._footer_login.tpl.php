<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:12:15
  from "/var/app/current/content/themes/default/templates/_footer_login.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c58726f9028c8_04396880',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0b47abbdf013d0f0cae778558f500740dcd823d6' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/_footer_login.tpl',
      1 => 1536745022,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_ads.tpl' => 1,
    'file:_js_files.tpl' => 1,
    'file:_js_templates.tpl' => 1,
  ),
),false)) {
function content_5c58726f9028c8_04396880 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- ads -->
<?php $_smarty_tpl->_subTemplateRender('file:_ads.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_ads'=>$_smarty_tpl->tpl_vars['ads_master']->value['footer'],'_master'=>true), 0, false);
?>

<!-- ads -->
<!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] -->
<link rel="stylesheet" href="/includes/assets/css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/includes/assets/css/twemoji-awesome/twemoji-awesome.min.css">
<link rel="stylesheet" href="/includes/assets/css/flag-icon/css/flag-icon.min.css">
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/includes/assets/js/jquery/jquery-3.2.1.min.js" ><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/includes/assets/js/jquery/jquery-3.2.1.min.js" ><?php echo '</script'; ?>
>
<!-- JS Files -->
<?php $_smarty_tpl->_subTemplateRender('file:_js_files.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- JS Templates -->
<?php $_smarty_tpl->_subTemplateRender('file:_js_templates.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- JS Templates -->
<?php echo '<script'; ?>
 type="text/javascript">
$(document).ready(function() {
	$("#signup_show").click(function() {
	  $(".twitter-main").addClass("signup-block");
	});
	$("#back").click(function() {
	  $(".twitter-main").removeClass("signup-block");
	});
});
<?php echo '</script'; ?>
>
<!-- Analytics Code -->
<?php if ($_smarty_tpl->tpl_vars['system']->value['analytics_code']) {
echo html_entity_decode($_smarty_tpl->tpl_vars['system']->value['analytics_code'],ENT_QUOTES);
}?>
<!-- Analytics Code -->
</body>
</html><?php }
}
