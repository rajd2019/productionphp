<?php
/* Smarty version 3.1.31, created on 2019-07-31 16:11:52
  from "E:\wamp64\www\guo-production\content\themes\default\templates\_js_files.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5d41bdc87200e8_45414018',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'beb4d7aaf740081f1c4d41555a9b2f131d88241e' => 
    array (
      0 => 'E:\\wamp64\\www\\guo-production\\content\\themes\\default\\templates\\_js_files.tpl',
      1 => 1564370770,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d41bdc87200e8_45414018 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- HTML5 shim, for IE6-8 support of HTML5 elements --><!--[if lt IE 9]><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/html5shiv/html5shiv.min.js"><?php echo '</script'; ?>
><![endif]--><!-- Initialize --><?php echo '<script'; ?>
 type="text/javascript">/* initialize vars */var site_title = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
";var site_path = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
";var ajax_path = site_path+"/includes/ajax/";var uploads_path = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
";var current_page = "<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
";var secret = "<?php echo $_smarty_tpl->tpl_vars['secret']->value;?>
";var min_data_heartbeat = "<?php echo $_smarty_tpl->tpl_vars['system']->value['data_heartbeat']*1000;?>
";var min_chat_heartbeat = "<?php echo $_smarty_tpl->tpl_vars['system']->value['chat_heartbeat']*1000;?>
";var chat_enabled = <?php if ($_smarty_tpl->tpl_vars['system']->value['chat_enabled']) {?>true<?php } else { ?>false<?php }?>;var geolocation_enabled = <?php if ($_smarty_tpl->tpl_vars['system']->value['geolocation_enabled']) {?>true<?php } else { ?>false<?php }?>;var daytime_msg_enabled = <?php if ($_smarty_tpl->tpl_vars['daytime_msg_enabled']->value) {?>true<?php } else { ?>false<?php }?>;var notifications_sound = <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['notifications_sound']) {?>true<?php } else { ?>false<?php }?>;var currency = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_currency'];?>
";var stripe_key = <?php if ($_smarty_tpl->tpl_vars['system']->value['stripe_mode'] == "live") {?>"<?php echo $_smarty_tpl->tpl_vars['system']->value['stripe_live_publishable'];?>
"<?php } else { ?>"<?php echo $_smarty_tpl->tpl_vars['system']->value['stripe_test_publishable'];?>
"<?php }
echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript">/* i18n for JS */var __ = [];__["Describe your item (optional)"] = "<?php echo __('Describe your item (optional)');?>
";__["Ask something"] = "<?php echo __('Ask something');?>
";__["Verification Requset"] = "<?php echo __('Verification Requset');?>
";__["Add Friend"] = "<?php echo __('Add Friend');?>
";__["Friends"] = "<?php echo __('Friends');?>
";__["Friend Request Sent"] = "<?php echo __('Friend Request Sent');?>
";__["Following"] = "<?php echo __('Following');?>
";__["Follow"] = "<?php echo __('Follow');?>
";__["Pending"] = "<?php echo __('Pending');?>
";__["Remove"] = "<?php echo __('Remove');?>
";__["Error"] = "<?php echo __('Error');?>
";__["Success"] = "<?php echo __('Success');?>
";__["Loading"] = "<?php echo __('Loading');?>
";__["Like"] = "<?php echo __('Like');?>
";__["Unlike"] = "<?php echo __('Unlike');?>
";__["Joined"] = "<?php echo __('Joined');?>
";__["Join"] = "<?php echo __('Join');?>
";__["Going"] = "<?php echo __('Going');?>
";__["Interested"] = "<?php echo __('Interested');?>
";__["Delete"] = "<?php echo __('Delete');?>
";__["Delete Cover"] = "<?php echo __('Delete Cover');?>
";__["Delete Picture"] = "<?php echo __('Delete Picture');?>
";__["Delete Post"] = "<?php echo __('Delete Post');?>
";__["Close comment for this post"] = "<?php echo __('Close comment for this post');?>
";__["Open comment for this post"] = "<?php echo __('Open comment for this post');?>
";__["Are you sure you want to disable comments for this post?"] = "<?php echo __('Are you sure you want to disable comments for this post?');?>
";__["Are you sure you want to enable comments for this post?"] = "<?php echo __('Are you sure you want to enable comments for this post?');?>
";__["Delete Comment"] = "<?php echo __('Delete Comment');?>
";__["Delete Conversation"] = "<?php echo __('Delete Conversation');?>
";__["Share Post"] = "<?php echo __('Share Post');?>
";__["Report"] = "<?php echo __('Report');?>
";__["Block User"] = "<?php echo __('Block User');?>
";__["Unblock User"] = "<?php echo __('Unblock User');?>
";__["Mark as Available"] = "<?php echo __('Mark as Available');?>
";__["Mark as Sold"] = "<?php echo __('Mark as Sold');?>
";__["Save Post"] = "<?php echo __('Save Post');?>
";__["Unsave Post"] = "<?php echo __('Unsave Post');?>
";__["Boost Post"] = "<?php echo __('Boost Post');?>
";__["Unboost Post"] = "<?php echo __('Unboost Post');?>
";__["Pin Post"] = "<?php echo __('Pin Post');?>
";__["Unpin Post"] = "<?php echo __('Unpin Post');?>
";__["Verify"] = "<?php echo __('Verify');?>
";__["Decline"] = "<?php echo __('Decline');?>
";__["Boost"] = "<?php echo __('Boost');?>
";__["Unboost"] = "<?php echo __('Unboost');?>
";__["Mark as Paid"] = "<?php echo __('Mark as Paid');?>
";__["Read more"] = "<?php echo __('Read more');?>
";__["Read less"] = "<?php echo __('Read less');?>
";__["Monthly Average"] = "<?php echo __('Monthly Average');?>
";__["Jan"] = "<?php echo __('Jan');?>
";__["Feb"] = "<?php echo __('Feb');?>
";__["Mar"] = "<?php echo __('Mar');?>
";__["Apr"] = "<?php echo __('Apr');?>
";__["May"] = "<?php echo __('May');?>
";__["Jun"] = "<?php echo __('Jun');?>
";__["Jul"] = "<?php echo __('Jul');?>
";__["Aug"] = "<?php echo __('Aug');?>
";__["Sep"] = "<?php echo __('Sep');?>
";__["Oct"] = "<?php echo __('Oct');?>
";__["Nov"] = "<?php echo __('Nov');?>
";__["Dec"] = "<?php echo __('Dec');?>
";__["Users"] = "<?php echo __('Users');?>
";__["Pages"] = "<?php echo __('Pages');?>
";__["Groups"] = "<?php echo __('Groups');?>
";__["Events"] = "<?php echo __('Events');?>
";__["Posts"] = "<?php echo __('Posts');?>
";__["Are you sure you want to delete this?"] = "<?php echo __('Are you sure you want to delete this?');?>
";__["Are you sure you want to remove your cover photo?"] = "<?php echo __('Are you sure you want to remove your cover photo?');?>
";__["Are you sure you want to remove your profile picture?"] = "<?php echo __('Are you sure you want to remove your profile picture?');?>
";__["Are you sure you want to delete this post?"] = "<?php echo __('Are you sure you want to delete this post?');?>
";__["Are you sure you want to share this post?"] = "<?php echo __('Are you sure you want to share this post?');?>
";__["Are you sure you want to delete this comment?"] = "<?php echo __('Are you sure you want to delete this comment?');?>
";__["Are you sure you want to delete this conversation?"] = "<?php echo __('Are you sure you want to delete this conversation?');?>
";__["Are you sure you want to report this?"] = "<?php echo __('Are you sure you want to report this?');?>
";__["Are you sure you want to block this user?"] = "<?php echo __('Are you sure you want to block this user?');?>
";__["Are you sure you want to unblock this user?"] = "<?php echo __('Are you sure you want to unblock this user?');?>
";__["Are you sure you want to delete your account?"] = "<?php echo __('Are you sure you want to delete your account?');?>
";__["Are you sure you want to verify this request?"] = "<?php echo __('Are you sure you want to verify this request?');?>
";__["Are you sure you want to decline this request?"] = "<?php echo __('Are you sure you want to decline this request?');?>
";__["Are you sure you want to approve this request?"] = "<?php echo __('Are you sure you want to approve this request?');?>
";__["There is something that went wrong!"] = "<?php echo __('There is something that went wrong!');?>
";__["There is no more data to show"] = "<?php echo __('There is no more data to show');?>
";__["This has been shared to your Timeline"] = "<?php echo __('This has been shared to your Timeline');?>
";__["Mute User"] = "<?php echo __('Mute User');?>
";__["Are you sure you want to mute this user?"] = "<?php echo __('Are you sure you want to mute this user?');?>
";__["Unmute User"] = "<?php echo __('Unmute User');?>
";__["Are you sure you want to unmute this user?"] = "<?php echo __('Are you sure you want to unmute this user?');?>
";<?php echo '</script'; ?>
><!-- Initialize --><!-- Dependencies Libs [jQuery|Bootstrap|Mustache] -->
<?php echo '<script'; ?>
>

$("document").ready(function(){	
   // alert('h2266');
});

<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/bootstrap/bootstrap.min.js" <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>defer<?php }?>><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/mustache/mustache.min.js" <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>defer<?php }?>><?php echo '</script'; ?>
><link rel="stylesheet" type="text/css" href="content/themes/default/plugins/jquery-emoji-picker-master/css/jquery.emojipicker.css"><?php echo '<script'; ?>
 type="text/javascript" src="content/themes/default/plugins/jquery-emoji-picker-master/js/jquery.emojipicker.js"><?php echo '</script'; ?>
><!-- Emoji Data --><link rel="stylesheet" type="text/css" href="content/themes/default/plugins/jquery-emoji-picker-master/css/jquery.emojipicker.a.css"><?php echo '<script'; ?>
 type="text/javascript" src="content/themes/default/plugins/jquery-emoji-picker-master/js/jquery.emojis.js"><?php echo '</script'; ?>
><!-- Dependencies Libs [jQuery|Bootstrap|Mustache] --><?php echo '<script'; ?>
 src=" https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.10/clipboard.min.js"><?php echo '</script'; ?>
><!-- Dependencies Plugins --><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/fastclick/fastclick.min.js" <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>defer<?php }?>><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/jquery.form/jquery.form.min.js" <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>defer<?php }?>><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/jquery.inview/jquery.inview.min.js" <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>defer<?php }?>><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/jquery.slimscroll/jquery.slimscroll.min.js" <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>defer<?php }?>><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/autosize/autosize.min.js" <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>defer<?php }?>><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/readmore/readmore.min.js" <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>defer<?php }?>><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/moment/moment-with-locales.min.js" <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>defer<?php }?>><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/mediaelementplayer/mediaelement-and-player.min.js" <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>defer<?php }?>><?php echo '</script'; ?>
><link rel="stylesheet" type='text/css' href="/includes/assets/js/plugins/mediaelementplayer/mediaelementplayer.min.css"><link rel="stylesheet" type='text/css' href="/includes/assets/js/plugins/mediaelementplayer/circle.css"><?php echo '<script'; ?>
 src="/includes/assets/js/lazyload.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
>
        $(function() {
            $("img.lazy").lazyload();
        });

    <?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/fastclick/app.js"><?php echo '</script'; ?>
><?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {
if ($_smarty_tpl->tpl_vars['system']->value['geolocation_enabled']) {
echo '<script'; ?>
 src="/includes/assets/js/plugins/jquery.geocomplete/jquery.geocomplete.min.js"><?php echo '</script'; ?>
><?php }
echo '<script'; ?>
 src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyA9cc1G9_RYf7ww7k3QkEWO_xqAv6x9okk"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="/includes/assets/js/jquery/map.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/bootstrap.select/bootstrap-select.min.js"><?php echo '</script'; ?>
><link rel="stylesheet" type='text/css' href="/includes/assets/js/plugins/bootstrap.select/bootstrap-select.min.css"><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/bootstrap.datetimepicker/bootstrap-datetimepicker.min.js"><?php echo '</script'; ?>
><link rel="stylesheet" type='text/css' href="/includes/assets/js/plugins/bootstrap.datetimepicker/bootstrap-datetimepicker.min.css"><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/noty/noty.min.js"><?php echo '</script'; ?>
><link rel="stylesheet" type='text/css' href="/includes/assets/js/plugins/noty/noty.css"><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/magnific-popup/magnific-popup.min.js"><?php echo '</script'; ?>
><link rel="stylesheet" type='text/css' href="/includes/assets/js/plugins/magnific-popup/magnific-popup.css"><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/tinymce4/tinymce.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/jquery.cookie/jquery.cookie.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="https://checkout.stripe.com/checkout.js"><?php echo '</script'; ?>
><?php }
if ($_smarty_tpl->tpl_vars['system']->value['reCAPTCHA_enabled']) {
echo '<script'; ?>
 src='https://www.google.com/recaptcha/api.js' <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>defer<?php }?>><?php echo '</script'; ?>
><?php }?><!-- Dependencies Plugins --><!-- Sngine [JS] --><?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {
echo '<script'; ?>
 src="/includes/assets/js/sngine/jquery.bxslider.js"><?php echo '</script'; ?>
><?php }
echo '<script'; ?>
 src="/includes/assets/js/sngine/core.js?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"  <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>defer<?php }?>><?php echo '</script'; ?>
><?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?><style>.custom_poll {width: 75%;position:relative;}.remove_all_poll {width: 25%;float: right;padding: 0;text-align: right;}.remove_poll {background: none !important;color: #5a656b !important;border: none;padding-right: 0 !important;}.remove_poll:hover {background: none !important;color: #5a656b !important;}.single_remove_poll:after {position: absolute;content: "\f00d";font-family: FontAwesome;font-size: 17px;top: -5px;color: #ccc;}.single_remove_poll:hover {background: none !important;color: #ccc;}.single_remove_poll {background: none;border: none;position: absolute;top: 10px;}.remove_poll:active {background: none !important;}button.btn.btn-primary.single_remove_poll:active {background: none !important;}.remove_poll.btn-primary[disabled]:hover, .btn-primary[disabled]:focus, .remove_poll.btn-primary:active:focus {border-color: none !important;background: none !important;color: #5a656b !important;}</style><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/francium-voice/src/recorder.js?v=1511974578"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/francium-voice/src/Fr.voice.js?v=1511974578"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/sngine/user.js?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/sngine/pro-post.js?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/sngine/chat.js?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"><?php echo '</script'; ?>
><link rel="stylesheet" href="/includes/assets/js/plugins/textarea-emoji/docs/assets/css/reset.css?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"><link rel="stylesheet" href="/includes/assets/js/plugins/textarea-emoji/docs/assets/css/style.css?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"><link rel="stylesheet" href="/includes/assets/js/plugins/textarea-emoji/docs/stylesheet.css?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/textarea-emoji/docs/assets/js/jquery.emojiarea.js?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"><?php echo '</script'; ?>
><link rel="stylesheet" href="/includes/assets/js/plugins/lightGallery-master/dist/css/lightgallery.css"><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/lightGallery-master/lib/jquery.mousewheel.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/lightGallery-master/modules/lg-zoom.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/lightGallery-master/dist/js/lightgallery.js?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"><?php echo '</script'; ?>
><link href="/includes/assets/js/plugins/new-emoji/src/jquery.lsxemojipicker.css?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
" rel="stylesheet"><?php echo '<script'; ?>
 src="/includes/assets/js/sngine/twemoji.min.js?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/new-emoji/src/jquery.lsxemojipicker.js?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"><?php echo '</script'; ?>
><!--script src="/includes/assets/js/sngine/adapter-latest.js?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/sngine/image_upload.js?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/sngine/video_upload.js?v=<?php echo $_smarty_tpl->tpl_vars['system']->value['FileMicroTime'];?>
"><?php echo '</script'; ?>
--><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/bootstrap.colorpicker/bootstrap-colorpicker.min.js"><?php echo '</script'; ?>
><link rel="stylesheet" type='text/css' href="/includes/assets/js/plugins/bootstrap.colorpicker/bootstrap-colorpicker.min.css"><?php }?><!-- Sngine [JS] --><?php if ($_smarty_tpl->tpl_vars['page']->value == "admin") {?><!-- Dependencies Plugins --><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/bootstrap.colorpicker/bootstrap-colorpicker.min.js"><?php echo '</script'; ?>
><link rel="stylesheet" type='text/css' href="/includes/assets/js/plugins/bootstrap.colorpicker/bootstrap-colorpicker.min.css"><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/metisMenu/metisMenu.min.js"><?php echo '</script'; ?>
><link rel="stylesheet" type='text/css' href="/includes/assets/js/plugins/metisMenu/metisMenu.min.css"><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/dataTables/jquery.dataTables.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="/includes/assets/js/plugins/dataTables/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
><link rel="stylesheet" type='text/css' href="/includes/assets/js/plugins/dataTables/dataTables.bootstrap.min.css"><!-- Dependencies Plugins [JS] --><!-- Sngine [JS] --><?php echo '<script'; ?>
 src="/includes/assets/js/sngine/admin.js?v=1511974578"><?php echo '</script'; ?>
><!-- Sngine [JS] --><!-- Admin Charts --><?php echo '<script'; ?>
 src="https://code.highcharts.com/highcharts.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="https://code.highcharts.com/modules/exporting.js"><?php echo '</script'; ?>
><?php if ($_smarty_tpl->tpl_vars['view']->value == "dashboard") {
echo '<script'; ?>
>$(function () {Highcharts.setOptions();$('#admin-chart-dashboard').highcharts({chart: {type: 'column'},title: {text: __["Monthly Average"]},xAxis: {categories: [__["Jan"],__["Feb"],__["Mar"],__["Apr"],__["May"],__["Jun"],__["Jul"],__["Aug"],__["Sep"],__["Oct"],__["Nov"],__["Dec"]],crosshair: true},yAxis: {min: 0,title: {text: "<?php echo date('Y');?>
"}},tooltip: {headerFormat: '<span style="font-size:10px">{point.key}</span><table>',pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +'<td style="padding:0"><b>{point.y}</b></td></tr>',footerFormat: '</table>',shared: true,useHTML: true},plotOptions: {column: {pointPadding: 0.2,borderWidth: 0}},series: [{name: __["Users"],data: [<?php echo implode(',',$_smarty_tpl->tpl_vars['chart']->value['users']);?>
]}, {name: __["Pages"],data: [<?php echo implode(',',$_smarty_tpl->tpl_vars['chart']->value['pages']);?>
]}, {name: __["Groups"],data: [<?php echo implode(',',$_smarty_tpl->tpl_vars['chart']->value['groups']);?>
]}, {name: __["Events"],data: [<?php echo implode(',',$_smarty_tpl->tpl_vars['chart']->value['events']);?>
]}, {name: __["Posts"],data: [<?php echo implode(',',$_smarty_tpl->tpl_vars['chart']->value['posts']);?>
]}]});});<?php echo '</script'; ?>
><?php }
if ($_smarty_tpl->tpl_vars['view']->value == "packages" && $_smarty_tpl->tpl_vars['sub_view']->value == "earnings") {
echo '<script'; ?>
>$(function () {Highcharts.setOptions();$('#admin-chart-earnings').highcharts({chart: {type: 'column'},title: {text: __["Monthly Average"]},xAxis: {categories: [__["Jan"],__["Feb"],__["Mar"],__["Apr"],__["May"],__["Jun"],__["Jul"],__["Aug"],__["Sep"],__["Oct"],__["Nov"],__["Dec"]],crosshair: true},yAxis: {min: 0,title: {text: "<?php echo date('Y');?>
"}},tooltip: {headerFormat: '<span style="font-size:10px">{point.key}</span><table>',pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +'<td style="padding:0"><b>{point.y}</b></td></tr>',footerFormat: '</table>',shared: true,useHTML: true},plotOptions: {column: {pointPadding: 0.2,borderWidth: 0}},series: [<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'value', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>{name: "<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
",data: [<?php echo implode(',',$_smarty_tpl->tpl_vars['value']->value['months_sales']);?>
]},<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>
]});});<?php echo '</script'; ?>
><?php }?><!-- Admin Charts --><?php }?><!-- DayTime Messages --><?php if ($_smarty_tpl->tpl_vars['page']->value == "index" && $_smarty_tpl->tpl_vars['user']->value->_logged_in && $_smarty_tpl->tpl_vars['view']->value == '') {
echo '<script'; ?>
>
        $(function() {
            if(daytime_msg_enabled) {
                var now = new Date();
                var hours = now.getHours();
                if ( hours >= 5 && hours <= 11 ) {
                    $(render_template('#message-morning')).insertAfter('.publisher').fadeIn();
                } else if ( hours >= 12 && hours <= 18 ) {
                    $(render_template('#message-afternoon')).insertAfter('.publisher').fadeIn();
                } else if ( hours >= 19 || hours <= 4 ) {
                    $(render_template('#message-evening')).insertAfter('.publisher').fadeIn();
                }
            }
        });
    <?php echo '</script'; ?>
><?php }
echo '<script'; ?>
 type="text/javascript">
  $(function(){
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  });

tinymce.init({
      selector:'#postBoxPro',
      theme: 'modern',
      toolbar: false,
      menubar: false,
      branding: false,
      statusbar: false,
      forced_root_block : "",
      setup: function (ed) {
                ed.on('keyup', function (e) { 
                    var count = CountCharactersOfMsg('postBoxPro');
                    
                    if(count > 1000){
                        document.getElementById("post_count_char_innerpage").innerHTML = '<span style="color:red;">-'+count+'</span>'+'/1000';
                    }
                    else{
                        document.getElementById("post_count_char_innerpage").innerHTML = count+'/1000';
                    }
                    
                });
                
                /*ed.on('focus',function(){
                  $('iframe').contents().find('#imThePlaceholder').remove();
                });*/
                
                ed.on( 'keypress', function(e) {
                    var count = CountCharactersOfMsg('postBoxPro');
                    
                    if(count >= 1000){
                        document.getElementById("post_count_char_innerpage").innerHTML = '<span style="color:red;">-'+count+'</span>'+'/1000';
                        tinymce.dom.Event.cancel(e);
                    }
                } );
                
                
                setInterval(function(e) {
                  var count = CountCharactersOfMsg('postBoxPro');
                    
                    if(count >= 1000){
                        document.getElementById("post_count_char_innerpage").innerHTML = '<span style="color:red;">-'+count+'</span>'+'/1000';
                        tinymce.dom.Event.cancel(e);
                    }
                    
                    if(count > 1000){
                        document.getElementById("post_count_char_innerpage").innerHTML = '<span style="color:red;">-'+count+'</span>'+'/1000';
                    }
                    else{
                        document.getElementById("post_count_char_innerpage").innerHTML = count+'/1000';
                    }
                }, 1000);
            }
    });
  
  tinymce.init({
      selector:'#postBox',
      theme: 'modern',
	  toolbar: false,
	  menubar: false,
	  branding: false,
	  statusbar: false,
	  forced_root_block : "",
	  setup: function (ed) {
                ed.on('keyup', function (e) { 
                    var count = CountCharacters();
					
					if(count > 1000){
						document.getElementById("count_char").innerHTML = '<span style="color:red;">-'+count+'</span>'+'/1000';
					}
					else{
						document.getElementById("count_char").innerHTML = count+'/1000';
					}
                    
                });
				
				/*ed.on('focus',function(){
				  $('iframe').contents().find('#imThePlaceholder').remove();
				});*/
				
				ed.on( 'keypress', function(e) {
					var count = CountCharacters();
					
					if(count >= 1000){
						document.getElementById("count_char").innerHTML = '<span style="color:red;">-'+count+'</span>'+'/1000';
						tinymce.dom.Event.cancel(e);
					}
				} );
				
				
				setInterval(function(e) {
				  var count = CountCharacters();
					
					if(count >= 1000){
						document.getElementById("count_char").innerHTML = '<span style="color:red;">-'+count+'</span>'+'/1000';
						tinymce.dom.Event.cancel(e);
					}
					
					if(count > 1000){
						document.getElementById("count_char").innerHTML = '<span style="color:red;">-'+count+'</span>'+'/1000';
					}
					else{
						document.getElementById("count_char").innerHTML = count+'/1000';
					}
				}, 1000);
            }
    });	

  function CountCharactersOfMsg(divId=null) {
        var body = tinymce.get(divId).getBody();
        var content = tinymce.trim(body.innerText || body.textContent);
        return content.length;
    };

    function ValidateCharacterLengthOfMsg(divId=null) {
        var max = 20;
        var count = CountCharacters(divId);
        if (count > max) {
            alert("Maximum " + max + " characters allowed.")
            return false;
        }
        return;
    }
	
	function CountCharacters() {
        var body = tinymce.get("postBox").getBody();
        var content = tinymce.trim(body.innerText || body.textContent);
        return content.length;
    };
    function ValidateCharacterLength() {
        var max = 20;
        var count = CountCharacters();
        if (count > max) {
            alert("Maximum " + max + " characters allowed.")
            return false;
        }
        return;
    }
<?php echo '</script'; ?>
><!-- DayTime Messages --><!--- Admin Script--><?php echo '<script'; ?>
 src="/includes/assets/js/sngine/modrator.js"><?php echo '</script'; ?>
><!--- Admin Script--><?php }
}
