<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:45:53
  from "/var/app/current/content/themes/default/templates/static.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c587a5120abf6_58193946',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '589f5f2bf6420e559b71b4485ca92737d4b2f0d8' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/static.tpl',
      1 => 1536745022,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:_sidebar.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_5c587a5120abf6_58193946 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>




<!-- page content -->

<div class="page-title">

    <?php echo __("Terms");?>


</div>



<div class="container offcanvas">

    <div class="row">



	    <!-- side panel -->

	    <div class="col-xs-12 visible-xs-block offcanvas-sidebar mt20">

	        <?php $_smarty_tpl->_subTemplateRender('file:_sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	    </div>

	    <!-- side panel -->



	    <div class="col-xs-12 offcanvas-mainbar">

			<div class="row">

		        <div class="col-xs-10 col-xs-offset-1 text-readable ptb10">
				<?php if ($_smarty_tpl->tpl_vars['system']->value['language']['code'] == 'en_us') {?>
					<h3 class="text-info"> <?php echo __("Terms and Conditions of Service");?>
 </h3>
                     
                 	<p> <?php echo __("GUO.MEDIA and its sub-domains (collectively, Sites) are powered by China Golden Spring Group (Hong Kong) Limited (Company), which is based in Hong Kong.  The services provided therein shall only be available to users, registered or non-registered, posting, viewing and/or otherwise using the advertisements, promotional materials, views, comments and/or other information on the Sites (Users).  Access to and use of the contents and services provided on the Sites shall be subject to the Privacy Policy and the terms and conditions (which are set out below) (Terms and Conditions). By using the Sites and any other site accessed through the Sites, Users acknowledge and agree that the Privacy Policy and the Terms and Conditions set out below are binding upon them. If a User does not accept either or both of the Privacy Policy and the Terms and Conditions, please do not use the Sites. The Company may revise the Privacy Policy and the Terms and Conditions at any time without prior notice. Once posted on the Sites, the amended Privacy Policy and the Terms and Conditions shall apply to all Users.  Users are advised to visit this page periodically to review the latest Privacy Policy and the Terms and Conditions.  A User’s access to the Sites and the Services (as defined below) will be terminated upon his/her notice to the Company that any change is unacceptable; otherwise the continued use shall constitute acceptance of all changes and they shall be binding upon the User.");?>
 </p>
                    
                    <p><?php echo __("The terms User and Users herein refer to all individuals and/or entities accessing and/or using the Sites at any time for any reason or purpose.");?>
</p>
                    
                    <ol type="1"> 
                    	<li> 
                        <h3 class="text-info"><?php echo __("1. General Terms");?>
</h3> 
                        <ol type="1"> 
                        <li> <?php echo __("(a) The Company provides an online platform for individuals, companies and organizations as discussion forums and online platform for Users to post their comments and photos and any information, text, links, graphics, videos and ads (Services).");?>

                        </li> 
                        <li>
                         <?php echo __("(b) The Company is not a party to nor is it involved in any actual transaction between Users.");?>

                        </li> 
                        <li><?php echo __("(c) The Company is committed to protect the privacy of the Users. The Company uses the information of the Users according to the terms as described in the Privacy Policy.");?>

                        </li> 
                        </ol> 
                        </li>
                        
                        <li> 
                        <h3 class="text-info"><?php echo __("2. Prohibited Uses for all Users");?>
 </h3> 
                        <p><?php echo __("Users of the Sites, registered or non-registered, agree not to use any of the Sites for any of the following purposes which are expressly prohibited");?>
: -</p> 
                        <ol type="i"> 
                        <li><?php echo __("(a) All Users are prohibited from violating or attempting to violate the security of the Sites including, without limitation, accessing data not intended for them or logging into a server or account which they are not authorized to access, attempting to probe, scan or test the vulnerability of a system or network or attempting to breach security or authentication measures without proper authorization, attempting to interfere with service to any user, host or network or sending unsolicited e-mail. Violation of system or network security may result in civil and/or criminal liabilities.");?>

                        </li> 
                        <li><?php echo __("(b) A User shall not delete or revise any material or information posted by any other Users.");?>
</li> 
                        <li><?php echo __("(c) All Users shall not use the Sites (1) for uploading posting publishing transmitting distributing circulating or storing material in violation of any applicable law or regulation or (2) in any manner that will infringe the copyright trademark trade secrets or other intellectual property rights of others or violate the privacy or publicity or other personal rights of others or (3) in any manner that is harmful defamatory libelous obscene discriminatory harassing threatening abusive hateful or is otherwise offensive or objectionable.  In particular all Users shall not print download duplicate or otherwise copy or use any personally identifiable information about other Users (if any).  All unsolicited communications of any type to Users are strictly prohibited. ");?>
</li> 
                        <li><?php echo __("(d) Users shall not use the Sites if they do not have legal capacity to form legally binding contracts.");?>
</li> 
                        <li><?php echo __("(e) Users shall not post any advertisement or materials on the Sites which contains any false, inaccurate, misleading or libelous content or contains any computer viruses, trojan horses, worms, computed files or other materials that may interrupt, damage or limit the functionality of any computer software or hardware or telecommunication equipment. Also, the advertisement shall not be fraudulent or involve sale of illegal products.");?>
</li> 
                        <li><?php echo __("(f) Users shall not engage in spamming, including but not limited to any form of emailing, posting or messaging that is unsolicited.");?>
</li> </ol> 
                        </li>
                        
                        <li> <h3 class="text-info"><?php echo __("3. Acceptable uses of the Sites");?>
</h3> 
                        <ol type="1"> 
                        <li> <p><?php echo __("(a) Specific uses - User(s) posting advertisements, photos, contents, views, comments, messages and/or other information on the Sites ( Material(s)).");?>
</p> 
                        <p><?php echo __("Such User(s) posting the Materials shall hereinafter referred to as the Posting User(s).");?>
</p> <ol type="i"> 
                        <li><?php echo __("I. The Posting User agrees that he/she/it shall only use the Sites for lawful purposes and for enjoying the Services provided by the Sites.  The Company reserves the right to edit, reject, disapprove, erase and delete any Materials posted on the Sites as it sees appropriate. ");?>
</li> 
                        <li><?php echo __("II. In the event that the Posting User is an individual, he/she shall not post his/her HK ID card and/or passport number on the Sites. ");?>
</li> 
                        <li><?php echo __("III. Although the Company shall use its reasonable endeavors to restrict access to the database of the Posting Users’ personal data only to the personnel of the Company and/or its affiliates, the Company does not guarantee that other parties will not, without the Company and/or its affiliates’ consent, gain access to such database.  For the usage and protection of personal data provided by the Posting Users, please refer to the Privacy Policy.");?>
</li> 
                        <li><?php echo __("IV. Users who post Materials on the Sites shall be solely responsible for the Materials posted by them and/or any web pages linked to the Sites posted by them.  The Company reserves the right to edit, reject, erase, remove and delete any Materials and links to web pages as it sees appropriate.  The Company shall have the right to terminate any services to any Posting Users at its sole discretion.  If a User posts Materials on the Sites and subsequently deletes and/or removes the same, or that the Company deletes any and/or removes such posted Materials, such Materials will no longer be accessible by the User who posted the same via that User’s account; however, such deleted Materials may still persist and appear on any part of the Sites, and/or be used in any form by the Company and/or its affiliates.");?>
</li> 
                        <li><?php echo __("V. The Company reserves the right to request any User to cease using or to change his/her/its username immediately upon notice given to the relevant user without giving any reason as and when the Company deems appropriate to do so; if any User disagrees and refuses to abide by such request made by the Company, the Company may at any time and its sole discretion, deactivate that User’s account without prior notification to that User and without prejudice to all the Company’s other rights and remedies.");?>
</li> 
                        <li><?php echo __("VI. This paragraph shall only be applicable to Users posting advertisements, promotional and market materials (Advertiser(s))");?>
:-
              <?php echo __("Upon payment of a service fee to the Company or upon acceptance of any free trial promotion offer, Users will be entitled to use the Sites to post advertisements and promotional materials (subject to the Terms and Conditions).  The Company also reserves the right to change the service fee or institute new charges or fees to be paid by Advertisers for posting advertisements and promotional materials on the Sites, as it deems appropriate.  In the event that any Advertiser posting advertisements and promotional materials fails to pay the service fee or any other fees or charges due to the Company, the Company reserves the right to suspend or terminate that Advertiser’s user account, advertisements and links to web pages without prejudice to all its other rights and remedies. ");?>
</li> 
              </ol> 
              </li> 
              <li> <p><?php echo __("(b) Specific uses - User(s) viewing the Materials posted on the Sites (Viewer(s))");?>
</p> 
              <p><?php echo __("The Viewer agrees that he/she/it shall only use the Sites for lawful purposes and for enjoying the Services provided by the Sites.  The Viewer agrees that any personal data received from the Sites or the Company shall only be used for the purpose of identifying and/or locating advertisements or materials or any content therein or for the purpose of enjoying the Services provided by the Sites.  Any personal data which are irrelevant to the above purposes shall be disregarded and shall not be saved, stored, collected, processed, used, distributed, published, disclosed or transmitted in any way, including but not limited to for any commercial purpose.  The Viewer also agrees that any personal data collected from the Sites or the Company shall be promptly and properly deleted when the above purposes have lapsed or been achieved.");?>

              </p> 
              <p><?php echo __("The Company shall not be responsible or held liable in any way if any Users, in breach of the Terms and Conditions, whether in Hong Kong or elsewhere, use the other Users’ personal data, information or materials (whether obtained from the Sites or not) for any purpose.  All Users accept that all personal data, information or materials provided by them publicly on the Sites are voluntarily provided and are given entirely at their own risk.  The Company shall not bear the responsibility of protecting the personal data, information or materials so provided publicly on the Sites.");?>
</p> 
              </li> 
              </ol>
              </li>
              
              <li> 
              <h3 class="text-info"> <?php echo __("4. Content License");?>
</h3> 
              <p><?php echo __("By posting Material on the Sites, the User unconditionally grants the Company and/or its affiliates a non-exclusive, worldwide, irrevocable, royalty-free right to exercise the copyright, publicity and database rights (but no other rights) he/she/it has in the Materials in order that the Company and/or its affiliates can use, publish, host, display, promote, copy, download, forward, distribute, reproduce, transfer, edit, sell and re-use the Materials in any form and anywhere, with or without making any commercial gains or profits, and carry out the purposes set out in the Privacy Policy and herein.");?>
</p> 
              </li>
              
              <li> 
              <h3 class="text-info"><?php echo __("5. Intellectual Property Rights");?>
</h3> 
              <p><?php echo __("All contents of the Sites, including without limitation the text, images, information, comments, layout, database, graphics, photos, pictures, sounds or audio formats, software, brands and HTML are the intellectual properties of the Company or the Users (as the case may be) which are protected by copyright and trademark laws and may not be downloaded or otherwise duplicated without the express written permission of the Company or the Users (as the case may be).  Re-use of any of the foregoing is strictly prohibited and the Company reserves all rights.  Any use of any of such contents other than those permitted under the Terms and Conditions is strictly prohibited and the Company reserves all rights in this respect.  For the avoidance of doubt, any purported consent of any third parties on the use of the contents and materials mentioned under this Clause shall not exonerate the Users from the restrictions/prohibitions imposed hereunder in whatsoever manner.");?>
</p> 
              </li>
              
              <li> 
              <h3 class="text-info"><?php echo __("6. Contents");?>
</h3> 
              <p><?php echo __("Users acknowledge that the Company may not pre-screen or pre-approve certain content posted on the Sites or any content sent through the Sites.  In any event, the Company takes no responsibility whatsoever for the content on the Sites or any content sent through the Sites, or for any content lost and does not make any representations or warranties regarding the content or accuracy of any material on the Sites.");?>
 </p>
               <p><?php echo __("Any Materials posted on the Sites by the Users may be viewed by users of other web sites linked to the Sites and the Company is not responsible for any improper and/or illegal use by any user or third party from linked third party web sites of any data or materials posted on the Sites.  Links to third party web sites provided on the Sites are provided solely as a convenience to the Users and as internet navigation tools, and not in any way as an endorsement by the Company of the contents on such third party web sites.  Unless otherwise stated on the Sites, the Company has no control over or rights in such third party web sites and is not responsible for any contents on such third party web sites or any use of services provided by such third party web sites by the Users.  All Users acknowledge and agree that they are solely responsible for the form, content and accuracy of any Materials, web page or other information contained therein placed by them.  The Company is not responsible for the content of any third party web sites linked to the Sites and does not make any representations or warranties regarding the contents or accuracy of materials on such third party web sites. If any User accesses any linked third party web sites, he/she/it does so entirely at his/her/it own risk.");?>
</p> 
               <p><?php echo __("The Company shall have the right to remove any Materials posted on the Sites at its sole discretion without any compensation or recourse to the Posting Users if the Company considers at its sole discretion that such Users have breached or is likely to breach any law or the Terms and Conditions.  In the event that the Company decides to remove any paid advertisement for any reasons not relating to any breach of law or the provisions herein, the Company may, after deducting the fees charged for the period that the advertisement has been posted on the Sites, refund the remaining fees (if any) to the related Posting User.</p> <p>Users agree and consent that the Company and/or its affiliates may, subject to the terms of the Privacy Policy, use their personal data and/or other information provided to the Sites for purposes relating to the provision of Services and/or offered by the Company and marketing services and/or special events of the Company and/or its affiliates.");?>
</p>
                </li>
                
                <li> 
                <h3 class="text-info"><?php echo __("7. Responsibility");?>
</h3> 
                <p><?php echo __("The Company may not monitor the Sites at all times but reserves the right to do so. The Company does not warrant that any Materials or web page will be viewed by any specific number of Users or that it will be viewed by any specific User.  The Company shall not in anyway be considered an agent of any User with respect to any use of the Sites and shall not be responsible in any way for any direct or indirect damage or loss that may arise or result from the use of the Sites, for whatever reason made.  WHILST ENDEAVOURING TO PROVIDE QUALITY SERVICE TO ALL USERS, THE COMPANY DOES NOT WARRANT THAT THE SITES WILL OPERATE ERROR-FREE OR THAT THE SITES AND ITS SERVER ARE FREE OF VIRUSES OR OTHER HARMFUL MECHANISMS.  IF USE OF THE SITES OR THEIR CONTENTS RESULT IN THE NEED FOR SERVICING OR REPLACING EQUIPMENT OR DATA BY ANY USER, THE COMPANY SHALL NOT BE RESPONSIBLE FOR THOSE COSTS.  THE SITES AND THEIR CONTENTS ARE PROVIDED ON AN AS IS BASIS WITHOUT ANY WARRANTIES OF ANY KIND.  TO THE FULLEST EXTENT PERMITTED BY LAW, THE COMPANY DISCLAIMS ALL WARRANTIES, INCLUDING, WITHOUT PREJUDICE TO THE FOREGOING, ANY IN RESPECT OF MERCHANTABILITY, NON-INFRINGEMENT OF THIRD PARTY RIGHTS, FITNESS FOR PARTICULAR PURPOSE, OR ABOUT THE ACCURACY, RELIABILITY, COMPLETENESS OR TIMELINESS OF THE CONTENTS, SERVICES, TEXT, GRAPHICS AND LINKS OF THE SITES.");?>

                </p> 
                </li>
                
                <li> 
                <h3 class="text-info"><?php echo __("8. Own Risk");?>
</h3> 
                <p><strong><?php echo __("All USERS SHALL USE THE SITES AND ANY OTHER WEB SITES ACCESSED THROUGH IT, AT ENTIRELY THEIR OWN RISK");?>
</strong>.  
                <?php echo __("ALL Users are responsible for the consequences of their postings.  The Company does not represent or guarantee the truthfulness, accuracy or reliability of any Materials posted by the Posting Users or endorse any opinions expressed by the Posting Users. Any reliance by any User on advertisements and materials posted by the other Users will be at their own risk.  The Company reserves the right to expel any User and prevent his/her/its further access to the Sites, at any time for breaching this agreement or violating the law and also reserves the right to remove any Materials which is abusive, illegal, disruptive or inappropriate at the Company’s sole discretion.");?>
</p> 
                </li>
                
                <li> 
                <h3 class="text-info"><?php echo __("9. Indemnity");?>
</h3> 
                <p><?php echo __("All Users agree to indemnify, and hold harmless the Company, its affiliates, its officers, directors, employees, agents, partners, representatives, shareholders, servants, attorneys, predecessors, successors and assigns from and against any claims, actions, demands, liabilities, losses, damages, costs and expenses (including legal fees and litigation expenses on a full indemnity basis) arising from or resulting from their use of the Sites or their breach of the terms of this Agreement.  The Company will provide prompt notice of any such claim, suit or proceedings to the relevant User.");?>
</p>
                 </li>
                 
                 <li> 
                 <h3 class="text-info"><?php echo __("10. Limitation of the Service");?>
</h3> 
                 <p><?php echo __("The Company shall have the right to limit the use of the Services, including the period of time that Materials will be posted on the Sites, the size, placement and position of the Materials, email messages or any other contents which are transmitted by the Services.  The Company reserves the right, in its sole discretion, to edit, modify, erase, delete or remove any Materials posted on the Sites, for any reason, without giving any prior notice or reason to the Users.  The Users acknowledge that the Company shall not be liable to any party for any modification, suspension or discontinuance of the Services.");?>
</p> 
                 </li>
                 
                 <li> 
                 <h3 class="text-info"><?php echo __("11. Termination of Service");?>
</h3> 
                 <p><?php echo __("The Company shall have the right to delete or deactivate any account, or block the email or IP address of any User, or terminate the access of Users to the Services, and remove any Materials within the Services immediately without notice for any reason, including but not limited to the reason that the User breached any law or the Terms and Conditions.  The Company reserves the right at any time to take such action as it considers appropriate, desirable or necessary including but not limited to taking legal actions against any such User.  The Company shall have no obligation to deliver any Materials posted on the Sites to any User at any time, both before or after cessation of the Services or upon removal of the related Material(s) from the Sites.");?>
 </p> 
                 </li>
                 
                 <li> 
                 <h3 class="text-info"> <?php echo __("12. Disclaimer");?>
</h3> 
                 <p><?php echo __("The Company does not have control over and does not guarantee the truth or accuracy of listings of any Materials posted on the Sites or any content on third party web sites accessed via the Sites.");?>
</p> 
                 <p><strong><?php echo __("IN ANY EVENT, THE COMPANY, ITS AFFILIATES, ITS OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, PARTNERS, REPRSENTATIVES, SHAREHOLDERS, SERVANTS, ATTORNEYS, PREDECESSORS AND SUCCESSORS SHALL NOT BE LIABLE IN ANY EVENT FOR ANY LOSSES, CLAIMS OR DAMAGES SUFFERED BY ANY USER WHATSOEVER AND HOWSOEVER ARISING OR RESULTING FROM HIS/HER/ITS USE OR INABILITY TO USE THE SITES AND THEIR CONTENTS, INCLUDING NEGLIGENCE AND DISPUTES BETWEEN ANY PARTIES");?>
</strong>.</p> 
                 </li>
                 
                 <li> 
                 <h3 class="text-info"><?php echo __("13. Limitation of Liability");?>
</h3> 
                 <p><?php echo __("Without prejudice to the above and subject to the applicable laws, the aggregate liability of the Company to any User for all claims arising from their use of the Services and the Sites shall be limited to the amount of HK"."$"."1,000.");?>
</p> 
                 </li>
                 
                 <li> 
                 <h3 class="text-info"><?php echo __("14. Security Measures");?>
</h3> 
                 <p><?php echo __("The Company will use its reasonable endeavours to ensure that its officers, directors, employees, agents and/or contractors will exercise their prudence and due diligence in handling the personal data submitted by the Users, and the access to and processing of the personal data by such persons is on a need-to-know and need-to-use basis. The Company will use its reasonable endeavours to protect the personal data against any unauthorized or accidental access, processing or erasure of the personal data.");?>
</p> 
                 </li>
                 
                 <li> 
                 <h3 class="text-info"><?php echo __("15. Severability");?>
</h3> 
                 <p><?php echo __("The provisions of the Terms and Conditions shall be enforceable independently of each other and the validity of each provision shall not be affected if any of the others is invalid.  In the event any provision of the Terms and Conditions is determined to be illegal, invalid or unenforceable, the validity and enforceability of the remaining provisions of the Terms and Conditions shall not be affected and, in lieu of such illegal, invalid, or unenforceable provision, there shall be added as part of the Terms and Conditions one or more provisions as similar in terms as may be legal, valid and enforceable under the applicable law.");?>
</p> 
                 </li>
                 
                 <li> 
                 <h3 class="text-info"><?php echo __("16. Governing Law and Dispute Resolutions");?>
</h3> 
                 <p><?php echo __("The Terms and Conditions and any dispute or matter arising from or incidental to the use of the Sites shall be governed by and construed in accordance with the laws of the Hong Kong Special Administrative Region of the People’s Republic of China (Hong Kong).");?>
</p> 
                 <p><?php echo __("Any dispute, controversy or claim arising out of or relating to the Terms and Conditions including the validity, invalidity, breach or termination thereof, shall be settled by arbitration in accordance with the Arbitration Rules as at present in force and as may be amended by the rest of this Clause:");?>
</p> 
                 <ol type="a"> 
                 <li><?php echo __("(a) The appointing authority shall be Hong Kong International Arbitration Centre (hereinafter referred to as HKIAC).");?>
</li> 
                 <li><?php echo __("(b) The place of arbitration shall be in Hong Kong at HKIAC.");?>
</li> 
                 <li><?php echo __("(c) There shall be only one arbitrator.");?>
</li> 
                 <li><?php echo __("(d) The language to be used in the arbitral proceedings shall be English.");?>
</li> 
                 </ol> <p><?php echo __("In the event of any breach of the Terms and Conditions by any one party, the other party shall be entitled to remedies in law and equity as determined by arbitration.");?>
</p> 
                 </li>
                
				   <li> 
                 <h3 class="text-info"><?php echo __("17. Discrepancy");?>
</h3> 
                 <p><?php echo __("This English version of the Terms and Conditions and any foreign language translation, the English version shall prevail.");?>
</p> 
                 </li>
				   <li> 
                 <h3 class="text-info"><?php echo __("18. Inquiry");?>
</h3> 
                 <p><?php echo __("For any query, please contact us by sending email to support@guo.media.");?>
</p> 
                 </li>
				  </ol>
                 <?php } else { ?>
				 <p>條款和細則

	 <p>Guo.media (又名《郭媒體》)及其子域（統稱“網站”）由中國金泉集團（香港）有限公司所持有，公司總部設於香港。本公司和/或其子公司和/或關聯公司 (統稱“本公司”; 而因應相關內文需要或許可，“本公司”亦可指本公司和/或其子公司和/或關聯公司) 除了通過網站、其他的媒體平臺或網站（“平臺”）提供服務，還會透過全部或部分由本公司開發的流動網絡和流動應用程式程式(其包括流動應用程式)（“應用程式”）向用戶提供服務。網站、平臺和應用程序 (簡稱或統稱“渠道”)。用戶瀏覽和使用由渠道所提供的服務必須遵守此條款和細則（列於下文）及私隱政策（“條款和細則”）。

	 </p><p>通過使用渠道或經渠道進入其它網站和/或媒體平臺和/或應用程式，任何渠道的用戶（“用戶”）會被視為接受並同意遵守條款和細則及私隱政策。若用戶不接受任何條款和細則及私隱政策，請不要使用渠道。本公司保留權利在沒有預先通知的情況下，隨時修改條款和細則及私隱政策。一旦在渠道上發佈，修訂後的條款和細則及私隱政策將適用於所有用戶。本公司建議用戶應定期瀏覽此頁面，查看最新條款和細則及私隱政策。用戶一旦通知本公司不接受任何修訂，其應立即終止使用渠道和服務（定義見下文），否則繼續使用渠道將被視為接受所有修訂並受其約束。

	 </p><p>用戶也接受並同意，當使用渠道時，除了受本文所載的條款和細則所約束，用戶將並同時受其使用的相關渠道的條款和細則及其上傳或張貼資料（定義見下文）於本公司或關聯公司所經營之相關渠道的條款和細則所約束(如適用)。

	 </p><p>>1.	一般條款
	 </p><p>1.	本公司為個人，公司和機構提供在線平台，作為論壇和在線平台，供用戶發表他們的評論和照片以及任何信息，文字，鏈接，圖形，視頻和廣告（“服務”）。
</p><p>2.	除條款和細則所述外，本公司不參與任何用戶之間的實際交易。
</p><p>3.	本公司致力於保護用戶的私隱。本公司根據私隱政策中所描述的條款使用用戶的信息。

</p><p>2.	禁止條款
</p><p>a.	用戶(包括會員)同意不使用任何渠道進行下列明確禁止的用途：
</p><p>i.	所有用戶禁止干擾、擾亂或侵犯，或試圖干擾、破壞或企圖違反渠道上伺服器或網路的安全性，包括但不限於登入未經授權的伺服器或帳戶，試圖探查、掃描或測試系統或網路的脆弱性或未經適當授權而試圖違反安全或認證措施，企圖干擾任何用戶、主機或發送未經認可的電子郵件。如用戶違反系統或網路安全可能引致民事和/或刑事的法律責任。
</p><p>ii.	用戶不得刪除或修改其他用戶發佈的任何材料或資訊。
</p><p>iii.	用戶不得使用渠道（1）上傳、張貼、發佈、傳輸、分配、流通或儲存違反任何現行法律或規例的資料；或（2）以任何方式侵犯版權、商標、商業秘密或他人權利；或（3）以任何方式違法、對人身傷害、誹謗、淫穢、歧視、騷擾、威脅、辱駡性的，或以其他方式攻擊或令人反感的行為；或（4）作出除依據本條款和細則或本公司和用戶之間已簽訂的任何服務合同的條款以外的其他商業目的。所有用戶不得列印、下載、複製或以其他方式使用其他用戶的個人身份資訊(如有)。用戶不可使用任何型式向其他用戶發送未經授權的通信騷擾。
</p><p>iv.	用戶如果不具有法律能力構成具有法律約束力的合同或其被法律禁止使用服務，不應使用此渠道。
</p><p>v.	用戶不得經渠道發佈任何包含虛假記載、誤導或誹謗性內容、任何電腦病毒、木馬、蠕蟲的電腦檔和可能中斷、破壞或限制任何電腦軟、硬體或電信設備的任何材料。
</p><p>vi.	用戶不得上傳或張貼任何材料涉及銷售或提供任何非法產品或服務或任何含有欺詐成分的資訊。
</p><p>vii.	用戶不得濫發資訊，包括但不限於以任何形式張貼、發送或散播任何不明的電子郵件或訊息。

	</p><p>可接受的渠道使用
	 <p>3.1	特定用途 — 適用於在渠道上發佈照片，文章，觀點，評論，留言和/或其他任何資訊（統稱“資料”）的用戶。發佈資料的用戶以下簡稱為“發佈用戶”。
</p><p>i.	發佈用戶同意僅用渠道於合法用途以及享受渠道所提供的服務。本公司可按其意願保留對發佈在渠道上的所有資料的編輯、拒絕、否決及刪除的權利。
</p><p>ii.	若發佈用戶為個人用戶，他/她不可將其身份證和/或護照號碼發佈在渠道上。
</p><p>iii.	雖然本公司將使用合理方法確保只限本公司員工接觸發佈用戶個人資料的資料庫，但是本公司並不保證其他人士將不會在沒有本公司允許的情況下接觸該資料庫。對於發佈用戶提供的個人資料的使用和保護，請參照 私隱政策。
</p><p>iv.	發佈用戶需對其通過渠道上傳、發佈或分享的資料、以及其連結到渠道的任何網頁，媒介平臺和/或應用程式負全部責任。本公司按其意願保留編輯，拒絕，刪除任何資料和連結到網頁和/或媒介平臺和/或應用程式的權利。本公司有權自行決定終止向任何發佈用戶提供任何服務。若發佈用戶在渠道發佈資料後將資料刪除或終止其與渠道設立的帳戶，或本公司刪除任何已發佈的資料，則該發佈用戶將無法通過其帳戶接觸該等資料；然而，被刪除的資料仍可持續出現在渠道的任一部分，和/或被本公司以其他任何形式使用。
</p><p>v.	本公司保留權利在不給予任何理由的情況下以通知要求任何用戶立即停止使用其在渠道設立的帳戶，或更改其用戶名稱；若用戶拒絕遵守本公司提出的要求，本公司可自行決定隨時在沒有預先通知的情況下關閉發佈用戶帳戶，而此行動並不損害本公司其他權利。
</p><p>vi.	本條款僅適用於用戶張貼廣告，促銷和市場材料（“廣告客戶”）：
</p><p>向本公司支付服務費或接受任何免費試用促銷優惠後，用戶將有權使用該網站發布廣告和宣傳材料（受條款和條件約束）。本公司亦保留更改服務費的權利，或在廣告商認為適當的情況下，刊登廣告刊登廣告及宣傳資料的新費用或費用。如果任何廣告商張貼廣告和宣傳材料未能支付服務費或任何其他費用或費用，公司保留權利暫停或終止該廣告客戶的用戶帳戶，廣告和鏈接到網頁，不妨礙所有其他權利和補救措施。


</p><p>3.2 	特定用途 — 適用於瀏覽和/或使用在渠道上的資料的用戶（簡稱“瀏覽者”） 

</p><p>瀏覽者同意其只使用渠道於合法用途和用來享受其所提供的服務。瀏覽者同意通過渠道或從本公司接收到的任何個人資料，只會用作鑒定和/或搜尋廣告或材料或其內容，或使其享受渠道所提供服務。任何接收到的個人資料與上述目的無關的，包括但不限於商業目的，需被忽視，且不可以任何形式存儲，收集，處理，使用，傳播。流覽者亦同意在上述目的失效或已完成的情況下，立即完全刪除從渠道或從本公司所收集的所有個人資料。 

</p><p>若任何用戶違反條款和細則，無論是在香港或其他地方，使用任何其他用戶個人資料、資訊或資料（無論是否從渠道獲得）本公司將不承擔任何責任。所有用戶接受其公開在渠道上提供的所有個人資料、資訊或資料都是自願提供，且個人承擔所提供資料的所有風險。本公司將不負責保護其公開提供的該等個人資料或資訊。

	</p><p>內容許可
	</p><p>通過在渠道上發佈資料, 用戶無條件授予本公司非獨家的、全球的、不可撤銷、無版稅版權、行使其資料之著作權，發表和數據庫權（但沒有其他權利），以便本公司在任何地方，無論有沒有商業收益，以任何形式使用、發佈、託管、展示、推廣、複製、下載、提交、傳播、複製，轉移、編輯、銷售和再利用資料，及實施載於私隱政策中和本文所述的用途。

	</p><p>知識產權
</p><p>渠道的所有內容,包括不限於文本、圖片、資訊、評論、版面、資料庫、照片、聲音、視頻格式、軟體、品牌和HTML 均是本公司或用戶的知識產權（視情況而定），這些均受版權和商標法保護而不可被下載、複製、修改、轉載、列印或以任何方式傳送，用戶下載及/或列印一份此類材料作為其個人合法和非商業用途或根據本公司與用戶已簽訂的服務合同範圍內而下載和/或列印此類材料除外。除條款和細則及任何特定渠道的條款和細則或與本公司簽訂的條款訂定之用途外，用戶嚴格禁止使用任何上述之內容，本公司保留與此相關的所有權利。用戶認同他們將單獨負責因違反版權法及/或其他知識產權法所引致的任何不良後果。

</p><p>為免生疑問，於任何第三方聲稱同意用戶使用條款和細則提到的內容及資料下，用戶亦不得免除此文所施加的限制。 
</p><p>
用戶同意本公司可以自由地使用、披露、採用和修改所有其通過渠道而提供的想法、概念、提案、建議及其它交流及資訊而毋須因此給予其任何報酬或補償。用戶謹此放棄就本公司使用、披露、採用及/或修改其想法、概念、提案、建議及其它交流及資訊而向本公司索取任何報酬、收費、特許權使用費、手續費及/或其他費用的權利。

	</p><p>內容
</p><p>用戶確認本公司可能不會預先過濾或預先授權通過渠道登載或發送之任何內容。在任何情況下，本公司對渠道上的內容或通過渠道發送的內容不承擔任何責任，對任何內容或資料的丟失，以及上述內容或資料的準確性不提供任何保證。 

</p><p>用戶在渠道上發佈的任何資料可供與渠道連結的其他網站及/或媒介平臺及/或應用程式之用戶瀏覽。本公司對任何用戶通過渠道連結的第三方網站、媒介平臺及應用程式而不合理及/或非法地使用在渠道上發佈的數據或資訊不承擔任何責任。由渠道上提供的其它網站、媒介平臺及應用程式的連結僅為了便利用戶，並作為互聯網的導航工具，本公司不對其它網站、媒介平臺及應用程式加以控制。本公司對其它網站、媒介平臺及應用程式的任何內容或其提供給用戶的任何服務不承擔任何責任。用戶確認並同意單獨對其發佈資料，網頁或其他資訊內容的準確性負全面的責任。本公司對任何連結渠道之其它網站之內容、媒介平臺及應用程式不承擔任何責任 (無論是否與渠道相聯), 亦不會對任何其它網站、媒介平臺及應用程式內之內容或資料的準確性提供任何保證。若用戶使用連結渠道的其它網站、媒介平臺及/或應用程式，由其自身承擔風險。 

</p><p>若本公司認為用戶違反或有可能違反任何特定渠道的條款或本公司與用戶簽訂的服務合同之條款和細則，本公司將有權刪除在渠道上已上載或發佈的任何資料並驅逐和防止用戶再使用渠道（例如:包括但不限於終止會員帳戶及阻止用戶進入互聯網通信協定位址），並無需對用戶作出任何補償。 

</p><p>若本公司基於任何不關乎違反法律或條款和細則規定之原因決定刪除任何已付款之廣告或終止任何已付款之服務（包括但不限於服務訂購），本公司可在扣除已刊登廣告的時期或已提供服務的相關費用後，向商家或廣告客戶退回餘款（如有）。 

</p><p>用戶同意本公司在遵守私隱政策的條款下，使用用戶在渠道上已提供的個人資料和其他資訊以提供本公司有關的服務、行銷服務或特別活動。 

	</p><p>責任
</p><p>本公司或不會在任何時間都監控渠道但保留此權利。本公司不保證任何資料或網頁或應用程式是否被特定數量的用戶或任何特定用戶瀏覽。在渠道的使用上，本公司不會作為任何用戶的代理，對因使用渠道或其他任何原因造成的直接或間接損害不會承擔任何責任。本公司竭力向用戶提供優質服務，但不保證渠道的操作無失誤或渠道和伺服器完全無病毒或無其他損害裝置。 

</p><p>若因使用渠道或其內容導致用戶需要維修或替換設備或資料，用戶需自行承擔相關費用。渠道和其內容是按照“目前的模式”提供，在模式種類上沒有任何保證。在法律允許的最大限度內，本公司對渠道內容、服務、文本、圖像和連結的適用性、適合特定用途性、準確性和可靠性、完整性或及時性不作任何保證。

	</p><p>自身風險
	</p><p>所有用戶需就使用渠道及通過渠道瀏覽其他網頁和/或媒體平臺和/或應用程式而承擔自身風險。所有用戶須對其發佈行為負責，本公司不保證用戶在渠道上發佈資料的真實性、準確性和可靠性，亦不代表本公司贊同發佈用戶表達的觀點。所有用戶依賴其其他用戶張貼的廣告和材料時應由其承擔自身風險。在用戶違反條款和細則內容或侵犯法律的情況下，本公司保留權力排除該用戶並禁止用戶再次登入渠道，亦保留權利可自行決定刪除任何具有辱駡性、非法性、分裂性或不適當的資料。

	</p><p>賠償
	</p><p>所有用戶同意保障及免除本公司、其執行官、董事、雇員、代理、合作商、代表、股東、服務人員、律師、前任、繼任人員和受讓人，因用戶使用渠道或通過使用本公司提供的任何服務或違反任何條款和細則或特定渠道之條款或本公司和用戶之間所簽訂的任何服務合同而產生的任何索賠、行為、要求、責任、損失和費用等方面的責任（包括全額賠償基準上的法務費用和訴訟費用）。如有任何此類索賠，訴訟或法律程序的，本公司將及時通知相關用戶。

	</p><p>服務範圍
	</p><p>本公司有權限制服務的使用，包括資料在網站上發布的時間，資料的大小，位置和位置，電子郵件或由服務傳送的任何其他內容。本公司保留自行決定，不經事先通知或向用戶作出任何理由，編輯，修改，刪除，刪除或刪除網站上張貼的任何材料的權利。用戶確認，公司不對任何一方對服務進行任何修改，暫停或中斷負責。

	</p><p>服務終止
	</p><p>本公司有權因任何原因(包括但不限於用戶違反任何法律、條款和細則、渠道或任何特定平臺之條款和細則或本公司與用戶簽署的任何服務合同之條款和細則)及沒有通知的情況下刪除或停用任何帳戶或屏蔽任何用戶的郵件或IP 地址, 或終止對用戶的服務, 或移除服務內的資料。本公司保留其認為適當或必要時，在任何時候採取上有關行動, 包括但不限於向該用戶採取法律行動。本公司沒有義務在停止服務或删除渠道中的資料之前或之後，向任何用戶傳達渠道上發佈的任何資料。

	</p><p>免責聲明
	</p><p>本公司不控制及不能確保通過渠道上進入的第三方網站和/或媒體平臺和/或應用程式發佈的任何內容之真實和準確性。

</p><p>本公司概不負責因超出其合理控制範圍的事件（包括但不限於網路故障或任何系統預設）可能引致的任何直接或間接的損失。

</p><p>在任何情況下，本公司、其執行官、董事、雇員、代理、合作商、代表、股東、服務員、律師，前任和繼任人員任絕不需因 (i) 用戶使用或無法使用渠道的內容或服務，包括因疏忽，或因各方的分歧;（ii）用戶使用任何廣告商或商家提供的服務或產品 ; 和/或（iii）任何廣告商或商家行為，提供的服務或政策，而直接或間接引起或造成的任何損失、索償或損害賠償（包括但不限於直接、間接、偶然、特殊、懲罰性或相應的損害賠償，業務或利潤損失）負任何責任。 


	</p><p>責任範圍
	</p><p>在不影響上文及符合適用法律的基礎上，本公司對任何用戶因使用服務和/或渠道而提出所有索償和費用的整體責任上限為港幣1000元。

	</p><p>安全措施
	</p><p>本公司將盡其合理努力，確保其執行官、董事、雇員、代理、或承包商審慎細緻地處理用戶提交的個人資料，確保接觸和處理個人資料的人是基於“需要瞭解”和“需要使用”的基礎上。本公司將努力保護個人資料免受非授權接觸、處理和刪除。有關本公司如何使用和保障用戶通過渠道上提供的個人資料，請參閱私隱政策。

	</p><p>可分割性
</p><p>本條款和細則的每一條款獨立於其他條款，當條款和細則的任何條文已屬或變成違法、無效或不可執行時，其他條款的有效性不受影響。並以現行使用法律下的一個或多個合法、有效和可行條例加以替換。

</p><p>
	</p><p>適用法律和爭議處理
</p><p>除非另有其他規定，如因此條款和細則或任何因使用渠道而產生的爭議均由中華人民共和國香港特別行政區（以下簡稱"香港"）的法律詮釋及約束。 

</p><p>任何由條款和細則（包括有效、無效，違反協議或終止條款）引起的爭議、分歧或索賠應根據截至目前生效的《香港國際仲裁中心仲裁規則》並通過此條款其餘條文修正而仲裁解決︰

</p><p>i.	指派機關應為香港國際仲裁中心（以下簡稱"仲裁中心"）; 或由本公司在任何國家中(以其唯一和絕對酌情權)任命其認為適合和適當的指派機關。任何用戶明白及同意如與本公司發生糾紛，本公司對指派機關的選擇和任命有最終決定權。
</p><p>ii.	仲裁地點應設在香港的仲裁中心。
</p><p>iii.	須只有一名仲裁員。
</p><p>iv.	在仲裁過程中使用的語言為英語。

</p><p>如任何一方違反此條款和細則，另一方有權根據仲裁認定的普通法和衡平法採取補救措施。

	</p><p>歧義
</p><p>如英文版本與其他語言的版本有任何歧義，概以英文版本為準。
	</p><p>查詢
</p><p>如有任何疑問，可電郵support@guo.media與本公司聯絡。</p>


				 <?php }?>
				 
		        </div>

		    </div>

	    </div>

	    

	</div>

</div>

<!-- page content -->



<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
