<?php
/* Smarty version 3.1.31, created on 2019-02-04 17:42:53
  from "/var/app/current/content/themes/default/templates/search.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5c58799d0ea691_29311290',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0f275f647b68d744a432ab21bd08d90044fca432' => 
    array (
      0 => '/var/app/current/content/themes/default/templates/search.tpl',
      1 => 1536745018,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:__feeds_user.tpl' => 1,
    'file:__trend_feeds_post.tpl' => 4,
    'file:__feeds_post.tpl' => 10,
    'file:_ads.tpl' => 1,
    'file:_widget.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_5c58799d0ea691_29311290 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- page content -->
<div class="search-result-heading">
    <div class="container">
        <?php echo $_smarty_tpl->tpl_vars['query']->value;?>

    </div>
</div>

<div class="search-result-custom">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="view-top <?php if ($_smarty_tpl->tpl_vars['follow']->value == '') {?> active <?php }?>">
                <a href="#top" data-toggle="tab">
                    <strong class="pr5"><?php echo __("Top");?>
</strong>
                </a>
            </li>
            <li class="view-posts">
                <a href="#posts" data-toggle="tab">
                    <strong class="pr5"><?php echo __("Posts");?>
</strong>
                </a>
            </li>
            <li class="view-people <?php ob_start();
echo $_smarty_tpl->tpl_vars['follow']->value;
$_prefixVariable1=ob_get_clean();
if ($_prefixVariable1 == 'follow') {?> active <?php }?>">
                <a href="#users" data-toggle="tab">
                    <strong class="pr5"><?php echo __("People");?>
</strong>
                </a>
            </li>
            <li class="view-trends">
                <a href="#trends" data-toggle="tab">
                    <strong class="pr5"><?php echo __("Trends");?>
</strong>
                </a>
            </li>
            <li class="view-media">
                <a href="#media" data-toggle="tab">
                    <strong class="pr5"><?php echo __("Media");?>
</strong>
                </a>
            </li>
            <li class="view-mentions">
                <a href="#mentions" data-toggle="tab">
                    <strong class="pr5"><?php echo __("Mentions");?>
</strong>
                </a>
            </li>
            <li class="view-articles">
                <a href="#articles" data-toggle="tab">
                    <strong class="pr5"><?php echo __("Articles");?>
</strong>
                </a>
            </li>
            <li class="view-polls">
                <a href="#polls" data-toggle="tab">
                    <strong class="pr5"><?php echo __("Polls");?>
</strong>
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="container mt20 offcanvas">
    <div class="row">
    
        <!-- side panel -->
        <div class="col-sm-4 col-md-3 offcanvas-sidebar">
            
            <div class="panel panel-default panel-widget">
        
            
                        <div class="panel-heading">
                            <div class="mt5">
                                <strong><?php echo __("Search");?>
</strong><span class="show-hide"><a href="javascript:void(0)"><?php echo __("Show");?>
</a></span> <span class="hide-show" style="display:none"><a href="javascript:void(0)"><?php echo __("hide");?>
</a></span>
                            </div>
                        </div>
                        <div class="panel-body search-panel-body" style="display:none">
                            <form class="form-horizontal js_search-form">
                                <div class="form-group">
                                    <div class="col-sm-8 mb5">
                                        <input type="text" name="query" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['query']->value;?>
" placeholder='<?php echo __("Search for people, pages and #hashtags");?>
' required>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="submit" name="submit" class="btn btn-primary"><?php echo __("Search");?>
</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                   
                </div>
                
               <div class="panel panel-default panel-widget">
                    <div class="panel-heading">
                        <div class="pull-right flip">
                            <small><a href="/people"><?php echo __("View All");?>
</a></small>
                        </div>
                        <strong class="wtf-module"><?php echo __("Who to follow");?>
</strong>
                    </div>
                    <div class="panel-body">
                        <ul>
                        <?php if (count($_smarty_tpl->tpl_vars['user']->value->_data['new_people']) > 0) {?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['user']->value->_data['new_people'], '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
                            <?php $_smarty_tpl->_subTemplateRender('file:__feeds_user.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_connection'=>"add",'_small'=>true), 0, true);
?>

                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                         <?php }?>
                         <?php if (count($_smarty_tpl->tpl_vars['user']->value->_data['new_people']) <= 0) {?>
                         <p class="text-center text-muted">
                            <?php echo __("No people available");?>

                         </p>
                         <?php }?>
                        </ul>
                    </div>
                </div>    


            
            <div class="site_bar_new">
                <!-- Disable weekily trends -->
                <!--<div class="sidebar_trends">
                <h3><?php echo __("Weekly Trends for you");?>
</h3>
                
                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['popular_tag'] != 'No Record Found') {?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['user']->value->_data['popular_tag'], 'custom_field');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['custom_field']->value) {
?>
                <div class="fb_post_rep">
                <h4><a href="/trend_post.php?tags=<?php echo $_smarty_tpl->tpl_vars['custom_field']->value['url_tags'];?>
"><?php echo $_smarty_tpl->tpl_vars['custom_field']->value['tags'];?>
</a></h4>
                <p><?php echo $_smarty_tpl->tpl_vars['custom_field']->value['count'];?>
 <?php echo __("Posts");?>
</p>
                </div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                <?php } else { ?>
                <h4><?php echo __("No Trends for you.");?>
</h4>
                <?php }?>
                
                </div>
            </div>-->
        </div>
        <!-- side panel -->
        </div>
        <div class="col-sm-8 col-md-9 offcanvas-mainbar">
            <div class="row">
                <!-- left panel -->
                <div class="col-sm-12">
                    <!-- search form -->
                    
                    <!-- search form -->

                    <?php if ($_smarty_tpl->tpl_vars['query']->value) {?>
                    <!-- search results -->
                     <div class="panel-body tab-content">
                        <div class="tab-pane <?php if ($_smarty_tpl->tpl_vars['follow']->value == '') {?> active <?php }?>" id="top">
                            
                            <?php if (count($_smarty_tpl->tpl_vars['results']->value['limit_user']) > 0) {?>
                            <div class="custom-view"><p class="mt10 mb10"><strong><?php echo __("People");?>
</strong></p><span class="people-view-all"><a href="#users" data-toggle="tab"><?php echo __("View All");?>
</a></span></div>
                                
                                <div class="row">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['limit_user'], '_user', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['_user']->value) {
?>
                                    <div class="col-sm-4">
                                     <div class="site_bar_new custom-site-bar-new">
                                         <div class="profile-bg-thumb"> 
                                        <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_cover']) {?>
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_cover'];?>
">
                                        <?php }?>
                                        </div>
                                        <div class="side_profile">
                                        <div class="pro_thumb"> <a href="/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>"><img src="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>
"></a></div>
                                        
                                        <div class="admin_detail">
                                        <span> 
                                        
										<?php if ($_smarty_tpl->tpl_vars['results']->value['custom_login_user_id'] != $_smarty_tpl->tpl_vars['_user']->value['user_id']) {?>
										
                                        <?php if ($_smarty_tpl->tpl_vars['_user']->value['following'] == 0) {?>
                                            <a class="js_follow follow_btn 1122<?php echo $_smarty_tpl->tpl_vars['_user']->value['following'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" href="#"><?php echo __("Follow");?>
</a>
                                        <?php } else { ?>  
                                            <a class="btn btn-default js_unfollow" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" href="#"><i class="fa fa-check"></i><?php echo __("Following");?>
</a>
                                        <?php }?>
										
										<?php }?>
                                        
                                        </span>
                                        <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
">
                                            <a href="/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>"><span><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];
echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>
 </span> <span class="user-at-btn">@<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
</span></a>
                                        </span>
                                        
                                        
                                        </div>
                                        <p><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_biography'];?>
</p>
                                        </div>
                                    </div>
                                    </div>
                                        <?php if (($_smarty_tpl->tpl_vars['k']->value+1)%3 == 0) {?>
                                            <div class="clearfix"></div>
                                        <?php }?>
                                 <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </div>

                                <?php }?>

                               <?php if (count($_smarty_tpl->tpl_vars['trend_results']->value['trends_result']) > 0) {?>
                                 <div class="custom-view"><p class="mt10 mb10"><strong><?php echo __("Trends");?>
</strong></p><span class="trends-view-all"><a href="#trends" data-toggle="tab"><?php echo __("View All");?>
</a></span></div>
                                <ul>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['trend_results']->value['trends_result'], 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__trend_feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_tpl'=>"list"), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                                <?php }?>
                                
                               
                                
                                <!--<div class="custom-view"><p class="mt10 mb10"><strong><?php echo __("Posts");?>
</strong></p><span><a href="#posts" data-toggle="tab">View all</a></span></div>-->
                             
                                
                                <?php if (count($_smarty_tpl->tpl_vars['trend_results']->value['limit_media']) > 0 && count($_smarty_tpl->tpl_vars['trend_results']->value['limit_photos']) > 0) {?>
                                <div class="custom-view"><p class="mt10 mb10"><strong><?php echo __("Media");?>
</strong></p><span class="media-view-all"><a href="#media" data-toggle="tab"><?php echo __("View All");?>
</a></span></div>
                                <ul>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['limit_media'], 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                                
                                <ul>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['limit_photos'], 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                                <?php }?>
                                
                                <?php if (count($_smarty_tpl->tpl_vars['results']->value['limit_mention']) > 0) {?>
                                <div class="custom-view"><p class="mt10 mb10"><strong><?php echo __("Mentions");?>
</strong></p><span class="mentions-view-all"><a href="#mentions" data-toggle="tab"><?php echo __("View All");?>
</a></span></div>
                                
                                <ul>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['limit_mention'], 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                                <?php }?>
                                
                                <?php if (count($_smarty_tpl->tpl_vars['results']->value['limit_articles']) > 0) {?>
                                <div class="custom-view"><p class="mt10 mb10"><strong><?php echo __("Articles");?>
</strong></p><span class="articles-view-all"><a href="#articles" data-toggle="tab"><?php echo __("View All");?>
</a></span></div>
                                
                                <ul>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['limit_articles'], 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                                <?php }?>
                                
                                <?php if (count($_smarty_tpl->tpl_vars['results']->value['limit_polls']) > 0) {?>
                                <div class="custom-view"><p class="mt10 mb10"><strong><?php echo __("Polls");?>
</strong></p><span class="polls-view-all"><a href="#polls" data-toggle="tab"><?php echo __("View All");?>
</a></span></div>
                                
                                <ul>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['limit_polls'], 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                                <?php }?>
                              <?php if (count($_smarty_tpl->tpl_vars['results']->value['posts']) > 0) {?>
                                <ul id="toptab">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['posts'], 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>'newsfeed'), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

          
                                </ul>
  <div class="alert alert-post see-more mb10 custom_js_see-more <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>custom_js_see-more-infinite<?php }?>" data-get="newsfeed1" data-filter="<?php if ($_smarty_tpl->tpl_vars['_filter']->value) {
echo $_smarty_tpl->tpl_vars['_filter']->value;
} else { ?>all<?php }?>" <?php if ($_smarty_tpl->tpl_vars['_id']->value) {?>data-id="<?php echo $_smarty_tpl->tpl_vars['_id']->value;?>
"<?php }?> data-query="<?php echo $_smarty_tpl->tpl_vars['query']->value;?>
">
			<span><?php echo __("More Stories");?>
</span>
			<div class="loader loader_small x-hidden"></div>
		</div>
                                
                                <?php } else { ?>
                                
                                <?php }?>
                                
                        </div>


                        <div class="tab-pane" id="posts">
                            
                 
                               <?php if (count($_smarty_tpl->tpl_vars['results']->value['posts']) > 0) {?>
                                <ul id="post-tab">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['posts'], 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                                   <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>'newsfeed'), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                         <!-- see-more -->
                          <div class="alert alert-post see-more mb10 post_js_see-more <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>post_js_see-more-infinite<?php }?>" data-get="newsfeed1" data-filter="<?php if ($_smarty_tpl->tpl_vars['_filter']->value) {
echo $_smarty_tpl->tpl_vars['_filter']->value;
} else { ?>all<?php }?>" <?php if ($_smarty_tpl->tpl_vars['_id']->value) {?>data-id="<?php echo $_smarty_tpl->tpl_vars['_id']->value;?>
"<?php }?> data-query="<?php echo $_smarty_tpl->tpl_vars['query']->value;?>
">
                                <span><?php echo __("More Stories");?>
</span>
                                <div class="loader loader_small x-hidden"></div>
                            </div>
		<!-- see-more -->
                                <?php }?> 
                           
                                
                                
                        </div>

                        <div class="tab-pane <?php if ($_smarty_tpl->tpl_vars['follow']->value != '') {?> active <?php }?>" id="users">
                            
                                
                                <div class="row">
                                <?php if (count($_smarty_tpl->tpl_vars['results']->value['users']) > 0) {?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['users'], '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
                                    <div class="col-sm-4">
                                     <div class="site_bar_new custom-site-bar-new">
                                        <div class="profile-bg-thumb"> 
                                        <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_cover']) {?>
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_cover'];?>
">
                                        <?php }?>
                                        </div>
                                        <div class="side_profile">
                                        <div class="pro_thumb"> <a href="/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>"><img src="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>
"></a></div>
                                        
                                        <div class="admin_detail">
                                        <span> 
										<?php if ($_smarty_tpl->tpl_vars['results']->value['custom_login_user_id'] != $_smarty_tpl->tpl_vars['_user']->value['user_id']) {?>
                                        
                                        <?php if ($_smarty_tpl->tpl_vars['_user']->value['following'] == 0) {?>
                                            <a class="js_follow follow_btn 22" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" href="#"><?php echo __("Follow");?>
</a>
                                        <?php } else { ?>  
                                            <a class="btn btn-default js_unfollow" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" href="#"><i class="fa fa-check"></i><?php echo __("Following");?>
</a>
                                        <?php }?>
										 <?php }?>
                                        
                                        </span>
                                        <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
">
                                            <a href="/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];
if ($_smarty_tpl->tpl_vars['_search']->value) {?>?ref=qs<?php }?>"><span><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_firstname'];
echo $_smarty_tpl->tpl_vars['_user']->value['user_lastname'];?>
 </span> <span class="user-at-btn">@<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
</span></a>
                                        </span>
                                        
                                        
                                        </div>
                                        <p><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_biography'];?>
</p>
                                        </div>
                                    </div>
                                    </div>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </div>
                                
                                <?php } else { ?>
                               
                                <?php }?>
                        </div>
                            <div class="tab-pane" id="trends">
                                <?php if (count($_smarty_tpl->tpl_vars['results']->value['trends']) > 0) {?>
                                <ul>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['trends'], 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__trend_feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                                <?php } else { ?>
                                
                                <?php }?>
                            </div>
                            <div class="tab-pane" id="media">
                               <?php if (count($_smarty_tpl->tpl_vars['results']->value['media']) > 0) {?>
                                <ul>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['media'], 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                                        <?php if ($_smarty_tpl->tpl_vars['post']->value['post_id'] != 'a') {?>
                                            <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                        <?php }?>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                               <?php } else { ?>
                                
                                <?php }?>
                                
                                <ul>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['photos'], 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                                

                            </div>
                            <div class="tab-pane" id="mentions">
                                <?php if (count($_smarty_tpl->tpl_vars['results']->value['mention']) > 0) {?>
                                <ul>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['mention'], 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                                <?php } else { ?>
                                
                                <?php }?>
                            </div>
                            <div class="tab-pane" id="articles">
                                <?php if (count($_smarty_tpl->tpl_vars['results']->value['article']) > 0) {?>
                                <ul>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['article'], 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__trend_feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                                <?php } else { ?>
                                
                                <?php }?>
                            </div>
                            <div class="tab-pane" id="polls">
                                <?php if (count($_smarty_tpl->tpl_vars['results']->value['poll']) > 0) {?>
                                <ul>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['poll'], 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__trend_feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </ul>
                                <?php } else { ?>
                                   <?php if (count($_smarty_tpl->tpl_vars['results']->value['poll']) == 0) {?>
                                        
                                   <?php }?>
                                <?php }?>
                            </div>
                    <!-- search results -->
                    <?php }?>
                </div>
                <!-- left panel -->

                <!-- right panel -->
                <div class="col-sm-4">
                    <?php $_smarty_tpl->_subTemplateRender('file:_ads.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:_widget.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>
                <!-- right panel -->
            </div>
        </div>
        

        
    </div>

<!-- page content -->

<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
