<?php
/* Smarty version 3.1.31, created on 2019-07-31 16:11:51
  from "E:\wamp64\www\guo-production\content\themes\default\templates\__feeds_photo.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5d41bdc794a8c5_76465224',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bb70b256492ca8b1b09a968b0cdd66ff4debfd45' => 
    array (
      0 => 'E:\\wamp64\\www\\guo-production\\content\\themes\\default\\templates\\__feeds_photo.tpl',
      1 => 1564370770,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d41bdc794a8c5_76465224 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_photo_thumb')) require_once 'E:\\wamp64\\www\\guo-production\\vendor\\smarty\\smarty\\libs\\plugins\\function.photo_thumb.php';
?>
<div class="<?php if ($_smarty_tpl->tpl_vars['_small']->value) {?>col-xs-6 col-sm-4<?php } else { ?>col-xs-6 col-sm-2<?php }?>">
    <a class="pg_photo <?php if (!$_smarty_tpl->tpl_vars['_small']->value) {?>large<?php }?> js_lightbox" href="/photos/<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" data-image="" data-context="<?php echo $_smarty_tpl->tpl_vars['_context']->value;?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo smarty_function_photo_thumb(array('param1'=>$_smarty_tpl->tpl_vars['photo']->value['source']),$_smarty_tpl);?>
);">
    	<?php if ($_smarty_tpl->tpl_vars['_manage']->value || $_smarty_tpl->tpl_vars['photo']->value['manage']) {?>
	    	<div class="pg_photo-btn">
	            <button type="button" class="close js_delete-photo" data-id="<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" data-toggle="tooltip" data-placement="top" title='<?php echo __("Delete");?>
'>
	                <span aria-hidden="true">&times;</span>
	            </button>
	        </div>
        <?php }?>
    </a>
</div><?php }
}
