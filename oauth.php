<?php
/**
 * oauth
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/bootstrap.php');

// process
try {

	Hybrid_Endpoint::process();

} catch (Exception $e) {
	_error('System Message', $e->getMessage());
}
