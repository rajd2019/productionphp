<?php

/**
 * Created by PhpStorm.
 * User: vitaliy
 * Date: 3/4/18
 * Time: 3:22 PM
 */
class Posts
{

    private $db;
    private $user;
    private $system;

    /**
     * Posts constructor.
     * @param $db
     * @param $user
     * @param $system
     */
    public function __construct($db, $user, $system)
    {
        $this->db = $db;
        $this->user = $user;
        $this->system = $system;
    }


    public function getHomePosts($ids)
    {
        //todo: use 'getHomePosts' when we will add 'video' to each element of post, where post_type is video
        $limit = count($ids);
        $post_implode = implode(",", $ids);
        $postsInfo = [];

        if($limit == 0){
            return $postsInfo;
        }

        $posts = $this->db->query(sprintf("SELECT posts.*, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_picture_id, users.user_cover_id, users.user_verified, users.user_subscribed, users.user_pinned_post FROM posts LEFT JOIN users ON posts.user_id = users.user_id AND posts.user_type = 'user' WHERE NOT (users.user_name <=> NULL) AND posts.post_id IN ( %s ) LIMIT %s", $post_implode, $limit)) or _error(SQL_ERROR_THROWEN);
        if ($posts->num_rows > 0) {
            while ($post = $posts->fetch_assoc()) {
                $post['i_save'] = false;
                $post['i_like'] = false;
                $post['post_comments'] = [];
                $postsInfo[$post['post_id']] = $post;
            }
        }


        if ($this->user->_logged_in) {

            $check_save = $this->db->query(sprintf("SELECT * FROM posts_saved WHERE user_id = %s AND post_id IN ( %s ) LIMIT %s", secure($this->user->_data['user_id'], 'int'), $post_implode, $limit)) or _error(SQL_ERROR_THROWEN);
            if ($check_save->num_rows > 0) {
                while ($post = $check_save->fetch_assoc()) {
                    $postsInfo[$post['post_id']]['i_like'] = true;
                }
            }
            /* like */
            $check_like = $this->db->query(sprintf("SELECT * FROM posts_likes WHERE user_id = %s AND post_id IN ( %s ) LIMIT %s", secure($this->user->_data['user_id'], 'int'), $post_implode, $limit)) or _error(SQL_ERROR_THROWEN);
            if ($check_like->num_rows > 0) {
                while ($post = $check_like->fetch_assoc()) {
                    $postsInfo[$post['post_id']]['i_like'] = true;
                }
            }
        }


        return $postsInfo;

    }


}
