<?php
if($system['environment'] != 'development'){
  define('ENV', $system['environment']);
  $bugsnag = Bugsnag\Client::make($system['bugsnag_key']);
  $bugsnag->setReleaseStage(ENV);
  Bugsnag\Handler::register($bugsnag);
}
