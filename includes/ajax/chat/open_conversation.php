<?php
/**
 * ajax -> posts -> story
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
is_ajax();

// user access
user_access(true);

try {

    // initialize the return array
    $return = [];

    // create a story
    $return['story'] = $smarty->fetch("ajax.chat_conversation_list.tpl");
    $return['callback'] = "$('#modal').modal('show'); $('.modal-content:last').html(response.story);";
            
    // return & exit
    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
