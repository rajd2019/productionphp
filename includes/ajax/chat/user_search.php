<?php
/**
 * ajax -> posts -> story
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
is_ajax();
// user access
user_access(true);

try {
    // initialize the return array
    $return = [];
    if (!isset($_GET['id'])) {
        if (isset($_GET['user_id'])) {
            $return['user_id']=$_GET['user_id'];
            $return['user_name']=$_GET['user_name'];
            $return['name']=$_GET['name'];
            $smarty->assign('user_detail', $return);
            $return['story'] = $smarty->fetch("ajax.chat_profile_user.tpl");
            $return['callback'] = "$('#modal').modal('show'); $('.modal-content:last').html(response.story);";
        } else {
            // create a story
            $return['story'] = $smarty->fetch("ajax.chat_search_user.tpl");
            $return['callback'] = "$('#modal').modal('show'); $('.modal-content:last').html(response.story);";
        }
    } else {
        $return = $user->get_conversation($_GET['id']);
        $return['messages'] = $user->get_conversation_messages($return['conversation_id']);
        $smarty->assign('conversation', $return);
        $return['story'] = $smarty->fetch("ajax.chat_with_user.tpl");
        $return['callback'] = "$('#modal').modal('show'); $('.modal-content:last').html(response.story);";
    }
    // return & exit
    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
