<?php
/**
 * ajax -> posts -> story
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
//is_ajax();

// user access
user_access(true);

try {

    // initialize the return array

    if (isset($_GET['id'])) {
        $post = $user->get_post($_GET['id']);
        if (!$post) {
            _error(404);
        }
        //$return['messages'] = $user->get_post($_GET['id']);
        //  $smarty->assign('conversation', $return);
        $smarty->assign('post', $post);
        $return = [];
        //$return['name'] = $post;
        $return['post_id'] = $_GET['id'];
        $smarty->assign('post_id', $_GET['id']);
        $return['story'] = $smarty->fetch("direct_share.tpl");
        $return['callback'] = "$('#modal').modal('show'); $('.modal-content:last').html(response.story);";
    }
    // return & exit
    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
