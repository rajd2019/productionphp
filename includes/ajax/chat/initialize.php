<?php
/**
 * ajax -> chat -> initialize
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if (!$user->_logged_in) {
    modal(LOGIN);
}

// initialize opened chat boxes session if not
if (!isset($_SESSION['chat_boxes_opened'])) {
    $_SESSION['chat_boxes_opened'] = [];
}

// get opened chat boxes session
try {

    // get opened chat boxes
    $conversations = [];
    if (!empty($_SESSION['chat_boxes_opened'])) {
        foreach ($_SESSION['chat_boxes_opened'] as $opened_conversation_id) {
            $conversation = $user->get_conversation($opened_conversation_id);
            if ($conversation) {
                $conversations[] = $conversation;
            }
        }
    }
    return_json($conversations);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
