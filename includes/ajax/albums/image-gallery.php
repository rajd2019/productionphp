<?php
/**
 * ajax -> albums -> action
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
is_ajax();

// user access
user_access(true);

$_posts = $user->Image_Gallery($_POST['post_id']);


$return = [];

$content_li = '';

$count = 0;

$go_to_slide = 0;
foreach($_posts[0]['photos'] as $photo){
	
	if($photo['photo_id'] == $_POST['photo_id']){
		
		$go_to_slide = $count;
	}	
	$content_li .= '<li class="col-xs-6 col-sm-4 col-md-3"  data-src="'.$system['system_uploads'].'/'.$photo['source'].'" data-width="900" data-sub-html="<p><span class=\'text-clickable mr20 js-custom-share-comment\' ><i class=\'Icon Icon--medium Icon--reply\'></i> <span>'.$_posts[0]['comments'].'</span></span><span class=\'text-clickable mr20 share-tweet-post\' ><i class=\'Icon Icon--medium Icon--retweet\'></i> <span >'.$_posts[0]['shares'].'</span></span><span class=\'text-clickable js_like-post\' ><i class=\'Icon Icon--heartBadge Icon--medium\'></i> <span >'.$_posts[0]['likes'].'</span></span></p>" id="li_'.$_POST['photo_id'].'"></li>';
	
	$count++;
}


$return['content'] = '<ul id="fixed-size" >'.$content_li.'</ul>';


$return['callback'] = '$(document).ready(function(){$("#fixed-size").lightGallery({download:false,controls:false,thumbnail:false,fullScreen:false,share:false,loop:false,actualSize:false,autoplayControls:false,index:'.$go_to_slide.'});$("#li_'.$_POST['photo_id'].'").trigger("click");$(".loader-on").find(".loader.loader_middium").addClass("ld-hide");});';


return_json($return);