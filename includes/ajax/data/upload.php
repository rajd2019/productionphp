<?php

require '../vendor/autoload.php';

require '../vendor/phpFFmpeg/autoload.php';

use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\MultipartUploader;
use Aws\S3\S3Client;

set_time_limit("0");
ini_set('memory_limit', '-1');
//newly added by yasir to increase max upload_max_filesize 
//'upload_max_filesize'='64M' set on AWS server and also on current/.ebextensions/guomedia.config

require_once(__DIR__ . '/../../../bootstrap.php');
require_once(__DIR__ . '/../../class-image.php');
require_once(__DIR__ . '/../../SimpleImage.php');

// check AJAX Request
is_ajax();

if($_SERVER['CONTENT_LENGTH'] > 0 && empty($_POST)){
  _error(403);
}
// check secret

if($_POST["type"] != 'take-video' && $_POST["type"] != 'take-photo'){
	// if ($_SESSION['secret'] != $_POST['secret']) {
	  // _error(403);
	// }
}	

// user access
user_access(true);

// check type
if (!isset($_POST["type"])) {
  _error(403);
}

// check handle
if($_POST["type"] != 'take-video' && $_POST["type"] != 'take-photo'){
	if (!isset($_POST["handle"])) {
	  _error(403);
	}
}	

// check multiple
if($_POST["type"] != 'take-video' && $_POST["type"] != 'take-photo'){
	if (!isset($_POST["multiple"])) {
	  _error(403);
	}
}	

function make_thumb($src, $dest, $desired_width,$extension)
{
	$extension=strtolower($extension);  
	if($extension == 'jpeg' ||  $extension == 'jpg' )
	{
		$source_image = imagecreatefromjpeg($src);
	}
	if($extension == 'png')
	{
		$source_image = imagecreatefrompng($src);
	}
	if($extension == 'gif')
	{
		$source_image = imagecreatefromgif($src);
	}
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	$desired_height = floor($height * ($desired_width / $width));
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
	if($extension == 'jpeg' ||  $extension == 'jpg')
	{
		imagejpeg($virtual_image, $dest);
	}
	if($extension == 'png' )
	{
		imagepng($virtual_image, $dest);
	}
	if($extension == 'gif' )
	{
		imagegif($virtual_image, $dest);
	}
}

// upload
try {
  
  //echo'<pre>';print_r($_FILES);
  //die('sd');

  switch ($_POST["type"]) {
  case 'photos':
    // check photo upload enabled
    if ($_POST['handle'] == 'publisher' && !$system['photos_enabled']) {
      modal(MESSAGE, __("Not Allowed"), __("This feature has been disabled"));
    }

    // get allowed file size
    if ($_POST['handle'] == 'picture-user' || $_POST['handle'] == 'picture-page' || $_POST['handle'] == 'picture-group') {
      $max_allowed_size = $system['max_avatar_size'] * 1024;
    } elseif ($_POST['handle'] == 'cover-user' || $_POST['handle'] == 'cover-page' || $_POST['handle'] == 'cover-group') {
      $max_allowed_size = $system['max_cover_size'] * 4096;
    } else {
      $max_allowed_size = $system['max_photo_size'] * 1024;
    }

    /* check & create uploads dir */
    $folder = 'photos';
    $depth = __DIR__ . '/../../../';

    if (!file_exists($depth . $system['uploads_directory'] . '/' . $folder)) {
      @mkdir($depth . $system['uploads_directory'] . '/' . $folder, 0777, true);
    }

    if (!file_exists($depth . $system['uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
      @mkdir($depth . $system['uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
    }

    if (!file_exists($system['uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
      @mkdir($depth . $system['uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
    }


    if ($_POST["multiple"] == "true") {
      $files = [];

      foreach ($_FILES['file'] as $key => $val) {
        for ($i = 0; $i < count($val); $i++) {
          $files[$i][$key] = $val[$i];
        }
      }

      $return_files = [];
      $files_num = count($files);

      foreach ($files as $file) {
        // valid inputs
        if (!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
          if ($files_num > 1) {
            continue;
          } else {
            modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
          }
        }

        // check file size
        if ($file["size"] > $max_allowed_size) {
          if ($files_num > 1) {
            continue;
          } else {
            modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
          }
        }

        /* prepare new file name */
        $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
        $prefix = $system['uploads_prefix'] . '_' . get_hash_token();
        $image = new Image($file["tmp_name"]);
//        $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
        $image_new_name = $directory . str_replace('guomedia','img',$prefix) . $image->_img_ext;
//        $path_new = $depth . $system['uploads_directory'] . '/' . $image_new_name;

        $tmpImageName = tempnam("/tmp", "img");

        /* save the new image */
        $image->save($tmpImageName, $file['tmp_name']);

        /* delete the tmp image */
        unlink($file['tmp_name']);
		
		//if($user->_data['user_name'] == 'apatle'){
			
		/***** Start Image Thumbnail Code *****/
			
			if(!AwsS3::upload($tmpImageName, $image_new_name)){
			  modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file s3"));
			}
			
			$sm_image = new SimpleImage();
			
			$uploaded_file = 'https://d57iplyuvntm7.cloudfront.net/uploads/'.$directory . str_replace('guomedia','img',$prefix) . $image->_img_ext;
			//$files=$_FILES;
			//$form_name="image";
			/*$thumb_tmp_path="../content/uploads/tmp/";
			//$upload_path="user/";//s3 buket folder
			$name=$file['name'];
			$i_array=explode(".",$name);
			$ext=end($i_array);
			$size=$file['size'];
			$tmp=$file['tmp_name'];
			$names=time().$name;*/
			
			$s_img = $sm_image
			->fromFile($uploaded_file)              // load parrot.jpg
			->autoOrient()                        // adjust orientation based on exif data
			->bestFit(400, 250)                   // proportinoally resize to fit inside a 250x400 box
			->toString();  
			
			
			$thumb_directory = 'photos/thumb/';
			$thumb_image_new_name = $thumb_directory . str_replace('guomedia','img',$prefix) . $image->_img_ext;
			
			//make_thumb($uploaded_file,$thumb_tmp_path.$names,400,$ext);
			
			if(!AwsS3::upload_thumbnail($s_img, $thumb_image_new_name)){
			  modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file s3"));
			}
			
			//unlink($thumb_tmp_path.$names);
			
		/***** End Image Thumbnail Code ******/
		//}
		//else{	
			//if(!AwsS3::upload($tmpImageName, $image_new_name)){
			  //modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file s3"));
			//}
		//}	

        unlink($tmpImageName);

        /* return */
        $return_files[] = $image_new_name;
      }

      // return the return_files & exit
      return_json(["files" => $return_files]);
    } else {
      // valid inputs
      if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
        modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
      }

      // check file size
      if ($_FILES["file"]["size"] > $max_allowed_size) {
        modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
      }

      /* prepare new file name */
      $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
      $prefix = $system['uploads_prefix'] . '_' . get_hash_token();
      $image = new Image($_FILES["file"]["tmp_name"]);

      $image_new_name = $directory . str_replace('guomedia','img',$prefix) . $image->_img_ext;
      $path_new = tempnam("/tmp", "img");

      /* save the new image */
      $resize = ($_POST['handle'] == 'x-image') ? false : true;
      $image->save($path_new, $_FILES['file']['tmp_name'], $resize);

      /* delete the tmp image */
      unlink($_FILES['file']['tmp_name']);

      /* upload to amazon s3 */
      if(!AwsS3::upload($path_new, $image_new_name)){
        modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
      }
	  
	  /*************** Start Upload Image Thumbnail **************/
		$sm_image = new SimpleImage();
			
		$uploaded_file = 'https://d57iplyuvntm7.cloudfront.net/uploads/'.$directory . str_replace('guomedia','img',$prefix) . $image->_img_ext;
		
		$s_img = $sm_image
		->fromFile($uploaded_file)              // load parrot.jpg
		->autoOrient()                        // adjust orientation based on exif data
		->bestFit(400, 250)                   // proportinoally resize to fit inside a 250x400 box
		->toString(); 
		
		
		$thumb_directory = 'photos/thumb/';
		$thumb_image_new_name = $thumb_directory . str_replace('guomedia','img',$prefix) . $image->_img_ext;
		
		//make_thumb($uploaded_file,$thumb_tmp_path.$names,400,$ext);
		
		if(!AwsS3::upload_thumbnail($s_img, $thumb_image_new_name)){
		  modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file s3"));
		}
	  /*************** End Upload Image Thumbnail ***************/
	  
      unlink($path_new);

      // check the handle
      switch ($_POST['handle']) {
      case 'cover-user':
        /* check for cover album */
        if (!$user->_data['user_album_covers']) {
          /* create new cover album */
          $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title, privacy) VALUES (%s, 'user', 'Cover Photos', 'public')", secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
          $user->_data['user_album_covers'] = $db->insert_id;

          /* update user cover album id */
          $db->query(sprintf("UPDATE users SET user_album_covers = %s WHERE user_id = %s", secure($user->_data['user_album_covers'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }

        /* insert updated cover photo post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, time, privacy) VALUES (%s, 'user', 'profile_cover', %s, 'public')", secure($user->_data['user_id'], 'int'), secure($date))) or _error(SQL_ERROR_THROWEN);
        $post_id = $db->insert_id;

        /* insert new cover photo to album */
        $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($user->_data['user_album_covers'], 'int'), secure($image_new_name))) or _error(SQL_ERROR_THROWEN);
        $photo_id = $db->insert_id;

        /* update user cover */
        $db->query(sprintf("UPDATE users SET user_cover = %s, user_cover_id = %s WHERE user_id = %s", secure($image_new_name), secure($photo_id, 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        break;

      case 'picture-user':
        /* check for profile pictures album */

        if (!$user->_data['user_album_pictures']) {
          /* create new profile pictures album */
          $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title, privacy) VALUES (%s, 'user', 'Profile Pictures', 'public')", secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
          $user->_data['user_album_pictures'] = $db->insert_id;

          /* update user profile picture album id */
          $db->query(sprintf("UPDATE users SET user_album_pictures = %s WHERE user_id = %s", secure($user->_data['user_album_pictures'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }

        /* insert updated profile picture post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, time, privacy) VALUES (%s, 'user', 'profile_picture', %s, 'public')", secure($user->_data['user_id'], 'int'), secure($date))) or _error(SQL_ERROR_THROWEN);
        $post_id = $db->insert_id;

        /* insert new profile picture to album */
        $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($user->_data['user_album_pictures'], 'int'), secure($image_new_name))) or _error(SQL_ERROR_THROWEN);
        $photo_id = $db->insert_id;

        /* update user profile picture */
        $db->query(sprintf("UPDATE users SET user_picture = %s, user_picture_id = %s WHERE user_id = %s", secure($image_new_name), secure($photo_id, 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

        $get_User = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s", secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $result_user = mysqli_fetch_array($get_User, MYSQLI_ASSOC);

        if (($result_user['sendbird_access_token']) && $result_user['sendbird_access_token'] != '') {
          $user_pic = 'https://s3.amazonaws.com/kwok7s/uploads/' . $result_user['user_picture'];
          $service_url = 'https://api.sendbird.com/v3/users/' . $result_user['uuid'];
          $curl_post_data = [
            "nickname" => $result_user['user_firstname'],
            "issue_access_token" => "false",
            "profile_url" => $user_pic
          ];

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
          curl_setopt($ch, CURLOPT_URL, $service_url);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_post_data);  //Post Fields
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          //TODO ????
          $headers = [
            'Api-Token:7bf48344bd96a621a861962257b05d0c7c7ab52f'
          ];

          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          $server_output = curl_exec($ch);
          curl_close($ch);
        }
        break;

      case 'cover-page':
        /* check if page id is set */
        if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
          /* delete the uploaded image & return error 403 */
          unlink($path);
          _error(403);
        }

        /* check if the user is the page admin */
        $get_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s AND page_admin = %s", secure($_POST['id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

        if ($get_page->num_rows == 0) {
          /* delete the uploaded image & return error 403 */
          unlink($path);
          _error(403);
        }

        $page = $get_page->fetch_assoc();

        /* check for cover album */
        if (!$page['page_album_covers']) {
          /* create new cover album */
          $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title, privacy) VALUES (%s, 'page', 'Cover Photos', 'public')", secure($page['page_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
          $page['page_album_covers'] = $db->insert_id;

          /* update page cover album id */
          $db->query(sprintf("UPDATE pages SET page_album_covers = %s WHERE page_id = %s", secure($page['page_album_covers'], 'int'), secure($page['page_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }

        /* insert updated cover photo post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, time, privacy) VALUES (%s, 'page', 'page_cover', %s, 'public')", secure($page['page_id'], 'int'), secure($date))) or _error(SQL_ERROR_THROWEN);
        $post_id = $db->insert_id;

        /* insert new cover photo to album */
        $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($page['page_album_covers'], 'int'), secure($image_new_name))) or _error(SQL_ERROR_THROWEN);
        $photo_id = $db->insert_id;

        /* update page cover */
        $db->query(sprintf("UPDATE pages SET page_cover = %s, page_cover_id = %s WHERE page_id = %s", secure($image_new_name), secure($photo_id, 'int'), secure($page['page_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        break;

      case 'picture-page':
        /* check if page id is set */
        if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
          /* delete the uploaded image & return error 403 */
          unlink($path);
          _error(403);
        }

        /* check if the user is the page admin */
        $get_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s AND page_admin = %s", secure($_POST['id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_page->num_rows == 0) {
          /* delete the uploaded image & return error 403 */
          unlink($path);
          _error(403);
        }

        $page = $get_page->fetch_assoc();

        /* check for page pictures album */
        if (!$page['page_album_pictures']) {
          /* create new page pictures album */
          $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title, privacy) VALUES (%s, 'page', 'Profile Pictures', 'public')", secure($page['page_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
          $page['page_album_pictures'] = $db->insert_id;
          /* update page profile picture album id */
          $db->query(sprintf("UPDATE pages SET page_album_pictures = %s WHERE page_id = %s", secure($page['page_album_pictures'], 'int'), secure($page['page_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }

        /* insert updated page picture post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, time, privacy) VALUES (%s, 'page', 'page_picture', %s, 'public')", secure($page['page_id'], 'int'), secure($date))) or _error(SQL_ERROR_THROWEN);
        $post_id = $db->insert_id;

        /* insert new page picture to album */
        $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($page['page_album_pictures'], 'int'), secure($image_new_name))) or _error(SQL_ERROR_THROWEN);
        $photo_id = $db->insert_id;

        /* update page picture */
        $db->query(sprintf("UPDATE pages SET page_picture = %s, page_picture_id = %s WHERE page_id = %s", secure($image_new_name), secure($photo_id, 'int'), secure($page['page_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        break;

      case 'cover-group':
        /* check if group id is set */
        if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
          /* delete the uploaded image & return error 403 */
          unlink($path);
          _error(403);
        }

        /* check if the user is the group admin */
        $get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s AND group_admin = %s", secure($_POST['id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_group->num_rows == 0) {
          /* delete the uploaded image & return error 403 */
          unlink($path);
          _error(403);
        }

        $group = $get_group->fetch_assoc();

        /* check for group covers album */
        if (!$group['group_album_covers']) {
          /* create new group covers album */
          $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, in_group, group_id, title, privacy) VALUES (%s, 'user', '1', %s, 'Cover Photos', 'public')", secure($user->_data['user_id'], 'int'), secure($group['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
          $group['group_album_covers'] = $db->insert_id;
          /* update group cover album id */
          $db->query(sprintf("UPDATE groups SET group_album_covers = %s WHERE group_id = %s", secure($group['group_album_covers'], 'int'), secure($group['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }

        /* insert updated group cover post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, in_group, group_id, time, privacy) VALUES (%s, 'user', 'group_cover', '1', %s, %s, 'custom')", secure($user->_data['user_id'], 'int'), secure($group['group_id'], 'int'), secure($date))) or _error(SQL_ERROR_THROWEN);
        $post_id = $db->insert_id;

        /* insert new group cover to album */
        $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($group['group_album_covers'], 'int'), secure($image_new_name))) or _error(SQL_ERROR_THROWEN);
        $photo_id = $db->insert_id;

        /* update group cover */
        $db->query(sprintf("UPDATE groups SET group_cover = %s, group_cover_id = %s WHERE group_id = %s", secure($image_new_name), secure($photo_id, 'int'), secure($group['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        break;

      case 'picture-group':
        /* check if group id is set */
        if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
          /* delete the uploaded image & return error 403 */
          unlink($path);
          _error(403);
        }

        /* check if the user is the group admin */
        $get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s AND group_admin = %s", secure($_POST['id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_group->num_rows == 0) {
          /* delete the uploaded image & return error 403 */
          unlink($path);
          _error(403);
        }

        $group = $get_group->fetch_assoc();

        /* check for group pictures album */
        if (!$group['group_album_pictures']) {
          /* create new group pictures album */
          $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, in_group, group_id, title, privacy) VALUES (%s, 'user', '1', %s, 'Profile Pictures', 'public')", secure($user->_data['user_id'], 'int'), secure($group['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
          $group['group_album_pictures'] = $db->insert_id;

          /* update group profile picture album id */
          $db->query(sprintf("UPDATE groups SET group_album_pictures = %s WHERE group_id = %s", secure($group['group_album_pictures'], 'int'), secure($group['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }

        /* insert updated group picture post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, in_group, group_id, time, privacy) VALUES (%s, 'user', 'group_picture', '1', %s, %s, 'custom')", secure($user->_data['user_id'], 'int'), secure($group['group_id'], 'int'), secure($date))) or _error(SQL_ERROR_THROWEN);
        $post_id = $db->insert_id;

        /* insert new group picture to album */
        $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($group['group_album_pictures'], 'int'), secure($image_new_name))) or _error(SQL_ERROR_THROWEN);
        $photo_id = $db->insert_id;

        /* update group picture */
        $db->query(sprintf("UPDATE groups SET group_picture = %s, group_picture_id = %s WHERE group_id = %s", secure($image_new_name), secure($photo_id, 'int'), secure($group['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        break;

      case 'cover-event':

        /* check if event id is set */
        if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
          /* delete the uploaded image & return error 403 */
          unlink($path);
          _error(403);
        }

        /* check if the user is the event admin */
        $get_event = $db->query(sprintf("SELECT * FROM events WHERE event_id = %s AND event_admin = %s", secure($_POST['id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

        if ($get_event->num_rows == 0) {
          /* delete the uploaded image & return error 403 */
          unlink($path);
          _error(403);
        }

        $event = $get_event->fetch_assoc();
        /* check for event covers album */

        if (!$event['event_album_covers']) {
          /* create new event covers album */
          $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, in_event, event_id, title, privacy) VALUES (%s, 'user', '1', %s, 'Cover Photos', 'public')", secure($user->_data['user_id'], 'int'), secure($event['event_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
          $event['event_album_covers'] = $db->insert_id;
          /* update event cover album id */
          $db->query(sprintf("UPDATE events SET event_album_covers = %s WHERE event_id = %s", secure($event['event_album_covers'], 'int'), secure($event['event_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }

        /* insert updated event cover post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, in_event, event_id, time, privacy) VALUES (%s, 'user', 'event_cover', '1', %s, %s, 'custom')", secure($user->_data['user_id'], 'int'), secure($event['event_id'], 'int'), secure($date))) or _error(SQL_ERROR_THROWEN);
        $post_id = $db->insert_id;

        /* insert new event cover to album */
        $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($event['event_album_covers'], 'int'), secure($image_new_name))) or _error(SQL_ERROR_THROWEN);
        $photo_id = $db->insert_id;

        /* update event cover */
        $db->query(sprintf("UPDATE events SET event_cover = %s, event_cover_id = %s WHERE event_id = %s", secure($image_new_name), secure($photo_id, 'int'), secure($event['event_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        break;
      }

      // return the file new name & exit
      return_json(["file" => $image_new_name]);
    }
    break;
  case 'video':
    // check video upload enabled
    if (!$system['videos_enabled']) {
      modal(MESSAGE, __("Not Allowed"), __("This feature has been disabled"));
    }

    //echo'<pre>';print_r($system);die;

    // valid inputs
    if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
      modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
    }

    // check file size
    $max_allowed_size = $system['max_video_size'] * 1024;

    if ($_FILES["file"]["size"] > $max_allowed_size) {
      modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
    }

    // check file extesnion
    $extension = get_extension($_FILES['file']['name']);

    if (!valid_extension($extension, $system['video_extensions'])) {
      modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
    }

    
    /* check & create uploads dir */
    /*$folder = 'videos';

    //prepare new file name
    $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
    $prefix = $system['uploads_prefix'] . '_' . get_hash_token();
    $file_name = $directory . $prefix . '.' . $extension;

    if(!AwsS3::upload($_FILES['file']['tmp_name'], $file_name)){
      modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
    }*/
	
		$folder = 'uploads/videos';
			
		$directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
		$prefix = $system['uploads_prefix'] . '_' . get_hash_token();
		$file_name = $directory . $prefix . '.' . $extension;
						
		$bucket = 'test-guo-media1';		
		$keyname = $file_name;
		$filename =$_FILES['file']['tmp_name'];
		
			$s3 = Aws\S3\S3Client::factory(array(
					'version'    => 'latest',
					'region'      => 'us-east-1',
					'credentials' => array(
						'key'    => 'AKIAIOD6UCOTIYB7BBSQ',
						'secret' => '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'
					),
					'use_accelerate_endpoint' => true
				));

			$result = $s3->createMultipartUpload([
				'Bucket'       => $bucket,
				'Key'          => $keyname,
				'StorageClass' => 'STANDARD',
				'ACL'          => 'public-read',
				'Metadata'     => [
					'param1' => 'value 1',
					'param2' => 'value 2',
					'param3' => 'value 3'
				]
			]);
		  $uploadId = $result['UploadId'];

			// Upload the file in parts.
			try {
				$file = fopen($filename, 'r');
				$partNumber = 1;
				while (!feof($file)) {
					$result = $s3->uploadPart([
						'Bucket'     => $bucket,
						'Key'        => $keyname,
						'UploadId'   => $uploadId,
						'PartNumber' => $partNumber,
						'Body'       => fread($file, 5 * 1024 * 1024),
					]);
					$parts['Parts'][$partNumber] = [
						'PartNumber' => $partNumber,
						'ETag' => $result['ETag'],
					];
					$partNumber++;

					//echo "Uploading part {$partNumber} of {$filename}." . PHP_EOL;
				}
				fclose($file);
			} catch (S3Exception $e) {
				$result = $s3->abortMultipartUpload([
					'Bucket'   => $bucket,
					'Key'      => $keyname,
					'UploadId' => $uploadId
				]);

				//echo "Upload of {$filename} failed." . PHP_EOL;
			}

      
			// Complete the multipart upload.
			$result = $s3->completeMultipartUpload([
				'Bucket'   => $bucket,
				'Key'      => $keyname,
				'UploadId' => $uploadId,
				'MultipartUpload'    => $parts,
			]);
			$url = $result['Location'];

      
			//echo "Uploaded {$filename} to {$url}." . PHP_EOL;*/

     /***************************************************************************************************************** 
     * Note: In FFMpeg there was permission issue on SERVER so Given permission to FFMpeg folder and issue is resolved 
     *****************************************************************************************************************/
		$ffmpeg = FFMpeg\FFMpeg::create(array(
		'ffmpeg.binaries'  => realpath(__DIR__ . '/../..').'/ffmpeg/bin/ffmpeg',
		'ffprobe.binaries' => realpath(__DIR__ . '/../..').'/ffmpeg/bin/ffprobe',
		'timeout'          => 3600, // The timeout for the underlying process
		'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
		), $logger);
    
    
		$sec = 2;
		
		$movie = 'https://d57iplyuvntm7.cloudfront.net/'.$keyname;
		
		$thumbnail = realpath(__DIR__ . '/../..').'/ffmpeg/'.$prefix.'.jpg';
		
		$video = $ffmpeg->open($movie);
		
		$frame = $video->frame(FFMpeg\Coordinate\TimeCode::fromSeconds($sec));
		$frame->save($thumbnail);
		
		chmod($thumbnail,0777);
		
		$video_thumb_folder = 'photos/thumb/';
		$video_thumb_filename = $video_thumb_folder . $prefix . '.jpg';
		
		$sm_image = new SimpleImage();
		
		$uploaded_file = realpath(__DIR__ . '/../..').'/ffmpeg/'.$prefix.'.jpg';
		
		$s_img = $sm_image
		->fromFile($uploaded_file)              // load parrot.jpg
		->autoOrient()                        // adjust orientation based on exif data
		->bestFit(400, 250)                   // proportinoally resize to fit inside a 250x400 box
		->toString(); 
		
		if(!AwsS3::upload_thumbnail($s_img, $video_thumb_filename)){
		  modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file s3"));
		}
		
		unlink($_FILES['file']['tmp_name']);
		
		unlink(realpath(__DIR__ . '/../..').'/ffmpeg/'.$prefix.'.jpg');
		
		//File Name For Database
		$folder = 'videos';

		//prepare new file name for databse
		$directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
		
		$file_name = $directory . $prefix . '.' . $extension;
		
		
    // return the file new name & exit
    $return_array = [
      'name' => $_FILES['file']['name'],
      'size' => $_FILES['file']['size'],
      'file' => $file_name
    ];

    return_json($return_array);
    break;
  case 'audio':
    // check audio upload enabled
    if (!$system['audio_enabled']) {
      modal(MESSAGE, __("Not Allowed"), __("This feature has been disabled"));
    }

    // valid inputs
    if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
      modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
    }

    // check file size
    $max_allowed_size = $system['max_audio_size'] * 1024;

    if ($_FILES["file"]["size"] > $max_allowed_size) {
      modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
    }

    // check file extesnion
    $extension = get_extension($_FILES['file']['name']);

    if (!valid_extension($extension, $system['audio_extensions'])) {
      modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
    }

    /* check & create uploads dir */
    $folder = 'sounds';
    /* prepare new file name */
    $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
    $prefix = $system['uploads_prefix'] . '_' . get_hash_token();
    $file_name = $directory . $prefix . '.' . $extension;

    if(!AwsS3::upload($_FILES['file']['tmp_name'], $file_name)){
      modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
    }

    unlink($_FILES['file']['tmp_name']);

    // return the file new name & exit
    $return_array = [
      'name' => $_FILES['file']['name'],
      'size' => $_FILES['file']['size'],
      'file' => $file_name
    ];

    return_json($return_array);
    break;
  case 'file':
   // check video upload enabled
	if (!$system['file_enabled']) {
      modal(MESSAGE, __("Not Allowed"), __("This feature has been disabled"));
    }

    // valid inputs
    if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
      modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
    }

    // check file size
    $max_allowed_size = $system['max_file_size'] * 1024;

    if ($_FILES["file"]["size"] > $max_allowed_size) {
      modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
    }

    // check file extesnion
    $extension = get_extension($_FILES['file']['name']);

    if (!valid_extension($extension, $system['file_extensions'])) {
      modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
    }

    /* check & create uploads dir */
    $folder = 'videos';

    /* prepare new file name */
    $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
    $prefix = $system['uploads_prefix'] . '_' . get_hash_token();
    $file_name = $directory . $prefix . '.' . $extension;

    if(!AwsS3::upload($_FILES['file']['tmp_name'], $file_name)){
      modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
    }

    unlink($_FILES['file']['tmp_name']);

    // return the file new name & exit
    $return_array = [
      'name' => $_FILES['file']['name'],
      'size' => $_FILES['file']['size'],
      'file' => $file_name
    ];

    return_json($return_array);
    break;
	
	case 'take-video':
    // check video upload enabled
    if (!$system['videos_enabled']) {
      modal(MESSAGE, __("Not Allowed"), __("This feature has been disabled"));
    }

    // valid inputs
    if (!isset($_FILES["video-blob"]) || $_FILES["video-blob"]["error"] != UPLOAD_ERR_OK) {
      modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
    }

    // check file size
    $max_allowed_size = $system['max_video_size'] * 1024;

    if ($_FILES["video-blob"]["size"] > $max_allowed_size) {
      modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
    }

    // check file extesnion
    $extension = get_extension($_FILES['video-blob']['name']);

    if (!valid_extension($extension, $system['video_extensions'])) {
      modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
    }
    
		$folder = 'uploads/videos';
			
		$directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
		$prefix = $system['uploads_prefix'] . '_' . get_hash_token();
		$file_name = $directory . $prefix . '.' . $extension;
						
		$bucket = 'test-guo-media1';		
		$keyname = $file_name;
		$filename =$_FILES['video-blob']['tmp_name'];
		
			$s3 = Aws\S3\S3Client::factory(array(
					'version'    => 'latest',
					'region'      => 'us-east-1',
					'credentials' => array(
						'key'    => 'AKIAIOD6UCOTIYB7BBSQ',
						'secret' => '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'
					),
					'use_accelerate_endpoint' => true
				));

			$result = $s3->createMultipartUpload([
				'Bucket'       => $bucket,
				'Key'          => $keyname,
				'StorageClass' => 'STANDARD',
				'ACL'          => 'public-read',
				'Metadata'     => [
					'param1' => 'value 1',
					'param2' => 'value 2',
					'param3' => 'value 3'
				]
			]);
			$uploadId = $result['UploadId'];

			// Upload the file in parts.
			try {
				$file = fopen($filename, 'r');
				$partNumber = 1;
				while (!feof($file)) {
					$result = $s3->uploadPart([
						'Bucket'     => $bucket,
						'Key'        => $keyname,
						'UploadId'   => $uploadId,
						'PartNumber' => $partNumber,
						'Body'       => fread($file, 5 * 1024 * 1024),
					]);
					$parts['Parts'][$partNumber] = [
						'PartNumber' => $partNumber,
						'ETag' => $result['ETag'],
					];
					$partNumber++;

					//echo "Uploading part {$partNumber} of {$filename}." . PHP_EOL;
				}
				fclose($file);
			} catch (S3Exception $e) {
				$result = $s3->abortMultipartUpload([
					'Bucket'   => $bucket,
					'Key'      => $keyname,
					'UploadId' => $uploadId
				]);

				//echo "Upload of {$filename} failed." . PHP_EOL;
			}

			// Complete the multipart upload.
			$result = $s3->completeMultipartUpload([
				'Bucket'   => $bucket,
				'Key'      => $keyname,
				'UploadId' => $uploadId,
				'MultipartUpload'    => $parts,
			]);
			$url = $result['Location'];

			//echo "Uploaded {$filename} to {$url}." . PHP_EOL;*/
	
		$ffmpeg = FFMpeg\FFMpeg::create(array(
		'ffmpeg.binaries'  => realpath(__DIR__ . '/../..').'/ffmpeg/bin/ffmpeg',
		'ffprobe.binaries' => realpath(__DIR__ . '/../..').'/ffmpeg/bin/ffprobe',
		'timeout'          => 3600, // The timeout for the underlying process
			'ffmpeg.threads'   => 2,   // The number of threads that FFMpeg should use
		), $logger);
		
		$sec = 2;
		
		$movie = 'https://d57iplyuvntm7.cloudfront.net/'.$keyname;
		
		$thumbnail = realpath(__DIR__ . '/../..').'/ffmpeg/'.$prefix.'.jpg';
		
		$video = $ffmpeg->open($movie);
		
		$frame = $video->frame(FFMpeg\Coordinate\TimeCode::fromSeconds($sec));
		$frame->save($thumbnail);
		
		chmod($thumbnail,0777);
		
		$video_thumb_folder = 'photos/thumb/';
		$video_thumb_filename = $video_thumb_folder . $prefix . '.jpg';
		
		$sm_image = new SimpleImage();
		
		$uploaded_file = realpath(__DIR__ . '/../..').'/ffmpeg/'.$prefix.'.jpg';
		
		$s_img = $sm_image
		->fromFile($uploaded_file)              // load parrot.jpg
		->autoOrient()                        // adjust orientation based on exif data
		->bestFit(400, 250)                   // proportinoally resize to fit inside a 250x400 box
		->toString(); 
		
		if(!AwsS3::upload_thumbnail($s_img, $video_thumb_filename)){
		  modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file s3"));
		}
		
		unlink($_FILES['video-blob']['tmp_name']);
		
		unlink(realpath(__DIR__ . '/../..').'/ffmpeg/'.$prefix.'.jpg');
		
		//File Name For Database
		$folder = 'videos';

		//prepare new file name for databse
		$directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
		
		$file_name = $directory . $prefix . '.' . $extension;
		
		
    // return the file new name & exit
    $return_array = [
      'name' => $_FILES['video-blob']['name'],
      'size' => $_FILES['video-blob']['size'],
      'file' => $file_name
    ];
	
	//print_r($return_array);exit;
    return_json($return_array);
    break;
	
	case 'take-photo':
		
		/* check & create uploads dir */
		$folder = 'photos';
		$depth = __DIR__ . '/../../../';
	
		if (!file_exists($depth . $system['uploads_directory'] . '/' . $folder)) {
		  @mkdir($depth . $system['uploads_directory'] . '/' . $folder, 0777, true);
		}

		if (!file_exists($depth . $system['uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
		  @mkdir($depth . $system['uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
		}

		if (!file_exists($system['uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
		  @mkdir($depth . $system['uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
		}
		
		/*$files = [];

		  foreach ($_FILES['photo-blob'] as $key => $val) {
			for ($i = 0; $i < count($val); $i++) {
			  $files[$i][$key] = $val[$i];
			}
		  }*/

		  $return_files = [];
		 // $files_num = count($files);

		  //foreach ($files as $file) {
			// valid inputs
			/*if (!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
			  if ($files_num > 1) {
				continue;
			  } else {
				modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
			  }
			}*/

			// check file size
			//if ($_FILES['photo-blob']["size"] > $max_allowed_size) {
			  //modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
			//}

			/* prepare new file name */
			$directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
			$prefix = $system['uploads_prefix'] . '_' . get_hash_token();
			$image = new Image($_FILES['photo-blob']['tmp_name']);
	//        $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
			$image_new_name = $directory . str_replace('guomedia','img',$prefix) . $image->_img_ext;
	//        $path_new = $depth . $system['uploads_directory'] . '/' . $image_new_name;

			$tmpImageName = tempnam("/tmp", "img");

			/* save the new image */
			$image->save($tmpImageName, $_FILES['photo-blob']['tmp_name']);

			/* delete the tmp image */
			unlink($_FILES['photo-blob']['tmp_name']);
			
			//if($user->_data['user_name'] == 'apatle'){
				
			/***** Start Image Thumbnail Code *****/
				
				if(!AwsS3::upload($tmpImageName, $image_new_name)){
				  modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file s3"));
				}
				
				$sm_image = new SimpleImage();
				
				$uploaded_file = 'https://d57iplyuvntm7.cloudfront.net/uploads/'.$directory . str_replace('guomedia','img',$prefix) . $image->_img_ext;
				//$files=$_FILES;
				//$form_name="image";
				/*$thumb_tmp_path="../content/uploads/tmp/";
				//$upload_path="user/";//s3 buket folder
				$name=$file['name'];
				$i_array=explode(".",$name);
				$ext=end($i_array);
				$size=$file['size'];
				$tmp=$file['tmp_name'];
				$names=time().$name;*/
				
				$s_img = $sm_image
				->fromFile($uploaded_file)              // load parrot.jpg
				->autoOrient()                        // adjust orientation based on exif data
				->bestFit(400, 250)                   // proportinoally resize to fit inside a 250x400 box
				->toString();  
				
				
				$thumb_directory = 'photos/thumb/';
				$thumb_image_new_name = $thumb_directory . str_replace('guomedia','img',$prefix) . $image->_img_ext;
				
				//make_thumb($uploaded_file,$thumb_tmp_path.$names,400,$ext);
				
				if(!AwsS3::upload_thumbnail($s_img, $thumb_image_new_name)){
				  modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file s3"));
				}
				
				//unlink($thumb_tmp_path.$names);
				
			/***** End Image Thumbnail Code ******/
			//}
			//else{	
				//if(!AwsS3::upload($tmpImageName, $image_new_name)){
				  //modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file s3"));
				//}
			//}	

			unlink($tmpImageName);

			/* return */
			$return_files[] = $image_new_name;
		  //}
				
      // return the return_files & exit
      return_json(["files" => $return_files]);
	
	break;
  }
} catch (Exception $e) {
  modal(ERROR, __("Error"), $e->getMessage());
}
