<?php
/**
 * ajax -> data -> reset
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if (!$user->_logged_in) {
    modal(LOGIN);
}

// valid inputs
$valid['reset'] = ['friend_requests', 'messages', 'notifications'];
if (!isset($_POST['reset']) || !in_array($_POST['reset'], $valid['reset'])) {
    _error(400);
}

// reset live counters
try {
    $user->live_counters_reset($_POST['reset']);
    return_json();
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
