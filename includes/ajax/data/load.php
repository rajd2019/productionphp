<?php

require_once(__DIR__ . '/../../../bootstrap.php');
is_ajax();
//user_access(true);

if (!isset($_POST['offset']) || !is_numeric($_POST['offset'])) {
  _error(400);
}

try { 
  $return = array();
  $append = true;

  if ($_POST['get'] == "newsfeed" || $_POST['get'] == "saved") {

    $data = $user->get_posts(['get' => $_POST['get'], 'filter' => $_POST['filter'], 'offset' => $_POST['offset']]);

    // get ads
    switch ($_POST['offset']) {
      case '1':
        $ads = $user->ads('newfeed_1');
        break;
      case '2':
        $ads = $user->ads('newfeed_2');
        break;
      case '3':
        $ads = $user->ads('newfeed_3');
        break;
    }

    $smarty->assign('ads', $ads);

  } elseif ($_POST['get'] == "newsfeed1") {

    $search = $user->search(array('get' => $_POST['get'], 'filter' => $_POST['filter'], 'offset' => $_POST['offset']));
    $data = $search["posts"];

    // get ads
    switch ($_POST['offset']) {
      case '1':
        $ads = $user->ads('newfeed_1');
        break;

      case '2':
        $ads = $user->ads('newfeed_2');
        break;

      case '3':
        $ads = $user->ads('newfeed_3');
        break;
    }

    $smarty->assign('ads', $ads);

  } elseif ($_POST['get'] == "posts_profile" || $_POST['get'] == "posts_page" || $_POST['get'] == "posts_group" || $_POST['get'] == "posts_event") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->get_posts(['get' => $_POST['get'], 'filter' => $_POST['filter'], 'offset' => $_POST['offset'], 'id' => $_POST['id']]);

    // get ads
    switch ($_POST['offset']) {
      case '1':
        $ads = $user->ads('newfeed_1');
        break;
      case '2':
        $ads = $user->ads('newfeed_2');
        break;
      case '3':
        $ads = $user->ads('newfeed_3');
        break;
    }

    $smarty->assign('ads', $ads);

  } elseif ($_POST['get'] == "shares") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->who_shares($_POST['id'], $_POST['offset']);

  } elseif ($_POST['get'] == "articles") {

    $data = $user->get_articles(array('offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "post_comments") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $append = false;
    $data = $user->get_comments($_POST['id'], $_POST['offset'], true, false);

  } elseif ($_POST['get'] == "photo_comments") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $append = false;
    $data = $user->get_comments($_POST['id'], $_POST['offset'], false, false);

  } elseif ($_POST['get'] == "comment_replies") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $append = false;
    $data = $user->get_replies($_POST['id'], $_POST['offset'], false);

  } elseif ($_POST['get'] == "photos") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->get_photos($_POST['id'], $_POST['type'], $_POST['offset'], false);
    $context = ($_POST['type'] == "album") ? "album" : "photos";
    $smarty->assign('context', $context);

  } elseif ($_POST['get'] == "albums") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->get_albums($_POST['id'], $_POST['type'], $_POST['offset']);

  } elseif ($_POST['get'] == "post_likes") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->who_likes(array('post_id' => $_POST['id'], 'offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "photo_likes") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->who_likes(array('photo_id' => $_POST['id'], 'offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "comment_likes") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->who_likes(array('comment_id' => $_POST['id'], 'offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "voters") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->who_votes($_POST['id'], $_POST['offset']);

  } elseif ($_POST['get'] == "blocks") {

    $data = $user->get_blocked($_POST['offset']);

  } elseif ($_POST['get'] == "affiliates") {

    if (!isset($_POST['uid']) || !is_numeric($_POST['uid'])) {
      _error(400);
    }

    $data = $user->get_affiliates($_POST['uid'], $_POST['offset']);

  } elseif ($_POST['get'] == "friend_requests") {

    $data = $user->get_friend_requests($_POST['offset']);

  } elseif ($_POST['get'] == "friend_requests_sent") {

    $data = $user->get_friend_requests_sent($_POST['offset']);

  } elseif ($_POST['get'] == "mutual_friends") {

    if (!isset($_POST['uid']) || !is_numeric($_POST['uid'])) {
      _error(400);
    }

    $data = $user->get_mutual_friends($_POST['uid'], $_POST['offset']);

  } elseif ($_POST['get'] == "new_people") {

    $data = $user->get_new_people($_POST['offset']);

  } elseif ($_POST['get'] == "friends") {

    if (!isset($_POST['uid']) || !is_numeric($_POST['uid'])) {
      _error(400);
    }

    $data = $user->get_friends($_POST['uid'], $_POST['offset']);

  } elseif ($_POST['get'] == "followers") {

    if (!isset($_POST['uid']) || !is_numeric($_POST['uid'])) {
      _error(400);
    }

    $data = $user->get_followers($_POST['uid'], $_POST['offset']);

  } elseif ($_POST['get'] == "get_likes") {

    if (!isset($_POST['uid']) || !is_numeric($_POST['uid'])) {
      _error(400);
    }

    $data = $user->get_likes_posts($_POST['uid'], $_POST['offset']);

  } elseif ($_POST['get'] == "get_media") {

    if (!isset($_POST['uid']) || !is_numeric($_POST['uid'])) {
      _error(400);
    }

    $data = $user->get_media_posts($_POST['uid'], $_POST['offset']);
    $video_posts = $user->get_video_posts($_POST['uid'], $_POST['offset']);

    if (count($video_posts) > 0) {
      $smarty->assign('video_posts', $video_posts);
      $return['append'] = $append;
      $return['data'] = $smarty->fetch("ajax.load_more.tpl");
    }

  } elseif ($_POST['get'] == "followings") {

    if (!isset($_POST['uid']) || !is_numeric($_POST['uid'])) {
      _error(400);
    }

    $data = $user->get_followings($_POST['uid'], $_POST['offset']);

  } elseif ($_POST['get'] == "page_invites") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->get_page_invites($_POST['id'], $_POST['offset']);

  } elseif ($_POST['get'] == "group_members") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->get_group_members($_POST['id'], $_POST['offset']);

  } elseif ($_POST['get'] == "group_invites") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->get_group_invites($_POST['id'], $_POST['offset']);

  } elseif ($_POST['get'] == "group_requests") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->get_group_requests($_POST['id'], $_POST['offset']);

  } elseif ($_POST['get'] == "event_going") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->get_event_members($_POST['id'], 'going', $_POST['offset']);

  } elseif ($_POST['get'] == "event_interested") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->get_event_members($_POST['id'], 'interested', $_POST['offset']);

  } elseif ($_POST['get'] == "event_invited") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->get_event_members($_POST['id'], 'invited', $_POST['offset']);

  } elseif ($_POST['get'] == "event_invites") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $data = $user->get_event_invites($_POST['id'], $_POST['offset']);

  } elseif ($_POST['get'] == "pages") {

    $data = $user->get_pages(array('offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "suggested_pages") {

    $data = $user->get_pages(array('suggested' => true, 'offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "liked_pages" || $_POST['get'] == "profile_pages") {

    if (!isset($_POST['uid']) || !is_numeric($_POST['uid'])) {
      _error(400);
    }

    $data = $user->get_pages(array('user_id' => $_POST['uid'], 'offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "groups") {

    $data = $user->get_groups(array('offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "suggested_groups") {

    $data = $user->get_groups(array('suggested' => true, 'offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "joined_groups" || $_POST['get'] == "profile_groups") {

    if (!isset($_POST['uid']) || !is_numeric($_POST['uid'])) {
      _error(400);
    }

    $data = $user->get_groups(array('user_id' => $_POST['uid'], 'offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "events") {

    $data = $user->get_events(array('offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "suggested_events") {

    $data = $user->get_events(array('suggested' => true, 'offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "going_events") {

    $data = $user->get_events(array('filter' => 'going', 'offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "interested_events") {

    $data = $user->get_events(array('filter' => 'interested', 'offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "invited_events") {

    $data = $user->get_events(array('filter' => 'invited', 'offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "profile_events") {

    if (!isset($_POST['uid']) || !is_numeric($_POST['uid'])) {
      _error(400);
    }

    $data = $user->get_events(array('user_id' => $_POST['uid'], 'offset' => $_POST['offset']));

  } elseif ($_POST['get'] == "notifications") {

    $data = $user->get_notifications($_POST['offset']);

  } elseif ($_POST['get'] == "conversations") {

    $data = $user->get_conversations($_POST['offset']);

  } elseif ($_POST['get'] == "messages") {

    if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
      _error(400);
    }

    $append = false;
    $data = $user->get_conversation_messages($_POST['id'], $_POST['offset']);

  } elseif ($_POST['get'] == "games") {

    $data = $user->get_games($_POST['offset']);

  } elseif ($_POST['get'] == "played_games") {

    $data = $user->get_games($_POST['offset'], true);

  } else {

    _error(400);

  }

  // handle data
  if (count($data) > 0) {

    $smarty->assign('offset', $_POST['offset']);
    $smarty->assign('get', $_POST['get']);
    $smarty->assign('data', $data);

    $return['append'] = $append;
    $return['data'] = $smarty->fetch("ajax.load_more.tpl");

  } else {
    $return['data'] = "";
  }


  // return & exit
  return_json($return);

} catch (Exception $e) {

  modal(ERROR, __("Error"), $e->getMessage());

}
