<?php
/**
 * ajax -> data -> mention
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if (!$user->_logged_in) {
    modal(LOGIN);
}



// mention
try {

    $current_user_id = $user->_data['user_id'];
    //echo sprintf("UPDATE users set feature_status = %s where user_id=%s", '0', secure($current_user_id));
    $sql_for_update_feature_status = $db->query(sprintf("UPDATE users set feature_status = %s where user_id=%s", 0, secure($current_user_id))) or _error(SQL_ERROR_THROWEN);
    return_json();
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}