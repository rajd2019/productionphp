<?php
/**
 * ajax -> users -> started
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
is_ajax();

// user access
user_access(true);

// started
try {
    $user->settings('started', $_POST);
    return_json(['success' => true, 'message' => __("Your info has been updated")]);
} catch (Exception $e) {
    return_json(['error' => true, 'message' => $e->getMessage()]);
}
