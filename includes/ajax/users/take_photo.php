<?php


/**
 * ajax -> posts -> share
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
is_ajax();

// user access
user_access(true);

try {
    /*$id = $_GET['post_id'];
    
    if ($_GET['handle'] == 'post') {
        $usersPost = $user->get_post_content($id);
    } else {
        $usersPost = $user->get_post_comment($id);
    }
    
    $smarty->assign('posts', $usersPost);
    $smarty->assign('id', $id);
    $smarty->assign('_handle', $_GET['handle']);
    // initialize the return array
    $return = [];
    // get album*/
	
	//$smarty->assign('username', $user->_data['user_name']);
	//$smarty->assign('user_email', $user->_data['user_email']);
	//$smarty->assign('user_id', $user->_data['user_id']);
	
    $return['content'] = $smarty->fetch("take-photo.tpl");
	$return['callback'] = "$('#modal-web-cam').modal('show'); $('#modal-web-cam .modal-content-modal3').html(response.content);";
    
    // return & exit
    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
