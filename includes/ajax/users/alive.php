<?php
require_once(__DIR__ . '/../../../bootstrap.php');

is_ajax();
user_access(true);

try {
  $user::update_online($user->_data['user_id']);
  return_json();
} catch (Exception $e) {
  modal(ERROR, __("Error"), $e->getMessage());
}
