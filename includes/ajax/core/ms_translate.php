<?php
ini_set("display_errors", false);
error_reporting(0);
// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');
require_once(__DIR__ . '/../../../vendor/autoload.php');
use Aws\Translate\TranslateClient; 
use Aws\Exception\AwsException;

is_ajax();

// user access
//user_access(true);

try {
    
	$detect_lang = ms_detect_language($_POST['post_text']);
	
	if($detect_lang == 'en'){
					
		$get_lang = ms_translate($_POST['post_text'],'zh');
	}
	else
	{
		if($detect_lang == 'zh' ||  $detect_lang == 'zh-TW'){
			
			$get_lang = ms_translate($_POST['post_text'],'en');
		
		}else{	
			$get_lang = ms_translate($_POST['post_text'],'zh');
		}					
	}
	
	//$return['return_language'] = htmlToPlainText($get_lang);
	$return['return_language'] = $get_lang;
	//$return['return_language'] = strip_tags(htmlspecialchars_decode($get_lang,ENT_NOQUOTES));
	
    return_json($return);
	
	
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}

function htmlToPlainText($str){
    $str = str_replace('&nbsp;', ' ', $str);
    $str = html_entity_decode($str, ENT_QUOTES | ENT_COMPAT , 'UTF-8');
    $str = html_entity_decode($str, ENT_HTML5, 'UTF-8');
    $str = html_entity_decode($str);
    $str = htmlspecialchars_decode($str);
    $str = strip_tags($str);

    return $str;
}


//Created by Gulfam => AWS translation To detect the language
function ms_detect_language($post_text){    
	$targetLanguage= zh;
		$textToTranslate = $post_text;
		$client = new Aws\Translate\TranslateClient(array(
			'version'    => 'latest',
			'region'      => 'us-east-1',
			'credentials' => [
				'key' => 'AKIAJQ7WSEHR6H5H7HHA',
				'secret' => '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'
			]
		));
		try {
			$languages = $client->translateText([
			'SourceLanguageCode' => auto,
			'TargetLanguageCode' => $targetLanguage, 
			'Text' => $textToTranslate, 
		]);
		return $languages['SourceLanguageCode'];
		}catch (AwsException $e) {
		// output error message if fails
		echo $e->getMessage();
		echo "\n";
		}
}


//Created by Gulfam => AWS translation To translate the text
function ms_translate($post_text,$detect_lang){    
	$client = new Aws\Translate\TranslateClient(array(
				'version'    => 'latest',
				'region'      => 'us-east-1',
				'credentials' => [
					'key' => 'AKIAJQ7WSEHR6H5H7HHA',
					'secret' => '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'
			]
			));	

		//$currentLanguage = 'en';
		$targetLanguage= $detect_lang;
		$textToTranslate = $post_text;
		try {
		$result = $client->translateText([
			'SourceLanguageCode' => auto,
			'TargetLanguageCode' => $targetLanguage, 
			'Text' => $textToTranslate, 
		]);
		return $result['TranslatedText'];
	}catch (AwsException $e) {
		// output error message if fails
		echo $e->getMessage();
		echo "\n";
		}
}

