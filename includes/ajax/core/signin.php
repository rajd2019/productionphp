<?php
/**
 * ajax -> core -> signin
 *
 * @package Sngine
 * @author Zamblek
 */
global $user;
// set override_shutdown
$override_shutdown = true;

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
is_ajax();

// HoneyPot PHP
   if($_POST['honeypot'] != ''){
		return_json(['error' => true, 'message' => $e->getMessage()]);
   }
   
// check user logged in
if ($user->_logged_in) {
    return_json(['callback' => 'location.href="https://guo.media/";']);
}

// signin
try {
    $remember = (isset($_POST['remember'])) ? true : false;

    $user->sign_in($_POST['username_email'], $_POST['password'], $remember);
    return_json(['callback' => 'location.href="https://guo.media/";']);
} catch (Exception $e) {
    return_json(['error' => true, 'message' => $e->getMessage()]);
}
