<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once(__DIR__ . '/../../../bootstrap.php');


is_ajax();

// user access
user_access(true);

try {
    
	$status = $user->support_submit_msg($_GET);
	
	if($status == 'Success'){
		
		// prepare email
        $subject = $_GET['support_subject'];
        $body  = $_GET['support_message'];       
		
		$mail = new PHPMailer();
		
        /*$mail->setFrom('support@guo.media', 'Guo Media');
		$mail->addAddress('support@guo.media'); 
		$mail->addReplyTo('noreply@guo.media', 'Guo Media');
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = $subject;
		$mail->Body    = $body;
		$mail->send();*/
		
		
        // To send HTML mail, the Content-type header must be set
		$header = "MIME-Version: 1.0\r\n";
		$header .= "Mailer: support@guo.media\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		$from = 'Guo Support <support@guo.media>';
		 
		// Create email headers
		$headers .= 'From: '.$from."\r\n".
			'Reply-To: '.$from."\r\n" .
			'X-Mailer: PHP/' . phpversion();
		 
		// Compose a simple HTML email message
		$message = '<html><body>';
		
		$message .= '<p>'.$body.'</p>';
		$message .= '</body></html>';
		 
		// Sending email
		if(mail('support@guo.media', $subject, $message, $headers)){
			//echo 'Your mail has been sent successfully.';
		} else{
			//echo 'Unable to send email. Please try again.';
		}
		
		
	}
	
	$return['return_status'] = $status;
	
    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
