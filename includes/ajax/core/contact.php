<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
/**
 * ajax -> core -> contact
 *
 * @package Sngine
 * @author Zamblek
 */
// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');
// check AJAX Request
is_ajax();

// contact
try {
    if (isset($_POST['msgtomiles'])) {
        $attachmentOk=null;

        if (is_empty($_POST['message'])) {
            throw new Exception(__("You must fill in all of the required fields"));
        }

        $target_dir = "content/uploads/";
        $target_file = $target_dir . basename($_FILES["upload"]["name"]);
        $uploadOk = 1;
        $attachment = $_FILES["upload"]["tmp_name"];
        if ($attachment) {
            $attachmentOk=1;
        }
        /* check reCAPTCHA */
        if ($system['reCAPTCHA_enabled']) {
            $recaptcha = new \ReCaptcha\ReCaptcha($system['reCAPTCHA_secret_key']);
            $resp = $recaptcha->verify($_POST['g-recaptcha-response'], get_user_ip());
            if (!$resp->isSuccess()) {
                throw new Exception(__("The security check is incorrect. Please try again"));
            }
        }
        // prepare email
        $subject = "New email message from 'Message to Miles' ".$system['system_title'];
        $body  = "<p>Hi Admin,</p>";
        $body .= "<p>You have a new email message </p>";
        $body .= "<p>Email Message: \r\n".$_POST['message']."</p>";
		
		$mail = new PHPMailer(true);
		//$mail->SMTPDebug = 2;                                 // Enable verbose debug output
			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'smtp.office365.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'support@guo.media';                 // SMTP username
			$mail->Password = 'support@2018';                           // SMTP password
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 587;
			
			$mail->setFrom('support@guo.media', 'Guo Support');
			
			$mail->addAddress($system['system_email']);
			
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = $subject;
			$mail->Body    = $body;
			
			$mail->send();	

        /*if (!Email::_email($system['system_email'], $subject, $body, $attachment=$attachmentOk)) {
            throw new Exception(__("Your email could not be sent. Please try again later"));
        }*/
        return_json(['success' => true, 'message' => __("Your message have been received succesfully. Please note that reply from Miles Guo is no guarantee depends on your content.")]);
    } else {
        $attachmentOk=null;

        if (is_empty($_POST['name']) || is_empty($_POST['subject']) || is_empty($_POST['message'])) {
            throw new Exception(__("You must fill in all of the fields"));
        }
        if (!valid_email($_POST['email'])) {
            throw new Exception(__("Please enter a valid email address"));
        }

        $target_dir = "content/uploads/";
        $target_file = $target_dir . basename($_FILES["upload"]["name"]);
        $uploadOk = 1;
        $attachment = $_FILES["upload"]["tmp_name"];
        if ($attachment) {
            $attachmentOk=1;
        }
        /* check reCAPTCHA */
        if ($system['reCAPTCHA_enabled']) {
            $recaptcha = new \ReCaptcha\ReCaptcha($system['reCAPTCHA_secret_key']);
            $resp = $recaptcha->verify($_POST['g-recaptcha-response'], get_user_ip());
            if (!$resp->isSuccess()) {
                throw new Exception(__("The security check is incorrect. Please try again"));
            }
        }

        // prepare email
        $subject = "New email message from ".$system['system_title'];
        $body  = "<p>Hi Admin,</p>";
        $body .= "<p>You have a new email message</p>";
        $body .= "<p>Email Subject: \r\n".$_POST['subject']."</p>";
        $body .= "<p>Email Message: \r\n".$_POST['message']."</p>";
        $body .= "<p>Sender Name: \r\n".$_POST['name']."</p>";
        $body .= "<p>Sender Inquiry Type: \r\n".$_POST['inquiry-type']."</p>";
        $body .= "<p>Sender Email: \r\n".$_POST['email']."</p>";

        // send email
        /*if (!Email::_email($system['contact_email'], $subject, $body, $attachment=$attachmentOk)) {
            throw new Exception(__("Your email could not be sent. Please try again later"));
        }*/
		
		$mail = new PHPMailer(true);
		//$mail->SMTPDebug = 2;                                 // Enable verbose debug output
			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'smtp.office365.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'support@guo.media';                 // SMTP username
			$mail->Password = 'support@2018';                           // SMTP password
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 587;
			
			$mail->setFrom('support@guo.media', 'Guo Support');
			
			$mail->addAddress($system['system_email']);
			
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = $subject;
			$mail->Body    = $body;
			
			$mail->send();
    }

    // return
    return_json(['success' => true, 'message' => __("We have successfully received your message. Your inquiry will be handled within the next 48 hours with a prompt reply. Thanks again for your support towards our platform!")]);
} catch (Exception $e) {
    return_json(['error' => true, 'message' => $e->getMessage()]);
}
