<?php
/**
 * ajax -> core -> signup
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
is_ajax();

// HoneyPot PHP For Spammer
   if($_POST['honeypot'] != ''){
		return_json(['error' => true, 'message' => $e->getMessage()]);
   }

// check user logged in
if ($user->_logged_in) {
    return_json(['callback' => 'window.location.reload();']);
}

// check if registration is open
if (!$system['registration_enabled']) {
    return_json(['error' => true, 'message' => __('Registration is closed right now')]);
}

// signup
try {
    $user->sign_up($_POST);
    return_json(['callback' => 'window.location.reload();']);
} catch (Exception $e) {
    return_json(['error' => true, 'message' => $e->getMessage()]);
}
