<?php

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

ini_set("display_errors", false);
  error_reporting(0);
is_ajax();

// user access
//user_access(true);

try {
    
	$detect_lang = ms_detect_language($_POST['post_text']);
		
	//$_POST['post_text'] = htmlToPlainText($_POST['post_text']);
	
	if($detect_lang == 'en'){
					
		$get_lang = ms_translate($_POST['post_text'],'zh-Hans');
	}
	else
	{
		if($detect_lang == 'zh-Hans' ||  $detect_lang == 'zh-Hant'){
			
			$get_lang = ms_translate($_POST['post_text'],'en');
		
		}else{	
			$get_lang = ms_translate($_POST['post_text'],'zh-Hans');
		}					
	}
	
	//$return['return_language'] = htmlToPlainText($get_lang);
	$return['return_language'] = $get_lang;
	//$return['return_language'] = strip_tags(htmlspecialchars_decode($get_lang,ENT_NOQUOTES));
	
    return_json($return);
	
	
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}

function htmlToPlainText($str){
    $str = str_replace('&nbsp;', ' ', $str);
    $str = html_entity_decode($str, ENT_QUOTES | ENT_COMPAT , 'UTF-8');
    $str = html_entity_decode($str, ENT_HTML5, 'UTF-8');
    $str = html_entity_decode($str);
    $str = htmlspecialchars_decode($str);
    $str = strip_tags($str);

    return $str;
}

function DictionaryLookup ($host, $path, $key, $params, $content) {

    $headers = "Content-type: application/json\r\n" .
        "Content-length: " . strlen($content) . "\r\n" .
        "Ocp-Apim-Subscription-Key: $key\r\n" .
        "X-ClientTraceId: " . com_create_guid() . "\r\n";

    // NOTE: Use the key 'http' even if you are making an HTTPS request. See:
    // http://php.net/manual/en/function.stream-context-create.php
    $options = array (
        'http' => array (
            'header' => $headers,
            'method' => 'POST',
            'content' => $content
        )
    );
    $context  = stream_context_create ($options);
    $result = file_get_contents ($host . $path . $params, false, $context);
    return $result;
}

function ms_detect_language($post_text)
{    
	// NOTE: Be sure to uncomment the following line in your php.ini file.
	// ;extension=php_openssl.dll

	// **********************************************
	// *** Update or verify the following values. ***
	// **********************************************
	
	// Replace the subscriptionKey string value with your valid subscription key.
	$key = '6b8f1fc0891144878e14bc09d48de6d7';

	$host = "https://api.cognitive.microsofttranslator.com";
	$path = "/detect?api-version=3.0";

	$text = $post_text;
	
	$requestBody = array (
    array (
			'Text' => $text,
		),
	);
	$content = json_encode($requestBody);
	
	$result = DictionaryLookup ($host, $path, $key, "", $content);
	
	// Note: We convert result, which is JSON, to and from an object so we can pretty-print it.
	// We want to avoid escaping any Unicode characters that result contains. See:
	// http://php.net/manual/en/function.json-encode.php
	//$json = json_encode(json_decode($result), JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
	
	$detect = json_decode($result);
	
	return $detect[0]->language;
}

function Translate($host, $path, $key, $params, $content) {

    $headers = "Content-type: application/json\r\n" .
        "Content-length: " . strlen($content) . "\r\n" .
        "Ocp-Apim-Subscription-Key: $key\r\n" .
        "X-ClientTraceId: " . com_create_guid() . "\r\n";

    // NOTE: Use the key 'http' even if you are making an HTTPS request. See:
    // http://php.net/manual/en/function.stream-context-create.php
    $options = array (
        'http' => array (
            'header' => $headers,
            'method' => 'POST',
            'content' => $content
        )
    );
    $context  = stream_context_create ($options);
    $result = file_get_contents ($host . $path . $params, false, $context);
    return $result;
}

function ms_translate($post_text,$detect_lang)
{    
	// NOTE: Be sure to uncomment the following line in your php.ini file.
	// ;extension=php_openssl.dll

	// **********************************************
	// *** Update or verify the following values. ***
	// **********************************************

	// Replace the subscriptionKey string value with your valid subscription key.
	$key = '6b8f1fc0891144878e14bc09d48de6d7';

	$host = "https://api.cognitive.microsofttranslator.com";
	$path = "/translate?api-version=3.0";

	// Translate to German and Italian.
	$params_lang = "&to=".$detect_lang;

	$text = $post_text;
	
	$requestBody = array (
		array (
			'Text' => $text,
		),
	);
	$content = json_encode($requestBody);

	$result = Translate ($host, $path, $key, $params_lang, $content);
	
	// Note: We convert result, which is JSON, to and from an object so we can pretty-print it.
	// We want to avoid escaping any Unicode characters that result contains. See:
	// http://php.net/manual/en/function.json-encode.php
	
	//$json = json_encode(json_decode($result), JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
	
	$translate = json_decode($result);
	
	return $translate[0]->translations[0]->text;
}

function com_create_guid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        mt_rand( 0, 0xffff ),
        mt_rand( 0, 0x0fff ) | 0x4000,
        mt_rand( 0, 0x3fff ) | 0x8000,
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}