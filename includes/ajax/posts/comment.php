<?php

/**

 * ajax -> posts -> story

 *

 * @package Sngine

 * @author Zamblek

 */

// fetch bootstrap

require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request

is_ajax();

// user access

user_access(true);

try {

    // initialize the return array

    $return = [];

    switch ($_REQUEST['do']) {

        case 'create':

            // create a story

            $smarty->assign('_id', $_REQUEST['id']);

            $return['comment'] = $smarty->fetch("ajax.post_comment.tpl");

            $return['callback'] = "$('#modal').modal('show'); $('.modal-content:last').html(response.comment);";

            break;

        default:

            // valid inputs

            $valid['handle'] = ['post', 'photo', 'comment'];

            if (!in_array($_POST['handle'], $valid['handle'])) {
                _error(400);
            }

            /* if id is set & not numeric */

            if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }

            /* if message not set */

            if (!isset($_POST['message'])) {
                _error(400);
            }
    
            /* filter comment photo */
            $photoPost = '';
            if (isset($_POST['photo'])) {

                /*$_POST['photo'] = _json_decode($_POST['photo']);*/
                $photoPost = $_POST['photo'];
            }

            $return = [];

            // publish the new comment
            $comment = $user->comment($_POST['handle'], $_POST['id'], $_POST['message'], $photoPost);
            
            /* assign variables */

            $smarty->assign('_is_reply', ($_POST['handle'] == "comment")? true: false);

            $smarty->assign('_comment', $comment);

            $return['callback'] = "<div class='custom-message-success'>Your post has been updated to timeline.</div>";

    }

    // return & exit

    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
