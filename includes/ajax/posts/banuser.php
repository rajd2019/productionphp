<?php

/**

 * ajax -> posts -> post

 *

 * @package Sngine

 * @author Zamblek

 */

// fetch bootstrap

require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request

is_ajax();

// user access

user_access(true);

// valid inputs

if (!in_array($_POST['handle'], ['banuser'])) {
    _error(400);
}

try {

    // initialize the return array

    $return = $inputs = [];
    // publish the new post
    $postvlue=true;
    if ($_POST['handle'] == 'banuser') {
        $user_banned=$user->user_banned();
        $postvlue = "yes";
    } else {
        _error(400);
    }
    $return['userback'] = $postvlue;

    // return & exit

    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
