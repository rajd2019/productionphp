<?php
/**
 * ajax -> posts -> story
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
is_ajax();

// user access
user_access(true);

try {

    // initialize the return array
    
    $smarty->assign('_id', $_REQUEST['id']);
    $return = [];
    $return['comment'] = $smarty->fetch("__feeds_post.comments.tpl");
    $return['callback'] = "$('#modal').modal('show'); $('.modal-content:last').html(response.comment);";
    // return & exit
    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
