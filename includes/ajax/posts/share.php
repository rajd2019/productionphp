<?php


/**
 * ajax -> posts -> share
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
is_ajax();

// user access
user_access(true);

try {
    $id = $_GET['post_id'];
    
    $usersPost = $user->get_post_content($id);
    $smarty->assign('posts', $usersPost);
    $smarty->assign('id', $id);
    // initialize the return array
    $return = [];
    // get album
    $return['content'] = $smarty->fetch("ajax.posts_share.tpl");
    $return['callback'] = "$('#modal').modal('show'); $('.modal-content:last').html(response.content);";

    // return & exit
    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
