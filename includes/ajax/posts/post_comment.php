<?php
/**
 * ajax -> posts -> story
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
is_ajax();

// user access
user_access(true);

try {

    // initialize the return array
    $return = [];
    
    switch ($_REQUEST['do']) {
        case 'publish':
            // valid inputs
            if (!isset($_POST['photos'])) {
                _error(400);
            }
            /* filter photos */
            $photos = [];
            if (isset($_POST['photos'])) {
                $_POST['photos'] = _json_decode($_POST['photos']);
                if (!is_object($_POST['photos'])) {
                    _error(400);
                }
                /* filter the photos */
                foreach ($_POST['photos'] as $photo) {
                    $photos[] = $photo;
                }
                if (count($photos) == 0) {
                    _error(400);
                }
            }
            $user->post_story($_POST['message'], $photos);
            $return['callback'] = "window.location.reload();";
            break;

        case 'create':
            // create a story
            $return['post_comment'] = $smarty->fetch("ajax.post_comment.tpl");
            $return['callback'] = "$('#modal').modal('show'); $('.modal-content:last').html(response.post_comment);";
            break;
    }

    // return & exit
    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
