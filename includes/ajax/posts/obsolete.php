<?php

/**

 * ajax -> posts -> post

 *

 * @package Sngine

 * @author Zamblek

 */

// fetch bootstrap

require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request

is_ajax();

// user access

user_access(true);

// valid inputs

if (!in_array($_POST['handle'], ['obsolete'])) {
    _error(400);
}

try {

    // initialize the return array

    $return = $inputs = [];
    // publish the new post
    if ($_POST['handle'] == 'obsolete') {
        $words=$user->obsolete_words();
        $ob_words = explode(",", $words);

        if (preg_match('/'.implode("|", $ob_words).'/i', $_POST['textval'], $matches)) {
            $postvlue=false;
        } else {
            $postvlue=true;
        }
    } else {
        _error(400);
    }
	
	/*** Commented By Abhishek ***/
    //$return['callback'] = $postvlue;
	
	/*** Added By Abhishek ***/
	$return['callback'] = true;

    // return & exit

    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
