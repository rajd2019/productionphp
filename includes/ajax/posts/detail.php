<?php


/**
 * ajax -> posts -> share
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../../../bootstrap.php');

// check AJAX Request
is_ajax();

// user access
user_access(true);

try {
    $post = $user->get_post($_GET['post_id']);
    if (!$post) {
        _error(404);
    }
    //print_r($post);

    /* assign variables */
    $smarty->assign('post', $post);
	$smarty->assign('is_ajax_post', 'Yes');
    // initialize the return array
    $return = [];
    // get album
    $return['content'] = $smarty->fetch("ajax.posts_detail.tpl");
    $return['callback'] = "$('#modal2').modal('show'); $('.modal-content-modal2').html(response.content);";

    // return & exit
    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
