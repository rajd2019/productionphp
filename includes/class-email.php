<?php

class Email
{
  public static function send_activation($user_id, $email, $first_name, $last_name, $activation_key)
  {
    $subject = __("GUO.MEDIA - Verify your email address to activate account");
    /*$body = __("Hi") . " " . ucwords($first_name . " " . $last_name) . ",";
    $body .= "\r\n\r\n" . __("Thank you for signing up for GUO.MEDIA. In order to start using your GUO.MEDIA account, you need to confirm your email address.");
    $body .= "\r\n\r\n" . __("Please click the following url below to confirm your email address:");
    
	$verification = '<a href="'.$_SERVER['HTTP_HOST'].'/activation/'.$user_id.'/'.$activation_key.'">'.$_SERVER['HTTP_HOST'].'/activation/'.$user_id.'/'.$activation_key.'</a>';
	
	$body .= "\r\n\r\n" .$verification;
	
	//$body .= "\r\n\r\n" . $_SERVER['HTTP_HOST']  . "/activation/" . $user_id . "/" . $activation_key;
    $body .= "\r\n\r\n" . __("Thanks again for your support towards the future of China. Everything is just a beginning!");
    $body .= "\r\n\r\n" . __("Regards,");
    $body .= "\r\n\r\n" . __("GUO.MEDIA");*/
	
	$body = '<p>'.__("Hi") . " " . ucwords($first_name . " " . $last_name) . ",".'</p>';
	$body .= '<p>'.__("Thank you for signing up for GUO MEDIA, In order to start using your GUO MEDIA account, you need to confirm your email address.").'</p>';
	$body .= '<p>'.__("Please click the following button to confirm your email address:").'</p>';
	
	//$verification = '<a href="https://guo.media/activation/'.$user_id.'/'.$activation_key.'">guo.media/activation/'.$user_id.'/'.$activation_key.'</a>';
	
	$verification = '<a href="https://www.guo.media/activation/'.$user_id.'/'.$activation_key.'" style="background-color: #2196F3;color: white;padding: 8px 8px;text-align: center;text-decoration: none;">'.__("Click to Confirm").'</a>';
	
	$body .= '<p>'.$verification.'</p>';
	$body .= '<p>'.__("Thanks again for your support towards the future of China. Everything is just a beginning!").'</p>';
	$body .= '<p>'.__("Regards,").'</p>';
	$body .= '<p>'.__("GUO.MEDIA").'</p>';

    return self::_email($email, $subject, $body,true);
  }

  public static function send_reset_password($email, $reset_key)
  {
    /*$subject = __("Forget password activation key!");
    $body = __("Hi") . " " . $email . ",";
    $body .= "\r\n\r\n" . __("To complete the reset password process, please copy this token:");
    $body .= "\r\n\r\n" . __("Token:") . " " . $reset_key;
    $body .= "\r\n\r" . SYSTEM_TITLE . " " . __("Team");*/
	
	$subject = __("Forget password activation key!");
	
	$body = "<p>". __("Hi") . " " . $email . ","."</p>";
	$body .= "<p>".__("To complete the reset password process, please copy this token and click on reset password button:")."</p>";
	
	$reset_link = '<a href="https://guo.media/resetmob?r_email='.$email.'" style="background-color: #2196F3;color: white;padding: 8px 8px;text-align: center;text-decoration: none;">'.__("Reset Password").'</a>';
	
	$body .= "<p>".__("Token:")." ".$reset_key."</p>";
	
	$body .= '<p style="padding-bottom:5px;"></p>';
	
	$body .= '<p>'.$reset_link.'</p>';
	
	$body .= '<p style="padding-top:5px;"></p>';
	
	$body .= "<p>".SYSTEM_TITLE." ".__("Team")."</p>";

    return self::_email($email, $subject, $body,true);
  }

  public static function send_notification($email, $first_name, $last_name, $message, $url)
  {
    $subject = __("New notification from") . " " . SYSTEM_TITLE;
    $body = __("Hi") . " " . ucwords($first_name . " " . $last_name) . ",";
    $body .= "\r\n\r\n" . $message;
    $body .= "\r\n\r\n" . $url;
    $body .= "\r\n\r" . SYSTEM_TITLE . " " . __("Team");

	//Disable by Abhishek 	
    //return self::_email($email, $subject, $body);
  }
  
  public static function send_support_msg($replyto_email, $first_name, $last_name, $subject, $body)
  {
    return self::_email('support@guo.media', $subject, $body,false,false,null,$first_name,$last_name, $replyto_email);
  }

  public static function _email($email, $subject, $body, $is_html = false, $only_smtp = false, $attachment = null,$first_name='', $last_name='', $replyto_email= '')
  {
	  	  
    if (!EMAIL_SMTP_ENABLED && $only_smtp) {
      return false;
    }

    /* set header */
    $header = "MIME-Version: 1.0\r\n";
    $header .= "Mailer: support@guo.media\r\n";

    if ($is_html) {
      $header .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
    } elseif ($attachment != null) {
      $filename = preg_replace('/[^a-zA-Z0-9._-]/', '', $_FILES["upload"]["name"]);
      $filetype = $_FILES["upload"]["type"];
      $filesize = $_FILES["upload"]["size"];
      $filetemp = $_FILES["upload"]["tmp_name"];
      $fp = fopen($filetemp, "rb");
      $file = fread($fp, $filesize);
      $file = chunk_split(base64_encode($file));

      // Attachment headers
      $header .= "Content-Type:" . $filetype . " ";
      $header .= "name=\"" . $filename . "\"r\n";
      $header .= "Content-Disposition: attachment; ";
      $header .= "filename=\"" . $filename . "\"\n";
      $header .= "" . $file . "";
    } else {
      $header .= "Content-Type: text/plain; charset=\"utf-8\"\r\n";
    }

    /* send email */
    $mail = new PHPMailer\PHPMailer\PHPMailer;
    $mail->CharSet = "UTF-8";   
	
	/*
    $mail->Host = EMAIL_SMTP_SERVER;
    $mail->SMTPAuth = (EMAIL_SMTP_AUTHENTIFICATION) ? true : false;
    $mail->Username = EMAIL_SMTP_USERNAME;
    $mail->Password = EMAIL_SMTP_PASSWORD;
    $mail->SMTPSecure = (EMAIL_SMTP_SSL) ? 'ssl' : 'tls';
    $mail->Port = EMAIL_SMTP_PORT;
	*/
	
	/*****Start Code Editied By Abhishek ********/
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'smtp.office365.com';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'support@guo.media';                 // SMTP username
	$mail->Password = 'support@2018';                           // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                                    // TCP port to connect to

	//Recipients
	$mail->setFrom('support@guo.media', 'Guo Media');
	
	if($replyto_email != ''){
		
		$mail->AddReplyTo($replyto_email, $first_name.' '.$last_name);
	}
	/*****End Code Editied By Abhishek ********/
	    
    $mail->addAddress($email);
    $mail->Subject = $subject;
    if ($is_html) {
      $mail->isHTML(true);
      $mail->AltBody = strip_tags($body);
    }
	
    $mail->Body = $body;
    if (!$mail->send()) {
		
      if ($only_smtp) {
        return false;
      }
      /* send using mail() */
      if (!mail($email, $subject, $body, $header)) {
        return false;
      }
    }

    return true;
  }

  public static function smtp_test()
  {
    $subject = __("Test SMTP Connection on") . " " . SYSTEM_TITLE;
    $body = __("This is a test email");
    $body .= "\r\n\r" . SYSTEM_TITLE . " " . __("Team");

    if (!self::_email(SYSTEM_EMAIL, $subject, $body, false, true)) {
      throw new Exception(__("Test email could not be sent. Please check your settings"));
    }
  }
}