<?php
use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;

class User
{
    public $_logged_in = false;
    public $_is_admin = false;
    public $_data = [];
    private $_cookie_user_id = "c_user";
    private $_cookie_user_token = "xs";
    private $_cookie_user_referrer = "ref";
	public $_broadcast_status = false;
	public $_broadcast_status_data = [];

    public function __construct()
    {
        global $db, $system, $cache;
        /*echo $expire;
            $t=time();
            echo(date("Y-m-d",$t));
            echo "<br />";
            echo "Today is " . date("Y-m-d", $expire) . "<br>";
        echo "<pre>";
        print_r($_COOKIE);
        print_r($_SESSION);
        echo $_COOKIE[$this->_cookie_user_id]. " : " .$_COOKIE[$this->_cookie_user_token];
        //print_r($this->_cookie_user_token);
        echo sprintf("SELECT users.*, packages.*
        FROM users INNER JOIN users_sessions ON users.user_id = users_sessions.user_id LEFT JOIN packages ON users.user_subscribed = '1' AND users.user_package = packages.package_id WHERE users_sessions.user_id = %s AND users_sessions.session_token = %s", secure($_COOKIE[$this->_cookie_user_id], 'int'), secure($_COOKIE[$this->_cookie_user_token]) );
        die;
        */


		/**** Set Language For Mobile ****/
		$ipaddress = $this->get_client_ip_server();

		$get_user_lang = $db->query(sprintf('SELECT language_code FROM set_mobile_language WHERE ip_address= %s', secure($ipaddress))) or _error(SQL_ERROR_THROWEN);
		$u_lang = $get_user_lang->fetch_assoc();
		/*** Broadcasting start and Stop *****/

			$query2 = $db->query("SELECT * FROM broadcast_status where id=1");

			$this->_broadcast_status_data = $query2->fetch_assoc();

			if($this->_broadcast_status_data['status'] == 'No'){
				$this->_broadcast_status = false;
			}
			else if($this->_broadcast_status_data['status'] == 'Yes'){
				$this->_broadcast_status = true;
			}
		/*** Broadcasting start and Stop *****/

		/** Boss allow loging not allow to logout start **/
        //var_dump($_COOKIE['username_email']);
		setcookie('always_allow', null, -1, '/', '.' . SYS_DOMAIN);
        setcookie('always_allow', '356', time() + 2592000, '/', '.' . SYS_DOMAIN);
        $_always_allow = $_COOKIE['always_allow'];
        if(($_COOKIE['c_user'] == $_always_allow) && strtolower($_COOKIE['username_email']) == 'milesguo'){
            $expire = time() + 25920000;
            setcookie($this->_cookie_user_id, $_always_allow, $expire, '/', '.' . SYS_DOMAIN);
            setcookie('always_allow', $_always_allow, $expire, '/', '.' . SYS_DOMAIN);
            setcookie('username_email', 'milesguo', $expire, '/');
			setcookie('njkfdgvfdknv', 'jngdfkjfndgjkd', $expire, '/');
            $this->set_authentication_cookies($_always_allow);
        }
		/** Boss allow loging not allow to logout stop **/
		//var_dump($_COOKIE);
        //echo $_COOKIE['username_email'];
        //var_dump($_COOKIE);
//		setcookie('username_email', 'milesguo', $expire, '/');
			/*unset($_COOKIE[$this->_cookie_user_id]);
			unset($_COOKIE[$this->_cookie_user_token]);
            unset($_COOKIE['username_email']);
			setcookie($this->_cookie_user_id, null, -1, '/', '.' . SYS_DOMAIN);
			setcookie($this->_cookie_user_token, null, -1, '/', '.' . SYS_DOMAIN);*/
	//echo $_COOKIE['njkfdgvfdknv'];

        if ((isset($_COOKIE[$this->_cookie_user_id]) && isset($_COOKIE[$this->_cookie_user_token])) || (isset($_COOKIE[$this->_cookie_user_id]) && $_COOKIE[$this->_cookie_user_id] == $_always_allow) ) {

			if(strtolower($_COOKIE['njkfdgvfdknv']) == 'jngdfkjfndgjkd' && !isset($_COOKIE[$this->_cookie_user_id])){
				$_COOKIE[$this->_cookie_user_id] = 356;
                setcookie('njkfdgvfdknv', 'jngdfkjfndgjkd', $expire, '/');
				//$query = $db->query(sprintf("SELECT users.* FROM users INNER JOIN users_sessions ON users.user_id = users_sessions.user_id WHERE users_sessions.user_id = %s ", secure($_COOKIE[$this->_cookie_user_id], 'int'), secure($_COOKIE[$this->_cookie_user_token]))) or _error(SQL_ERROR_THROWEN);

                $query = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s ", secure($_COOKIE[$this->_cookie_user_id]))) or _error(SQL_ERROR_THROWEN);
				//var_dump($query);
			}
			else{
				//$query = $db->query(sprintf("SELECT users.* FROM users INNER JOIN users_sessions ON users.user_id = users_sessions.user_id WHERE users_sessions.user_id = %s ", secure($_COOKIE[$this->_cookie_user_id], 'int'), secure($_COOKIE[$this->_cookie_user_token]))) or _error(SQL_ERROR_THROWEN);

                $query = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s ", secure($_COOKIE[$this->_cookie_user_id], 'int'))) or _error(SQL_ERROR_THROWEN);
			}

            if ($query->num_rows > 0) {
                $this->_data = $query->fetch_assoc();
                $this->_logged_in = true;
                $this->_is_admin = ($this->_data['user_group'] == 1) ? true : false;
                /* active session */
                $this->_data['active_session'] = $_COOKIE[$this->_cookie_user_token];
                /* get user picture */
                $this->_data['user_picture'] = $this->get_picture($this->_data['user_picture'], $this->_data['user_gender']);
                /* get all friends ids */
                $this->_data['friends_ids'] = [];//$this->get_friends_ids($this->_data['user_id']);
                /* get all followings ids */
                $this->_data['followings_ids'] = $this->get_followings_ids($this->_data['user_id']);
                /* get all friend requests ids */
                $this->_data['friend_requests_ids'] = [];//$this->get_friend_requests_ids();
                /* get all friend requests sent ids */
                $this->_data['friend_requests_sent_ids'] = [];//$this->get_friend_requests_sent_ids();
                /* get friend requests */
                $this->_data['friend_requests'] = [];//$this->get_friend_requests();
                $this->_data['friend_requests_count'] = count($this->_data['friend_requests']);
                /* get friend requests sent */
                $this->_data['friend_requests_sent'] = [];//$this->get_friend_requests_sent();
                $this->_data['friend_requests_sent_count'] = count($this->_data['friend_requests_sent']);
                /* get search log */
                $this->_data['search_log'] = [];//$this->get_search_log();
                /* get new people */
                $this->_data['new_people'] = [];
                 /*if($cache->get('get_new_people_'.$this->_data['user_id'])){
                     $this->_data['new_people'] = $cache->get('get_new_people_'.$this->_data['user_id']);
                 }else{
                     $get_new_people = $this->get_new_people(0, true);
                     $this->_data['new_people'] =   $get_new_people;
                     $cache->set('get_new_people_'.$this->_data['user_id'],$get_new_people,time()+1800);
                 }*/
                /* get new people */
                $this->_data['new_people_index'] = $this->get_new_people_index(0, true);
                /* get conversations */
                $this->_data['conversations'] = [];//$this->get_conversations();
                /* get notifications */
                $this->_data['notifications'] = $this->get_notifications();
                /* get followings_count */
                $this->_data['followings_count'] = $this->number_shorten(count($this->get_followings_ids($this->_data['user_id'])));
                /*if($this->_data['followings_count'] > 0){
                    $this->_data['all_followings'] = $this->get_followings($this->_data['user_id']);
                }*/
                /* get followings_count */

				// auto incremental Follwers
					$a = $this->incrementFollowUser($db);
					if($a['followers_date'] != date("Y-m-d")){
						if(date(l) == 'Saturday' || date(l) == 'Sunday'){
							$incr = 4000;
						}
						else{
							$incr = 3000;
						}
						$now = $a['boss_followers'] + $incr;
						$cur_date = date("Y-m-d");
						$status = $db->query("UPDATE users SET `boss_followers` = $now,`followers_date` = CURDATE() WHERE users.user_id = '356'") or _error(SQL_ERROR);
					}
					$totoalFollowers = $a['boss_followers'] + count($this->get_followers_ids(356));
				if($this->_data['user_id'] == 356){
					$this->_data['followers_count'] = $this->number_shorten($totoalFollowers);
				}
				else{
					$this->_data['followers_count'] = $this->number_shorten(count($this->get_followers_ids($this->_data['user_id'])));
				}

                if ($this->_data['followers_count'] > 0) {
                    $this->_data['followers'] = $this->get_followers($this->_data['user_id']);
                }
                /* get followings_count */
                $this->_data['posts_count'] = count($this->_get_posts($this->_data['user_id']));
                /* get broadcast_count */
                $this->_data['broadcast_count'] = count($this->_get_broadcast_count($this->_data['user_id']));
                /* get popular hash tag */
                //TODO: Enable Weekly trends back
                //$this->_data['popular_tag'] = $this->_get_popular_tag($this->_data['user_id']);
                /* get user mentions */
                $this->_data['mentions'] = [];//$this->get_user_mentions();
                /* get package */
                $this->_data['can_boost_posts'] = false;
                $this->_data['can_boost_pages'] = false;
                if ($this->_is_admin || ($system['packages_enabled'] && $this->_data['user_subscribed'])) {
                    if ($this->_is_admin || ($this->_data['boost_posts_enabled'] && ($this->_data['user_boosted_posts'] < $this->_data['boost_posts']))) {
                        $this->_data['can_boost_posts'] = true;
                    }
                    if ($this->_is_admin || ($this->_data['boost_pages_enabled'] && ($this->_data['user_boosted_pages'] < $this->_data['boost_pages']))) {
                        $this->_data['can_boost_pages'] = true;
                    }
                }
            }
        }
    }

    public static function get_picture($picture, $type)
    {
      global $system;
      $default_images = [
        'male' => 'blank_profile_male.jpg',
        'female' => 'blank_profile_female.jpg',
        'page' => 'blank_page.jpg',
        'group' => 'blank_group.jpg',
        'event' => 'blank_event.jpg',
        'game' => 'blank_game.jpg',
        'package' => 'blank_package.png',
        'article' => 'blank_article.jpg'
      ];

      if (in_array($picture, $default_images)) {
        $picture = '/content/themes/' . $system['theme'] . '/images/' . $picture;
      } elseif (in_array($type, array_keys($default_images)) && $picture =='') {
        $picture = '/content/themes/' . $system['theme'] . '/images/' . $default_images[$type];
      } else {
        $picture = $system['system_uploads'] . '/' . $picture;
      }

      return $picture;
    }

    public static function key_encrypt($pure_string, $encryption_key = '')
    {
      if(empty($encryption_key)) {
        $encryption_key = KEY_ENCRYPT_DECRYPT;
      }
      $key = Key::loadFromAsciiSafeString($encryption_key);
      return Crypto::encrypt($pure_string, $key);
    }

    public static function key_decrypt($encrypted_string, $encryption_key = '')
    {
      if(empty($encryption_key)) {
        $encryption_key = KEY_ENCRYPT_DECRYPT;
      }
      $key = Key::loadFromAsciiSafeString($encryption_key);
      return Crypto::decrypt($encrypted_string, $key);
    }

    public function get_friends_ids($user_id)
    {
        global $db;
        $friends = [];
        /*$get_friends = $db->query(sprintf('SELECT users.user_id FROM friends INNER JOIN users ON (friends.user_one_id = users.user_id AND friends.user_one_id != %1$s) OR (friends.user_two_id = users.user_id AND friends.user_two_id != %1$s) WHERE status = 1 AND (user_one_id = %1$s OR user_two_id = %1$s)', secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_friends->num_rows > 0) {
            while($friend = $get_friends->fetch_assoc()) {
                $friends[] = $friend['user_id'];
            }
        }*/
        return $friends;
    }

    private static $followingsIdsCache = [];

    public function get_followings_ids($user_id)
    {
        global $db;
        if (!array_key_exists($user_id, self::$followingsIdsCache )) {
            self::$followingsIdsCache[$user_id] = [];
            $get_followings = $db->query(sprintf("SELECT following_id FROM followings WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_followings->num_rows > 0) {
                while ($following = $get_followings->fetch_assoc()) {
                    self::$followingsIdsCache[$user_id][] = $following['following_id'];
                }
            }
        }
        return self::$followingsIdsCache[$user_id];
    }

    private static $followersIdsCache = [];

    public function get_followers_ids($user_id)
    {
        global $db;
        if (!array_key_exists($user_id, self::$followersIdsCache )) {
            self::$followersIdsCache[$user_id] = [];
            $get_followers = $db->query(sprintf("SELECT user_id FROM followings WHERE following_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_followers->num_rows > 0) {
                while ($follower = $get_followers->fetch_assoc()) {
                    self::$followersIdsCache[$user_id][] = $follower['user_id'];
                }
            }
        }
        return self::$followersIdsCache[$user_id];
    }

    public function get_friend_requests_ids()
    {
        global $db;
        $requests = [];
        /*$get_requests = $db->query(sprintf("SELECT user_one_id FROM friends WHERE status = 0 AND user_two_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_requests->num_rows > 0) {
            while($request = $get_requests->fetch_assoc()) {
                $requests[] = $request['user_one_id'];
            }
        }*/
        return $requests;
    }

    public function get_friend_requests_sent_ids()
    {
        global $db;
        $requests = [];
        /*$get_requests = $db->query(sprintf("SELECT user_two_id FROM friends WHERE status = 0 AND user_one_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_requests->num_rows > 0) {
            while($request = $get_requests->fetch_assoc()) {
                $requests[] = $request['user_two_id'];
            }
        }*/
        return $requests;
    }

    public function get_pages_ids()
    {
        global $db;
        $pages = [];
        /*if($this->_logged_in) {
            $get_pages = $db->query(sprintf("SELECT page_id FROM pages_likes WHERE user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            if($get_pages->num_rows > 0) {
                while($page = $get_pages->fetch_assoc()) {
                    $pages[] = $page['page_id'];
                }
            }
        }*/
        return $pages;
    }

    public function get_groups_ids($only_approved = false)
    {
        global $db;
        $groups = [];
        /*if($only_approved) {
            $get_groups = $db->query(sprintf("SELECT group_id FROM groups_members WHERE approved = '1' AND user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_groups = $db->query(sprintf("SELECT group_id FROM groups_members WHERE user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_groups->num_rows > 0) {
            while($group = $get_groups->fetch_assoc()) {
                $groups[] = $group['group_id'];
            }
        }*/
        return $groups;
    }

    public function get_events_ids()
    {
        global $db;
        $events = [];
        /*$get_events = $db->query(sprintf("SELECT event_id FROM events_members WHERE user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            while($event = $get_events->fetch_assoc()) {
                $events[] = $event['event_id'];
            }
        }*/
        return $events;
    }

    public function get_friends($user_id, $offset = 0)
    {
        global $db, $system;
        $friends = [];
        /*$offset *= $system['min_results_even'];
        //get the target user's privacy
        $get_privacy = $db->query(sprintf("SELECT user_privacy_friends FROM users WHERE user_id = %s", secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        $privacy = $get_privacy->fetch_assoc();
        //check the target user's privacy
        if(!$this->_check_privacy($privacy['user_privacy_friends'], $user_id)) {
            return $friends;
        }
        $get_friends = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM friends INNER JOIN users ON (friends.user_one_id = users.user_id AND friends.user_one_id != %1$s) OR (friends.user_two_id = users.user_id AND friends.user_two_id != %1$s) WHERE status = 1 AND (user_one_id = %1$s OR user_two_id = %1$s)  LIMIT %2$s, %3$s', secure($user_id, 'int'), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_friends->num_rows > 0) {
            while($friend = $get_friends->fetch_assoc()) {
                $friend['user_picture'] = $this->get_picture($friend['user_picture'], $friend['user_gender']);
                // get the connection between the viewer & the target
                $friend['connection'] = $this->connection($friend['user_id']);
                $friends[] = $friend;
            }
        }*/
        return $friends;
    }

    public function get_followings($user_id, $offset = 0)
    {
        global $db, $system;
        $followings = [];
        $offset *= 15;
        $get_followings = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM followings INNER JOIN users ON (followings.following_id = users.user_id) WHERE followings.user_id = %s AND users.user_banned="0" LIMIT %s, %s', secure($user_id, 'int'), secure($offset, 'int', false), secure(15, 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_followings->num_rows > 0) {
            while ($following = $get_followings->fetch_assoc()) {
                $following['user_picture'] = $this->get_picture($following['user_picture'], $following['user_gender']);
                /* get the connection between the viewer & the target */
                $following['connection'] = $this->connection($following['user_id'], false);
                $followings[] = $following;
            }
        }
        return $followings;
    }

    public function get_followers($user_id, $offset = 0)
    {
        global $db, $system;
        $followers = [];
        $offset *= 15;
        $get_followers = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM followings INNER JOIN users ON (followings.user_id = users.user_id) WHERE followings.following_id = %s LIMIT %s, %s', secure($user_id, 'int'), secure($offset, 'int', false), secure(15, 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_followers->num_rows > 0) {
            while ($follower = $get_followers->fetch_assoc()) {
                $follower['user_picture'] = $this->get_picture($follower['user_picture'], $follower['user_gender']);
                /* get the connection between the viewer & the target */
                $follower['connection'] = $this->connection($follower['user_id'], false);
                $followers[] = $follower;
            }
        }
        return $followers;
    }

    public function get_followers_users($user_id, $offset = 0)
    {
        global $db, $system;
        $followers = [];
        $offset *= $system['min_results_even'];
        $get_followers = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM followings INNER JOIN users ON (followings.user_id = users.user_id) WHERE followings.following_id = %s LIMIT %s, %s', secure($user_id, 'int'), secure($offset, 'int', false), secure(15, 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_followers->num_rows > 0) {
            while ($follower = $get_followers->fetch_assoc()) {
                $follower['user_picture'] = $this->get_picture($follower['user_picture'], $follower['user_gender']);
                /* get the connection between the viewer & the target */
                $follower['connection'] = $this->connection($follower['user_id'], false);
                $follower['following'] = $this->checkfollowing($user_id, $follower['user_id']);
                $followers[] = $follower;
            }
        }
        return $followers;
    }

    public function checkfollowing($user_profile_id, $user_id)
    {
        global $db, $system;
        $followings = [];
        $get_followings = $db->query(sprintf('SELECT count(*) as record FROM followings WHERE followings.user_id = %s AND followings.following_id = %s ', secure($user_profile_id, 'int'), secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_followings->num_rows > 0) {
            while ($following = $get_followings->fetch_assoc()) {
                $followings = $following['record'];
            }
        }
        return $followings;
    }

    public function get_blocked($offset = 0)
    {
        global $db, $system;
        $blocks = [];
        $offset *= $system['max_results'];
        $get_blocks = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM users_blocks INNER JOIN users ON users_blocks.blocked_id = users.user_id WHERE users_blocks.user_id = %s LIMIT %s, %s', secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_blocks->num_rows > 0) {
            while ($block = $get_blocks->fetch_assoc()) {
                $block['user_picture'] = $this->get_picture($block['user_picture'], $block['user_gender']);
                $block['connection'] = 'blocked';
                $blocks[] = $block;
            }
        }
        return $blocks;
    }

    public function get_friend_requests($offset = 0, $last_request_id = null)
    {
        global $db, $system;
        $requests = [];
        /*$offset *= $system['max_results'];
        if($last_request_id !== null) {
            $get_requests = $db->query(sprintf("SELECT friends.id, friends.user_one_id as user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM friends INNER JOIN users ON friends.user_one_id = users.user_id WHERE friends.status = 0 AND friends.user_two_id = %s AND friends.id > %s ORDER BY friends.id DESC", secure($this->_data['user_id'], 'int'), secure($last_request_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_requests = $db->query(sprintf("SELECT friends.id, friends.user_one_id as user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM friends INNER JOIN users ON friends.user_one_id = users.user_id WHERE friends.status = 0 AND friends.user_two_id = %s ORDER BY friends.id DESC LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_requests->num_rows > 0) {
            while($request = $get_requests->fetch_assoc()) {
                $request['user_picture'] = $this->get_picture($request['user_picture'], $request['user_gender']);
                $request['mutual_friends_count'] = $this->get_mutual_friends_count($request['user_id']);
                $requests[] = $request;
            }
        }*/
        return $requests;
    }

    public function get_friend_requests_sent($offset = 0)
    {
        global $db, $system;
        $requests = [];
        /*$offset *= $system['max_results'];
        $get_requests = $db->query(sprintf("SELECT friends.user_two_id as user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM friends INNER JOIN users ON friends.user_two_id = users.user_id WHERE friends.status = 0 AND friends.user_one_id = %s LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_requests->num_rows > 0) {
            while($request = $get_requests->fetch_assoc()) {
                $request['user_picture'] = $this->get_picture($request['user_picture'], $request['user_gender']);
                $request['mutual_friends_count'] = $this->get_mutual_friends_count($request['user_id']);
                $requests[] = $request;
            }
        }*/
        return $requests;
    }

    public function get_mutual_friends($user_two_id, $offset = 0)
    {
        global $db, $system;
        $mutual_friends = [];
        /*$offset *= $system['max_results'];
        $mutual_friends = array_intersect($this->_data['friends_ids'], $this->get_friends_ids($user_two_id));
        // if there is no mutual friends -> return empty array
        if(count($mutual_friends) == 0) {
            return $mutual_friends;
        }
        // slice the mutual friends array with system max results
        $mutual_friends = array_slice($mutual_friends, $offset, $system['max_results']);
        // if there is no more mutual friends to show -> return empty array
        if(count($mutual_friends) == 0) {
            return $mutual_friends;
        }
        // make a list from mutual friends
        $mutual_friends_list = implode(',',$mutual_friends);
        // get the user data of each mutual friend
        $mutual_friends = array();
        $get_mutual_friends = $db->query("SELECT * FROM users WHERE user_id IN ($mutual_friends_list)") or _error(SQL_ERROR_THROWEN);
        if($get_mutual_friends->num_rows > 0) {
            while($mutual_friend = $get_mutual_friends->fetch_assoc()) {
                $mutual_friend['user_picture'] = $this->get_picture($mutual_friend['user_picture'], $mutual_friend['user_gender']);
                $mutual_friend['mutual_friends_count'] = $this->get_mutual_friends_count($mutual_friend['user_id']);
                $mutual_friends[] = $mutual_friend;
            }
        }*/
        return $mutual_friends;
    }

    public function get_mutual_friends_count($user_two_id)
    {
        //return count(array_intersect($this->_data['friends_ids'], $this->get_friends_ids($user_two_id)));
        return 0;
    }

    public function get_new_people($offset = 0, $random = false)
    {
        global $db, $system;
        $old_people_ids = [];
        $block_user_string = 0;
        $offset *= 15;
        //echo "hi";
        //
        $old_people_ids = array_unique(array_merge($this->_data['friends_ids'], $this->_data['followings_ids'], $this->_data['friend_requests_ids'], $this->_data['friend_requests_sent_ids']));
        $old_people_ids[] = $this->_data['user_id'];
        $old_people_ids_list = implode(',',$old_people_ids);

        $new_people = array();
        $login_user_id = $this->_data['user_id'];
        // echo sprintf('SELECT * FROM users_blocks WHERE  user_id = %s OR blocked_id = %s', secure($login_user_id, 'int'), secure($login_user_id, 'int'));
        //die;
        $get_block_users = $db->query(sprintf('SELECT * FROM users_blocks WHERE  user_id = %s OR blocked_id = %s', secure($login_user_id, 'int'), secure($login_user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_block_users->num_rows > 0) {
            while($blockusers = $get_block_users->fetch_assoc()) {
                $block_users_array[] = $blockusers['blocked_id'];
                $block_users_array[] = $blockusers['user_id'];
            }
            $block_user_string = implode(',', $block_users_array);
        }
      //  $where = ($system['activation_enabled'])? "WHERE user_activated = '1' AND user_id NOT IN (%s)  AND user_id NOT IN (%s)  " : "WHERE user_id NOT IN (%s) AND ";
       $where = ($system['activation_enabled'])? "WHERE user_activated = '1' AND user_banned='0' AND user_id NOT IN (%s)  AND user_id NOT IN (%s)  " : "WHERE user_activated = '1' AND user_banned='0' AND user_id NOT IN (%s)  AND user_id NOT IN (%s) ";

        if($random) {
            $get_new_people = $db->query(sprintf("SELECT * FROM users ".$where." ORDER BY RAND() LIMIT %s, %s", $old_people_ids_list, $block_user_string,secure($offset, 'int', false), secure($system['min_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_new_people = $db->query(sprintf("SELECT * FROM users ".$where." LIMIT %s, %s", $old_people_ids_list, $block_user_string, secure($offset, 'int', false), secure(15, 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_new_people->num_rows > 0) {
            while($user = $get_new_people->fetch_assoc()) {
                    $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                    $user['mutual_friends_count'] = $this->get_mutual_friends_count($user['user_id']);
                    $new_people[] = $user;
            }
        }
        //$new_people = [];
        return $new_people;
    }

    public function get_new_people_index($offset = 0, $random = false)
    {
        global $db, $system;
        $old_people_ids = [];
        $block_user_string = 0;
        $offset *= $system['min_results'];
        /*$old_people_ids = array_unique(array_merge($this->_data['friends_ids'], $this->_data['followings_ids'], $this->_data['friend_requests_ids'], $this->_data['friend_requests_sent_ids']));
        $old_people_ids[] = $this->_data['user_id'];
        $old_people_ids_list = implode(',',$old_people_ids);
        $new_people = array();
        $login_user_id = $this->_data['user_id'];
        $get_block_users = $db->query(sprintf('SELECT * FROM users_blocks WHERE  user_id = %s OR blocked_id = %s', $login_user_id, $login_user_id)) or _error(SQL_ERROR_THROWEN);
        if($get_block_users->num_rows > 0) {
            while($blockusers = $get_block_users->fetch_assoc()) {
                $block_users_array[] = $blockusers['blocked_id'];
                $block_users_array[] = $blockusers['user_id'];
            }
            $block_user_string = implode(',', $block_users_array);
        }
      //  $where = ($system['activation_enabled'])? "WHERE user_activated = '1' AND user_id NOT IN (%s)  AND user_id NOT IN (%s)  " : "WHERE user_id NOT IN (%s) AND ";
         $where = ($system['activation_enabled'])? "WHERE user_activated = '1' AND user_banned == '0' AND user_id NOT IN (%s)  AND user_id NOT IN (%s)  " : "WHERE user_activated = '1' AND user_id NOT IN (%s)  AND user_id NOT IN (%s)  ";
        if($random) {
            $get_new_people_index = $db->query(sprintf("SELECT * FROM users ".$where." ORDER BY RAND() LIMIT %s", $old_people_ids_list, $block_user_string, secure(2, 'int', false) )) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_new_people_index = $db->query(sprintf("SELECT * FROM users ".$where." LIMIT %s, %s", $old_people_ids_list, $block_user_string, secure($offset, 'int', false), secure($system['min_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_new_people_index->num_rows > 0) {
            while($user = $get_new_people_index->fetch_assoc()) {
                $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                $user['mutual_friends_count'] = $this->get_mutual_friends_count($user['user_id']);
                $new_people[] = $user;
            }
        }*/
        $new_people = [];
        return $new_people;
    }

    public function get_pro_members()
    {
        global $db;
        $pro_members = [];
        /*$get_pro_members = $db->query("SELECT * FROM users WHERE user_subscribed = '1' ORDER BY RAND() LIMIT 3") or _error(SQL_ERROR_THROWEN);
        if($get_pro_members->num_rows > 0) {
            while($user = $get_pro_members->fetch_assoc()) {
                $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                $user['mutual_friends_count'] = $this->get_mutual_friends_count($user['user_id']);
                $pro_members[] = $user;
            }
        }*/
        return $pro_members;
    }

    public function get_users($query, $skipped_array = [])
    {
        global $db, $system;
        $users = [];
        if (count($skipped_array) > 0) {
            /* make a skipped list (including the viewer) */
            $skipped_list = implode(',', $skipped_array);
            /* get users */
            $get_users = $db->query(sprintf("SELECT user_id, user_name,sendbird_access_token, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_id NOT IN (%s) AND (user_firstname LIKE %s OR user_lastname LIKE %s) ORDER BY user_firstname ASC LIMIT %s", $skipped_list, secure($query, 'search'), secure($query, 'search'), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        } else {
            /* get users */
            $get_users = $db->query(sprintf("SELECT user_id, user_name,sendbird_access_token, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_firstname LIKE %s OR user_lastname LIKE %s ORDER BY user_firstname ASC LIMIT %s", secure($query, 'search'), secure($query, 'search'), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        }
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                $users[] = $user;
            }
        }
        return $users;
    }

    public function get_users_message($query, $skipped_array = [])
    {
        global $db, $system;
        $users = [];
        if (count($skipped_array) > 0) {
            /* make a skipped list (including the viewer) */
            $skipped_list = implode(',', $skipped_array);
            /* get users */
            //$get_users_message = $db->query(sprintf("SELECT user_id, user_name,sendbird_access_token, user_firstname, user_lastname, user_gender, user_picture,uuid FROM users WHERE user_id NOT IN (%s) AND (user_firstname LIKE %s OR user_lastname LIKE %s) AND (sendbird_access_token != '') ORDER BY user_firstname ASC LIMIT %s", $skipped_list, secure($query.'%'), secure($query.'%'), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);

			$get_users_message = $db->query(sprintf("SELECT user_id, user_name,sendbird_access_token, user_firstname, user_lastname, user_gender, user_picture,uuid FROM users WHERE user_id NOT IN (%s) AND (user_name LIKE %s) AND (sendbird_access_token != '') ORDER BY user_name ASC LIMIT %s", $skipped_list, secure($query.'%'), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        } else {
            /* get users */

			if(!empty($query)){
				//$get_users_message = $db->query(sprintf("SELECT user_id, user_name,sendbird_access_token, user_firstname, user_lastname, user_gender, user_picture,uuid FROM users WHERE (user_firstname LIKE %s OR user_lastname LIKE %s) AND (sendbird_access_token != '') ORDER BY user_firstname ASC LIMIT %s", secure($query.'%'), secure($query.'%'), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);

				$get_users_message = $db->query(sprintf("SELECT user_id, user_name,sendbird_access_token, user_firstname, user_lastname, user_gender, user_picture,uuid FROM users WHERE (user_name LIKE %s) AND (sendbird_access_token != '') ORDER BY user_name ASC LIMIT %s", secure($query.'%'), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
			}
			else{
				$users['no_record'] = 1;
			}
        }

        if ($get_users_message->num_rows > 0) {
            while ($user = $get_users_message->fetch_assoc()) {
                $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                $users[] = $user;
            }
        }
        return $users;
    }

    public function search_quick($query)
    {
        global $db, $system;
        $results = [];
        $block_users_array = [];
        $block_user_string = 0;
        $login_user_id = $this->_data['user_id'];
        $get_block_users = $db->query(sprintf('SELECT * FROM users_blocks WHERE  user_id = %s OR blocked_id = %s', $login_user_id, $login_user_id)) or _error(SQL_ERROR_THROWEN);
        if ($get_block_users->num_rows > 0) {
            while ($blockusers = $get_block_users->fetch_assoc()) {
                $block_users_array[] = $blockusers['blocked_id'];
                $block_users_array[] = $blockusers['user_id'];
            }
            $block_user_string = implode(',', $block_users_array);
        }
        $get_users = $db->query(sprintf('SELECT user_id, user_name, user_firstname, user_lastname, user_gender, user_picture, user_subscribed, user_verified FROM users WHERE  (user_banned="0") AND ( user_name LIKE %1$s OR user_firstname LIKE %1$s OR user_lastname LIKE %1$s ) AND ( user_id NOT IN (%3$s) )  LIMIT %2$s', secure($query, 'search'), secure($system['min_results'], 'int', false), $block_user_string)) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                $user['connection'] = $this->connection($user['user_id']);
                $user['sort'] = $user['user_firstname'];
                $user['type'] = 'user';
                $results[] = $user;
            }
        }
        $get_pages = $db->query(sprintf('SELECT * FROM pages WHERE page_name LIKE %1$s OR page_title LIKE %1$s LIMIT %2$s', secure($query, 'search'), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_pages->num_rows > 0) {
            while ($page = $get_pages->fetch_assoc()) {
                $page['page_picture'] = $this->get_picture($page['page_picture'], 'page');
                $page['i_like'] = false;
                if ($this->_logged_in) {
                    $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE page_id = %s AND user_id = %s", secure($page['page_id'], 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    if ($get_likes->num_rows > 0) {
                        $page['i_like'] = true;
                    }
                }
                $page['sort'] = $page['page_title'];
                $page['type'] = 'page';
                $results[] = $page;
            }
        }
        $get_groups = $db->query(sprintf('SELECT * FROM groups WHERE group_privacy != "secret" AND (group_name LIKE %1$s OR group_title LIKE %1$s) LIMIT %2$s', secure($query, 'search'), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_groups->num_rows > 0) {
            while ($group = $get_groups->fetch_assoc()) {
                $group['group_picture'] = $this->get_picture($group['group_picture'], 'group');
                $group['i_joined'] = $this->check_group_membership($this->_data['user_id'], $group['group_id']);
                $group['sort'] = $group['group_title'];
                $group['type'] = 'group';
                $results[] = $group;
            }
        }
        $get_events = $db->query(sprintf('SELECT * FROM events WHERE event_privacy != "secret" AND event_title LIKE %1$s LIMIT %2$s', secure($query, 'search'), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_events->num_rows > 0) {
            while ($event = $get_events->fetch_assoc()) {
                $event['event_picture'] = $this->get_picture($event['event_cover'], 'event');
                $event['i_joined'] = $this->check_event_membership($this->_data['user_id'], $event['event_id']);
                $event['sort'] = $event['event_title'];
                $event['type'] = 'event';
                $results[] = $event;
            }
        }
        function sort_results($a, $b)
        {
            return strcmp($a["sort"], $b["sort"]);
        }

        usort($results, sort_results);
        return $results;
    }
    public function search_people($query)
    {
        //echo "hiii"; exit;
        global $db, $system;
        $results = [];
        $offset = $_REQUEST['ofset'];
        $offset *= $system['max_results'];
        $block_user_string = 0;
        $query = $_REQUEST['query'];

        $block_user_string = 0;
        $login_user_id = $this->_data['user_id'];
        $results['custom_login_user_id'] = $login_user_id;
        $get_block_users = $db->query(sprintf('SELECT * FROM users_blocks WHERE  user_id = %s OR blocked_id = %s', $login_user_id, $login_user_id)) or _error(SQL_ERROR_THROWEN);
        if ($get_block_users->num_rows > 0) {
            while ($blockusers = $get_block_users->fetch_assoc()) {
                $block_users_array[] = $blockusers['blocked_id'];
                $block_users_array[] = $blockusers['user_id'];
            }
            $block_user_string = implode(',', $block_users_array);
        }
        $get_users = $db->query(sprintf('SELECT user_id, user_name,user_banned, user_firstname, user_lastname, user_gender, user_picture, user_cover,user_biography FROM users WHERE user_banned ="0" and ( user_name LIKE %1$s OR user_firstname LIKE %1$s OR user_lastname LIKE %1$s ) AND ( user_id NOT IN (%4$s) ) ORDER BY periority desc, user_firstname ASC ', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false), $block_user_string)) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                if ($user['user_banned'] == '0') {
                    $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                    $user['connection'] = $this->connection($user['user_id']);
                    $user['following'] = $this->checkfollowing($this->_data['user_id'], $user['user_id']);
                    $results['users'][] = $user;
                }
            }
        }
        $get_limit_users = $db->query(sprintf('SELECT user_id, user_name,user_banned, user_firstname, user_lastname, user_gender, user_picture, user_cover,user_biography FROM users WHERE   user_banned ="0" and ( user_name LIKE %1$s OR user_firstname LIKE %1$s OR user_lastname LIKE %1$s ) AND ( user_id NOT IN (%4$s) )  ORDER BY periority desc,user_firstname ASC LIMIT 8', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false), $block_user_string)) or _error(SQL_ERROR_THROWEN);
        if ($get_limit_users->num_rows > 0) {
            while ($limit_user = $get_limit_users->fetch_assoc()) {
                if ($limit_user['user_banned'] == '0') {
                    $limit_user['user_picture'] = $this->get_picture($limit_user['user_picture'], $limit_user['user_gender']);
                    $limit_user['connection'] = $this->connection($limit_user['user_id']);
                    $limit_user['following'] = $this->checkfollowing($this->_data['user_id'], $limit_user['user_id']);
                    $results['limit_user'][] = $limit_user;
                }
            }
        }

        return $results;
    }

    public function search($query)
    {
        global $db, $system;
        $results = [];
        $offset = $_REQUEST['ofset'];
        $offset *= $system['max_results'];
        $block_user_string = 0;
        //$query = $_REQUEST['query'];
        $posts = $this->get_posts(['query' => $query, 'get' => $_POST['get'], 'filter' => $_POST['filter'], 'offset' => $_POST['offset']]);
        if (count($posts) > 0) {
            $results['posts'] = $posts;
        }
        $block_user_string = 0;
        $login_user_id = $this->_data['user_id'];
        $results['custom_login_user_id'] = $login_user_id;
        $get_block_users = $db->query(sprintf('SELECT * FROM users_blocks WHERE  user_id = %s OR blocked_id = %s', $login_user_id, $login_user_id)) or _error(SQL_ERROR_THROWEN);
        if ($get_block_users->num_rows > 0) {
            while ($blockusers = $get_block_users->fetch_assoc()) {
                $block_users_array[] = $blockusers['blocked_id'];
                $block_users_array[] = $blockusers['user_id'];
            }
            $block_user_string = implode(',', $block_users_array);
        }
        $get_users = $db->query(sprintf('SELECT user_id, user_name,user_banned, user_firstname, user_lastname, user_gender, user_picture, user_cover,user_biography FROM users WHERE user_banned ="0" and ( user_name LIKE %1$s OR user_firstname LIKE %1$s OR user_lastname LIKE %1$s ) AND ( user_id NOT IN (%4$s) ) ORDER BY periority desc, user_firstname ASC LIMIT %2$s, %3$s', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false), $block_user_string)) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                if ($user['user_banned'] == '0') {
                    $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                    $user['connection'] = $this->connection($user['user_id']);
                    $user['following'] = $this->checkfollowing($this->_data['user_id'], $user['user_id']);
                    $results['users'][] = $user;
                }
            }
        }
        $get_limit_users = $db->query(sprintf('SELECT user_id, user_name,user_banned, user_firstname, user_lastname, user_gender, user_picture, user_cover,user_biography FROM users WHERE   user_banned ="0" and ( user_name LIKE %1$s OR user_firstname LIKE %1$s OR user_lastname LIKE %1$s ) AND ( user_id NOT IN (%4$s) )  ORDER BY periority desc,user_firstname ASC LIMIT 6', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false), $block_user_string)) or _error(SQL_ERROR_THROWEN);
        if ($get_limit_users->num_rows > 0) {
            while ($limit_user = $get_limit_users->fetch_assoc()) {
                if ($limit_user['user_banned'] == '0') {
                    $limit_user['user_picture'] = $this->get_picture($limit_user['user_picture'], $limit_user['user_gender']);
                    $limit_user['connection'] = $this->connection($limit_user['user_id']);
                    $limit_user['following'] = $this->checkfollowing($this->_data['user_id'], $limit_user['user_id']);
                    $results['limit_user'][] = $limit_user;
                }
            }
        }
        /*        $get_pages = $db->query(sprintf('SELECT * FROM pages WHERE page_name LIKE %1$s OR page_title LIKE %1$s ORDER BY page_title ASC LIMIT %2$s, %3$s', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_pages->num_rows > 0) {
            while($page = $get_pages->fetch_assoc()) {
                $page['page_picture'] = $this->get_picture($page['page_picture'], 'page');
                $page['i_like'] = false;
                if($this->_logged_in) {
                    $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE page_id = %s AND user_id = %s", secure($page['page_id'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    if($get_likes->num_rows > 0) {
                        $page['i_like'] = true;
                    }
                }
                $results['pages'][] = $page;
            }
        }*/
        /*   $get_groups = $db->query(sprintf('SELECT * FROM groups WHERE group_privacy != "secret" AND (group_name LIKE %1$s OR group_title LIKE %1$s) ORDER BY group_title ASC LIMIT %2$s, %3$s', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_groups->num_rows > 0) {
            while($group = $get_groups->fetch_assoc()) {
                $group['group_picture'] = $this->get_picture($group['group_picture'], 'group');
                $group['i_joined'] = $this->check_group_membership($this->_data['user_id'], $group['group_id']);
                $results['groups'][] = $group;
            }
        }
*/
        /*$get_events = $db->query(sprintf('SELECT * FROM events WHERE event_privacy != "secret" AND event_title LIKE %1$s ORDER BY event_title ASC LIMIT %2$s, %3$s', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            while($event = $get_events->fetch_assoc()) {
                $event['event_picture'] = $this->get_picture($event['event_cover'], 'event');
                $event['i_joined'] = $this->check_event_membership($this->_data['user_id'], $event['event_id']);
                $results['events'][] = $event;
            }
        }
        */
        $get_trends = $db->query(sprintf("SELECT * FROM posts WHERE ( posts.text LIKE %s AND YEARWEEK(`time`,1) >= YEARWEEK(CURDATE(), 1) AND posts.user_type = 'user' AND ( posts.user_id NOT IN (%s) ))", secure($query, 'search'), $block_user_string)) or _error(SQL_ERROR_THROWEN);
        if ($get_trends->num_rows > 0) {
            while ($trends = $get_trends->fetch_assoc()) {
                $trends = $this->get_post($trends['post_id'], true, true);
                if ($trends) {
                    $results['trends'][] = $trends;
                }
            }
        }
        $get_media = $db->query(sprintf("SELECT post_id FROM posts WHERE ( post_type LIKE 'media' AND posts.text LIKE %s) AND ( posts.user_id NOT IN (%s) )", secure($query, 'search'), $block_user_string)) or _error(SQL_ERROR_THROWEN);
        if ($get_media->num_rows > 0) {
            while ($media = $get_media->fetch_assoc()) {
                $media = $this->get_post($media['post_id'], true, true);
                if ($media) {
                    $results['media'][] = $media;
                }
            }
        }
        $get_limit_media = $db->query(sprintf("SELECT post_id FROM posts WHERE (post_type LIKE 'media' AND posts.text LIKE %s) AND ( posts.user_id NOT IN (%s) ) LIMIT %s", secure($query, 'search'), $block_user_string, secure('1', 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_limit_media->num_rows > 0) {
            while ($limit_media = $get_limit_media->fetch_assoc()) {
                $limit_media = $this->get_post($limit_media['post_id'], true, true);
                if ($limit_media) {
                    $results['limit_media'][] = $limit_media;
                }
            }
        }
        $get_photos = $db->query(sprintf("SELECT post_id FROM posts WHERE ( post_type LIKE 'photos' AND posts.text LIKE %s) AND ( posts.user_id NOT IN (%s) )", secure($query, 'search'), $block_user_string)) or _error(SQL_ERROR_THROWEN);
        if ($get_photos->num_rows > 0) {
            while ($photos = $get_photos->fetch_assoc()) {
                $photos = $this->get_post($photos['post_id'], true, true);
                if ($photos) {
                    $results['photos'][] = $photos;
                }
            }
        }
        $get_limit_photos = $db->query(sprintf("SELECT post_id FROM posts WHERE ( post_type LIKE 'photos' AND posts.text LIKE %s) AND ( posts.user_id NOT IN (%s) ) LIMIT %s", secure($query, 'search'), $block_user_string, secure('1', 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_limit_photos->num_rows > 0) {
            while ($limit_photos = $get_limit_photos->fetch_assoc()) {
                $limit_photos = $this->get_post($limit_photos['post_id'], true, true);
                if ($limit_photos) {
                    $results['limit_photos'][] = $limit_photos;
                }
            }
        }
        /*$get_mention = $db->query(sprintf("SELECT post_id FROM posts WHERE ( posts.text LIKE %s) AND ( posts.user_id NOT IN (%s) )", secure('['.$query.']', 'search'), $block_user_string )) or _error(SQL_ERROR_THROWEN);
        if($get_mention->num_rows > 0) {
            while($mention = $get_mention->fetch_assoc()) {
                $mention = $this->get_post($mention['post_id'], true, true);
                if($mention) {
                    $results['mention'][] = $mention;
                }
            }
        }*/
        /*$get_limit_mention = $db->query(sprintf("SELECT post_id FROM posts WHERE ( posts.text LIKE %s) AND ( posts.user_id NOT IN (%s) ) LIMIT %s", secure('['.$query.']', 'search'), $block_user_string, secure('2', 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_limit_mention->num_rows > 0) {
            while($limit_mention = $get_limit_mention->fetch_assoc()) {
                $limit_mention = $this->get_post($limit_mention['post_id'], true, true);
                if($limit_mention) {
                    $results['limit_mention'][] = $limit_mention;
                }
            }
        }*/
        /*$get_articles = $db->query(sprintf("SELECT post_id FROM posts WHERE ( post_type LIKE 'article') AND ( posts.user_id NOT IN (%s) )" , $block_user_string)) or _error(SQL_ERROR_THROWEN);
        if($get_articles->num_rows > 0) {
            while($article = $get_articles->fetch_assoc()) {
                $article = $this->get_post_article($article['post_id'], true, true);
                if($article) {
                    $results['article'][] = $article;
                }
            }
        }*/
        /*$get_limit_articles = $db->query(sprintf("SELECT post_id FROM posts WHERE ( post_type LIKE 'article') AND ( posts.user_id NOT IN (%s) ) LIMIT %s", $block_user_string, secure('2', 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_limit_articles->num_rows > 0) {
            while($limit_articles = $get_limit_articles->fetch_assoc()) {
                $limit_articles = $this->get_post_article($limit_articles['post_id'], true, true);
                if($limit_articles) {
                    $results['limit_articles'][] = $limit_articles;
                }
            }
        }*/
        /*$get_polls = $db->query(sprintf("SELECT post_id FROM posts WHERE ( post_type LIKE 'poll' AND posts.text LIKE %s) AND ( posts.user_id NOT IN (%s) )", secure($query, 'search'), $block_user_string )) or _error(SQL_ERROR_THROWEN);
        if($get_polls->num_rows > 0) {
            while($polls = $get_polls->fetch_assoc()) {
                $polls = $this->get_post($polls['post_id'], true, true);
                if($polls) {
                    $results['poll'][] = $polls;
                }
            }
        }*/
        /*$get_limit_polls = $db->query(sprintf("SELECT post_id FROM posts WHERE ( post_type LIKE 'poll' AND posts.text LIKE %s) AND ( posts.user_id NOT IN (%s) ) LIMIT %s", secure($query, 'search'), $block_user_string, secure('2', 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_limit_polls->num_rows > 0) {
            while($limit_polls = $get_limit_polls->fetch_assoc()) {
                $limit_polls = $this->get_post($limit_polls['post_id'], true, true);
                if($limit_polls) {
                    $results['limit_polls'][] = $limit_polls;
                }
            }
        }*/
        return $results;
    }

    public function user_search($query)
    {
        global $db, $system;
        $results = [];
        $offset = 2;
        $get_users = $db->query(sprintf('SELECT user_id, user_name, user_firstname, user_lastname, user_gender, user_picture, user_cover,user_biography FROM users WHERE ( user_name LIKE %1$s OR user_firstname LIKE %1$s OR user_lastname LIKE %1$s ) AND ( user_id NOT IN (%4$s) ) ORDER BY user_firstname ASC LIMIT %2$s', secure($query, 'search'), secure($offset, 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                $user['connection'] = $this->connection($user['user_id']);
                $results['users_result'][] = $user;
            }
        }
        return $results;
    }

    public function trend_search($query)
    {
        global $db, $system;
        $results_trends = [];
        $offer = 2;
        $block_user_string = 0;
        $login_user_id = $this->_data['user_id'];
        $get_block_users = $db->query(sprintf('SELECT * FROM users_blocks WHERE  user_id = %s OR blocked_id = %s', $login_user_id, $login_user_id)) or _error(SQL_ERROR_THROWEN);
        if ($get_block_users->num_rows > 0) {
            while ($blockusers = $get_block_users->fetch_assoc()) {
                $block_users_array[] = $blockusers['blocked_id'];
                $block_users_array[] = $blockusers['user_id'];
            }
            $block_user_string = implode(',', $block_users_array);
        }
        $get_limit_trends = $db->query(sprintf("SELECT * FROM posts WHERE ( posts.text LIKE %s AND YEARWEEK(`time`,1) >= YEARWEEK(CURDATE(), 1) AND posts.user_type = 'user' AND ( user_id NOT IN (%s) )) LIMIT %s", secure($query, 'search'), $block_user_string, secure($offer, 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_limit_trends->num_rows > 0) {
            while ($limit_trends = $get_limit_trends->fetch_assoc()) {
                $limit_trends = $this->get_post($limit_trends['post_id'], true, true);
                if (!empty($limit_trends)) {
                    $results_trends['trends_result'][] = $limit_trends;
                }
            }
        }
        return $results_trends;
    }

    public function search_users($query, $gender, $relationship, $status)
    {
        global $db, $system;
        $results = [];
        $offset = $system['max_results'];
        if (!in_array($gender, ['any', 'male', 'female'])) {
            return $results;
        }
        if (!in_array($relationship, ['any', 'single', 'relationship', 'married', "complicated", 'separated', 'divorced', 'widowed'])) {
            return $results;
        }
        if (!in_array($status, ['any', 'online', 'offline'])) {
            return $results;
        }
        $where = "";
        $where .= ($gender != "any") ? " AND users.user_gender = '$gender'" : "";
        $where .= ($relationship != "any") ? " AND users.user_relationship = '$relationship'" : "";
        if ($status == "online") {
            $where .= " AND users_online.last_seen IS NOT NULL";
        } elseif ($status == "offline") {
            $where .= " AND users_online.last_seen IS NULL";
        }
        $login_user_id = $this->_data['user_id'];
        $get_block_users = $db->query(sprintf('SELECT * FROM users_blocks WHERE  user_id = %s OR blocked_id = %s', $login_user_id, $login_user_id)) or _error(SQL_ERROR_THROWEN);
        if ($get_block_users->num_rows > 0) {
            while ($blockusers = $get_block_users->fetch_assoc()) {
                $block_users_array[] = $blockusers['blocked_id'];
                $block_users_array[] = $blockusers['user_id'];
            }
            $block_user_string = implode(',', $block_users_array);
            $where .= " AND  users.user_id NOT IN ($block_user_string) ";
        }
        $get_users = $db->query(sprintf('SELECT users.* FROM users WHERE (users.user_name LIKE %1$s OR users.user_firstname LIKE %1$s OR users.user_lastname LIKE %1$s )' . $where . ' ORDER BY user_firstname ASC LIMIT %2$s', secure($query, 'search'), secure($system['max_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                if ($user['user_banned'] == '0') {
                    $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                    $user['connection'] = $this->connection($user['user_id']);
                    $results[] = $user;
                }
            }
        }
        return $results;
    }

    public function get_search_log()
    {
        global $db, $system;
        $results = [];
        $get_search_log = $db->query(sprintf("SELECT users_searches.log_id, users_searches.node_type, users.user_id, user_name, user_firstname, user_lastname, user_gender, user_picture, user_subscribed, user_verified, pages.*, groups.*, events.* FROM users_searches LEFT JOIN users ON users_searches.node_type = 'user' AND users_searches.node_id = users.user_id LEFT JOIN pages ON users_searches.node_type = 'page' AND users_searches.node_id = pages.page_id LEFT JOIN groups ON users_searches.node_type = 'group' AND users_searches.node_id = groups.group_id LEFT JOIN events ON users_searches.node_type = 'event' AND users_searches.node_id = events.event_id WHERE users_searches.user_id = %s ORDER BY users_searches.log_id DESC LIMIT %s", secure($this->_data['user_id'], 'int'), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        // if($get_search_log->num_rows > 0) {
        //     while($result = $get_search_log->fetch_assoc()) {
        //         switch ($result['node_type']) {
        //             case 'user':
        //                 $result['user_picture'] = $this->get_picture($result['user_picture'], $result['user_gender']);
        //                 /* get the connection between the viewer & the target */
        //                 $result['connection'] = $this->connection($result['user_id']);
        //                 break;
        //             case 'page':
        //                 $result['page_picture'] = $this->get_picture($result['page_picture'], 'page');
        //                 /* check if the viewer liked the page */
        //                 $result['i_like'] = false;
        //                 $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE page_id = %s AND user_id = %s", secure($result['page_id'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        //                 if($get_likes->num_rows > 0) {
        //                     $result['i_like'] = true;
        //                 }
        //                 break;
        //             case 'group':
        //                 $result['group_picture'] = $this->get_picture($result['group_picture'], 'group');
        //                 /* check if the viewer joined the group */
        //                 $result['i_joined'] = $this->check_group_membership($this->_data['user_id'], $result['group_id']);
        //                 break;
        //             case 'event':
        //                 $result['event_picture'] = $this->get_picture($result['event_cover'], 'event');
        //                 /* check if the viewer joined the event */
        //                 $result['i_joined'] = $this->check_event_membership($this->_data['user_id'], $result['event_id']);
        //                 break;
        //         }
        //         $result['type'] = $result['node_type'];
        //         $results[] = $result;
        //     }
        // }
        return $results;
    }

    public function add_search_log($node_id, $node_type)
    {
        global $db, $date;
        $db->query(sprintf("INSERT INTO users_searches (user_id, node_id, node_type, time) VALUES (%s, %s, %s, %s)", secure($this->_data['user_id'], 'int'), secure($node_id, 'int'), secure($node_type), secure($date)));
    }

    public function clear_search_log()
    {
        global $db, $system;
        $db->query(sprintf("DELETE FROM users_searches WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function connect($do, $id, $uid = null)
    {
        global $db;
        switch ($do) {
            case 'block':
                /* check blocking */
                if ($this->blocked($id)) {
                    throw new Exception(__("You have already blocked this user before!"));
                }
                /* remove any friendship */
                $this->connect('friend-remove', $id);
                /* delete the target from viewer's followings */
                $this->connect('unfollow', $id);
                /* delete the viewer from target's followings */
                $db->query(sprintf("DELETE FROM followings WHERE user_id = %s AND following_id = %s", secure($id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* block the user */
                $db->query(sprintf("INSERT INTO users_blocks (user_id, blocked_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                $this->delete_comment_on_block($id);
                break;
            case 'unblock':
                /* check blocking */
                if (!$this->blocked($id)) {
                    return;
                }
                /* unblock the user */
                $db->query(sprintf("DELETE FROM users_blocks WHERE user_id = %s AND blocked_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'friend-accept':
                /* check if there is a friend request from the target to the viewer */
                $check = $db->query(sprintf("SELECT * FROM friends WHERE user_one_id = %s AND user_two_id = %s AND status = 0", secure($id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if ($check->num_rows == 0) {
                    return;
                }
                /* add the target as a friend */
                $db->query(sprintf("UPDATE friends SET status = 1 WHERE user_one_id = %s AND user_two_id = %s", secure($id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* post new notification */
                $this->post_notification(['to_user_id' => $id, 'action' => 'friend_accept', 'node_url' => $this->_data['user_name']]);
                /* follow */
                $this->_follow($id);
                break;
            case 'friend-decline':
                /* check if there is a friend request from the target to the viewer */
                $check = $db->query(sprintf("SELECT * FROM friends WHERE user_one_id = %s AND user_two_id = %s AND status = 0", secure($id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if ($check->num_rows == 0) {
                    return;
                }
                /* decline this friend request */
                $db->query(sprintf("UPDATE friends SET status = -1 WHERE user_one_id = %s AND user_two_id = %s", secure($id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* unfollow */
                $this->_unfollow($id);
                break;
            case 'friend-add':
                /* check blocking */
                if ($this->blocked($id)) {
                    _error(403);
                }
                /* check if there is any relation between the viewer & the target */
                $check = $db->query(sprintf('SELECT * FROM friends WHERE (user_one_id = %1$s AND user_two_id = %2$s) OR (user_one_id = %2$s AND user_two_id = %1$s)', secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if ($check->num_rows > 0) {
                    return;
                }
                /* add the friend request */
                $db->query(sprintf("INSERT INTO friends (user_one_id, user_two_id, status) VALUES (%s, %s, '0')", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* update requests counter +1 */
                $db->query(sprintf("UPDATE users SET user_live_requests_counter = user_live_requests_counter + 1 WHERE user_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* post new notification */
                $this->post_notification(['to_user_id' => $id, 'action' => 'friend_add', 'node_url' => $this->_data['user_name']]);
                /* follow */
                $this->_follow($id);
                break;
            case 'friend-cancel':
                /* check if there is a request from the viewer to the target */
                $check = $db->query(sprintf("SELECT * FROM friends WHERE user_one_id = %s AND user_two_id = %s AND status = 0", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if ($check->num_rows == 0) {
                    return;
                }
                /* delete the friend request */
                $db->query(sprintf("DELETE FROM friends WHERE user_one_id = %s AND user_two_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* update requests counter -1 */
                $db->query(sprintf("UPDATE users SET user_live_requests_counter = IF(user_live_requests_counter=0,0,user_live_requests_counter-1), user_live_notifications_counter = IF(user_live_notifications_counter=0,0,user_live_notifications_counter-1) WHERE user_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* unfollow */
                $this->_unfollow($id);
                break;
            case 'friend-remove':
                /* check if there is any relation between me & him */
                $check = $db->query(sprintf('SELECT * FROM friends WHERE (user_one_id = %1$s AND user_two_id = %2$s AND status = 1) OR (user_one_id = %2$s AND user_two_id = %1$s AND status = 1)', secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if ($check->num_rows == 0) {
                    return;
                }
                /* delete this friend */
                $db->query(sprintf('DELETE FROM friends WHERE (user_one_id = %1$s AND user_two_id = %2$s AND status = 1) OR (user_one_id = %2$s AND user_two_id = %1$s AND status = 1)', secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'follow':
                $this->_follow($id);
                break;
            case 'unfollow':
                $this->_unfollow($id);
                break;
            case 'page-like':
                /* check if the viewer already liked this page */
                $check = $db->query(sprintf("SELECT * FROM pages_likes WHERE user_id = %s AND page_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if ($check->num_rows > 0) {
                    return;
                }
                /* if no -> like this page */
                $db->query(sprintf("INSERT INTO pages_likes (user_id, page_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* update likes counter +1 */
                $db->query(sprintf("UPDATE pages SET page_likes = page_likes + 1  WHERE page_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'page-unlike':
                /* check if the viewer already liked this page */
                $check = $db->query(sprintf("SELECT * FROM pages_likes WHERE user_id = %s AND page_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if ($check->num_rows == 0) {
                    return;
                }
                /* if yes -> unlike this page */
                $db->query(sprintf("DELETE FROM pages_likes WHERE user_id = %s AND page_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* update likes counter -1 */
                $db->query(sprintf("UPDATE pages SET page_likes = IF(page_likes=0,0,page_likes-1) WHERE page_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'page-boost':
                /* check if the user is the page admin */
                $check = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s AND page_admin = %s", secure($id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($check->num_rows == 0) {
                    _error(403);
                }
                $spage = $check->fetch_assoc();
                /* check if viewer can boost page */
                if (!$this->_data['can_boost_pages']) {
                    modal(MESSAGE, __("Sorry"), __("You reached the maximum number of boosted pages! Upgrade your package to get more"));
                }
                /* boost page */
                if (!$spage['page_boosted']) {
                    /* boost page */
                    $db->query(sprintf("UPDATE pages SET page_boosted = '1' WHERE page_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_boosted_pages = user_boosted_pages + 1 WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                }
                break;
            case 'page-unboost':
                /* check if the user is the page admin */
                $check = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s AND page_admin = %s", secure($id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($check->num_rows == 0) {
                    _error(403);
                }
                $spage = $check->fetch_assoc();
                /* unboost page */
                if ($spage['page_boosted']) {
                    /* unboost page */
                    $db->query(sprintf("UPDATE pages SET page_boosted = '0' WHERE page_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_boosted_pages = IF(user_boosted_pages=0,0,user_boosted_pages-1) WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                }
                break;
            case 'page-invite':
                if ($uid == null) {
                    _error(400);
                }
                /* check if the viewer liked the page */
                $check_viewer = $db->query(sprintf("SELECT pages.* FROM pages INNER JOIN pages_likes ON pages.page_id = pages_likes.page_id WHERE pages_likes.user_id = %s AND pages_likes.page_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if ($check_viewer->num_rows == 0) {
                    return;
                }
                /* check if the target already liked this page */
                $check_target = $db->query(sprintf("SELECT * FROM pages_likes WHERE user_id = %s AND page_id = %s", secure($uid, 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if ($check_target->num_rows > 0) {
                    return;
                }
                /* check if the viewer already invited to the viewer to this page */
                $check_target = $db->query(sprintf("SELECT * FROM pages_invites WHERE page_id = %s AND user_id = %s AND from_user_id = %s", secure($id, 'int'), secure($uid, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if ($check_target->num_rows > 0) {
                    return;
                }
                /* if no -> invite to this page */
                /* get page */
                $page = $check_viewer->fetch_assoc();
                /* insert invitation */
                $db->query(sprintf("INSERT INTO pages_invites (page_id, user_id, from_user_id) VALUES (%s, %s, %s)", secure($id, 'int'), secure($uid, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* send notification (page invitation) to the target user */
                $this->post_notification(['to_user_id' => $uid, 'action' => 'page_invitation', 'node_type' => $page['page_title'], 'node_url' => $page['page_name']]);
                break;
            case 'group-join':
                /* check if the viewer already joined (approved||pending) this group */
                $check = $db->query(sprintf("SELECT * FROM groups_members WHERE user_id = %s AND group_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if ($check->num_rows > 0) {
                    return;
                }
                /* if no -> join this group */
                /* get group */
                $get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                $group = $get_group->fetch_assoc();
                /* check approved */
                $approved = 0;
                if ($group['group_privacy'] == 'public') {
                    /* the group is public */
                    $approved = '1';
                } elseif ($this->_data['user_id'] == $group['group_admin']) {
                    /* the group admin join his group */
                    $approved = '1';
                }
                $db->query(sprintf("INSERT INTO groups_members (user_id, group_id, approved) VALUES (%s, %s, %s)", secure($this->_data['user_id'], 'int'), secure($id, 'int'), secure($approved))) or _error(SQL_ERROR_THROWEN);
                if ($approved) {
                    /* update members counter +1 */
                    $db->query(sprintf("UPDATE groups SET group_members = group_members + 1  WHERE group_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                } else {
                    /* send notification (pending request) to group admin  */
                    $this->post_notification(['to_user_id' => $group['group_admin'], 'action' => 'group_join', 'node_type' => $group['group_title'], 'node_url' => $group['group_name']]);
                }
                break;
            case 'group-leave':
                /* check if the viewer already joined (approved||pending) this group */
                $check = $db->query(sprintf("SELECT groups_members.approved, groups.* FROM groups_members INNER JOIN groups ON groups_members.group_id = groups.group_id WHERE groups_members.user_id = %s AND groups_members.group_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if ($check->num_rows == 0) {
                    return;
                }
                /* if yes -> leave this group */
                $group = $check->fetch_assoc();
                $db->query(sprintf("DELETE FROM groups_members WHERE user_id = %s AND group_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* update members counter -1 */
                if ($group['approved']) {
                    $db->query(sprintf("UPDATE groups SET group_members = IF(group_members=0,0,group_members-1) WHERE group_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                } else {
                    /* delete notification (pending request) that sent to group admin */
                    $this->delete_notification($group['group_admin'], 'group_join', $group['group_title'], $group['group_name']);
                }
                break;
            case 'group-invite':
                if ($uid == null) {
                    _error(400);
                }
                /* check if the viewer is group member (approved) */
                $check_viewer = $db->query(sprintf("SELECT groups.* FROM groups INNER JOIN groups_members ON groups.group_id = groups_members.group_id WHERE groups_members.approved = '1' AND groups_members.user_id = %s AND groups_members.group_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if ($check_viewer->num_rows == 0) {
                    return;
                }
                /* check if the target already joined (approved||pending) this group */
                $check_target = $db->query(sprintf("SELECT * FROM groups_members WHERE user_id = %s AND group_id = %s", secure($uid, 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if ($check_target->num_rows > 0) {
                    return;
                }
                /* if no -> join this group */
                /* get group */
                $group = $check_viewer->fetch_assoc();
                /* check approved */
                $approved = 0;
                if ($group['group_privacy'] == 'public') {
                    /* the group is public */
                    $approved = '1';
                } elseif ($this->_data['user_id'] == $group['group_admin']) {
                    /* the group admin want to add a user */
                    $approved = '1';
                } elseif ($uid == $group['group_admin']) {
                    /* the viewer invite group admin to join his group */
                    $approved = '1';
                }
                /* add the target user as group member */
                $db->query(sprintf("INSERT INTO groups_members (user_id, group_id, approved) VALUES (%s, %s, %s)", secure($uid, 'int'), secure($id, 'int'), secure($approved))) or _error(SQL_ERROR_THROWEN);
                if ($approved) {
                    /* update members counter +1 */
                    $db->query(sprintf("UPDATE groups SET group_members = group_members + 1  WHERE group_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                    /* send notification (group addition) to the target user */
                    $this->post_notification(['to_user_id' => $uid, 'action' => 'group_add', 'node_type' => $group['group_title'], 'node_url' => $group['group_name']]);
                } else {
                    /* send notification (group addition) to the target user */
                    if ($group['group_privacy'] != 'secret') {
                        $this->post_notification(['to_user_id' => $uid, 'action' => 'group_add', 'node_type' => $group['group_title'], 'node_url' => $group['group_name']]);
                    }
                    /* send notification (pending request) to group admin [from the target]  */
                    $this->post_notification(['to_user_id' => $group['group_admin'], 'from_user_id' => $uid, 'action' => 'group_join', 'node_type' => $group['group_title'], 'node_url' => $group['group_name']]);
                }
                break;
            case 'group-accept':
                if ($uid == null) {
                    _error(400);
                }
                /* check if the target has pending request */
                $check = $db->query(sprintf("SELECT groups.* FROM groups INNER JOIN groups_members ON groups.group_id = groups_members.group_id WHERE groups_members.approved = '0' AND groups_members.user_id = %s AND groups_members.group_id = %s", secure($uid, 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if ($check->num_rows == 0) {
                    return;
                }
                $group = $check->fetch_assoc();
                /* check if the viewer is group admin */
                if ($this->_data['user_id'] != $group['group_admin']) {
                    _error(400);
                }
                /* update request */
                $db->query(sprintf("UPDATE groups_members SET approved = '1' WHERE user_id = %s AND group_id = %s", secure($uid, 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* update members counter +1 */
                $db->query(sprintf("UPDATE groups SET group_members = group_members + 1  WHERE group_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* send notification (group acceptance) to the target user */
                $this->post_notification(['to_user_id' => $uid, 'action' => 'group_accept', 'node_type' => $group['group_title'], 'node_url' => $group['group_name']]);
                break;
            case 'group-decline':
                if ($uid == null) {
                    _error(400);
                }
                /* check if the target has pending request */
                $check = $db->query(sprintf("SELECT groups.* FROM groups INNER JOIN groups_members ON groups.group_id = groups_members.group_id WHERE groups_members.approved = '0' AND groups_members.user_id = %s AND groups_members.group_id = %s", secure($uid, 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if ($check->num_rows == 0) {
                    return;
                }
                $group = $check->fetch_assoc();
                /* check if the viewer is group admin */
                if ($this->_data['user_id'] != $group['group_admin']) {
                    _error(400);
                }
                /* delete request */
                $db->query(sprintf("DELETE FROM groups_members WHERE user_id = %s AND group_id = %s", secure($uid, 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'event-go':
                /* check if the viewer member to this event */
                $check = $db->query(sprintf("SELECT * FROM events_members WHERE user_id = %s AND event_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                $invited = false;
                $interested = false;
                if ($check->num_rows > 0) {
                    $member = $check->fetch_assoc();
                    /* if going -> return */
                    if ($member['is_going'] == '1') {
                        return;
                    }
                    $invited = ($member['is_invited'] == '1') ? true : false;
                    $interested = ($member['is_interested'] == '1') ? true : false;
                }
                $approved = false;
                if ($invited || $interested) {
                    $approved = true;
                } else {
                    /* get event */
                    $get_event = $db->query(sprintf("SELECT * FROM events WHERE event_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                    $event = $get_event->fetch_assoc();
                    if ($event['event_privacy'] == 'public') {
                        /* the event is public */
                        $approved = true;
                    } elseif ($this->_data['user_id'] == $event['event_admin']) {
                        /* the event admin going his event */
                        $approved = true;
                    }
                }
                if ($approved) {
                    if ($invited || $interested) {
                        $db->query(sprintf("UPDATE events_members SET is_going = '1' WHERE user_id = %s AND event_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                    } else {
                        $db->query(sprintf("INSERT INTO events_members (user_id, event_id, is_going) VALUES (%s, %s, '1')", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                    }
                    /* update going counter +1 */
                    $db->query(sprintf("UPDATE events SET event_going = event_going + 1  WHERE event_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
                break;
            case 'event-ungo':
                /* check if the viewer member to this event */
                $check = $db->query(sprintf("SELECT * FROM events_members WHERE user_id = %s AND event_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if ($check->num_rows == 0) {
                    return;
                }
                $invited = false;
                $interested = false;
                $member = $check->fetch_assoc();
                /* if not going -> return */
                if ($member['is_going'] == '0') {
                    return;
                }
                $invited = ($member['is_invited'] == '1') ? true : false;
                $interested = ($member['is_interested'] == '1') ? true : false;
                if (!$invited && !$interested) {
                    $db->query(sprintf("DELETE FROM events_members WHERE user_id = %s AND event_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                } else {
                    $db->query(sprintf("UPDATE events_members SET is_going = '0' WHERE user_id = %s AND event_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
                /* update going counter -1 */
                $db->query(sprintf("UPDATE events SET event_going = IF(event_going=0,0,event_going-1)  WHERE event_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'event-interest':
                /* check if the viewer member to this event */
                $check = $db->query(sprintf("SELECT * FROM events_members WHERE user_id = %s AND event_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                $invited = false;
                $going = false;
                if ($check->num_rows > 0) {
                    $member = $check->fetch_assoc();
                    /* if interested -> return */
                    if ($member['is_interested'] == '1') {
                        return;
                    }
                    $invited = ($member['is_invited'] == '1') ? true : false;
                    $going = ($member['is_going'] == '1') ? true : false;
                }
                $approved = false;
                if ($invited || $going) {
                    $approved = true;
                } else {
                    /* get event */
                    $get_event = $db->query(sprintf("SELECT * FROM events WHERE event_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                    $event = $get_event->fetch_assoc();
                    if ($event['event_privacy'] == 'public') {
                        /* the event is public */
                        $approved = true;
                    } elseif ($this->_data['user_id'] == $event['event_admin']) {
                        /* the event admin interested his event */
                        $approved = true;
                    }
                }
                if ($approved) {
                    if ($invited || $going) {
                        $db->query(sprintf("UPDATE events_members SET is_interested = '1' WHERE user_id = %s AND event_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                    } else {
                        $db->query(sprintf("INSERT INTO events_members (user_id, event_id, is_interested) VALUES (%s, %s, '1')", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                    }
                    /* update interested counter +1 */
                    $db->query(sprintf("UPDATE events SET event_interested = event_interested + 1  WHERE event_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
                break;
            case 'event-uninterest':
                /* check if the viewer member to this event */
                $check = $db->query(sprintf("SELECT * FROM events_members WHERE user_id = %s AND event_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if ($check->num_rows == 0) {
                    return;
                }
                $invited = false;
                $going = false;
                $member = $check->fetch_assoc();
                /* if not interested -> return */
                if ($member['is_interested'] == '0') {
                    return;
                }
                $invited = ($member['is_invited'] == '1') ? true : false;
                $going = ($member['is_going'] == '1') ? true : false;
                if (!$invited && !$going) {
                    $db->query(sprintf("DELETE FROM events_members WHERE user_id = %s AND event_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                } else {
                    $db->query(sprintf("UPDATE events_members SET is_interested = '0' WHERE user_id = %s AND event_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
                /* update interested counter -1 */
                $db->query(sprintf("UPDATE events SET event_interested = IF(event_interested=0,0,event_interested-1)  WHERE event_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'event-invite':
                if ($uid == null) {
                    _error(400);
                }
                /* check if the viewer is event member */
                $check_viewer = $db->query(sprintf("SELECT events.* FROM events INNER JOIN events_members ON events.event_id = events_members.event_id WHERE events_members.user_id = %s AND events_members.event_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if ($check_viewer->num_rows == 0) {
                    return;
                }
                /* check if the target already event member */
                $check_target = $db->query(sprintf("SELECT * FROM events_members WHERE user_id = %s AND event_id = %s", secure($uid, 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if ($check_target->num_rows > 0) {
                    return;
                }
                /* get event */
                $event = $check_viewer->fetch_assoc();
                /* insert invitation */
                $db->query(sprintf("INSERT INTO events_members (user_id, event_id, is_invited) VALUES (%s, %s, '1')", secure($uid, 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* update invited counter +1 */
                $db->query(sprintf("UPDATE events SET event_invited = event_invited + 1  WHERE event_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* send notification (page invitation) to the target user */
                $this->post_notification(['to_user_id' => $uid, 'action' => 'event_invitation', 'node_type' => $event['event_title'], 'node_url' => $event['event_id']]);
                break;
        }
    }

    public function connection($user_id, $friendship = true)
    {
        /* check which type of connection (friendship|follow) connections to get */
        if ($friendship) {
            /* check if there is a logged user */
            if (!$this->_logged_in) {
                /* no logged user */
                return "add";
            }
            /* check if the viewer is the target */
            if ($user_id == $this->_data['user_id']) {
                return "me";
            }
            /* check if the viewer & the target are friends */
            if (in_array($user_id, $this->_data['friends_ids'])) {
                return "remove";
            }
            /* check if the target sent a request to the viewer */
            if (in_array($user_id, $this->_data['friend_requests_ids'])) {
                return "request";
            }
            /* check if the viewer sent a request to the target */
            if (in_array($user_id, $this->_data['friend_requests_sent_ids'])) {
                return "cancel";
            }
            /* there is no relation between the viewer & the target */
            return "add";
        } else {
            /* check if there is a logged user */
            if (!$this->_logged_in) {
                /* no logged user */
                return "follow";
            }
            /* check if the viewer is the target */
            if ($user_id == $this->_data['user_id']) {
                return "me";
            }
            if (in_array($user_id, $this->_data['followings_ids'])) {
                /* the viewer follow the target */
                return "unfollow";
            } else {
                /* the viewer not follow the target */
                return "follow";
            }
        }
    }

    public function banned($user_id)
    {
        global $db;
        $check = $db->query(sprintf("SELECT * FROM users WHERE user_banned = '1' AND user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($check->num_rows > 0) {
            return true;
        }
        return false;
    }

    public function blocked($user_id)
    {
        global $db;
        /* check if there is any blocking between the viewer & the target */
        if ($this->_logged_in) {
            $check = $db->query(sprintf('SELECT * FROM users_blocks WHERE (user_id = %1$s AND blocked_id = %2$s) OR (user_id = %2$s AND blocked_id = %1$s)', secure($this->_data['user_id'], 'int'), secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($check->num_rows > 0) {
                return true;
            }
        }
        return false;
    }

    public function delete_user($user_id)
    {
        global $db;
        /* (check&get) user */
        $get_user = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_user->num_rows == 0) {
            _error(403);
        }
        $user = $get_user->fetch_assoc();
        // delete user
        $can_delete = false;
        /* target is (admin|moderator) */
        if ($user['user_group'] < 3) {
            throw new Exception(__("You can not delete admin/morderator accounts"));
        }
        /* viewer is (admin|moderator) */
        if ($this->_data['user_group'] < 3) {
            $can_delete = true;
        }
        /* viewer is the target */
        if ($this->_data['user_id'] == $user_id) {
            $can_delete = true;
        }
        /* delete the user */
        if ($can_delete) {
            /* delete the user */
            $db->query(sprintf("DELETE FROM users WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        }
    }

    private function _follow($user_id)
    {
        global $db;
        /* check blocking */
        if ($this->blocked($user_id)) {
            _error(403);
        }
        /* check if the viewer already follow the target */
        $check = $db->query(sprintf("SELECT * FROM followings WHERE user_id = %s AND following_id = %s", secure($this->_data['user_id'], 'int'), secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* if yes -> return */
        if ($check->num_rows > 0) {
            return;
        }
        /* add as following */
        $db->query(sprintf("INSERT INTO followings (user_id, following_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'), secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* post new notification */
        $this->post_notification(['to_user_id' => $user_id, 'action' => 'follow']);
    }

    private function _unfollow($user_id)
    {
        global $db;
        /* check if the viewer already follow the target */
        $check = $db->query(sprintf("SELECT * FROM followings WHERE user_id = %s AND following_id = %s", secure($this->_data['user_id'], 'int'), secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* if no -> return */
        if ($check->num_rows == 0) {
            return;
        }
        /* delete from viewer's followings */
        $db->query(sprintf("DELETE FROM followings WHERE user_id = %s AND following_id = %s", secure($this->_data['user_id'], 'int'), secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* delete notification */
        $this->delete_notification($user_id, 'follow');
    }

    public function live_counters_reset($counter)
    {
        global $db;
        if ($counter == "friend_requests") {
            $db->query(sprintf("UPDATE users SET user_live_requests_counter = 0 WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        } elseif ($counter == "messages") {
            $db->query(sprintf("UPDATE users SET user_live_messages_counter = 0 WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        } elseif ($counter == "notifications") {
//            $db->query(sprintf("/ WHERE user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            $db->query(sprintf("UPDATE notifications SET seen = '1' WHERE to_user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }
    }

    public function get_notifications($offset = 0, $last_notification_id = null)
    {
        global $db, $system;
        $offset *= 15;
        $notifications = [];
        if ($last_notification_id !== null) {
            $get_notifications = $db->query(sprintf("SELECT notifications.*, users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM notifications INNER JOIN users ON notifications.from_user_id = users.user_id WHERE notifications.to_user_id = %s AND notifications.notification_id > %s ORDER BY notifications.notification_id DESC", secure($this->_data['user_id'], 'int'), secure($last_notification_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_notifications = $db->query(sprintf("SELECT notifications.*, users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM notifications INNER JOIN users ON notifications.from_user_id = users.user_id WHERE notifications.to_user_id = %s ORDER BY notifications.notification_id DESC LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure(15, 'int', false))) or _error(SQL_ERROR_THROWEN);
        }
        if ($get_notifications->num_rows > 0) {

			$count = 0;

            while ($notification = $get_notifications->fetch_assoc()) {
                $notification['user_picture'] = $this->get_picture($notification['user_picture'], $notification['user_gender']);
                $notification['notify_id'] = ($notification['notify_id']) ? "?notify_id=" . $notification['notify_id'] : "";
                /* parse notification */
                switch ($notification['action']) {
                    case 'friend_add':
                        $notification['icon'] = "fa-user-plus";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = __("send you a friend request");
                        break;
                    case 'friend_accept':
                        $notification['icon'] = "fa-user-plus";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = __("accepted your friend request");
                        break;
                    case 'follow':
                        $notification['icon'] = "Icon--follower";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = __("now following you");
                        break;
                    case 'like':
                        $notification['icon'] = "Icon--heart";
                        if ($notification['node_type'] == "post") {
                            // $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = __("likes your post");
                        } elseif ($notification['node_type'] == "photo") {
                            // $notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = __("likes your photo");
                        } elseif ($notification['node_type'] == "post_comment") {
                            // $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = __("likes your comment");
                        } elseif ($notification['node_type'] == "photo_comment") {
                            //$notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = __("likes your comment");
                        } elseif ($notification['node_type'] == "post_reply") {
                            //$notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = __("likes your reply");
                        } elseif ($notification['node_type'] == "photo_reply") {
                            //$notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = __("likes your reply");
                        }
                        break;
                    case 'comment':
                        $notification['icon'] = "Icon--reply";
                        if ($notification['node_type'] == "post") {
                            //$notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = __("commented on your post");
                        } elseif ($notification['node_type'] == "photo") {
                            //$notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = __("commented on your photo");
                        }
                        break;
                    case 'reply':
                        $notification['icon'] = "Icon--reply";
                        if ($notification['node_type'] == "post") {
                            //$notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        } elseif ($notification['node_type'] == "photo") {
                            //$notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        }
                        $notification['message'] = __("replied to your comment");
                        break;
                    case 'share':
                        $notification['icon'] = "Icon--retweet";
                        //$notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'];
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = __("shared your post");
                        break;
                    case 'vote':
                        $notification['icon'] = "Icon--check";
                        $notification['url'] = $system['system_url'] . '/posts/' . $notification['node_url'];
                        $notification['message'] = __("voted on your poll");
                        break;
                    case 'mention':
                        $notification['icon'] = "Icon--follower";
                        switch ($notification['node_type']) {
                            case 'post':
                                $notification['url'] = $system['system_url'] . '/posts/' . $notification['node_url'];
                                $notification['message'] = __("mentioned you in a post");
                                break;
                            case 'comment_post':
                                $notification['url'] = $system['system_url'] . '/posts/' . $notification['node_url'] . $notification['notify_id'];
                                $notification['message'] = __("mentioned you in a comment");
                                break;
                            case 'comment_photo':
                                $notification['url'] = $system['system_url'] . '/photos/' . $notification['node_url'] . $notification['notify_id'];
                                $notification['message'] = __("mentioned you in a comment");
                                break;
                            case 'reply_post':
                                $notification['url'] = $system['system_url'] . '/posts/' . $notification['node_url'] . $notification['notify_id'];
                                $notification['message'] = __("mentioned you in a reply");
                                break;
                            case 'reply_photo':
                                $notification['url'] = $system['system_url'] . '/photos/' . $notification['node_url'] . $notification['notify_id'];
                                $notification['message'] = __("mentioned you in a reply");
                                break;
                        }
                        break;
                    case 'profile_visit':
                        $notification['icon'] = "fa-eye";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = __("visited your profile");
                        break;
                    case 'wall':
                        $notification['icon'] = "fa-comment";
                        $notification['url'] = $system['system_url'] . '/posts/' . $notification['node_url'];
                        $notification['message'] = __("posted on your wall");
                        break;
                    case 'page_invitation':
                        $notification['icon'] = "fa-flag";
                        $notification['url'] = $system['system_url'] . '/pages/' . $notification['node_url'];
                        $notification['message'] = __("invite you to like a page") . " '" . $notification['node_type'] . "'";
                        break;
                    case 'group_join':
                        $notification['icon'] = "fa-users";
                        $notification['url'] = $system['system_url'] . '/groups/' . $notification['node_url'] . '/settings/requests';
                        $notification['message'] = __("asked to join your group") . " '" . $notification['node_type'] . "'";
                        break;
                    case 'group_add':
                        $notification['icon'] = "fa-users";
                        $notification['url'] = $system['system_url'] . '/groups/' . $notification['node_url'];
                        $notification['message'] = __("added you to") . " '" . $notification['node_type'] . "' " . __("group");
                        break;
                    case 'group_accept':
                        $notification['icon'] = "fa-users";
                        $notification['url'] = $system['system_url'] . '/groups/' . $notification['node_url'];
                        $notification['message'] = __("accepted your request to join") . " '" . $notification['node_type'] . "' " . __("group");
                        break;
                    case 'event_invitation':
                        $notification['icon'] = "fa-calendar";
                        $notification['url'] = $system['system_url'] . '/events/' . $notification['node_url'];
                        $notification['message'] = __("invite you to join an event") . " '" . $notification['node_type'] . "'";
                        break;
                    case 'live':
                        $notification['icon'] = "Icon--cameraVideo";
                        $notification['url'] = $system['system_url'] . '/video_post?post_id=' . $notification['node_url'];
                        $notification['message'] = __("started a live video");
                        break;
                    case 'invite':
                        $data = [
                          'accessToken' => $this->_data['accessToken'],
                          'inviteToken' => $notification['node_url']
                        ];
                        $encrypt_data = urlencode(self::key_encrypt(json_encode($data)));
                        $notification['icon'] = "Icon--cameraVideo";
                        $notification['url'] = SYS_URL_API . '/live-broadcast/setup-broadcast.php?action=invite&token=' . $encrypt_data;
                        $notification['message'] = __("has just invited you to join a broadcast event!");
                        break;
                }
                $notification['full_message'] = $notification['user_firstname'] . " " . $notification['user_lastname'] . " " . $notification['message'];
                $notifications[] = $notification;

				//Formatted Time EST
				if($notifications[$count]['time'] != ''){
					$guo_datetime = $notifications[$count]['time'];
					$notifications[$count]['time'] = $this->twitter_type_datetime($guo_datetime);
					$notifications[$count]['formatted_time'] = $this->new_formatted_time($guo_datetime);
				}
				$count++;
				//Formatted Time EST End
            }
        }
        return $notifications;
    }

    public function post_notification($args = [])
    {
        global $db, $date, $system;
        /* initialize arguments */
        $to_user_id = !isset($args['to_user_id']) ? _error(400) : $args['to_user_id'];
        $from_user_id = !isset($args['from_user_id']) ? $this->_data['user_id'] : $args['from_user_id'];
        $action = !isset($args['action']) ? _error(400) : $args['action'];
        $node_type = !isset($args['node_type']) ? '' : $args['node_type'];
        $node_url = !isset($args['node_url']) ? '' : $args['node_url'];
        $notify_id = !isset($args['notify_id']) ? '' : $args['notify_id'];
        /* if the viewer is the target */
        if ($this->_data['user_id'] == $to_user_id) {
            return;
        }
        /* get receiver user */
        $get_receiver = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s", secure($to_user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_receiver->num_rows == 0) {
            return;
        }
        /* insert notification */
        $db->query(sprintf("INSERT INTO notifications (to_user_id, from_user_id, action, node_type, node_url, notify_id, time) VALUES (%s, %s, %s, %s, %s, %s, %s)", secure($to_user_id, 'int'), secure($from_user_id, 'int'), secure($action), secure($node_type), secure($node_url), secure($notify_id), secure($date))) or _error(SQL_ERROR_THROWEN);
        /* update notifications counter +1 */
        // $db->query(sprintf("UPDATE users SET user_live_notifications_counter = user_live_notifications_counter + 1 WHERE user_id = %s", secure($to_user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        // Email Notifications

		$receiver = $get_receiver->fetch_assoc();

        if (/*$system['email_notifications']*/ 1) {
            /* prepare receiver */
            /* parse notification */
            $notify_id = ($notify_id != '') ? "?notify_id=" . $notify_id : "";
            switch ($action) {
                case 'friend_add':
//                    if ($system['email_friend_requests'] && $receiver['email_friend_requests']) {
                        $notification['url'] = $system['system_url'] . '/' . $node_url;
                        $notification['message'] = __("send you a friend request");
 //                   }
                    break;
                case 'friend_accept':
     //               if ($system['email_friend_requests'] && $receiver['email_friend_requests']) {
                        $notification['url'] = $system['system_url'] . '/' . $node_url;
                        $notification['message'] = __("accepted your friend request");
      //              }
                    break;
                case 'like':
    //                if ($system['email_post_likes'] && $receiver['email_post_likes']) {
                        if ($node_type == "post") {
                            $notification['url'] = $system['system_url'] . '/posts/' . $node_url;
                            $notification['message'] = __("likes your post");
                        } elseif ($node_type == "photo") {
                            $notification['url'] = $system['system_url'] . '/photos/' . $node_url;
                            $notification['message'] = __("likes your photo");
                        } elseif ($node_type == "post_comment") {
                            $notification['url'] = $system['system_url'] . '/posts/' . $node_url . $notify_id;
                            $notification['message'] = __("likes your comment");
                        } elseif ($node_type == "photo_comment") {
                            $notification['url'] = $system['system_url'] . '/photos/' . $node_url . $notify_id;
                            $notification['message'] = __("likes your comment");
                        } elseif ($node_type == "post_reply") {
                            $notification['url'] = $system['system_url'] . '/posts/' . $node_url . $notify_id;
                            $notification['message'] = __("likes your reply");
                        } elseif ($node_type == "photo_reply") {
                            $notification['url'] = $system['system_url'] . '/photos/' . $node_url . $notify_id;
                            $notification['message'] = __("likes your reply");
                        }
      //              }
                    break;
                case 'comment':
      //              if ($system['email_post_comments'] && $receiver['email_post_comments']) {
                        if ($node_type == "post") {
                            $notification['url'] = $system['system_url'] . '/posts/' . $node_url . $notify_id;
                            $notification['message'] = __("commented on your post");
                        } elseif ($node_type == "photo") {
                            $notification['url'] = $system['system_url'] . '/photos/' . $node_url . $notify_id;
                            $notification['message'] = __("commented on your photo");
                        }
      //              }
                    break;
                case 'reply':
       //             if ($system['email_post_comments'] && $receiver['email_post_comments']) {
                        if ($node_type == "post") {
                            $notification['url'] = $system['system_url'] . '/posts/' . $node_url . $notify_id;
                        } elseif ($node_type == "photo") {
                            $notification['url'] = $system['system_url'] . '/photos/' . $node_url . $notify_id;
                        }
                        $notification['message'] = __("replied to your comment");
        //            }
                    break;
                case 'share':
         //           if ($system['email_post_shares'] && $receiver['email_post_shares']) {
                        $notification['url'] = $system['system_url'] . '/posts/' . $node_url;
                        $notification['message'] = __("shared your post");
         //           }
                    break;
                case 'mention':
          //          if ($system['email_mentions'] && $receiver['email_mentions']) {
                        $notification['icon'] = "Icon--follower";
                        switch ($node_type) {
                            case 'post':
                                $notification['url'] = $system['system_url'] . '/posts/' . $node_url;
                                $notification['message'] = __("mentioned you in a post");
                                break;
                            case 'comment_post':
                                $notification['url'] = $system['system_url'] . '/posts/' . $node_url . $notify_id;
                                $notification['message'] = __("mentioned you in a comment");
                                break;
                            case 'comment_photo':
                                $notification['url'] = $system['system_url'] . '/photos/' . $node_url . $notify_id;
                                $notification['message'] = __("mentioned you in a comment");
                                break;
                            case 'reply_post':
                                $notification['url'] = $system['system_url'] . '/posts/' . $node_url . $notify_id;
                                $notification['message'] = __("mentioned you in a reply");
                                break;
                            case 'reply_photo':
                                $notification['url'] = $system['system_url'] . '/photos/' . $node_url . $notify_id;
                                $notification['message'] = __("mentioned you in a reply");
                                break;
                        }
            //        }
                    break;
                case 'profile_visit':
        //            if ($system['email_profile_visits'] && $receiver['email_profile_visits']) {
                        $notification['url'] = $system['system_url'] . '/' . $this->_data['user_name'];
                        $notification['message'] = __("visited your profile");
         //           }
                    break;
                case 'new_post':
                //    if ($system['email_friend_requests'] && $receiver['email_friend_requests']) {
                        $notification['url'] = $system['system_url'] . '/posts/' . $node_url;
                        $notification['message'] = __("added new post");
                  //  }
                    break;                case 'wall':
           //         if ($system['email_wall_posts'] && $receiver['email_wall_posts']) {
                        $notification['url'] = $system['system_url'] . '/posts/' . $node_url;
                        $notification['message'] = __("posted on your wall");
       //             }
                    break;
                default:
                    return;
                    break;
            }
            /* prepare notification email */

			if($receiver['notification_activated'] == 1){
				if ($notification['message']) {
					$_message =  $this->_data['user_name'] . " " . $notification['message'];
					//echo $node_url;
					//var_dump($this->getTokenByUserId($args['to_user_id']));
					if(!empty($this->getTokenByUserId($args['to_user_id']))) {
						foreach($this->getTokenByUserId($args['to_user_id']) as $val){
							if($val['device_type'] != 'iPhone'){
								//send android
								//{"gcm.notification.body":"chetan ghagre","title":"Testing here chetan","subtitle":"This is a subtitle. subtitle","tickerText":"Ticker text here...Ticker text here...Ticker text here","vibrate":1,"sound":1,"largeIcon":"large_icon","smallIcon":"small_icon"}
								$data = array("gcm.notification.body" => $_message, 'postId' => $node_url);
								$this->send_push_notifications(array($val['device_token']), $data);
							}
							else{
								//send iOS
								//{"alert":"testing chetan here","title":"Testing here chetan","subtitle":"This is a subtitle. subtitle","postId":123456,"vibrate":1,"sound":1,"largeIcon":"large_icon","smallIcon":"small_icon"}
								$cnt = $this->notification_unread_count($args['to_user_id']);
                                $post = $this->get_post_api($node_url);
							     $data = array('alert' => $_message.':'.$post['text_plain'], 'postId' => $node_url, 'badge' => $cnt['response']['unread_count']);
								//$data = array('alert' => $_message, 'postId' => $node_url, 'badge' => 1);
								$this->send_push_notifications_ios($val['device_token'], $data);
							}
						}
					}
					//Email::send_notification($receiver['user_email'], $receiver['user_firstname'], $receiver['user_lastname'], $_message, $notification['url']);
				}
			}
        }
    }

    public function delete_notification($to_user_id, $action, $node_type = '', $node_url = '')
    {
        global $db;
        /* delete notification */
        $db->query(sprintf("DELETE FROM notifications WHERE to_user_id = %s AND from_user_id = %s AND action = %s AND node_type = %s AND node_url = %s", secure($to_user_id, 'int'), secure($this->_data['user_id'], 'int'), secure($action), secure($node_type), secure($node_url))) or _error(SQL_ERROR_THROWEN);
        /* update notifications counter -1 */
        $db->query(sprintf("UPDATE users SET user_live_notifications_counter = IF(user_live_notifications_counter=0,0,user_live_notifications_counter-1) WHERE user_id = %s", secure($to_user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function user_online($user_id)
    {
        global $db;
        /* first -> check if the target user enable the chat */
        $get_user_status = $db->query(sprintf("SELECT * FROM users WHERE user_chat_enabled = '1' AND user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_user_status->num_rows == 0) {
            /* if no > return false */
            return false;
        }
        /* second -> check if the target user is friend to the viewer */
        if (!in_array($user_id, $this->_data['friends_ids'])) {
            /* if no > return false */
            return false;
        }
        /* third > check if the target user is online */
        $check_user_online = $db->query(sprintf("SELECT * FROM users_online WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($check_user_online->num_rows == 0) {
            /* if no > return false */
            return false;
        } else {
            /* if yes > return true */
            return true;
        }
    }

    public function get_online_friends()
    {
        global $db, $system, $date;

        /* get online friends */
        $online_friends = [];
        /* check if the viewer has friends */
        if (count($this->_data['friends_ids']) == 0) {
            return $online_friends;
        }
        /* make a list from viewer's friends */
        $friends_list = implode(',', $this->_data['friends_ids']);
        $get_online_friends = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users_online.last_seen FROM users_online INNER JOIN users ON users_online.user_id = users.user_id WHERE users_online.user_id IN (%s) AND users.user_chat_enabled = '1'", $friends_list)) or _error(SQL_ERROR_THROWEN);
        if ($get_online_friends->num_rows > 0) {
            while ($online_friend = $get_online_friends->fetch_assoc()) {
                $online_friend['user_picture'] = $this->get_picture($online_friend['user_picture'], $online_friend['user_gender']);
                $online_friend['user_is_online'] = '1';
                $online_friends[] = $online_friend;
            }
        }
        return $online_friends;
    }

    public static function update_online($user_id){
      global $db;
      /* check if the viewer is already online */
      $check = $db->query(sprintf("SELECT * FROM users_online WHERE user_id = %s LIMIT 1", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
      if ($check->num_rows == 0) {
        /* if no -> insert into online table */
        $db->query(sprintf("INSERT INTO users_online (user_id) VALUES (%s)", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
      } else {
        /* if yes -> update it's last seen time */
        $db->query(sprintf("UPDATE users_online SET last_seen = NOW() WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
      }
    }

    public static function delete_online($user_id){
      global $db;
      $db->query(sprintf("DELETE FROM users_online WHERE user_id = %s", secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }

    public static function clear_outdated_online(){
        global $db, $system, $date;
        /* remove any user not seen in last the $system['offline_time'] seconds */
        $get_not_seen = $db->query(sprintf("SELECT * FROM users_online WHERE (TO_SECONDS(NOW()) - TO_SECONDS(last_seen)) > %d", secure($system['offline_time'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        while($not_seen = $get_not_seen->fetch_assoc()) {
            /* update last login time */
            $db->query(sprintf("UPDATE users SET user_last_login = %s WHERE user_id = %s", secure($date), secure($not_seen['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* remove this user from online table */
            self::delete_online($not_seen['user_id']);
        }
    }

    public function get_offline_friends()
    {
        global $db, $system;
        /* get offline friends */
        $offline_friends = [];
        /* check if the viewer has friends */
        if (count($this->_data['friends_ids']) == 0) {
            return $offline_friends;
        }
        /* make a list from viewer's friends */
        $friends_list = implode(',', $this->_data['friends_ids']);
        $get_offline_friends = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_last_login FROM users LEFT JOIN users_online ON users.user_id = users_online.user_id WHERE users.user_chat_enabled = '1' AND users.user_id IN (%s) AND users_online.user_id IS NULL LIMIT %s", $friends_list, secure($system['max_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_offline_friends->num_rows > 0) {
            while ($offline_friend = $get_offline_friends->fetch_assoc()) {
                $offline_friend['user_picture'] = $this->get_picture($offline_friend['user_picture'], $offline_friend['user_gender']);
                $offline_friend['user_is_online'] = '0';
                $offline_friends[] = $offline_friend;
            }
        }
        return $offline_friends;
    }

    public function get_conversations_new()
    {
        global $db;
        $conversations = [];
        if (!empty($_SESSION['chat_boxes_opened'])) {
            /* make list from opened chat boxes keys (conversations ids) */
            $chat_boxes_opened_list = implode(',', $_SESSION['chat_boxes_opened']);
            $get_conversations = $db->query(sprintf("SELECT conversation_id FROM conversations_users WHERE user_id = %s AND seen = '0' AND deleted = '0' AND conversation_id NOT IN (%s)", secure($this->_data['user_id'], 'int'), $chat_boxes_opened_list)) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_conversations = $db->query(sprintf("SELECT conversation_id FROM conversations_users WHERE user_id = %s AND seen = '0' AND deleted = '0'", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }
        if ($get_conversations->num_rows > 0) {
            while ($conversation = $get_conversations->fetch_assoc()) {
                $db->query(sprintf("UPDATE conversations_users SET seen = '1' WHERE conversation_id = %s AND user_id = %s", secure($conversation['conversation_id'], 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $conversations[] = $this->get_conversation($conversation['conversation_id']);
            }
        }
        return $conversations;
    }

    public function get_conversations($offset = 0)
    {
        global $db, $system;
        $conversations = [];
        $offset *= $system['max_results'];
        $get_conversations = $db->query(sprintf("SELECT conversations.conversation_id FROM conversations INNER JOIN conversations_messages ON conversations.last_message_id = conversations_messages.message_id INNER JOIN conversations_users ON conversations.conversation_id = conversations_users.conversation_id WHERE conversations_users.deleted = '0' AND conversations_users.user_id = %s ORDER BY conversations_messages.time DESC LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_conversations->num_rows > 0) {
            while ($conversation = $get_conversations->fetch_assoc()) {
                $conversation = $this->get_conversation($conversation['conversation_id']);
                if ($conversation) {
                    $conversations[] = $conversation;
                }
            }
        }
        return $conversations;
    }

    public function get_conversation($conversation_id)
    {
        global $db;
        $conversation = [];
        $get_conversation = $db->query(sprintf("SELECT conversations.conversation_id, conversations.last_message_id, conversations_messages.message, conversations_messages.image, conversations_messages.time, conversations_users.seen FROM conversations INNER JOIN conversations_messages ON conversations.last_message_id = conversations_messages.message_id INNER JOIN conversations_users ON conversations.conversation_id = conversations_users.conversation_id WHERE conversations_users.deleted = '0' AND conversations_users.user_id = %s AND conversations.conversation_id = %s", secure($this->_data['user_id'], 'int'), secure($conversation_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_conversation->num_rows == 0) {
            return false;
        }
        $conversation = $get_conversation->fetch_assoc();
        /* get recipients */
        $get_recipients = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM conversations_users INNER JOIN users ON conversations_users.user_id = users.user_id WHERE conversations_users.conversation_id = %s AND conversations_users.user_id != %s", secure($conversation['conversation_id'], 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $recipients_num = $get_recipients->num_rows;
        if ($recipients_num == 0) {
            return false;
        }
        $i = 1;
        while ($recipient = $get_recipients->fetch_assoc()) {
            $recipient['user_picture'] = $this->get_picture($recipient['user_picture'], $recipient['user_gender']);
            $conversation['recipients'][] = $recipient;
            $conversation['name_list'] .= $recipient['user_firstname'];
            $conversation['ids'] .= $recipient['user_id'];
            if ($i < $recipients_num) {
                $conversation['name_list'] .= ", ";
                $conversation['ids'] .= "_";
            }
            $i++;
        }
        /* prepare conversation with multiple_recipients */
        if ($recipients_num > 1) {
            /* multiple recipients */
            $conversation['multiple_recipients'] = true;
            $conversation['picture_left'] = $conversation['recipients'][0]['user_picture'];
            $conversation['picture_right'] = $conversation['recipients'][1]['user_picture'];
            if ($recipients_num > 2) {
                $conversation['name'] = $conversation['recipients'][0]['user_firstname'] . ", " . $conversation['recipients'][1]['user_firstname'] . " & " . ($recipients_num - 2) . " " . __("more");
            } else {
                $conversation['name'] = $conversation['recipients'][0]['user_firstname'] . " & " . $conversation['recipients'][1]['user_firstname'];
            }
        } else {
            /* one recipient */
            $conversation['multiple_recipients'] = false;
            $conversation['picture'] = $conversation['recipients'][0]['user_picture'];
            $conversation['name'] = html_entity_decode($conversation['recipients'][0]['user_firstname']);
            $conversation['name_html'] = popover($conversation['recipients'][0]['user_id'], $conversation['recipients'][0]['user_name'], $conversation['recipients'][0]['user_firstname']);
        }
        /* get total number of messages */
        $get_messages = $db->query(sprintf("SELECT * FROM conversations_messages WHERE conversation_id = %s", secure($conversation_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        $conversation['total_messages'] = $get_messages->num_rows;
        $conversation['message_orginal'] = $this->decode_emoji($conversation['message']);
        $conversation['message'] = $this->_parse($conversation['message'], true, false);
        /* return */
        return $conversation;
    }

    public function get_mutual_conversation($recipients, $check_deleted = false)
    {
        global $db;
        $recipients_array = (array)$recipients;
        $recipients_array[] = $this->_data['user_id'];
        $recipients_list = implode(',', $recipients_array);
        $get_mutual_conversations = $db->query(sprintf('SELECT conversation_id FROM conversations_users WHERE user_id IN (%s) AND deleted = %s GROUP BY conversation_id HAVING COUNT(conversation_id) = %s', $recipients_list, $check_deleted, count($recipients_array))) or _error(SQL_ERROR_THROWEN);
        if ($get_mutual_conversations->num_rows == 0) {
            return false;
        }
        while ($mutual_conversation = $get_mutual_conversations->fetch_assoc()) {
            /* get recipients */
            $where_statement = ($check_deleted) ? " AND deleted != '1' " : "";
            $get_recipients = $db->query(sprintf("SELECT * FROM conversations_users WHERE conversation_id = %s" . $where_statement, secure($mutual_conversation['conversation_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_recipients->num_rows == count($recipients_array)) {
                return $mutual_conversation['conversation_id'];
            }
        }
    }

    public function get_conversation_messages($conversation_id, $offset = 0, $last_message_id = null)
    {
        global $db, $system;
        /* check if user authorized */
        $check_conversation = $db->query(sprintf("SELECT * FROM conversations_users WHERE conversations_users.conversation_id = %s AND conversations_users.user_id = %s", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($check_conversation->num_rows == 0) {
            throw new Exception(__("You are not authorized to view this"));
        }
        $offset *= $system['max_results'];
        $messages = [];
        if ($last_message_id !== null) {
            /* get all messages after the last_message_id */
            $get_messages = $db->query(sprintf("SELECT conversations_messages.message_id, conversations_messages.message, conversations_messages.image, conversations_messages.time, users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM conversations_messages INNER JOIN users ON conversations_messages.user_id = users.user_id WHERE conversations_messages.conversation_id = %s AND conversations_messages.message_id > %s", secure($conversation_id, 'int'), secure($last_message_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_messages = $db->query(sprintf("SELECT * FROM ( SELECT conversations_messages.message_id, conversations_messages.message, conversations_messages.image, conversations_messages.time, users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM conversations_messages INNER JOIN users ON conversations_messages.user_id = users.user_id WHERE conversations_messages.conversation_id = %s ORDER BY conversations_messages.message_id DESC LIMIT %s,%s ) messages ORDER BY messages.message_id ASC", secure($conversation_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        }
        while ($message = $get_messages->fetch_assoc()) {
            $message['user_picture'] = $this->get_picture($message['user_picture'], $message['user_gender']);
            $message['message'] = $this->_parse($message['message'], true, false);
            /* return */
            $messages[] = $message;
        }
        return $messages;
    }

    public function post_conversation_message($message, $image, $conversation_id = null, $recipients = null)
    {
        global $db, $date;
        /* check if posting the message to (new || existed) conversation */
        if ($conversation_id == null) {
            /* [1] post the message to -> a new conversation */
            /* [first] check previous conversation between (me & recipients) */
            $mutual_conversation = $this->get_mutual_conversation($recipients, true);
            if (!$mutual_conversation) {
                /* [1] there is no conversation between me and the recipients -> start new one */
                /* insert conversation */
                $db->query("INSERT INTO conversations (last_message_id) VALUES ('0')") or _error(SQL_ERROR_THROWEN);
                $conversation_id = $db->insert_id;
                /* insert the sender (me) */
                $db->query(sprintf("INSERT INTO conversations_users (conversation_id, user_id, seen) VALUES (%s, %s, '1')", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* insert recipients */
                foreach ($recipients as $recipient) {
                    $db->query(sprintf("INSERT INTO conversations_users (conversation_id, user_id) VALUES (%s, %s)", secure($conversation_id, 'int'), secure($recipient, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
            } else {
                /* [2] there is a conversation between me and the recipients */
                /* set the conversation_id */
                $conversation_id = $mutual_conversation;
            }
        } else {
            /* [2] post the message to -> existed conversation */
            /* check if user authorized */
            $check_conversation = $db->query(sprintf("SELECT * FROM conversations_users WHERE conversation_id = %s AND user_id = %s", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($check_conversation->num_rows == 0) {
                throw new Exception(__("You are not authorized to do this"));
            }
            /* update sender me as seen and not deleted if any */
            $db->query(sprintf("UPDATE conversations_users SET seen = '1', deleted = '0' WHERE conversation_id = %s AND user_id = %s", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* update recipients as not seen */
            $db->query(sprintf("UPDATE conversations_users SET seen = '0' WHERE conversation_id = %s AND user_id != %s", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }
        /* insert message */
        $image = ($image != '') ? $image : '';
        $db->query(sprintf("INSERT INTO conversations_messages (conversation_id, user_id, message, image, time) VALUES (%s, %s, %s, %s, %s)", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int'), secure($message), secure($image), secure($date))) or _error(SQL_ERROR_THROWEN);
        $message_id = $db->insert_id;
        /* update the conversation with last message id */
        $db->query(sprintf("UPDATE conversations SET last_message_id = %s WHERE conversation_id = %s", secure($message_id, 'int'), secure($conversation_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* update sender (me) with last message id */
        $db->query(sprintf("UPDATE users SET user_live_messages_lastid = %s WHERE user_id = %s", secure($message_id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        /* get conversation */
        $conversation = $this->get_conversation($conversation_id);
        /* update the only offline recipient messages counter & all with last message id */
        /* get current online users */
        $online_users = [];
        $get_online_users = $db->query("SELECT user_id FROM users_online") or _error(SQL_ERROR_THROWEN);
        if ($get_online_users->num_rows > 0) {
            while ($online_user = $get_online_users->fetch_assoc()) {
                $online_users[] = $online_user['user_id'];
            }
        }
        foreach ($conversation['recipients'] as $recipient) {
            if ($system['chat_enabled'] && in_array($recipient['user_id'], $online_users)) {
                $db->query(sprintf("UPDATE users SET user_live_messages_lastid = %s WHERE user_id = %s", secure($message_id, 'int'), secure($recipient['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            } else {
                $db->query(sprintf("UPDATE users SET user_live_messages_lastid = %s, user_live_messages_counter = user_live_messages_counter + 1 WHERE user_id = %s", secure($message_id, 'int'), secure($recipient['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            }
        }
        /* return with converstaion */
        return $conversation;
    }

    public function delete_conversation($conversation_id)
    {
        global $db;
        /* check if user authorized */
        $check_conversation = $db->query(sprintf("SELECT * FROM conversations_users WHERE conversation_id = %s AND user_id = %s", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($check_conversation->num_rows == 0) {
            throw new Exception(__("You are not authorized to do this"));
        }
        /* update converstaion as deleted */
        $db->query(sprintf("UPDATE conversations_users SET deleted = '1' WHERE conversation_id = %s AND user_id = %s", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function get_emojis()
    {
        global $db;
        $emojis = [];
        /*$get_emojis = $db->query("SELECT * FROM emojis") or _error(SQL_ERROR_THROWEN);
        if($get_emojis->num_rows > 0) {
            while($emoji = $get_emojis->fetch_assoc()) {
                $emojis[] = $emoji;
            }
        }*/
        return $emojis;
    }

    private static $emojiCache = null;

    public function decode_emoji($text)
    {
        global $db;
        if (self::$emojiCache === null) {
            self::$emojiCache = [];
            $get_emojis = $db->query("SELECT * FROM emojis") or _error(SQL_ERROR_THROWEN);
            if ($get_emojis->num_rows > 0) {
                while ($emoji = $get_emojis->fetch_assoc()) {
                    self::$emojiCache[] = $emoji;
                }
            }
        }


        foreach (self::$emojiCache as $emoji) {
            $replacement = '<i class="twa twa-xlg twa-' . $emoji['class'] . '"></i>';
            $pattern = preg_quote($emoji['pattern'], '/');
            $text = preg_replace('/(^|\s)' . $pattern . '/', $replacement, $text);
        }
        return $text;
    }

    public function get_stickers()
    {
        global $db;
        $stickers = [];
        /*$get_stickers = $db->query("SELECT * FROM stickers") or _error(SQL_ERROR_THROWEN);
        if($get_stickers->num_rows > 0) {
            while($sticker = $get_stickers->fetch_assoc()) {
                $stickers[] = $sticker;
            }
        }*/
        return $stickers;
    }

    private static $stickersCache = null;

    public function decode_stickers($text)
    {
        global $db, $system;
        if (self::$stickersCache === null) {
            self::$stickersCache = [];
            $get_stickers = $db->query("SELECT * FROM stickers") or _error(SQL_ERROR_THROWEN);
            if ($get_stickers->num_rows > 0) {
                while ($sticker = $get_stickers->fetch_assoc()) {
                    self::$stickersCache[] = $sticker;
                }
            }
        }

        foreach (self::$stickersCache as $sticker) {
            $replacement = '<img class="" src="' . $system['system_uploads'] . '/' . $sticker['image'] . '"></i>';
            $text = preg_replace('/(^|\s):STK-' . $sticker['sticker_id'] . ':/', $replacement, $text);
        }

        return $text;
    }

    public function get_mentions($matches)
    {
        global $db;
        $get_user = $db->query(sprintf("SELECT user_id, user_name, user_firstname, user_lastname FROM users WHERE user_name = %s", secure($matches[1]))) or _error(SQL_ERROR_THROWEN);
        if ($get_user->num_rows > 0) {
            $user = $get_user->fetch_assoc();
            $replacement = popover($user['user_id'], $user['user_name'], $user['user_firstname'] . " " . $user['user_lastname']);
        } else {
            $replacement = $matches[0];
        }
        return $replacement;
    }

    public function post_mentions($text, $node_url, $node_type = 'post', $notify_id = '', $excluded_ids = [])
    {
        global $db;
        $where_query = "";
        if ($excluded_ids) {
            $excluded_list = implode(',', $excluded_ids);
            $where_query = " user_id NOT IN ($excluded_list) AND ";
        }
        $done = [];
        if (preg_match_all('/\[([a-z0-9._]+)\]/', $text, $matches)) {
            foreach ($matches[1] as $username) {
                if ($this->_data['user_name'] != $username && !in_array($username, $done)) {
                    $get_user = $db->query(sprintf("SELECT user_id FROM users WHERE " . $where_query . " user_name = %s", secure($username))) or _error(SQL_ERROR_THROWEN);
                    if ($get_user->num_rows > 0) {
                        $_user = $get_user->fetch_assoc();
                        $this->post_notification(['to_user_id' => $_user['user_id'], 'action' => 'mention', 'node_type' => $node_type, 'node_url' => $node_url, 'notify_id' => $notify_id]);
                        $done[] = $username;
                    }
                }
            }
        }
    }

    public function popover($id, $type)
    {
        global $db;
        $profile = [];
        if ($type == "user") {
            $get_profile = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_profile->num_rows > 0) {
                $profile = $get_profile->fetch_assoc();
                $profile['user_picture'] = $this->get_picture($profile['user_picture'], $profile['user_gender']);
                $profile['followers_count'] = count($this->get_followers_ids($profile['user_id']));
                $profile['posts_count'] = count($this->_get_posts($profile['user_id']));
                $profile['followings_count'] = count($this->get_followings_ids($profile['user_id']));
                if ($this->_logged_in && $this->_data['user_id'] != $profile['user_id']) {
                    $profile['mutual_friends_count'] = $this->get_mutual_friends_count($profile['user_id']);
                }
                if ($profile['user_id'] != $this->_data['user_id']) {
                    $profile['we_friends'] = (in_array($profile['user_id'], $this->_data['friends_ids'])) ? true : false;
                    $profile['he_request'] = (in_array($profile['user_id'], $this->_data['friend_requests_ids'])) ? true : false;
                    $profile['i_request'] = (in_array($profile['user_id'], $this->_data['friend_requests_sent_ids'])) ? true : false;
                    $profile['i_follow'] = (in_array($profile['user_id'], $this->_data['followings_ids'])) ? true : false;
                }
            }
        } else {
            $get_profile = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_profile->num_rows > 0) {
                $profile = $get_profile->fetch_assoc();
                $profile['page_picture'] = User::get_picture($profile['page_picture'], "page");
                $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE page_id = %s AND user_id = %s", secure($id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $profile['i_like'] = true;
                } else {
                    $profile['i_like'] = false;
                }
            }
        }
        return $profile;
    }

    public function get_stories()
    {
        global $db, $system, $smarty;
        $stories = [];
        /* delete old stories */
        $db->query("DELETE FROM stories WHERE time<=DATE_SUB(NOW(), INTERVAL 1 DAY)") or _error(SQL_ERROR_THROWEN);
        /* get stories */
        $authors = $this->_data['friends_ids'];
        $authors[] = $this->_data['user_id'];
        $friends_list = implode(',', $authors);
        $get_stories = $db->query("SELECT stories.*, users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM stories INNER JOIN users ON stories.user_id = users.user_id WHERE stories.user_id IN ($friends_list) ORDER BY stories.story_id DESC LIMIT 5") or _error(SQL_ERROR_THROWEN);
        if ($get_stories->num_rows > 0) {
            while ($story = $get_stories->fetch_assoc()) {
                $story['user_picture'] = $this->get_picture($story['user_picture'], $story['user_gender']);
                /* get story media */
                $get_media = $db->query(sprintf("SELECT is_photo, source as src FROM stories_media WHERE story_id = %s", secure($story['story_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                while ($media = $get_media->fetch_assoc()) {
                    if ($media['is_photo']) {
                        $media['src'] = $system['system_uploads'] . '/' . $media['src'];
                        $media['type'] = 'image';
                    } else {
                        $smarty->assign('src', $media['src']);
                        $media['src'] = $smarty->fetch("_story.tpl");
                        $media['type'] = 'inline';
                    }
                    unset($media['is_photo']);
                    $media['title'] = htmlentities($story['text'], ENT_QUOTES, 'utf-8');
                    $story['media'][] = $media;
                }
                $story['media'] = json_encode($story['media']);
                $stories[] = $story;
            }
        }
        return $stories;
    }

    public function post_story($message, $photos)
    {
        global $db, $system, $date;
        /* insert the story */
        $db->query(sprintf("INSERT INTO stories (user_id, text, time) VALUES (%s, %s, %s)", secure($this->_data['user_id'], 'int'), secure($message), secure($date))) or _error(SQL_ERROR_THROWEN);
        $story_id = $db->insert_id;
        /* insert story photos */
        foreach ($photos as $photo) {
            $db->query(sprintf("INSERT INTO stories_media (story_id, source) VALUES (%s, %s)", secure($story_id, 'int'), secure($photo))) or _error(SQL_ERROR_THROWEN);
        }
    }

    public function publisher($args = [])
    {
        global $db, $system, $date;
        $post = [];
        /* default */
        $post['user_id'] = $this->_data['user_id'];
        $post['user_type'] = "user";
        $post['in_wall'] = 0;
        $post['wall_id'] = null;
        $post['in_group'] = 0;
        $post['group_id'] = null;
        $post['in_event'] = 0;
        $post['event_id'] = null;
        $post['author_id'] = $this->_data['user_id'];
        $post['post_author_picture'] = $this->_data['user_picture'];
        $post['post_author_url'] = $system['system_url'] . '/' . $this->_data['user_name'];
        $post['post_author_name'] = $this->_data['user_firstname'] . " " . $this->_data['user_lastname'];
        $post['post_author_verified'] = $this->_data['user_verified'];
        /* check the user_type */
        if ($args['handle'] == "me") {
            /* check if system allow wall posts */
            if (!$system['wall_posts_enabled']) {
                _error(400);
            }
            $post['user_name'] = $this->_data['user_name'];
            $post['user_firstname'] = $this->_data['user_firstname'];

        } elseif ($args['handle'] == "user") {
            /* check if system allow wall posts */
            if (!$system['wall_posts_enabled']) {
                _error(400);
            }
            /* check if the user is valid & the viewer can post on his wall */
            $check_user = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s", secure($args['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($check_user->num_rows == 0) {
                _error(400);
            }
            $_user = $check_user->fetch_assoc();
            if ($_user['user_privacy_wall'] == 'me' || ($_user['user_privacy_wall'] == 'friends' && !in_array($args['id'], $this->_data['friends_ids']))) {
                _error(400);
            }
            $post['in_wall'] = 1;
            $post['wall_id'] = $args['id'];
            $post['wall_username'] = $_user['user_name'];
            $post['wall_fullname'] = $_user['user_firstname'] . " " . $_user['user_lastname'];
        } elseif ($args['handle'] == "page") {
            /* check if the page is valid & the viewer is the admin */
            $check_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s AND page_admin = %s", secure($args['id'], 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($check_page->num_rows == 0) {
                _error(400);
            }
            $_page = $check_page->fetch_assoc();
            $post['user_id'] = $_page['page_id'];
            $post['user_type'] = "page";
            $post['post_author_picture'] = $this->get_picture($_page['page_picture'], "page");
            $post['post_author_url'] = $system['system_url'] . '/pages/' . $_page['page_name'];
            $post['post_author_name'] = $_page['page_title'];
            $post['post_author_verified'] = $this->_data['page_verified'];
        } elseif ($args['handle'] == "group") {
            /* check if the group is valid & the viewer is group member (approved) */
            $check_group = $db->query(sprintf("SELECT groups.* FROM groups INNER JOIN groups_members ON groups.group_id = groups_members.group_id WHERE groups_members.approved = '1' AND groups_members.user_id = %s AND groups_members.group_id = %s", secure($this->_data['user_id'], 'int'), secure($args['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            $_group = $check_group->fetch_assoc();
            $post['in_group'] = 1;
            $post['group_id'] = $args['id'];
        } elseif ($args['handle'] == "event") {
            /* check if the event is valid & the viewer is event member */
            $check_event = $db->query(sprintf("SELECT events.* FROM events INNER JOIN events_members ON events.event_id = events_members.event_id WHERE events_members.user_id = %s AND events_members.event_id = %s", secure($this->_data['user_id'], 'int'), secure($args['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            $_event = $check_event->fetch_assoc();
            $post['in_event'] = 1;
            $post['event_id'] = $args['id'];
        }
        /* prepare post data */
        $post['text'] = $args['message'];
        $post['time'] = $date;
        $post['location'] = (!is_empty($args['location']) && valid_location($args['location'])) ? $args['location'] : '';
        $post['privacy'] = $args['privacy'];
        $post['likes'] = 0;
        $post['comments'] = 0;
        $post['shares'] = 0;
        /* post feeling */
        $post['feeling_action'] = '';
        $post['feeling_value'] = '';
        $post['feeling_icon'] = '';
        if (!is_empty($args['feeling_action']) && !is_empty($args['feeling_value'])) {
            if ($args['feeling_action'] != "Feeling") {
                $_feeling_icon = get_feeling_icon($args['feeling_action'], get_feelings());
            } else {
                $_feeling_icon = get_feeling_icon($args['feeling_value'], get_feelings_types());
            }
            if ($_feeling_icon) {
                $post['feeling_action'] = $args['feeling_action'];
                $post['feeling_value'] = $args['feeling_value'];
                $post['feeling_icon'] = $_feeling_icon;
            }
        }
        /* prepare post type */
        if ($args['link']) {
            if ($args['link']->source_type == "link") {
                $post['post_type'] = 'link';
            } else {
                $post['post_type'] = 'media';
            }
        } elseif ($args['poll_options']) {
            $post['post_type'] = 'poll';
        } elseif ($args['product']) {
            $post['post_type'] = 'product';
            $post['privacy'] = "public";
        } elseif ($args['video']) {
            $post['post_type'] = 'video';
        } elseif ($args['audio']) {
            $post['post_type'] = 'audio';
        } elseif ($args['file']) {
            $post['post_type'] = 'file';
        } elseif (count($args['photos']) > 0) {
            if (!is_empty($args['album'])) {
                $post['post_type'] = 'album';
            } else {
                $post['post_type'] = 'photos';
            }
        } else {
            if ($post['location'] != '') {
                $post['post_type'] = 'map';
            } else {
                $post['post_type'] = '';
            }
        }
        /* insert the post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, in_wall, wall_id, in_group, group_id, in_event, event_id, post_type, time, location, privacy, text, feeling_action, feeling_value) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($post['user_id'], 'int'), secure($post['user_type']), secure($post['in_wall'], 'int'), secure($post['wall_id'], 'int'), secure($post['in_group']), secure($post['group_id'], 'int'), secure($post['in_event']), secure($post['event_id'], 'int'), secure($post['post_type']), secure($post['time']), secure($post['location']), secure($post['privacy']), secure($post['text']), secure($post['feeling_action']), secure($post['feeling_value']))) or _error(SQL_ERROR_THROWEN);
        $post['post_id'] = $db->insert_id;
        switch ($post['post_type']) {
            case 'link':
                $db->query(sprintf("INSERT INTO posts_links (post_id, source_url, source_host, source_title, source_text, source_thumbnail) VALUES (%s, %s, %s, %s, %s, %s)", secure($post['post_id'], 'int'), secure($args['link']->source_url), secure($args['link']->source_host), secure($args['link']->source_title), secure($args['link']->source_text), secure($args['link']->source_thumbnail))) or _error(SQL_ERROR_THROWEN);
                $post['link']['link_id'] = $db->insert_id;
                $post['link']['post_id'] = $post['post_id'];
                $post['link']['source_url'] = $args['link']->source_url;
                $post['link']['source_host'] = $args['link']->source_host;
                $post['link']['source_title'] = $args['link']->source_title;
                $post['link']['source_text'] = $args['link']->source_text;
                $post['link']['source_thumbnail'] = $args['link']->source_thumbnail;
                break;
            case 'poll':
                /* insert poll */
                $db->query(sprintf("INSERT INTO posts_polls (post_id) VALUES (%s)", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $post['poll']['poll_id'] = $db->insert_id;
                $post['poll']['post_id'] = $post['post_id'];
                $post['poll']['votes'] = '0';
                /* insert poll options */
                foreach ($args['poll_options'] as $option) {
                    $db->query(sprintf("INSERT INTO posts_polls_options (poll_id, text) VALUES (%s, %s)", secure($post['poll']['poll_id'], 'int'), secure($option))) or _error(SQL_ERROR_THROWEN);
                    $poll_option['option_id'] = $db->insert_id;
                    $poll_option['text'] = $option;
                    $poll_option['votes'] = 0;
                    $poll_option['checked'] = false;
                    $post['poll']['options'][] = $poll_option;
                }
                break;
            case 'product':
                $args['product']->location = (!is_empty($args['product']->location) && valid_location($args['product']->location)) ? $args['product']->location : '';
                $db->query(sprintf("INSERT INTO posts_products (post_id, name, price, category_id, location) VALUES (%s, %s, %s, %s, %s)", secure($post['post_id'], 'int'), secure($args['product']->name), secure($args['product']->price), secure($args['product']->category_id, 'int'), secure($args['product']->location))) or _error(SQL_ERROR_THROWEN);
                $post['product']['product_id'] = $db->insert_id;
                $post['product']['post_id'] = $post['post_id'];
                $post['product']['name'] = $args['product']->name;
                $post['product']['price'] = $args['product']->price;
                $post['product']['available'] = true;
                /* photos */
                if (count($args['photos']) > 0) {
                    foreach ($args['photos'] as $photo) {
                        $db->query(sprintf("INSERT INTO posts_photos (post_id, source) VALUES (%s, %s)", secure($post['post_id'], 'int'), secure($photo))) or _error(SQL_ERROR_THROWEN);
                        $post_photo['photo_id'] = $db->insert_id;
                        $post_photo['post_id'] = $post['post_id'];
                        $post_photo['source'] = $photo;
                        $post_photo['likes'] = 0;
                        $post_photo['comments'] = 0;
                        $post['photos'][] = $post_photo;
                    }
                    $post['photos_num'] = count($post['photos']);
                }
            case 'video':
                $db->query(sprintf("INSERT INTO posts_videos (post_id, source) VALUES (%s, %s)", secure($post['post_id'], 'int'), secure($args['video']->source))) or _error(SQL_ERROR_THROWEN);
                $post['video']['source'] = $args['video']->source;
                break;
            case 'audio':
                $db->query(sprintf("INSERT INTO posts_audios (post_id, source) VALUES (%s, %s)", secure($post['post_id'], 'int'), secure($args['audio']->source))) or _error(SQL_ERROR_THROWEN);
                $post['audio']['source'] = $args['audio']->source;
                break;
            case 'file':
                $db->query(sprintf("INSERT INTO posts_files (post_id, source) VALUES (%s, %s)", secure($post['post_id'], 'int'), secure($args['file']->source))) or _error(SQL_ERROR_THROWEN);
                $post['file']['source'] = $args['file']->source;
                break;
            case 'media':
                $db->query(sprintf("INSERT INTO posts_media (post_id, source_url, source_provider, source_type, source_title, source_text, source_html) VALUES (%s, %s, %s, %s, %s, %s, %s)", secure($post['post_id'], 'int'), secure($args['link']->source_url), secure($args['link']->source_provider), secure($args['link']->source_type), secure($args['link']->source_title), secure($args['link']->source_text), secure($args['link']->source_html))) or _error(SQL_ERROR_THROWEN);
                $post['media']['media_id'] = $db->insert_id;
                $post['media']['post_id'] = $post['post_id'];
                $post['media']['source_url'] = $args['link']->source_url;
                $post['media']['source_type'] = $args['link']->source_type;
                $post['media']['source_provider'] = $args['link']->source_provider;
                $post['media']['source_title'] = $args['link']->source_title;
                $post['media']['source_text'] = $args['link']->source_text;
                $post['media']['source_html'] = $args['link']->source_html;
                break;
            case 'photos':
                if ($args['handle'] == "page") {
                    /* check for page timeline album (public by default) */
                    if (!$_page['page_album_timeline']) {
                        /* create new page timeline album */
                        $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title) VALUES (%s, 'page', 'Timeline Photos')", secure($_page['page_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                        $_page['page_album_timeline'] = $db->insert_id;
                        /* update page */
                        $db->query(sprintf("UPDATE pages SET page_album_timeline = %s WHERE page_id = %s", secure($_page['page_album_timeline'], 'int'), secure($_page['page_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    }
                    $album_id = $_page['page_album_timeline'];
                } elseif ($args['handle'] == "group") {
                    /* check for group timeline album */
                    if (!$_group['group_album_timeline']) {
                        /* create new group timeline album */
                        $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, in_group, group_id, title, privacy) VALUES (%s, %s, %s, %s, 'Timeline Photos', 'custom')", secure($post['user_id'], 'int'), secure($post['user_type']), secure($post['in_group']), secure($post['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                        $_group['group_album_timeline'] = $db->insert_id;
                        /* update group */
                        $db->query(sprintf("UPDATE groups SET group_album_timeline = %s WHERE group_id = %s", secure($_group['group_album_timeline'], 'int'), secure($_group['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    }
                    $album_id = $_group['group_album_timeline'];
                } elseif ($args['handle'] == "event") {
                    /* check for event timeline album */
                    if (!$_event['event_album_timeline']) {
                        /* create new event timeline album */
                        $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, in_event, event_id, title, privacy) VALUES (%s, %s, %s, %s, 'Timeline Photos', 'custom')", secure($post['user_id'], 'int'), secure($post['user_type']), secure($post['in_event']), secure($post['event_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                        $_event['event_album_timeline'] = $db->insert_id;
                        /* update event */
                        $db->query(sprintf("UPDATE events SET event_album_timeline = %s WHERE event_id = %s", secure($_event['event_album_timeline'], 'int'), secure($_event['event_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    }
                    $album_id = $_event['event_album_timeline'];
                } else {
                    /* check for timeline album */
                    if (!$this->_data['user_album_timeline']) {
                        /* create new timeline album (public by default) */
                        $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title) VALUES (%s, 'user', 'Timeline Photos')", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                        $this->_data['user_album_timeline'] = $db->insert_id;
                        /* update user */
                        $db->query(sprintf("UPDATE users SET user_album_timeline = %s WHERE user_id = %s", secure($this->_data['user_album_timeline'], 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    }
                    $album_id = $this->_data['user_album_timeline'];
                }
                foreach ($args['photos'] as $photo) {
                    $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post['post_id'], 'int'), secure($album_id, 'int'), secure($photo))) or _error(SQL_ERROR_THROWEN);
                    $post_photo['photo_id'] = $db->insert_id;
                    $post_photo['post_id'] = $post['post_id'];
                    $post_photo['source'] = $photo;
                    $post_photo['likes'] = 0;
                    $post_photo['comments'] = 0;
                    $post['photos'][] = $post_photo;
                }
                $post['photos_num'] = count($post['photos']);
                break;
            case 'album':
                /* create new album */
                $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, in_group, group_id, in_event, event_id, title, privacy) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", secure($post['user_id'], 'int'), secure($post['user_type']), secure($post['in_group']), secure($post['group_id'], 'int'), secure($post['in_event']), secure($post['event_id'], 'int'), secure($args['album']), secure($post['privacy']))) or _error(SQL_ERROR_THROWEN);
                $album_id = $db->insert_id;
                foreach ($args['photos'] as $photo) {
                    $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post['post_id'], 'int'), secure($album_id, 'int'), secure($photo))) or _error(SQL_ERROR_THROWEN);
                    $post_photo['photo_id'] = $db->insert_id;
                    $post_photo['post_id'] = $post['post_id'];
                    $post_photo['source'] = $photo;
                    $post_photo['likes'] = 0;
                    $post_photo['comments'] = 0;
                    $post['photos'][] = $post_photo;
                }
                $post['album']['album_id'] = $album_id;
                $post['album']['title'] = $args['album'];
                $post['photos_num'] = count($post['photos']);
                /* get album path */
                if ($post['in_group']) {
                    $post['album']['path'] = 'groups/' . $_group['group_name'];
                } elseif ($post['in_event']) {
                    $post['album']['path'] = 'events/' . $_event['event_id'];
                } elseif ($post['user_type'] == "user") {
                    $post['album']['path'] = $this->_data['user_name'];
                } elseif ($post['user_type'] == "page") {
                    $post['album']['path'] = 'pages/' . $_page['page_name'];
                }
                break;
        }
        $this->post_mentions($args['message'], $post['post_id']);

        if ($post['in_wall']) {
            $this->post_notification(['to_user_id' => $post['wall_id'], 'action' => 'wall', 'node_type' => 'post', 'node_url' => $post['post_id']]);
        }

        $post['text_plain'] = htmlentities($post['text'], ENT_QUOTES, 'utf-8');
        $post['text'] = $this->_parse($post['text_plain']);
        $post['manage_post'] = true;

        return $post;
    }

    public function scraper($url)
    {
        $url_parsed = parse_url($url);
        if (!isset($url_parsed["scheme"])) {
            $url = "http://" . $url;
        }
        $url = ger_origenal_url($url);
        $advanced = true;
        if ($advanced) {
            require_once(ABSPATH . 'includes/libs/Embed/v3/autoloader.php');
            $dispatcher = new Embed\Http\CurlDispatcher([
                CURLOPT_FOLLOWLOCATION => false,
            ]);
            $embed = Embed\Embed::create($url, null, $dispatcher);
        } else {
            require_once(ABSPATH . 'includes/libs/Embed/v1/autoloader.php');
            $config = [
                'image' => [
                    'class' => 'Embed\\ImageInfo\\Sngine'
                ]
            ];
            $embed = Embed\Embed::create($url, $config);
        }
        if ($embed) {
            $return = [];
            $return['source_url'] = $url;
            $return['source_title'] = $embed->title;
            $return['source_text'] = $embed->description;
            $return['source_type'] = $embed->type;
            if ($return['source_type'] == "link") {
                $return['source_host'] = $url_parsed['host'];
                $return['source_thumbnail'] = $embed->image;
            } else {
                $return['source_html'] = $embed->code;
                $return['source_provider'] = $embed->providerName;
            }
            return $return;
        } else {
            return false;
        }
    }

    private function _parse($text, $nl2br = true, $mention = true)
    {
        $text = decode_urls($text);
        $text = $this->decode_emoji($text);
        $text = $this->decode_stickers($text);
        $text = decode_hashtag($text);

        if ($mention) {
            $text = decode_mention($text);
        }

        $text = censored_words($text);

        if ($nl2br) {
            $text = nl2br($text);
        }

        return $text;
    }

    public function get_posts($args = [])
    {
        global $db, $system;
        $posts = [];
        $get_query = json_decode(base64_decode($_GET['tags']));
        $get = !isset($args['get']) ? 'newsfeed' : $args['get'];
        $filter = !isset($args['filter']) ? 'all' : $args['filter'];
        $valid['filter'] = ['all', '', 'photos', 'video', 'audio', 'file', 'poll', 'product', 'article', 'map'];
        if (!in_array($filter, $valid['filter'])) {
            _error(400);
        }
        $last_post_id = !isset($args['last_post_id']) ? null : $args['last_post_id'];
        if (isset($args['last_post']) && !is_numeric($args['last_post'])) {
            _error(400);
        }
        $offset = !isset($args['offset']) ? 0 : $args['offset'];
        $offset *= $system['max_results'];
        if (isset($args['query'])) {
            if (is_empty($args['query'])) {
                return $posts;
            } else {
                $query = secure($args['query'], 'search', false);
            }
        }
        $order_query = "ORDER BY posts.time DESC";
        $where_query = "";
        /* get postsc */
        switch ($get) {
            case 'newsfeed':
                if (!$this->_logged_in && $query) {
                    $where_query .= "WHERE (";
                    $where_query .= "(posts.text LIKE $query)";
                    /* get only public posts [except: group posts & event posts & wall posts] */
                    $where_query .= " AND (posts.in_group = '0' AND posts.in_event = '0' AND posts.in_wall = '0' AND posts.privacy = 'public')";
                    $where_query .= ")";
                } else {
                    /* get viewer user's newsfeed */
                    $where_query .= "WHERE (";
                    /* get viewer posts */
                    $me = $this->_data['user_id'];
                    $where_query .= "(posts.user_id = $me AND posts.user_type = 'user')";
                    /* get posts from friends still followed */
                    $friends_ids = array_intersect($this->_data['friends_ids'], $this->_data['followings_ids']);
                    if ($friends_ids) {
                        $friends_list = implode(',', $friends_ids);
                        /* viewer friends posts -> authors */
                        $where_query .= " OR (posts.user_id IN ($friends_list) AND posts.user_type = 'user' AND posts.privacy = 'friends' AND posts.in_group = '0')";
                        /* viewer friends posts -> their wall posts */
                        $where_query .= " OR (posts.in_wall = '1' AND posts.wall_id IN ($friends_list) AND posts.user_type = 'user' AND posts.privacy = 'friends')";
                    }
                    /* get posts from followings */
                    if ($this->_data['followings_ids']) {
                        $followings_list = implode(',', $this->_data['followings_ids']);
                        /* viewer followings posts -> authors */
                        $where_query .= " OR (posts.user_id IN ($followings_list) AND posts.user_type = 'user' AND posts.privacy = 'public' AND posts.in_group = '0')";
                        /* viewer followings posts -> their wall posts */
                        $where_query .= " OR (posts.in_wall = '1' AND posts.wall_id IN ($followings_list) AND posts.user_type = 'user' AND posts.privacy = 'public')";
                    }
                    /* get pages posts */
                    $pages_ids = $this->get_pages_ids();
                    if ($pages_ids) {
                        $pages_list = implode(',', $pages_ids);
                        $where_query .= " OR (posts.user_id IN ($pages_list) AND posts.user_type = 'page')";
                    }
                    /* get groups (approved only) posts & exclude the viewer posts */
                    $groups_ids = $this->get_groups_ids(true);
                    if ($groups_ids) {
                        $groups_list = implode(',', $groups_ids);
                        $where_query .= " OR (posts.group_id IN ($groups_list) AND posts.in_group = '1' AND posts.user_id != $me)";
                    }
                    /* get events posts & exclude the viewer posts */
                    $events_ids = $this->get_events_ids();
                    if ($events_ids) {
                        $events_list = implode(',', $events_ids);
                        $where_query .= " OR (posts.event_id IN ($events_list) AND posts.in_event = '1' AND posts.user_id != $me)";
                    }
                    $where_query .= ")";
                    if ($query) {
                        $where_query .= " AND (posts.text LIKE $query)";
                    }
                }
                break;
            case 'newsfeed1':
                if ($this->_logged_in && $query) {
                    $where_query .= "WHERE (";
                    $where_query .= "(posts.text LIKE $query)";
                    /* get only public posts [except: group posts & event posts & wall posts] */
                    $where_query .= " AND (posts.in_group = '0' AND posts.in_event = '0' AND posts.in_wall = '0' AND posts.privacy = 'public')";
                    $where_query .= ")";
                } else {
                    /* get viewer user's newsfeed */
                    $where_query .= "WHERE (";
                    /* get viewer posts */
                    $me = $this->_data['user_id'];
                    $where_query .= "(posts.user_id = $me AND posts.user_type = 'user')";
                    /* get posts from friends still followed */
                    $friends_ids = array_intersect($this->_data['friends_ids'], $this->_data['followings_ids']);
                    if ($friends_ids) {
                        $friends_list = implode(',', $friends_ids);
                        /* viewer friends posts -> authors */
                        $where_query .= " OR (posts.user_id IN ($friends_list) AND posts.user_type = 'user' AND posts.privacy = 'friends' AND posts.in_group = '0')";
                        /* viewer friends posts -> their wall posts */
                        $where_query .= " OR (posts.in_wall = '1' AND posts.wall_id IN ($friends_list) AND posts.user_type = 'user' AND posts.privacy = 'friends')";
                    }
                    /* get posts from followings */
                    if ($this->_data['followings_ids']) {
                        $followings_list = implode(',', $this->_data['followings_ids']);
                        /* viewer followings posts -> authors */
                        $where_query .= " OR (posts.user_id IN ($followings_list) AND posts.user_type = 'user' AND posts.privacy = 'public' AND posts.in_group = '0')";
                        /* viewer followings posts -> their wall posts */
                        $where_query .= " OR (posts.in_wall = '1' AND posts.wall_id IN ($followings_list) AND posts.user_type = 'user' AND posts.privacy = 'public')";
                    }
                    /* get pages posts */
                    $pages_ids = $this->get_pages_ids();
                    if ($pages_ids) {
                        $pages_list = implode(',', $pages_ids);
                        $where_query .= " OR (posts.user_id IN ($pages_list) AND posts.user_type = 'page')";
                    }
                    /* get groups (approved only) posts & exclude the viewer posts */
                    $groups_ids = $this->get_groups_ids(true);
                    if ($groups_ids) {
                        $groups_list = implode(',', $groups_ids);
                        $where_query .= " OR (posts.group_id IN ($groups_list) AND posts.in_group = '1' AND posts.user_id != $me)";
                    }
                    /* get events posts & exclude the viewer posts */
                    $events_ids = $this->get_events_ids();
                    if ($events_ids) {
                        $events_list = implode(',', $events_ids);
                        $where_query .= " OR (posts.event_id IN ($events_list) AND posts.in_event = '1' AND posts.user_id != $me)";
                    }
                    $where_query .= ")";
                    if ($query) {
                        $where_query .= " AND (posts.text LIKE $query)";
                    }
                }
                break;
            case 'posts_profile':
                if (isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                /* get target user's posts */
                /* check if there is a viewer user */
                if ($this->_logged_in) {
                    /* check if the target user is the viewer */
                    if ($id == $this->_data['user_id']) {
                        /* get all posts */
                        $where_query .= "WHERE (";
                        /* get all target posts */
                        $where_query .= "(posts.user_id = $id AND posts.user_type = 'user')";
                        /* get taget wall posts */
                        $where_query .= " OR (posts.wall_id = $id AND posts.in_wall = '1')";
                        $where_query .= ")";
                    } else {
                        /* check if the viewer & the target user are friends */
                        if (in_array($id, $this->_data['friends_ids'])) {
                            $where_query .= "WHERE (";
                            /* get all target posts [except: group posts] */
                            $where_query .= "(posts.user_id = $id AND posts.user_type = 'user' AND posts.in_group = '0' AND posts.privacy != 'me' )";
                            /* get taget wall posts */
                            $where_query .= " OR (posts.wall_id = $id AND posts.in_wall = '1')";
                            $where_query .= ")";
                        } else {
                            /* get only public posts [except: wall posts & group posts] */
                            $where_query .= "WHERE (posts.user_id = $id AND posts.user_type = 'user' AND posts.in_group = '0' AND posts.in_wall = '0' AND posts.privacy = 'public')";
                        }
                    }
                } else {
                    /* get only public posts [except: wall posts & group posts] */
                    $where_query .= "WHERE (posts.user_id = $id AND posts.user_type = 'user' AND posts.in_group = '0' AND posts.in_wall = '0' AND posts.privacy = 'public')";
                }
                break;
            case 'posts_page':
                if (isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                $where_query .= "WHERE (posts.user_id = $id AND posts.user_type = 'page')";
                break;
            case 'posts_group':
                if (isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                $where_query .= "WHERE (posts.group_id = $id AND posts.in_group = '1')";
                break;
            case 'posts_event':
                if (isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                $where_query .= "WHERE (posts.event_id = $id AND posts.in_event = '1')";
                break;
            case 'saved':
                $id = $this->_data['user_id'];
                $where_query .= "INNER JOIN posts_saved ON posts.post_id = posts_saved.post_id WHERE (posts_saved.user_id = $id)";
                $order_query = "ORDER BY posts_saved.time DESC";
                break;
            default:
                _error(400);
                break;
        }
        /* get his hidden posts to exclude from newsfeed */
        $hidden_posts = $this->_get_hidden_posts($this->_data['user_id']);
        if (count($hidden_posts) > 0) {
            $hidden_posts_list = implode(',', $hidden_posts);
            $where_query .= " AND (posts.post_id NOT IN ($hidden_posts_list))";
        }
        /* filter posts */
        if ($filter != "all") {
            $where_query .= " AND (posts.post_type = '$filter')";
        }
        /* get posts */
        if ($get_query != '') {
            $me = $this->_data['user_id'];
            if (count($hidden_posts) > 0) {
                $resultQery = sprintf("SELECT * FROM posts WHERE ( posts.text LIKE %s AND YEARWEEK(`time`,1) >= YEARWEEK(CURDATE(), 1) AND posts.user_type = 'user' AND posts.post_type != 'live' AND (posts.post_id NOT IN ($hidden_posts_list))) LIMIT %s", secure($get_query, 'search'), secure('10', 'int', false));
                $get_posts = $db->query($resultQery) or _error(SQL_ERROR_THROWEN);
            } else {
                $resultQery = sprintf("SELECT * FROM posts WHERE ( posts.text LIKE %s AND YEARWEEK(`time`,1) >= YEARWEEK(CURDATE(), 1) AND posts.user_type = 'user' AND posts.post_type != 'live' ) LIMIT %s", secure($get_query, 'search'), secure('10', 'int', false));
                $get_posts = $db->query($resultQery) or _error(SQL_ERROR_THROWEN);
            }
        } else {
            if ($last_post_id != null && $get != 'saved') {
                $resultQery = sprintf("SELECT * FROM (SELECT posts.post_id,posts.post_type FROM posts " . $where_query . ") posts WHERE posts.post_id > %s AND posts.post_type != 'live' ORDER BY posts.time DESC", secure($last_post_id, 'int'));
               // $get_posts = $db->query($resultQery) or _error(SQL_ERROR_THROWEN);
            } else {

                //home
                $resultQery = sprintf("SELECT posts.post_id FROM posts " . $where_query . " AND posts.post_type != 'live' " . $order_query . " LIMIT %s, %s", secure($offset, 'int', false), secure($system['max_results'], 'int', false));
                $get_posts = $db->query($resultQery) or _error(SQL_ERROR_THROWEN);
            }
        }

        if ($get_posts->num_rows > 0) {
            while ($post = $get_posts->fetch_assoc()) {
                $post = $this->get_post($post['post_id'], true, true); /* $full_details = true, $pass_privacy_check = true */
                if ($post) {
                    $posts[] = $post;
                }
            }
        }
        return $posts;
    }

    public function get_postsOptimized($args = [])
    {
        //die;
        global $db, $system;
        /* initialize vars */
        $posts = [];
        /* validate arguments */
        $get_query = json_decode(base64_decode($_GET['tags']));
        $get = !isset($args['get']) ? 'newsfeed' : $args['get'];
        $filter = !isset($args['filter']) ? 'all' : $args['filter'];
        $valid['filter'] = ['all', '', 'photos', 'video', 'audio', 'file', 'poll', 'product', 'article', 'map'];
        if (!in_array($filter, $valid['filter'])) {
            _error(400);
        }
        $last_post_id = !isset($args['last_post_id']) ? null : $args['last_post_id'];
        if (isset($args['last_post']) && !is_numeric($args['last_post'])) {
            _error(400);
        }
        $offset = !isset($args['offset']) ? 0 : $args['offset'];
        $offset *= $system['max_results'];
        if (isset($args['query'])) {
            if (is_empty($args['query'])) {
                return $posts;
            } else {
                $query = secure($args['query'], 'search', false);
            }
        }
        $order_query = "ORDER BY posts.time DESC";
        $where_query = "";
        /* get postsc */
        switch ($get) {
            case 'newsfeed':
                if (!$this->_logged_in && $query) {
                    $where_query .= "WHERE (";
                    $where_query .= "(posts.text LIKE $query)";
                    /* get only public posts [except: group posts & event posts & wall posts] */
                    $where_query .= " AND (posts.in_group = '0' AND posts.in_event = '0' AND posts.in_wall = '0' AND posts.privacy = 'public')";
                    $where_query .= ")";
                } else {
                    /* get viewer user's newsfeed */
                    $where_query .= "WHERE (";
                    /* get viewer posts */
                    $me = $this->_data['user_id'];
                    $where_query .= "(posts.user_id = $me AND posts.user_type = 'user')";
                    /* get posts from friends still followed */
                    $friends_ids = array_intersect($this->_data['friends_ids'], $this->_data['followings_ids']);
                    if ($friends_ids) {
                        $friends_list = implode(',', $friends_ids);
                        /* viewer friends posts -> authors */
                        $where_query .= " OR (posts.user_id IN ($friends_list) AND posts.user_type = 'user' AND posts.privacy = 'friends' AND posts.in_group = '0')";
                        /* viewer friends posts -> their wall posts */
                        $where_query .= " OR (posts.in_wall = '1' AND posts.wall_id IN ($friends_list) AND posts.user_type = 'user' AND posts.privacy = 'friends')";
                    }
                    /* get posts from followings */
                    if ($this->_data['followings_ids']) {
                        $followings_list = implode(',', $this->_data['followings_ids']);
                        /* viewer followings posts -> authors */
                        $where_query .= " OR (posts.user_id IN ($followings_list) AND posts.user_type = 'user' AND posts.privacy = 'public' AND posts.in_group = '0')";
                        /* viewer followings posts -> their wall posts */
                        $where_query .= " OR (posts.in_wall = '1' AND posts.wall_id IN ($followings_list) AND posts.user_type = 'user' AND posts.privacy = 'public')";
                    }
                    /* get pages posts */
                    $pages_ids = $this->get_pages_ids();
                    if ($pages_ids) {
                        $pages_list = implode(',', $pages_ids);
                        $where_query .= " OR (posts.user_id IN ($pages_list) AND posts.user_type = 'page')";
                    }
                    /* get groups (approved only) posts & exclude the viewer posts */
                    $groups_ids = $this->get_groups_ids(true);
                    if ($groups_ids) {
                        $groups_list = implode(',', $groups_ids);
                        $where_query .= " OR (posts.group_id IN ($groups_list) AND posts.in_group = '1' AND posts.user_id != $me)";
                    }
                    /* get events posts & exclude the viewer posts */
                    $events_ids = $this->get_events_ids();
                    if ($events_ids) {
                        $events_list = implode(',', $events_ids);
                        $where_query .= " OR (posts.event_id IN ($events_list) AND posts.in_event = '1' AND posts.user_id != $me)";
                    }
                    $where_query .= ")";
                    if ($query) {
                        $where_query .= " AND (posts.text LIKE $query)";
                    }
                }
                break;
            case 'newsfeed1':
                if ($this->_logged_in && $query) {
                    $where_query .= "WHERE (";
                    $where_query .= "(posts.text LIKE $query)";
                    /* get only public posts [except: group posts & event posts & wall posts] */
                    $where_query .= " AND (posts.in_group = '0' AND posts.in_event = '0' AND posts.in_wall = '0' AND posts.privacy = 'public')";
                    $where_query .= ")";
                } else {
                    /* get viewer user's newsfeed */
                    $where_query .= "WHERE (";
                    /* get viewer posts */
                    $me = $this->_data['user_id'];
                    $where_query .= "(posts.user_id = $me AND posts.user_type = 'user')";
                    /* get posts from friends still followed */
                    $friends_ids = array_intersect($this->_data['friends_ids'], $this->_data['followings_ids']);
                    if ($friends_ids) {
                        $friends_list = implode(',', $friends_ids);
                        /* viewer friends posts -> authors */
                        $where_query .= " OR (posts.user_id IN ($friends_list) AND posts.user_type = 'user' AND posts.privacy = 'friends' AND posts.in_group = '0')";
                        /* viewer friends posts -> their wall posts */
                        $where_query .= " OR (posts.in_wall = '1' AND posts.wall_id IN ($friends_list) AND posts.user_type = 'user' AND posts.privacy = 'friends')";
                    }
                    /* get posts from followings */
                    if ($this->_data['followings_ids']) {
                        $followings_list = implode(',', $this->_data['followings_ids']);
                        /* viewer followings posts -> authors */
                        $where_query .= " OR (posts.user_id IN ($followings_list) AND posts.user_type = 'user' AND posts.privacy = 'public' AND posts.in_group = '0')";
                        /* viewer followings posts -> their wall posts */
                        $where_query .= " OR (posts.in_wall = '1' AND posts.wall_id IN ($followings_list) AND posts.user_type = 'user' AND posts.privacy = 'public')";
                    }
                    /* get pages posts */
                    $pages_ids = $this->get_pages_ids();
                    if ($pages_ids) {
                        $pages_list = implode(',', $pages_ids);
                        $where_query .= " OR (posts.user_id IN ($pages_list) AND posts.user_type = 'page')";
                    }
                    /* get groups (approved only) posts & exclude the viewer posts */
                    $groups_ids = $this->get_groups_ids(true);
                    if ($groups_ids) {
                        $groups_list = implode(',', $groups_ids);
                        $where_query .= " OR (posts.group_id IN ($groups_list) AND posts.in_group = '1' AND posts.user_id != $me)";
                    }
                    /* get events posts & exclude the viewer posts */
                    $events_ids = $this->get_events_ids();
                    if ($events_ids) {
                        $events_list = implode(',', $events_ids);
                        $where_query .= " OR (posts.event_id IN ($events_list) AND posts.in_event = '1' AND posts.user_id != $me)";
                    }
                    $where_query .= ")";
                    if ($query) {
                        $where_query .= " AND (posts.text LIKE $query)";
                    }
                }
                break;
            case 'posts_profile':
                if (isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                /* get target user's posts */
                /* check if there is a viewer user */
                if ($this->_logged_in) {
                    /* check if the target user is the viewer */
                    if ($id == $this->_data['user_id']) {
                        /* get all posts */
                        $where_query .= "WHERE (";
                        /* get all target posts */
                        $where_query .= "(posts.user_id = $id AND posts.user_type = 'user')";
                        /* get taget wall posts */
                        $where_query .= " OR (posts.wall_id = $id AND posts.in_wall = '1')";
                        $where_query .= ")";
                    } else {
                        /* check if the viewer & the target user are friends */
                        if (in_array($id, $this->_data['friends_ids'])) {
                            $where_query .= "WHERE (";
                            /* get all target posts [except: group posts] */
                            $where_query .= "(posts.user_id = $id AND posts.user_type = 'user' AND posts.in_group = '0' AND posts.privacy != 'me' )";
                            /* get taget wall posts */
                            $where_query .= " OR (posts.wall_id = $id AND posts.in_wall = '1')";
                            $where_query .= ")";
                        } else {
                            /* get only public posts [except: wall posts & group posts] */
                            $where_query .= "WHERE (posts.user_id = $id AND posts.user_type = 'user' AND posts.in_group = '0' AND posts.in_wall = '0' AND posts.privacy = 'public')";
                        }
                    }
                } else {
                    /* get only public posts [except: wall posts & group posts] */
                    $where_query .= "WHERE (posts.user_id = $id AND posts.user_type = 'user' AND posts.in_group = '0' AND posts.in_wall = '0' AND posts.privacy = 'public')";
                }
                break;
            case 'posts_page':
                if (isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                $where_query .= "WHERE (posts.user_id = $id AND posts.user_type = 'page')";
                break;
            case 'posts_group':
                if (isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                $where_query .= "WHERE (posts.group_id = $id AND posts.in_group = '1')";
                break;
            case 'posts_event':
                if (isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                $where_query .= "WHERE (posts.event_id = $id AND posts.in_event = '1')";
                break;
            case 'saved':
                $id = $this->_data['user_id'];
                $where_query .= "INNER JOIN posts_saved ON posts.post_id = posts_saved.post_id WHERE (posts_saved.user_id = $id)";
                $order_query = "ORDER BY posts_saved.time DESC";
                break;
            default:
                _error(400);
                break;
        }
        /* get his hidden posts to exclude from newsfeed */
        $hidden_posts = $this->_get_hidden_posts($this->_data['user_id']);
        if (count($hidden_posts) > 0) {
            $hidden_posts_list = implode(',', $hidden_posts);
            $where_query .= " AND (posts.post_id NOT IN ($hidden_posts_list))";
        }
        /* filter posts */
        if ($filter != "all") {
            $where_query .= " AND (posts.post_type = '$filter')";
        }

        /* get posts */
        if ($get_query != '') {
            $me = $this->_data['user_id'];
            if (count($hidden_posts) > 0) {
                $resultQery = sprintf("SELECT * FROM posts WHERE ( posts.text LIKE %s AND YEARWEEK(`time`,1) >= YEARWEEK(CURDATE(), 1) AND posts.user_type = 'user' AND posts.post_type != 'live' AND (posts.post_id NOT IN ($hidden_posts_list))) LIMIT %s", secure($get_query, 'search'), secure('10', 'int', false));
                $get_posts = $db->query($resultQery) or _error(SQL_ERROR_THROWEN);
            } else {
                $resultQery = sprintf("SELECT * FROM posts WHERE ( posts.text LIKE %s AND YEARWEEK(`time`,1) >= YEARWEEK(CURDATE(), 1) AND posts.user_type = 'user' AND posts.post_type != 'live' ) LIMIT %s", secure($get_query, 'search'), secure('10', 'int', false));
                $get_posts = $db->query($resultQery) or _error(SQL_ERROR_THROWEN);
            }
        } else {
            if ($last_post_id != null && $get != 'saved') {
                $resultQery = sprintf("SELECT * FROM (SELECT posts.post_id,posts.post_type FROM posts " . $where_query . ") posts WHERE posts.post_id > %s AND posts.post_type != 'live' ORDER BY posts.time DESC", secure($last_post_id, 'int'));
                $get_posts = $db->query($resultQery) or _error(SQL_ERROR_THROWEN);
            } else {

                //home
                $resultQery = sprintf("SELECT posts.post_id FROM posts " . $where_query . " AND posts.post_type != 'live' " . $order_query . " LIMIT %s, %s", secure($offset, 'int', false), secure($system['max_results'], 'int', false));
                $get_posts = $db->query($resultQery) or _error(SQL_ERROR_THROWEN);
            }
        }
        //echo $resultQery;
        $postIds = [];
        if ($get_posts->num_rows > 0) {
            while ($post = $get_posts->fetch_assoc()) {
                $postIds[] = $post['post_id'];
            }
        }
        return $postIds;
    }

    public function get_likes_posts($user_id, $offset = 0)
    {
        global $db;
        /* initialize vars */
        $posts = array();
        /* validate arguments */
        $get_username = $_REQUEST['username'];
        /* get posts */
        if ($offset) {
            $offset *= 6;
            $limit = $offset . "," . 6;
        } else {
            $limit = 6;
        }
        $get_posts = $db->query(sprintf("SELECT post_id FROM  posts_likes WHERE user_id = %s ORDER BY posts_likes.post_id DESC LIMIT $limit", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_posts->num_rows > 0) {
            while ($post = $get_posts->fetch_assoc()) {
                $post_ids[] = $post['post_id'];
            }
        }
        if (count($post_ids) > 0) {
            foreach ($post_ids as $post_id) {
                $post = $this->get_post($post_id, true, true); /* $full_details = true, $pass_privacy_check = true */
                if ($post) {
                    $posts[] = $post;

					//Formatted Time EST
					if($posts['time'] != ''){
						$guo_datetime = $posts['time'];
						$posts['time'] = $this->twitter_type_datetime($guo_datetime);
						$posts['formatted_time'] = $this->new_formatted_time($guo_datetime);
					}
					//Formatted Time EST End
                }
            }
            return $posts;
        }
    }

    public function get_likes_postsIds($user_id, $offset = 0)
    {
        global $db;
        /* initialize vars */
        $post_ids = [];
        /* validate arguments */
        $get_username = $_REQUEST['username'];
        /* get posts */
        if ($offset) {
            $offset *= 6;
            $limit = $offset . "," . 6;
        } else {
            $limit = 6;
        }
        $get_posts = $db->query(sprintf("SELECT post_id FROM  posts_likes WHERE user_id = %s ORDER BY posts_likes.post_id DESC LIMIT $limit", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_posts->num_rows > 0) {
            while ($post = $get_posts->fetch_assoc()) {
                $post_ids[] = $post['post_id'];
            }
        }

        return $post_ids;
    }

    public function get_broadcasts_posts($user_id)
    {
        global $db;
        /* initialize vars */
        $posts = [];
        /* validate arguments */
        $get_username = $_REQUEST['username'];
        /* get posts */
        $get_posts = $db->query(sprintf("SELECT post_id FROM  posts WHERE is_broadcast='1' AND post_type='video' AND user_id = %s ORDER BY posts.time DESC", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_posts->num_rows > 0) {
            while ($post = $get_posts->fetch_assoc()) {
                $post_ids[] = $post['post_id'];
            }
        }
        if (count($post_ids) > 0) {
            foreach ($post_ids as $post_id) {
                $post = $this->get_post($post_id, true, true); /* $full_details = true, $pass_privacy_check = true */
                if ($post) {
                    $posts[] = $post;
                }
            }
            return $posts;
        }
        return [];

    }

    public function get_media_postsIds($user_id, $offset = 0)
    {
        global $db;

        $post_ids = [];

        if ($offset) {
            $offset *= 6;
            $limit = $offset . "," . 6;
        } else {
            $limit = 6;
        }
        $get_posts = $db->query(sprintf("SELECT post_id FROM posts WHERE (post_type = 'photos' OR post_type = 'profile_cover') AND (user_id = %s) LIMIT $limit", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_posts->num_rows > 0) {
            while ($post = $get_posts->fetch_assoc()) {
                $post_ids[] = $post['post_id'];
            }
        }

        return $post_ids;
    }

    public function get_media_posts($user_id, $offset = 0)
    {

        $post_ids = $this->get_media_postsIds($user_id, $offset);
        if (count($post_ids) > 0) {
            foreach ($post_ids as $post_id) {
                $post = $this->get_post($post_id, true, true); /* $full_details = true, $pass_privacy_check = true */
                if ($post) {
                    $posts[] = $post;
                }
            }
            return $posts;
        }
        return [];
    }

    public function get_video_posts($user_id, $offset = 0)
    {
        global $db;
        /* initialize vars */
        $posts = [];
        /* validate arguments */
        if ($offset) {
            $offset *= 6;
            $limit = $offset . "," . 6;
        } else {
            $limit = 6;
        }
        /* get posts */
        $get_posts = $db->query(sprintf("SELECT post_id FROM posts WHERE ( post_type = 'media' AND user_id = %s) LIMIT $limit", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_posts->num_rows > 0) {
            while ($post = $get_posts->fetch_assoc()) {
                $post_ids[] = $post['post_id'];
            }
        }
        if (count($post_ids) > 0) {
            foreach ($post_ids as $post_id) {
                $post = $this->get_post($post_id, true, true); /* $full_details = true, $pass_privacy_check = true */
                if ($post) {
                    $posts[] = $post;
                }
            }
            return $posts;
        }
    }

    public function get_post_view_count($post_id)
    {
        global $db;
        $get_rows = $db->query("SELECT `views` FROM `broadcasts` WHERE `post_id` =" . $post_id) or _error(SQL_ERROR_THROWEN);
        $count = $get_rows->num_rows;
        $views = 0;
        if ($count == 0) {
            return $views;
        }
        while ($post_get = $get_rows->fetch_assoc()) {
            $views = $post_get['views'];
        }
        return $views;
    }

    public function get_post($post_id, $full_details = true, $pass_privacy_check = true)
    {
        global $db;
        $post = $this->_check_post($post_id, $pass_privacy_check);
        if (!$post) {
            return false;
        }
        /* post type */

        $postTypes = [
            'album',
            'photos',
            'profile_picture',
            'profile_cover',
            'page_picture',
            'page_cover',
            'group_picture',
            'group_cover',
            'event_cover',
        ];

        if (in_array($post['post_type'],$postTypes)) {
            /* get photos */
            $get_photos = $db->query(sprintf("SELECT * FROM posts_photos WHERE post_id = %s ORDER BY photo_id ASC", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            $post['photos_num'] = $get_photos->num_rows;
            /* check if photos has been deleted */
            if ($post['photos_num'] == 0) {
                return false;
            }
            while ($post_photo = $get_photos->fetch_assoc()) {
                $post['photos'][] = $post_photo;
            }
            if ($post['post_type'] == 'album') {
                $post['album'] = $this->get_album($post['photos'][0]['album_id'], false);
                if (!$post['album']) {
                    return false;
                }
            }
        } elseif ($post['post_type'] == 'media') {
            /* get media */
            $get_media = $db->query(sprintf("SELECT * FROM posts_media WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if media has been deleted */
            if ($get_media->num_rows == 0) {
                return false;
            }
            $post['media'] = $get_media->fetch_assoc();
        } elseif ($post['post_type'] == 'link') {
            /* get link */
            $get_link = $db->query(sprintf("SELECT * FROM posts_links WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if link has been deleted */
            if ($get_link->num_rows == 0) {
                return false;
            }
            $post['link'] = $get_link->fetch_assoc();
        } elseif ($post['post_type'] == 'poll') {
            /* get poll */
            $get_poll = $db->query(sprintf("SELECT * FROM posts_polls WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if video has been deleted */
            if ($get_poll->num_rows == 0) {
                return false;
            }
            $post['poll'] = $get_poll->fetch_assoc();
            /* get poll options */
            $get_poll_options = $db->query(sprintf("SELECT option_id, text FROM posts_polls_options WHERE poll_id = %s", secure($post['poll']['poll_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_poll_options->num_rows == 0) {
                return false;
            }
            while ($poll_option = $get_poll_options->fetch_assoc()) {
                /* get option votes */
                $get_option_votes = $db->query(sprintf("SELECT * FROM users_polls_options WHERE option_id = %s", secure($poll_option['option_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $poll_option['votes'] = $get_option_votes->num_rows;
                /* check if viewer voted */
                $poll_option['checked'] = false;
                if ($this->_logged_in) {
                    $check = $db->query(sprintf("SELECT * FROM users_polls_options WHERE user_id = %s AND option_id = %s", secure($this->_data['user_id'], 'int'), secure($poll_option['option_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    if ($check->num_rows > 0) {
                        $poll_option['checked'] = true;
                    }
                }
                $post['poll']['options'][] = $poll_option;
            }
        } elseif ($post['post_type'] == 'product') {
            /* get product */
            $get_product = $db->query(sprintf("SELECT * FROM posts_products WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if link has been deleted */
            if ($get_product->num_rows == 0) {
                return false;
            }
            $post['product'] = $get_product->fetch_assoc();
            /* get photos */
            $get_photos = $db->query(sprintf("SELECT * FROM posts_photos WHERE post_id = %s ORDER BY photo_id DESC", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            $post['photos_num'] = $get_photos->num_rows;
            /* check if photos has been deleted */
            if ($post['photos_num'] > 0) {
                while ($post_photo = $get_photos->fetch_assoc()) {
                    $post['photos'][] = $post_photo;
                }
            }
        } elseif ($post['post_type'] == 'article') {
            /* get article */
            $get_article = $db->query(sprintf("SELECT * FROM posts_articles WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if article has been deleted */
            if ($get_article->num_rows == 0) {
                return false;
            }
            $post['article'] = $get_article->fetch_assoc();
            $post['article']['parsed_cover'] = $this->get_picture($post['article']['cover'], 'article');
            $post['article']['title_url'] = get_url_text($post['article']['title']);
            $post['article']['parsed_text'] = htmlspecialchars_decode($post['article']['text'], ENT_QUOTES);
            $post['article']['text_snippet'] = get_snippet_text($post['article']['text']);
            $tags = (!is_empty($post['article']['tags'])) ? explode(',', $post['article']['tags']) : [];
            $post['article']['parsed_tags'] = array_map('get_tag', $tags);
        } elseif ($post['post_type'] == 'video') {
            /* get video */
            $get_video = $db->query(sprintf("SELECT * FROM posts_videos WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if video has been deleted */
            if ($get_video->num_rows == 0) {
                return false;
            }
            $post['video'] = $get_video->fetch_assoc();
        } elseif ($post['post_type'] == 'audio') {
            /* get audio */
            $get_audio = $db->query(sprintf("SELECT * FROM posts_audios WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if audio has been deleted */
            if ($get_audio->num_rows == 0) {
                return false;
            }
            $post['audio'] = $get_audio->fetch_assoc();
        } elseif ($post['post_type'] == 'file') {
            /* get file */
            $get_file = $db->query(sprintf("SELECT * FROM posts_files WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if file has been deleted */
            if ($get_file->num_rows == 0) {
                return false;
            }
            $post['file'] = $get_file->fetch_assoc();
        } elseif ($post['post_type'] == 'shared') {
            /* get origin post */
            $post['origin'] = $this->get_post($post['origin_id'], false);
            /* check if origin post has been deleted */
            if (!$post['origin']) {
                return false;
            }
        }
        /* post feeling */
        if (!is_empty($post['feeling_action']) && !is_empty($post['feeling_value'])) {
            if ($post['feeling_action'] != "Feeling") {
                $_feeling_icon = get_feeling_icon($post['feeling_action'], get_feelings());
            } else {
                $_feeling_icon = get_feeling_icon($post['feeling_value'], get_feelings_types());
            }
            if ($_feeling_icon) {
                $post['feeling_icon'] = $_feeling_icon;
            }
        }
        /* parse text */
        $post['text_plain'] = $post['text'];
        $post['text'] = $this->_parse($post['text_plain']);
        /* check if get full post details */
        if ($full_details) {
            /* get post comments */
            if ($post['comments'] > 0) {
                $post['post_comments'] = $this->get_comments($post['post_id'], 0, true, true, $post);
            }
        }

		//Formatted Time EST
		if($post['time'] != ''){
			$guo_datetime = $post['time'];
			$post['time'] = $this->twitter_type_datetime($guo_datetime);
			$post['formatted_time'] = $this->new_formatted_time($guo_datetime);

		}
        return $post;
    }

    public function get_post_article($post_id, $full_details = true, $pass_privacy_check = false)
    {
        global $db;
        $post = $this->_check_post($post_id, $pass_privacy_check);
        if (!$post) {
            return false;
        }
        $segments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));
        $numSegments = count($segments);
        $currentSegment = $segments[$numSegments - 1];
        /* get article */
        $get_article = $db->query(sprintf("SELECT * FROM posts_articles WHERE post_id = %s AND text LIKE %s", secure($post['post_id'], 'int'), secure($currentSegment, 'search'))) or _error(SQL_ERROR_THROWEN);
        /* check if article has been deleted */
        if ($get_article->num_rows == 0) {
            return false;
        }
        $post['article'] = $get_article->fetch_assoc();
        $post['article']['parsed_cover'] = $this->get_picture($post['article']['cover'], 'article');
        $post['article']['title_url'] = get_url_text($post['article']['title']);
        $post['article']['parsed_text'] = htmlspecialchars_decode($post['article']['text'], ENT_QUOTES);
        $post['article']['text_snippet'] = get_snippet_text($post['article']['text']);
        $tags = (!is_empty($post['article']['tags'])) ? explode(',', $post['article']['tags']) : [];
        $post['article']['parsed_tags'] = array_map('get_tag', $tags);
        /* parse text */
        $post['text_plain'] = $post['text'];
        $post['text'] = $this->_parse($post['text_plain']);
        /* check if get full post details */
        if ($full_details) {
            /* get post comments */
            if ($post['comments'] > 0) {
                $post['post_comments'] = $this->get_comments($post['post_id'], 0, true, true, $post);
            }
        }
        return $post;
    }

    public function get_boosted_post()
    {
        global $db, $system;
        $get_random_post = $db->query("SELECT post_id FROM posts WHERE boosted = '1' ORDER BY RAND() LIMIT 1") or _error(SQL_ERROR_THROWEN);
        if ($get_random_post->num_rows == 0) {
            return false;
        }
        $random_post = $get_random_post->fetch_assoc();
        return $this->get_post($random_post['post_id'], true, true);
    }

    public function who_likes($args = [])
    {
        global $db, $system;
        /* initialize arguments */
        $post_id = !isset($args['post_id']) ? null : $args['post_id'];
        $photo_id = !isset($args['photo_id']) ? null : $args['photo_id'];
        $comment_id = !isset($args['comment_id']) ? null : $args['comment_id'];
        $offset = !isset($args['offset']) ? 0 : $args['offset'];
        /* initialize vars */
        $users = [];
        $offset *= $system['max_results'];
        if ($post_id != null) {
            /* get users who like the post */
            $get_users = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM posts_likes INNER JOIN users ON (posts_likes.user_id = users.user_id) WHERE posts_likes.post_id = %s LIMIT %s, %s', secure($post_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        } elseif ($photo_id != null) {
            /* get users who like the photo */
            $get_users = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM posts_photos_likes INNER JOIN users ON (posts_photos_likes.user_id = users.user_id) WHERE posts_photos_likes.photo_id = %s LIMIT %s, %s', secure($photo_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        } else {
            /* get users who like the comment */
            $get_users = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM posts_comments_likes INNER JOIN users ON (posts_comments_likes.user_id = users.user_id) WHERE posts_comments_likes.comment_id = %s LIMIT %s, %s', secure($comment_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        }
        if ($get_users->num_rows > 0) {
            while ($_user = $get_users->fetch_assoc()) {
                $_user['user_picture'] = $this->get_picture($_user['user_picture'], $_user['user_gender']);
                /* get the connection between the viewer & the target */
                $_user['connection'] = $this->connection($_user['user_id']);
                /* get mutual friends count */
                $_user['mutual_friends_count'] = $this->get_mutual_friends_count($_user['user_id']);
                $users[] = $_user;
            }
        }
        return $users;
    }

    public function who_shares($post_id, $offset = 0)
    {
        global $db, $system;
        $posts = [];
        $offset *= $system['max_results'];
        $get_posts = $db->query(sprintf('SELECT posts.post_id FROM posts INNER JOIN users ON posts.user_id = users.user_id WHERE posts.post_type = "shared" AND posts.origin_id = %s LIMIT %s, %s', secure($post_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_posts->num_rows > 0) {
            while ($post = $get_posts->fetch_assoc()) {
                $post = $this->get_post($post['post_id']);
                if ($post) {
                    $posts[] = $post;
                }
            }
        }
        return $posts;
    }

    public function who_votes($option_id, $offset = 0)
    {
        global $db, $system;
        $voters = [];
        $offset *= $system['max_results'];
        $get_voters = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_picture, users.user_gender FROM users_polls_options INNER JOIN users ON users_polls_options.user_id = users.user_id WHERE option_id = %s LIMIT %s, %s", secure($option_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        while ($voter = $get_voters->fetch_assoc()) {
            $voter['user_picture'] = $this->get_picture($voter['user_picture'], $voter['user_gender']);
            /* get the connection between the viewer & the target */
            $voter['connection'] = $this->connection($voter['user_id']);
            $voters[] = $voter;
        }
        return $voters;
    }

    private function _get_hidden_posts($user_id)
    {
        global $db;
        $hidden_posts = [];
        $get_hidden_posts = $db->query(sprintf("SELECT post_id FROM posts_hidden WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_hidden_posts->num_rows > 0) {
            while ($hidden_post = $get_hidden_posts->fetch_assoc()) {
                $hidden_posts[] = $hidden_post['post_id'];
            }
        }
        return $hidden_posts;
    }

    public function get_mute($user_id)
    {
        global $db;
        $hidden_posts = [];
        $get_hidden_posts = $db->query(sprintf("SELECT post_id FROM posts_hidden WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_hidden_posts->num_rows > 0) {
            while ($hidden_post = $get_hidden_posts->fetch_assoc()) {
                $hidden_posts[] = $hidden_post['post_id'];
            }
        }
        return $hidden_posts;
    }

    private static $getPostsCache = [];

    public function _get_posts($user_id)
    {
        global $db;

        if (!array_key_exists($user_id,self::$getPostsCache)) {
            self::$getPostsCache[$user_id] = [];
            $get_posts = $db->query(sprintf("SELECT post_id FROM  posts WHERE is_broadcast = '0' AND user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_posts->num_rows > 0) {
                while ($post = $get_posts->fetch_assoc()) {
                    self::$getPostsCache[$user_id][] = $post['post_id'];
                }
            }
        }
        return self::$getPostsCache[$user_id];
    }

    private static $getBroadcastCountCache = [];

    public function _get_broadcast_count($user_id)
    {
        global $db;
        if (!array_key_exists($user_id,self::$getBroadcastCountCache )) {
            self::$getBroadcastCountCache[$user_id] = [];
            $get_posts = $db->query(sprintf("SELECT post_id FROM  posts WHERE is_broadcast='1' AND post_type='video' AND user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_posts->num_rows > 0) {
                while ($post = $get_posts->fetch_assoc()) {
                    self::$getBroadcastCountCache[$user_id][] = $post['post_id'];
                }
            }
        }
        return self::$getBroadcastCountCache[$user_id];
    }

    private function _check_post($id, $pass_privacy_check = false, $full_details = true)
    {
        global $db, $system;
        /* get post */
        $get_post = $db->query(sprintf("SELECT posts.*, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_picture_id, users.user_cover_id, users.user_verified, users.user_subscribed, users.user_pinned_post FROM posts LEFT JOIN users ON posts.user_id = users.user_id AND posts.user_type = 'user' WHERE NOT (users.user_name <=> NULL) AND posts.post_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_post->num_rows == 0) {
            return false;
        }
        $post = $get_post->fetch_assoc();
        /* check if the page has been deleted */
        if ($post['user_type'] == "page" && !$post['page_admin']) {
            return false;
        }
        /* get the author */
        $post['author_id'] = ($post['user_type'] == "page") ? $post['page_admin'] : $post['user_id'];
        $post['is_page_admin'] = ($this->_logged_in && $this->_data['user_id'] == $post['page_admin']) ? true : false;
        $post['is_group_admin'] = ($this->_logged_in && $this->_data['user_id'] == $post['group_admin']) ? true : false;
        $post['is_event_admin'] = ($this->_logged_in && $this->_data['user_id'] == $post['event_admin']) ? true : false;
        /* check the post author type */
        if ($post['user_type'] == "user") {
            /* user */
            $post['post_author_picture'] = $this->get_picture($post['user_picture'], $post['user_gender']);
            $post['post_author_url'] = $system['system_url'] . '/' . $post['user_name'];
            $post['post_author_name'] = $post['user_firstname'] . " " . $post['user_lastname'];
            $post['post_author_verified'] = $post['user_verified'];
            $post['pinned'] = ((!$post['in_group'] && !$post['in_event'] && $post['post_id'] == $post['user_pinned_post']) || ($post['in_group'] && $post['post_id'] == $post['group_pinned_post']) || ($post['in_event'] && $post['post_id'] == $post['event_pinned_post'])) ? true : false;
            $post['post_user_name'] = $post['user_name'];
        } else {
            /* page */
            $post['post_author_picture'] = $this->get_picture($post['page_picture'], "page");
            $post['post_author_url'] = $system['system_url'] . '/pages/' . $post['page_name'];
            $post['post_author_name'] = $post['page_title'];
            $post['post_author_verified'] = $post['page_verified'];
            $post['pinned'] = ($post['post_id'] == $post['page_pinned_post']) ? true : false;
        }
        /* check if viewer can manage post [Edit|Pin|Delete] */
        $post['manage_post'] = false;
        if ($this->_logged_in) {
            /* viewer is (admins|moderators)] */
            if ($this->_data['user_group'] < 3) {
                $post['manage_post'] = true;
            }
            /* viewer is the author of post || page admin */
            if ($this->_data['user_id'] == $post['author_id']) {
                $post['manage_post'] = true;
            }
            /* viewer is the admin of the group of the post */
            if ($post['in_group'] && $post['is_group_admin']) {
                $post['manage_post'] = true;
            }
            /* viewer is the admin of the event of the post */
            if ($post['in_event'] && $post['is_event_admin']) {
                $post['manage_post'] = true;
            }
        }
        /* full details */
        if ($full_details) {
            /* check if wall post */
            if ($post['in_wall']) {
                $get_wall_user = $db->query(sprintf("SELECT user_firstname, user_lastname, user_name FROM users WHERE user_id = %s", secure($post['wall_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_wall_user->num_rows == 0) {
                    return false;
                }
                $wall_user = $get_wall_user->fetch_assoc();
                $post['wall_username'] = $wall_user['user_name'];
                $post['wall_fullname'] = $wall_user['user_firstname'] . " " . $wall_user['user_lastname'];
            }
            /* check if viewer [liked|saved] this post */
            $post['i_save'] = false;
            $post['i_like'] = false;
            if ($this->_logged_in) {
                /* save */
                $check_save = $db->query(sprintf("SELECT * FROM posts_saved WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($check_save->num_rows > 0) {
                    $post['i_save'] = true;
                }
                /* like */
                $check_like = $db->query(sprintf("SELECT * FROM posts_likes WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($check_like->num_rows > 0) {
                    $post['i_like'] = true;
                }
            }
        }
        /* check privacy */
        /* if post in group & (the group is public || the viewer approved member of this group) => pass privacy check */
        if ($post['in_group'] && ($post['group_privacy'] == 'public' || $this->check_group_membership($this->_data['user_id'], $post['group_id']) == 'approved')) {
            $pass_privacy_check = true;
        }
        /* if post in event & (the event is public || the viewer member of this event) => pass privacy check */
        if ($post['in_event'] && ($post['event_privacy'] == 'public' || $this->check_event_membership($this->_data['user_id'], $post['event_id']))) {
            $pass_privacy_check = true;
        }

		/* Get New Total Number Of Comments (No Repeated). Code Edited By Abhishek */
            $get_comments = $db->query(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'post' AND posts_comments.node_id = %s AND posts_comments.text != '' group by posts_comments.text ORDER BY posts_comments.comment_id DESC) comments group by comments.text ORDER BY comments.comment_id DESC", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);

			$post['comments'] = $get_comments->num_rows;

			/* End Get New Total Number Of Comments (No Repeated). Code Edited By Abhishek */

        if ($pass_privacy_check || $this->_check_privacy($post['privacy'], $post['author_id'])) {
            return $post;
        }
        return false;
    }

    private function _check_privacy($privacy, $author_id)
    {
        if ($privacy == 'public') {
            return true;
        }
        if ($this->_logged_in) {
            /* check if the viewer is the system admin */
            if ($this->_data['user_group'] < 3) {
                return true;
            }
            /* check if the viewer is the target */
            if ($author_id == $this->_data['user_id']) {
                return true;
            }
            /* check if the viewer and the target are friends */
            if ($privacy == 'friends' && in_array($author_id, $this->_data['friends_ids'])) {
                return true;
            }
        }
        return false;
    }

    public function get_comments($node_id, $offset = 0, $is_post = true, $pass_privacy_check = true, $post = [])
    {
        global $db, $system;
        $comments = [];
        $offset *= $system['min_results'];
        /* get comments */
        if ($is_post) {
            /* get post comments */
            if (!$pass_privacy_check) {
                /* (check|get) post */
                $post = $this->_check_post($node_id, false);
                if (!$post) {
                    return false;
                }
            }
            /* get post comments */
            $get_comments = $db->query(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'post' AND posts_comments.node_id = %s AND posts_comments.text != '' group by posts_comments.text ORDER BY posts_comments.comment_id DESC LIMIT %s, %s) comments group by comments.text ORDER BY comments.comment_id DESC", secure($node_id, 'int'), secure($offset, 'int', false), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        } else {
            /* get photo comments */
            /* check privacy */
            if (!$pass_privacy_check) {
                /* (check|get) photo */
                $photo = $this->get_photo($node_id);
                if (!$photo) {
                    _error(403);
                }
                $post = $photo['post'];
            }
            /* get photo comments */
            $get_comments = $db->query(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'photo' AND posts_comments.node_id = %s AND posts_comments.text != '' group by posts_comments.text ORDER BY posts_comments.comment_id DESC LIMIT %s, %s) comments group by comments.text ORDER BY comments.comment_id DESC", secure($node_id, 'int'), secure($offset, 'int', false), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        }
        if ($get_comments->num_rows == 0) {
            return $comments;
        }
		$count = 0;
        while ($comment = $get_comments->fetch_assoc()) {
            /* get replies */
            if ($comment['replies'] > 0) {
                $comment['comment_replies'] = $this->get_replies($comment['comment_id']);
            }
            /* parse text */
            $comment['text_plain'] = $comment['text'];
            $comment['text'] = $this->_parse($comment['text']);
            /* get the comment author */
            if ($comment['user_type'] == "user") {
                /* user type */
                $comment['author_id'] = $comment['user_id'];
                $comment['author_picture'] = $this->get_picture($comment['user_picture'], $comment['user_gender']);
                $comment['author_url'] = $system['system_url'] . '/' . $comment['user_name'];
                $comment['author_name'] = $comment['user_firstname'] . " " . $comment['user_lastname'];
                $comment['author_verified'] = $comment['user_verified'];
            } else {
                /* page type */
                $comment['author_id'] = $comment['page_admin'];
                $comment['author_picture'] = $this->get_picture($comment['page_picture'], "page");
                $comment['author_url'] = $system['system_url'] . '/pages/' . $comment['page_name'];
                $comment['author_name'] = $comment['page_title'];
                $comment['author_verified'] = $comment['page_verified'];
            }
            /* check if viewer user likes this comment */
            if ($this->_logged_in && $comment['likes'] > 0) {
                $check_like = $db->query(sprintf("SELECT * FROM posts_comments_likes WHERE user_id = %s AND comment_id = %s", secure($this->_data['user_id'], 'int'), secure($comment['comment_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $comment['i_like'] = ($check_like->num_rows > 0) ? true : false;
            }
            /* check if viewer can manage comment [Edit|Delete] */
            $comment['edit_comment'] = false;
            $comment['delete_comment'] = false;
            if ($this->_logged_in) {
                /* viewer is (admins|moderators)] */
                if ($this->_data['user_group'] < 3) {
                    $comment['edit_comment'] = true;
                    $comment['delete_comment'] = true;
                }
                /* viewer is the author of comment */
                if ($this->_data['user_id'] == $comment['author_id']) {
                    $comment['edit_comment'] = true;
                    $comment['delete_comment'] = true;
                }
                /* viewer is the author of post || page admin */
                if ($this->_data['user_id'] == $post['author_id']) {
                    $comment['delete_comment'] = true;
                }
                /* viewer is the admin of the group of the post */
                if ($post['in_group'] && $post['is_group_admin']) {
                    $comment['delete_comment'] = true;
                }
                /* viewer is the admin of the event of the post */
                if ($post['in_group'] && $post['is_event_admin']) {
                    $comment['delete_comment'] = true;
                }
            }
            $comments[] = $comment;

			//Formatted Time EST
			if($comments[$count]['time'] != ''){
                $guo_datetime = $comments[$count]['time'];
				$comments[$count]['time'] = $this->twitter_type_datetime($guo_datetime);
				$comments[$count]['formatted_time'] = $this->new_formatted_time($guo_datetime);
			}
			$count++;
			//Formatted Time EST End
        }
        return $comments;
    }

    public function get_replies($comment_id, $offset = 0, $pass_check = true)
    {
        global $db, $system;
        $replies = [];
        $offset *= $system['min_results'];
        if (!$pass_check) {
            $comment = $this->get_comment($comment_id);
            if (!$comment) {
                _error(403);
            }
        }
        /* get replies */
        $get_replies = $db->query(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'comment' AND posts_comments.node_id = %s AND posts_comments.text != '' group by posts_comments.text ORDER BY posts_comments.comment_id DESC LIMIT %s, %s) comments group by comments.text ORDER BY comments.comment_id DESC", secure($comment_id, 'int'), secure($offset, 'int', false), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_replies->num_rows == 0) {
            return $replies;
        }
        while ($reply = $get_replies->fetch_assoc()) {
            /* parse text */
            $reply['text_plain'] = $reply['text'];
            $reply['text'] = $this->_parse($reply['text']);
            /* get the reply author */
            if ($reply['user_type'] == "user") {
                /* user type */
                $reply['author_id'] = $reply['user_id'];
                $reply['author_picture'] = $this->get_picture($reply['user_picture'], $reply['user_gender']);
                $reply['author_url'] = $system['system_url'] . '/' . $reply['user_name'];
                $reply['author_user_name'] = $reply['user_name'];
                $reply['author_name'] = $reply['user_firstname'] . " " . $reply['user_lastname'];
                $reply['author_verified'] = $reply['user_verified'];
            } else {
                /* page type */
                $reply['author_id'] = $reply['page_admin'];
                $reply['author_picture'] = $this->get_picture($reply['page_picture'], "page");
                $reply['author_url'] = $system['system_url'] . '/pages/' . $reply['page_name'];
                $reply['author_name'] = $reply['page_title'];
                $reply['author_verified'] = $reply['page_verified'];
            }
            /* check if viewer user likes this reply */
            if ($this->_logged_in && $reply['likes'] > 0) {
                $check_like = $db->query(sprintf("SELECT * FROM posts_comments_likes WHERE user_id = %s AND comment_id = %s", secure($this->_data['user_id'], 'int'), secure($reply['comment_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $reply['i_like'] = ($check_like->num_rows > 0) ? true : false;
            }
            /* check if viewer can manage comment [Edit|Delete] */
            $reply['edit_comment'] = false;
            $reply['delete_comment'] = false;
            if ($this->_logged_in) {
                /* viewer is (admins|moderators)] */
                if ($this->_data['user_group'] < 3) {
                    $reply['edit_comment'] = true;
                    $reply['delete_comment'] = true;
                }
                /* viewer is the author of comment */
                if ($this->_data['user_id'] == $reply['author_id']) {
                    $reply['edit_comment'] = true;
                    $reply['delete_comment'] = true;
                }
            }
            $replies[] = $reply;
        }
        return $replies;
    }

    public function comment($handle, $node_id, $message, $photo)
    {
        global $db, $system, $date;
        $comment = [];
        /* default */
        $comment['node_id'] = $node_id;
        $comment['node_type'] = $handle;
        $comment['text'] = $message;
        $comment['image'] = $photo;
        $comment['time'] = $date;
        $comment['likes'] = 0;
        $comment['replies'] = 0;
        /* check the handle */
        switch ($handle) {
            case 'post':
                /* (check|get) post */
                $post = $this->_check_post($node_id, false, false);
                if (!$post) {
                    _error(403);
                }
                break;
            case 'photo':
                /* (check|get) photo */
                $photo = $this->get_photo($node_id);
                if (!$photo) {
                    _error(403);
                }
                $post = $photo['post'];
                break;
            case 'comment':
                /* (check|get) comment */
                $parent_comment = $this->get_comment($node_id, false);
                if (!$parent_comment) {
                    _error(403);
                }
                $post = $parent_comment['post'];
                break;
        }
        /* check if there is any blocking between the viewer & the target */
        if ($this->blocked($post['author_id']) || ($handle == "comment" && $this->blocked($parent_comment['author_id']))) {
            _error(403);
        }
        /* check if the viewer is page admin of the target post */
        if (!$post['is_page_admin']) {
            $comment['user_id'] = $this->_data['user_id'];
            $comment['user_type'] = "user";
            $comment['author_picture'] = $this->_data['user_picture'];
            $comment['author_url'] = $system['system_url'] . '/' . $this->_data['user_name'];
            $comment['author_user_name'] = $this->_data['user_name'];
            $comment['author_name'] = $this->_data['user_firstname'] . " " . $this->_data['user_lastname'];
            $comment['author_verified'] = $this->_data['user_verified'];
        } else {
            $comment['user_id'] = $post['page_id'];
            $comment['user_type'] = "page";
            $comment['author_picture'] = $this->get_picture($post['page_picture'], "page");
            $comment['author_url'] = $system['system_url'] . '/pages/' . $post['page_name'];
            $comment['author_name'] = $post['page_title'];
            $comment['author_verified'] = $post['page_verified'];
        }
        /* insert the comment */
        $db->query(sprintf("INSERT INTO posts_comments (node_id, node_type, user_id, user_type, text, image, time) VALUES (%s, %s, %s, %s, %s, %s, %s)", secure($comment['node_id'], 'int'), secure($comment['node_type']), secure($comment['user_id'], 'int'), secure($comment['user_type']), secure($comment['text']), secure($comment['image']), secure($comment['time']))) or _error(SQL_ERROR_THROWEN);
        //echo "INSERT INTO posts_comments ( node_id, node_type, user_id, user_type, text, image, time) VALUES (".$comment['node_id'].", ".$comment['node_type'].", ".$comment['user_id'].", ".$comment['user_type".'].", ".$comment['text'].", ".$comment['image'].", ".$comment['time'].")"; die;
        $comment['comment_id'] = $db->insert_id;
        /* update (post|photo|comment) (comments|replies) counter */
        switch ($handle) {
            case 'post':
                $db->query(sprintf("UPDATE posts SET comments = comments + 1 WHERE post_id = %s", secure($node_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'photo':
                $db->query(sprintf("UPDATE posts_photos SET comments = comments + 1 WHERE photo_id = %s", secure($node_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'comment':
                $db->query(sprintf("UPDATE posts_comments SET replies = replies + 1 WHERE comment_id = %s", secure($node_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                $db->query(sprintf("UPDATE posts SET comments = comments + 1 WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
        }
        /* post notification */
        if ($handle == "comment") {
            $this->post_notification(['to_user_id' => $parent_comment['author_id'], 'action' => 'reply', 'node_type' => $parent_comment['node_type'], 'node_url' => $parent_comment['node_id'], 'notify_id' => 'comment_' . $comment['comment_id']]);
            if ($post['author_id'] != $parent_comment['author_id']) {
                $this->post_notification(['to_user_id' => $post['author_id'], 'action' => 'comment', 'node_type' => $parent_comment['node_type'], 'node_url' => $parent_comment['node_id'], 'notify_id' => 'comment_' . $comment['comment_id']]);
            }
        } else {
            $this->post_notification(['to_user_id' => $post['author_id'], 'action' => 'comment', 'node_type' => $handle, 'node_url' => $node_id, 'notify_id' => 'comment_' . $comment['comment_id']]);
        }
        /* post mention notifications if any */
        if ($handle == "comment") {
            $this->post_mentions($comment['text'], $parent_comment['node_id'], "reply_" . $parent_comment['node_type'], 'comment_' . $comment['comment_id'], [$post['author_id'], $parent_comment['author_id']]);
        } else {
            $this->post_mentions($comment['text'], $node_id, "comment_" . $handle, 'comment_' . $comment['comment_id'], [$post['author_id']]);
        }
        /* parse text */
        $comment['text_plain'] = htmlentities($comment['text'], ENT_QUOTES, 'utf-8');
        $comment['text'] = $this->_parse($comment['text_plain']);
        /* check if viewer can manage comment [Edit|Delete] */
        $comment['edit_comment'] = true;
        $comment['delete_comment'] = true;

		//Formatted Time EST
		if($comment['time'] != ''){
			$guo_datetime = $comment['time'];
			$comment['time'] = $this->twitter_type_datetime($guo_datetime);
			$comment['formatted_time'] = $this->new_formatted_time($guo_datetime);
		}
		//Formatted Time EST End

        /* return */
        return $comment;
    }

    public function get_comment($comment_id, $recursive = true)
    {
        global $db;
        /* get comment */
        $get_comment = $db->query(sprintf("SELECT posts_comments.*, pages.page_admin FROM posts_comments LEFT JOIN pages ON posts_comments.user_type = 'page' AND posts_comments.user_id = pages.page_id WHERE posts_comments.comment_id = %s", secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_comment->num_rows == 0) {
            return false;
        }
        $comment = $get_comment->fetch_assoc();
        /* check if the page has been deleted */
        if ($comment['user_type'] == "page" && !$comment['page_admin']) {
            return false;
        }
        /* get the author */
        $comment['author_id'] = ($comment['user_type'] == "page") ? $comment['page_admin'] : $comment['user_id'];
        /* get post */
        switch ($comment['node_type']) {
            case 'post':
                $post = $this->_check_post($comment['node_id'], false, false);
                /* check if the post has been deleted */
                if (!$post) {
                    return false;
                }
                $comment['post'] = $post;
                break;
            case 'photo':
                /* (check|get) photo */
                $photo = $this->get_photo($comment['node_id']);
                if (!$photo) {
                    return false;
                }
                $comment['post'] = $photo['post'];
                break;
            case 'comment':
                if (!$recursive) {
                    return false;
                }
                /* (check|get) comment */
                $parent_comment = $this->get_comment($comment['node_id'], false);
                if (!$parent_comment) {
                    return false;
                }
                $comment['parent_comment'] = $parent_comment;
                $comment['post'] = $parent_comment['post'];
                break;
        }
        /* check if viewer user likes this comment */
        if ($this->_logged_in && $comment['likes'] > 0) {
            $check_like = $db->query(sprintf("SELECT * FROM posts_comments_likes WHERE user_id = %s AND comment_id = %s", secure($this->_data['user_id'], 'int'), secure($comment['comment_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            $comment['i_like'] = ($check_like->num_rows > 0) ? true : false;
        }
        return $comment;
    }

    public function delete_comment($comment_id)
    {
        global $db;
        /* (check|get) comment */
        $comment = $this->get_comment($comment_id);
        if (!$comment) {
            _error(403);
        }
        /* check if viewer can manage comment [Delete] */
        $comment['delete_comment'] = false;
        if ($this->_logged_in) {
            /* viewer is (admins|moderators)] */
            if ($this->_data['user_group'] < 3) {
                $comment['delete_comment'] = true;
            }
            /* viewer is the author of comment */
            if ($this->_data['user_id'] == $comment['author_id']) {
                $comment['delete_comment'] = true;
            }
            /* viewer is the author of post || page admin */
            if ($this->_data['user_id'] == $comment['post']['author_id']) {
                $comment['delete_comment'] = true;
            }
            /* viewer is the admin of the group of the post */
            if ($comment['post']['in_group'] && $comment['post']['is_group_admin']) {
                $comment['delete_comment'] = true;
            }
            /* viewer is the admin of the event of the post */
            if ($comment['post']['in_event'] && $comment['post']['is_event_admin']) {
                $comment['delete_comment'] = true;
            }
        }
        /* delete the comment */
        if ($comment['delete_comment']) {
            /* delete the comment */
            $db->query(sprintf("DELETE FROM posts_comments WHERE comment_id = %s", secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* update comments counter */
            switch ($comment['node_type']) {
                case 'post':
                    $db->query(sprintf("UPDATE posts SET comments = IF(comments=0,0,comments-1) WHERE post_id = %s", secure($comment['node_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    break;
                case 'photo':
                    $db->query(sprintf("UPDATE posts_photos SET comments = IF(comments=0,0,comments-1) WHERE photo_id = %s", secure($comment['node_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    break;
                case 'comment':
                    $db->query(sprintf("UPDATE posts_comments SET replies = IF(replies=0,0,replies-1) WHERE comment_id = %s", secure($comment['node_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    break;
            }
        }
    }

    public function edit_comment($comment_id, $message, $photo)
    {
        global $db, $system;
        /* (check|get) comment */
        $comment = $this->get_comment($comment_id);
        if (!$comment) {
            _error(403);
        }
        /* check if viewer can manage comment [Edit] */
        $comment['edit_comment'] = false;
        if ($this->_logged_in) {
            /* viewer is (admins|moderators)] */
            if ($this->_data['user_group'] < 3) {
                $comment['edit_comment'] = true;
            }
            /* viewer is the author of comment */
            if ($this->_data['user_id'] == $comment['author_id']) {
                $comment['edit_comment'] = true;
            }
        }
        if (!$comment['edit_comment']) {
            _error(400);
        }
        /* update comment */
        $comment['text'] = $message;
        $comment['image'] = (!is_empty($comment['image'])) ? $comment['image'] : $photo;
        $db->query(sprintf("UPDATE posts_comments SET text = %s, image = %s WHERE comment_id = %s", secure($comment['text']), secure($comment['image']), secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        // /* post mention notifications if any */
        if ($comment['node_type'] == "comment") {
            $this->post_mentions($comment['text'], $comment['parent_comment']['node_id'], "reply_" . $comment['parent_comment']['node_type'], 'comment_' . $comment['comment_id'], [$comment['post'], $comment['parent_comment']['author_id']]);
        } else {
            $this->post_mentions($comment['text'], $comment['node_id'], "comment_" . $comment['node_type'], 'comment_' . $comment['comment_id'], [$comment['post']['author_id']]);
        }
        // /* parse text */
        $comment['text_plain'] = htmlentities($comment['text'], ENT_QUOTES, 'utf-8');
        $comment['text'] = $this->_parse($comment['text_plain']);
        /* return */
        return $comment;
    }

    public function like_comment($comment_id)
    {
        global $db;
        /* (check|get) comment */
        $comment = $this->get_comment($comment_id);
        if (!$comment) {
            _error(403);
        }
        /* check blocking */
        if ($this->blocked($comment['author_id'])) {
            _error(403);
        }
        /* like the comment */
        if (!$comment['i_like']) {
            $db->query(sprintf("INSERT INTO posts_comments_likes (user_id, comment_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'), secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            //     /* update comment likes counter */
            $db->query(sprintf("UPDATE posts_comments SET likes = likes + 1 WHERE comment_id = %s", secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            //     /* post notification */
            switch ($comment['node_type']) {
                case 'post':
                    $this->post_notification(['to_user_id' => $comment['author_id'], 'action' => 'like', 'node_type' => 'post_comment', 'node_url' => $comment['node_id'], 'notify_id' => 'comment_' . $comment_id]);
                    break;
                case 'photo':
                    $this->post_notification(['to_user_id' => $comment['author_id'], 'action' => 'like', 'node_type' => 'photo_comment', 'node_url' => $comment['node_id'], 'notify_id' => 'comment_' . $comment_id]);
                    break;
                case 'comment':
                    $_node_type = ($comment['parent_comment']['node_type'] == "post") ? "post_reply" : "photo_reply";
                    $_node_url = $comment['parent_comment']['node_id'];
                    $this->post_notification(['to_user_id' => $comment['author_id'], 'action' => 'like', 'node_type' => $_node_type, 'node_url' => $_node_url, 'notify_id' => 'comment_' . $comment_id]);
                    break;
            }
        }
    }

    public function unlike_comment($comment_id)
    {
        global $db;
        /* (check|get) comment */
        $comment = $this->get_comment($comment_id);
        if (!$comment) {
            _error(403);
        }
        /* check blocking */
        if ($this->blocked($comment['author_id'])) {
            _error(403);
        }
        // /* unlike the comment */
        if ($comment['i_like']) {
            $db->query(sprintf("DELETE FROM posts_comments_likes WHERE user_id = %s AND comment_id = %s", secure($this->_data['user_id'], 'int'), secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            //     /* update comment likes counter */
            $db->query(sprintf("UPDATE posts_comments SET likes = IF(likes=0,0,likes-1) WHERE comment_id = %s", secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            //     /* delete notification */
            switch ($comment['node_type']) {
                case 'post':
                    $this->delete_notification($comment['author_id'], 'like', 'post_comment', $comment['node_id']);
                    break;
                case 'photo':
                    $this->delete_notification($comment['author_id'], 'like', 'photo_comment', $comment['node_id']);
                    break;
                case 'comment':
                    $_node_type = ($comment['parent_comment']['node_type'] == "post") ? "post_reply" : "photo_reply";
                    $_node_url = $comment['parent_comment']['node_id'];
                    $this->delete_notification($comment['author_id'], 'like', $_node_type, $_node_url);
                    break;
            }
        }
    }

    public function get_photos($id, $type = 'user', $offset = 0, $pass_check = true)
    {
        global $db, $system;
        $photos = [];
        switch ($type) {
            case 'album':
                $offset *= $system['max_results_even'];
                if ($pass_check) {
                    $get_photos = $db->query(sprintf("SELECT * FROM posts_photos WHERE album_id = %s ORDER BY photo_id DESC LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                    if ($get_photos->num_rows > 0) {
                        while ($photo = $get_photos->fetch_assoc()) {
                            $photos[] = $photo;
                        }
                    }
                } else {
                    $album = $this->get_album($id, false);
                    if (!$album) {
                        return $photos;
                    }
                    $get_photos = $db->query(sprintf("SELECT * FROM posts_photos WHERE album_id = %s ORDER BY photo_id DESC LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                    if ($get_photos->num_rows > 0) {
                        while ($photo = $get_photos->fetch_assoc()) {
                            $photo['manage'] = $album['manage_album'];
                            $photos[] = $photo;
                        }
                    }
                }
                break;
            case 'user':
                $offset *= $system['min_results_even'];
                /* get the target user's privacy */
                $get_privacy = $db->query(sprintf("SELECT user_privacy_photos FROM users WHERE user_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                $privacy = $get_privacy->fetch_assoc();
                /* check the target user's privacy  */
                if (!$this->_check_privacy($privacy['user_privacy_photos'], $id)) {
                    return $photos;
                }
                $get_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source, posts.privacy FROM posts_photos INNER JOIN posts ON posts_photos.post_id = posts.post_id WHERE posts.user_id = %s AND user_type = 'user' ORDER BY posts_photos.photo_id DESC LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                if ($get_photos->num_rows > 0) {
                    while ($photo = $get_photos->fetch_assoc()) {
                        /* check the photo privacy */
                        if ($this->_check_privacy($photo['privacy'], $id)) {
                            $photos[] = $photo;
                        }
                    }
                }
                break;
            case 'page':
                $offset *= $system['min_results_even'];
                $get_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts_photos INNER JOIN posts ON posts_photos.post_id = posts.post_id WHERE posts.user_id = %s AND user_type = 'page' ORDER BY posts_photos.photo_id DESC LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                if ($get_photos->num_rows > 0) {
                    while ($photo = $get_photos->fetch_assoc()) {
                        $photos[] = $photo;
                    }
                }
                break;
            case 'group':
                $offset *= $system['min_results_even'];
                if (!$pass_check) {
                    /* check if the viewer is group member (approved) */
                    $check_group = $db->query(sprintf("SELECT groups.* FROM groups INNER JOIN groups_members ON groups.group_id = groups_members.group_id WHERE groups_members.approved = '1' AND groups_members.user_id = %s AND groups_members.group_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                    /* if no -> return */
                    if ($check_group->num_rows == 0) {
                        return $photos;
                    }
                }
                $get_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts_photos INNER JOIN posts ON posts_photos.post_id = posts.post_id WHERE posts.group_id = %s AND in_group = '1' ORDER BY posts_photos.photo_id DESC LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                if ($get_photos->num_rows > 0) {
                    while ($photo = $get_photos->fetch_assoc()) {
                        $photos[] = $photo;
                    }
                }
                break;
            case 'event':
                $offset *= $system['min_results_even'];
                if (!$pass_check) {
                    /* check if the viewer is event member (approved) */
                    $check_event = $db->query(sprintf("SELECT events.* FROM events INNER JOIN events_members ON events.event_id = events_members.event_id WHERE events_members.user_id = %s AND events_members.event_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                    /* if no -> return */
                    if ($check_event->num_rows == 0) {
                        _error(403);
                    }
                }
                $get_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts_photos INNER JOIN posts ON posts_photos.post_id = posts.post_id WHERE posts.event_id = %s AND in_event = '1' ORDER BY posts_photos.photo_id DESC LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                if ($get_photos->num_rows > 0) {
                    while ($photo = $get_photos->fetch_assoc()) {
                        $photos[] = $photo;
                    }
                }
                break;
        }
        return $photos;
    }

    public function get_photo($photo_id, $full_details = false, $get_gallery = false, $context = 'photos')
    {
        global $db;
        /* get photo */
        $get_photo = $db->query(sprintf("SELECT * FROM posts_photos WHERE photo_id = %s", secure($photo_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_photo->num_rows == 0) {
            return false;
        }
        $photo = $get_photo->fetch_assoc();
        /* get post */
        $post = $this->_check_post($photo['post_id'], false, $full_details);
        if (!$post) {
            return false;
        }
        $check_like = $db->query(sprintf("SELECT * FROM posts_likes WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $photo['i_like'] = ($check_like->num_rows > 0) ? true : false;
        /* check if photo can be deleted */
        if ($post['in_group']) {
            /* check if (cover|profile) photo */
            $photo['can_delete'] = (($photo_id == $post['group_picture_id']) or ($photo_id == $post['group_cover_id'])) ? false : true;
        } elseif ($post['in_event']) {
            /* check if (cover) photo */
            $photo['can_delete'] = (($photo_id == $post['event_cover_id'])) ? false : true;
        } elseif ($post['user_type'] == "user") {
            /* check if (cover|profile) photo */
            $photo['can_delete'] = (($photo_id == $post['user_picture_id']) or ($photo_id == $post['user_cover_id'])) ? false : true;
        } elseif ($post['user_type'] == "page") {
            /* check if (cover|profile) photo */
            $photo['can_delete'] = (($photo_id == $post['page_picture_id']) or ($photo_id == $post['page_cover_id'])) ? false : true;
        }
        /* check photo type [single|mutiple] */
        $check_single = $db->query(sprintf("SELECT * FROM posts_photos WHERE post_id = %s", secure($photo['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $photo['is_single'] = ($check_single->num_rows > 1) ? false : true;
        /* get full details */
        if ($full_details) {
            if ($photo['is_single']) {
                /* single photo => get (likes|comments) of post */
                /* check if viewer likes this post */
                if ($this->_logged_in && $post['likes'] > 0) {
                    $check_like = $db->query(sprintf("SELECT * FROM posts_likes WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    $photo['i_like'] = ($check_like->num_rows > 0) ? true : false;
                }
                /* get post comments */
                if ($post['comments'] > 0) {
                    $post['post_comments'] = $this->get_comments($post['post_id'], 0, true, true, $post);
                }
            } else {
                /* mutiple photo => get (likes|comments) of photo */
                /* check if viewer user likes this photo */
                if ($this->_logged_in && $photo['likes'] > 0) {
                    $check_like = $db->query(sprintf("SELECT * FROM posts_photos_likes WHERE user_id = %s AND photo_id = %s", secure($this->_data['user_id'], 'int'), secure($photo['photo_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    $photo['i_like'] = ($check_like->num_rows > 0) ? true : false;
                }
                /* get post comments */
                if ($photo['comments'] > 0) {
                    $photo['photo_comments'] = $this->get_comments($photo['photo_id'], 0, false, true, $post);
                }
            }
        }
        /* get gallery */
        if ($get_gallery) {
            switch ($context) {
                case 'post':
                    $get_post_photos = $db->query(sprintf("SELECT photo_id, source FROM posts_photos WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    while ($post_photo = $get_post_photos->fetch_assoc()) {
                        $post_photos[$post_photo['photo_id']] = $post_photo;
                    }

                    $c_val = (int) count($post_photos);

					if(empty($post_photos[get_array_key($post_photos, $photo['photo_id'], 1)])){
						$photo['next'] = $post_photos[get_array_key($post_photos, $photo['photo_id'], -($c_val - 1))];
					}
					else{
						$photo['next'] = $post_photos[get_array_key($post_photos, $photo['photo_id'], 1)];
					}

					if(empty($post_photos[get_array_key($post_photos, $photo['photo_id'], 1)])){
						$photo['prev'] = $post_photos[get_array_key($post_photos, $photo['photo_id'], -($c_val - 1))];
					}
					else{
						$photo['prev'] = $post_photos[get_array_key($post_photos, $photo['photo_id'], 1)];
					}

                    //$photo['next'] = $post_photos[get_array_key($post_photos, $photo['photo_id'], 1)];
                    //$photo['prev'] = $post_photos[get_array_key($post_photos, $photo['photo_id'], 1)];
                    break;
                case 'album':
                    $get_album_photos = $db->query(sprintf("SELECT photo_id, source FROM posts_photos WHERE album_id = %s", secure($photo['album_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    while ($album_photo = $get_album_photos->fetch_assoc()) {
                        $album_photos[$album_photo['photo_id']] = $album_photo;
                    }

					$c_val = (int) count($album_photos);

					if(empty($album_photos[get_array_key($album_photos, $photo['photo_id'], 1)])){
						$photo['next'] = $album_photos[get_array_key($album_photos, $photo['photo_id'], -($c_val - 1))];
					}
					else{
						$photo['next'] = $album_photos[get_array_key($album_photos, $photo['photo_id'], 1)];
					}

					if(empty($album_photos[get_array_key($album_photos, $photo['photo_id'], 1)])){
						$photo['prev'] = $album_photos[get_array_key($album_photos, $photo['photo_id'], -($c_val - 1))];
					}
					else{
						$photo['prev'] = $album_photos[get_array_key($album_photos, $photo['photo_id'], 1)];
					}

                    //$photo['next'] = $album_photos[get_array_key($album_photos, $photo['photo_id'], -1)];
                    //$photo['prev'] = $album_photos[get_array_key($album_photos, $photo['photo_id'], 1)];
                    break;
                case 'photos':
                    if ($post['in_group']) {
                        $get_target_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts INNER JOIN posts_photos ON posts.post_id = posts_photos.post_id WHERE posts.in_group = '1' AND posts.group_id = %s", secure($post['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    } elseif ($post['in_event']) {
                        $get_target_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts INNER JOIN posts_photos ON posts.post_id = posts_photos.post_id WHERE posts.in_event = '1' AND posts.event_id = %s", secure($post['event_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    } elseif ($post['user_type'] == "page") {
                        $get_target_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts INNER JOIN posts_photos ON posts.post_id = posts_photos.post_id WHERE posts.user_type = 'page' AND posts.user_id = %s", secure($post['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    } elseif ($post['user_type'] == "user") {
                        $get_target_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts INNER JOIN posts_photos ON posts.post_id = posts_photos.post_id WHERE posts.user_type = 'user' AND posts.user_id = %s", secure($post['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    }
                    while ($target_photo = $get_target_photos->fetch_assoc()) {
                        $target_photos[$target_photo['photo_id']] = $target_photo;
                    }

					$c_val = (int) count($target_photos);

					if(empty($target_photos[get_array_key($target_photos, $photo['photo_id'], 1)])){
						$photo['next'] = $target_photos[get_array_key($target_photos, $photo['photo_id'], -($c_val - 1))];
					}
					else{
						$photo['next'] = $target_photos[get_array_key($target_photos, $photo['photo_id'], 1)];
					}

					if(empty($target_photos[get_array_key($target_photos, $photo['photo_id'], 1)])){
						$photo['prev'] = $target_photos[get_array_key($target_photos, $photo['photo_id'], -($c_val - 1))];
					}
					else{
						$photo['prev'] = $target_photos[get_array_key($target_photos, $photo['photo_id'], 1)];
					}

                    //$photo['next'] = $target_photos[get_array_key($target_photos, $photo['photo_id'], -1)];
                    //$photo['prev'] = $target_photos[get_array_key($target_photos, $photo['photo_id'], 1)];
                    break;
            }
        }
        /* return post array with photo */
        $photo['post'] = $post;
        return $photo;
    }

    public function _get_photo($user_id)
    {
        global $db, $system;
        $get_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts_photos INNER JOIN posts ON posts_photos.post_id = posts.post_id WHERE posts.user_id = %s AND user_type = 'user' ORDER BY posts_photos.photo_id DESC", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_photos->num_rows > 0) {
            while ($photo = $get_photos->fetch_assoc()) {
                /* check the photo privacy */
                $photos[] = $photo['photo_id'];
            }
        }
        return $photos;
    }

    public function delete_photo($photo_id)
    {
        global $db, $system;
        /* (check|get) photo */
        $photo = $this->get_photo($photo_id);
        if (!$photo) {
            _error(403);
        }
        $post = $photo['post'];
        /* check if viewer can manage post */
        if (!$post['manage_post']) {
            _error(403);
        }
        /* check if photo can be deleted */
        if (!$photo['can_delete']) {
            throw new Exception(__("This photo can't be deleted"));
        }
        /* delete the photo */
        $db->query(sprintf("DELETE FROM posts_photos WHERE photo_id = %s", secure($photo_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function like_photo($photo_id)
    {
        global $db;
        /* (check|get) photo */
        $photo = $this->get_photo($photo_id);
        if (!$photo) {
            _error(403);
        }
        $post = $photo['post'];
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            _error(403);
        }
        /* like the photo */
        $db->query(sprintf("INSERT INTO posts_photos_likes (user_id, photo_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'), secure($photo_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* update photo likes counter */
        $db->query(sprintf("UPDATE posts_photos SET likes = likes + 1 WHERE photo_id = %s", secure($photo_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* post notification */
        $this->post_notification(['to_user_id' => $post['author_id'], 'action' => 'like', 'node_type' => 'photo', 'node_url' => $photo_id]);
    }

    public function unlike_photo($photo_id)
    {
        global $db;
        /* (check|get) photo */
        $photo = $this->get_photo($photo_id);
        if (!$photo) {
            _error(403);
        }
        $post = $photo['post'];
        /* unlike the photo */
        $db->query(sprintf("DELETE FROM posts_photos_likes WHERE user_id = %s AND photo_id = %s", secure($this->_data['user_id'], 'int'), secure($photo_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* update photo likes counter */
        $db->query(sprintf("UPDATE posts_photos SET likes = IF(likes=0,0,likes-1) WHERE photo_id = %s", secure($photo_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* delete notification */
        $this->delete_notification($post['author_id'], 'like', 'photo', $photo_id);
    }

    public function get_albums($id, $type = 'user', $offset = 0)
    {
        global $db, $system;
        /* initialize vars */
        $albums = [];
        $offset *= $system['max_results_even'];
        if (!in_array($type, ['user', 'page', 'group', 'event'])) {
            return $albums;
        }
        switch ($type) {
            case 'user':
                $get_albums = $db->query(sprintf("SELECT album_id FROM posts_photos_albums WHERE user_type = 'user' AND user_id = %s AND in_group = '0' AND in_event = '0' LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'page':
                $get_albums = $db->query(sprintf("SELECT album_id FROM posts_photos_albums WHERE user_type = 'page' AND user_id = %s AND in_group = '0' AND in_event = '0' LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'group':
                $get_albums = $db->query(sprintf("SELECT album_id FROM posts_photos_albums WHERE in_group = '1' AND group_id = %s LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'event':
                $get_albums = $db->query(sprintf("SELECT album_id FROM posts_photos_albums WHERE in_event = '1' AND event_id = %s LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                break;
        }
        if ($get_albums->num_rows > 0) {
            while ($album = $get_albums->fetch_assoc()) {
                $album = $this->get_album($album['album_id'], false); /* $full_details = false */
                if ($album) {
                    $albums[] = $album;
                }
            }
        }
        return $albums;
    }

    public function get_album($album_id, $full_details = true)
    {
        global $db, $system;
        $get_album = $db->query(sprintf("SELECT posts_photos_albums.*, users.user_name, users.user_album_pictures, users.user_album_covers, users.user_album_timeline, pages.page_id, pages.page_name, pages.page_admin, pages.page_album_pictures, pages.page_album_covers, pages.page_album_timeline, groups.group_name, groups.group_admin, groups.group_album_pictures, groups.group_album_covers, groups.group_album_timeline, events.event_admin, events.event_album_covers, events.event_album_timeline FROM posts_photos_albums LEFT JOIN users ON posts_photos_albums.user_id = users.user_id AND posts_photos_albums.user_type = 'user' LEFT JOIN pages ON posts_photos_albums.user_id = pages.page_id AND posts_photos_albums.user_type = 'page' LEFT JOIN groups ON posts_photos_albums.in_group = '1' AND posts_photos_albums.group_id = groups.group_id LEFT JOIN events ON posts_photos_albums.in_event = '1' AND posts_photos_albums.event_id = events.event_id WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_photos_albums.album_id = %s", secure($album_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_album->num_rows == 0) {
            return false;
        }
        $album = $get_album->fetch_assoc();
        /* get the author */
        $album['author_id'] = ($album['user_type'] == "page") ? $album['page_admin'] : $album['user_id'];
        /* check the album privacy  */
        /* if post in group & (the group is public || the viewer approved member of this group) => pass privacy check */
        if ($album['in_group'] && ($album['group_privacy'] == 'public' || $this->check_group_membership($this->_data['user_id'], $album['group_id']) == 'approved')) {
            $pass_privacy_check = true;
        }
        /* if post in event & (the event is public || the viewer member of this event) => pass privacy check */
        if ($album['in_event'] && ($album['event_privacy'] == 'public' || $this->check_event_membership($this->_data['user_id'], $album['event_id']))) {
            $pass_privacy_check = true;
        }
        if (!$pass_privacy_check) {
            if (!$this->_check_privacy($album['privacy'], $album['author_id'])) {
                return false;
            }
        }
        /* get album path */
        if ($album['in_group']) {
            $album['path'] = 'groups/' . $album['group_name'];
            /* check if (cover|profile|timeline) album */
            $album['can_delete'] = (($album_id == $album['group_album_pictures']) or ($album_id == $album['group_album_covers']) or ($album_id == $album['group_album_timeline'])) ? false : true;
        } elseif ($album['in_event']) {
            $album['path'] = 'events/' . $album['event_id'];
            /* check if (cover|profile|timeline) album */
            $album['can_delete'] = (($album_id == $album['event_album_pictures']) or ($album_id == $album['event_album_covers']) or ($album_id == $album['event_album_timeline'])) ? false : true;
        } elseif ($album['user_type'] == "user") {
            $album['path'] = $album['user_name'];
            /* check if (cover|profile|timeline) album */
            $album['can_delete'] = (($album_id == $album['user_album_pictures']) or ($album_id == $album['user_album_covers']) or ($album_id == $album['user_album_timeline'])) ? false : true;
        } elseif ($album['user_type'] == "page") {
            $album['path'] = 'pages/' . $album['page_name'];
            /* check if (cover|profile|timeline) album */
            $album['can_delete'] = (($album_id == $album['user_album_timeline']) or ($album_id == $album['page_album_covers']) or ($album_id == $album['page_album_timeline'])) ? false : true;
        }
        /* get album cover photo */
        $get_cover = $db->query(sprintf("SELECT source FROM posts_photos WHERE album_id = %s ORDER BY photo_id DESC", secure($album_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_cover->num_rows == 0) {
            $album['cover'] = $system['system_url'] . '/content/themes/' . $system['theme'] . '/images/blank_album.jpg';
        } else {
            $cover = $get_cover->fetch_assoc();
            $album['cover'] = $system['system_uploads'] . '/' . $cover['source'];
        }
        $album['photos_count'] = $get_cover->num_rows;
        /* check if viewer can manage album [Edit|Update|Delete] */
        $album['is_page_admin'] = ($this->_logged_in && $this->_data['user_id'] == $album['page_admin']) ? true : false;
        $album['is_group_admin'] = ($this->_logged_in && $this->_data['user_id'] == $album['group_admin']) ? true : false;
        $album['is_event_admin'] = ($this->_logged_in && $this->_data['user_id'] == $album['event_admin']) ? true : false;
        /* check if viewer can manage album [Edit|Update|Delete] */
        $album['manage_album'] = false;
        if ($this->_logged_in) {
            /* viewer is (admins|moderators)] */
            if ($this->_data['user_group'] < 3) {
                $album['manage_album'] = true;
            }
            /* viewer is the author of post || page admin */
            if ($this->_data['user_id'] == $album['author_id']) {
                $album['manage_album'] = true;
            }
            /* viewer is the admin of the group of the post */
            if ($album['in_group'] && $album['is_group_admin']) {
                $album['manage_album'] = true;
            }
            /* viewer is the admin of the event of the post */
            if ($album['in_event'] && $album['is_event_admin']) {
                $album['manage_album'] = true;
            }
        }
        /* get album photos */
        if ($full_details) {
            $album['photos'] = $this->get_photos($album_id, 'album');
        }
        return $album;
    }

    public function delete_album($album_id)
    {
        global $db, $system;
        /* (check|get) album */
        $album = $this->get_album($album_id, false);
        if (!$album) {
            _error(403);
        }
        /* check if viewer can manage album */
        if (!$album['manage_album']) {
            _error(403);
        }
        /* check if album can be deleted */
        if (!$album['can_delete']) {
            throw new Exception(__("This album can't be deleted"));
        }
        /* delete the album */
        $db->query(sprintf("DELETE FROM posts_photos_albums WHERE album_id = %s", secure($album_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* delete all album photos */
        $db->query(sprintf("DELETE FROM posts_photos WHERE album_id = %s", secure($album_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* retrun path */
        $path = $system['system_url'] . "/" . $album['path'] . "/albums";
        return $path;
    }

    public function edit_album($album_id, $title)
    {
        global $db;
        /* (check|get) album */
        $album = $this->get_album($album_id, false);
        if (!$album) {
            _error(400);
        }
        /* check if viewer can manage album */
        if (!$album['manage_album']) {
            _error(400);
        }
        /* validate all fields */
        if (is_empty($title)) {
            throw new Exception(__("You must fill in all of the fields"));
        }
        /* edit the album */
        $db->query(sprintf("UPDATE posts_photos_albums SET title = %s WHERE album_id = %s", secure($title), secure($album_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function add_album_photos($args = [])
    {
        global $db, $system, $date;
        /* (check|get) album */
        $album = $this->get_album($args['album_id'], false);
        if (!$album) {
            _error(400);
        }
        /* check if viewer can manage album */
        if (!$album['manage_album']) {
            _error(400);
        }
        /* check user_id */
        $user_id = ($album['user_type'] == "page") ? $album['page_id'] : $album['user_id'];
        /* check post location */
        $args['location'] = (!is_empty($args['location']) && valid_location($args['location'])) ? $args['location'] : '';
        /* check privacy */
        if ($album['in_group'] || $album['in_event']) {
            $args['privacy'] = 'custom';
        } elseif ($album['user_type'] == "page") {
            $args['privacy'] = 'public';
        } else {
            if (!in_array($args['privacy'], ['me', 'friends', 'public', 'follow'])) {
                $args['privacy'] = 'public';
            }
        }
        /* insert the post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, in_group, group_id, in_event, event_id, post_type, time, location, privacy, text) VALUES (%s, %s, %s, %s, %s, %s, 'album', %s, %s, %s, %s)", secure($user_id, 'int'), secure($album['user_type']), secure($album['in_group']), secure($album['group_id'], 'int'), secure($album['in_event']), secure($album['event_id'], 'int'), secure($date), secure($args['location']), secure($args['privacy']), secure($args['message']))) or _error(SQL_ERROR_THROWEN);
        $post_id = $db->insert_id;
        /* insert new photos */
        foreach ($args['photos'] as $photo) {
            $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($args['album_id'], 'int'), secure($photo))) or _error(SQL_ERROR_THROWEN);
        }
        /* post mention notifications */
        $this->post_mentions($args['message'], $post_id);
        return $post_id;
    }

    public function share($post_id)
    {
        global $db, $date;
        /* check if the viewer can share the post */
        $post = $this->_check_post($post_id, true);
        if (!$post || $post['privacy'] != 'public') {
            _error(403);
        }
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            _error(403);
        }
        // share post
        /* share the origin post */
        if ($post['post_type'] == "shared") {
            $origin = $this->_check_post($post['origin_id'], true);
            if (!$origin || $origin['privacy'] != 'public') {
                _error(403);
            }
            $post_id = $origin['post_id'];
            $author_id = $origin['author_id'];
        } else {
            $post_id = $post['post_id'];
            $author_id = $post['author_id'];
        }
        /* insert the new shared post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, origin_id, time, privacy) VALUES (%s, 'user', 'shared', %s, %s, 'public')", secure($this->_data['user_id'], 'int'), secure($post_id, 'int'), secure($date))) or _error(SQL_ERROR_THROWEN);
        $share_post_id = $db->insert_id;        /* update the origin post shares counter */
        $db->query(sprintf("UPDATE posts SET shares = shares + 1 WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* post notification */
        $this->post_notification(['to_user_id' => $author_id, 'action' => 'share', 'node_type' => 'post', 'node_url' => $post_id]);
        return $share_post_id;    }

    public function delete_post($post_id)
    {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id, true);
        if (!$post) {
            _error(403);
        }
        /* delete the post */
        if (!$post['manage_post']) {
            _error(403);
        }
        /* delete post */
        $db->query(sprintf("DELETE FROM posts WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* delete post photos (if any) */
        $db->query(sprintf("DELETE FROM posts_photos WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* delete post likes (if any) */
        $db->query(sprintf("DELETE FROM posts_likes WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* delete post likes from posts_likes table(if any) */
        $db->query(sprintf("DELETE FROM posts_likes WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);        /* delete cover & profile pictures (if any) */        /* delete cover & profile pictures (if any) */
        /* delete cover & profile pictures (if any) */        $refresh = false;
        switch ($post['post_type']) {
            case 'profile_cover':
                /* update user cover */
                //$db->query(sprintf("UPDATE users SET user_cover = null, user_cover_id = null WHERE user_id = %s", secure($post['author_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* return */
                $refresh = true;
                break;
            case 'profile_picture':
                /* update user picture */
                //$db->query(sprintf("UPDATE users SET user_picture = null, user_picture_id = null WHERE user_id = %s", secure($post['author_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* return */
                $refresh = true;
                break;
            case 'page_cover':
                /* update page cover */
                $db->query(sprintf("UPDATE pages SET page_cover = null, page_cover_id = null WHERE page_id = %s", secure($post['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* return */
                $refresh = true;
                break;
            case 'page_picture':
                /* update page picture */
                $db->query(sprintf("UPDATE pages SET page_picture = null, page_picture_id = null WHERE page_id = %s", secure($post['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* return */
                $refresh = true;
                break;
            case 'group_cover':
                /* update group cover */
                $db->query(sprintf("UPDATE groups SET group_cover = null, group_cover_id = null WHERE group_id = %s", secure($post['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* return */
                $refresh = true;
                break;
            case 'group_picture':
                /* update group picture */
                $db->query(sprintf("UPDATE groups SET group_picture = null, group_picture_id = null WHERE group_id = %s", secure($post['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* return */
                $refresh = true;
                break;
            case 'event_cover':
                /* update event cover */
                $db->query(sprintf("UPDATE events SET event_cover = null, event_cover_id = null WHERE event_id = %s", secure($post['event_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* return */
                $refresh = true;
                break;
        }
        return $refresh;
    }

    public function edit_post($post_id, $message)
    {
        global $db, $system;
        /* (check|get) post */
        $post = $this->_check_post($post_id, true);
        if (!$post) {
            _error(403);
        }
        /* check if viewer can edit post */
        if (!$post['manage_post']) {
            _error(403);
        }
        /* update post */
        $db->query(sprintf("UPDATE posts SET text = %s WHERE post_id = %s", secure($message), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* post mention notifications */
        $this->post_mentions($message, $post_id);
        /* parse text */
        $post['text_plain'] = htmlentities($message, ENT_QUOTES, 'utf-8');
        $post['text'] = $this->_parse($post['text_plain']);
        /* return */
        return $post;
    }

    public function edit_product($post_id, $message, $name, $price, $category_id, $location)
    {
        global $db, $system;
        /* (check|get) post */
        $post = $this->_check_post($post_id, true);
        if (!$post) {
            _error(403);
        }
        /* check if viewer can edit post */
        if (!$post['manage_post']) {
            _error(403);
        }
        /* update post */
        $db->query(sprintf("UPDATE posts SET text = %s WHERE post_id = %s", secure($message), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* update product */
        $location = (!is_empty($location) && valid_location($location)) ? $location : '';
        $db->query(sprintf("UPDATE posts_products SET name = %s, price = %s, category_id = %s, location = %s WHERE post_id = %s", secure($name), secure($price), secure($category_id, 'int'), secure($location), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function edit_privacy($post_id, $privacy)
    {
        global $db, $system;
        /* (check|get) post */
        $post = $this->_check_post($post_id, true);
        if (!$post) {
            _error(403);
        }
        /* check if viewer can edit privacy */
        if ($post['manage_post'] && $post['user_type'] == 'user' && !$post['in_group'] && !$post['in_event'] && $post['post_type'] != "product") {
            /* update privacy */
            $db->query(sprintf("UPDATE posts SET privacy = %s WHERE post_id = %s", secure($privacy), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        } else {
            _error(403);
        }
    }

    public function sold_post($post_id)
    {
        global $db, $date;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if (!$post) {
            _error(403);
        }
        /* check if viewer can edit post */
        if (!$post['manage_post']) {
            _error(403);
        }
        /* sold post */
        $db->query(sprintf("UPDATE posts_products SET available = '0' WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function unsold_post($post_id)
    {
        global $db, $date;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if (!$post) {
            _error(403);
        }
        /* check if viewer can edit post */
        if (!$post['manage_post']) {
            _error(403);
        }
        /* unsold post */
        $db->query(sprintf("UPDATE posts_products SET available = '1' WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function save_post($post_id)
    {
        global $db, $date;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if (!$post) {
            _error(403);
        }
        /* save post */
        if (!$post['i_save']) {
            $db->query(sprintf("INSERT INTO posts_saved (post_id, user_id, time) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($this->_data['user_id'], 'int'), secure($date))) or _error(SQL_ERROR_THROWEN);
        }
    }

    public function unsave_post($post_id)
    {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if (!$post) {
            _error(403);
        }
        /* unsave post */
        if ($post['i_save']) {
            $db->query(sprintf("DELETE FROM posts_saved WHERE post_id = %s AND user_id = %s", secure($post_id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }
    }

    public function boost_post($post_id)
    {
        global $db, $date;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if (!$post) {
            _error(403);
        }
        /* check if viewer can edit post */
        if (!$post['manage_post']) {
            _error(403);
        }
        /* check if viewer can boost post */
        if (!$this->_data['can_boost_posts']) {
            modal(MESSAGE, __("Sorry"), __("You reached the maximum number of boosted posts! Upgrade your package to get more"));
        }
        /* boost post */
        if (!$post['boosted']) {
            /* boost post */
            $db->query(sprintf("UPDATE posts SET boosted = '1' WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* update user */
            $db->query(sprintf("UPDATE users SET user_boosted_posts = user_boosted_posts + 1 WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }
    }

    public function unboost_post($post_id)
    {
        global $db, $date;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if (!$post) {
            _error(403);
        }
        /* check if viewer can edit post */
        if (!$post['manage_post']) {
            _error(403);
        }
        /* unboost post */
        if ($post['boosted']) {
            /* unboost post */
            $db->query(sprintf("UPDATE posts SET boosted = '0' WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* update user */
            $db->query(sprintf("UPDATE users SET user_boosted_posts = IF(user_boosted_posts=0,0,user_boosted_posts-1) WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }
    }

    public function pin_post($post_id)
    {
        global $db, $date;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if (!$post) {
            _error(403);
        }
        /* check if viewer can edit post */
        if (!$post['manage_post']) {
            _error(403);
        }
        /* pin post */
        if (!$post['pinned']) {
            /* check the post author type */
            if ($post['user_type'] == "user") {
                /* user */
                if ($post['in_group']) {
                    /* update group */
                    $db->query(sprintf("UPDATE groups SET group_pinned_post = %s WHERE group_id = %s", secure($post_id, 'int'), secure($post['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                } elseif ($post['in_event']) {
                    /* update event */
                    $db->query(sprintf("UPDATE events SET event_pinned_post = %s WHERE event_id = %s", secure($post_id, 'int'), secure($post['event_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                } else {
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_pinned_post = %s WHERE user_id = %s", secure($post_id, 'int'), secure($post['author_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                }
            } else {
                /* update page */
                $db->query(sprintf("UPDATE pages SET page_pinned_post = %s WHERE page_id = %s", secure($post_id, 'int'), secure($post['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            }
        }
    }

    public function unpin_post($post_id)
    {
        global $db, $date;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if (!$post) {
            _error(403);
        }
        /* check if viewer can edit post */
        if (!$post['manage_post']) {
            _error(403);
        }
        /* pin post */
        if ($post['pinned']) {
            /* check the post author type */
            if ($post['user_type'] == "user") {
                /* user */
                if ($post['in_group']) {
                    /* update group */
                    $db->query(sprintf("UPDATE groups SET group_pinned_post = '0' WHERE group_id = %s", secure($post['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                } elseif ($post['in_event']) {
                    /* update event */
                    $db->query(sprintf("UPDATE events SET event_pinned_post = '0' WHERE event_id = %s", secure($post['event_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                } else {
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_pinned_post = '0' WHERE user_id = %s", secure($post['author_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                }
            } else {
                /* update page */
                $db->query(sprintf("UPDATE pages SET page_pinned_post = '0' WHERE page_id = %s", secure($post['author_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            }
        }
    }

    public function like_post($post_id)
    {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if (!$post) {
            _error(403);
        }
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            _error(403);
        }
        /* like the post */
        if (!$post['i_like']) {
            $db->query(sprintf("INSERT INTO posts_likes (user_id, post_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* update post likes counter */
            $db->query(sprintf("UPDATE posts SET likes = likes + 1 WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* post notification */
            $this->post_notification(['to_user_id' => $post['author_id'], 'action' => 'like', 'node_type' => 'post', 'node_url' => $post_id]);
        }
    }

    public function unlike_post($post_id)
    {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if (!$post) {
            _error(403);
        }
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            _error(403);
        }
        /* unlike the post */
        if ($post['i_like']) {
            $db->query(sprintf("DELETE FROM posts_likes WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* update post likes counter */
            $db->query(sprintf("UPDATE posts SET likes = IF(likes=0,0,likes-1) WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* delete notification */
            $this->delete_notification($post['author_id'], 'like', 'post', $post_id);
        }
    }

    public function hide_post($post_id)
    {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if (!$post) {
            _error(403);
        }
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            _error(403);
        }
        /* hide the post */
        $db->query(sprintf("INSERT INTO posts_hidden (user_id, post_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function unhide_post($post_id)
    {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if (!$post) {
            _error(403);
        }
        /* unhide the post */
        $db->query(sprintf("DELETE FROM posts_hidden WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function add_vote($option_id)
    {
        global $db;
        /* get poll */
        $get_poll = $db->query(sprintf("SELECT posts_polls.* FROM posts_polls_options INNER JOIN posts_polls ON posts_polls_options.poll_id = posts_polls.poll_id WHERE option_id = %s", secure($option_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_poll->num_rows == 0) {
            _error(403);
        }
        $poll = $get_poll->fetch_assoc();
        /* (check|get) post */
        $post = $this->_check_post($poll['post_id']);
        if (!$post) {
            _error(403);
        }
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            _error(403);
        }
        /* insert user vote */
        $vote = $db->query(sprintf("INSERT INTO users_polls_options (user_id, poll_id, option_id) VALUES (%s, %s, %s)", secure($this->_data['user_id'], 'int'), secure($poll['poll_id'], 'int'), secure($option_id, 'int')));
        if ($vote) {
            /* update poll votes */
            $db->query(sprintf("UPDATE posts_polls SET votes = votes + 1 WHERE poll_id = %s", secure($poll['poll_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* post notification */
            $this->post_notification(['to_user_id' => $post['author_id'], 'action' => 'vote', 'node_type' => 'post', 'node_url' => $post['post_id']]);
        }
    }

    public function delete_vote($option_id)
    {
        global $db;
        /* get poll */
        $get_poll = $db->query(sprintf("SELECT posts_polls.* FROM posts_polls_options INNER JOIN posts_polls ON posts_polls_options.poll_id = posts_polls.poll_id WHERE option_id = %s", secure($option_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_poll->num_rows == 0) {
            _error(403);
        }
        $poll = $get_poll->fetch_assoc();
        /* (check|get) post */
        $post = $this->_check_post($poll['post_id']);
        if (!$post) {
            _error(403);
        }
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            _error(403);
        }
        /* delete user vote */
        $db->query(sprintf("DELETE FROM users_polls_options WHERE user_id = %s AND poll_id = %s AND option_id = %s", secure($this->_data['user_id'], 'int'), secure($poll['poll_id'], 'int'), secure($option_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($db->affected_rows > 0) {
            /* update poll votes */
            $db->query(sprintf("UPDATE posts_polls SET votes = IF(votes=0,0,votes-1) WHERE poll_id = %s", secure($poll['poll_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* delete notification */
            $this->delete_notification($post['author_id'], 'vote', 'post', $post['post_id']);
        }
    }

    public function change_vote($option_id, $checked_id)
    {
        global $db;
        /* get poll */
        $get_poll = $db->query(sprintf("SELECT posts_polls.* FROM posts_polls_options INNER JOIN posts_polls ON posts_polls_options.poll_id = posts_polls.poll_id WHERE option_id = %s", secure($option_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_poll->num_rows == 0) {
            _error(403);
        }
        $poll = $get_poll->fetch_assoc();
        /* (check|get) post */
        $post = $this->_check_post($poll['post_id']);
        if (!$post) {
            _error(403);
        }
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            _error(403);
        }
        /* delete old vote */
        $db->query(sprintf("DELETE FROM users_polls_options WHERE user_id = %s AND poll_id = %s AND option_id = %s", secure($this->_data['user_id'], 'int'), secure($poll['poll_id'], 'int'), secure($checked_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($db->affected_rows > 0) {
            /* insert new vote */
            $db->query(sprintf("INSERT INTO users_polls_options (user_id, poll_id, option_id) VALUES (%s, %s, %s)", secure($this->_data['user_id'], 'int'), secure($poll['poll_id'], 'int'), secure($option_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        }
    }

    public function get_articles($args = [])
    {
        global $db, $system;
        /* initialize arguments */
        $offset = !isset($args['offset']) ? 0 : $args['offset'];
        $random = !isset($args['random']) ? false : true;
        $results = !isset($args['results']) ? $system['max_results'] : $args['results'];
        /* initialize vars */
        $posts = [];
        $offset *= $results;
        /* get posts */
        $order_query = ($random) ? "ORDER BY RAND()" : "ORDER BY posts.time DESC";
        $get_posts = $db->query(sprintf("SELECT posts.post_id FROM posts INNER JOIN posts_articles ON posts.post_id = posts_articles.post_id WHERE posts.post_type = 'article' " . $order_query . " LIMIT %s, %s", secure($offset, 'int', false), secure($results, 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_posts->num_rows > 0) {
            while ($post = $get_posts->fetch_assoc()) {
                $post = $this->get_post($post['post_id']);
                if ($post) {
                    $posts[] = $post;
                }
            }
        }
        return $posts;
    }

    public function post_article($title, $text, $cover, $tags)
    {
        global $db, $system, $date;
        /* check if blogs enabled */
        if (!$system['blogs_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* validate title */
        if (is_empty($title)) {
            throw new Exception(__("You must enter a title for your article"));
        }
        if (strlen($title) < 3) {
            throw new Exception(__("Article title must be at least 3 characters long. Please try another"));
        }
        /* validate text */
        if (is_empty($text)) {
            throw new Exception(__("You must enter some text for your article"));
        }
        /* insert the post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, time, privacy) VALUES (%s, 'user', 'article', %s, 'public')", secure($this->_data['user_id'], 'int'), secure($date))) or _error(SQL_ERROR_THROWEN);
        $post_id = $db->insert_id;
        /* insert article */
        $db->query(sprintf("INSERT INTO posts_articles (post_id, cover, title, text, tags) VALUES (%s, %s, %s, %s, %s)", secure($post_id, 'int'), secure($cover), secure($title), secure($text), secure($tags))) or _error(SQL_ERROR_THROWEN);
        return $post_id;
    }

    public function edit_article($post_id, $title, $text, $cover, $tags)
    {
        global $db, $system, $date;
        /* check if blogs enabled */
        if (!$system['blogs_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* (check|get) post */
        $post = $this->_check_post($post_id, true);
        if (!$post) {
            _error(403);
        }
        /* check if viewer can edit post */
        if (!$post['manage_post']) {
            _error(403);
        }
        /* validate title */
        if (is_empty($title)) {
            throw new Exception(__("You must enter a title for your article"));
        }
        if (strlen($title) < 3) {
            throw new Exception(__("Article title must be at least 3 characters long. Please try another"));
        }
        /* validate text */
        if (is_empty($text)) {
            throw new Exception(__("You must enter some text for your article"));
        }
        /* update the article */
        $db->query(sprintf("UPDATE posts_articles SET cover = %s, title = %s, text = %s, tags = %s WHERE post_id = %s", secure($cover), secure($title), secure($text), secure($tags), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function update_article_views($article_id)
    {
        global $db;
        /* update */
        $db->query(sprintf("UPDATE posts_articles SET views = views + 1 WHERE article_id = %s", secure($article_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function get_pages($args = [])
    {
        global $db, $system;
        /* initialize arguments */
        $user_id = !isset($args['user_id']) ? null : $args['user_id'];
        $offset = !isset($args['offset']) ? 0 : $args['offset'];
        $promoted = !isset($args['promoted']) ? false : true;
        $suggested = !isset($args['suggested']) ? false : true;
        $random = !isset($args['random']) ? false : true;
        $results = !isset($args['results']) ? $system['max_results_even'] : $args['results'];
        /* initialize vars */
        $pages = [];
        $offset *= $results;
        /* get suggested pages */
        if ($promoted) {
            $get_pages = $db->query("SELECT * FROM pages WHERE page_boosted = '1' ORDER BY RAND() LIMIT 3") or _error(SQL_ERROR_THROWEN);
        } elseif ($suggested) {
            $pages_ids = $this->get_pages_ids();
            $random_statement = ($random) ? "ORDER BY RAND()" : "";
            if (count($pages_ids) > 0) {
                /* make a list from liked pages */
                $pages_list = implode(',', $pages_ids);
                $get_pages = $db->query(sprintf("SELECT * FROM pages WHERE page_id NOT IN (%s) " . $random_statement . " LIMIT %s, %s", $pages_list, secure($offset, 'int', false), secure($results, 'int', false))) or _error(SQL_ERROR_THROWEN);
            } else {
                $get_pages = $db->query(sprintf("SELECT * FROM pages " . $random_statement . " LIMIT %s, %s", secure($offset, 'int', false), secure($results, 'int', false))) or _error(SQL_ERROR_THROWEN);
            }
            /* get the "viewer" pages who admin */
        } elseif ($user_id == null) {
            $get_pages = $db->query(sprintf("SELECT * FROM pages WHERE page_admin = %s LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($results, 'int', false))) or _error(SQL_ERROR_THROWEN);
            /* get the "target" liked pages*/
        } else {
            /* get the target user's privacy */
            $get_privacy = $db->query(sprintf("SELECT user_privacy_pages FROM users WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            $privacy = $get_privacy->fetch_assoc();
            /* check the target user's privacy  */
            if (!$this->_check_privacy($privacy['user_privacy_pages'], $user_id)) {
                return $pages;
            }
            $get_pages = $db->query(sprintf("SELECT pages.* FROM pages INNER JOIN pages_likes ON pages.page_id = pages_likes.page_id WHERE pages_likes.user_id = %s LIMIT %s, %s", secure($user_id, 'int'), secure($offset, 'int', false), secure($results, 'int', false))) or _error(SQL_ERROR_THROWEN);
        }
        if ($get_pages->num_rows > 0) {
            while ($page = $get_pages->fetch_assoc()) {
                $page['page_picture'] = $this->get_picture($page['page_picture'], 'page');
                /* check if the viewer liked the page */
                $page['i_like'] = false;
                if ($this->_logged_in) {
                    $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE page_id = %s AND user_id = %s", secure($page['page_id'], 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    if ($get_likes->num_rows > 0) {
                        $page['i_like'] = true;
                    }
                }
                $pages[] = $page;
            }
        }
        return $pages;
    }

    public function get_pages_categories()
    {
        global $db;
        $categories = [];
        $get_categories = $db->query("SELECT * FROM pages_categories") or _error(SQL_ERROR_THROWEN);
        if ($get_categories->num_rows > 0) {
            while ($category = $get_categories->fetch_assoc()) {
                $categories[] = $category;
            }
        }
        return $categories;
    }

    public function create_page($title, $username, $category, $description)
    {
        global $db, $system, $date;
        /* check if pages enabled */
        if ($this->_data['user_group'] >= 3 && !$system['pages_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* validate title */
        if (is_empty($title)) {
            throw new Exception(__("You must enter a title for your page"));
        }
        if (strlen($title) < 3) {
            throw new Exception(__("Page title must be at least 3 characters long. Please try another"));
        }
        /* validate username */
        if (is_empty($username)) {
            throw new Exception(__("You must enter a username for your page"));
        }
        if (!valid_username($username)) {
            throw new Exception(__("Please enter a valid username (a-z0-9_.) with minimum 3 characters long"));
        }
        if (reserved_username($username)) {
            throw new Exception(__("You can't use") . " <strong>" . $username . "</strong> " . __("as username"));
        }
        if ($this->check_username($username, 'page')) {
            throw new Exception(__("Sorry, it looks like this username") . " <strong>" . $username . "</strong> " . __("belongs to an existing page"));
        }
        /* validate category */
        if (is_empty($category)) {
            throw new Exception(__("You must select valid category for your page"));
        }
        $check = $db->query(sprintf("SELECT * FROM pages_categories WHERE category_id = %s", secure($category, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($check->num_rows == 0) {
            throw new Exception(__("You must select valid category for your page"));
        }
        /* insert new page */
        $db->query(sprintf("INSERT INTO pages (page_admin, page_category, page_name, page_title, page_description, page_date) VALUES (%s, %s, %s, %s, %s, %s)", secure($this->_data['user_id'], 'int'), secure($category, 'int'), secure($username), secure($title), secure($description), secure($date))) or _error(SQL_ERROR_THROWEN);
        /* join the group */
        $this->connect("page-like", $db->insert_id);
    }

    public function edit_page($page_id, $title, $username, $category, $description, $verified = null)
    {
        global $db, $system;
        /* check if pages enabled */
        if ($this->_data['user_group'] >= 3 && !$system['pages_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* (check&get) page */
        $get_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($page_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_page->num_rows == 0) {
            _error(403);
        }
        $page = $get_page->fetch_assoc();
        /* check permission */
        $can_edit = false;
        /* viewer is (admin|moderator) */
        if ($this->_data['user_group'] < 3) {
            $can_edit = true;
            /* viewer is the admin of page */
        } elseif ($this->_data['user_id'] == $page['page_admin']) {
            $can_edit = true;
        }
        if (!$can_edit) {
            _error(403);
        }
        /* validate title */
        if (is_empty($title)) {
            throw new Exception(__("You must enter a title for your page"));
        }
        if (strlen($title) < 3) {
            throw new Exception(__("Page title must be at least 3 characters long. Please try another"));
        }
        /* validate username */
        if (strtolower($username) != strtolower($page['page_name'])) {
            if (is_empty($username)) {
                throw new Exception(__("You must enter a username for your page"));
            }
            if (!valid_username($username)) {
                throw new Exception(__("Please enter a valid username (a-z0-9_.) with minimum 3 characters long"));
            }
            if (reserved_username($username)) {
                throw new Exception(__("You can't use") . " <strong>" . $username . "</strong> " . __("as username"));
            }
            if ($this->check_username($username, 'page')) {
                throw new Exception(__("Sorry, it looks like this username") . " <strong>" . $username . "</strong> " . __("belongs to an existing page"));
            }
        }
        /* validate category */
        if (is_empty($category)) {
            throw new Exception(__("You must select valid category for your page"));
        }
        $check = $db->query(sprintf("SELECT * FROM pages_categories WHERE category_id = %s", secure($category, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($check->num_rows == 0) {
            throw new Exception(__("You must select valid category for your page"));
        }
        /* check validation */
        $verified = ($verified == null) ? $page['page_verified'] : $verified;
        /* update the page */
        $db->query(sprintf("UPDATE pages SET page_category = %s, page_name = %s, page_title = %s, page_description = %s, page_verified = %s WHERE page_id = %s", secure($category, 'int'), secure($username), secure($title), secure($description), secure($verified), secure($page_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function delete_page($page_id)
    {
        global $db, $system;
        /* check if pages enabled */
        if ($this->_data['user_group'] >= 3 && !$system['pages_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* (check&get) page */
        $get_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($page_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_page->num_rows == 0) {
            _error(403);
        }
        $page = $get_page->fetch_assoc();
        // delete page
        $can_delete = false;
        /* viewer is (admin|moderator) */
        if ($this->_data['user_group'] < 3) {
            $can_delete = true;
        }
        /* viewer is the admin of page */
        if ($this->_data['user_id'] == $page['page_admin']) {
            $can_delete = true;
        }
        /* delete the page */
        if ($can_delete) {
            /* delete the page */
            $db->query(sprintf("DELETE FROM pages WHERE page_id = %s", secure($page_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        }
    }

    public function get_page_invites($page_id, $offset = 0)
    {
        global $db, $system;
        $friends = [];
        $offset *= $system['max_results_even'];
        if ($this->_data['friends_ids']) {
            /* get page members */
            $members = [];
            $get_members = $db->query(sprintf("SELECT user_id FROM pages_likes WHERE page_id = %s", secure($page_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_members->num_rows > 0) {
                while ($member = $get_members->fetch_assoc()) {
                    $members[] = $member['user_id'];
                }
            }
            /*  */
            $potential_invites = array_diff($this->_data['friends_ids'], $members);
            if ($potential_invites) {
                /* get already invited friends */
                $invited_friends = [];
                $get_invited_friends = $db->query(sprintf("SELECT user_id FROM pages_invites WHERE page_id = %s AND from_user_id = %s", secure($page_id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_invited_friends->num_rows > 0) {
                    while ($invited_friend = $get_invited_friends->fetch_assoc()) {
                        $invited_friends[] = $invited_friend['user_id'];
                    }
                }
                $invites_list = implode(',', array_diff($potential_invites, $invited_friends));
                if ($invites_list) {
                    $get_friends = $db->query(sprintf("SELECT user_id, user_name, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_id IN ($invites_list) LIMIT %s, %s", secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                    while ($friend = $get_friends->fetch_assoc()) {
                        $friend['user_picture'] = $this->get_picture($friend['user_picture'], $friend['user_gender']);
                        $friend['connection'] = 'page_invite';
                        $friend['node_id'] = $page_id;
                        $friends[] = $friend;
                    }
                }
            }
        }
        return $friends;
    }

    public function get_groups($args = [])
    {
        global $db, $system;
        /* initialize arguments */
        $user_id = !isset($args['user_id']) ? null : $args['user_id'];
        $offset = !isset($args['offset']) ? 0 : $args['offset'];
        $suggested = !isset($args['suggested']) ? false : true;
        $random = !isset($args['random']) ? false : true;
        $results = !isset($args['results']) ? $system['max_results_even'] : $args['results'];
        /* initialize vars */
        $groups = [];
        $offset *= $results;
        /* get suggested groups */
        if ($suggested) {
            $where_statement = "";
            /* make a list from joined groups (approved|pending) */
            $groups_ids = $this->get_groups_ids();
            if ($groups_ids) {
                $groups_list = implode(',', $groups_ids);
                $where_statement .= "AND group_id NOT IN (" . $groups_list . ") ";
            }
            $sort_statement = ($random) ? "ORDER BY RAND()" : "ORDER BY group_id DESC";
            $get_groups = $db->query(sprintf("SELECT * FROM groups WHERE group_privacy != 'secret' " . $where_statement . $sort_statement . " LIMIT %s, %s", secure($offset, 'int', false), secure($results, 'int', false))) or _error(SQL_ERROR_THROWEN);
            /* get the "viewer" groups who admin */
        } elseif ($user_id == null) {
            $get_groups = $db->query(sprintf("SELECT * FROM groups WHERE group_admin = %s ORDER BY group_id DESC LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($results, 'int', false))) or _error(SQL_ERROR_THROWEN);
            /* get the "target" groups*/
        } else {
            /* get the target user's privacy */
            $get_privacy = $db->query(sprintf("SELECT user_privacy_groups FROM users WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            $privacy = $get_privacy->fetch_assoc();
            /* check the target user's privacy  */
            if (!$this->_check_privacy($privacy['user_privacy_groups'], $user_id)) {
                return $groups;
            }
            /* if the viewer not the target exclude secret groups */
            $where_statement = ($this->_data['user_id'] == $user_id) ? "" : "AND groups.group_privacy != 'secret'";
            $get_groups = $db->query(sprintf("SELECT groups.* FROM groups INNER JOIN groups_members ON groups.group_id = groups_members.group_id WHERE groups_members.approved = '1' And groups_members.user_id = %s " . $where_statement . " ORDER BY group_id DESC LIMIT %s, %s", secure($user_id, 'int'), secure($offset, 'int', false), secure($results, 'int', false))) or _error(SQL_ERROR_THROWEN);
        }
        if ($get_groups->num_rows > 0) {
            while ($group = $get_groups->fetch_assoc()) {
                $group['group_picture'] = $this->get_picture($group['group_picture'], 'group');
                /* check if the viewer joined the group */
                $group['i_joined'] = $this->check_group_membership($this->_data['user_id'], $group['group_id']);;
                $groups[] = $group;
            }
        }
        return $groups;
    }

    public function create_group($title, $username, $privacy, $description)
    {
        global $db, $system, $date;
        /* check if groups enabled */
        if ($this->_data['user_group'] >= 3 && !$system['groups_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* validate title */
        if (is_empty($title)) {
            throw new Exception(__("You must enter a name for your group"));
        }
        if (strlen($title) < 3) {
            throw new Exception(__("Group name must be at least 3 characters long. Please try another"));
        }
        /* validate username */
        if (is_empty($username)) {
            throw new Exception(__("You must enter a web address for your group"));
        }
        if (!valid_username($username)) {
            throw new Exception(__("Please enter a valid web address (a-z0-9_.) with minimum 3 characters long"));
        }
        if (reserved_username($username)) {
            throw new Exception(__("You can't use") . " <strong>" . $username . "</strong> " . __("as web address"));
        }
        if ($this->check_username($username, 'group')) {
            throw new Exception(__("Sorry, it looks like this web address") . " <strong>" . $username . "</strong> " . __("belongs to an existing group"));
        }
        /* validate privacy */
        if (!in_array($privacy, ['secret', 'closed', 'public'])) {
            throw new Exception(__("You must select a valid privacy for your group"));
        }
        /* insert new group */
        $db->query(sprintf("INSERT INTO groups (group_privacy, group_admin, group_name, group_title, group_description, group_date) VALUES (%s, %s, %s, %s, %s, %s)", secure($privacy), secure($this->_data['user_id'], 'int'), secure($username), secure($title), secure($description), secure($date))) or _error(SQL_ERROR_THROWEN);
        /* join the group */
        $this->connect("group-join", $db->insert_id);
    }

    public function edit_group($group_id, $title, $username, $privacy, $description)
    {
        global $db, $system;
        /* check if groups enabled */
        if ($this->_data['user_group'] >= 3 && !$system['groups_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* (check&get) group */
        $get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($group_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_group->num_rows == 0) {
            _error(403);
        }
        $group = $get_group->fetch_assoc();
        /* check permission */
        $can_edit = false;
        /* viewer is (admin|moderator) */
        if ($this->_data['user_group'] < 3) {
            $can_edit = true;
            /* viewer is the admin of group */
        } elseif ($this->_data['user_id'] == $group['group_admin']) {
            $can_edit = true;
        }
        if (!$can_edit) {
            _error(403);
        }
        /* validate title */
        if (is_empty($title)) {
            throw new Exception(__("You must enter a name for your group"));
        }
        if (strlen($title) < 3) {
            throw new Exception(__("Group name must be at least 3 characters long. Please try another"));
        }
        /* validate username */
        if (strtolower($username) != strtolower($group['group_name'])) {
            if (is_empty($username)) {
                throw new Exception(__("You must enter a web address for your group"));
            }
            if (!valid_username($username)) {
                throw new Exception(__("Please enter a valid web address (a-z0-9_.) with minimum 3 characters long"));
            }
            if (reserved_username($username)) {
                throw new Exception(__("You can't use") . " <strong>" . $username . "</strong> " . __("as web address"));
            }
            if ($this->check_username($username, 'group')) {
                throw new Exception(__("Sorry, it looks like this web address") . " <strong>" . $username . "</strong> " . __("belongs to an existing group"));
            }
        }
        /* validate privacy */
        if (!in_array($privacy, ['secret', 'closed', 'public'])) {
            throw new Exception(__("You must select a valid privacy for your group"));
        }
        /* update the group */
        $db->query(sprintf("UPDATE groups SET group_privacy = %s, group_name = %s, group_title = %s, group_description = %s WHERE group_id = %s", secure($privacy), secure($username), secure($title), secure($description), secure($group_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function delete_group($group_id)
    {
        global $db, $system;
        /* check if groups enabled */
        if ($this->_data['user_group'] >= 3 && !$system['groups_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* (check&get) group */
        $get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($group_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_group->num_rows == 0) {
            _error(403);
        }
        $group = $get_group->fetch_assoc();
        // delete group
        $can_delete = false;
        /* viewer is (admin|moderator) */
        if ($this->_data['user_group'] < 3) {
            $can_delete = true;
            /* viewer is the admin of group */
        } elseif ($this->_data['user_id'] == $group['group_admin']) {
            $can_delete = true;
        }
        /* delete the group */
        if (!$can_delete) {
            _error(403);
        }
        $db->query(sprintf("DELETE FROM groups WHERE group_id = %s", secure($group_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function get_group_members($group_id, $offset = 0)
    {
        global $db, $system;
        $members = [];
        $offset *= $system['max_results_even'];
        $get_members = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM groups_members INNER JOIN users ON groups_members.user_id = users.user_id WHERE  groups_members.approved = '1' AND groups_members.group_id = %s LIMIT %s, %s", secure($group_id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_members->num_rows > 0) {
            while ($member = $get_members->fetch_assoc()) {
                $member['user_picture'] = $this->get_picture($member['user_picture'], $member['user_gender']);
                /* get the connection between the viewer & the target */
                $member['connection'] = $this->connection($member['user_id']);
                $members[] = $member;
            }
        }
        return $members;
    }

    public function get_group_invites($group_id, $offset = 0)
    {
        global $db, $system;
        $friends = [];
        $offset *= $system['max_results_even'];
        if ($this->_data['friends_ids']) {
            $get_members = $db->query(sprintf("SELECT user_id FROM groups_members WHERE group_id = %s", secure($group_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_members->num_rows > 0) {
                while ($member = $get_members->fetch_assoc()) {
                    $members[] = $member['user_id'];
                }
                $invites_list = implode(',', array_diff($this->_data['friends_ids'], $members));
                if ($invites_list) {
                    $get_friends = $db->query(sprintf("SELECT user_id, user_name, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_id IN ($invites_list) LIMIT %s, %s", secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                    while ($friend = $get_friends->fetch_assoc()) {
                        $friend['user_picture'] = $this->get_picture($friend['user_picture'], $friend['user_gender']);
                        $friend['connection'] = 'group_invite';
                        $friend['node_id'] = $group_id;
                        $friends[] = $friend;
                    }
                }
            }
        }
        return $friends;
    }

    public function get_group_requests($group_id, $offset = 0)
    {
        global $db, $system;
        $requests = [];
        $offset *= $system['max_results'];
        $get_requests = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM users INNER JOIN groups_members ON users.user_id = groups_members.user_id WHERE groups_members.approved = '0' AND groups_members.group_id = %s LIMIT %s, %s", secure($group_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_requests->num_rows > 0) {
            while ($request = $get_requests->fetch_assoc()) {
                $request['user_picture'] = $this->get_picture($request['user_picture'], $request['user_gender']);
                $request['connection'] = 'group_request';
                $request['node_id'] = $group_id;
                $requests[] = $request;
            }
        }
        return $requests;
    }

    public function get_group_requests_total($group_id)
    {
        global $db, $system;
        $get_requests = $db->query(sprintf("SELECT  * FROM groups_members WHERE approved = '0' AND group_id = %s", secure($group_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        return $get_requests->num_rows;
    }

    public function check_group_membership($user_id, $group_id)
    {
        global $db;
        if ($this->_logged_in) {
            $get_membership = $db->query(sprintf("SELECT * FROM groups_members WHERE user_id = %s AND group_id = %s", secure($user_id, 'int'), secure($group_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_membership->num_rows > 0) {
                $membership = $get_membership->fetch_assoc();
                return ($membership['approved'] == '1') ? "approved" : "pending";
            }
        }
        return false;
    }

    public function get_events($args = [])
    {
        global $db, $system;
        /* initialize arguments */
        $user_id = !isset($args['user_id']) ? null : $args['user_id'];
        $offset = !isset($args['offset']) ? 0 : $args['offset'];
        $suggested = !isset($args['suggested']) ? false : true;
        $random = !isset($args['random']) ? false : true;
        $filter = !isset($args['filter']) ? "admin" : $args['filter'];
        $results = !isset($args['results']) ? $system['max_results_even'] : $args['results'];
        /* initialize vars */
        $events = [];
        $offset *= $results;
        /* get suggested events */
        if ($suggested) {
            $where_statement = "";
            /* make a list from joined events */
            $events_ids = $this->get_events_ids();
            if ($events_ids) {
                $events_list = implode(',', $events_ids);
                $where_statement .= "AND event_id NOT IN (" . $events_list . ") ";
            }
            $sort_statement = ($random) ? "ORDER BY RAND()" : "ORDER BY event_id DESC";
            $get_events = $db->query(sprintf("SELECT * FROM events WHERE event_privacy != 'secret' " . $where_statement . $sort_statement . " LIMIT %s, %s", secure($offset, 'int', false), secure($results, 'int', false))) or _error(SQL_ERROR_THROWEN);
            /* get the "viewer" events who admin */
        } elseif ($user_id == null) {
            switch ($filter) {
                case 'going':
                    $get_events = $db->query(sprintf("SELECT events.* FROM events INNER JOIN events_members ON events.event_id = events_members.event_id WHERE events_members.is_going = '1' And events_members.user_id = %s ORDER BY event_id DESC LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($results, 'int', false))) or _error(SQL_ERROR_THROWEN);
                    break;
                case 'interested':
                    $get_events = $db->query(sprintf("SELECT events.* FROM events INNER JOIN events_members ON events.event_id = events_members.event_id WHERE events_members.is_interested = '1' And events_members.user_id = %s ORDER BY event_id DESC LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($results, 'int', false))) or _error(SQL_ERROR_THROWEN);
                    break;
                case 'invited':
                    $get_events = $db->query(sprintf("SELECT events.* FROM events INNER JOIN events_members ON events.event_id = events_members.event_id WHERE events_members.is_invited = '1' And events_members.user_id = %s ORDER BY event_id DESC LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($results, 'int', false))) or _error(SQL_ERROR_THROWEN);
                    break;
                default:
                    $get_events = $db->query(sprintf("SELECT * FROM events WHERE event_admin = %s ORDER BY event_id DESC LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($results, 'int', false))) or _error(SQL_ERROR_THROWEN);
                    break;
            }
            /* get the "target" events */
        } else {
            /* get the target user's privacy */
            $get_privacy = $db->query(sprintf("SELECT user_privacy_events FROM users WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            $privacy = $get_privacy->fetch_assoc();
            /* check the target user's privacy  */
            if (!$this->_check_privacy($privacy['user_privacy_events'], $user_id)) {
                return $events;
                exit;
            }
            /* if the viewer not the target exclude secret groups */
            $where_statement = ($this->_data['user_id'] == $user_id) ? "" : "AND events.event_privacy != 'secret'";
            $get_events = $db->query(sprintf("SELECT events.* FROM events INNER JOIN events_members ON events.event_id = events_members.event_id WHERE (events_members.is_going = '1' OR events_members.is_interested = '1') AND events_members.user_id = %s " . $where_statement . " ORDER BY event_id DESC LIMIT %s, %s", secure($user_id, 'int'), secure($offset, 'int', false), secure($results, 'int', false))) or _error(SQL_ERROR_THROWEN);
        }
        if ($get_events->num_rows > 0) {
            while ($event = $get_events->fetch_assoc()) {
                $event['event_picture'] = $this->get_picture($event['event_cover'], 'event');
                /* check if the viewer joined the event */
                $event['i_joined'] = $this->check_event_membership($this->_data['user_id'], $event['event_id']);;
                $events[] = $event;
            }
        }
        return $events;
    }

    public function get_events_categories()
    {
        global $db;
        $categories = [];
        $get_categories = $db->query("SELECT * FROM events_categories") or _error(SQL_ERROR_THROWEN);
        if ($get_categories->num_rows > 0) {
            while ($category = $get_categories->fetch_assoc()) {
                $categories[] = $category;
            }
        }
        return $categories;
    }

    public function create_event($title, $location, $start_date, $end_date, $privacy, $category, $description, $lat, $long)
    {
        global $db, $system, $date;
        /* check if events enabled */
        if ($this->_data['user_group'] >= 3 && !$system['events_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* validate title */
        if (is_empty($title)) {
            throw new Exception(__("You must enter a name for your event"));
        }
        if (strlen($title) < 3) {
            throw new Exception(__("Event name must be at least 3 characters long. Please try another"));
        }
        /* validate start & end dates */
        if (is_empty($start_date)) {
            throw new Exception(__("You have to enter the event start date"));
        }
        if (is_empty($end_date)) {
            throw new Exception(__("You have to enter the event end date"));
        }
        if (strtotime($start_date) > strtotime($end_date)) {
            throw new Exception(__("Event end date must be after the start date"));
        }
        /* validate privacy */
        if (!in_array($privacy, ['secret', 'closed', 'public'])) {
            throw new Exception(__("You must select a valid privacy for your event"));
        }
        /* validate category */
        if (is_empty($category)) {
            throw new Exception(__("You must select valid category for your event"));
        }
        $check = $db->query(sprintf("SELECT * FROM events_categories WHERE category_id = %s", secure($category, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($check->num_rows == 0) {
            throw new Exception(__("You must select valid category for your event"));
        }
        /* insert new group */
        $db->query(sprintf("INSERT INTO events (event_privacy, event_admin, event_category, event_title, event_location,  event_location_lat, event_location_long, event_description, event_start_date, event_end_date, event_date) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s)", secure($privacy), secure($this->_data['user_id'], 'int'), secure($category, 'int'), secure($title), secure($location), secure($lat), secure($long), secure($description), secure($start_date, 'datetime'), secure($end_date, 'datetime'), secure($date))) or _error(SQL_ERROR_THROWEN);
        $event_id = $db->insert_id;
        /* join the group */
        $this->connect("event-interest", $event_id);
        /* return event id */
        return $event_id;
    }

    public function edit_event($event_id, $title, $location, $start_date, $end_date, $privacy, $category, $description, $lat, $long)
    {
        global $db, $system;
        /* check if events enabled */
        if ($this->_data['user_group'] >= 3 && !$system['events_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* (check&get) event */
        $get_event = $db->query(sprintf("SELECT * FROM events WHERE event_id = %s", secure($event_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_event->num_rows == 0) {
            _error(403);
        }
        $event = $get_event->fetch_assoc();
        /* check permission */
        $can_edit = false;
        /* viewer is (admin|moderator) */
        if ($this->_data['user_group'] < 3) {
            $can_edit = true;
            /* viewer is the admin of event */
        } elseif ($this->_data['user_id'] == $event['event_admin']) {
            $can_edit = true;
        }
        if (!$can_edit) {
            _error(403);
        }
        /* validate title */
        if (is_empty($title)) {
            throw new Exception(__("You must enter a name for your event"));
        }
        if (strlen($title) < 3) {
            throw new Exception(__("Event name must be at least 3 characters long. Please try another"));
        }
        /* validate start & end dates */
        if (is_empty($start_date)) {
            throw new Exception(__("You have to enter the event start date"));
        }
        if (is_empty($end_date)) {
            throw new Exception(__("You have to enter the event end date"));
        }
        if (strtotime($start_date) > strtotime($end_date)) {
            throw new Exception(__("Event end date must be after the start date"));
        }
        /* validate privacy */
        if (!in_array($privacy, ['secret', 'closed', 'public'])) {
            throw new Exception(__("You must select a valid privacy for your event"));
        }
        /* validate category */
        if (is_empty($category)) {
            throw new Exception(__("You must select valid category for your event"));
        }
        $check = $db->query(sprintf("SELECT * FROM events_categories WHERE category_id = %s", secure($category, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($check->num_rows == 0) {
            throw new Exception(__("You must select valid category for your event"));
        }
        /* update the event */
        $db->query(sprintf("UPDATE events SET event_privacy = %s, event_category = %s, event_title = %s, event_location = %s, event_location_lat = %s,event_location_long = %s,event_description = %s, event_start_date = %s, event_end_date = %s WHERE event_id = %s", secure($privacy), secure($category, 'int'), secure($title), secure($location), secure($lat), secure($long), secure($description), secure($start_date, 'datetime'), secure($end_date, 'datetime'), secure($event_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function delete_event($event_id)
    {
        global $db, $system;
        /* check if events enabled */
        if ($this->_data['user_group'] >= 3 && !$system['events_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* (check&get) event */
        $get_event = $db->query(sprintf("SELECT * FROM events WHERE event_id = %s", secure($event_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_event->num_rows == 0) {
            _error(403);
        }
        $event = $get_event->fetch_assoc();
        // delete event
        $can_delete = false;
        /* viewer is (admin|moderator) */
        if ($this->_data['user_group'] < 3) {
            $can_delete = true;
            /* viewer is the admin of event */
        } elseif ($this->_data['user_id'] == $event['event_admin']) {
            $can_delete = true;
        }
        /* delete the event */
        if (!$can_delete) {
            _error(403);
        }
        $db->query(sprintf("DELETE FROM events WHERE event_id = %s", secure($event_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function get_event_members($event_id, $handle, $offset = 0)
    {
        global $db, $system;
        $members = [];
        $offset *= $system['max_results_even'];
        switch ($handle) {
            case 'going':
                $get_members = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM events_members INNER JOIN users ON events_members.user_id = users.user_id WHERE  events_members.is_going = '1' AND events_members.event_id = %s LIMIT %s, %s", secure($event_id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'interested':
                $get_members = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM events_members INNER JOIN users ON events_members.user_id = users.user_id WHERE  events_members.is_interested = '1' AND events_members.event_id = %s LIMIT %s, %s", secure($event_id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'invited':
                $get_members = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM events_members INNER JOIN users ON events_members.user_id = users.user_id WHERE  events_members.is_invited = '1' AND events_members.event_id = %s LIMIT %s, %s", secure($event_id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                break;
            default:
                _error(400);
                break;
        }
        if ($get_members->num_rows > 0) {
            while ($member = $get_members->fetch_assoc()) {
                $member['user_picture'] = $this->get_picture($member['user_picture'], $member['user_gender']);
                /* get the connection between the viewer & the target */
                $member['connection'] = $this->connection($member['user_id']);
                $members[] = $member;
            }
        }
        return $members;
    }

    public function get_event_invites($event_id, $offset = 0)
    {
        global $db, $system;
        $friends = [];
        $offset *= $system['max_results_even'];
        if ($this->_data['friends_ids']) {
            $get_members = $db->query(sprintf("SELECT user_id FROM events_members WHERE event_id = %s", secure($event_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_members->num_rows > 0) {
                while ($member = $get_members->fetch_assoc()) {
                    $members[] = $member['user_id'];
                }
                $invites_list = implode(',', array_diff($this->_data['friends_ids'], $members));
                if ($invites_list) {
                    $get_friends = $db->query(sprintf("SELECT user_id, user_name, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_id IN ($invites_list) LIMIT %s, %s", secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
                    while ($friend = $get_friends->fetch_assoc()) {
                        $friend['user_picture'] = $this->get_picture($friend['user_picture'], $friend['user_gender']);
                        $friend['connection'] = 'event_invite';
                        $friend['node_id'] = $event_id;
                        $friends[] = $friend;
                    }
                }
            }
        }
        return $friends;
    }

    public function check_event_membership($user_id, $event_id)
    {
        global $db;
        if ($this->_logged_in) {
            $get_membership = $db->query(sprintf("SELECT is_invited, is_interested, is_going FROM events_members WHERE user_id = %s AND event_id = %s", secure($user_id, 'int'), secure($event_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_membership->num_rows > 0) {
                return $get_membership->fetch_assoc();
            }
        }
        return false;
    }

    public function report($id, $handle, $reason = '')
    {
        global $db, $date;
        switch ($handle) {
            case 'user':
                /* check the user */
                $check = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'page':
                /* check the page */
                $check = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'group':
                /* check the group */
                $check = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'event':
                /* check the event */
                $check = $db->query(sprintf("SELECT * FROM events WHERE event_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'post':
                /* check the post */
                $check = $db->query(sprintf("SELECT * FROM posts WHERE post_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'comment':
                /* check the comment */
                $check = $db->query(sprintf("SELECT * FROM posts_comments WHERE comment_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            default:
                _error(403);
                break;
        }
        /* check node */
        if ($check->num_rows == 0) {
            _error(403);
        }
        /* check old reports */
        $check = $db->query(sprintf("SELECT * FROM reports WHERE user_id = %s AND node_id = %s AND node_type = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'), secure($handle))) or _error(SQL_ERROR_THROWEN);
        if ($check->num_rows > 0) {
            throw new Exception(__("You have already reported this before!"));
        }
        /* report */
        $db->query(sprintf("INSERT INTO reports (user_id, node_id, node_type, time, reason) VALUES (%s, %s, %s, %s, %s)", secure($this->_data['user_id'], 'int'), secure($id, 'int'), secure($handle), secure($date), secure($reason))) or _error(SQL_ERROR_THROWEN);
    }

    public function get_games($offset = 0, $played = false)
    {
        global $db, $system;
        $games = [];
        $offset *= $system['max_results_even'];
        if ($played) {
            $get_games = $db->query(sprintf('SELECT games.* FROM games INNER JOIN game_players ON games.game_id = game_players.game_id WHERE game_players.user_id = %s LIMIT %s, %s', secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_games = $db->query(sprintf('SELECT * FROM games LIMIT %s, %s', secure($offset, 'int', false), secure($system['max_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        }
        if ($get_games->num_rows > 0) {
            while ($game = $get_games->fetch_assoc()) {
                $game['thumbnail'] = $this->get_picture($game['thumbnail'], 'game');
                $games[] = $game;
            }
        }
        return $games;
    }

    public function get_game($game_id)
    {
        global $db;
        $get_game = $db->query(sprintf('SELECT * FROM games WHERE game_id = %s', secure($game_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_game->num_rows == 0) {
            return false;
        }
        $game = $get_game->fetch_assoc();
        $game['thumbnail'] = $this->get_picture($game['thumbnail'], 'game');
        /* check if the viewer played this game before */
        if ($this->_logged_in) {
            $db->query(sprintf("INSERT INTO game_players (game_id, user_id) VALUES (%s, %s)", secure($game['game_id'], 'int'), secure($this->_data['user_id'], 'int')));
        }
        return $game;
    }

    public function get_market_categories_ids()
    {
        global $db;
        $categories = [];
        $get_categories = $db->query("SELECT category_id FROM market_categories") or _error(SQL_ERROR_THROWEN);
        if ($get_categories->num_rows > 0) {
            while ($category = $get_categories->fetch_assoc()) {
                $categories[] = $category['category_id'];
            }
        }
        return $categories;
    }

    public function get_market_categories()
    {
        global $db;
        $categories = [];
        $get_categories = $db->query("SELECT * FROM market_categories") or _error(SQL_ERROR_THROWEN);
        if ($get_categories->num_rows > 0) {
            while ($category = $get_categories->fetch_assoc()) {
                $category['category_url'] = get_url_text($category['category_name']);
                $categories[] = $category;
            }
        }
        return $categories;
    }

    public function get_packages()
    {
        global $db;
        $packages = [];
        $get_packages = $db->query("SELECT * FROM packages") or _error(SQL_ERROR_THROWEN);
        if ($get_packages->num_rows > 0) {
            while ($package = $get_packages->fetch_assoc()) {
                $package['icon'] = $this->get_picture($package['icon'], 'package');
                $packages[] = $package;
            }
        }
        return $packages;
    }

    public function get_package($package_id)
    {
        global $db;
        $get_package = $db->query(sprintf('SELECT * FROM packages WHERE package_id = %s', secure($package_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_package->num_rows == 0) {
            return false;
        }
        $package = $get_package->fetch_assoc();
        return $package;
    }

    public function check_user_package()
    {
        global $db;
        if ($this->_data['user_subscribed']) {
            $package = $this->get_package($this->_data['user_package']);
            if ($package) {
                switch ($package['period']) {
                    case 'day':
                        $duration = 86400;
                        break;
                    case 'week':
                        $duration = 604800;
                        break;
                    case 'month':
                        $duration = 2629743;
                        break;
                    case 'year':
                        $duration = 31556926;
                        break;
                    case 'life':
                        return;
                        break;
                }
                $time_left = time() - ($package['period_num'] * $duration);
                if (strtotime($this->_data['user_subscription_date']) > $time_left) {
                    return;
                }
            }
            // remove user package
            $db->query(sprintf("UPDATE users SET user_subscribed = '0', user_package = null, user_subscription_date = null, user_boosted_posts = '0', user_boosted_pages = '0' WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }
    }

    public function update_user_package($package_id, $package_name, $package_price)
    {
        global $db, $date;
        /* update user package */
        $db->query(sprintf("UPDATE users SET user_verified = '1', user_subscribed = '1', user_package = %s, user_subscription_date = %s, user_boosted_posts = '0', user_boosted_pages = '0' WHERE user_id = %s", secure($package_id, 'int'), secure($date), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        /* insert the payment */
        $db->query(sprintf("INSERT INTO packages_payments (payment_date, package_name, package_price) VALUES (%s, %s, %s)", secure($date), secure($package_name), secure($package_price))) or _error(SQL_ERROR_THROWEN);
        /* affiliates system */
        $this->process_affiliates("packages", $this->_data['user_id'], $this->_data['user_referrer_id']);
    }

    public function init_affiliates()
    {
        if (!$this->_logged_in && isset($_GET['ref'])) {
            $expire = time() + 2592000;
            setcookie($this->_cookie_user_referrer, $_GET['ref'], $expire, '/');
        }
    }

    private function process_affiliates($type, $referee_id, $referrer_id = null)
    {
        global $db, $system;
        if (!$system['affiliates_enabled'] || $system['affiliate_type'] != $type || !isset($_COOKIE[$this->_cookie_user_referrer])) {
            return;
        }
        $get_referrer = $db->query(sprintf("SELECT user_id, user_name, user_affiliate_balance FROM users WHERE user_name = %s", secure($_COOKIE[$this->_cookie_user_referrer]))) or _error(SQL_ERROR_THROWEN);
        if ($get_referrer->num_rows == 0) {
            return;
        }
        $referrer = $get_referrer->fetch_assoc();
        if ($referrer['user_id'] == $referee_id || $referrer['user_id'] == $referrer_id) {
            return;
        }
        /* update referrer balance */
        $balance = $referrer['user_affiliate_balance'] + $system['affiliates_per_user'];
        $db->query(sprintf("UPDATE users SET user_affiliate_balance = %s WHERE user_id = %s", secure($balance), secure($referrer['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        /* update referee */
        $db->query(sprintf("UPDATE users SET user_referrer_id = %s WHERE user_id = %s", secure($referrer['user_id'], 'int'), secure($referee_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function get_affiliates($user_id, $offset = 0)
    {
        global $db, $system;
        $affiliates = [];
        $offset *= $system['max_results'];
        $get_affiliates = $db->query(sprintf('SELECT user_id, user_name, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_referrer_id = %s LIMIT %s, %s', secure($user_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_affiliates->num_rows > 0) {
            while ($affiliate = $get_affiliates->fetch_assoc()) {
                $affiliate['user_picture'] = $this->get_picture($affiliate['user_picture'], $affiliate['user_gender']);
                /* get the connection between the viewer & the target */
                $affiliate['connection'] = $this->connection($affiliate['user_id'], false);
                $affiliates[] = $affiliate;
            }
        }
        return $affiliates;
    }

    public function get_custom_fields($args = [])
    {
        global $db, $system;
        $fields = [];
        $where_query = "";
        $args['get'] = (isset($args['get'])) ? $args['get'] : "registration";
        if ($args['get'] == "registration") {
            $where_query = "WHERE in_registration = '1'";
        } elseif ($args['get'] == "profile") {
            $where_query = "WHERE in_profile = '1'";
        }
        $get_fields = $db->query("SELECT * FROM custom_fields " . $where_query) or _error(SQL_ERROR_THROWEN);
        if ($get_fields->num_rows > 0) {
            while ($field = $get_fields->fetch_assoc()) {
                if ($field['type'] == "selectbox") {
                    $field['options'] = explode(PHP_EOL, $field['select_options']);
                }
                if ($args['get'] == "registration") {
                    $fields[] = $field;
                } else {
                    /* prepare user info */
                    $args['user_id'] = (isset($args['user_id'])) ? $args['user_id'] : $this->_data['user_id'];
                    /* get field value */
                    $get_field_value = $db->query(sprintf("SELECT value FROM users_custom_fields WHERE user_id = %s AND field_id = %s", secure($args['user_id'], 'int'), secure($field['field_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    $field_value = $get_field_value->fetch_assoc();
                    if ($field['type'] == "selectbox") {
                        $field['value'] = $field['options'][$field_value['value']];
                    } else {
                        $field['value'] = $field_value['value'];
                    }
                    if ($args['get'] == "profile" && is_empty($field['value'])) {
                        continue;
                    }
                    $fields[$field['place']][] = $field;
                }
            }
        }
        return $fields;
    }

    public function set_custom_fields($args, $is_registration = true)
    {
        global $db, $system;
        $fields = [];
        $where_query = ($is_registration) ? " AND in_registration = '1'" : "";
        $prefix = "fld_";
        $prefix_len = strlen($prefix);
        foreach ($args as $key => $value) {
            if (strpos($key, $prefix) === false) {
                continue;
            }
            $field_id = substr($key, $prefix_len);
            $get_field = $db->query(sprintf("SELECT * FROM custom_fields WHERE field_id = %s" . $where_query, secure($field_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_field->num_rows == 0) {
                continue;
            }
            $field = $get_field->fetch_assoc();
            /* valid field */
            if ($field['mandatory']) {
                if ($field['type'] != "selectbox" && is_empty($value)) {
                    throw new Exception(__("You must enter") . " " . $field['label']);
                }
                if ($field['type'] == "selectbox" && $value == "none") {
                    throw new Exception(__("You must select") . " " . $field['label']);
                }
            }
            if (strlen($value) > $field['length']) {
                throw new Exception(__("The maximum value for") . " " . $field['label'] . " " . __("is") . " " . $field['length']);
            }
            /* (insert|update) users custom fields */
            if ($is_registration) {
                /* insert query */
                $fields[$field['field_id']] = $value;
            } else {
                $insert_field = $db->query(sprintf("INSERT INTO users_custom_fields (user_id, field_id, value) VALUES (%s, %s, %s)", secure($this->_data['user_id'], 'int'), secure($field_id, 'int'), secure($value)));
                if (!$insert_field) {
                    /* update if already exist */
                    $db->query(sprintf("UPDATE users_custom_fields SET value = %s WHERE user_id = %s AND field_id = %s", secure($value), secure($this->_data['user_id'], 'int'), secure($field_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
            }
        }
        if ($is_registration) {
            return $fields;
        }
        return;
    }

    public function announcements()
    {
        global $db, $date;
        $announcements = [];
        $get_announcement = $db->query(sprintf('SELECT * FROM announcements WHERE start_date <= %1$s AND end_date >= %1$s', secure($date))) or _error(SQL_ERROR_THROWEN);
        if ($get_announcement->num_rows > 0) {
            while ($announcement = $get_announcement->fetch_assoc()) {
                $check = $db->query(sprintf("SELECT * FROM announcements_users WHERE announcement_id = %s AND user_id = %s", secure($announcement['announcement_id'], 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($check->num_rows == 0) {
                    $announcement['code'] = html_entity_decode($announcement['code'], ENT_QUOTES);
                    $announcements[] = $announcement;
                }
            }
        }
        return $announcements;
    }

    public function hide_announcement($id)
    {
        global $db, $system;
        /* check announcement */
        $check = $db->query(sprintf("SELECT * FROM announcements WHERE announcement_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($check->num_rows == 0) {
            _error(403);
        }
        /* hide announcement */
        $db->query(sprintf("INSERT INTO announcements_users (announcement_id, user_id) VALUES (%s, %s)", secure($id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    public function ads($place)
    {
        global $db;
        $ads = [];
        $get_ads = $db->query(sprintf("SELECT * FROM ads WHERE place = %s", secure($place))) or _error(SQL_ERROR_THROWEN);
        if ($get_ads->num_rows > 0) {
            while ($ads_unit = $get_ads->fetch_assoc()) {
                $ads_unit['code'] = html_entity_decode($ads_unit['code'], ENT_QUOTES);
                $ads[] = $ads_unit;
            }
        }
        return $ads;
    }

    public function widgets($place)
    {
        global $db;
        $widgets = [];
        $get_widgets = $db->query(sprintf("SELECT * FROM widgets WHERE place = %s", secure($place))) or _error(SQL_ERROR_THROWEN);
        if ($get_widgets->num_rows > 0) {
            while ($widget = $get_widgets->fetch_assoc()) {
                $widget['code'] = html_entity_decode($widget['code'], ENT_QUOTES);
                $widgets[] = $widget;
            }
        }
        return $widgets;
    }

    public function settings($edit, $args)
    {
        global $db, $system;
        switch ($edit) {
            case 'username':
                /* validate username */
                if (strtolower($args['username']) != strtolower($this->_data['user_name'])) {
                    if (!valid_username($args['username'])) {
                        throw new Exception(__("Please enter a valid username (a-z0-9_.) with minimum 3 characters long"));
                    }
                    if (reserved_username($args['username'])) {
                        throw new Exception(__("You can't use") . " <strong>" . $args['username'] . "</strong> " . __("as username"));
                    }
                    if ($this->check_username($args['username'])) {
                        throw new Exception(__("Sorry, it looks like") . " <strong>" . $args['username'] . "</strong> " . __("belongs to an existing account"));
                    }
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_name = %s WHERE user_id = %s", secure($args['username']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                }
                break;
            case 'email':
                /* validate email */
                if ($args['email'] != $this->_data['user_email']) {
                    if (!valid_email($args['email'])) {
                        throw new Exception(__("Please enter a valid email address"));
                    }
                    if ($this->check_email($args['email'])) {
                        throw new Exception(__("Sorry, it looks like") . " <strong>" . $args['email'] . "</strong> " . __("belongs to an existing account"));
                    }
                    /* generate activation key */
                    $activation_key = get_hash_token();
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_email = %s, user_activation_key = %s, user_activated = '0' WHERE user_id = %s", secure($args['email']), secure($activation_key), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    // send activation email
                    if(!Email::send_activation($this->_data['user_id'], $args['email'], $this->_data['user_firstname'], $this->_data['user_lastname'], $activation_key)){
                      throw new Exception(__("New activation email could not be sent"));
                    }
                }
                break;
            case 'password':
                /* validate all fields */
                if (is_empty($args['current']) || is_empty($args['new']) || is_empty($args['confirm'])) {
                    throw new Exception(__("You must fill in all of the fields"));
                }
                /* validate current password (MD5 check for versions < v2.5) */
                if (md5($args['current']) != $this->_data['user_password'] && !password_verify($args['current'], $this->_data['user_password'])) {
                    throw new Exception(__("Your current password is incorrect"));
                }
                /* validate new password */
                if ($args['new'] != $args['confirm']) {
                    throw new Exception(__("Your passwords do not match"));
                }
                if (strlen($args['new']) < 6) {
                    throw new Exception(__("Password must be at least 6 characters long. Please try another"));
                }
                /* update user */
                $db->query(sprintf("UPDATE users SET user_password = %s WHERE user_id = %s", secure(_password_hash($args['new'])), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'basic':
                /* validate firstname */
                if (is_empty($args['firstname'])) {
                    throw new Exception(__("You must enter full name"));
                }
                $args['birth_date'] = $args['birth_year'] . '-' . $args['birth_month'] . '-' . $args['birth_day'];
                /* check/set custom fields */
                $custom_fields = $this->set_custom_fields($args, false);
                /* update user */
				
				if($args['theme_color'] == ''){
					
					$args['theme_color'] = $args['otherColor'];
				}
				
                $db->query(sprintf("UPDATE users SET user_firstname = %s,
                  user_gender = %s,
                  user_biography = %s,
                  user_location = %s,
                  user_website = %s,
                  user_birthdate = %s,
                  user_privacy_birthdate = %s,
                  user_privacy_birthyear = %s,
				  theme_color = %s
                  WHERE user_id = %s",
                  secure($args['firstname']),
                  secure($args['gender']),
                  secure($args['biography']),
                  secure($args['location']),
                  secure($args['website']),
                  secure($args['birth_date']),
                  secure($args['privacy_birthdate']),
                  secure($args['privacy_birthyear']),
				  secure($args['theme_color']),
                  secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                //$db->query(sprintf("UPDATE users SET user_firstname = %s,user_gender = %s, user_biography = %s WHERE user_id = %s", secure($args['firstname']),secure($args['gender']), secure($args['biography']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'work':
                /* check/set custom fields */
                $custom_fields = $this->set_custom_fields($args, false);
                /* update user */
                $db->query(sprintf("UPDATE users SET user_work_title = %s, user_work_place = %s WHERE user_id = %s", secure($args['work_title']), secure($args['work_place']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'location':
                /* check/set custom fields */
                $custom_fields = $this->set_custom_fields($args, false);
                /* update user */
                $db->query(sprintf("UPDATE users SET user_current_city = %s, user_hometown = %s WHERE user_id = %s", secure($args['city']), secure($args['hometown']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'education':
                /* check/set custom fields */
                $custom_fields = $this->set_custom_fields($args, false);
                /* update user */
                $db->query(sprintf("UPDATE users SET user_edu_major = %s, user_edu_school = %s, user_edu_class = %s WHERE user_id = %s", secure($args['edu_major']), secure($args['edu_school']), secure($args['edu_class']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'social':
                /* validate facebook */
                if (!is_empty($args['facebook']) && !valid_url($args['facebook'])) {
                    throw new Exception(__("Please enter a valid Facebook Profile URL"));
                }
                /* validate twitter */
                if (!is_empty($args['twitter']) && !valid_url($args['twitter'])) {
                    throw new Exception(__("Please enter a valid Twitter Profile URL"));
                }
                /* validate google */
                if (!is_empty($args['google']) && !valid_url($args['google'])) {
                    throw new Exception(__("Please enter a valid Google+ Profile URL"));
                }
                /* validate youtube */
                if (!is_empty($args['youtube']) && !valid_url($args['youtube'])) {
                    throw new Exception(__("Please enter a valid YouTube Profile URL"));
                }
                /* validate instagram */
                if (!is_empty($args['instagram']) && !valid_url($args['instagram'])) {
                    throw new Exception(__("Please enter a valid Instagram Profile URL"));
                }
                /* validate linkedin */
                if (!is_empty($args['linkedin']) && !valid_url($args['linkedin'])) {
                    throw new Exception(__("Please enter a valid Linkedin Profile URL"));
                }
                /* validate vkontakte */
                if (!is_empty($args['vkontakte']) && !valid_url($args['vkontakte'])) {
                    throw new Exception(__("Please enter a valid Vkontakte Profile URL"));
                }
                /* check/set custom fields */
                $custom_fields = $this->set_custom_fields($args, false);
                /* update user */
                $db->query(sprintf("UPDATE users SET user_social_facebook = %s, user_social_twitter = %s, user_social_google = %s, user_social_youtube = %s, user_social_instagram = %s, user_social_linkedin = %s, user_social_vkontakte = %s WHERE user_id = %s", secure($args['facebook']), secure($args['twitter']), secure($args['google']), secure($args['youtube']), secure($args['instagram']), secure($args['linkedin']), secure($args['vkontakte']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'other':
                /* check/set custom fields */
                $custom_fields = $this->set_custom_fields($args, false);
                break;
            case 'privacy':
                $args['privacy_chat'] = ($args['privacy_chat'] == 0) ? 0 : 1;
                $privacy = ['public', 'follow'];
                if (!in_array($args['user_privacy_message'], $privacy)) {
                    _error(400);
                }
                /* update user */
                $db->query(sprintf("UPDATE users SET  user_privacy_message = %s WHERE user_id = %s", secure($args['user_privacy_message']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /*$privacy = array('me', 'friends', 'public', 'follow');
                if(!in_array($args['privacy_wall'], $privacy) || !in_array($args['privacy_birthdate'], $privacy) || !in_array($args['privacy_relationship'], $privacy) || !in_array($args['privacy_basic'], $privacy) || !in_array($args['privacy_work'], $privacy) || !in_array($args['privacy_location'], $privacy) || !in_array($args['privacy_education'], $privacy) || !in_array($args['privacy_other'], $privacy) || !in_array($args['privacy_photos'], $privacy) || !in_array($args['user_privacy_message'], $privacy)) {
                    _error(400);
                }*/
                /* update user */
                /*$db->query(sprintf("UPDATE users SET user_chat_enabled = %s, user_privacy_wall = %s, user_privacy_birthdate = %s, user_privacy_relationship = %s, user_privacy_basic = %s, user_privacy_work = %s, user_privacy_location = %s, user_privacy_education = %s, user_privacy_other = %s, user_privacy_friends = %s, user_privacy_photos = %s, user_privacy_message = %s, user_privacy_pages = %s, user_privacy_groups = %s, user_privacy_events = %s WHERE user_id = %s", secure($args['privacy_chat']), secure($args['privacy_wall']), secure($args['privacy_birthdate']), secure($args['privacy_relationship']), secure($args['privacy_basic']), secure($args['privacy_work']), secure($args['privacy_location']), secure($args['privacy_education']), secure($args['privacy_other']), secure($args['privacy_friends']), secure($args['privacy_photos']), secure($args['user_privacy_message']), secure($args['privacy_pages']), secure($args['privacy_groups']), secure($args['privacy_events']), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);*/
                break;
            case 'notifications':
                /* update user */
                $db->query(sprintf("UPDATE users SET email_post_likes = %s, email_post_comments = %s, email_post_shares = %s, email_wall_posts = %s, email_mentions = %s, email_profile_visits = %s, email_friend_requests = %s WHERE user_id = %s", secure($args['email_post_likes']), secure($args['email_post_comments']), secure($args['email_post_shares']), secure($args['email_wall_posts']), secure($args['email_mentions']), secure($args['email_profile_visits']), secure($args['email_friend_requests']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'edit':
                /* update user */
                //$db->query(sprintf("UPDATE users SET email_post_likes = %s, email_post_comments = %s, email_post_shares = %s, email_wall_posts = %s, email_mentions = %s, email_profile_visits = %s, email_friend_requests = %s WHERE user_id = %s", secure($args['email_post_likes']), secure($args['email_post_comments']), secure($args['email_post_shares']), secure($args['email_wall_posts']), secure($args['email_mentions']), secure($args['email_profile_visits']), secure($args['email_friend_requests']), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                break;
            case 'notifications_sound':
                $args['notifications_sound'] = ($args['notifications_sound'] == 0) ? 0 : 1;
                /* update user */
                $db->query(sprintf("UPDATE users SET notifications_sound = %s WHERE user_id = %s", secure($args['notifications_sound']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'chat':
                $args['privacy_chat'] = ($args['privacy_chat'] == 0) ? 0 : 1;
                /* update user */
                $db->query(sprintf("UPDATE users SET user_chat_enabled = %s WHERE user_id = %s", secure($args['privacy_chat']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'started':
                /* update user */
                $db->query(sprintf("UPDATE users SET user_website = %s, user_biography  = %s WHERE user_id = %s", secure($args['website']), secure($args['biography']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'verification':
                /* validate all fields */
                if (is_empty($args['enable_verification'])) {
                    throw new Exception(__("You must fill in all of the fields"));
                }
                /* validate all fields */
                if (is_empty($args['verification'])) {
                    throw new Exception(__("Please select email or SMS verification type"));
                }
                /* validate all fields */
                if (is_empty($args['verification_email']) && is_empty($args['verification_phone'])) {
                    throw new Exception(__("Please insert email or phone number"));
                }
                if (!is_empty($args['verification_email']) && $args['verification'] == 'email') {
                    if ($args['verification_email'] != $this->_data['user_email']) {
                        if (!valid_email($args['verification_email'])) {
                            throw new Exception(__("Please enter a valid email address"));
                        }
                        if ($this->check_email($args['verification_email'])) {
                            throw new Exception(__("Sorry, it looks like") . " <strong>" . $args['verification_email'] . "</strong> " . __("belongs to an existing account"));
                        }
                        /* generate activation key */
                        $activation_key = get_hash_token();
                        /* update user */
                        $db->query(sprintf("UPDATE users SET user_email = %s  WHERE user_id = %s", secure($args['verification_email']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    }
                }
                /* update user */
                if (!is_empty($args['verification_phone']) && $args['verification'] == 'sms') {
                    $db->query(sprintf("UPDATE users SET user_phone = %s WHERE user_id = %s", secure($args['verification_phone']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                }
                $db->query(sprintf("UPDATE users SET login_verification = %s WHERE user_id = %s", secure($args['verification']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
        }
    }

    public function sign_up($args)
    {
        global $db, $system, $date;
        /* check IP */
        $this->_check_ip();
        if (is_empty($args['username']) || is_empty($args['password'])) {
            if(isset($args['api'])){
                $response = array('status'=>'ERROR', 'message'=>__("You must fill in all of the fields."), 'response' => null);
                return $response;
            }
            throw new Exception(__("You must fill in all of the fields"));
        }
        if (!valid_username($args['username'])) {
            if(isset($args['api'])){
                $response = array('status'=>'ERROR', 'message'=>__("Please enter a valid username (a-z0-9_.) with minimum 3 characters long."), 'response' => null);
                return $response;
            }
            throw new Exception(__("Please enter a valid username (a-z0-9_.) with minimum 3 characters long"));
        }
        if (reserved_username($args['username'])) {
            if(isset($args['api'])){
                $response = array('status'=>'ERROR', 'message'=>__("You can't use") . " '" . $args['username'] . "' " . __("as username"), 'response' => null);
                return $response;
            }
            throw new Exception(__("You can't use") . " <strong>" . $args['username'] . "</strong> " . __("as username"));
        }
        if ($this->check_username($args['username'])) {
            if(isset($args['api'])){
                $response = array('status'=>'ERROR', 'message'=>__("Sorry, it looks like") . " '" . $args['username'] . "' " . __("belongs to an existing account"), 'response' => null);
                return $response;
            }
            throw new Exception(__("Sorry, it looks like") . " <strong>" . $args['username'] . "</strong> " . __("belongs to an existing account"));
        }
        if ($system['activation_enabled'] && $system['activation_type'] == "sms") {
            /* phone activation */
            if (is_empty($args['phone'])) {
                throw new Exception(__("Please enter a valid phone number"));
            }
            if ($this->check_phone($args['phone'])) {
                throw new Exception(__("Sorry, it looks like") . " <strong>" . $args['phone'] . "</strong> " . __("belongs to an existing account"));
            }
        } else {
            /* email activation */
            $args['phone'] = 'null';
        }
        if (strlen($args['password']) < 6) {
            if(isset($args['api'])){
                $response = array('status'=>'ERROR', 'message'=>__("Your password must be at least 6 characters long. Please try another"), 'response' => null);
                return $response;
            }
            throw new Exception(__("Your password must be at least 6 characters long. Please try another"));
        }
        if(isset($args['first_name'])){
            if (strlen($args['first_name']) < 3) {
                if(isset($args['api'])){
                    $response = array('status'=>'ERROR', 'message'=>__("Your full name must be at least 3 characters long. Please try another"), 'response' => null);
                    return $response;
                }
                throw new Exception(__("Your full name must be at least 3 characters long. Please try another"));
            }
        }
        if ($args['gender'] == "none") {
            throw new Exception(__("Please select either Male or Female"));
        }

		if(!empty($args['email'])){
          if (!valid_email($args['email']) ) {
              throw new Exception(__("Please enter a valid email address"));
          }
          if ($this->check_email($args['email'])) {
              throw new Exception(__("Sorry, it looks like") . " <strong>" . $args['email'] . "</strong> " . __("belongs to an existing account"));
          }
        }

        /* $args['gender'] = ($args['gender'] == "male")? "male" : "female";*/
        $args['gender'] = "";
        /* check age restriction */
        if ($system['age_restriction']) {
            if (!in_array($args['birth_month'], range(1, 12))) {
                throw new Exception(__("Please select a valid birth month"));
            }
            if (!in_array($args['birth_day'], range(1, 31))) {
                throw new Exception(__("Please select a valid birth day"));
            }
            if (!in_array($args['birth_year'], range(1905, 2017))) {
                throw new Exception(__("Please select a valid birth year"));
            }
            if (date("Y") - $args['birth_year'] < $system['minimum_age']) {
                throw new Exception(__("Sorry, You must be") . " <strong>" . $system['minimum_age'] . "</strong> " . __("years old to register"));
            }
            $args['birth_date'] = $args['birth_year'] . '-' . $args['birth_month'] . '-' . $args['birth_day'];
        } else {
            $args['birth_date'] = 'null';
        }
        /* check/get custom fields */
        $custom_fields = $this->set_custom_fields($args);
        /* check reCAPTCHA */
        if ($system['reCAPTCHA_enabled']) {
            $recaptcha = new \ReCaptcha\ReCaptcha($system['reCAPTCHA_secret_key']);
            $resp = $recaptcha->verify($args['g-recaptcha-response'], get_user_ip());
            if (!$resp->isSuccess()) {
                throw new Exception(__("The security check is incorrect. Please try again"));
            }
        }
        /* generate activation_key */
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $token = substr(str_shuffle($chars), 0, 10);
        $uuid = md5(uniqid());
        $service_url = 'https://api.sendbird.com/v3/users';
        $curl_post_data = [
            "user_id" => $uuid,
            "nickname" => $args['first_name'],
            "issue_access_token" => "true",
            "profile_url" => "https://sendbird.com/main/img/profiles/profile_05_512px.png"
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.sendbird.com/v3/users");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_post_data);  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $headers = [
            'Api-Token:7bf48344bd96a621a861962257b05d0c7c7ab52f'
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $server_output = curl_exec($ch);
        curl_close($ch);
        if ($server_output) {
            $sendbird_result = json_decode($server_output);
            $sendbird_access_token = $sendbird_result->access_token;
        } else {
            $sendbird_access_token = '';
        }
        $user_activated = 0;
        $user_verified = 0;
        $user_default_language = 14;
        $db->query(sprintf("INSERT INTO users (user_name, user_phone, user_password,user_email, user_firstname, user_lastname, user_gender, user_birthdate, user_registered, user_activation_key,accessToken, uuid,sendbird_access_token, user_activated, user_verified, user_default_language) VALUES (%s, %s, %s, %s, %s, %s, %s,%s, %s, %s,%s,%s,%s,%s,%s,%s)", secure($args['username']), secure($args['phone']), secure(_password_hash($args['password'])),secure($args['email']), secure(ucwords($args['first_name'])), secure(ucwords($args['last_name'])), secure('no_gender'), secure($args['birth_date']), secure($date), secure($activation_key), secure($token), secure($uuid), secure($sendbird_access_token), secure($user_activated), secure($user_verified), secure($user_default_language))) or _error(SQL_ERROR_THROWEN);
        /* get user_id  */
        $user_id = $db->insert_id;
        $this->follow_admin($user_id);
        /* insert custom fields */
        if (count($custom_fields) > 0) {
            foreach ($custom_fields as $field_id => $value) {
                $db->query(sprintf("INSERT INTO users_custom_fields (user_id, field_id, value) VALUES (%s, %s, %s)", secure($user_id, 'int'), secure($field_id, 'int'), secure($value))) or _error(SQL_ERROR_THROWEN);
            }
        }
        /* send activation */
        if ($system['activation_enabled']) {
            if ($system['activation_type'] == "email") {
                if(!Email::send_activation($user_id, $args['email'], $args['first_name'], $args['last_name'], $activation_key)){
                  throw new Exception(__("Activation email could not be sent. But you can login now"));
                }
            } else {
                /* prepare activation SMS */
                $message = $system['system_title'] . " " . __("Activation Token") . ": " . $activation_key;
                /* send SMS */
                if (!sms_send($args['phone'], $message)) {
                    throw new Exception(__("Activation SMS could not be sent. But you can login now"));
                }
            }
        } else {
            /* affiliates system */
            $this->process_affiliates("registration", $user_id);
        }
        /* set cookies */
        $this->set_authentication_cookies($user_id);
        self::update_online($this->_data['user_id']);
    }

    public function sign_in($username_email, $password, $remember = false, $mobile = false)
    {
        global $db, $system;
		if($mobile == 'mobile'){
			$remember = 'mobile';
		}
        elseif($remember == true){
            $remember = false;
        }
        else{
            $remember = false;
        }
        //echo $remember;
        /* valid inputs */
        if (is_empty($username_email) || is_empty($password)) {
            if($remember == 'mobile'){
                $response = array('status'=>'ERROR', 'message'=>__("You must fill in all of the fields"), 'response' => null);
                return $response;
            }
            throw new Exception(__("You must fill in all of the fields"));
        }
        /* check if username or email */
        if (valid_email($username_email)) {
            $user = $this->check_email($username_email, true);
            if ($user === false) {
                if($remember == 'mobile'){
                    $response = array('status'=>'ERROR', 'message'=>__("The email you entered does not belong to any account"), 'response' => null);
                    return $response;
                }
                throw new Exception(__("The email you entered does not belong to any account"));
            }
            $field = "user_email";
        } else {
            if (!valid_username($username_email)) {
                if($remember == 'mobile'){
                    $response = array('status'=>'ERROR', 'message'=>__("Please enter a valid email address or username"), 'response' => null);
                    return $response;
                }
                throw new Exception(__("Please enter a valid email address or username"));
            }
            $user = $this->check_username($username_email, 'user', true);
            if ($user === false) {
                if($remember == 'mobile'){
                    $response = array('status'=>'ERROR', 'message'=>__("The username you entered does not belong to any account"), 'response' => null);
                    return $response;
                }
                throw new Exception(__("The username you entered does not belong to any account"));
            }
            $field = "user_name";
        }
        /* check password */
        //password_hash($password, PASSWORD_DEFAULT);
        if (md5($password) == $user['user_password']) {
            /* validate current password (MD5 check for versions < v2.5) */
            $user['user_password'] = _password_hash($password);
            //password_hash($password, PASSWORD_DEFAULT);
            /* update user password hash from MD5 */
            $db->query(sprintf("UPDATE users SET user_password = %s WHERE user_id = %s", secure($user['user_password']), secure($user['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }
        if (!password_verify($password, $user['user_password'])) {
            if($remember == 'mobile'){
                $response = array('status'=>'ERROR', 'message'=>__("Please re-enter your password") .' '. __("The password you entered is incorrect"), 'response' => null);
                return $response;
            }
            throw new Exception("<p><strong>" . __("Please re-enter your password") . "</strong></p><p>" . __("The password you entered is incorrect") . ". " . __("If you forgot your password?") . " <a href='" . $system['system_url'] . "/reset'>" . __("Request a new one") . "</a></p>");
        }

		/**** Set Language Code For Mobile App By Abhishek *****/
		$this->setMobileLanguageByUserId($user['user_id']);
		/**** End Set Language Code For Mobile App By Abhishek *****/

        /* set cookies */
        $get_profile = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s", secure($user['user_id']))) or _error(SQL_ERROR_THROWEN);
        $result = $get_profile->fetch_assoc();
        /* set cookies */
        if ($result['accessToken'] == '') {
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            $token = substr(str_shuffle($chars), 0, 10);
            $sql_update_access_token = $db->query(sprintf("UPDATE users set accessToken = %s where user_id=%s", secure($token), secure($user['user_id']))) or _error(SQL_ERROR_THROWEN);
        }
        if ($result['uuid'] == '') {
            $uuid = md5(uniqid());
            $sql_update_uuid = $db->query(sprintf("UPDATE users set uuid = %s where user_id=%s", secure($uuid), secure($user['user_id']))) or _error(SQL_ERROR_THROWEN);
            if($remember == 'mobile'){
                $response = array('status'=>'SUCCESS', 'message'=>__('No messages'), 'response' => $user);
                //echo json_encode($response);
                return $response;
            }
        } else {
            $uuid = $user['uuid'];
            if($remember == 'mobile'){
                $response = array('status'=>'SUCCESS', 'message'=>__('No messages'), 'response' => $user);
                //echo json_encode($response);
                return $response;
            }
        }
        if ($result['sendbird_access_token'] == '') {
            $service_url = 'https://api.sendbird.com/v3/users';
            $curl_post_data = [
                "user_id" => $uuid,
                "nickname" => $result['user_firstname'],
                "issue_access_token" => "true",
                "profile_url" => "https://sendbird.com/main/img/profiles/profile_05_512px.png"
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://api.sendbird.com/v3/users");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_post_data);  //Post Fields
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $headers = [
                'Api-Token:7bf48344bd96a621a861962257b05d0c7c7ab52f'
            ];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $server_output = curl_exec($ch);
            curl_close($ch);
            $sendbird_result = json_decode($server_output);
            $sendbird_access_token = $sendbird_result->access_token;
            $sql_update_user = $db->query(sprintf("UPDATE users set sendbird_access_token = %s where user_id=%s", secure($sendbird_access_token), secure($user['user_id']))) or _error(SQL_ERROR_THROWEN);
            $result = $get_profile->fetch_assoc();
        }

        $this->set_authentication_cookies($user['user_id'], $remember);

		$sql_for_update_feature_status = $db->query(sprintf("UPDATE users set feature_status = %s where user_id=%s", 1, secure($user['user_id']))) or _error(SQL_ERROR_THROWEN);

        if ($remember == true) {
            $expire = time() + 2592000;
            setcookie('username_email', $username_email, $expire, '/');
        } else {
            unset($_COOKIE['username_email']);
            unset($_COOKIE['password']);
        }
        self::update_online($this->_data['user_id']);
    }

    public function sign_out()
    {
		//if($_COOKIE['always_allow'] != $_COOKIE[$this->_cookie_user_id]){
			global $db;
			/* delete the session */
			$db->query(sprintf("DELETE FROM users_sessions WHERE session_token = %s AND user_id = %d", secure($_COOKIE[$this->_cookie_user_token]), secure($_COOKIE[$this->_cookie_user_id], 'int') )) or _error(SQL_ERROR_THROWEN);

			/* destroy the session */
			session_destroy();

			/* unset the cookies */
			unset($_COOKIE[$this->_cookie_user_id]);
			unset($_COOKIE[$this->_cookie_user_token]);
            unset($_COOKIE['username_email']);
			setcookie($this->_cookie_user_id, null, -1, '/', '.' . SYS_DOMAIN);
			setcookie($this->_cookie_user_token, null, -1, '/', '.' . SYS_DOMAIN);
            setcookie('username_email', null, -1, '/', '.' . SYS_DOMAIN);
		//}
    }

    private function set_authentication_cookies($user_id, $remember = false, $path = '/')
    {
        global $db, $date;
        /* generate new token */
        $session_token = get_hash_token();
        /* set cookies */
        $expire = $remember ? time() + 2592999 : 0;
        setcookie($this->_cookie_user_id, $user_id, $expire, $path, '.' . SYS_DOMAIN);
        setcookie($this->_cookie_user_token, $session_token, $expire, $path, '.' . SYS_DOMAIN);
        $_COOKIE[$this->_cookie_user_id] = $user_id;
        $_COOKIE[$this->_cookie_user_token] = $session_token;
        $db->query(sprintf("INSERT INTO users_sessions (session_token, session_date, user_id, user_browser, user_os, user_ip) VALUES (%s, %s, %s, %s, %s, %s)", secure($session_token), secure($date), secure($user_id, 'int'), secure(get_user_browser()), secure(get_user_os()), secure(get_user_ip()))) or _error(SQL_ERROR_THROWEN);
    }

    private function _check_ip()
    {
        global $db, $system;
        if ($system['max_accounts'] > 0) {
            $check = $db->query(sprintf("SELECT user_ip, COUNT(*) FROM users_sessions WHERE user_ip = %s GROUP BY user_id", secure(get_user_ip()))) or _error(SQL_ERROR_THROWEN);
            if ($check->num_rows >= $system['max_accounts']) {
                throw new Exception(__("You have reached the maximum number of account for your IP"));
            }
        }
    }

    public function socail_login($provider, $user_profile)
    {
        global $db, $smarty;
        switch ($provider) {
            case 'facebook':
                $social_id = "facebook_id";
                $social_connected = "facebook_connected";
                break;
            case 'twitter':
                $social_id = "twitter_id";
                $social_connected = "twitter_connected";
                break;
            case 'google':
                $social_id = "google_id";
                $social_connected = "google_connected";
                break;
            case 'instagram':
                $social_id = "instagram_id";
                $social_connected = "instagram_connected";
                break;
            case 'linkedin':
                $social_id = "linkedin_id";
                $social_connected = "linkedin_connected";
                break;
            case 'vkontakte':
                $social_id = "vkontakte_id";
                $social_connected = "vkontakte_connected";
                break;
        }
        /* check if user connected or not */
        $check_user = $db->query(sprintf("SELECT user_id FROM users WHERE $social_id = %s", secure($user_profile->identifier))) or _error(SQL_ERROR_THROWEN);
        if ($check_user->num_rows > 0) {
            /* social account connected and just signing-in */
            $user = $check_user->fetch_assoc();
            /* signout if user logged-in */
            if ($this->_logged_in) {
                $this->sign_out();
            }
            /* set cookies */
            $this->set_authentication_cookies($user['user_id'], true);
            redirect();
        } else {
            /* user cloud be connecting his social account or signing-up */
            if ($this->_logged_in) {
                /* [1] connecting social account */
                $db->query(sprintf("UPDATE users SET $social_connected = '1', $social_id = %s WHERE user_id = %s", secure($user_profile->identifier), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                redirect('/settings/linked');
            } else {
                /* [2] signup with social account */
                $_SESSION['social_id'] = $user_profile->identifier;
                $smarty->assign('provider', $provider);
                $smarty->assign('user_profile', $user_profile);
                $smarty->display("signup_social.tpl");
            }
        }
    }

    public function social_register($first_name, $last_name, $username, $email, $password, $gender, $avatar, $provider)
    {
        global $db, $system, $date;
        switch ($provider) {
            case 'facebook':
                $social_id = "facebook_id";
                $social_connected = "facebook_connected";
                break;
            case 'twitter':
                $social_id = "twitter_id";
                $social_connected = "twitter_connected";
                break;
            case 'google':
                $social_id = "google_id";
                $social_connected = "google_connected";
                break;
            case 'instagram':
                $social_id = "instagram_id";
                $social_connected = "instagram_connected";
                break;
            case 'linkedin':
                $social_id = "linkedin_id";
                $social_connected = "linkedin_connected";
                break;
            case 'vkontakte':
                $social_id = "vkontakte_id";
                $social_connected = "vkontakte_connected";
                break;
            default:
                _error(400);
                break;
        }
        /* check IP */
        $this->_check_ip();
        if (is_empty($first_name) || is_empty($last_name) || is_empty($username) || is_empty($email) || is_empty($password)) {
            throw new Exception(__("You must fill in all of the fields"));
        }
        if (!valid_username($username)) {
            throw new Exception(__("Please enter a valid username (a-z0-9_.) with minimum 3 characters long"));
        }
        if (reserved_username($username)) {
            throw new Exception(__("You can't use") . " <strong>" . $username . "</strong> " . __("as username"));
        }
        if ($this->check_username($username)) {
            throw new Exception(__("Sorry, it looks like") . " <strong>" . $username . "</strong> " . __("belongs to an existing account"));
        }
        if (!valid_email($email)) {
            throw new Exception(__("Please enter a valid email address"));
        }
        if ($this->check_email($email)) {
            throw new Exception(__("Sorry, it looks like") . " <strong>" . $email . "</strong> " . __("belongs to an existing account"));
        }
        if (strlen($password) < 6) {
            throw new Exception(__("Your password must be at least 6 characters long. Please try another"));
        }
        if (!valid_name($first_name)) {
            throw new Exception(__("Your first name contains invalid characters"));
        }
        if (strlen($first_name) < 3) {
            throw new Exception(__("Your first name must be at least 3 characters long. Please try another"));
        }
        if (!valid_name($last_name)) {
            throw new Exception(__("Your last name contains invalid characters"));
        }
        if (strlen($last_name) < 3) {
            throw new Exception(__("Your last name must be at least 3 characters long. Please try another"));
        }
        if ($gender == "none") {
            throw new Exception(__("Please select either Male or Female"));
        }
        //$gender = ($gender == "male")? "male" : "female";
        if ($gender == "male") {
            $gender = "male";
        } elseif ($gender == "female") {
            $gender = "female";
        } else {
            $gender = "";
        }
        /* save avatar */
        /* check & create uploads dir */
        $folder = 'photos';
        $depth = '../../../';
        if (!file_exists($depth . $system['uploads_directory'] . '/' . $folder)) {
            @mkdir($depth . $system['uploads_directory'] . '/' . $folder, 0777, true);
        }
        if (!file_exists($depth . $system['uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
            @mkdir($depth . $system['uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
        }
        if (!file_exists($system['uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
            @mkdir($depth . $system['uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
        }
        /* prepare new file name */
        $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
        $prefix = $system['uploads_prefix'] . '_' . get_hash_token();
        $image = new Image($avatar);
        $image_new_name = $directory . $prefix . $image->_img_ext;
        $path_new = $depth . $system['uploads_directory'] . '/' . $image_new_name;
        /* save the new image */
        $image->save($path_new);
        /* upload to amazon s3 */
        if ($system['s3_enabled']) {
            if(AwsS3::upload($path_new, $image_new_name)){
                unlink($path_new);
            }
        }
        /* register user */
        $db->query(sprintf("INSERT INTO users (user_name, user_email, user_password, user_firstname, user_lastname, user_gender, user_registered, user_activated, user_picture, $social_id, $social_connected) VALUES (%s, %s, %s, %s, %s, %s, %s, '1', %s, %s, '1')", secure($username), secure($email), secure(_password_hash($password)), secure(ucwords($first_name)), secure(ucwords($last_name)), secure($gender), secure($date), secure($image_new_name), secure($_SESSION['social_id']))) or _error(SQL_ERROR_THROWEN);
        /* get user_id */
        $user_id = $db->insert_id;
        /* affiliates system */
        $this->process_affiliates("registration", $user_id);
        /* set cookies */
        $this->set_authentication_cookies($user_id);
    }

    public function forget_password($email, $recaptcha_response)
    {
        global $db, $system;
        if (!valid_email($email)) {
			if($recaptcha_response == 'mobile'){
				$response = array('status'=>'ERROR', 'message'=>__("Please enter a valid email address"), 'response' => null);
				return $response;
			}
            throw new Exception(__("Please enter a valid email address"));
        }
        if (!$this->check_email($email)) {
			if($recaptcha_response == 'mobile'){
				$response = array('status'=>'ERROR', 'message'=>__("Sorry, it looks like") . " " . $email . " " . __("doesn't belong to any account"), 'response' => null);
				return $response;
			}
            throw new Exception(__("Sorry, it looks like") . " " . $email . " " . __("doesn't belong to any account"));
        }
        /* check reCAPTCHA */
        if ($system['reCAPTCHA_enabled']) {
            $recaptcha = new \ReCaptcha\ReCaptcha($system['reCAPTCHA_secret_key']);
            $resp = $recaptcha->verify($recaptcha_response, get_user_ip());
            if (!$resp->isSuccess()) {
				if($recaptcha_response == 'mobile'){
					$response = array('status'=>'ERROR', 'message'=>__("The security check is incorrect. Please try again"), 'response' => null);
					return $response;
				}
                throw new Exception(__("The security check is incorrect. Please try again"));
            }
        }
        /* generate reset key */
        $reset_key = get_hash_key(6);
        /* update user */
        $db->query(sprintf("UPDATE users SET user_reset_key = %s, user_reseted = '1' WHERE user_email = %s", secure($reset_key), secure($email))) or _error(SQL_ERROR_THROWEN);
        /* send reset email */
        if(!Email::send_reset_password($email, $reset_key)){
			if($recaptcha_response == 'mobile'){
				$response = array('status'=>'ERROR', 'message'=>__("Password Reset email could not be sent!"), 'response' => null);
				return $response;
			}
          throw new Exception(__("Password Reset email could not be sent!"));
        }
		else{
			if($recaptcha_response == 'mobile'){
				$response = array('status'=>'SUCCESS', 'message'=>__("Please check mail for Password."), 'response' => null);
				return $response;
			}
		}
    }

    public function forget_password_confirm($email, $reset_key)
    {
        global $db;
        if (!valid_email($email)) {
            throw new Exception(__("Invalid email, please try again"));
        }
        /* check reset key */
        $check_key = $db->query(sprintf("SELECT user_reset_key FROM users WHERE user_email = %s AND user_reset_key = %s AND user_reseted = '1'", secure($email), secure($reset_key))) or _error(SQL_ERROR_THROWEN);
        if ($check_key->num_rows == 0) {
            throw new Exception(__("Invalid code, please try again"));
        }
    }

    public function forget_password_reset($email, $reset_key, $password, $confirm)
    {
        global $db;
        if (!valid_email($email)) {
            throw new Exception(__("Invalid email, please try again"));
        }
        /* check reset key */
        $check_key = $db->query(sprintf("SELECT user_reset_key FROM users WHERE user_email = %s AND user_reset_key = %s AND user_reseted = '1'", secure($email), secure($reset_key))) or _error(SQL_ERROR_THROWEN);
        if ($check_key->num_rows == 0) {
            throw new Exception(__("Invalid code, please try again"));
        }
        /* check password length */
        if (strlen($password) < 6) {
            throw new Exception(__("Your password must be at least 6 characters long. Please try another"));
        }
        /* check password confirm */
        if ($password !== $confirm) {
            throw new Exception(__("Your passwords do not match. Please try another"));
        }
        /* update user password */
        $db->query(sprintf("UPDATE users SET user_password = %s, user_reseted = '0' WHERE user_email = %s", secure(_password_hash($password)), secure($email))) or _error(SQL_ERROR_THROWEN);
    }

    public function activation_email_resend()
    {
        global $db, $system;
        /* generate user activation key */
        $activation_key = get_hash_token();
        /* update user */
        $db->query(sprintf("UPDATE users SET user_activation_key = %s WHERE user_id = %s", secure($activation_key), secure($this->_data['user_id']))) or _error(SQL_ERROR_THROWEN);
        // resend activation email
        if(!Email::send_activation($this->_data['user_id'], $this->_data['user_email'], $this->_data['user_firstname'], $this->_data['user_lastname'], $activation_key)){
          throw new Exception(__("Activation email could not be sent"));
        }
    }

    public function activation_email_reset($email)
    {
        global $db, $system;
        if (!valid_email($email)) {
            throw new Exception(__("Invalid email, please try again"));
        }
        if ($this->check_email($email)) {
            throw new Exception(__("Sorry, it looks like") . " " . $email . " " . __("belongs to an existing account"));
        }
        /* generate user activation key */
        $activation_key = get_hash_token();
        /* update user */
        $db->query(sprintf("UPDATE users SET user_email = %s, user_activation_key = %s WHERE user_id = %s", secure($email), secure($activation_key), secure($this->_data['user_id']))) or _error(SQL_ERROR_THROWEN);
        // send activation email
        if(!Email::send_activation($this->_data['user_id'], $email, $this->_data['user_firstname'], $this->_data['user_lastname'], $activation_key)){
          throw new Exception(__("Activation email could not be sent. But you can login now"));
        }
    }

    public function activation_email($id, $token)
    {
        global $db, $system;
        if ($this->_logged_in && !$this->_data['user_activated']) {
            if ($this->_data['user_id'] != $id && $this->_data['user_activation_key'] != $token) {
                _error(404);
            }
            $referrer_id = $this->_data['user_referrer_id'];
            /* activate user */
            $db->query(sprintf("UPDATE users SET user_activated = '1' WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* affiliates system */
            $this->process_affiliates("registration", $this->_data['user_id'], $this->_data['user_referrer_id']);
            /* redirect */
            redirect();
        } else {
            $check_user = $db->query(sprintf("SELECT user_id, user_referrer_id FROM users WHERE user_activated = '0' AND user_id = %s AND user_activation_key = %s", secure($id, 'int'), secure($token))) or _error(SQL_ERROR_THROWEN);
            if ($check_user->num_rows == 0) {
                echo 'already activated';
                //_error(404);
            }
            $_user = $check_user->fetch_assoc();
            /* activate user */
            $db->query(sprintf("UPDATE users SET user_activated = '1' WHERE user_id = %s", secure($_user['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* affiliates system */
            $this->process_affiliates("registration", $_user['user_id'], $_user['user_referrer_id']);
            /* set cookies */
            $this->set_authentication_cookies($_user['user_id']);
        }
    }

    public function activation_phone_resend()
    {
        global $db, $system;
        /* generate user activation key */
        $activation_key = get_hash_key();
        /* update user */
        $db->query(sprintf("UPDATE users SET user_activation_key = %s WHERE user_id = %s", secure($activation_key), secure($this->_data['user_id']))) or _error(SQL_ERROR_THROWEN);
        // resend activation sms
        /* prepare activation SMS */
        $message = $system['system_title'] . " " . __("Activation Token") . ": " . $activation_key;
        /* send SMS */
        if (!sms_send($this->_data['user_phone'], $message)) {
            throw new Exception(__("Activation SMS could not be sent. But you can login now"));
        }
    }

    public function activation_phone_reset($phone)
    {
        global $db, $system;
        if (is_empty($phone)) {
            throw new Exception(__("Please enter a valid phone number"));
        }
        if ($this->check_phone($phone)) {
            throw new Exception(__("Sorry, it looks like") . " <strong>" . $phone . "</strong> " . __("belongs to an existing account"));
        }
        /* generate user activation key */
        $activation_key = get_hash_key();
        /* update user */
        $db->query(sprintf("UPDATE users SET user_phone = %s, user_activation_key = %s WHERE user_id = %s", secure($phone), secure($activation_key), secure($this->_data['user_id']))) or _error(SQL_ERROR_THROWEN);
        // resend activation sms
        /* prepare activation SMS */
        $message = $system['system_title'] . " " . __("Activation Token") . ": " . $activation_key;
        /* send SMS */
        if (!sms_send($phone, $message)) {
            throw new Exception(__("Activation SMS could not be sent. But you can login now"));
        }
    }

    public function activation_phone($token)
    {
        global $db;
        if (is_empty($token)) {
            throw new Exception(__("Please enter a valid activation key"));
        }
        $check_token = $db->query(sprintf("SELECT * FROM users WHERE user_activated = '0' AND user_id = %s AND user_activation_key = %s", secure($this->_data['user_id'], 'int'), secure($token))) or _error(SQL_ERROR_THROWEN);
        if ($check_token->num_rows == 0) {
            throw new Exception(__("Invalid code, please try again"));
        }
        /* activate user */
        $db->query(sprintf("UPDATE users SET user_activated = '1' WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        /* affiliates system */
        $this->process_affiliates("registration", $this->_data['user_id'], $this->_data['user_referrer_id']);
    }

    public function check_email($email, $return_info = false)
    {
        global $db;
        $query = $db->query(sprintf("SELECT * FROM users WHERE user_email = %s", secure($email))) or _error(SQL_ERROR_THROWEN);
        if ($query->num_rows > 0) {
            if ($return_info) {
                $info = $query->fetch_assoc();
                return $info;
            }
            return true;
        }
        return false;
    }

    public function check_phone($phone)
    {
        global $db;
        $query = $db->query(sprintf("SELECT * FROM users WHERE user_phone = %s", secure($phone))) or _error(SQL_ERROR_THROWEN);
        if ($query->num_rows > 0) {
            return true;
        }
        return false;
    }

    public function check_username($username, $type = 'user', $return_info = false)
    {
        global $db;
        /* check type (user|page|group) */
        switch ($type) {
            case 'page':
                $query = $db->query(sprintf("SELECT * FROM pages WHERE page_name = %s", secure($username))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'group':
                $query = $db->query(sprintf("SELECT * FROM groups WHERE group_name = %s", secure($username))) or _error(SQL_ERROR_THROWEN);
                break;
            default:
                $query = $db->query(sprintf("SELECT * FROM users WHERE user_name = %s", secure($username))) or _error(SQL_ERROR_THROWEN);
                break;
        }
        if ($query->num_rows > 0) {
            if ($return_info) {
                $info = $query->fetch_assoc();

				if($user['user_id'] != ''){
					$db->query('delete from users_sessions where user_id = '.$info['user_id']);
				}

                return $info;
            }
            return true;
        }
        return false;
    }

    public function video_view_manager($post_id)
    {
        global $db;
        /* check type (post view) */
        $user_ip = $_SERVER['REMOTE_ADDR'];
        $query = $db->query(sprintf("SELECT * FROM posts_video_views WHERE post_id = %s AND  user_ip = %s", secure($post_id, 'int'), secure($user_ip))) or _error(SQL_ERROR_THROWEN);
        if ($query->num_rows > 0) {
            return false;
        } else {
            $db->query(sprintf("INSERT INTO posts_video_views (post_id, user_ip) VALUES (%s, %s)", secure($post_id, 'int'), secure($user_ip)));
            $db->query(sprintf("UPDATE posts SET views = views + 1 WHERE post_id = %s", secure($post_id, 'int')));
            return;
        }
    }

    public function views_format($views)
    {
        if ($views == 1) {
            return "$views View";
        } elseif ($views > 1000 && $views < 1000000) {
            $views_count = $views * 1 / 1000;
            $views_k = round($views_count, PHP_ROUND_HALF_UP);
            return "$views_k K Views";
        } elseif ($views > 1000000) {
            $views_count = $views * 1 / 1000000;
            $views_m = round($views_count, PHP_ROUND_HALF_UP);
            return "$views_m M Views";
        } else {
            return "$views Views";
        }
    }

    public function delete_comment_on_block($user_id)
    {
        global $db;
        $login_user_id = $this->_data['user_id'];
        $query = $db->query(sprintf("SELECT * FROM posts WHERE user_id = %s", secure($login_user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($query->num_rows > 0) {
            while ($posts = $query->fetch_assoc()) {
                $posts_array[] = $posts['post_id'];
            }
        }
        $post_implode = implode(",", $posts_array);
        $query2 = $db->query(sprintf("SELECT * FROM posts_comments WHERE node_type = 'post' AND node_id IN ( %s ) AND user_id = %s ", $post_implode, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($query2->num_rows > 0) {
            while ($comments = $query2->fetch_assoc()) {
                $comments_array[] = $comments['comment_id'];
            }
        }
        $query4 = $db->query(sprintf("SELECT * FROM posts_comments WHERE node_type = 'post' AND node_id IN ( %s ) AND user_id = %s", $post_implode, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($query4->num_rows > 0) {
            while ($comments3 = $query4->fetch_assoc()) {
                $comments2_array[] = $comments3['comment_id'];
                $comments3_comment_id = $comments3['comment_id'];
                $comments3_post_id = $comments3['node_id'];
                $query3 = $db->query(sprintf("SELECT * FROM posts_comments WHERE node_type = 'comment' AND node_id =%s ", secure($comments3_comment_id))) or _error(SQL_ERROR_THROWEN);
                if ($query3->num_rows > 0) {
                    while ($comments2 = $query3->fetch_assoc()) {
                        $comments2_array[] = $comments2['comment_id'];
                        $this->delete_comment_block_recursive($comments2['comment_id'], $comments3_post_id);
                    }
                }
            }
        }
        $query5 = $db->query(sprintf("SELECT * FROM posts_comments WHERE node_type = 'post' AND node_id IN ( %s )", $post_implode)) or _error(SQL_ERROR_THROWEN);
        if ($query5->num_rows > 0) {
            while ($comments5 = $query5->fetch_assoc()) {
                $comments5_array[] = $comments5['comment_id'];
                $comments5_comment_id = $comments5['comment_id'];
                $comments5_post_id = $comments5['node_id'];
                $query6 = $db->query(sprintf("SELECT * FROM posts_comments WHERE node_type = 'comment' AND node_id =%s AND user_id = %s ", secure($comments5_comment_id), secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($query6->num_rows > 0) {
                    while ($comments6 = $query6->fetch_assoc()) {
                        $comments6_array[] = $comments6['comment_id'];
                        $this->delete_comment_block_recursive($comments6['comment_id'], $comments5_post_id);
                    }
                }
            }
        }
        $query7 = $db->query(sprintf("SELECT * FROM posts_comments WHERE node_type = 'post' AND node_id IN ( %s ) AND user_id = %s ", $post_implode, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($query7->num_rows > 0) {
            while ($comments7 = $query7->fetch_assoc()) {
                $this->delete_comment_block($comments7['comment_id']);
            }
        }
        // $comment2_implode = implode(",", $comments2_array);
        return;
    }

    public function delete_comment_block($comment_id)
    {
        global $db;
        $comment = $this->get_comment($comment_id);
        $db->query(sprintf("DELETE FROM posts_comments WHERE comment_id = %s", secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        switch ($comment['node_type']) {
            case 'post':
                $db->query(sprintf("UPDATE posts SET comments = IF(comments=0,0,comments-1) WHERE post_id = %s", secure($comment['node_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'photo':
                $db->query(sprintf("UPDATE posts_photos SET comments = IF(comments=0,0,comments-1) WHERE photo_id = %s", secure($comment['node_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'comment':
                $db->query(sprintf("UPDATE posts SET comments = IF(comments=0,0,comments-1) WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                $db->query(sprintf("UPDATE posts_comments SET replies = IF(replies=0,0,replies-1) WHERE comment_id = %s", secure($comment['node_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
        }
    }

    public function delete_comment_block_recursive($comment_id, $post_id)
    {
        global $db;
        $comment = $this->get_comment($comment_id);
        $db->query(sprintf("DELETE FROM posts_comments WHERE comment_id = %s", secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        switch ($comment['node_type']) {
            case 'post':
                $db->query(sprintf("UPDATE posts SET comments = IF(comments=0,0,comments-1) WHERE post_id = %s", secure($comment['node_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'photo':
                $db->query(sprintf("UPDATE posts_photos SET comments = IF(comments=0,0,comments-1) WHERE photo_id = %s", secure($comment['node_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'comment':
                $db->query(sprintf("UPDATE posts SET comments = IF(comments=0,0,comments-1) WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                $db->query(sprintf("UPDATE posts_comments SET replies = IF(replies=0,0,replies-1) WHERE comment_id = %s", secure($comment['node_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
        }
    }

    public function _get_popular_tag($user_id)
    {
        global $db;
        $day = date('w');
        $week_start = date('m-d-Y', strtotime('-' . $day . ' days'));
        $week_end = date('m-d-Y', strtotime('+' . (6 - $day) . ' days'));
        $login_user_id = $this->_data['user_id'];
        $query = $db->query(sprintf("SELECT text FROM posts WHERE YEARWEEK(`time`,1) >= YEARWEEK(CURDATE(), 1)")) or _error(SQL_ERROR_THROWEN);
        if ($query->num_rows > 0) {
            while ($posts = $query->fetch_assoc()) {
                $posts_text = $posts['text'];
                $posts_content[] = $posts['text'];
                preg_match_all('/#([\p{Pc}\p{N}\p{L}\p{Mn}]+)/u', $posts_text, $matches);
                foreach ($matches as $matche) {
                    if (count($matche) > 0) {
                        foreach ($matche as $matcheR) {
                            $matchArray[] = str_replace('#', '', $matcheR);
                        }
                    }
                }
            }
        }
        $query_mention = $db->query(sprintf("SELECT * FROM posts WHERE YEARWEEK(`time`,1) >= YEARWEEK(CURDATE(), 1)")) or _error(SQL_ERROR_THROWEN);
        if ($query_mention->num_rows > 0) {
            while ($posts_mention = $query_mention->fetch_assoc()) {
                $posts_text_mention = $posts_mention['text'];
                $posts_content_mention[] = $posts_mention['text'];
                preg_match_all("/\[[^\]]*\]/", $posts_text_mention, $matches_mention);
                foreach ($matches_mention as $matche_mention) {
                    if (count($matche_mention) > 0) {
                        foreach ($matche_mention as $matcheR_mention) {
                            $matchArray_mention[] = str_replace(']', '', str_replace('[', '', $matcheR_mention));
                        }
                    }
                }
            }
        }
        if (count($matchArray) > 0 && count($matchArray_mention) > 0) {
            $uniqyeValues_mention = array_unique($matchArray_mention);
            foreach ($uniqyeValues_mention as $uniqyeValue_mention) {
                $mention_posts = $db->query(sprintf("SELECT * FROM posts WHERE YEARWEEK(`time`,1) >= YEARWEEK(CURDATE(), 1) AND text LIKE %s", secure('[' . $uniqyeValue_mention . ']', 'search'))) or _error(SQL_ERROR_THROWEN);
                $countVal_mention = $mention_posts->num_rows;
                /*$countVal_mention = '';
            foreach($posts_content_mention as $posts_contents_mention){
                $countVal_mention += substr_count($posts_contents_mention, $uniqyeValue_mention); // 2
            }*/
                $value_count_mention[] = [
                    'count' => $countVal_mention,
                    'tags' => $uniqyeValue_mention,
                    'url_tags' => base64_encode(json_encode('[' . $uniqyeValue_mention . ']')),
                ];
            }
            $uniqyeValues = array_unique($matchArray);
            foreach ($uniqyeValues as $uniqyeValue) {
                $hash_posts = $db->query(sprintf("SELECT * FROM posts WHERE YEARWEEK(`time`,1) >= YEARWEEK(CURDATE(), 1) AND text LIKE %s", secure('#' . $uniqyeValue, 'search'))) or _error(SQL_ERROR_THROWEN);
                $countVal = $hash_posts->num_rows;
                /*$countVal = '';
            foreach($posts_content as $posts_contents){
                $countVal += substr_count($posts_contents, $uniqyeValue); // 2
            }*/
                $value_count[] = [
                    'count' => $countVal,
                    'tags' => '#' . $uniqyeValue,
                    'url_tags' => base64_encode(json_encode('#' . $uniqyeValue)),
                ];
                //count($value_count);
            }
            //$merge_array = array_slice(array_merge($value_count,$value_count_mention),0,10);
            $merge_array = array_merge($value_count, $value_count_mention);
            $merge_array1 = $this->sortBy('count', $merge_array, 'desc');
            return array_slice($merge_array, 0, 10);
        } else {
            $value = 'No Record Found';
            return $value;
        }
    }

    public function sortBy($field, &$array, $direction = 'asc')
    {
        usort($array, create_function('$a, $b', '
            $a = $a["' . $field . '"];
            $b = $b["' . $field . '"];
            if ($a == $b)
            {
                return 0;
            }
            return ($a ' . ($direction == 'desc' ? '>' : '<') . ' $b) ? -1 : 1;
        '));
        return true;
    }

    /**
     * _get_likes_count
     *
     * @param integer $photo_id
     * @return void
     */
    public function _get_likes($user_id)
    {
        global $db, $system;
        $get_likes = $db->query(sprintf("SELECT count(post_id) as count_likes FROM posts_likes WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_likes->num_rows > 0) {
            while ($likes = $get_likes->fetch_assoc()) {
                /* check the photo privacy */
                $count_likes = $likes['count_likes'];
            }
        }
        return $count_likes;
    }

    public function get_user_mentions($offset = 0, $last_notification_id = null)
    {
        global $db, $system;
        $offset *= $system['max_results'];
        $mentions = [];
        if ($last_notification_id !== null) {
            $get_user_mentions = $db->query(sprintf("SELECT notifications.*, users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM notifications INNER JOIN users ON notifications.from_user_id = users.user_id WHERE notifications.to_user_id = %s AND notifications.notification_id > %s AND notifications.action = 'mention' ORDER BY notifications.notification_id DESC", secure($this->_data['user_id'], 'int'), secure($last_notification_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_user_mentions = $db->query(sprintf("SELECT notifications.*, users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM notifications INNER JOIN users ON notifications.from_user_id = users.user_id WHERE notifications.to_user_id = %s AND notifications.action = 'mention' ORDER BY notifications.notification_id DESC LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        }
        if ($get_user_mentions->num_rows > 0) {
            while ($notification = $get_user_mentions->fetch_assoc()) {
                $notification['user_picture'] = $this->get_picture($notification['user_picture'], $notification['user_gender']);
                $notification['notify_id'] = ($notification['notify_id']) ? "?notify_id=" . $notification['notify_id'] : "";
                /* parse notification */
                $notification['icon'] = "Icon--info";
                switch ($notification['node_type']) {
                    case 'post':
                        $notification['url'] = $system['system_url'] . '/posts/' . $notification['node_url'];
                        $notification['message'] = __("mentioned you in a post");
                        break;
                    case 'comment_post':
                        $notification['url'] = $system['system_url'] . '/posts/' . $notification['node_url'] . $notification['notify_id'];
                        $notification['message'] = __("mentioned you in a comment");
                        break;
                    case 'comment_photo':
                        $notification['url'] = $system['system_url'] . '/photos/' . $notification['node_url'] . $notification['notify_id'];
                        $notification['message'] = __("mentioned you in a comment");
                        break;
                    case 'reply_post':
                        $notification['url'] = $system['system_url'] . '/posts/' . $notification['node_url'] . $notification['notify_id'];
                        $notification['message'] = __("mentioned you in a reply");
                        break;
                    case 'reply_photo':
                        $notification['url'] = $system['system_url'] . '/photos/' . $notification['node_url'] . $notification['notify_id'];
                        $notification['message'] = __("mentioned you in a reply");
                        break;
                }
                $notification['full_message'] = $notification['user_firstname'] . " " . $notification['user_lastname'] . " " . $notification['message'];
                $mentions[] = $notification;
            }
        }
        return $mentions;
    }

    public function get_post_content($post_id)
    {
        global $db, $system;
        $posts = [];
        $get_post_content = $db->query(sprintf('SELECT posts.user_id, posts.post_type,posts.text,users.user_picture,users.user_gender,users.user_name,users.user_firstname,users.user_lastname,posts.time FROM posts INNER JOIN users ON posts.user_id = users.user_id WHERE posts.post_id = %s LIMIT 1', secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_post_content->num_rows > 0) {
            while ($postarr = $get_post_content->fetch_assoc()) {
                $post['user_name'] = $postarr['user_name'];
                $post['name'] = $postarr['user_firstname'] . ' ' . $postarr['user_lastname'];
                //$post['text'] = $postarr['text'];
                $post['text'] = $this->decode_emoji($postarr['text']);
                $post['time'] = $postarr['time'];
                $post['post_id'] = $post_id;
                $post['post_type'] = $postarr['post_type'];
                $post['user_picture'] = $this->get_picture($postarr['user_picture'], $postarr['user_gender']);
                $post['text_plain'] = htmlentities($postarr['text'], ENT_QUOTES, 'utf-8');
                $posts[] = $post;
            }
        }
        return $posts;
    }

    public function follow_admin($user_id)
    {
        global $db;
        $check = $db->query(sprintf("SELECT * FROM followings WHERE user_id = %s AND following_id = 356", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($check->num_rows > 0) {
            return;
        }
        /* add as following */
        $db->query(sprintf("INSERT INTO followings (user_id, following_id) VALUES (%s, 356)", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* post new notification */
        $this->post_notification(['to_user_id' => 356, 'action' => 'follow']);
        return true;
    }

    public function share_comment($post_id, $comment)
    {
        global $db, $date;
        /* check if the viewer can share the post */
        $post = $this->_check_post($post_id, true);
        if (!$post || $post['privacy'] != 'public') {
            _error(403);
        }
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            _error(403);
        }
        if ($post['post_type'] == "shared") {
            $origin = $this->_check_post($post['origin_id'], true);
            if (!$origin || $origin['privacy'] != 'public') {
                _error(403);
            }
            $post_id = $origin['post_id'];
            $author_id = $origin['author_id'];
        } else {
            $post_id = $post['post_id'];
            $author_id = $post['author_id'];
        }
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, origin_id, time, privacy, text) VALUES (%s, 'user', 'shared', %s, %s, 'public', %s)", secure($this->_data['user_id'], 'int'), secure($post_id, 'int'), secure($date), secure($comment))) or _error(SQL_ERROR_THROWEN);
        $share_post_id = $db->insert_id;
        $db->query(sprintf("UPDATE posts SET shares = shares + 1 WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        $this->post_notification(['to_user_id' => $author_id, 'action' => 'share', 'node_type' => 'post', 'node_url' => $post_id]);
        return $share_post_id;
    }

    public function get_video_thumbnail($id, $type = 'user', $offset = 0, $pass_check = true)
    {
        global $db, $system;
        $videos = [];
        $get_videos = $db->query(sprintf("SELECT posts_videos.video_id, posts_videos.source, posts.privacy FROM posts_videos INNER JOIN posts ON posts_videos.post_id = posts.post_id WHERE posts.user_id = %s AND user_type = 'user' ORDER BY posts_videos.video_id DESC LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_videos->num_rows > 0) {
            while ($video = $get_videos->fetch_assoc()) {
                /* check the photo privacy */
                if ($this->_check_privacy($video['privacy'], $id)) {
                    $src = $video['source'];
                    $url_pieces = explode('/', $src);
                    // If Youtube
                    $extract_id = explode('?', $url_pieces[4]);
                    $id = $extract_id[0];
                    $thumbnail[] = 'http://img.youtube.com/vi/' . $id . '/mqdefault.jpg';
                }
            }
        }
        return $thumbnail;
    }

    public function obsolete_words()
    {
        global $db;
        $censored_words = "";
        $get_obsolete = $db->query(sprintf('SELECT censored_words FROM system_options')) or _error(SQL_ERROR_THROWEN);
        if ($get_obsolete->num_rows > 0) {
            while ($obsolete = $get_obsolete->fetch_assoc()) {
                $censored_words = $obsolete['censored_words'];
            }
        }
        return $censored_words;
    }

    public function user_banned()
    {
        global $db, $system;

		if($this->_data['user_id'] != '356'){
			$db->query(sprintf("UPDATE users SET user_banned ='1' WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
		}
        /*$db->query(sprintf("DELETE FROM posts WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $db->query(sprintf("DELETE FROM followings WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $db->query(sprintf("DELETE FROM followings WHERE following_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $db->query(sprintf("DELETE FROM posts_comments WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $db->query(sprintf("DELETE FROM posts_likes WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $db->query(sprintf("DELETE FROM notifications WHERE to_user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $db->query(sprintf("DELETE FROM notifications WHERE from_user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);*/
        // return true;
    }

    public function get_post_comment($post_id)
    {
        global $db, $system;
        $posts = [];
        $get_post_content = $db->query(sprintf('SELECT posts_comments.user_id,posts_comments.text,users.user_picture,users.user_gender,users.user_name,users.user_firstname,users.user_lastname,posts_comments.time FROM posts_comments INNER JOIN users ON posts_comments.user_id = users.user_id WHERE posts_comments.comment_id = %s LIMIT 1', secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_post_content->num_rows > 0) {
            while ($postarr = $get_post_content->fetch_assoc()) {
                $post['user_name'] = $postarr['user_name'];
                $post['name'] = $postarr['user_firstname'] . ' ' . $postarr['user_lastname'];
                $post['text'] = $this->decode_emoji($postarr['text']);
                $post['time'] = $postarr['time'];
                $post['post_id'] = $post_id;
                $post['post_type'] = $postarr['post_type'];
                $post['user_picture'] = $this->get_picture($postarr['user_picture'], $postarr['user_gender']);
                $post['text_plain'] = htmlentities($postarr['text'], ENT_QUOTES, 'utf-8');
                $posts[] = $post;
            }
        }
        return $posts;
    }

	public function test_abhi()
    {
		global $db, $system;

		//$alter = "ALTER TABLE broadcasts ADD custom_status VARCHAR( 50 ) NOT NULL after views";

		/*$create = "CREATE TABLE broadcast_status (
			id int NOT NULL auto_increment PRIMARY KEY,
			status varchar (15) NOT NULL
			)";*/

		/*$insert_value = 'Insert Into broadcast_status(`status`) Values ("No")';

        $db->query($insert_value);

		echo 'victory again';exit;	*/
	}
	public function broadcast_start()
    {
		global $db, $system;

		$sql = 'Update broadcast_status set status="Yes" where id=1';

		$db->query($sql);

		$return["json"] = 'Success';
		$return["status_start"] = 'Yes';

		echo json_encode($return);
		exit;
	}
	public function broadcast_stop()
    {
		global $db, $system;

		$sql = 'Update broadcast_status set status="No" where id=1';

		$db->query($sql);

		$return["json"] = 'Success';
		$return["status_start"] = 'No';

		echo json_encode($return);
		exit;
	}
	public function support_submit_msg($_content){

		global $db, $system;

		$user_id = $_content['user_id'];
		$support_subject = $_content['support_subject'];
		$support_message = $_content['support_message'];

		if($_content['support_subject'] == ''){

			return 'Error';
		}

		if($_content['support_message'] == ''){

			return 'Error';
		}

		$sql = 'INSERT INTO support_msg_froms(`user_id`,`subject`,`message`,`created`,`modified`) VALUES('.$user_id.',"'.$support_subject.'","'.$support_message.'",Now(),Now())';

		if($db->query($sql))
		{
			return 'Success';
		}
		else{
			return 'Error';
		}
	}
    public function _get_posts_api($user_id, $me_id='',$self='', $limit = 10, $page = 1)
    {
        ini_set('display_errors', '0');
        global $db;
            $arr = array();
            $posts_follo = $this->get_followings_api($user_id,'',-1);
            $user_ids = "user_id = $user_id";
            $vr = 0;
			if($self != '1'){
				foreach($posts_follo['response'] as $key => $following){
					if($vr == 0){
						$user_ids .= " OR ";
						$vr = 1;
					}
					if(count($posts_follo['response'])-1 == $key){
						$user_ids .= " user_id = ".$following['user_id'];
					}
					else{
						$user_ids .= " user_id = ".$following['user_id'].' OR ';
					}
				}
			}
            //var_dump($user_ids); exit;
            $get_posts = $db->query(sprintf("SELECT `post_id` FROM posts WHERE is_broadcast = '0' AND $user_ids ORDER BY post_id DESC"." LIMIT " . ( ( $page - 1 ) * $limit ) . ", $limit", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_posts->num_rows > 0) {
                while ($post = $get_posts->fetch_assoc()) {

					$current_post = $this->get_post_api($post['post_id'],$me_id);
					//unset($current_post['post_comments']);
                    //var_dump($current_post); exit;
					// Formatted Time EST
					$current_post['formatted_time'] = $this->time_elapsed_string($current_post['time'], $full = false);
					$arr[] = $current_post;
                }
            }
			//var_dump($arr); exit;
        return $arr;
    }

    public function _get_broadcast_api($user_id)
    {
        ini_set('display_errors', '0');
        global $db;
            $arr = array();
            $get_posts = $db->query(sprintf("SELECT * FROM  posts WHERE is_broadcast = '1' AND user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_posts->num_rows > 0) {
                while ($post = $get_posts->fetch_assoc()) {
                    $arr[] = $post;
                }
            }
        return $arr;
    }

    public function like_post_api($post_id, $user_id)
    {
        global $db;
        /* (check|get) post */
        $post = $this->get_post_api($post_id, $user_id);
        if (!$post) {
            return $response = array('status'=>'ERROR', 'message'=>__('Post does not exist'), 'response' => null);
        }
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            return $response = array('status'=>'ERROR', 'message'=>__('post author is blocked by system.'), 'response' => null);
        }
        /* like the post */
        if (!$post['i_like']) {
            //echo $post['author_id']; echo $post_id; exit;
			$db->query(sprintf("INSERT INTO posts_likes (user_id, post_id) VALUES (%s, %s)", secure($user_id, 'int'), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* update post likes counter */
            $db->query(sprintf("UPDATE posts SET likes = likes + 1 WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* post notification */
			$user = $this->get_user_api($user_id);
			$this->_data = $user['response'];
            $this->post_notification(['to_user_id' => $post['author_id'], 'action' => 'like', 'node_type' => 'post', 'node_url' => $post_id]);
			return $response = array('status'=>'SUCCESS', 'message'=>__('post liked successfully.'), 'response' => null);
		}
		else{
			return $response = array('status'=>'ERROR', 'message'=>__('You already liked this post'), 'response' => null);
		}
    }

    public function unlike_post_api($post_id, $user_id)
    {
        global $db;
        /* (check|get) post */
        $post = $this->get_post_api($post_id, $user_id);
        if (!$post) {
            return $response = array('status'=>'ERROR', 'message'=>__('Post does not exist'), 'response' => null);
        }
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            return $response = array('status'=>'ERROR', 'message'=>__('post author is blocked by system.'), 'response' => null);
        }
        /* unlike the post */
        if ($post['i_like']) {
            $db->query(sprintf("DELETE FROM posts_likes WHERE user_id = %s AND post_id = %s", secure($user_id, 'int'), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* update post likes counter */
            $db->query(sprintf("UPDATE posts SET likes = IF(likes=0,0,likes-1) WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
			$user = $this->get_user_api($user_id);
			$this->_data = $user['response'];
            /* delete notification */
            $this->delete_notification($post['author_id'], 'like', 'post', $post_id);
			return $response = array('status'=>'SUCCESS', 'message'=>__('post unlike successfully.'), 'response' => null);
        }
		else{
			return $response = array('status'=>'ERROR', 'message'=>__('You did not like this post'), 'response' => null);
		}
    }

public function get_notifications_api($user_id, $limit = 10, $page = 1, $offset = 0, $last_notification_id = null)
    {
        global $db, $system;
        $offset *= 15;
        $notifications = [];
        if ($last_notification_id !== null) {
            $get_notifications = $db->query(sprintf("SELECT notifications.*, users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM notifications INNER JOIN users ON notifications.from_user_id = users.user_id WHERE notifications.to_user_id = %s AND notifications.notification_id > %s ORDER BY notifications.notification_id DESC", secure($user_id, 'int'), secure($last_notification_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_notifications = $db->query(sprintf("SELECT notifications.*, users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM notifications INNER JOIN users ON notifications.from_user_id = users.user_id WHERE notifications.to_user_id = %s ORDER BY notifications.notification_id DESC "." LIMIT " . ( ( $page - 1 ) * $limit ) . ", $limit", secure($user_id, 'int'), secure($offset, 'int', false), secure(15, 'int', false))) or _error(SQL_ERROR_THROWEN);
        }
        if ($get_notifications->num_rows > 0) {
			$count = 0;
			$likes_counter = 1;
			$likes_counter_photo = 1;
            while ($notification = $get_notifications->fetch_assoc()) {
				
				/****** Loop Continue When Total Likes Greater Than 1 In Type Post ********/
				if($notification['action'] == 'like' && $notification['node_type'] == 'post' && $likes_counter > 1){
					
					$getTotalLikes = $this->get_total_likes($notification['node_url'],'post');	
					if($getTotalLikes > 1){
						continue;
					}	
				}
				/****** Loop Continue When Total Likes Greater Than 1 In Type photo ********/
				if($notification['action'] == 'like' && $notification['node_type'] == 'photo' && $likes_counter_photo > 1){
					
					$getTotalLikes = $this->get_total_likes($notification['node_url'],'photo');	
					if($getTotalLikes > 1){
						continue;
					}	
				}
				/****** Loop Continue When Total Likes Greater Than 1 In Type Post ********/
					
				
                $notification['user_picture'] = $this->get_picture($notification['user_picture'], $notification['user_gender']);
                $notification['notify_id'] = ($notification['notify_id']) ? "?notify_id=" . $notification['notify_id'] : "";
                /* parse notification */
                switch ($notification['action']) {

                    case 'friend_add':
                        $notification['icon'] = "fa-user-plus";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = __("send you a friend request");
                        break;
                    case 'new_post':
                    //var_dump($notification);
                        $post = $this->get_post_api($notification['node_url']);
                        if ($post['status'] != 'ERROR') {
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = __("added new post");
                            unset($post['post_comments']);
                            $notification['obj'] = $post;
                        }
                        else{
                            unset($notification);
                        }
                        break;
                    case 'friend_accept':
                        $notification['icon'] = "fa-user-plus";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = __("accepted your friend request");
                        break;
                    case 'follow':
                        $notification['icon'] = "Icon--follower";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = __("now following you");
                        break;
                    case 'like':
                        $notification['icon'] = "Icon--heart";
                        if ($notification['node_type'] == "post") {
                            $post = $this->get_post_api($notification['node_url'], true);
                            if ($post['status'] != 'ERROR') {
                                // $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'];
                                $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
								
								/** Total Likes Count and cumulate the notification in one for post **/
								$total_likes_count = $this->get_total_likes($notification['node_url'],'post');
								if($total_likes_count > 1){
									
									$two_username = $this->get_two_username_api($user_id,$notification['node_url'],'post');
									
									if($total_likes_count > 2)
										$total_likes_count = ($total_likes_count - 2);
									if($total_likes_count == 2)
										$total_likes_count = ($total_likes_count - 1);
									
									$notification['message'] = $two_username.' '.__("and").' '.$total_likes_count.' '.__("other people").' '.__("likes your post");
									
								/****** End ********/
								}
								else{
									$notification['message'] = __("likes your post");
								}	
                                $notification['obj'] = $post;
                            }
                            else{
                                unset($notification);
                            }
                        } elseif ($notification['node_type'] == "photo") {
                            // $notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
							
							/** Total Likes Count and cumulate the notification in one for post **/
								$total_likes_count = $this->get_total_likes($notification['node_url'],'photo');
								if($total_likes_count > 1){
									
									$two_username = $this->get_two_username_api($user_id,$notification['node_url'],'photo');
									
									if($total_likes_count > 2)
										$total_likes_count = ($total_likes_count - 2);
									if($total_likes_count == 2)
										$total_likes_count = ($total_likes_count - 1);
									
									$notification['message'] = $two_username.' '.__("and").' '.$total_likes_count.' '.__("other people").' '.__("likes your photo");
									
								/****** End ********/
								}
								else{
									$notification['message'] = __("likes your photo");
								}
																
                        } elseif ($notification['node_type'] == "post_comment") {
                            // $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = __("likes your comment");
                        } elseif ($notification['node_type'] == "photo_comment") {
                            //$notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = __("likes your comment");
                        } elseif ($notification['node_type'] == "post_reply") {
                            //$notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = __("likes your reply");
                        } elseif ($notification['node_type'] == "photo_reply") {
                            //$notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = __("likes your reply");
                        }
                        break;
                    case 'comment':
                        $notification['icon'] = "Icon--reply";
                        if ($notification['node_type'] == "post") {
                            $post = $this->get_post_api($notification['node_url'], true);
                            if ($post['status'] != 'ERROR') {
                                //$notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'].$notification['notify_id'];
                                $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                                $notification['message'] = __("commented on your post");
                                $notification['obj'] = $post;
                            }
                            else{
                                unset($notification);
                            }
                        } elseif ($notification['node_type'] == "photo") {
                            //$notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = __("commented on your photo");
                        }
                        break;
                    case 'reply':
                        $notification['icon'] = "Icon--reply";
                        if ($notification['node_type'] == "post") {
                            //$notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        } elseif ($notification['node_type'] == "photo") {
                            //$notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'].$notification['notify_id'];
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        }
                        $notification['message'] = __("replied to your comment");
                        break;
                    case 'share':
                        $post = $this->get_post_api($notification['node_url'], true);
                            if ($post['status'] != 'ERROR') {
                                $notification['icon'] = "Icon--retweet";
                                $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                                $notification['message'] = __("shared your post");
                                $notification['obj'] = $post;
                            }
                            else{
                                unset($notification);
                            }
                        break;
                    case 'vote':
                        $notification['icon'] = "Icon--check";
                        $notification['url'] = $system['system_url'] . '/posts/' . $notification['node_url'];
                        $notification['message'] = __("voted on your poll");
                        break;
                    case 'mention':
                        $notification['icon'] = "Icon--follower";
                        switch ($notification['node_type']) {
                            case 'post':
                                $notification['url'] = $system['system_url'] . '/posts/' . $notification['node_url'];
                                $notification['message'] = __("mentioned you in a post");
                                break;
                            case 'comment_post':
                                $notification['url'] = $system['system_url'] . '/posts/' . $notification['node_url'] . $notification['notify_id'];
                                $notification['message'] = __("mentioned you in a comment");
                                break;
                            case 'comment_photo':
                                $notification['url'] = $system['system_url'] . '/photos/' . $notification['node_url'] . $notification['notify_id'];
                                $notification['message'] = __("mentioned you in a comment");
                                break;
                            case 'reply_post':
                                $notification['url'] = $system['system_url'] . '/posts/' . $notification['node_url'] . $notification['notify_id'];
                                $notification['message'] = __("mentioned you in a reply");
                                break;
                            case 'reply_photo':
                                $notification['url'] = $system['system_url'] . '/photos/' . $notification['node_url'] . $notification['notify_id'];
                                $notification['message'] = __("mentioned you in a reply");
                                break;
                        }
                        break;
                    case 'profile_visit':
                        $notification['icon'] = "fa-eye";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = __("visited your profile");
                        break;
                    case 'wall':
                        $notification['icon'] = "fa-comment";
                        $notification['url'] = $system['system_url'] . '/posts/' . $notification['node_url'];
                        $notification['message'] = __("posted on your wall");
                        break;
                    case 'page_invitation':
                        $notification['icon'] = "fa-flag";
                        $notification['url'] = $system['system_url'] . '/pages/' . $notification['node_url'];
                        $notification['message'] = __("invite you to like a page") . " '" . $notification['node_type'] . "'";
                        break;
                    case 'group_join':
                        $notification['icon'] = "fa-users";
                        $notification['url'] = $system['system_url'] . '/groups/' . $notification['node_url'] . '/settings/requests';
                        $notification['message'] = __("asked to join your group") . " '" . $notification['node_type'] . "'";
                        break;
                    case 'group_add':
                        $notification['icon'] = "fa-users";
                        $notification['url'] = $system['system_url'] . '/groups/' . $notification['node_url'];
                        $notification['message'] = __("added you to") . " '" . $notification['node_type'] . "' " . __("group");
                        break;
                    case 'group_accept':
                        $notification['icon'] = "fa-users";
                        $notification['url'] = $system['system_url'] . '/groups/' . $notification['node_url'];
                        $notification['message'] = __("accepted your request to join") . " '" . $notification['node_type'] . "' " . __("group");
                        break;
                    case 'event_invitation':
                        $notification['icon'] = "fa-calendar";
                        $notification['url'] = $system['system_url'] . '/events/' . $notification['node_url'];
                        $notification['message'] = __("invite you to join an event") . " '" . $notification['node_type'] . "'";
                        break;
                    case 'live':
                        $notification['icon'] = "Icon--cameraVideo";
                        $notification['url'] = $system['system_url'] . '/video_post?post_id=' . $notification['node_url'];
                        $notification['message'] = __("started a live video");
                        break;
                    case 'invite':
                        $data = [
                          'accessToken' => $this->_data['accessToken'],
                          'inviteToken' => $notification['node_url']
                        ];
                        $encrypt_data = urlencode(self::key_encrypt(json_encode($data)));
                        $notification['icon'] = "Icon--cameraVideo";
                        $notification['url'] = SYS_URL_API . '/live-broadcast/setup-broadcast.php?action=invite&token=' . $encrypt_data;
                        $notification['message'] = __("has just invited you to join a broadcast event!");
                        break;
                }
                if(isset($notification)){
                    $notification['full_message'] = $notification['user_name'] . " " . $notification['message'];
                    $notifications[] = $notification;             
                }
				//Formatted Time EST
				if($notifications[$count]['time'] != ''){
					$notifications[$count]['formatted_time'] = __($this->time_elapsed_string($notifications[$count]['time'], $full = false));
				}
				$count++;
				
				if($notification['action'] == 'like' && $notification['node_type'] == 'post'){
					$likes_counter++;
				}
				if($notification['action'] == 'like' && $notification['node_type'] == 'photo'){
					$likes_counter_photo++;
				}	
				//Formatted Time EST End
            }
        }
		
        return $notifications;
    }

    public function comment_api($user_id, $handle, $node_id, $message, $photo, $video)
    {
        global $db, $system, $date;
			$user = $this->get_user_api($user_id);
			$this->_data = $user['response'];
        $comment = [];
        /* default */
        $comment['node_id'] = $node_id;
        $comment['node_type'] = $handle;
        $comment['text'] = $message;
        $comment['image'] = $photo;
		$comment['video'] = $video;
        $comment['time'] = $date;
        $comment['likes'] = 0;
        $comment['replies'] = 0;
        /* check the handle */
        switch ($handle) {
            case 'post':
                /* (check|get) post */
                $post = $this->_check_post($node_id, true);
                if (!$post) {
					return $response = array('status'=>'ERROR', 'message'=>__('Post does not exist'), 'response' => null);
                    //_error(403);
                }
                break;
            case 'photo':
                /* (check|get) photo */
                $photo = $this->get_photo($node_id);
                if (!$photo) {
					return $response = array('status'=>'ERROR', 'message'=>__('Post does not exist'), 'response' => null);
                    //_error(403);
                }
                $post = $photo['post'];
                break;
            case 'comment':
                /* (check|get) comment */
                $parent_comment = $this->get_comment_api($node_id, false);
                //echo $node_id.'<hr>';
                //var_dump($parent_comment); exit;
                if (!$parent_comment) {
					return $response = array('status'=>'ERROR', 'message'=>__('Comment Does not exist.'), 'response' => null);
                    //_error(403);
                }
                $post = $parent_comment['post'];
                break;
        }
        /* check if there is any blocking between the viewer & the target */
        if ($this->blocked($post['author_id']) || ($handle == "comment" && $this->blocked($parent_comment['author_id']))) {
            return $response = array('status'=>'ERROR', 'message'=>__('User is blocked.'), 'response' => null);
        }
        /* check if the viewer is page admin of the target post */
        if (!$post['is_page_admin']) {
            $comment['user_id'] = $user_id;
            $comment['user_type'] = "user";
            $comment['author_picture'] = $this->_data['user_picture'];
            $comment['author_url'] = $system['system_url'] . '/' . $this->_data['user_name'];
            $comment['author_user_name'] = $this->_data['user_name'];
            $comment['author_name'] = $this->_data['user_firstname'] . " " . $this->_data['user_lastname'];
            $comment['author_verified'] = $this->_data['user_verified'];
        } else {
            $comment['user_id'] = $post['page_id'];
            $comment['user_type'] = "page";
            $comment['author_picture'] = $this->get_picture($post['page_picture'], "page");
            $comment['author_url'] = $system['system_url'] . '/pages/' . $post['page_name'];
            $comment['author_name'] = $post['page_title'];
            $comment['author_verified'] = $post['page_verified'];
        }
        /* insert the comment */
        $db->query(sprintf("INSERT INTO posts_comments (node_id, node_type, user_id, user_type, text, image, time, video) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", secure($comment['node_id'], 'int'), secure($comment['node_type']), secure($comment['user_id'], 'int'), secure($comment['user_type']), secure($comment['text']), secure($comment['image']), secure($comment['time']), secure($comment['video']))) or _error(SQL_ERROR_THROWEN);
        //echo "INSERT INTO posts_comments ( node_id, node_type, user_id, user_type, text, image, time) VALUES (".$comment['node_id'].", ".$comment['node_type'].", ".$comment['user_id'].", ".$comment['user_type".'].", ".$comment['text'].", ".$comment['image'].", ".$comment['time'].")"; die;
        $comment['comment_id'] = $db->insert_id;
        /* update (post|photo|comment) (comments|replies) counter */
        switch ($handle) {
            case 'post':
                $db->query(sprintf("UPDATE posts SET comments = comments + 1 WHERE post_id = %s", secure($node_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'photo':
                $db->query(sprintf("UPDATE posts_photos SET comments = comments + 1 WHERE photo_id = %s", secure($node_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'comment':
                $db->query(sprintf("UPDATE posts_comments SET replies = replies + 1 WHERE comment_id = %s", secure($node_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                $db->query(sprintf("UPDATE posts SET comments = comments + 1 WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
        }
        /* post notification */
        if ($handle == "comment") {
            $this->post_notification(['to_user_id' => $parent_comment['author_id'], 'action' => 'reply', 'node_type' => $parent_comment['node_type'], 'node_url' => $parent_comment['node_id'], 'notify_id' => 'comment_' . $comment['comment_id']]);
            if ($post['author_id'] != $parent_comment['author_id']) {
                $this->post_notification(['to_user_id' => $post['author_id'], 'action' => 'comment', 'node_type' => $parent_comment['node_type'], 'node_url' => $parent_comment['node_id'], 'notify_id' => 'comment_' . $comment['comment_id']]);
            }
        } else {
            $this->post_notification(['to_user_id' => $post['author_id'], 'action' => 'comment', 'node_type' => $handle, 'node_url' => $node_id, 'notify_id' => 'comment_' . $comment['comment_id']]);
        }
        /* post mention notifications if any */
        if ($handle == "comment") {
            $this->post_mentions($comment['text'], $parent_comment['node_id'], "reply_" . $parent_comment['node_type'], 'comment_' . $comment['comment_id'], [$post['author_id'], $parent_comment['author_id']]);
        } else {
            $this->post_mentions($comment['text'], $node_id, "comment_" . $handle, 'comment_' . $comment['comment_id'], [$post['author_id']]);
        }
        /* parse text */
        $comment['text_plain'] = htmlentities($comment['text'], ENT_QUOTES, 'utf-8');
        $comment['text'] = $this->_parse($comment['text_plain']);

		/*** Plain Text Code By Abhishek ****/
		$html = strip_tags($post['text']);
		// Convert HTML entities to single characters
		$html = html_entity_decode($html, ENT_QUOTES, 'UTF-8');
		$html = preg_replace("/&#?[a-z0-9]{2,8};/i","",$html);
		$html = filter_var($html, FILTER_SANITIZE_STRING);
		$post['text_plain'] = $html;
		/*** End Plain Text Code By Abhishek ****/


        /* check if viewer can manage comment [Edit|Delete] */
        $comment['edit_comment'] = true;
        $comment['delete_comment'] = true;

		//Formatted Time EST
		if($comment['time'] != ''){
			$comment['formatted_time'] = $this->time_elapsed_string($comment['time'], $full = false);
		}
		//Formatted Time EST End

        /* return */
		return $response = array('status'=>'SUCCESS', 'message'=>'', 'response' => $comment);
        //return $comment;
    }

	public function share_api($user_id, $post_id, $comment)
    {
        global $db, $date;

			$user = $this->get_user_api($user_id);
			$this->_data = $user['response'];
        /* check if the viewer can share the post */
        $post = $this->_check_post($post_id, true);
        //var_dump($post); exit;
        /*if (!$post || $post['privacy'] != 'public') {
            return $response = array('status'=>'ERROR', 'message'=>'Please Check Post ID.', 'response' => null);
        }*/
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            return $response = array('status'=>'ERROR', 'message'=>__('User is Blocked.'), 'response' => null);
        }
        // share post
        /* share the origin post */
        if ($post['post_type'] == "shared") {
            $origin = $this->_check_post($post['origin_id'], true);
            if (!$origin || $origin['privacy'] != 'public') {
                return $response = array('status'=>'ERROR', 'message'=>__('Post is not sharable'), 'response' => null);
            }
            $post_id = $origin['post_id'];
            $author_id = $origin['author_id'];
        } else {
            $post_id = $post['post_id'];
            $author_id = $post['author_id'];
        }
        /* insert the new shared post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, origin_id, time, privacy, text) VALUES (%s, 'user', 'shared', %s, %s, 'public', %s)", secure($user_id, 'int'), secure($post_id, 'int'), secure($date), secure($comment))) or _error(SQL_ERROR_THROWEN);
        $post_id_new = $db->insert_id;
        /* update the origin post shares counter */
        $db->query(sprintf("UPDATE posts SET shares = shares + 1 WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* post notification */
        $this->post_notification(['to_user_id' => $author_id, 'action' => 'share', 'node_type' => 'post', 'node_url' => $post_id]);
		return $response = array('status'=>'SUCCESS', 'message'=>__('Post Shared Successfully.'), 'response' => $this->get_post_api($post_id_new));
    }

    public function settings_api($edit, $args)
    {
        global $db, $system;

        switch ($edit) {
		case 'basic':
                /* validate firstname */
               /* if (is_empty(".$args['firstname'].') ';                  //throw new Exception(__("You must enter full name"));
					return $response = array('status'=>'E
                    if(){

                    }RROR', 'message'=>"You must enter firstname", 'response' => null);
                }*/
                /* check/set custom fields */
                $custom_fields = $this->set_custom_fields($args, false);
                /* update user */ // user_firstname, user_lastname
                $qu = '';
                if(!is_empty($args['firstname'])){
                    $qu .= "user_firstname = '".$args['firstname']."' AND ";
                }
                if(!is_empty($args['firstname'])){
                    $qu .= "user_lastname = '".$args['lastname']."' AND ";
                }
                if(!is_empty($args['email'])){
                    $qu .= "user_email = '".$args['email']."' AND ";
                }
                $db->query(sprintf("UPDATE users SET $qu user_biography = %s WHERE user_id = %s", secure($args['biography']), secure($args['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                return $response = array('status'=>'SUCCESS', 'message'=>__("Information has been updated Successfully."), 'response' => $this->popover_api($args['user_id'], 'user'));
				break;

         case 'email':
                /* validate email */
                if ($args['email'] != $this->_data['user_email']) {
                    $user = $this->get_user_api($args['user_id']);
                    $this->_data = $user['response'];
                    if (!valid_email($args['email'])) {
						return $response = array('status'=>'ERROR', 'message'=>__("Please enter a valid email address"), 'response' => null);
                        //throw new Exception(__("Please enter a valid email address"));
                    }
                    if ($this->check_email($args['email'])) {
                        return $response = array('status'=>'ERROR', 'message'=>__("Sorry, it looks like") . " <strong>" . $args['email'] . "</strong> " . __("belongs to an existing account"), 'response' => null);
						//throw new Exception(__("Sorry, it looks like") . " <strong>" . $args['email'] . "</strong> " . __("belongs to an existing account"));
                    }

                    /* generate activation key */
                    $activation_key = get_hash_token();
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_email = %s, user_activation_key = %s, user_activated = '0' WHERE user_id = %s", secure($args['email']), secure($activation_key), secure($args['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    // send activation email
                    if(!Email::send_activation($this->_data['user_id'], $args['email'], $this->_data['user_firstname'], $this->_data['user_lastname'], $activation_key)){
                      return $response = array('status'=>'ERROR', 'message'=>__("New activation email could not be sent"), 'response' => null);
					  //throw new Exception(__("New activation email could not be sent"));
                    }
					else{
						return $response = array('status'=>'SUCCESS', 'message'=>__("Varification Email has been send successfully."), 'response' => $this->popover_api($args['user_id'], 'user'));
					}
                }
                break;

         case 'send_activation':

                /* validate email */
                $user = $this->get_user_api($args['user_id']);
                    $this->_data = $user['response'];
                if ($args['email'] == $this->_data['user_email']) {

                    if (!valid_email($args['email'])) {
                        return $response = array('status'=>'ERROR', 'message'=>__("Please enter a valid email address"), 'response' => null);
                        //throw new Exception(__("Please enter a valid email address"));
                    }

                    /* generate activation key */
                    $activation_key = get_hash_token();
                    // send activation email
                    if(!Email::send_activation($this->_data['user_id'], $args['email'], $this->_data['user_firstname'], $this->_data['user_lastname'], $activation_key)){
                      return $response = array('status'=>'ERROR', 'message'=>__("New activation email could not be sent"), 'response' => null);
                      //throw new Exception(__("New activation email could not be sent"));
                    }
                    else{
                        return $response = array('status'=>'SUCCESS', 'message'=>__("Varification Email has been send successfully."), 'response' => $this->popover_api($args['user_id'], 'user'));
                    }
                }
                else{
                    return $response = array('status'=>'ERROR', 'message'=>__("Please enter a valid email address"), 'response' => null);
                }
                break;

            case 'password':
                /* validate all fields */
                if ( is_empty($args['new']) || is_empty($args['confirm'])) {
					return $response = array('status'=>'ERROR', 'message'=>__("You must fill in all of the fields"), 'response' => null);
                    //throw new Exception(__("You must fill in all of the fields"));
                }
                /* validate current password (MD5 check for versions < v2.5)
                if (md5($args['current']) != $this->_data['user_password'] && !password_verify($args['current'], $this->_data['user_password'])) {
                    return $response = array('status'=>'ERROR', 'message'=>"Your current password is incorrect", 'response' => null);
					//throw new Exception(__("Your current password is incorrect"));
                }
                /* validate new password */
                if ($args['new'] != $args['confirm']) {
					return $response = array('status'=>'ERROR', 'message'=>__("Your passwords do not match"), 'response' => null);
                    //throw new Exception(__("Your passwords do not match"));
                }
                if (strlen($args['new']) < 6) {
					return $response = array('status'=>'ERROR', 'message'=>__("Password must be at least 6 characters long. Please try another"), 'response' => null);
                    //throw new Exception(__("Password must be at least 6 characters long. Please try another"));
                }
                /* update user */
                $db->query(sprintf("UPDATE users SET user_password = %s WHERE user_id = %s", secure(_password_hash($args['new'])), secure($args['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                return $response = array('status'=>'SUCCESS', 'message'=>__("Password has been set successfully."), 'response' => $this->popover_api($args['user_id'], 'user'));
				break;

			case 'username':
                /* validate username */
                //if (strtolower($args['username']) != strtolower($this->_data['user_name'])) {
                    if (!valid_username($args['username'])) {
						return $response = array('status'=>'ERROR', 'message'=>__("Please enter a valid username (a-z0-9_.) with minimum 3 characters long"), 'response' => null);
                        //throw new Exception(__("Please enter a valid username (a-z0-9_.) with minimum 3 characters long"));
                    }
                    if (reserved_username($args['username'])) {
						return $response = array('status'=>'ERROR', 'message'=>__("You can't use ") . $args['username']  . __(" as username"), 'response' => null);
                        //throw new Exception(__("You can't use") . " <strong>" . $args['username'] . "</strong> " . __("as username"));
                    }
                    if ($this->check_username($args['username'])) {
						return $response = array('status'=>'ERROR', 'message'=>__("Sorry, it looks like ") . $args['username'] .  __(" belongs to an existing account"), 'response' => null);
                        //throw new Exception(__("Sorry, it looks like") . " <strong>" . $args['username'] . "</strong> " . __("belongs to an existing account"));
                    }
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_name = %s WHERE user_id = %s", secure($args['username']), secure($args['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
					return $response = array('status'=>'SUCCESS', 'message'=>__("Username has been changed successfully."), 'response' => $this->popover_api($args['user_id'], 'user'));
               // }
                break;
            case 'privacy':
                $args['privacy_chat'] = ($args['privacy_chat'] == 0) ? 0 : 1;
				//return $args;
                $privacy = ['public', 'follow'];
                if (!in_array($args['user_privacy_message'], $privacy)) {
                    return $response = array('status'=>'ERROR', 'message'=>__("User privacy message not passed properly."), 'response' => null);
                }
			/*	$result = $db->query(sprintf("SELECT `user_privacy_message` FROM users WHERE user_id = %s", secure($args['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
				//$profile = $result->fetch_assoc();
				var_dump($result->num_rows);
				exit;*/
                /* update user */
                $result = $db->query(sprintf("UPDATE users SET  user_privacy_message = %s WHERE user_id = %s", secure($args['user_privacy_message']), secure($args['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

				if($result){
					return $response = array('status'=>'SUCCESS', 'message'=>__("User privacy message set properly."), 'response' => null);
				}
				else{
					return $response = array('status'=>'ERROR', 'message'=>__("Something went wrong."), 'response' => null);
				}
                break;
		}
	}
	public function popover_api($id, $type)
    {
        global $db;
        $profile = [];
        if ($type == "user") {
            $get_profile = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_profile->num_rows > 0) {
                $profile = $get_profile->fetch_assoc();
                $profile['user_picture'] = $this->get_picture($profile['user_picture'], $profile['user_gender']);
                $profile['followers_count'] = count($this->get_followers_ids($profile['user_id']));
                $profile['posts_count'] = count($this->_get_posts($profile['user_id']));
                $profile['followings_count'] = count($this->get_followings_ids($profile['user_id']));
                if ($this->_logged_in && $this->_data['user_id'] != $profile['user_id']) {
                    $profile['mutual_friends_count'] = $this->get_mutual_friends_count($profile['user_id']);
                }
                /*if ($profile['user_id'] != $this->_data['user_id']) {
                    $profile['we_friends'] = (in_array($profile['user_id'], $this->_data['friends_ids'])) ? true : false;
                    $profile['he_request'] = (in_array($profile['user_id'], $this->_data['friend_requests_ids'])) ? true : false;
                    $profile['i_request'] = (in_array($profile['user_id'], $this->_data['friend_requests_sent_ids'])) ? true : false;
                    $profile['i_follow'] = (in_array($profile['user_id'], $this->_data['followings_ids'])) ? true : false;
                }*/
            }
        } else {
            $get_profile = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_profile->num_rows > 0) {
                $profile = $get_profile->fetch_assoc();
                $profile['page_picture'] = User::get_picture($profile['page_picture'], "page");
                $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE page_id = %s AND user_id = %s", secure($id, 'int'), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $profile['i_like'] = true;
                } else {
                    $profile['i_like'] = false;
                }
            }
        }
        return $profile;
    }
	public function Image_Gallery($post_id){

		global $db, $system;
        $posts = [];

		$resultQery = "SELECT posts.post_id FROM posts where posts.post_id = ".$post_id;

		$get_posts = $db->query($resultQery) or _error(SQL_ERROR_THROWEN);

        if ($get_posts->num_rows > 0) {
            while ($post = $get_posts->fetch_assoc()) {
                $post = $this->get_post($post['post_id'], true, true); // $full_details = true, $pass_privacy_check = true
                if ($post) {
                    $posts[] = $post;
                }
            }
        }
        return $posts;
	}
	public function incrementFollowUser($db){
		$get_theme = $db->query("SELECT `boss_followers`,`followers_date` FROM users WHERE users.user_id = '356'") or _error(SQL_ERROR);
		$theme = $get_theme->fetch_assoc();
		return $theme;
	}
    public function get_post_api($post_id, $user_id = null, $full_details = true, $pass_privacy_check = true)
    {
        global $db;
        $post = $this->_check_post_api($post_id, $pass_privacy_check);

        if (!$post) {
            return $response = array('status'=>'ERROR', 'message'=>__('Post does not exist'), 'response' => null);
        }
       // var_dump($post);
        /* post type */

        $postTypes = [
            'album',
            'photos',
            'profile_picture',
            'profile_cover',
            'page_picture',
            'page_cover',
            'group_picture',
            'group_cover',
            'event_cover',
        ];
//var_dump($post['post_type']); exit;
        if (in_array($post['post_type'],$postTypes)) {
            /* get photos */
            $get_photos = $db->query(sprintf("SELECT * FROM posts_photos WHERE post_id = %s ORDER BY photo_id ASC", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            $post['photos_num'] = $get_photos->num_rows;
            /* check if photos has been deleted */
            if ($post['photos_num'] == 0) {
                return $response = array('status'=>'ERROR', 'message'=>__('Photo has been deleted.'), 'response' => null);
            }
            while ($post_photo = $get_photos->fetch_assoc()) {
                $post['photos'][] = $post_photo;
            }
            if ($post['post_type'] == 'album') {
                $post['album'] = $this->get_album($post['photos'][0]['album_id'], false);
                if (!$post['album']) {
                    return $response = array('status'=>'ERROR', 'message'=>__('Album does not exist.'), 'response' => null);
                }
            }
        } elseif ($post['post_type'] == 'media') {
            /* get media */
            $get_media = $db->query(sprintf("SELECT * FROM posts_media WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if media has been deleted */
            if ($get_media->num_rows == 0) {
                return $response = array('status'=>'ERROR', 'message'=>__('Media has been deleted.'), 'response' => null);
            }
            $post['media'] = $get_media->fetch_assoc();
        } elseif ($post['post_type'] == 'link') {
            /* get link */
            $get_link = $db->query(sprintf("SELECT * FROM posts_links WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if link has been deleted */
            if ($get_link->num_rows == 0) {
                return $response = array('status'=>'ERROR', 'message'=>__('Link has been deleted.'), 'response' => null);
            }
            $post['link'] = $get_link->fetch_assoc();
        } elseif ($post['post_type'] == 'poll') {
            /* get poll */
            $get_poll = $db->query(sprintf("SELECT * FROM posts_polls WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if video has been deleted */
            if ($get_poll->num_rows == 0) {
                return $response = array('status'=>'ERROR', 'message'=>__('Video has been deleted.'), 'response' => null);
            }
            $post['poll'] = $get_poll->fetch_assoc();
            /* get poll options */
            $get_poll_options = $db->query(sprintf("SELECT option_id, text FROM posts_polls_options WHERE poll_id = %s", secure($post['poll']['poll_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_poll_options->num_rows == 0) {
                return $response = array('status'=>'ERROR', 'message'=>__('There is no poll options.'), 'response' => null);
            }
            while ($poll_option = $get_poll_options->fetch_assoc()) {
                /* get option votes */
                $get_option_votes = $db->query(sprintf("SELECT * FROM users_polls_options WHERE option_id = %s", secure($poll_option['option_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $poll_option['votes'] = $get_option_votes->num_rows;
                /* check if viewer voted */
                $poll_option['checked'] = false;
                if ($this->_logged_in) {
                    $check = $db->query(sprintf("SELECT * FROM users_polls_options WHERE user_id = %s AND option_id = %s", secure($user_id, 'int'), secure($poll_option['option_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    if ($check->num_rows > 0) {
                        $poll_option['checked'] = true;
                    }
                }
                $post['poll']['options'][] = $poll_option;
            }
        } elseif ($post['post_type'] == 'product') {
            /* get product */
            $get_product = $db->query(sprintf("SELECT * FROM posts_products WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if link has been deleted */
            if ($get_product->num_rows == 0) {
                return $response = array('status'=>'ERROR', 'message'=>__('Link has been deleted.'), 'response' => null);
            }
            $post['product'] = $get_product->fetch_assoc();
            /* get photos */
            $get_photos = $db->query(sprintf("SELECT * FROM posts_photos WHERE post_id = %s ORDER BY photo_id DESC", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            $post['photos_num'] = $get_photos->num_rows;
            /* check if photos has been deleted */
            if ($post['photos_num'] > 0) {
                while ($post_photo = $get_photos->fetch_assoc()) {
                    $post['photos'][] = $post_photo;
                }
            }
        } elseif ($post['post_type'] == 'article') {
            /* get article */
            $get_article = $db->query(sprintf("SELECT * FROM posts_articles WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if article has been deleted */
            if ($get_article->num_rows == 0) {
                return $response = array('status'=>'ERROR', 'message'=>__('Article has been deleted.'), 'response' => null);
            }
            $post['article'] = $get_article->fetch_assoc();
            $post['article']['parsed_cover'] = $this->get_picture($post['article']['cover'], 'article');
            $post['article']['title_url'] = get_url_text($post['article']['title']);
            $post['article']['parsed_text'] = htmlspecialchars_decode($post['article']['text'], ENT_QUOTES);
            $post['article']['text_snippet'] = get_snippet_text($post['article']['text']);
            $tags = (!is_empty($post['article']['tags'])) ? explode(',', $post['article']['tags']) : [];
            $post['article']['parsed_tags'] = array_map('get_tag', $tags);
        } elseif ($post['post_type'] == 'video') {
            /* get video */
            $get_video = $db->query(sprintf("SELECT * FROM posts_videos WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if video has been deleted */
            if ($get_video->num_rows == 0) {
                return false;
            }
            $post['video'] = $get_video->fetch_assoc();
        } elseif ($post['post_type'] == 'audio') {
            /* get audio */
            $get_audio = $db->query(sprintf("SELECT * FROM posts_audios WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if audio has been deleted */
            if ($get_audio->num_rows == 0) {
                return false;
            }
            $post['audio'] = $get_audio->fetch_assoc();
        } elseif ($post['post_type'] == 'file') {
            /* get file */
            $get_file = $db->query(sprintf("SELECT * FROM posts_files WHERE post_id = %s", secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if file has been deleted */
            if ($get_file->num_rows == 0) {
                return false;
            }
            $post['file'] = $get_file->fetch_assoc();
        } elseif ($post['post_type'] == 'shared') {

            /* get origin post */
            $post['origin'] = $this->get_post_api($post['origin_id'], true);
            //var_dump($post['origin']); exit;
            /* check if origin post has been deleted */

			//Formatted Time EST
			if($post['origin']['time'] != ''){
				$post['origin']['formatted_time'] = $this->time_elapsed_string($post['origin']['time'], $full = false);
			}
			//Formatted Time EST End

            if (!$post['origin']) {
                return false;
            }
        }
        /* post feeling */
        if (!is_empty($post['feeling_action']) && !is_empty($post['feeling_value'])) {
            if ($post['feeling_action'] != "Feeling") {
                $_feeling_icon = get_feeling_icon($post['feeling_action'], get_feelings());
            } else {
                $_feeling_icon = get_feeling_icon($post['feeling_value'], get_feelings_types());
            }
            if ($_feeling_icon) {
                $post['feeling_icon'] = $_feeling_icon;
            }
        }
        /* parse text */
		//header("Content-Type: text/plain");
        $post['text_plain'] = stripcslashes($post['text']);
        $post['text'] = $this->_parse($post['text_plain']);
		$post['text_plain'] = preg_replace('/&#?[a-z0-9]{2,8};/i', '', $post['text_plain']);

		/*** Plain Text Code By Abhishek ****/
		$html = strip_tags($post['text']);
		// Convert HTML entities to single characters
		$html = html_entity_decode($html, ENT_QUOTES, 'UTF-8');
		$html = preg_replace("/&#?[a-z0-9]{2,8};/i","",$html);
		$html = filter_var($html, FILTER_SANITIZE_STRING);
		$post['text_plain'] = $html;
		/*** End Plain Text Code By Abhishek ****/

        /* check if get full post details */
        if ($full_details) {
            /* get post comments */
            if ($post['comments'] > 0) {
                $post['post_comments'] = $this->get_comments_api($post['post_id'], $user_id, 0, true, true, $post);
            }
        }
            $get_like = $db->query(sprintf("SELECT * FROM posts_likes WHERE `user_id` = %s AND post_id = %s", secure($user_id, 'int'), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* check if article has been deleted */
            if ($get_like->num_rows == 0) {
                $post['i_like'] = false;
            }
			else{
				$post['i_like'] = true;
			}

		//Formatted Time EST
		if($post['time'] != ''){
			$post['formatted_time'] = $this->time_elapsed_string($post['time'], $full = false);
		}
		//Formatted Time EST End

        return $post;
    }

	public function follow_api($user_id, $following_id)
    {
        global $db;
        /* check blocking */
        if ($this->blocked($user_id)) {
            return $response = array('status'=>'ERROR', 'message'=>__('User is blocked.'), 'response' => null);
        }
        /* check if the viewer already follow the target */
        $check = $db->query(sprintf("SELECT * FROM followings WHERE user_id = %s AND following_id = %s", secure($user_id, 'int'), secure($following_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* if yes -> return */
        if ($check->num_rows > 0) {
            return $response = array('status'=>'ERROR', 'message'=>__('You already follower.'), 'response' => null);
        }
        /* add as following */
        $db->query(sprintf("INSERT INTO followings (user_id, following_id) VALUES (%s, %s)", secure($user_id, 'int'), secure($following_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* post new notification */
        $this->post_notification(['to_user_id' => $following_id, 'action' => 'follow']);
		return $response = array('status'=>'SUCCESS', 'message'=>__('You follow successfully.'), 'response' => null);
    }

    public function unfollow_api($user_id, $following_id)
    {
        global $db;
        /* check if the viewer already follow the target */
        $check = $db->query(sprintf("SELECT * FROM followings WHERE user_id = %s AND following_id = %s", secure($user_id, 'int'), secure($following_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* if no -> return */
        if ($check->num_rows == 0) {
            return $response = array('status'=>'ERROR', 'message'=>__('You are not follower.'), 'response' => null);
        }
        /* delete from viewer's followings */
        $db->query(sprintf("DELETE FROM followings WHERE user_id = %s AND following_id = %s", secure($user_id, 'int'), secure($following_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* delete notification */
        $this->delete_notification($user_id, 'follow');
		return $response = array('status'=>'SUCCESS', 'message'=>__('You unfollow successfully.'), 'response' => null);
    }

	public function get_followers_api($user_id, $me_id = '', $limit = 10, $page = 1, $offset = 0)
    {
        global $db, $system;
        $followers = [];
        $offset *= 15;
        if($limit == -1){
            $get_followers = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM followings INNER JOIN users ON (followings.user_id = users.user_id) WHERE followings.following_id = %s ', secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        }
        else{
            $get_followers = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM followings INNER JOIN users ON (followings.user_id = users.user_id) WHERE followings.following_id = %s '." LIMIT " . ( ( $page - 1 ) * $limit ) . ", $limit", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        }

        if ($get_followers->num_rows > 0) {
            while ($follower = $get_followers->fetch_assoc()) {
                $follower['user_picture'] = $this->get_picture($follower['user_picture'], $follower['user_gender']);
                /* get the connection between the viewer & the target */
                $follower['connection'] = $this->connection($follower['user_id'], false);
                if($me_id != ''){
                    $arr = $this->get_followings_status_api($me_id, $follower['user_id']);
                    $follower['is_following'] = $arr['response'];
                }
                $followers[] = $follower;
            }
			return $response = array('status'=>'SUCCESS', 'message'=>__('No messages'), 'response' => $followers);
        }
		else{
			return $response = array('status'=>'ERROR', 'message'=>__('You have no followers.'), 'response' => null);
		}
    }

    public function get_followings_status_api($user_id, $following_id = '', $limit = 10, $page = 1, $offset = 0)
    {
        global $db, $system;
        $followings = [];
        $offset *= 15;
        if($following_id != ''){
            $get_followings = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM followings INNER JOIN users ON (followings.following_id = users.user_id) WHERE followings.user_id = %s AND users.user_id = %s AND users.user_banned="0" '." LIMIT " . ( ( $page - 1 ) * $limit ) . ", $limit", secure($user_id, 'int'), secure($following_id, 'int'), secure($offset, 'int', false), secure(15, 'int', false))) or _error(SQL_ERROR_THROWEN);
            if ($get_followings->num_rows > 0){
                return $response = array('status'=>'SUCCESS', 'message'=>__('Requested id following you.'), 'response' => true);
            }
            else{
                return $response = array('status'=>'ERROR', 'message'=>__('Requested id is not following you.'), 'response' => false);
            }
        }
    }

    public function get_followings_api($user_id, $following_id = '', $limit = 10, $page = 1, $offset = 0)
    {
        global $db, $system;
        $followings = [];
        $offset *= 15;
      /*  if($following_id != ''){
            $get_followings = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM followings INNER JOIN users ON (followings.following_id = users.user_id) WHERE followings.user_id = %s AND users.user_id = %s AND users.user_banned="0" '." LIMIT " . ( ( $page - 1 ) * $limit ) . ", $limit", secure($user_id, 'int'), secure($following_id, 'int'), secure($offset, 'int', false), secure(15, 'int', false))) or _error(SQL_ERROR_THROWEN);
            if ($get_followings->num_rows > 0){
                return $response = array('status'=>'SUCCESS', 'message'=>'Requested id following you.', 'response' => true);
            }
            else{
                return $response = array('status'=>'ERROR', 'message'=>'Requested id is not following you.', 'response' => false);
            }
        }*/
    //    else{
        if($limit == -1){
            $get_followings = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM followings INNER JOIN users ON (followings.following_id = users.user_id) WHERE followings.user_id = %s AND users.user_banned="0" ', secure($user_id, 'int'), secure($offset, 'int', false), secure(15, 'int', false))) or _error(SQL_ERROR_THROWEN);
        }
        else{
            $get_followings = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM followings INNER JOIN users ON (followings.following_id = users.user_id) WHERE followings.user_id = %s AND users.user_banned="0" '." LIMIT " . ( ( $page - 1 ) * $limit ) . ", $limit", secure($user_id, 'int'), secure($offset, 'int', false), secure(15, 'int', false))) or _error(SQL_ERROR_THROWEN);
        }
     //   }

        if ($get_followings->num_rows > 0) {
            while ($following = $get_followings->fetch_assoc()) {
                $following['user_picture'] = $this->get_picture($following['user_picture'], $following['user_gender']);
                /* get the connection between the viewer & the target */
                $following['connection'] = $this->connection($following['user_id'], false);
                if($following_id != ''){
                    $arr = $this->get_followings_status_api($following_id, $following['user_id']);
                    $following['is_following'] = $arr['response'];
                }
                $followings[] = $following;
            }
			return $response = array('status'=>'SUCCESS', 'message'=>__('No messages'), 'response' => $followings);
        }
		else{
			return $response = array('status'=>'ERROR', 'message'=>__('You have no followings.'), 'response' => null);
		}
    }

	/*
	** get post likes by user for API
	*/
	public function get_likes_posts_api($user_id, $limit = 10, $page = 1)
    {
        global $db;
        /* initialize vars */
        $posts = array();
		$post_ids = array();
        /* get posts */
        $get_posts = $db->query(sprintf("SELECT post_id FROM  posts_likes WHERE user_id = %s ORDER BY posts_likes.post_id DESC "." LIMIT " . ( ( $page - 1 ) * $limit ) . ", $limit", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_posts->num_rows > 0) {
            while ($post = $get_posts->fetch_assoc()) {
				//$post_ids = $this->get_post_api($post['post_id']);
				$current_post = $this->get_post_api($post['post_id'],$user_id);
				unset($current_post['post_comments']);
				if($current_post['status'] != 'ERROR'){

					//Formatted Time EST
					if($current_post['time'] != ''){
						$current_post['formatted_time'] = $this->time_elapsed_string($current_post['time'], $full = false);
					}
					//Formatted Time EST End

					$post_ids[] = $current_post;
				}
            }
			if(count($post_ids)){
				return $response = array('status'=>'SUCCESS', 'message'=>__('No messages'), 'response' => $post_ids);
			}
			else{
				return $response = array('status'=>'ERROR', 'message'=>__('You have no liked posts.'), 'response' => null);
			}
        }
		else{
			return $response = array('status'=>'ERROR', 'message'=>__('You have no liked posts.'), 'response' => null);
		}
    }

	public function delete_post_api($post_id)
    {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id, true);
        if (!$post) {
            return $response = array('status'=>'ERROR', 'message'=>__('Post does not exist'), 'response' => null);
        }
        /* delete the post
        if (!$post['manage_post']) {
            return $response = array('status'=>'ERROR', 'message'=>'Post does not exist 2.', 'response' => null);
        }*/
        /* delete post */
        $db->query(sprintf("DELETE FROM posts WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* delete post photos (if any) */
        $db->query(sprintf("DELETE FROM posts_photos WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* delete cover & profile pictures (if any) */
        $refresh = true;
        switch ($post['post_type']) {
            case 'profile_cover':
                /* update user cover */
                //$db->query(sprintf("UPDATE users SET user_cover = null, user_cover_id = null WHERE user_id = %s", secure($post['author_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* return */
                $refresh = true;
                break;
            case 'profile_picture':
                /* update user picture */
                //$db->query(sprintf("UPDATE users SET user_picture = null, user_picture_id = null WHERE user_id = %s", secure($post['author_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* return */
                $refresh = true;
                break;
            case 'page_cover':
                /* update page cover */
                $db->query(sprintf("UPDATE pages SET page_cover = null, page_cover_id = null WHERE page_id = %s", secure($post['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* return */
                $refresh = true;
                break;
            case 'page_picture':
                /* update page picture */
                $db->query(sprintf("UPDATE pages SET page_picture = null, page_picture_id = null WHERE page_id = %s", secure($post['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* return */
                $refresh = true;
                break;
            case 'group_cover':
                /* update group cover */
                $db->query(sprintf("UPDATE groups SET group_cover = null, group_cover_id = null WHERE group_id = %s", secure($post['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* return */
                $refresh = true;
                break;
            case 'group_picture':
                /* update group picture */
                $db->query(sprintf("UPDATE groups SET group_picture = null, group_picture_id = null WHERE group_id = %s", secure($post['group_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* return */
                $refresh = true;
                break;
            case 'event_cover':
                /* update event cover */
                $db->query(sprintf("UPDATE events SET event_cover = null, event_cover_id = null WHERE event_id = %s", secure($post['event_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                /* return */
                $refresh = true;
                break;
        }
        if($refresh){
			return $response = array('status'=>'SUCCESS', 'message'=>__('Post has been removed successfully.'), 'response' => null);
		}
		else{
			return $response = array('status'=>'ERROR', 'message'=>__('Something went wrong.'), 'response' => null);
		}
    }
    public function hide_post_api($user_id, $post_id)
    {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id, true);
        if (!$post) {
            return $response = array('status'=>'ERROR', 'message'=>__('Post does not exist'), 'response' => null);
        }
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            return $response = array('status'=>'ERROR', 'message'=>__('Post author is blocked.'), 'response' => null);
        }
		$get_posts = $db->query(sprintf("SELECT * FROM posts_hidden WHERE user_id = %s AND post_id= %s", secure($user_id, 'int'), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
		if ($get_posts->num_rows > 0) {
			return $response = array('status'=>'ERROR', 'message'=>__('Post already muted.'), 'response' => null);
		}
		else{
			 /* hide the post */
			$db->query(sprintf("INSERT INTO posts_hidden (user_id, post_id) VALUES (%s, %s)", secure($user_id, 'int'), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
			return $response = array('status'=>'SUCCESS', 'message'=>__('Post has been muted successfully.'), 'response' => null);
		}
    }
    public function unhide_post_api($user_id, $post_id)
    {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id, true);
        if (!$post) {
            return $response = array('status'=>'ERROR', 'message'=>__('Post does not exist'), 'response' => null);
        }
		$get_posts = $db->query(sprintf("SELECT * FROM posts_hidden WHERE user_id = %s AND post_id= %s", secure($user_id, 'int'), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
		if ($get_posts->num_rows > 0) {
			/* unhide the post */
			$db->query(sprintf("DELETE FROM posts_hidden WHERE user_id = %s AND post_id = %s", secure($user_id, 'int'), secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
			return $response = array('status'=>'SUCCESS', 'message'=>__('Post has been unmuted successfully.'), 'response' => null);
		}
		else{
			return $response = array('status'=>'ERROR', 'message'=>__('Post does not muted.'), 'response' => null);
		}
    }
	public function change_picture_api($user_id, $picture, $type){
		global $db;
		switch ($type) {
			case 'profile_cover':
				/*update user cover*/
				$result = $db->query(sprintf("UPDATE users SET user_cover = '$picture' WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
				if($result){
					$response = $this->get_user_api($user_id);
					return array('status' => "SUCCESS", 'message' => "Profile cover has been changed successfully.", 'response' => $response['response']);
				}
				else{
					return array('status' => "ERROR", 'message' => "Please check user_id.", 'response' => null);
				}
			break;
		case 'profile_picture':
				/* update user picture */
				$result = $db->query(sprintf("UPDATE users SET user_picture = '$picture' WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
				if($result){
					$response = $this->get_user_api($user_id);
					return array('status' => "SUCCESS", 'message' => "Profile picture has been changed successfully.", 'response' => $response['response']);
				}
				else{
					return array('status' => "ERROR", 'message' => "Please check user_id.", 'response' => null);
				}
			break;

		default:
		return array('status' => "ERROR", 'message' => "Please check inputs properly.", 'response' => null);
		break;
		}
	}
	public function get_user_api($user_id, $me_id = ''){
		global $db;
		$get_profile = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s", secure($user_id))) or _error(SQL_ERROR_THROWEN);
        if ($get_profile->num_rows > 0) {
			$result = $get_profile->fetch_assoc();
                if($me_id != ''){
                    $arr = $this->get_followings_status_api($me_id, $user_id);
                    $result['is_following'] = $arr['response'];
                }
            //var_dump($result); exit;
			return array('status' => "SUCCESS", 'message' => "", 'response' => $result);
		}
		else{
			return array('status' => "ERROR", 'message' => "Please check user_id properly.", 'response' => null);
		}
	}
    public function blocked_api($user_id, $other_user)
    {
        global $db;
        /* check if there is any blocking between the viewer & the target */
		$check = $db->query(sprintf('SELECT * FROM users_blocks WHERE (user_id = %1$s AND blocked_id = %2$s) OR (user_id = %2$s AND blocked_id = %1$s)', secure($user_id, 'int'), secure($other_user, 'int'))) or _error(SQL_ERROR_THROWEN);
		if ($check->num_rows > 0) {
			return true;
		}
        return false;
    }
    public function blocked_user_list_api($user_id)
    {
        global $db;
        /* check if there is any blocking between the viewer & the target */
		$check = $db->query(sprintf('SELECT `blocked_id` FROM users_blocks WHERE user_id = %s ', secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
		$blocked_users = [];
		if ($check->num_rows > 0) {
            while ($blocked = $check->fetch_assoc()) {
				$user = $this->get_user_api($blocked['blocked_id']);
				if($user['status'] != 'ERROR'){
					$blocked_users[] = $user['response']; //var_dump($user); exit;
				}
            }
			return array('status' => "SUCCESS", 'message' => "", 'response' => $blocked_users);
		}
		else{
			return array('status' => "ERROR", 'message' => __("You didn't block any user."), 'response' => null);
		}
    }
    public function connect_api($do, $user_id, $id, $uid = null)
    {
        global $db;
        switch ($do) {
            case 'block':
                /* check blocking */
                if ($this->blocked_api($user_id, $id)) {
					return array('status' => "ERROR", 'message' => __("You have already blocked this user before!"), 'response' => null);
                    //throw new Exception(__("You have already blocked this user before!"));
                }
                /* remove any friendship */
                $this->connect_api('friend-remove', $user_id, $id);
                /* delete the target from viewer's followings */
                $this->unfollow_api($user_id, $id);
                /* delete the viewer from target's followings */
                $db->query(sprintf("DELETE FROM followings WHERE user_id = %s AND following_id = %s", secure($id, 'int'), secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* block the user */
                $db->query(sprintf("INSERT INTO users_blocks (user_id, blocked_id) VALUES (%s, %s)", secure($user_id, 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
				return array('status' => "SUCCESS", 'message' => "You have blocked user successfully!", 'response' => null);
                //$this->delete_comment_on_block($id);
                break;
            case 'unblock':
                /* check blocking */
                if (!$this->blocked_api($user_id, $id)) {
                    return array('status' => "ERROR", 'message' => "You have not blocked this user!", 'response' => null);
                }
                /* unblock the user */
                $db->query(sprintf("DELETE FROM users_blocks WHERE user_id = %s AND blocked_id = %s", secure($user_id, 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                return array('status' => "SUCCESS", 'message' => "You have unblocked user successfully!", 'response' => null);
				break;
            case 'friend-remove':
                /* check if there is any relation between me & him */
                $check = $db->query(sprintf('SELECT * FROM friends WHERE (user_one_id = %1$s AND user_two_id = %2$s AND status = 1) OR (user_one_id = %2$s AND user_two_id = %1$s AND status = 1)', secure($user_id, 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if ($check->num_rows == 0) {
                    return;
                }
                /* delete this friend */
                $db->query(sprintf('DELETE FROM friends WHERE (user_one_id = %1$s AND user_two_id = %2$s AND status = 1) OR (user_one_id = %2$s AND user_two_id = %1$s AND status = 1)', secure($user_id, 'int'), secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
		}
	}
    public function report_api($user_id, $id, $handle, $reason = '')
    {
        global $db, $date;
        switch ($handle) {
            case 'user':
                /* check the user */
                $check = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'page':
                /* check the page */
                $check = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'group':
                /* check the group */
                $check = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'event':
                /* check the event */
                $check = $db->query(sprintf("SELECT * FROM events WHERE event_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'post':
                /* check the post */
                $check = $db->query(sprintf("SELECT * FROM posts WHERE post_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'comment':
                /* check the comment */
                $check = $db->query(sprintf("SELECT * FROM posts_comments WHERE comment_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
                break;
            default:
                _error(403);
                break;
        }
        /* check node */
        if ($check->num_rows == 0) {
            return array('status' => "ERROR", 'message' => "Your $handle does not exist.!", 'response' => null);
        }
        /* check old reports */
        $check = $db->query(sprintf("SELECT * FROM reports WHERE user_id = %s AND node_id = %s AND node_type = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'), secure($handle))) or _error(SQL_ERROR_THROWEN);
        if ($check->num_rows > 0) {
			return array('status' => "ERROR", 'message' => "You have already reported this before!", 'response' => null);
            //throw new Exception(__("You have already reported this before!"));
        }
        /* report */
        $db->query(sprintf("INSERT INTO reports (user_id, node_id, node_type, time, reason) VALUES (%s, %s, %s, %s, %s)", secure($this->_data['user_id'], 'int'), secure($id, 'int'), secure($handle), secure($date), secure($reason))) or _error(SQL_ERROR_THROWEN);
		return array('status' => "SUCCESS", 'message' => "Your report has been submited successfully!", 'response' => null);
    }

	public function get_counts_api($user_id){
        global $db;
        $get_posts = $db->query(sprintf("SELECT `post_id` FROM posts WHERE is_broadcast = '0' AND user_id = %s ", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        $get_followers = 0;
        $a = $this->incrementFollowUser($db);
        if($a['followers_date'] != date("Y-m-d")){
            if(date(l) == 'Saturday' || date(l) == 'Sunday'){
                $incr = 4000;
            }
            else{
                $incr = 3000;
            }
            $now = $a['boss_followers'] + $incr;
            $cur_date = date("Y-m-d");
            $status = $db->query("UPDATE users SET `boss_followers` = $now,`followers_date` = CURDATE() WHERE users.user_id = '356'") or _error(SQL_ERROR);
        }
        $totoalFollowers = $a['boss_followers'] + count($this->get_followers_ids(356));
        //echo $user_id;
        if($user_id == 356){
            $get_followers = $totoalFollowers;
        }
        else{
            $get_followers = count($this->get_followers_ids($user_id));
        }
        //$get_followers = $db->query(sprintf('SELECT users.user_id FROM followings INNER JOIN users ON (followings.user_id = users.user_id) WHERE followings.following_id = %s ', secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        $get_followings = $db->query(sprintf('SELECT users.user_id FROM followings INNER JOIN users ON (followings.following_id = users.user_id) WHERE followings.user_id = %s AND users.user_banned="0" ', secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        $get_likes = $db->query(sprintf("SELECT post_id FROM  posts_likes WHERE user_id = %s ORDER BY posts_likes.post_id DESC ", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        $response = array('posts'=> $get_posts->num_rows, 'followers'=> $get_followers, 'followings' => $get_followings->num_rows, 'likes'=> $get_likes->num_rows);
        return array('status' => "SUCCESS", 'message' => "Your report has been submited successfully!", 'response' => $response);
    }

	public function add_post_api($user_id,$post_text,$media_files = ''){
		//var_dump($media_files); exit;
		global $db, $system;
            $user = $this->get_user_api($user_id);
            $this->_data = $user['response'];
		if(count($media_files->video) > 0){

			$post_type = 'video';
		}
		elseif(count($media_files->audio) > 0){

			$post_type = 'audio';
		}
		elseif(count($media_files->photos) > 0){

			$post_type = 'photos';
		}
		else{
			$post_type = '';
		}

		$db->query(sprintf("INSERT INTO posts (`user_id`, `user_type`,`post_type`,`time`,`privacy`,`text`) VALUES (%s,'user','".$post_type."',Now(),'public',%s)", secure($user_id, 'int'),secure($post_text))) or _error(SQL_ERROR_THROWEN);

		$post_id = $db->insert_id;

        if(isset($media_files->video)){
            if(count($media_files->video) > 0){

                foreach($media_files->video as $video){

                    $db->query(sprintf("INSERT INTO posts_videos (post_id, source) VALUES (%s, %s)", secure($post_id, 'int'), secure($video))) or _error(SQL_ERROR_THROWEN);
                }
            }
        }

		if(isset($media_files->audio)){
            if(count($media_files->audio) > 0){

                foreach($media_files->audio as $audio){

                    $db->query(sprintf("INSERT INTO posts_audios (post_id, source) VALUES (%s, %s)", secure($post_id, 'int'), secure($audio))) or _error(SQL_ERROR_THROWEN);
                }
            }
        }

		if(isset($media_files->photos)){
            if(count($media_files->photos) > 0){

                foreach($media_files->photos as $photos){

                    $db->query(sprintf("INSERT INTO posts_photos (post_id, source) VALUES (%s, %s)", secure($post_id, 'int'), secure($photos))) or _error(SQL_ERROR_THROWEN);
                }
            }
        }
        $response = $this->get_post_api($post_id);
        if(isset($response['status'])){
            return $response;
//            exit;
        }
        else{
            $ids = $this->get_followers_ids($user_id);
            foreach($ids as $val){
                $this->post_notification(['to_user_id' => $val, 'action' => 'new_post', 'node_type' => 'post', 'node_url' => $post_id, 'notify_id' => 'post_' . $post_id, 'obj' => $response]);
            }

            return $response = array('status'=>'SUCCESS', 'message'=>__('No messages'), 'response' => $response);
        }
        //return $this->get_post_api($post_id);
	}

    public function get_users_api($query, $user_id, $limit = 10, $page = 1, $skipped_array = [])
    {
        global $db, $system;
        //echo $query; exit;
        $users = [];
        $system['min_results'] = 10;
        if($query == ''){
            $get_users = $db->query(sprintf("SELECT user_id, user_name,sendbird_access_token, user_firstname, user_lastname, user_gender, user_picture FROM users ORDER BY user_firstname ASC "." LIMIT " . ( ( $page - 1 ) * $limit ) . ", $limit",  secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        }
        else{
            if (count($skipped_array) > 0) {
                /* make a skipped list (including the viewer) */
                $skipped_list = implode(',', $skipped_array);
                /* get users */
                $get_users = $db->query(sprintf("SELECT user_id, user_name,sendbird_access_token, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_id NOT IN (%s) AND (user_firstname LIKE %s OR user_lastname LIKE %s) ORDER BY user_firstname ASC "." LIMIT " . ( ( $page - 1 ) * $limit ) . ", $limit", $skipped_list, secure($query, 'search'), secure($query, 'search'), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
            } else {
                /* get users */
                $get_users = $db->query(sprintf("SELECT user_id, user_name,sendbird_access_token, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_firstname LIKE %s OR user_lastname LIKE %s OR user_name LIKE %s ORDER BY user_firstname ASC "." LIMIT " . ( ( $page - 1 ) * $limit ) . ", $limit", secure($query, 'search'), secure($query, 'search'), secure($query, 'search'), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
            }
        }
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                if($user['user_id'] != ''){
                    $arr = $this->get_followings_status_api($user_id, $user['user_id']);
                    $user['is_following'] = $arr['response'];
                }
                $users[] = $user;
            }
        }
        else{
            return $response = array('status'=>'ERROR', 'message'=>__('No matching users found.'), 'response'=>null);
        }
        //var_dump($users); exit;
        return $response = array('status'=>'SUCCESS', 'message'=>__('No messages'), 'response'=>$users);
    }

public function search_api($query, $get, $filter, $offset = 1)
    {
        global $db, $system;
        $results = [];
        $offset = $_REQUEST['ofset'];
        $offset *= $system['max_results'];
        $block_user_string = 0;
        $query = $_REQUEST['query'];
        $posts = $this->get_posts(['query' => $query, 'get' => $_POST['get'], 'filter' => $_POST['filter'], 'offset' => $offset]);
        if (count($posts) > 0) {
            $results['posts'] = $posts;
        }
        $block_user_string = 0;
        $login_user_id = $this->_data['user_id'];
        $results['custom_login_user_id'] = $login_user_id;
        $get_block_users = $db->query(sprintf('SELECT * FROM users_blocks WHERE  user_id = %s OR blocked_id = %s', $login_user_id, $login_user_id)) or _error(SQL_ERROR_THROWEN);
        if ($get_block_users->num_rows > 0) {
            while ($blockusers = $get_block_users->fetch_assoc()) {
                $block_users_array[] = $blockusers['blocked_id'];
                $block_users_array[] = $blockusers['user_id'];
            }
            $block_user_string = implode(',', $block_users_array);
        }
        $get_users = $db->query(sprintf('SELECT user_id, user_name,user_banned, user_firstname, user_lastname, user_gender, user_picture, user_cover,user_biography FROM users WHERE user_banned ="0" and ( user_name LIKE %1$s OR user_firstname LIKE %1$s OR user_lastname LIKE %1$s ) AND ( user_id NOT IN (%4$s) ) ORDER BY periority desc, user_firstname ASC LIMIT %2$s, %3$s', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false), $block_user_string)) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                if ($user['user_banned'] == '0') {
                    $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                    $user['connection'] = $this->connection($user['user_id']);
                    $user['following'] = $this->checkfollowing($this->_data['user_id'], $user['user_id']);
                    $results['users'][] = $user;
                }
            }
        }
        $get_limit_users = $db->query(sprintf('SELECT user_id, user_name,user_banned, user_firstname, user_lastname, user_gender, user_picture, user_cover,user_biography FROM users WHERE   user_banned ="0" and ( user_name LIKE %1$s OR user_firstname LIKE %1$s OR user_lastname LIKE %1$s ) AND ( user_id NOT IN (%4$s) )  ORDER BY periority desc,user_firstname ASC LIMIT 6', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false), $block_user_string)) or _error(SQL_ERROR_THROWEN);
        if ($get_limit_users->num_rows > 0) {
            while ($limit_user = $get_limit_users->fetch_assoc()) {
                if ($limit_user['user_banned'] == '0') {
                    $limit_user['user_picture'] = $this->get_picture($limit_user['user_picture'], $limit_user['user_gender']);
                    $limit_user['connection'] = $this->connection($limit_user['user_id']);
                    $limit_user['following'] = $this->checkfollowing($this->_data['user_id'], $limit_user['user_id']);
                    $results['limit_user'][] = $limit_user;
                }
            }
        }
        $get_trends = $db->query(sprintf("SELECT * FROM posts WHERE ( posts.text LIKE %s AND YEARWEEK(`time`,1) >= YEARWEEK(CURDATE(), 1) AND posts.user_type = 'user' AND ( posts.user_id NOT IN (%s) ))", secure($query, 'search'), $block_user_string)) or _error(SQL_ERROR_THROWEN);
        if ($get_trends->num_rows > 0) {
            while ($trends = $get_trends->fetch_assoc()) {
                $trends = $this->get_post($trends['post_id'], true, true);
                if ($trends) {
                    $results['trends'][] = $trends;
                }
            }
        }
        $get_media = $db->query(sprintf("SELECT post_id FROM posts WHERE ( post_type LIKE 'media' AND posts.text LIKE %s) AND ( posts.user_id NOT IN (%s) )", secure($query, 'search'), $block_user_string)) or _error(SQL_ERROR_THROWEN);
        if ($get_media->num_rows > 0) {
            while ($media = $get_media->fetch_assoc()) {
                $media = $this->get_post($media['post_id'], true, true);
                if ($media) {
                    $results['media'][] = $media;
                }
            }
        }
        $get_limit_media = $db->query(sprintf("SELECT post_id FROM posts WHERE (post_type LIKE 'media' AND posts.text LIKE %s) AND ( posts.user_id NOT IN (%s) ) LIMIT %s", secure($query, 'search'), $block_user_string, secure('1', 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_limit_media->num_rows > 0) {
            while ($limit_media = $get_limit_media->fetch_assoc()) {
                $limit_media = $this->get_post($limit_media['post_id'], true, true);
                if ($limit_media) {
                    $results['limit_media'][] = $limit_media;
                }
            }
        }
        $get_photos = $db->query(sprintf("SELECT post_id FROM posts WHERE ( post_type LIKE 'photos' AND posts.text LIKE %s) AND ( posts.user_id NOT IN (%s) )", secure($query, 'search'), $block_user_string)) or _error(SQL_ERROR_THROWEN);
        if ($get_photos->num_rows > 0) {
            while ($photos = $get_photos->fetch_assoc()) {
                $photos = $this->get_post($photos['post_id'], true, true);
                if ($photos) {
                    $results['photos'][] = $photos;
                }
            }
        }
        $get_limit_photos = $db->query(sprintf("SELECT post_id FROM posts WHERE ( post_type LIKE 'photos' AND posts.text LIKE %s) AND ( posts.user_id NOT IN (%s) ) LIMIT %s", secure($query, 'search'), $block_user_string, secure('1', 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_limit_photos->num_rows > 0) {
            while ($limit_photos = $get_limit_photos->fetch_assoc()) {
                $limit_photos = $this->get_post($limit_photos['post_id'], true, true);
                if ($limit_photos) {
                    $results['limit_photos'][] = $limit_photos;
                }
            }
        }
        return $results;
    }

    public function _search_posts_api($user_id, $handle = '', $keyword='all', $limit = 10, $page = 1)
    {
        ini_set('display_errors', '0');
        global $db;
            $arr = array();
            $get_posts = '';
            $limit_sql = '';
            $post_type = '';
            $keyword = strtolower($keyword);
           /* $user_ids = " ( user_id = $user_id";
            $vr = 0;
            foreach($posts_follo['response'] as $key => $following){
                if($vr == 0){
                    $user_ids .= " OR ";
                    $vr = 1;
                }
                if(count($posts_follo['response'])-1 == $key){
                    $user_ids .= " user_id = ".$following['user_id'].' ) ';
                }
                else{
                    $user_ids .= " user_id = ".$following['user_id'].' OR ';
                }

            }*/

            if($limit == '-1'){
                $limit_sql = '';
            }
            else{
                $limit_sql = " LIMIT " . ( ( $page - 1 ) * $limit ) . ", $limit";
            }
            if($handle == 'global'){
                if($keyword == 'all'){
                    $get_posts = $db->query(sprintf("SELECT `post_id` FROM `posts` WHERE  `is_broadcast` = '0' ORDER BY post_id DESC".$limit_sql, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
                else{
                    $get_posts = $db->query("SELECT `post_id` FROM `posts` WHERE LCASE(text) LIKE '%$keyword%' AND is_broadcast = '0'  ORDER BY post_id DESC".$limit_sql);// exit;
       //             $get_posts = $db->query(sprintf("SELECT `post_id` FROM `posts` WHERE ( `post_type` LIKE '$handle' AND `text` LIKE '%$keyword%') AND is_broadcast = '0' AND user_id = $user_id ORDER BY post_id DESC ".$limit_sql, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
            }
            elseif($handle == 'mymedia'){
                if($keyword == 'all'){
                    $get_posts = $db->query(sprintf("SELECT `post_id` FROM `posts` WHERE  ( `post_type` LIKE 'photos' OR  `post_type` LIKE 'video' ) AND `is_broadcast` = '0' AND user_id = %s ORDER BY post_id DESC".$limit_sql, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
                else{
                    $get_posts = $db->query("SELECT `post_id` FROM `posts` WHERE ( `post_type` LIKE 'photos' OR  `post_type` LIKE 'video' ) AND LCASE(text) LIKE '%$keyword%' AND is_broadcast = '0' AND user_id = $user_id ORDER BY post_id DESC".$limit_sql);// exit;
       //             $get_posts = $db->query(sprintf("SELECT `post_id` FROM `posts` WHERE ( `post_type` LIKE '$handle' AND `text` LIKE '%$keyword%') AND is_broadcast = '0' AND user_id = $user_id ORDER BY post_id DESC ".$limit_sql, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
            }
            elseif($handle == 'media'){
                if($keyword == 'all'){
                    $get_posts = $db->query(sprintf("SELECT `post_id` FROM `posts` WHERE  ( `post_type` LIKE 'photos' OR  `post_type` LIKE 'video' ) AND `is_broadcast` = '0' ORDER BY post_id DESC".$limit_sql, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
                else{
                    $get_posts = $db->query("SELECT `post_id` FROM `posts` WHERE ( `post_type` LIKE 'photos' OR  `post_type` LIKE 'video' ) AND LCASE(text) LIKE '%$keyword%' AND is_broadcast = '0'  ORDER BY post_id DESC".$limit_sql);// exit;
       //             $get_posts = $db->query(sprintf("SELECT `post_id` FROM `posts` WHERE ( `post_type` LIKE '$handle' AND `text` LIKE '%$keyword%') AND is_broadcast = '0' AND user_id = $user_id ORDER BY post_id DESC ".$limit_sql, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
            }
            elseif($handle == 'posts'){
                if($keyword == 'all'){
                    $get_posts = $db->query(sprintf("SELECT `post_id` FROM `posts` WHERE `is_broadcast` = '0' ORDER BY post_id DESC".$limit_sql, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
                else{
                    $get_posts = $db->query("SELECT `post_id` FROM `posts` WHERE LCASE(text) LIKE '%$keyword%' AND is_broadcast = '0'  ORDER BY post_id DESC".$limit_sql);// exit;
       //             $get_posts = $db->query(sprintf("SELECT `post_id` FROM `posts` WHERE ( `post_type` LIKE '$handle' AND `text` LIKE '%$keyword%') AND is_broadcast = '0' AND user_id = $user_id ORDER BY post_id DESC ".$limit_sql, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
            }
            else{
                if($keyword == 'all'){
                    $get_posts = $db->query(sprintf("SELECT `post_id` FROM `posts` WHERE  `post_type` LIKE '$handle'  AND `is_broadcast` = '0'  ORDER BY post_id DESC".$limit_sql, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
                else{
                    //echo "SELECT `post_id` FROM `posts` WHERE `post_type` LIKE '$handle' AND `text` LIKE '%$keyword%' AND is_broadcast = '0'  ORDER BY post_id DESC".$limit_sql;
                    $get_posts = $db->query("SELECT `post_id` FROM `posts` WHERE `post_type` LIKE '$handle' AND LCASE(text) LIKE '%$keyword%' AND is_broadcast = '0'  ORDER BY post_id DESC".$limit_sql);// exit;
       //             $get_posts = $db->query(sprintf("SELECT `post_id` FROM `posts` WHERE ( `post_type` LIKE '$handle' AND `text` LIKE '%$keyword%') AND is_broadcast = '0' AND user_id = $user_id ORDER BY post_id DESC ".$limit_sql, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
                }
            }


            if ($get_posts->num_rows > 0) {
                while ($post = $get_posts->fetch_assoc()) {
                    $current_post = $this->get_post_api($post['post_id'],$user_id);
					if($current_post['status'] != 'ERROR'){
						unset($current_post['post_comments']);
						$arr[] = $current_post;
					}                }
            }
        return $arr;
    }

public function get_comment_api($comment_id, $recursive = true)
    {
        global $db;
        /* get comment */
        $get_comment = $db->query(sprintf("SELECT posts_comments.*, pages.page_admin FROM posts_comments LEFT JOIN pages ON posts_comments.user_type = 'page' AND posts_comments.user_id = pages.page_id WHERE posts_comments.comment_id = %s", secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        //echo $comment_id;
        //$comment = $get_comment->fetch_assoc();

        if ($get_comment->num_rows == 0) {
            return false;
        }
        //var_dump($get_comment->num_rows); exit;
        $comment = $get_comment->fetch_assoc();
        /* check if the page has been deleted */
        if ($comment['user_type'] == "page" && !$comment['page_admin']) {
            return false;
        }

        /* get the author */
        $comment['author_id'] = ($comment['user_type'] == "page") ? $comment['page_admin'] : $comment['user_id'];

        /* get post */
        switch ($comment['node_type']) {
            case 'post':
                $post = $this->_check_post($comment['node_id'], true, true);
                //var_dump($post); exit;
                /* check if the post has been deleted */
                if (!$post) {
                    return false;
                }
                $comment['post'] = $post;
                break;
            case 'photo':
                /* (check|get) photo */
                $photo = $this->get_photo($comment['node_id']);
                if (!$photo) {
                    return false;
                }
                $comment['post'] = $photo['post'];
                break;
            case 'comment':
                if (!$recursive) {
                    return false;
                }
                /* (check|get) comment */
                $parent_comment = $this->get_comment($comment['node_id'], false);
                if (!$parent_comment) {
                    return false;
                }
                $comment['parent_comment'] = $parent_comment;
                $comment['post'] = $parent_comment['post'];
                break;
        }
        /* check if viewer user likes this comment */
        if ($comment['likes'] > 0) {
            $check_like = $db->query(sprintf("SELECT * FROM posts_comments_likes WHERE user_id = %s AND comment_id = %s", secure($this->_data['user_id'], 'int'), secure($comment['comment_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            $comment['i_like'] = ($check_like->num_rows > 0) ? true : false;
        }
        return $comment;
    }

 public function get_comments_api($node_id, $me_id = '', $offset = 0, $is_post = true, $pass_privacy_check = true, $post = [])
    {
        global $db, $system;
        $comments = [];
        $offset *= $system['min_results'];
        /* get comments */
        if ($is_post) {
            /* get post comments */
            if (!$pass_privacy_check) {
                /* (check|get) post */
                $post = $this->_check_post($node_id, false);
                if (!$post) {
                    return false;
                }
            }
            /* get post comments */
            $get_comments = $db->query(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'post' AND posts_comments.node_id = %s AND posts_comments.text != '' group by posts_comments.text ORDER BY posts_comments.comment_id DESC ) comments group by comments.text ORDER BY comments.comment_id DESC", secure($node_id, 'int'), secure($offset, 'int', false), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        } else {
            /* get photo comments */
            /* check privacy */
            if (!$pass_privacy_check) {
                /* (check|get) photo */
                $photo = $this->get_photo($node_id);
                if (!$photo) {
                    _error(403);
                }
                $post = $photo['post'];
            }
            /* get photo comments */
            $get_comments = $db->query(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'photo' AND posts_comments.node_id = %s AND posts_comments.text != '' group by posts_comments.text ORDER BY posts_comments.comment_id DESC ) comments group by comments.text ORDER BY comments.comment_id DESC", secure($node_id, 'int'), secure($offset, 'int', false), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        }

        if ($get_comments->num_rows == 0) {
            return $comments;
        }
		$count = 0;
        while ($comment = $get_comments->fetch_assoc()) {

            /* get replies */
            if ($comment['replies'] > 0) {
                $comment['comment_replies'] = $this->get_replies_api($comment['comment_id'], $me_id);
            }
            /* parse text */
            $comment['text_plain'] = $comment['text'];
            $comment['text'] = $this->_parse($comment['text']);

			/*** Plain Text Code By Abhishek ****/
			$html = strip_tags($post['text']);
			// Convert HTML entities to single characters
			$html = html_entity_decode($html, ENT_QUOTES, 'UTF-8');
			$html = preg_replace("/&#?[a-z0-9]{2,8};/i","",$html);
			$html = filter_var($html, FILTER_SANITIZE_STRING);
			$post['text_plain'] = $html;
			/*** End Plain Text Code By Abhishek ****/


            /* get the comment author */
            if ($comment['user_type'] == "user") {
                /* user type */
                $comment['author_id'] = $comment['user_id'];
                $comment['author_picture'] = $this->get_picture($comment['user_picture'], $comment['user_gender']);
                $comment['author_url'] = $system['system_url'] . '/' . $comment['user_name'];
                $comment['author_name'] = $comment['user_firstname'] . " " . $comment['user_lastname'];
                $comment['author_verified'] = $comment['user_verified'];
            } else {
                /* page type */
                $comment['author_id'] = $comment['page_admin'];
                $comment['author_picture'] = $this->get_picture($comment['page_picture'], "page");
                $comment['author_url'] = $system['system_url'] . '/pages/' . $comment['page_name'];
                $comment['author_name'] = $comment['page_title'];
                $comment['author_verified'] = $comment['page_verified'];
            }
            /* check if viewer user likes this comment */
            if ( $comment['likes'] > 0) {
                $check_like = $db->query(sprintf("SELECT * FROM posts_comments_likes WHERE user_id = %s AND comment_id = %s", secure($me_id, 'int'), secure($comment['comment_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $comment['i_like'] = ($check_like->num_rows > 0) ? true : false;
            }
            /* check if viewer can manage comment [Edit|Delete] */
            $comment['edit_comment'] = false;
            $comment['delete_comment'] = false;
            if ($this->_logged_in) {
                /* viewer is (admins|moderators)] */
                if ($this->_data['user_group'] < 3) {
                    $comment['edit_comment'] = true;
                    $comment['delete_comment'] = true;
                }
                /* viewer is the author of comment */
                if ($this->_data['user_id'] == $comment['author_id']) {
                    $comment['edit_comment'] = true;
                    $comment['delete_comment'] = true;
                }
                /* viewer is the author of post || page admin */
                if ($this->_data['user_id'] == $post['author_id']) {
                    $comment['delete_comment'] = true;
                }
                /* viewer is the admin of the group of the post */
                if ($post['in_group'] && $post['is_group_admin']) {
                    $comment['delete_comment'] = true;
                }
                /* viewer is the admin of the event of the post */
                if ($post['in_group'] && $post['is_event_admin']) {
                    $comment['delete_comment'] = true;
                }
            }
            $comments[] = $comment;
			//Formatted Time EST
			if($comments[$count]['time'] != ''){
				$comments[$count]['formatted_time'] = $this->time_elapsed_string($comments[$count]['time'], $full = false);
			}
			$count++;
			//Formatted Time EST End
        }
        return $comments;
    }

    public function get_replies_api($comment_id, $me_id = '', $offset = 0, $pass_check = true)
    {
        global $db, $system;
        $replies = [];
        $offset *= $system['min_results'];
        if (!$pass_check) {
            $comment = $this->get_comment($comment_id);
            if (!$comment) {
                _error(403);
            }
        }
        /* get replies */
        $get_replies = $db->query(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'comment' AND posts_comments.node_id = %s AND posts_comments.text != '' group by posts_comments.text ORDER BY posts_comments.comment_id DESC LIMIT %s, %s) comments group by comments.text ORDER BY comments.comment_id DESC", secure($comment_id, 'int'), secure($offset, 'int', false), secure($system['min_results'], 'int', false))) or _error(SQL_ERROR_THROWEN);
        if ($get_replies->num_rows == 0) {
            return $replies;
        }
        while ($reply = $get_replies->fetch_assoc()) {
            /* parse text */
            $reply['text_plain'] = $reply['text'];
            $reply['text'] = $this->_parse($reply['text']);

			/*** Plain Text Code By Abhishek ****/
			$html = strip_tags($post['text']);
			// Convert HTML entities to single characters
			$html = html_entity_decode($html, ENT_QUOTES, 'UTF-8');
			$html = preg_replace("/&#?[a-z0-9]{2,8};/i","",$html);
			$html = filter_var($html, FILTER_SANITIZE_STRING);
			$post['text_plain'] = $html;
			/*** End Plain Text Code By Abhishek ****/

            /* get the reply author */
            if ($reply['user_type'] == "user") {
                /* user type */
                $reply['author_id'] = $reply['user_id'];
                $reply['author_picture'] = $this->get_picture($reply['user_picture'], $reply['user_gender']);
                $reply['author_url'] = $system['system_url'] . '/' . $reply['user_name'];
                $reply['author_user_name'] = $reply['user_name'];
                $reply['author_name'] = $reply['user_firstname'] . " " . $reply['user_lastname'];
                $reply['author_verified'] = $reply['user_verified'];
            } else {
                /* page type */
                $reply['author_id'] = $reply['page_admin'];
                $reply['author_picture'] = $this->get_picture($reply['page_picture'], "page");
                $reply['author_url'] = $system['system_url'] . '/pages/' . $reply['page_name'];
                $reply['author_name'] = $reply['page_title'];
                $reply['author_verified'] = $reply['page_verified'];
            }
            /* check if viewer user likes this reply */
            if ( $reply['likes'] > 0) {
                $check_like = $db->query(sprintf("SELECT * FROM posts_comments_likes WHERE user_id = %s AND comment_id = %s", secure($me_id, 'int'), secure($reply['comment_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $reply['i_like'] = ($check_like->num_rows > 0) ? true : false;
            }
            /* check if viewer can manage comment [Edit|Delete] */
            $reply['edit_comment'] = false;
            $reply['delete_comment'] = false;
            if ($this->_logged_in) {
                /* viewer is (admins|moderators)] */
                if ($this->_data['user_group'] < 3) {
                    $reply['edit_comment'] = true;
                    $reply['delete_comment'] = true;
                }
                /* viewer is the author of comment */
                if ($me_id == $reply['author_id']) {
                    $reply['edit_comment'] = true;
                    $reply['delete_comment'] = true;
                }
            }
            $replies[] = $reply;
        }
        return $replies;
    }

    public function delete_user_api($user_id)
    {
        global $db;
		if(isset($user_id) && $user_id != 356){
			/* (check&get) user */
			//return $response = array('status'=>'ERROR', 'message'=>'User Does not exist.', 'response'=>null);
			$get_user = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
			if ($get_user->num_rows == 0) {
				return $response = array('status'=>'ERROR', 'message'=>__('User Does not exist.'), 'response'=>null);
			}
			$user = $this->get_user_api($user_id);
			$this->_data = $user['response'];
			$user = $get_user->fetch_assoc();
			// delete user
			$can_delete = false;
			/* target is (admin|moderator) */
			if ($user['user_group'] < 3) {
				return $response = array('status'=>'ERROR', 'message'=>__('You can not delete admin/morderator accounts.'), 'response'=>null);
				//throw new Exception(__("You can not delete admin/morderator accounts"));
			}
			/* viewer is (admin|moderator) */
			if ($this->_data['user_group'] < 3) {
				$can_delete = true;
			}
			/* viewer is the target */
			if ($this->_data['user_id'] == $user_id) {
				$can_delete = true;
			}
			//var_dump($can_delete); exit;
			/* delete the user */
			if ($can_delete) {
				/* delete the user */
				if($db->query(sprintf("DELETE FROM users WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN)){
					return $response = array('status'=>'SUCCESS', 'message'=>__('You has been removed successfully.'), 'response'=>null);
				}
				else{
					return $response = array('status'=>'ERROR', 'message'=>__('Something went wrong.'), 'response'=>null);
				}
			}
			else{
				return $response = array('status'=>'ERROR', 'message'=>__('Something went wrong.'), 'response'=>null);
			}
		}
		else{
			return $response = array('status'=>'ERROR', 'message'=>__('You can not delete admin/morderator accounts.'), 'response'=>null);
		}
    }

    public function like_comment_api($comment_id, $user_id)
    {
        global $db;

        $user = $this->get_user_api($user_id);
        $this->_data = $user['response'];
        /* (check|get) comment */
        $comment = $this->get_comment_api($comment_id, true);
        if (!$comment) {
            return $response = array('status'=>'ERROR', 'message'=>__('comment does not exist.'), 'response' => null);
        }
        /* check blocking */
        if ($this->blocked($comment['author_id'])) {
            return $response = array('status'=>'ERROR', 'message'=>__('comment author is blocked by system.'), 'response' => null);
        }

        /* like the comment */
        if (!$comment['i_like']) {
            $db->query(sprintf("INSERT INTO posts_comments_likes (user_id, comment_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'), secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            //     /* update comment likes counter */
            $db->query(sprintf("UPDATE posts_comments SET likes = likes + 1 WHERE comment_id = %s", secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            //     /* post notification */
            switch ($comment['node_type']) {
                case 'post':
                    $this->post_notification(['to_user_id' => $comment['author_id'], 'action' => 'like', 'node_type' => 'post_comment', 'node_url' => $comment['node_id'], 'notify_id' => 'comment_' . $comment_id]);
                    break;
                case 'photo':
                    $this->post_notification(['to_user_id' => $comment['author_id'], 'action' => 'like', 'node_type' => 'photo_comment', 'node_url' => $comment['node_id'], 'notify_id' => 'comment_' . $comment_id]);
                    break;
                case 'comment':
                    $_node_type = ($comment['parent_comment']['node_type'] == "post") ? "post_reply" : "photo_reply";
                    $_node_url = $comment['parent_comment']['node_id'];
                    $this->post_notification(['to_user_id' => $comment['author_id'], 'action' => 'like', 'node_type' => $_node_type, 'node_url' => $_node_url, 'notify_id' => 'comment_' . $comment_id]);
                    break;
            }
            return $response = array('status'=>'SUCCESS', 'message'=>__('comment liked successfully.'), 'response' => null);
        }
        else{
            return $response = array('status'=>'ERROR', 'message'=>__('You already liked this comment.'), 'response' => null);
        }
    }

    public function unlike_comment_api($comment_id, $user_id)
    {
        global $db;

        $user = $this->get_user_api($user_id);
        $this->_data = $user['response'];
        /* (check|get) comment */
        $comment = $this->get_comment_api($comment_id);
        if (!$comment) {
            return $response = array('status'=>'ERROR', 'message'=>__('Comment does not exist.'), 'response' => null);
        }
        /* check blocking */
        if ($this->blocked($comment['author_id'])) {
            return $response = array('status'=>'ERROR', 'message'=>__('Comment author is blocked by system.'), 'response' => null);
        }
        // /* unlike the comment */
        if ($comment['i_like']) {
            $db->query(sprintf("DELETE FROM posts_comments_likes WHERE user_id = %s AND comment_id = %s", secure($this->_data['user_id'], 'int'), secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            //     /* update comment likes counter */
            $db->query(sprintf("UPDATE posts_comments SET likes = IF(likes=0,0,likes-1) WHERE comment_id = %s", secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            //     /* delete notification */
            switch ($comment['node_type']) {
                case 'post':
                    $this->delete_notification($comment['author_id'], 'like', 'post_comment', $comment['node_id']);
                    break;
                case 'photo':
                    $this->delete_notification($comment['author_id'], 'like', 'photo_comment', $comment['node_id']);
                    break;
                case 'comment':
                    $_node_type = ($comment['parent_comment']['node_type'] == "post") ? "post_reply" : "photo_reply";
                    $_node_url = $comment['parent_comment']['node_id'];
                    $this->delete_notification($comment['author_id'], 'like', $_node_type, $_node_url);
                    break;
            }
            return $response = array('status'=>'SUCCESS', 'message'=>__('Comment unlike successfully.'), 'response' => null);
        }
        else{
            return $response = array('status'=>'ERROR', 'message'=>__('You did not like this comment.'), 'response' => null);
        }
    }

    function update_device_token($user_id, $device_token, $device_type){
        global $db;
        $check_target = $db->query(sprintf("SELECT * FROM push_notification WHERE user_id = $user_id AND device_token = '$device_token'")) or _error(SQL_ERROR_THROWEN);
        /* if yes -> return */
        if ($check_target->num_rows > 0) {
            return $response = array('status'=>'SUCCESS', 'message'=>__('Device token successfully updated.'), 'response' => null);
        }
        else{
            $db->query(sprintf("INSERT INTO push_notification (`user_id`, `device_token`, `device_type`) values(%s,%s,%s)", secure($user_id, 'int'), secure($device_token), secure($device_type))) or _error(SQL_ERROR_THROWEN);
            return $response = array('status'=>'SUCCESS', 'message'=>__('Device token successfully updated.'), 'response' => null);
        }
    }

    function delete_device_token($user_id, $device_token, $device_type){
        global $db;
        $check_target = $db->query(sprintf("SELECT * FROM push_notification WHERE user_id = %s AND device_token = %s", secure($user_id, 'int'), secure($device_token))) or _error(SQL_ERROR_THROWEN);
        /* if yes -> return */
        if ($check_target->num_rows > 0) {
            $db->query(sprintf("DELETE FROM push_notification WHERE user_id = %s AND device_token = %s", secure($user_id, 'int'), secure($device_token))) or _error(SQL_ERROR_THROWEN);
            return $response = array('status'=>'SUCCESS', 'message'=>__('Logged out successfully.'), 'response' => null);
        }
        else{
            return $response = array('status'=>'SUCCESS', 'message'=>__('Logged out successfully.'), 'response' => null);
        }
    }

/**
*
$registrationIds = array( 'eH5Q6qh0rUg:APA91bGlC4F9ljDtEHl34woUgsUu6GMBTAGMW_B7UI8cETZ6i2Rstecu4lW-QWcHUSRAoP4nAokcUYoGl63xSi7g-EIVaMxmGS4iJ1yWcDqYGQMs4gzMKj92Yy08lJJPOCL8JElx63qJOvMmx9VvgvnSFMDj6KGr-w', 'c0Z9nW4GlEo:APA91bEd5Ayj70cSNXtvmg-wFw_vzzgTV6iYuROC_4sGfO11csF768OPT0dJy_rK2cOmdA1pf9MVyiBgklIfJkHEowvDdFSiPi--vP6JYTSTDiwmEVkMfFuOHZJZ5_-Q-HocqYOypZ2e21JHnVtzW6EfCP68pISJdg' );

$msg = array
(
    'message'   => 'here is a message. message',
    'title'     => 'Testing here chetan',
    'subtitle'  => 'This is a subtitle. subtitle',
    'tickerText'    => 'Ticker text here...Ticker text here...Ticker text here',
    'vibrate'   => 1,
    'sound'     => 1,
    'largeIcon' => 'https://www.google.co.in/logos/doodles/2018/world-cup-2018-day-22-5384495837478912-law.gif',
    'smallIcon' => 'https://www.google.co.in/logos/doodles/2018/world-cup-2018-day-22-5384495837478912-law.gif'
);

**/
    function send_push_notifications($registrationIds, $msg){
        // API access key from Google API's Console
        define( 'API_ACCESS_KEY', 'AAAAmhAs1RA:APA91bHlZCZVRXWpN70rsO73UHbJU3yjBxW1zL4cn9gUTHA19c2zA6OB-b2utH8wlRDXYKgVTTMOp_QP9ZyxYtcBiIG939zYAqjExWZ2ELzw7FiKooUngbvu18brmCivLMVMpShJoSL6ryObYsG2DZXLD0yjvm19fw' );
        $fields = array('registration_ids'  => $registrationIds, 'data' => $msg);
        $headers = array('Authorization: key=' . API_ACCESS_KEY, 'Content-Type: application/json');
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        return json_decode($result);
    }

    function send_push_notifications_ios($devicetoken, $data) {
		$apnsHost = 'gateway.push.apple.com';
		$apnsCert = 'ios_push_prod_certificate.pem';
		$apnsPort = 2195;
		$passphrase = 'guo@tiu';
		$streamContext = stream_context_create();
		stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
		stream_context_set_option($streamContext, 'ssl', 'passphrase', $passphrase);
		$apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
		if($apns){
			$payload['aps'] = $data; //array('alert' => 'Oh hai!', 'badge' => 1, 'sound' => 'default');
			$output = json_encode($payload);
			$token=$devicetoken;
			//$token = 'abe012eb8933d2189bd2a617c11e8895f4f9912b61a6a011d95872730aadb4fb';
			$token = pack('H*', str_replace(' ', '', $token));
			$apnsMessage = chr(0) . chr(0) . chr(32) . $token . chr(0) . chr(strlen($output)) . $output;
			//$result = fwrite($apns, $apnsMessage, strlen($apnsMessage));
			$result = fwrite($apns, $apnsMessage);
			//echo 'test'; exit;
			//echo '<hr>';
			if (!$result){
				$response = array('status'=>'ERROR', 'message'=>__('Message not delivered.'), 'response' => null);
			}
			else{
				$response = array('status'=>'SUCCESS', 'message'=>__('Message successfully delivered.'), 'response' => null);
			}
			fclose($apns);
		}
		else{
			$response = array('status'=>'ERROR', 'message'=>__('failed to connect.'), 'response' => null);
		}
		return $response;
    }

	function getTokenByUserId($user_id){
		global $db;
		$tokens = array();
		$get_tokens = $db->query(sprintf("SELECT * FROM `push_notification` WHERE `user_id` = $user_id", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
		if ($get_tokens->num_rows != 0) {
			while ($token = $get_tokens->fetch_assoc()) {
                $tokens[] = $token;
            }
            return $tokens;
        }
		else{
			return false;
		}
	}

	function _check_post_api($id, $pass_privacy_check = true, $full_details = true)
    {
        global $db, $system;
        /* get post */
		//echo sprintf("SELECT posts.*, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_picture_id, users.user_cover_id, users.user_verified, users.user_subscribed, users.user_pinned_post FROM posts LEFT JOIN users ON posts.user_id = users.user_id AND posts.user_type = 'user' WHERE NOT (users.user_name <=> NULL) AND posts.post_id = %s", secure($id, 'int')); exit;
        $get_post = $db->query(sprintf("SELECT posts.*, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_picture_id, users.user_cover_id, users.user_verified, users.user_subscribed, users.user_pinned_post FROM posts LEFT JOIN users ON posts.user_id = users.user_id AND posts.user_type = 'user' WHERE NOT (users.user_name <=> NULL) AND posts.post_id = %s", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
        //var_dump($get_post); exit;
		if ($get_post->num_rows == 0) {
            return false;
        }

        $post = $get_post->fetch_assoc();
        /* check if the page has been deleted */
        if ($post['user_type'] == "page" && !$post['page_admin']) {
            return false;
        }
        /* get the author */
        $post['author_id'] = ($post['user_type'] == "page") ? $post['page_admin'] : $post['user_id'];
        $post['is_page_admin'] = ($this->_logged_in && $this->_data['user_id'] == $post['page_admin']) ? true : false;
        $post['is_group_admin'] = ($this->_logged_in && $this->_data['user_id'] == $post['group_admin']) ? true : false;
        $post['is_event_admin'] = ($this->_logged_in && $this->_data['user_id'] == $post['event_admin']) ? true : false;
        /* check the post author type */
        if ($post['user_type'] == "user") {
            /* user */
            $post['post_author_picture'] = $this->get_picture($post['user_picture'], $post['user_gender']);
            $post['post_author_url'] = $system['system_url'] . '/' . $post['user_name'];
            $post['post_author_name'] = $post['user_firstname'] . " " . $post['user_lastname'];
            $post['post_author_verified'] = $post['user_verified'];
            $post['pinned'] = ((!$post['in_group'] && !$post['in_event'] && $post['post_id'] == $post['user_pinned_post']) || ($post['in_group'] && $post['post_id'] == $post['group_pinned_post']) || ($post['in_event'] && $post['post_id'] == $post['event_pinned_post'])) ? true : false;
            $post['post_user_name'] = $post['user_name'];
        } else {
            /* page */
            $post['post_author_picture'] = $this->get_picture($post['page_picture'], "page");
            $post['post_author_url'] = $system['system_url'] . '/pages/' . $post['page_name'];
            $post['post_author_name'] = $post['page_title'];
            $post['post_author_verified'] = $post['page_verified'];
            $post['pinned'] = ($post['post_id'] == $post['page_pinned_post']) ? true : false;
        }
        /* check if viewer can manage post [Edit|Pin|Delete] */
        $post['manage_post'] = false;
        if ($this->_logged_in) {
            /* viewer is (admins|moderators)] */
            if ($this->_data['user_group'] < 3) {
                $post['manage_post'] = true;
            }
            /* viewer is the author of post || page admin */
            if ($this->_data['user_id'] == $post['author_id']) {
                $post['manage_post'] = true;
            }
            /* viewer is the admin of the group of the post */
            if ($post['in_group'] && $post['is_group_admin']) {
                $post['manage_post'] = true;
            }
            /* viewer is the admin of the event of the post */
            if ($post['in_event'] && $post['is_event_admin']) {
                $post['manage_post'] = true;
            }
        }
        /* full details */
        if ($full_details) {
            /* check if wall post */
            if ($post['in_wall']) {
                $get_wall_user = $db->query(sprintf("SELECT user_firstname, user_lastname, user_name FROM users WHERE user_id = %s", secure($post['wall_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_wall_user->num_rows == 0) {
                    return false;
                }
                $wall_user = $get_wall_user->fetch_assoc();
                $post['wall_username'] = $wall_user['user_name'];
                $post['wall_fullname'] = $wall_user['user_firstname'] . " " . $wall_user['user_lastname'];
            }
            /* check if viewer [liked|saved] this post */
            $post['i_save'] = false;
            $post['i_like'] = false;
            if ($this->_logged_in) {
                /* save */
                $check_save = $db->query(sprintf("SELECT * FROM posts_saved WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($check_save->num_rows > 0) {
                    $post['i_save'] = true;
                }
                /* like */
                $check_like = $db->query(sprintf("SELECT * FROM posts_likes WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post['post_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($check_like->num_rows > 0) {
                    $post['i_like'] = true;
                }
            }
        }
        /* check privacy */
        /* if post in group & (the group is public || the viewer approved member of this group) => pass privacy check */
        if ($post['in_group'] && ($post['group_privacy'] == 'public' || $this->check_group_membership($this->_data['user_id'], $post['group_id']) == 'approved')) {
            $pass_privacy_check = true;
        }
        /* if post in event & (the event is public || the viewer member of this event) => pass privacy check */
        if ($post['in_event'] && ($post['event_privacy'] == 'public' || $this->check_event_membership($this->_data['user_id'], $post['event_id']))) {
            $pass_privacy_check = true;
        }

		/* Get New Total Number Of Comments (No Repeated). Code Edited By Abhishek */
            $get_comments = $db->query(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'post' AND posts_comments.node_id = %s AND posts_comments.text != '' group by posts_comments.text ORDER BY posts_comments.comment_id DESC) comments group by comments.text ORDER BY comments.comment_id DESC", secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);

			$post['comments'] = $get_comments->num_rows;

			/* End Get New Total Number Of Comments (No Repeated). Code Edited By Abhishek */

        if ($pass_privacy_check || $this->_check_privacy($post['privacy'], $post['author_id'])) {
            return $post;
        }
        return false;
    }

    // Function to get the client ip address
    function get_client_ip_env() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    // Function to get the client ip address
    function get_client_ip_server() {
        $ipaddress = '';
        if ($_SERVER['HTTP_CLIENT_IP'])
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if($_SERVER['HTTP_X_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if($_SERVER['HTTP_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if($_SERVER['HTTP_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if($_SERVER['REMOTE_ADDR'])
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

	public function setMobileLanguageByUserId($user_id){

		/* For mobile app start */
        global $db;
        $ipaddress = $this->get_client_ip_env();

		$exp_ipaddress = explode(', ',$ipaddress);

		if($exp_ipaddress[0] != ''){
			$new_ipaddress = $exp_ipaddress[0];
		}
		elseif($exp_ipaddress[1] != ''){
			$new_ipaddress = $exp_ipaddress[1];
		}
		elseif($exp_ipaddress[2] != ''){
			$new_ipaddress = $exp_ipaddress[2];
		}
		else{
			$new_ipaddress = $exp_ipaddress;
		}

		if($user_id != ''){

			$get_user_language = $db->query(sprintf('SELECT set_mobile_language FROM users WHERE user_id= %s', secure($user_id))) or _error(SQL_ERROR_THROWEN);

			if($get_user_language->num_rows > 0) {

				$ut_lang = $get_user_language->fetch_assoc();

				if($ut_lang['set_mobile_language'] == 'NULL' || $ut_lang['set_mobile_language'] == ''){

					$get_user_lang = $db->query(sprintf('SELECT language_code FROM set_mobile_language WHERE ip_address= %s', secure($new_ipaddress))) or _error(SQL_ERROR_THROWEN);

					$u_lang = $get_user_lang->fetch_assoc();

					$db->query(sprintf("UPDATE users SET set_mobile_language = '".$u_lang['language_code']."' WHERE user_id=%s", secure($user_id))) or _error(SQL_ERROR_THROWEN);

					/* for mobile app start */
					if(!empty($u_lang['language_code'])){
					  $_COOKIE['s_lang'] = $u_lang['language_code'];
					  $system['language'] = $u_lang['language_code'];
					  T_setlocale(LC_MESSAGES, $u_lang['language_code']);
					}
					/* for mobile app end */
				}
				else{

					/* for mobile app start */
					if(!empty($ut_lang['set_mobile_language'])){
					  $_COOKIE['s_lang'] = $ut_lang['set_mobile_language'];
					  $system['language'] = $ut_lang['set_mobile_language'];
					  T_setlocale(LC_MESSAGES, $ut_lang['set_mobile_language']);
					}
					/* for mobile app end */
				}
			}
		}
	}
    function getMobileLng($user_id='',$me_id=''){
        /* For mobile app start */

        global $db;
        $ipaddress = $this->get_client_ip_env();

		$exp_ipaddress = explode(', ',$ipaddress);

		if($exp_ipaddress[0] != ''){
			$new_ipaddress = $exp_ipaddress[0];
		}
		elseif($exp_ipaddress[1] != ''){
			$new_ipaddress = $exp_ipaddress[1];
		}
		elseif($exp_ipaddress[2] != ''){
			$new_ipaddress = $exp_ipaddress[2];
		}
		else{
			$new_ipaddress = $exp_ipaddress;
		}

		/*if($me_id != ''){
			$user_id = $me_id;
		}*/

		if($me_id != ''){
			$user_id = $me_id;
		}

		/*$response = array('status'=>'ERROR', 'message'=>__('Post does not exist').'  U- '.$me_id, 'response' => null);
		echo json_encode($response);
		exit;*/
		if($user_id != ''){

			$get_user_language = $db->query(sprintf('SELECT set_mobile_language FROM users WHERE user_id= %s', secure($user_id))) or _error(SQL_ERROR_THROWEN);

			if($get_user_language->num_rows > 0) {

				$ut_lang = $get_user_language->fetch_assoc();

				if($ut_lang['set_mobile_language'] == 'NULL' || $ut_lang['set_mobile_language'] == ''){

					$get_user_lang = $db->query(sprintf('SELECT language_code FROM set_mobile_language WHERE ip_address= %s', secure($new_ipaddress))) or _error(SQL_ERROR_THROWEN);

					$u_lang = $get_user_lang->fetch_assoc();

					$db->query(sprintf("UPDATE users SET set_mobile_language = '".$u_lang['language_code']."' WHERE user_id=%s", secure($user_id))) or _error(SQL_ERROR_THROWEN);

					/* for mobile app start */
					if(!empty($u_lang['language_code'])){
					  $_COOKIE['s_lang'] = $u_lang['language_code'];
					  $system['language'] = $u_lang['language_code'];
					  T_setlocale(LC_MESSAGES, $u_lang['language_code']);
					}
					/* for mobile app end */
				}
				else{
					if($user_id == 79527){
						//echo $ut_lang['set_mobile_language'];exit;
					}
					/* for mobile app start */
					if(!empty($ut_lang['set_mobile_language'])){
					  $_COOKIE['s_lang'] = $ut_lang['set_mobile_language'];
					  $system['language'] = $ut_lang['set_mobile_language'];
					  T_setlocale(LC_MESSAGES, $ut_lang['set_mobile_language']);
					}
					/* for mobile app end */
				}
			}
		}
		else{
			$get_user_lang = $db->query(sprintf('SELECT language_code FROM set_mobile_language WHERE ip_address= %s', secure($new_ipaddress))) or _error(SQL_ERROR_THROWEN);

			$u_lang = $get_user_lang->fetch_assoc();

			/* for mobile app start */
			if(!empty($u_lang['language_code'])){
			  $_COOKIE['s_lang'] = $u_lang['language_code'];
			  $system['language'] = $u_lang['language_code'];
			  T_setlocale(LC_MESSAGES, $u_lang['language_code']);
			}
			/* for mobile app end */
		}
    }
	function set_notification_read($from_notifi_id,$to_notifi_id,$user_id){

		global $db;

		$db->query(sprintf("UPDATE notifications SET seen = '1' WHERE notification_id between (%s) and (%s) and (to_user_id = %s)", secure($from_notifi_id,'int'),secure($to_notifi_id,'int'), secure($user_id,'int'))) or _error(SQL_ERROR_THROWEN);

		$response = array('status'=>'SUCCESS', 'message'=>'Marked as read successfully', 'response' => null);

		return $response;
	}
	function notification_unread_count($user_id){

		global $db;

		//Get Max Notification Id From User TABLE
		$get_max_notification = $db->query(sprintf('SELECT max_notification_id FROM users WHERE user_id= %s LIMIT 1', secure($user_id,'int'))) or _error(SQL_ERROR_THROWEN);

		if($get_max_notification->num_rows > 0) {

			$max_notifi_id = $get_max_notification->fetch_assoc();
			if($max_notifi_id['max_notification_id'] != NULL){
				$get_notification = $db->query(sprintf("Select * FROM notifications WHERE to_user_id = %s and notification_id > %s AND (from_user_id <> 0 AND action <> 'follow')", secure($user_id,'int'),secure($max_notifi_id['max_notification_id'],'int'))) or _error(SQL_ERROR_THROWEN);
			}
			else{
				$get_notification = $db->query(sprintf("Select * FROM notifications WHERE to_user_id = %s AND (from_user_id <> 0 AND action <> 'follow')", secure($user_id,'int'))) or _error(SQL_ERROR_THROWEN);
			}

			$count = $get_notification->num_rows;

			$response = array('status'=>'SUCCESS', 'message'=>null, 'response' => array('unread_count'=>$count));

			return $response;
		}
		else{
			$response = array('status'=>'ERROR', 'message'=>'User does not exist', 'response' => null);

			return $response;
		}

	}
	function set_max_notification_id($user_id,$notifi_id){

		global $db;

		if($user_id == ''){

			$response = array('status'=>'ERROR', 'message'=>'To user id should not be blank', 'response' => null);

			return $response;
		}
		if($notifi_id == ''){
			$response = array('status'=>'ERROR', 'message'=>'Notification id should not be blank', 'response' => null);

			return $response;
		}

		$db->query(sprintf("UPDATE users SET max_notification_id = %s WHERE user_id = %s", secure($notifi_id), secure($user_id))) or _error(SQL_ERROR_THROWEN);

		$response = array('status'=>'SUCCESS', 'message'=>'Max notification successfully set', 'response' => null);

		return $response;
	}
	public function _support_submit_msg_api($_content){

		global $db, $system;

		$user_id = $_content['user_id'];
		$support_subject = $_content['support_subject'];
		$support_message = $_content['support_message'];

		if($_content['support_subject'] == ''){

			$response = array('status'=>'ERROR', 'message'=>'Support subject should not be blank', 'response' => null);

			return $response;
		}

		if($_content['support_message'] == ''){

			$response = array('status'=>'ERROR', 'message'=>'Support message should not be blank', 'response' => null);

			return $response;
		}

		$sql = 'INSERT INTO support_msg_froms(`user_id`,`subject`,`message`,`created`,`modified`) VALUES('.$user_id.',"'.$support_subject.'","'.$support_message.'",Now(),Now())';

		$get_user = $db->query(sprintf('SELECT user_email,user_firstname,user_lastname FROM users WHERE user_id= %s', secure($user_id))) or _error(SQL_ERROR_THROWEN);

		if($get_user->num_rows > 0) {
			$user_details = $get_user->fetch_assoc();
			if($user_details['user_email'] != '')
			{
				$_content['user_email'] = $user_details['user_email'];
			}
			if($user_details['user_firstname'] != '')
			{
				$_content['first_name'] = $user_details['user_firstname'];
			}
			if($user_details['user_lastname'] != '')
			{
				$_content['last_name'] = $user_details['user_lastname'];
			}
		}

		if($db->query($sql))
		{
			if(!Email::send_support_msg($_content['user_email'], $_content['first_name'], $_content['last_name'], $_content['support_subject'], $_content['support_message'])){

				$response = array('status'=>'ERROR', 'message'=>__("There are some error while sending email"), 'response' => null);

				return $response;
			}
			else{


				$response = array('status'=>'SUCCESS', 'message'=>__("Support email successfully sent"), 'response' => null);

				return $response;
			}
		}
		else{

			$response = array('status'=>'ERROR', 'message'=>__("There are some error internal error while sending email"), 'response' => null);

			return $response;
		}
	}
	public function htmlToPlainText($str){
		$str = str_replace('&nbsp;', ' ', $str);
		$str = html_entity_decode($str, ENT_QUOTES | ENT_COMPAT , 'UTF-8');
		$str = html_entity_decode($str, ENT_HTML5, 'UTF-8');
		$str = html_entity_decode($str);
		$str = htmlspecialchars_decode($str);
		$str = strip_tags($str);

		return $str;
	}

	public function DictionaryLookup ($host, $path, $key, $params, $content) {

		$headers = "Content-type: application/json\r\n" .
			"Content-length: " . strlen($content) . "\r\n" .
			"Ocp-Apim-Subscription-Key: $key\r\n" .
			"X-ClientTraceId: " . $this->com_create_guid() . "\r\n";

		// NOTE: Use the key 'http' even if you are making an HTTPS request. See:
		// http://php.net/manual/en/function.stream-context-create.php
		$options = array (
			'http' => array (
				'header' => $headers,
				'method' => 'POST',
				'content' => $content
			)
		);
		$context  = stream_context_create ($options);
		$result = file_get_contents ($host . $path . $params, false, $context);
		return $result;
	}

	public function ms_detect_language($post_text)
	{
		// NOTE: Be sure to uncomment the following line in your php.ini file.
		// ;extension=php_openssl.dll

		// **********************************************
		// *** Update or verify the following values. ***
		// **********************************************

		// Replace the subscriptionKey string value with your valid subscription key.
		$key = '6b8f1fc0891144878e14bc09d48de6d7';

		$host = "https://api.cognitive.microsofttranslator.com";
		$path = "/detect?api-version=3.0";

		$text = $post_text;

		$requestBody = array (
		array (
				'Text' => $text,
			),
		);
		$content = json_encode($requestBody);

		$result = $this->DictionaryLookup ($host, $path, $key, "", $content);

		// Note: We convert result, which is JSON, to and from an object so we can pretty-print it.
		// We want to avoid escaping any Unicode characters that result contains. See:
		// http://php.net/manual/en/function.json-encode.php
		//$json = json_encode(json_decode($result), JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

		$detect = json_decode($result);

		return $detect[0]->language;
	}

	public function Translate($host, $path, $key, $params, $content) {

		$headers = "Content-type: application/json\r\n" .
			"Content-length: " . strlen($content) . "\r\n" .
			"Ocp-Apim-Subscription-Key: $key\r\n" .
			"X-ClientTraceId: " . $this->com_create_guid() . "\r\n";

		// NOTE: Use the key 'http' even if you are making an HTTPS request. See:
		// http://php.net/manual/en/function.stream-context-create.php
		$options = array (
			'http' => array (
				'header' => $headers,
				'method' => 'POST',
				'content' => $content
			)
		);
		$context  = stream_context_create ($options);
		$result = file_get_contents ($host . $path . $params, false, $context);
		return $result;
	}

	public function ms_translate($post_text,$detect_lang)
	{
		// NOTE: Be sure to uncomment the following line in your php.ini file.
		// ;extension=php_openssl.dll

		// **********************************************
		// *** Update or verify the following values. ***
		// **********************************************

		// Replace the subscriptionKey string value with your valid subscription key.
		$key = '6b8f1fc0891144878e14bc09d48de6d7';

		$host = "https://api.cognitive.microsofttranslator.com";
		$path = "/translate?api-version=3.0";

		// Translate to German and Italian.
		$params_lang = "&to=".$detect_lang;

		$text = $post_text;

		$requestBody = array (
			array (
				'Text' => $text,
			),
		);
		$content = json_encode($requestBody);

		$result = $this->Translate ($host, $path, $key, $params_lang, $content);

		// Note: We convert result, which is JSON, to and from an object so we can pretty-print it.
		// We want to avoid escaping any Unicode characters that result contains. See:
		// http://php.net/manual/en/function.json-encode.php

		//$json = json_encode(json_decode($result), JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

		$translate = json_decode($result);

		return $translate[0]->translations[0]->text;
	}

	public function com_create_guid() {
		return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
			mt_rand( 0, 0xffff ),
			mt_rand( 0, 0x0fff ) | 0x4000,
			mt_rand( 0, 0x3fff ) | 0x8000,
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		);
	}

	public function time_elapsed_string($datetime, $full = false) {
		/*$now = new DateTime;
		$ago = new DateTime($datetime);
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}

		if (!$full) $string = array_slice($string, 0, 1);
		return $string ? implode(', ', $string) . ' ago' : 'just now';*/

		//$datetime = $this->dateToTimezone($datetime);

		//$userTimezone = new DateTimeZone('Asia/Kolkata');

		// Commented on 24-Aug-2018
        /*$userTimezone = new DateTimeZone('America/New_York');
		$gmtTimezone = new DateTimeZone('GMT');
		$myDateTime = new DateTime($datetime, $gmtTimezone);
		$offset = $userTimezone->getOffset($myDateTime);
		$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
		$myDateTime->add($myInterval);
		$result = $myDateTime->format('Y-m-d H:i:s');

		$timestamp = strtotime($result);

		$r_date = date("F d, Y g:i A", $timestamp);

		return $r_date;*/

		$olddate = $datetime;       //date as string
		$now = time();                  //pick present time from server
		$old = strtotime($olddate);  //create integer value of old time
		$diff =  $now-$old;             //calculate difference

		$userTimezone = new DateTimeZone('America/New_York');
		$gmtTimezone = new DateTimeZone('GMT');

		$old = new DateTime($olddate,$gmtTimezone);
		$old = $old->format('Y M d');       //format date to "2015 Aug 2015" format

		if ($diff /60 <1)                       //check the difference and do echo as required
		{
			if(intval($diff%60) == 0){
				return "1 ".__('second ago');
			}
			else{
				return intval($diff%60)." ".__('seconds ago');
			}
		}
		else if (intval($diff/60) == 1)
		{
			return " 1 ".__('minute ago');
		}
		else if ($diff / 60 < 60)
		{
			return intval($diff/60)." ".__('minutes ago');
		}
		else if (intval($diff / 3600) == 1)
		{
			return "1 ".__('hour ago');
		}
		else if ($diff / 3600 <24)
		{
			return intval($diff/3600) ." ".__('hours ago');
		}
		else if ($diff/86400 < 30)
		{
			//return intval($diff/86400) . " days ago";

			$o_dt = new DateTime($olddate);
			//$r_date = $o_dt->format('d M');

			//$r_date = $o_dt->format('m/d/Y g:i A');
			$r_date = $o_dt->format('n/j/Y');

			return $r_date;
		}
		else
		{
			//return $old;  ////format date to "2015 Aug 2015" format

			//return intval($diff/86400) . " days ago";

			$o_dt = new DateTime($olddate);
			$r_date = $o_dt->format('n/j/Y');

			return $r_date;
		}
	}

    public function like_photo_api($photo_id)
    {
        global $db;
        /* (check|get) photo */
        $photo = $this->get_photo($photo_id);
        if (!$photo) {
            $response = array('status'=>'ERROR', 'message'=>__("Photo does not exist."), 'response' => null);
            return $response;
            //_error(403);
        }
        $post = $photo['post'];
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            $response = array('status'=>'ERROR', 'message'=>__("You can not like this photo because it's Author is blocked."), 'response' => null);
            return $response;
            //_error(403);
        }
//echo sprintf("SELECT * FROM posts_photos_likes WHERE `user_id` = %s AND `photo_id` = %s", secure($this->_data['user_id'], 'int'), secure($photo_id, 'int'));
        $query = $db->query(sprintf("SELECT * FROM posts_photos_likes WHERE `user_id` = %s AND `photo_id` = %s", secure($this->_data['user_id'], 'int'), secure($photo_id, 'int'))) or _error(SQL_ERROR_THROWEN);
//exit;
        if($query->num_rows == 0){
            /* like the photo */
            $db->query(sprintf("INSERT INTO posts_photos_likes (user_id, photo_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'), secure($photo_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* update photo likes counter */
            $db->query(sprintf("UPDATE posts_photos SET likes = likes + 1 WHERE photo_id = %s", secure($photo_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* post notification */
            $this->post_notification(['to_user_id' => $post['author_id'], 'action' => 'like', 'node_type' => 'photo', 'node_url' => $photo_id]);
            $response = array('status'=>'SUCCESS', 'message'=>__("You liked photo successfully."), 'response' => null);
            return $response;
        }
        else{
            $response = array('status'=>'ERROR', 'message'=>__("You already liked this photo."), 'response' => null);
            return $response;
        }
    }

    public function unlike_photo_api($photo_id)
    {
        global $db;
        /* (check|get) photo */
        $photo = $this->get_photo($photo_id);
        if (!$photo) {
            $response = array('status'=>'ERROR', 'message'=>__("Photo does not exist."), 'response' => null);
            return $response;
            //_error(403);
        }
        $post = $photo['post'];
        $query = $db->query(sprintf("SELECT * FROM posts_photos_likes WHERE `user_id` = %s AND `photo_id` = %s", secure($this->_data['user_id'], 'int'), secure($photo_id, 'int'))) or _error(SQL_ERROR_THROWEN);

        if($query->num_rows > 0){
            /* unlike the photo */
            $db->query(sprintf("DELETE FROM posts_photos_likes WHERE user_id = %s AND photo_id = %s", secure($this->_data['user_id'], 'int'), secure($photo_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* update photo likes counter */
            $db->query(sprintf("UPDATE posts_photos SET likes = IF(likes=0,0,likes-1) WHERE photo_id = %s", secure($photo_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* delete notification */
            $this->delete_notification($post['author_id'], 'like', 'photo', $photo_id);
            $response = array('status'=>'SUCCESS', 'message'=>__("You unliked photo successfully."), 'response' => null);
            return $response;
        }
        else{
            $response = array('status'=>'ERROR', 'message'=>__("You didn't like this photo."), 'response' => null);
            return $response;
        }
    }

    function set_session_user($user_id){
        $user = $this->get_user_api($user_id);
        $this->_data = $user['response'];
    }

    public function share_photo_api($user_id, $post_id, $comment)
    {
        global $db, $date;

            $user = $this->get_user_api($user_id);
            $this->_data = $user['response'];
        /* check if the viewer can share the post */
        //$post = $this->_check_post($post_id, true);

        $photo = $this->get_photo($post_id);
        if (!$photo) {
            $response = array('status'=>'ERROR', 'message'=>__("Photo does not exist."), 'response' => null);
            return $response;
        }
        $post = $photo['post'];

        //var_dump($post); exit;
        /*if (!$post || $post['privacy'] != 'public') {
            return $response = array('status'=>'ERROR', 'message'=>'Please Check Post ID.', 'response' => null);
        }*/
        /* check blocking */
        if ($this->blocked($post['author_id'])) {
            return $response = array('status'=>'ERROR', 'message'=>__('User is Blocked.'), 'response' => null);
        }
        // share post
        /* share the origin post */

            //$post_id = $post['post_id'];
            $author_id = $post['author_id'];

        /* insert the new shared post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, origin_id, time, privacy, text) VALUES (%s, 'user', 'sharedPhoto', %s, %s, 'public', %s)", secure($user_id, 'int'), secure($post_id, 'int'), secure($date), secure($comment))) or _error(SQL_ERROR_THROWEN);
        $post_id_new = $db->insert_id;
        /* update the origin post shares counter */
        $db->query(sprintf("UPDATE posts SET shares = shares + 1 WHERE post_id = %s", secure($post_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* post notification */
        $this->post_notification(['to_user_id' => $author_id, 'action' => 'share', 'node_type' => 'post', 'node_url' => $post_id]);
        return $response = array('status'=>'SUCCESS', 'message'=>__('Photo Shared Successfully.'), 'response' => $this->get_post_api($post_id_new));
    }
	public function delete_comment_api($comment_id)
    {
        global $db;
        /* (check|get) comment */
        $comment = $this->get_comment($comment_id);

        if (!$comment) {
            return $response = array('status'=>'ERROR', 'message'=>__('comment does not exist.'), 'response' => null);
        }

        /* check if viewer can manage comment [Delete] */
        $comment['delete_comment'] = false;
        // if ($this->_logged_in) {
            /* viewer is (admins|moderators)] */
            if ($this->_data['user_group'] < 3) {
                $comment['delete_comment'] = true;
            }
            /* viewer is the author of comment */
            if ($this->_data['user_id'] == $comment['author_id']) {
                $comment['delete_comment'] = true;
            }
            /* viewer is the author of post || page admin */
            if ($this->_data['user_id'] == $comment['post']['author_id']) {
                $comment['delete_comment'] = true;
            }
            /* viewer is the admin of the group of the post */
            if ($comment['post']['in_group'] && $comment['post']['is_group_admin']) {
                $comment['delete_comment'] = true;
            }
            /* viewer is the admin of the event of the post */
            if ($comment['post']['in_event'] && $comment['post']['is_event_admin']) {
                $comment['delete_comment'] = true;
            }
        // }
        /* delete the comment */
        if ($comment['delete_comment']) {
            /* delete the comment */
            $db->query(sprintf("DELETE FROM posts_comments WHERE comment_id = %s", secure($comment_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* update comments counter */
            switch ($comment['node_type']) {
                case 'post':
                    $db->query(sprintf("UPDATE posts SET comments = IF(comments=0,0,comments-1) WHERE post_id = %s", secure($comment['node_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

                    break;
                case 'photo':
                    $db->query(sprintf("UPDATE posts_photos SET comments = IF(comments=0,0,comments-1) WHERE photo_id = %s", secure($comment['node_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

                    break;
                case 'comment':
                    $db->query(sprintf("UPDATE posts_comments SET replies = IF(replies=0,0,replies-1) WHERE comment_id = %s", secure($comment['node_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

                    break;
            }
        }
        return $response = array('status'=>'SUCCESS', 'message'=>__('Comment has been removed successfully.'), 'response' => null);
    }
	public function twitter_type_datetime($dt) {

		$olddate = $dt;       //date as string
		$now = time();                  //pick present time from server
		$old = strtotime($olddate);  //create integer value of old time
		$diff =  $now-$old;             //calculate difference

		$userTimezone = new DateTimeZone('America/New_York');
		$gmtTimezone = new DateTimeZone('GMT');

		$old = new DateTime($olddate,$gmtTimezone);
		$old = $old->format('Y M d');       //format date to "2015 Aug 2015" format

		if ($diff /60 <1)                       //check the difference and do echo as required
		{
			if(intval($diff%60) == 0){
				return "1 ".__('second ago');
			}
			else{
				return intval($diff%60).__('seconds ago');
			}
		}
		else if (intval($diff/60) == 1)
		{
			return " 1 ".__('minute ago');
		}
		else if ($diff / 60 < 60)
		{
			return intval($diff/60).__('minutes ago');
		}
		else if (intval($diff / 3600) == 1)
		{
			return "1 ".__('hour ago');
		}
		else if ($diff / 3600 <24)
		{
			return intval($diff/3600) .__('hours ago');
		}
		else if ($diff/86400 < 30)
		{
			//return intval($diff/86400) . " days ago";

			$o_dt = new DateTime($olddate);
			//$r_date = $o_dt->format('d M');

			//$r_date = $o_dt->format('m/d/Y g:i A');
			$r_date = $o_dt->format('m/d/Y');

			return $r_date;
		}
		else
		{
			//return $old;  ////format date to "2015 Aug 2015" format

			//return intval($diff/86400) . " days ago";

			$o_dt = new DateTime($olddate);
			$r_date = $o_dt->format('m/d/Y');

			return $r_date;
		}
	}

    // Shortens a number and attaches K, M, B, etc. accordingly
    function number_shorten($number, $precision = 1, $divisors = null) {
        if($number > 999){
            // Setup default $divisors if not provided
            if (!isset($divisors)) {
                $divisors = array(
                    pow(1000, 0) => '', // 1000^0 == 1
                    pow(1000, 1) => 'K', // Thousand
                    pow(1000, 2) => 'M', // Million
                    pow(1000, 3) => 'B', // Billion
                    pow(1000, 4) => 'T', // Trillion
                    pow(1000, 5) => 'Qa', // Quadrillion
                    pow(1000, 6) => 'Qi', // Quintillion
                );
            }

            // Loop through each $divisor and find the
            // lowest amount that matches
            foreach ($divisors as $divisor => $shorthand) {
                if (abs($number) < ($divisor * 1000)) {
                    // We found a match!
                    break;
                }
            }

            // We found our match, or there were no matches.
            // Either way, use the last defined value for $divisor.
            return number_format($number / $divisor, $precision) . $shorthand;
        }
        else{
            return $number;
        }
    }
	public function new_formatted_time($dt) {

        /*$olddate = $dt;       //date as string
		$now = time();                  //pick present time from server
		$old = strtotime($olddate);  //create integer value of old time
		$diff =  $now-$old;             //calculate difference

		$userTimezone = new DateTimeZone('America/New_York');
		$gmtTimezone = new DateTimeZone('GMT');

		$old = new DateTime($olddate,$gmtTimezone);
		$old = $old->format('Y M d');

		$o_dt = new DateTime($olddate);
		$r_date = $o_dt->format("F d, Y g:i A");

		return $r_date;*/

		$olddate = $dt;       //date as string
		$now = time();                  //pick present time from server
		$old = strtotime($olddate);  //create integer value of old time
		$diff =  $now-$old;             //calculate difference

		$userTimezone = new DateTimeZone('America/New_York');
		$gmtTimezone = new DateTimeZone('GMT');

		$old = new DateTime($olddate,$gmtTimezone);
		$old = $old->format('Y M d');       //format date to "2015 Aug 2015" format

		if ($diff /60 <1)                       //check the difference and do echo as required
		{
			if(intval($diff%60) == 0){
				return "1 second ago";
			}
			else{
				return intval($diff%60)." seconds ago";
			}
		}
		else if (intval($diff/60) == 1)
		{
			return " 1 minute ago";
		}
		else if ($diff / 60 < 60)
		{
			return intval($diff/60)." minutes ago";
		}
		else if (intval($diff / 3600) == 1)
		{
			return "1 hour ago";
		}
		else if ($diff / 3600 <24)
		{
			return intval($diff/3600) . " hours ago";
		}
		else if ($diff/86400 < 30)
		{
			//return intval($diff/86400) . " days ago";

			$o_dt = new DateTime($olddate);
			//$r_date = $o_dt->format('d M');

			$r_date = $o_dt->format('m/d/Y g:i A');

			return $r_date;
		}
		else
		{
			return $old;  ////format date to "2015 Aug 2015" format
		}
	}
	function notification_activation($user_id,$status){

		global $db;

		if($user_id == ''){

			$response = array('status'=>'ERROR', 'message'=>'User id should not be blank', 'response' => null);

			return $response;
		}
		if($status == ''){
			$response = array('status'=>'ERROR', 'message'=>'Notification status should not be blank', 'response' => null);

			return $response;
		}

		$db->query(sprintf("UPDATE users SET notification_activated = %s WHERE user_id = %s", secure($status), secure($user_id))) or _error(SQL_ERROR_THROWEN);

		$response = array('status'=>'SUCCESS', 'message'=>'Notification setting successfully updated', 'response' => null);

		return $response;
	}
	public function get_total_likes($post_id,$type){
		
		global $db;
				
		$get_total_likes = $db->query(sprintf("SELECT count('action') as total_likes from notifications where node_url = %s and node_type='".$type."' and action = 'like'", secure($post_id))) or _error(SQL_ERROR_THROWEN);
		
		$result = $get_total_likes->fetch_assoc();
		
		return $result['total_likes'];
	}	
	public function get_two_username_api($user_id,$post_id,$type){
		
		global $db;
				
		$get_username = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_firstname, users.user_lastname FROM notifications INNER JOIN users ON notifications.from_user_id = users.user_id WHERE notifications.to_user_id = %s and notifications.node_url =%s and notifications.node_type='".$type."' and notifications.action = 'like' ORDER BY notifications.notification_id DESC  LIMIT 1, 1", secure($user_id),secure($post_id))) or _error(SQL_ERROR_THROWEN);
		
		$concat = '';
		while($get_result = $get_username->fetch_assoc()) {
			
			$concat = ',';
			$getUsername .= $concat.$get_result['user_name'];
			
		}
		
		return $getUsername;
	}
}
