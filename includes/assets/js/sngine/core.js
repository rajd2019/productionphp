/**

 * core js

 *

 * @package Sngine

 * @author Zamblek

 */



// initialize API URLs

var api = [];

api['core/translator'] = ajax_path + "core/translator.php";


api['data/load'] = ajax_path + "data/load.php";

api['data/search'] = ajax_path + "data/search.php";


api['payments/paypal'] = ajax_path + "payments/paypal.php";

api['payments/stripe'] = ajax_path + "payments/stripe.php";
api['data/translate'] = ajax_path + "core/ms_translate.php";
api['posts/detail']      = ajax_path+"posts/detail.php";


// is_empty

function is_empty(value) {

    if (value.match(/\S/) == null) {

        return true;

    } else {

        return false;

    }

}


// initialize the plugins

function initialize() {

    // run bootstrap tooltip

    $('body').tooltip({

        selector: '[data-toggle="tooltip"], [data-tooltip=tooltip]'

    });

    // run autosize (expand textarea) plugin

    autosize($('.js_autosize'));

    // run moment plugin

    $(".js_moment").each(function () {

        /*var _this = $(this);

        var time_utc = _this.data('time');

        var locale = $('html').attr('lang') || 'en_us';

        if (locale == 'fr-fr') {
            locale = 'zh_CN';
        }
        if (locale == 'zh_cn') {
            locale = 'zh_TW';
        }

        var offset = moment().utcOffset();

        var time = moment(time_utc).add({minutes: offset}).locale(locale);

        //_this.text(time.fromNow()).attr('title', time.format("dddd, MMMM D, YYYY h:m a"));

		var time2 = moment(time + 6048e5).twitterLong();


		_this.text(time2);*/

    });

    // run slimScroll plugin

    $('.js_scroller').each(function () {

        var _this = $(this);

        /* return if the plugin already running  */

        if (_this.parent('.slimScrollDiv').length > 0) {

            return;

        }

        /* run if not */

        _this.slimScroll({

            height: _this.attr('data-slimScroll-height') || '280px',

            start: _this.attr('data-slimScroll-start') || 'top',

            distance: '2px'

        })

    });

	$('.js_scroller_popup').each(function () {

        var _this = $(this);

        /* return if the plugin already running  */

        if (_this.parent('.slimScrollDiv').length > 0) {

            return;

        }

        /* run if not */

        _this.slimScroll({

            height: _this.attr('data-slimScroll-height') || 'calc(100vh - 92px)',

            start: _this.attr('data-slimScroll-start') || 'top',

            distance: '2px'

        })

    });
    // run readmore

    $('.js_readmore').each(function () {

        /*var _this = $(this);

        var height = _this.attr('data-height') || 120;

        //return if the plugin already running

        if (_this.attr('data-readmore') !== undefined) {

            return;

        }

        //run if not

        _this.readmore({

            collapsedHeight: height,

            moreLink: '<a href="#">' + __["Read more"] + '</a>',

            lessLink: '<a href="#">' + __["Read less"] + '</a>'

        });

		$(this).css({'border-bottom':'0px solid transparent'})*/
    });

    // run mediaelementplayer plugin

    $('video.js_mediaelementplayer, audio.js_mediaelementplayer').mediaelementplayer();

}


// modal

function modal() {

    if (arguments[0] == "#modal-login") {

        /* disable the backdrop (don't close modal when click outside) */

        if ($('#modal').data('bs.modal')) {

            $('#modal').data('bs.modal').options = {backdrop: 'static', keyboard: false};

        } else {

            $('#modal').modal({backdrop: 'static', keyboard: false});

        }

    }

    /* check if the modal not visible, show it */

    if (!$('#modal').is(":visible")) $('#modal').modal('show');

    /* update the modal-content with the rendered template */

    $('.modal-content:last').html(render_template(arguments[0], arguments[1]));

    /* initialize modal if the function defined (user logged in) */

    if (typeof initialize_modal === "function") {

        initialize_modal();

    }

}


// confirm

function confirm(title, message, callback) {

    modal('#modal-confirm', {'title': title, 'message': message});

    $("#modal-confirm-ok").click(function () {

        if (callback) callback();

    });

}


// render template

function render_template(selector, options) {

    var template = $(selector).html();
if(template != null){
    Mustache.parse(template);

    var rendered_template = Mustache.render(template, options);

    return rendered_template;
}else{
    return '';
}


}


// load more
function custom_load_more(element, loadid) {

    if (element.hasClass('done') || element.hasClass('loading')) return;
    var _this = element;
    var loading = _this.find('.loader');
    var text = _this.find('span');
    var remove = _this.data('remove') || false;
    var stream = _this.parent().find('ul#' + loadid);
    /* prepare data object */
    var data = {};
    data['get'] = _this.data('get');
    if (_this.data('filter') !== undefined) {
        data['filter'] = _this.data('filter');
    }
    if (_this.data('query') !== undefined) {
        data['query'] = _this.data('query');
    }

    if (_this.data('type') !== undefined) {
        data['type'] = _this.data('type');
    }
    if (_this.data('uid') !== undefined) {
        data['uid'] = _this.data('uid');
    }
    if (_this.data('id') !== undefined) {
        data['id'] = _this.data('id');
    }
    data['offset'] = _this.data('offset') || 1;
    /* we start from iteration 1 because 0 already loaded */
    /* show loader & hide text */
    _this.addClass('loading');
    text.hide();
    loading.removeClass('x-hidden');
    /* get & load data */
    $.post(api['data/load'], data, function (response) {
        _this.removeClass('loading');
        text.show();
        loading.addClass('x-hidden');
        /* check the response */
        if (response.callback) {
            eval(response.callback);
        } else {

            if (response.data) {
                data['offset']++;
                if (response.append) {
                    stream.append(response.data);
                } else {
                    $('.js_comments').prepend(response.data);
                }
                setTimeout(photo_grid(), 200);
            } else {
                /*if(remove) {
                    _this.remove();
                } else {
                    _this.addClass('done');
                    text.text(__['There is no more data to show']);
                }*/
                _this.addClass('done');
                text.text(__['There is no more data to show']);
            }
        }
        _this.data('offset', data['offset']);
    }, 'json')
        .fail(function () {
            _this.removeClass('loading');
            text.show();
            loading.addClass('x-hidden');
            modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
        });
}

function load_more(element) {

    if (element.hasClass('done') || element.hasClass('loading')) return;

    var _this = element;

    var loading = _this.find('.loader');

    var text = _this.find('span');

    var remove = _this.data('remove') || false;

    var stream = _this.parent().find('ul:first');

    /* prepare data object */

    var data = {};

    data['get'] = _this.data('get');

    if (_this.data('filter') !== undefined) {

        data['filter'] = _this.data('filter');

    }

    if (_this.data('type') !== undefined) {

        data['type'] = _this.data('type');

    }

    if (_this.data('uid') !== undefined) {

        data['uid'] = _this.data('uid');

    }

    if (_this.data('id') !== undefined) {

        data['id'] = _this.data('id');

    }

    data['offset'] = _this.data('offset') || 1;
    /* we start from iteration 1 because 0 already loaded */

    /* show loader & hide text */

    _this.addClass('loading');

    text.hide();

    loading.removeClass('x-hidden');

    /* get & load data */

    $.post(api['data/load'], data, function (response) {

        _this.removeClass('loading');

        text.show();

        loading.addClass('x-hidden');

        /* check the response */

        if (response.callback) {

            eval(response.callback);

        } else {

            if (response.data) {


                data['offset']++;

                if (response.append) {


                    stream.append(response.data);

                } else {

                    $('.js_comments').prepend(response.data);

                }

                setTimeout(photo_grid(), 200);

            } else {

                if (remove) {

                    _this.remove();

                } else {

                    _this.addClass('done');

                    text.text(__['There is no more data to show']);

                }

            }

        }

        _this.data('offset', data['offset']);
        $("img.lazy").lazyload();

		__sharethis__.initialize();

    }, 'json')

        .fail(function () {

            _this.removeClass('loading');

            text.show();

            loading.addClass('x-hidden');

            modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});

        });

}


function load_more_comments(element) {

    if (element.hasClass('done') || element.hasClass('loading')) return;

    var _this = element;

    var loading = _this.find('.loader');

    var text = _this.find('span');

    var remove = _this.data('remove') || false;

    var stream = _this.parent().find('ul:first');

    /* prepare data object */

    var data = {};

    data['get'] = _this.data('get');

    if (_this.data('filter') !== undefined) {

        data['filter'] = _this.data('filter');

    }

    if (_this.data('type') !== undefined) {

        data['type'] = _this.data('type');

    }

    if (_this.data('uid') !== undefined) {

        data['uid'] = _this.data('uid');

    }

    if (_this.data('id') !== undefined) {

        data['id'] = _this.data('id');

    }

    data['offset'] = _this.data('offset') || 1;
    /* we start from iteration 1 because 0 already loaded */

    /* show loader & hide text */

    _this.addClass('loading');

    text.hide();

    loading.removeClass('x-hidden');

    /* get & load data */

    $.post(api['data/load'], data, function (response) {

        _this.removeClass('loading');

        text.show();

        loading.addClass('x-hidden');

        /* check the response */

        if (response.callback) {

            eval(response.callback);

        } else {

            if (response.data) {


                data['offset']++;

                if (response.append) {


                    stream.append(response.data);

                } else {

                    $('.js_comments').append(response.data);

                }

                setTimeout(photo_grid(), 200);

            } else {

                if (remove) {

                    _this.remove();

                } else {

                    _this.addClass('done');

                    text.text(__['There is no more data to show']);

                }

            }

        }

        _this.data('offset', data['offset']);

    }, 'json')

        .fail(function () {

            _this.removeClass('loading');

            text.show();

            loading.addClass('x-hidden');

            modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});

        });

}


// photo grid

function photo_grid() {

    /* main photo */

    $('.pg_2o3_in').each(function () {

        if ($(this).parents('.pg_3x').length > 0) {

            var width = height = $(this).parents('.pg_3x').width() * 0.667;

        }

        if ($(this).parents('.pg_4x').length > 0) {

            var width = height = $(this).parents('.pg_4x').width() * 0.749;

        }

        $(this).width(width);

        $(this).height(height);

    });

    /* side photos */

    $('.pg_1o3_in').each(function () {

        if ($(this).parents('.pg_3x').length > 0) {

            var width = $(this).parents('.pg_3x').width() * 0.332;

            var height = ($(this).parent('.pg_1o3').prev().height() - 1) / 2;

        }

        if ($(this).parents('.pg_4x').length > 0) {

            var width = $(this).parents('.pg_4x').width() * 0.25;

            var height = ($(this).parent('.pg_1o3').prev().height() - 2) / 3;

        }

        $(this).width(width);

        $(this).height(height);

    });

}


$(function () {



    // init plugins

    initialize();

    $(document).ajaxComplete(function () {

        initialize();

    });


    // init hash

    var _t = $('body').attr('data-hash-tok') ? $('body').attr('data-hash-tok') : '';

    var _p = $('body').attr('data-hash-pos');

    switch (_p) {

        case '1':

            var _l = 'Z';

            break;

        case '2':

            var _l = 'm';

            break;

        case '3':

            var _l = 'B';

            break;

        case '4':

            var _l = 'l';

            break;

        case '5':

            var _l = 'K';

            break;

    }

    if (_p != 6 && _t[_t[0]] != _l) {

        document.write("Your session hash has been broken, Please contact Sngine's support!");

    }


    // init fastlink plugin

    FastClick.attach(document.body);


    // init offcanvas-sidebar

    $('[data-toggle=offcanvas]').click(function () {

        $('.offcanvas').toggleClass('active');

    });


    // run sticky-sidebar

    if ($(window).width() > 750) { // Desktops (=768px)

        $(window).scroll(function () {

            var orginal_width = $('.js_sticky-sidebar').width();

            if ($(document).scrollTop() > 200) {

                $('.js_sticky-sidebar').addClass('fixed');

                $('.js_sticky-sidebar').width(orginal_width);

            } else {

                $('.js_sticky-sidebar').removeClass('fixed');

                $('.js_sticky-sidebar').removeAttr('style');

            }

        })

    }


    // run photo grid

    photo_grid();

    $(window).on("resize", function () {

        setTimeout(photo_grid(), 200);

    });


    // run bootstrap modal

    $('body').on('click', '[data-toggle="modal"]', function (e) {

        e.preventDefault();

        var url = $(this).data('url');

        var options = $(this).data('options');

        if (url.indexOf('#') == 0) {

            /* open already loaded modal with #id */

            modal(url, options);

        } else {

            /* get & load modal from url */

            $.getJSON(ajax_path + url, function (response) {

                /* check the response */

                if (!response) return;

                /* check if there is a callback */

                if (response.callback) {

                    eval(response.callback);

                }

            })

                .fail(function () {

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});

                });

        }

    });


    // bootsrap dropdown keep open (and for slimScrollBar)

    $('body').on('click', '.js_dropdown-keepopen, .slimScrollBar', function (e) {

        e.stopPropagation();

    });


    // run bootstrap btn-group

    $('body').on('click', '.btn-group a', function (e) {

        e.preventDefault();

        var parent = $(this).parents('.btn-group');

        /* change the value */

        parent.find('input[type="hidden"]').val($(this).data('value'));

        /* copy text to btn-group-text */

        parent.find('.btn-group-text').text($(this).text());

        /* copy icon to btn-group-icon */

        parent.find('.btn-group-icon').attr("class", $(this).find('i.fa').attr("class")).addClass('btn-group-icon');

        /* copy title to tooltip */

        parent.data('original-title', $(this).data('title'));

        parent.data('value', $(this).data('value'));

        parent.tooltip();

    });


    // run toggle-panel

    $('.js_toggle-panel').click(function (event) {

        event.preventDefault;

        var parent = $(this).parents('.panel-body');

        parent.hide();

        parent.siblings().fadeIn();

        return false;

    });


    // run ajax-forms

    $('body').on('submit', '.js_ajax-forms', function (e) {


        e.preventDefault();

        var url = $(this).data('url');

        var submit = $(this).find('button[type="submit"]');

        var error = $(this).find('.alert.alert-danger');

        var success = $(this).find('.alert.alert-success');

        /* show any collapsed section if any */

        if ($(this).find('.js_hidden-section').length > 0 && !$(this).find('.js_hidden-section').is(':visible')) {

            $(this).find('.js_hidden-section').slideDown();

            return false;

        }

        /* show loading */

        submit.data('text', submit.html());

        submit.prop('disabled', true);

        submit.html(__['Loading']);

        /* get ajax response */


        $.post(ajax_path + url, $(this).serialize(), function (response) {

            /* hide loading */

            submit.prop('disabled', false);

            submit.html(submit.data('text'));

            /* handle response */

            if (response.error) {

                if (success.is(":visible")) success.hide(); // hide previous alert

                error.html(response.message).slideDown();


            } else if (response.success) {
                /* setTimeout(reload_page(), 3000);*/
                if (error.is(":visible")) error.hide(); // hide previous alert
                success.html(response.message).slideDown();


                // window.location.href = "/settings";
                location.reload();

            } else {

                eval(response.callback);

            }

        }, "json")

            .fail(function () {

                /* hide loading */

                submit.prop('disabled', false);

                submit.html(submit.data('text'));

                /* handle error */

                if (success.is(":visible")) success.hide(); // hide previous alert

                error.html(__['There is something that went wrong!']).slideDown();

            });

    });


    function reload_page() {
        location.reload();
    }

    // run translator

    $('body').on('click', '.js_translator', function () {

        var language = $(this).data('language');

        /* set language */

        $.post(api['core/translator'], {'language': language}, function (response) {

            /* check the response */

            if (!response) return;

            /* check if there is a callback */

            if (response.callback) {

                eval(response.callback);

            }

        }, 'json')

            .fail(function () {

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});

            });

    });


    // run load-more

    /* load more data by click */

    $('body').on('click', '.js_see-more', function () {

        load_more($(this));

    });
    /*$('body').on('click', '.js_see-more-comments', function () {

        load_more_comments($(this));

    });*/
    /* load more data by scroll */

    $('.see-more').show();

    // direct_message from post
    // $('.newMessagesModaldm .DMActivity-back').click(function () {
    //
    //   $('#desktop-live-msg-id').click();
    //
    // });
    $('body').on('click', '.direct_message_frm_post', function(evt) {

      if( $(window).width() > 767){

        //$('#desktop-live-msg-id').click();
        //$('#myModal .DMComposeButton').click();

        //$('.newMessagesModaldm').modal('show');

          $('#newMessagesModal').modal('show');

      //  $('.newMessagesModaldm .DMActivity-back').hide();
        //getUserList(true);
        //getRecentUsers(5);

      }else{

        //$('#myModal').modal('show');
        //$('#newMessagesModal').click();
        //$('#myModal .DMComposeButton').click();

      //  $('.newMessagesModaldm').modal('show');

        $('#newMessagesModal').modal('show');
        //getUserList(true);
        //$('#newMessagesModal').modal('show');

      }



      var post_id = $(this).attr('data-id');
      if(post_id > 0){
        $('#showSlowLoaderPost').show();
      }
      $.ajax({
        type: "GET",
        dataType: "json",
        url: site_path+'/api.php?query=2y10goLM7YVrc.T2G6AvkMdfS.XUF9Wej5O5XPLwL/H2S59z9yvM6W.AG&get=get_post&post_id='+post_id, //Relative or absolute path to response.php file
        success: function(data) {
              console.log(data.response);
              $('#showSlowLoaderPost').hide();
              $('.DMComposer-attachment').show();
              $('#org_post_detail').attr('org-post-id', data.response.post_id);
              $('#post_author_name_id').text(data.response.post_author_name);
              $('#post_author_url_id').attr('href', data.response.post_author_url);
              $('#post_author_url_id').attr('data-user-id', data.response.user_id);

              $("#post_avatar_id").attr('src', data.response.post_author_picture);
              $('#post_author_username_id').html('@<b>'+data.response.post_user_name+'</b>');
              if(data.response.post_type=='shared'){
                var postText = data.response.origin.text.substr(0, 25)
              }else{
                var postText = data.response.text.substr(0, 25)
              }

              $('#post_content_id').html(postText);
              $('.DMComposer-send button').attr('aria-disabled', false);
              $('.DMComposer-send button').removeClass('disabled');

              //$('.modal-tweet').html(data.response.text);
        }
      });

    });

    $('#chatModal').on('show.bs.modal', function () {
      // $('.DMComposer-attachment').hide();
      // $('#org_post_detail').attr('org-post-id', '');
      // $('#post_author_name_id').text('');
      // $('#post_author_url_id').attr('href','');
      // $('#post_author_url_id').attr('data-user-id', '');
      // $("#post_avatar_id").attr('src', '');
      // $('#post_author_username_id').html('');
      // $('#post_content_id').html('');
    });

    $('#DMComposer-discardAttachment').click(function() {
        $('.DMComposer-attachment').hide();
        $('#org_post_detail').attr('org-post-id', '');
        $('#post_author_name_id').text('');
        $('#post_author_url_id').attr('href','');
        $('#post_author_url_id').attr('data-user-id', '');
        $("#post_avatar_id").attr('src', '');
        $('#post_author_username_id').html('');
        $('#post_content_id').html('');
        $('.DMComposer-send button').attr('aria-disabled', true);
        $('.DMComposer-send button').addClass('disabled');
    });

   $('body').on('click', '#org_post_detail', function(evt) {

     $('#chatModal').modal('hide');
     $('html').css({'position':''});

      $('#modal2 .modal-body').html('<div class="loader pt10 pb10"></div>');
 		  $('#modal2').modal('show');
         setTimeout(function(){
          $("img.lazy").lazyload();
         },800);

         var id = $('#org_post_detail').attr('org-post-id');

         $.getJSON(api['posts/detail'], {'post_id': id}, function (response) {
                     /* check the response */
                     if (response.callback) {
                         eval(response.callback);
                         $('#currnt_post_id').val(id);
                     } else {
                         $(".js_conversation-container").html(response.conversation);
                         $('.msg-back-button').click(function() {
                             $('.main-header ul.navbar-nav li.visible-xs-block a').click();
                         });
                     }
         })

   });








    $('.js_see-more-infinite').bind('inview', function (event, visible) {


        if (visible == true) {

            load_more($(this));

        }

    });

    $('body').on('click', '.post_js_see-more', function () {
        custom_load_more($(this), "post-tab");
    });
    /* load more data by scroll */
    $('.see-more').show();

    $('.post_js_see-more-infinite').bind('inview', function (event, visible) {

        /*if (visible == true) {
            custom_load_more($(this), "post-tab");
        }*/
    });
    $('body').on('click', '.custom_js_see-more', function () {
        custom_load_more($(this), "toptab");
    });
    /* load more data by scroll */
    $('.see-more').show();

    $('.custom_js_see-more-infinite').bind('inview', function (event, visible) {

        /*if (visible == true) {
            custom_load_more($(this), "toptab");
        }*/
    });


    // run search

    /* show and get the search results */

    $('body').on('keyup', '#search-input', function () {

        var query = $(this).val();

        if (!is_empty(query)) {

			$('#search-input-value').val(query);

			$('.js_dropdown-keepopen').css('display','block');

            $('#search-history').hide();

            $('#search-results').show();

            var hashtags = query.match(/#(\w+)/ig);

            if (hashtags !== null && hashtags.length > 0) {

                var query = hashtags[0].replace("#", "");

                $('#search-results .dropdown-widget-header').hide();

                $('#search-results-all').hide();

                $('#search-results .dropdown-widget-body').html(render_template('#search-for', {
                    'query': query,
                    'hashtag': true
                }));

            } else {

                $.post(api['data/search'], {'query': query}, function (response) {

                    if (response.callback) {

                        eval(response.callback);

                    } else if (response.results) {

                        $('#search-results .dropdown-widget-header').show();

                        $('#search-results-all').show();

                        $('#search-results .dropdown-widget-body').html(response.results);

                        $('#search-results-all').attr('href', site_path + '/search/' + query);

                    } else {

                        $('#search-results .dropdown-widget-header').hide();

                        $('#search-results-all').hide();

                        $('#search-results .dropdown-widget-body').html(render_template('#search-for', {'query': query}));

                    }

                }, 'json');

            }

        }

    });
	$('body').on('click', '#search-results-all', function (eventt) {
        var search_key = $('#search-input-value').val();
        window.location = site_path+'/search/'+search_key+'&view=';
    });
    $('body').on('click', '.Icon--search', function (eventt) {
        //$('#search-input').trigger("keydown", {which: 13});
        var e = $.Event('keydown');
        e.which = 13;
        e.keyCode = 13;
        $("form.navbar-form").find("#search-input").trigger(e);

        //  // eventt.preventDefault;
        //   var query = $("form.navbar-form").find("#search-input").attr("value");
        //   alert(query);
        //   if(!is_empty(query)) {
        //       var hashtags = query.match(/#(\w+)/ig);
        //       if(hashtags !== null && hashtags.length > 0) {
        //           var query = hashtags[0].replace("#", "");
        //           window.location = site_path+'/search/hashtag/'+query
        //       } else {
        //           window.location = site_path+'/search/'+query+'&view='
        //       }
        //   }
        // //  return false;
    });


    /* submit search form */

    $('body').on('keydown', '#search-input', function (event) {
        //alert(event.keyCode);
        if (event.keyCode == 13) {
            event.preventDefault;
            var query = $(this).val();
            if (!is_empty(query)) {
                // alert(query);

                var hashtags = query.match(/#(\w+)/ig);

                if (hashtags !== null && hashtags.length > 0) {

                    var query = hashtags[0].replace("#", "");

                    window.location = site_path + '/search/hashtag/' + query

                } else {

                    window.location = site_path + '/search/' + query + '&view='

                }

            }

            return false;

        }

    });


    /* show previous search (results|history) when the search-input is clicked */

    $('body').on('click', '#search-input', function () {

        if ($(this).val() != '') {

            $('#search-results').show();

        } else {

            $('#search-history').show();

        }

    });

    /* hide the search (results|history) when clicked outside search-input */

    $('body').on('click', function (e) {

        if (!$(e.target).is("#search-input")) {

            $('#search-results, #search-history').hide();

        }

    });

    /* submit search form */

    $('body').on('submit', '.js_search-form', function (e) {

        e.preventDefault;

        var query = this.query.value;

        var handle = $(this).data('handle');

        console.log(handle);

        if (!is_empty(query)) {

            if (handle !== undefined) {

                window.location = site_path + '/' + handle + '/search/' + query

            } else {

                var hashtags = query.match(/#(\w+)/ig);

                if (hashtags !== null && hashtags.length > 0) {

                    var query = hashtags[0].replace("#", "");

                    window.location = site_path + '/search/hashtag/' + query

                } else {

                    window.location = site_path + '/search/' + query

                }

            }

        }

        return false;

    });


    // run YouTube player

    $('body').on('click', '.youtube-player', function () {

        $(this).html('<iframe src="https://www.youtube.com/embed/' + $(this).data('id') + '?autoplay=1" frameborder="0" allowfullscreen="1"></iframe>');

    });


    // run payments

    $('body').on('click', '.js_payment-paypal', function () {

        var _this = $(this);

        var package_id = _this.data('id');

        /* post the request */

        _this.button('loading');

        $.post(api['payments/paypal'], {'id': package_id}, function (response) {

            _this.button('reset');

            /* check the response */

            if (!response) return;

            /* check if there is a callback */

            if (response.callback) {

                eval(response.callback);

            }

        }, "json")

            .fail(function () {

                _this.button('reset');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});

            });

    });

    $('body').on('click', '.js_payment-stripe', function () {

        var _this = $(this);

        var method = _this.data('method');

        var package_id = _this.data('id');

        var package_price = _this.data('price') * 100;

        var package_name = _this.data('name');

        var package_img = _this.data('img');

        _this.button('loading');

        var handler = StripeCheckout.configure({

            key: stripe_key,

            locale: 'english',

            image: package_img,

            token: function (token) {

                $.post(api['payments/stripe'], {
                    'id': package_id,
                    'token': token.id,
                    'email': token.email
                }, function (response) {

                    /* check the response */

                    if (!response) return;

                    /* check if there is a callback */

                    if (response.callback) {

                        eval(response.callback);

                    }

                }, "json")

                    .fail(function () {

                        modal('#modal-message', {
                            title: __['Error'],
                            message: __['There is something that went wrong!']
                        });

                    });

            }

        });

        handler.open({

            name: site_title,

            description: package_name,

            amount: package_price,

            currency: currency,

            bitcoin: (method == "bitcoin") ? true : false,

            alipay: (method == "alipay") ? true : false,

            opened: function () {

                _this.button('reset');

                $('#modal').modal('hide');

            }

        });

        $(window).on('popstate', function () {

            handler.close();

        });

    });


    /**Submit header search form*/

    $('body').on('click', '.nav-search', function (event) {


        event.preventDefault;

        var query = $("#search-input").val();

        if (!is_empty(query)) {

            var hashtags = query.match(/#(\w+)/ig);

            if (hashtags !== null && hashtags.length > 0) {

                var query = hashtags[0].replace("#", "");

                window.location = site_path + '/search/hashtag/' + query

            } else {


                window.location = site_path + '/search/' + query + '&view=fdgf'

            }

        }

        return false;

    });


    $('body').on('click', '.nav-follow-search', function (event) {

        event.preventDefault();

        var query = $("#search-input-follow").val();
        var query_search_type = '';
        if($('#search_for_id').length > 0){
            query_search_type = $("#search_for_id").val();
        }


        if (!is_empty(query)) {

            var hashtags = query.match(/#(\w+)/ig);

            if (hashtags !== null && hashtags.length > 0) {

                var query = hashtags[0].replace("#", "");

                window.location = site_path + '/search/hashtag/' + query

            } else if(!is_empty(query_search_type)) {

                window.location = site_path + '/search_people/?query=' + query;

            } else {

                window.location = site_path + '/search/' + query + '&view=follow'

            }

        }

        else {

            $("#search-input-follow").addClass('error-input')

        }

        return false;

    });


    window.onscroll = function () {
        scrollFunction()
    };


    function scrollFunction() {



        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
// missing button on UI cause errors. disable it for now
//            document.getElementById("goToTop").style.display = "block";

        } else {

//            document.getElementById("goToTop").style.display = "none";

        }

    }



    $('body').on('click', '#goToTop', function () {

        $('html,body').animate({scrollTop: 0}, 'slow');

    });

    $('body').on('click', '.youtube-player', function (e) {

        if ($(this).parents('.post').closest("div.modal-body").attr('class') != 'modal-body post-hover-content') {

            $('#modal2').modal('hide');
            e.preventDefault();
            return false;
        }

    });
    $('body').on('click', '.js_mediaelementplayer', function (e) {
        if ($(this).parents('.post').closest("div.modal-body").attr('class') != 'modal-body post-hover-content') {
            $('#modal2').modal('hide');
            e.preventDefault();
            return false;
        }

    });
    $('body').on('click', '#postBox', function () {

        if ($('#feelings-menu').css('display') == 'block')

            $('#feelings-menu').css('display', 'none');


    });

    $(".nav-search").click(function () {

        var query = $("#search-input").val();
        if (!is_empty(query)) {

            $('#search-history').hide();

            $('#search-results').show();

            var hashtags = query.match(/#(\w+)/ig);

            if (hashtags !== null && hashtags.length > 0) {
                var query = hashtags[0].replace("#", "");

                $('#search-results .dropdown-widget-header').hide();

                $('#search-results-all').hide();

                $('#search-results .dropdown-widget-body').html(render_template('#search-for', {
                    'query': query,
                    'hashtag': true
                }));

            } else {

                $.post(api['data/search'], {'query': query}, function (response) {

                    if (response.callback) {

                        eval(response.callback);

                    } else if (response.results) {

                        $('#search-results .dropdown-widget-header').show();

                        $('#search-results-all').show();

                        $('#search-results .dropdown-widget-body').html(response.results);

                        $('#search-results-all').attr('href', site_path + '/search/' + query);

                    } else {

                        $('#search-results .dropdown-widget-header').hide();

                        $('#search-results-all').hide();

                        $('#search-results .dropdown-widget-body').html(render_template('#search-for', {'query': query}));

                    }

                }, 'json');

            }

        }

    });
	$('body').on('click', '.support-popup', function(evt) {

		 $('#modal3 .modal-body').html('<div class="loader pt10 pb10"></div>');
		 $('#modal3').modal('show');
		 $('.post_custom').html('');

        var replyid=$(this).closest("ul.js_replies").closest("div.comment").attr('data-id');
        $('#modal2').addClass('custom-modal-opacity');
        if(replyid){
            var id = replyid;
        }
        else
        {
             var id = $(this).data('id');
        }
        var handle=$(this).data('handle');
        $.getJSON(site_path+'/includes/ajax/core/support.php', {'post_id': id,'handle':handle}, function (response) {
                    if (response.callback) {
                        eval(response.callback);

                    } else {
                        $(".js_conversation-container").html(response.conversation);
                        $('.msg-back-button').click(function() {
                            $('.main-header ul.navbar-nav li.visible-xs-block a').click();
                        });
                    }
        })
    });
	$('body').on('click', '.submit_support', function(evt) {



		if($('#user-email').val() == ''){

			alert('Please enter email id');
			$('#user-email').focus();
			return false;
		}
		else{

			var x = $('#user-email').val();
			var atpos = x.indexOf("@");
			var dotpos = x.lastIndexOf(".");
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
				alert("Please enter valid email address");
				return false;
			}

		}
		if($('#support-subject').val() == ''){

			alert('Please enter subject');
			$('#support-subject').focus();
			return false;
		}

		if($('#support-message').val() == ''){

			alert('Please enter your message');
			$('#support-message').focus();
			return false;
		}

		var user_id = $('#user_id').val();
		var support_subject = $('#support-subject').val();
		var support_message = $('#support-message').val();

		$('.btn-primary').removeClass('submit_support');
		$('.btn-primary').text('Please wait...');

        $.getJSON(site_path+'/includes/ajax/core/submit_support_message.php', {'support_subject':support_subject,'support_message':support_message,'user_id':user_id}, function (response) {
                    if (response.return_status == 'Success') {

						$('#response-div').html('<div class="post_custom" style="background:none;border:none;"><div class="alert alert-success"><strong>Success!</strong> Message has been successfully sent</div></div>');

						$('.btn-primary').addClass('submit_support');
						$('.btn-primary').text('Submit');

                    } else {

						$('#response-div').html('<div class="post_custom" style="background:none;border:none;"><div class="alert alert-danger"><strong>Error!</strong> There are some Internal Error While Sending Message</div></div>');

						$('.btn-primary').addClass('submit_support');
						$('.btn-primary').text('Submit');
                    }
        })
    });

	//update feature status of login user
    $('#newFeatureModal').on('hide.bs.modal', function (evt) {


        $.getJSON(site_path+'/includes/ajax/users/new_feature_update.php', {'set_status':'0'}, function (response) {

                    /*if (response.return_status == 'Success') {


                    } else {

                    }*/
        })
    });

});
function closeFeaturePopup(){

	$('#newFeatureModal').modal('hide');

	$(function () {
		$.getJSON(site_path+'/includes/ajax/users/new_feature_update.php', {'set_status':'0'}, function (response) {

			/*if (response.return_status == 'Success') {


			} else {

			}*/
		})
	});
}

$(function () {

	/*$( ".post" ).hover(
	  function() {
		$(this).find('.post-text').css({'border-bottom':'7px solid #f5f8fa'})
		//console.log('aaaaaaaaaaaaaaaa');
	  }, function() {
		$(this).find('.post-text').css({'border-bottom':'7px solid #ffffff'})

		//console.log('bbbbbbbbbbbbbbb');
	  }
	);*/

});

$(document).click(function(e){
    if($( "#search_toggle" ).hasClass( "active" )) {

    }else{
        $('.search_field').hide();
    }
});
function TranslateText(post_id){

	$(document).ready(function(){

		var toggle_val = $('#translate-toggle-val-'+post_id).val();

		if(toggle_val == 0){

			if($('#translated-text-'+post_id).html() != ''){

				$('#t-translate-'+post_id).show();
				$('#translate-toggle-val-'+post_id).val(1);
				return false;
			}
			$('#translate-toggle-val-'+post_id).val(1);

			$('#translated-text-'+post_data_id).html('')

			$('#t-translate-'+post_id).show();

			//var post_data_id = $(this).attr('data-id-translate');

			var post_data_id = post_id;

			var post_text = $('#hidden-post-'+post_data_id).html();

			$('#translate-loader-'+post_data_id).show();

			$(this).next(".text-translate").toggle("test");

			$.post(api['data/translate'], {'post_text':post_text}, function (response) {

                    $('#translated-text-'+post_data_id).html(response.return_language);

					$('#translated-by-'+post_data_id).show();
					$('#translate-loader-'+post_data_id).hide();

            }, 'json');
		}
		else{
			$('#translate-toggle-val-'+post_id).val(0);
			$('#t-translate-'+post_id).hide();
		}
	});
}

function get_post_detail_box(post_id){
  $('#chatModal').modal('hide');
  $('html').css({'position':''});

   $('#modal2 .modal-body').html('<div class="loader pt10 pb10"></div>');
   $('#modal2').modal('show');
      setTimeout(function(){
       $("img.lazy").lazyload();
      },800);

      var id = post_id;

      $.getJSON(api['posts/detail'], {'post_id': id}, function (response) {
                  /* check the response */
                  if (response.callback) {
                      eval(response.callback);
                      $('#currnt_post_id').val(id);
                  } else {
                      $(".js_conversation-container").html(response.conversation);
                      $('.msg-back-button').click(function() {
                          $('.main-header ul.navbar-nav li.visible-xs-block a').click();
                      });
                  }
      });
}
$(document).ready(function(){
	$(".take-photo").click(function(){
				 
		$('#modal-web-cam .modal-body').html('<div class="loader pt10 pb10"></div>');
		$('#modal-web-cam').modal('show');
		 
		$("#modal-web-cam .modal-dialog").css({'width':'90vw'});	
		$("#modal-web-cam .modal-dialog").css({'height':'90vh'});		 
		$.getJSON(site_path+'/includes/ajax/users/take_photo.php', {'post_id': ''}, function (response) {
					/*if (response.callback) {
						eval(response.callback);
						
					} else {
						$(".js_conversation-container").html(response.conversation);
						$('.msg-back-button').click(function() {
							$('.main-header ul.navbar-nav li.visible-xs-block a').click();
						});
					}*/
					
					//$('.modal-content-modal3').html(response.content)
					
					eval(response.callback)
		})
	});
});
$(document).ready(function(){
	$(".take-video").click(function(){
				 
		$('#modal-web-cam .modal-body').html('<div class="loader pt10 pb10"></div>');
		$('#modal-web-cam').modal('show');
		 		
		$("#modal-web-cam .modal-dialog").css({'width':'90vw'});	
		$("#modal-web-cam .modal-dialog").css({'height':'90vh'});	
		$.getJSON(site_path+'/includes/ajax/users/take_video.php', {'post_id': ''}, function (response) {
					/*if (response.callback) {
						eval(response.callback);
						
					} else {
						$(".js_conversation-container").html(response.conversation);
						$('.msg-back-button').click(function() {
							$('.main-header ul.navbar-nav li.visible-xs-block a').click();
						});
					}*/
					
					//$('.modal-content-modal3').html(response.content)
					
					eval(response.callback)
		})
	});
});