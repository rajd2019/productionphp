<?php
/**
 * Created by PhpStorm.
 * User: vitalii
 * Date: 06.03.18
 * Time: 11:49
 */

class AwsS3 {

    /**
     * @return Aws\S3\S3Client
     */
    static function connect()
    {
        return Aws\S3\S3Client::factory(array(
            'version'    => 'latest',
            'region'      => S3_REGION,
            'credentials' => array(
                'key'    => S3_KEY,
                'secret' => S3_SECRET,
            )
        ));
    }

    static function get_bucket(){
        return S3_BUCKET;
    }

    static function get_key($file_name){
        return 'uploads/' . $file_name;
    }

    /**
     * upload
     *
     * @param string $path
     * @param string $file_name
     * @return boolean
     */
    static function upload($path, $file_name) {
        $key = self::get_key($file_name);
        $client = self::connect();
        $bucket = self::get_bucket();
        $client->putObject([
            'Bucket' => $bucket,
            'Key'    => $key,
            'Body'   => fopen($path, 'r+'),
            'ACL'    => 'public-read',
            'headers' => array(
                "Cache-Control" => "max-age=94608000",
                "Expires" => gmdate("D, d M Y H:i:s T",
                    strtotime("+3 years"))
            ),
        ]);
        /* remove local file */
        gc_collect_cycles();
        return self::is_present_object($file_name, $client);
    }
	
	static function upload_thumbnail($path, $file_name) {
        $key = self::get_key($file_name);
        $client = self::connect();
        $bucket = self::get_bucket();
        $client->putObject([
			'Bucket'      => $bucket,
			'Key'         => $key,
			'ContentType' => 'image/jpeg',
			'ACL'    => 'public-read',
			'Body'   => ($path),
            'headers' => array(
                "Cache-Control" => "max-age=94608000",
                "Expires" => gmdate("D, d M Y H:i:s T",
                    strtotime("+3 years"))
            ),
		 ]);	
        /* remove local file */
        gc_collect_cycles();
        return self::is_present_object($file_name, $client);
    }

    static function list_buckets(){
        $client = self::connect();
        return $client->listBuckets();
    }

    static function is_present_bucket($bucket){
        $client = self::connect();
        return $client->doesBucketExist($bucket);
    }

    static function is_present_object($file_name, $client = null){
        if(!is_object($client)){
            $client = self::connect();
        }
        $bucket = self::get_bucket();
        $key = self::get_key($file_name);
        return $client->doesObjectExist($bucket, $key);
    }

    static function delete_object($file_name, $client = null){
        if(!is_object($client)){
            $client = self::connect();
        }
        $bucket = self::get_bucket();
        $key = self::get_key($file_name);
        $client->deleteObject(array(
            'Bucket' => $bucket,
            'Key'    => $key
        ));
        return !self::is_present_object($file_name, $client);
    }

    static function test() {
        if(empty(self::list_buckets())) {
            throw new Exception(__("There is no buckets in your account"));
        }
        if(!self::is_present_bucket(self::get_bucket())) {
            throw new Exception(__("There is no bucket with this name in your account"));
        }
    }
}