<?php

function array_flatten(array $array, $prev_key = '')
{
  $return = array();

  foreach ($array as $key => $val) {
    $_key = $prev_key == '' ? $key : $prev_key . "_" . $key;
    if (is_array($val)) {
      $return += array_flatten($val, $_key);
    } else {
      $return[$_key] = $val;
    }
  }

  return $return;
}

function getHttpSchemeAndUrl()
{
  $scheme = array_key_exists('REQUEST_SCHEME',$_SERVER) ? $_SERVER['REQUEST_SCHEME']: 'https';
  if (array_key_exists('HTTP_CLOUDFRONT_FORWARDED_PROTO', $_SERVER)) {
    $scheme = $_SERVER['HTTP_CLOUDFRONT_FORWARDED_PROTO'];
  } else if (array_key_exists('HTTP_X_FORWARDED_PROTO', $_SERVER)) {
    $scheme = $_SERVER['HTTP_X_FORWARDED_PROTO'];
  }

  if(!isset($_SERVER['SERVER_PORT']) || !isset($_SERVER['SERVER_NAME'])){
    return false;
  }

  $port = $_SERVER['SERVER_PORT'];

  if($port!='80' && $port!='443'){
    return $scheme.'://'.$_SERVER['SERVER_NAME'].':'.$port;
  }

  return $scheme.'://'.$_SERVER['SERVER_NAME'];
}

function getDomain($url)
{
  return str_replace('www.', '', parse_url($url, PHP_URL_HOST));
}

function to_define(array $array){
  foreach ($array as $key => $value){
    define(strtoupper($key), $value);
  }
}

global $system; //TODO: use only define variables
//TODO: delete table system_options from DB

$_config_path = __DIR__ . "/config.json";

if(apcu_exists('system_config')){
  $system = apcu_fetch('system_config');
  to_define($system);
}else if (file_exists($_config_path)) {
  $_raw_systems = json_decode(file_get_contents($_config_path), true);

  if (empty($_raw_systems)) {
    die('Error: Config file have broken format');
  }

  $system = array_flatten($_raw_systems);

  if ($system['s3_enabled']) {
    $system['system_uploads'] = $system['s3_endpoint'];
  } else {
    $system['system_uploads'] = $system['system_url'] . '/' . $system['uploads_directory'];
  }
  apcu_add('system_config', $system);
  to_define($system);
} else {
  die("Error: Can't find config file");
}

$_getHttpSchemeAndUrl = getHttpSchemeAndUrl();
define('SYS_URL', $_getHttpSchemeAndUrl ? $_getHttpSchemeAndUrl : 'http://localhost'); // e.g (http://example.com)
define('SYS_DOMAIN', getDomain(SYS_URL));
$system['system_url'] = SYS_URL;

$system['FileMicroTime'] = microtime();
$system['REQUEST_URI'] = $_SERVER['REQUEST_URI'];
