<?php

/**
 * class -> image
 *
 * @package Sngine
 * @author Zamblek
 */
class Image
{
  public $_img;
  public $_img_ext;
  public $_img_type;
  public $_img_width;
  public $_img_height;

  /**
   * __construct
   *
   * @param string $file
   */
  public function __construct($file)
  {
    $this->_img_type = @mime_content_type($file);
    if (!$this->_img_type) {
      throw new Exception(__("The file type is not valid image"));
    }
    if ($this->_img_type == 'image/jpeg' || $this->_img_type == 'image/jpg') {
      $this->_img = imagecreatefromjpeg($file);
      $this->_img_ext = '.jpg';
    } elseif ($this->_img_type == 'image/gif') {
      $this->_img = imagecreatefromgif($file);
      $this->_img_ext = '.gif';
    } elseif ($this->_img_type == 'image/png') {
      $this->_img = imagecreatefrompng($file);
      $this->_img_ext = '.png';
    } else {
      throw new Exception(__($this->_img_type));
    }
  }

  /**
   * save
   *
   * @param string $path_new
   * @param string $path_tmp
   * @param boolean $resize
   * @return void
   */
  public function save($path_new, $path_tmp = '', $resize = true)
  {
    if ($this->_img_type == 'image/jpeg' || $this->_img_type == 'image/jpg') {
      imagejpeg($this->_img, $path_new);
    } elseif ($this->_img_type == 'image/gif') {
      copy($path_tmp, $path_new);
    } elseif ($this->_img_type == 'image/png') {
      imagealphablending($this->_img, false);
      imagesavealpha($this->_img, true);
      imagepng($this->_img, $path_new);
    }
  }
}
