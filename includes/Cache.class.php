<?php

class Cache {

    private $_endpoint;
    private $_port;
    private $_client;

    function __construct() {
        $this->_endpoint = '127.0.0.1';
        $this->_port = '11211';

        try {
          
                $this->_client = new Memcached();
            
            $this->_client->addServer($this->_endpoint, $this->_port);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            exit;
        }
    }

    function set($k, $v,$t) {
        $this->_client->set($k, $v,$t);
    }

    function get($k) {
        return $this->_client->get($k);
    }

}
