<?php
$request_url = strtok($_SERVER["REQUEST_URI"],'?');

$rewrite_rules = [
  "#^/report(.*)/?$#" => "controllers/report.php", //rewrite ^/report(.*)/?$ /report.php last; 
  "#^/profile.php(.*)/?$#" => "controllers/profile.php",
  "#^/people/?$#" => "controllers/people.php", //rewrite ^/people/?$ /people.php last;
  "#^/static/(?P<url>[^/]+)/?$#" => "controllers/static.php", //rewrite ^/static/([^/]+)/?$ /static.php?url=$1 last;
  "#^/contacts/?$#" => "controllers/contact.php", //rewrite ^/contacts/?$ /contact.php last;

  "#^/list(.*)/?$#" => "controllers/list.php", //rewrite ^/list(.*)/?$ /list.php last;
  "#^/video_post(.*)/?$#" => "controllers/video_post.php", //rewrite ^/video_post(.*)/?$ /video_post.php last;

  "#^/post_broadcast(.*)/?$#" => "controllers/post_broadcast.php", //rewrite ^/post_broadcast(.*)/?$ /post_broadcast.php last;
  "#^/download_broadcast(.*)/?$#" => "controllers/download_broadcast.php", //rewrite ^/download_broadcast(.*)/?$ /download_broadcast.php last;
  "#^/broadcast(.*)/?$#" => "controllers/broadcast.php",
  "#^/start_bcast(.*)/?$#" => "controllers/broadcast_start.php", 
  "#^/stop_bcast(.*)/?$#" => "controllers/broadcast_stop.php",  //rewrite ^/broadcast(.*)/?$ /broadcast.php last;
  //"#^/do_broadcast(.*)/?$#" => "controllers/do_broadcast.php", //rewrite ^/do_broadcast(.*)/?$ /do_broadcast.php last;

  "#^/trend_post(.*)/?$#" => "controllers/trend_post.php", //rewrite ^/trend_post(.*)/?$ /trend_post.php last;
  "#^/followings(.*)/?$#" => "controllers/followings.php", //rewrite ^/followings(.*)/?$ /followings.php last;
  "#^/likes(.*)/?$#" => "controllers/likes.php", //rewrite ^/likes(.*)/?$ /likes.php last;
  "#^/followers(.*)/?$#" => "controllers/followers.php", //rewrite ^/followers(.*)/?$ /followers.php last;
  "#^/media(.*)/?$#" => "controllers/media.php", //rewrite ^/media(.*)/?$ /media.php last;

  # Message to Miles
  "#^/message_to_miles/?$#" => "controllers/message_to_miles.php", //rewrite ^/message_to_miles/?$ /message_to_miles.php last;

  # Directory
  "#^/directory/?$#" => "controllers/directory.php", //rewrite ^/directory/?$ /directory.php last;
  "#^/directory/(?P<view>[^/]+)/?$#" => "controllers/directory.php", //rewrite ^/directory/([^/]+)/?$ /directory.php?view=$1 last;
  "#^/directory/(?P<view>[^/]+)/(?P<page>[^/]+)/?$#" => "controllers/directory.php", //rewrite ^/directory/([^/]+)/([^/]+)/?$ /directory.php?view=$1&page=$2 last;

  #Search
  "#^/search/?$#" => "controllers/search.php", //rewrite ^/search/?$ /search.php last;
  "#^/search_people/?$#" => "controllers/search_people.php",

  # Sign(in|up|out)
  "#^/activation/(?P<id>[^/]+)/(?P<token>[^/]+)/?$#" => "controllers/activation.php", //rewrite ^/activation/([^/]+)/([^/]+)/?$ /activation.php?id=$1&token=$2 last;

  # Packages
  "#^/packages/?$#" => "controllers/packages.php", //rewrite ^/packages/?$ /packages.php last;

  # Started
  "#^/started/?$#" => "controllers/started.php", //rewrite ^/started/?$ /started.php last;

  # Messages
  "#^/messages/?$#" => "controllers/messages.php", //rewrite ^/messages/?$ /messages.php last;

  # Notifications
  "#^/notifications/?$#" => "controllers/notifications.php", //rewrite ^/notifications/?$ /notifications.php last;

  # Mentions
  "#^/mentions/?$#" => "controllers/mentions.php", //rewrite ^/mentions/?$ /mentions.php last;

  # Settings
  "#^/settings/?$#" => "controllers/settings.php", //rewrite ^/settings/?$ /settings.php last;
  "#^/settings/(?P<view>[^/]+)/?$#" => "controllers/settings.php", //rewrite ^/settings/([^/]+)/?$ /settings.php?view=$1 last;

  # Posts & Photos
  "#^/posts/(?P<post_id>[^/]+)/?$#" => "controllers/post.php", //rewrite ^/posts/([^/]+)/?$ /post.php?post_id=$1 last;
  "#^/photos/(?P<photo_id>[^/]+)/?$#" => "controllers/photo.php", //rewrite ^/photos/([^/]+)/?$ /photo.php?photo_id=$1 last;

  # Pages
  "#^/pages/?$#" => "controllers/pages.php", //rewrite ^/pages/?$ /pages.php last;
  "#^/pages/(?P<username>[^/]+)/?$#" => "controllers/page.php", //rewrite ^/pages/([^/]+)/?$ /page.php?username=$1 last;
  "#^/pages/(?P<username>[^/]+)/(?P<view>[^/]+)/?$#" => "controllers/page.php", //rewrite ^/pages/([^/]+)/([^/]+)/?$ /page.php?username=$1&view=$2 last;
  "#^/pages/(?P<username>[^/]+)/(?P<view>[^/]+)/(?P<id>[^/]+)/?$#" => "controllers/page.php", //rewrite ^/pages/([^/]+)/([^/]+)/([^/]+)/?$ /page.php?username=$1&view=$2&id=$3 last;

  # Groups
  "#^/groups/?$#" => "controllers/groups.php", //rewrite ^/groups/?$ /groups.php last;
  "#^/groups/(?P<username>[^/]+)/?$#" => "controllers/group.php", //rewrite ^/groups/([^/]+)/?$ /group.php?username=$1 last;
  "#^/groups/(?P<username>[^/]+)/(?P<view>[^/]+)/?$#" => "controllers/group.php", //rewrite ^/groups/([^/]+)/([^/]+)/?$ /group.php?username=$1&view=$2 last;
  "#^/groups/(?P<username>[^/]+)/(?P<view>[^/]+)/(?P<id>[^/]+)/?$#" => "controllers/group.php", //rewrite ^/groups/([^/]+)/([^/]+)/([^/]+)/?$ /group.php?username=$1&view=$2&id=$3 last;

  # Events
  "#^/events/?$#" => "controllers/events.php", //rewrite ^/events/?$ /events.php last;
  "#^/events/(?P<event_id>[^/]+)/?$#" => "controllers/event.php", //rewrite ^/events/([^/]+)/?$ /event.php?event_id=$1 last;
  "#^/events/(?P<event_id>[^/]+)/(?P<view>[^/]+)/?$#" => "controllers/event.php", //rewrite ^/events/([^/]+)/([^/]+)/?$ /event.php?event_id=$1&view=$2 last;
  "#^/events/(?P<event_id>[^/]+)/(?P<view>[^/]+)/(?P<id>[^/]+)/?$#" => "controllers/event.php", //rewrite ^/events/([^/]+)/([^/]+)/([^/]+)/?$ /event.php?event_id=$1&view=$2&id=$3 last;

  # Blogs
  "#^/blogs/?$#" => "controllers/blogs.php", //rewrite ^/blogs/?$ /blogs.php last;

  # Market
  "#^/market/?$#" => "controllers/market.php", //rewrite ^/market/?$ /market.php last;

  # Games
  "#^/games/?$#" => "controllers/games.php", //rewrite ^/games/?$ /games.php last;

  # Admin Panel
  "#^/admincp/?$#" => "controllers/admin.php", //rewrite ^/admincp/?$ /admin.php last;
  "#^/admincp/(?P<view>[^/]+)/?$#" => "controllers/admin.php", //rewrite ^/admincp/([^/]+)/?$ /admin.php?view=$1 last;
  "#^/admincp/(?P<view>[^/]+)/(?P<sub_view>[^/]+)/?$#" => "controllers/admin.php", //rewrite ^/admincp/([^/]+)/([^/]+)/?$ /admin.php?view=$1&sub_view=$2 last;
  "#^/admincp/(?P<view>[^/]+)/(?P<sub_view>[^/]+)/(?P<id>[^/]+)/?$#" => "controllers/admin.php", //rewrite ^/admincp/([^/]+)/([^/]+)/([^/]+)/?$ /admin.php?view=$1&sub_view=$2&id=$3 last;

  # Public scripts
  "#^/api\.php$#" => "api.php",
  "#^/oauth\.php$#" => "oauth.php",
  "#^/paypal\.php$#" => "paypal.php",
  "#^/video_streams\.php$#" => "video_streams.php",

  # twitter_profile
  "#^/twitter_profile/(?P<user_id>[^/]+)/?$#" => "controllers/twitter_profile.php",

  //TODO: resolve this mistake
  # NOT using in nginx rules
  //"#^/install/?$#"                => "install.php", // RewriteRule ^/install/?$ install.php [L]
  //"#^/people/(?P<view>\w+)/?$#" => "people.php",  // RewriteRule ^people/find/?$ people.php?view=find [L]
  //RewriteRule ^people/friend_requests/?$ people.php?view=friend_requests [L]
  //RewriteRule ^people/sent_requests/?$ people.php?view=sent_requests [L]
];

$found_flag = false;
foreach ($rewrite_rules as $rewrite_pattern => $controller_file) {
  if (preg_match($rewrite_pattern, $request_url, $matches)) {

    $keys = array_filter(array_keys($matches), 'is_string');
    if (sizeof($keys) > 0) {
      $_GET += array_intersect_key($matches, array_flip($keys)); //TODO: bad solution write variable to $_GET
    }

    require_once(__DIR__ . '/' . $controller_file);
    $found_flag = true;
    break;
  }
}

if (!$found_flag) {
  # AJAX scripts
  if (preg_match("#^/includes/ajax/([^/]+)/([^/]+)\.php$#", $request_url, $matches)) {
    if (file_exists(__DIR__ . $matches[0])) {
      require_once(__DIR__ . $matches[0]);
    } else {
      http_response_code(404);
    }
  }

  //TODO: bad solution write variable to $_GET
  elseif (preg_match("#^/search/hashtag/([^/]+)/?$#", $request_url, $matches)) {
    //rewrite ^/search/hashtag/([^/]+)/?$ /search.php?query=$1&hashtag=1 last;
    $_GET['query'] = $matches[1];
    $_GET['hashtag'] = 1;
    require_once(__DIR__ . '/controllers/search.php');
  } elseif (preg_match("#^/search/([^/^&]+).+/?$#", $request_url, $matches)) {
    //rewrite ^/search/([^/^&]+).+/?$ /search.php?query=$1&hashtag=0 last;
    $_GET['query'] = $matches[1];
    $_GET['hashtag'] = 0;
    require_once(__DIR__ . '/controllers/search.php');
  } # Sign(in|up|out|reset)
  elseif (preg_match("#^/signin/?$#", $request_url, $matches)) {
    //rewrite ^/signin/?$ /index.php?do=in last;
    $_GET['do'] = 'in';
    require_once(__DIR__ . '/controllers/sign.php');
  } elseif (preg_match("#^/signup/?$#", $request_url, $matches)) {
    //rewrite ^/signup/?$ /index.php?do=up last;
    $_GET['do'] = 'up';
    require_once(__DIR__ . '/controllers/sign.php');
  } elseif (preg_match("#^/signout/?$#", $request_url, $matches)) {
    //rewrite ^/signout/?$ /sign.php?do=out last;
    $_GET['do'] = 'out';
    require_once(__DIR__ . '/controllers/sign.php');
  } elseif (preg_match("#^/reset/?$#", $request_url, $matches)) {
    //rewrite ^/reset/?$ /sign.php?do=reset last;
    $_GET['do'] = 'reset';
    require_once(__DIR__ . '/controllers/sign.php');
  } # Social Logins
  elseif (preg_match("#^/resetmob/?$#", $request_url, $matches)) {
    //rewrite ^/reset/?$ /sign.php?do=reset last;
    $_GET['do'] = 'resetmob';
    require_once(__DIR__ . '/controllers/sign.php');
  } # Mobile App Forgot Password
  elseif (preg_match("#^/connect/([^/]+)/?$#", $request_url, $matches)) {
    //rewrite ^/connect/([^/]+)/?$ /connect.php?do=connect&provider=$1 last;
    $_GET['do'] = 'connect';
    $_GET['provider'] = $matches[1];
    require_once(__DIR__ . '/controllers/connect.php');
  } elseif (preg_match("#^/revoke/([^/]+)/?$#", $request_url, $matches)) {
    //rewrite ^/revoke/([^/]+)/?$ /connect.php?do=revoke&provider=$1 last;
    $_GET['do'] = 'revoke';
    $_GET['provider'] = $matches[1];
    require_once(__DIR__ . '/controllers/connect.php');
  } elseif (preg_match("#^/started/finished/?$#", $request_url, $matches)) {
    //rewrite ^/started/finished?$ /started.php?finished=true last; //NOTICE: add / at the end of pattern
    $_GET['finished'] = 'true';
    require_once(__DIR__ . '/controllers/started.php');
  } # Messages
  elseif (preg_match("#^/messages/new/?$#", $request_url, $matches)) {
    //rewrite ^/messages/new?$ /messages.php?view=new last; //NOTICE: add / at the end of pattern
    $_GET['view'] = 'new';
    require_once(__DIR__ . '/controllers/messages.php');
  } elseif (preg_match("#^/messages/([^/]+)/?$#", $request_url, $matches)) {
    //rewrite ^/messages/([^/]+)/?$ /messages.php?cid=$1 last;
    $_GET['cid'] = $matches[1];
    require_once(__DIR__ . '/controllers/messages.php');
  } # Saved
  elseif (preg_match("#^/saved/?$#", $request_url, $matches)) {
    //rewrite ^/saved/?$ /index.php?view=saved last;
    $_GET['view'] = 'saved';
    require_once(__DIR__ . '/controllers/index.php');
  } # Pages
  elseif (preg_match("#^/pages/liked/?$#", $request_url, $matches)) {
    //rewrite ^/pages/liked/?$ /pages.php?view=liked last;
    $_GET['view'] = 'liked';
    require_once(__DIR__ . '/controllers/pages.php');
  } elseif (preg_match("#^/pages/manage/?$#", $request_url, $matches)) {
    //rewrite ^/pages/manage/?$ /pages.php?view=manage last;
    $_GET['view'] = 'manage';
    require_once(__DIR__ . '/controllers/pages.php');
  } # Groups
  elseif (preg_match("#^/groups/joined/?$#", $request_url, $matches)) {
    //rewrite ^/groups/joined/?$ /groups.php?view=joined last;
    $_GET['view'] = 'joined';
    require_once(__DIR__ . '/controllers/groups.php');
  } elseif (preg_match("#^/groups/manage/?$#", $request_url, $matches)) {
    //rewrite ^/groups/manage/?$ /groups.php?view=manage last;
    $_GET['view'] = 'manage';
    require_once(__DIR__ . '/controllers/groups.php');
  } # Events
  elseif (preg_match("#^/events/going/?$#", $request_url, $matches)) {
    //rewrite ^/events/going/?$ /events.php?view=going last;
    $_GET['view'] = 'going';
    require_once(__DIR__ . '/controllers/events.php');
  } elseif (preg_match("#^/events/interested/?$#", $request_url, $matches)) {
    //rewrite ^/events/interested/?$ /events.php?view=interested last;
    $_GET['view'] = 'interested';
    require_once(__DIR__ . '/controllers/events.php');
  } elseif (preg_match("#^/events/invited/?$#", $request_url, $matches)) {
    //rewrite ^/events/invited/?$ /events.php?view=invited last;
    $_GET['view'] = 'invited';
    require_once(__DIR__ . '/controllers/events.php');
  } elseif (preg_match("#^/events/manage/?$#", $request_url, $matches)) {
    //rewrite ^/events/manage/?$ /events.php?view=manage last;
    $_GET['view'] = 'manage';
    require_once(__DIR__ . '/controllers/events.php');
  } # Blogs
  elseif (preg_match("#^/articles/?$#", $request_url, $matches)) {
    //rewrite ^/articles/?$ /index.php?view=articles last;
    $_GET['view'] = 'articles';
    require_once(__DIR__ . '/controllers/index.php');
  } elseif (preg_match("#^/blogs/new/?$#", $request_url, $matches)) {
    //rewrite ^/blogs/new/?$ /blogs.php?view=new last;
    $_GET['view'] = 'new';
    require_once(__DIR__ . '/controllers/blogs.php');
  } elseif (preg_match("#^/blogs/edit/([^/]+)/?$#", $request_url, $matches)) {
    //rewrite ^/blogs/edit/([^/]+)/?$ /blogs.php?view=edit&post_id=$1 last;
    $_GET['view'] = 'edit';
    $_GET['post_id'] = $matches[1];
    require_once(__DIR__ . '/controllers/blogs.php');
  } elseif (preg_match("#^/blogs/([^/]+)/([^/]+)/?$#", $request_url, $matches)) {
    //rewrite ^/blogs/([^/]+)/([^/]+)/?$ /blogs.php?view=article&post_id=$1 last;
    $_GET['view'] = 'article';
    $_GET['post_id'] = $matches[1];
    require_once(__DIR__ . '/controllers/blogs.php');
  } # Market
  elseif (preg_match("#^/products/?$#", $request_url, $matches)) {
    //rewrite ^/products/?$ /index.php?view=products last;
    $_GET['view'] = 'products';
    require_once(__DIR__ . '/controllers/index.php');
  } elseif (preg_match("#^/market/search/?$#", $request_url, $matches)) {
    //rewrite ^/market/search/?$ /market.php?view=search last;
    $_GET['view'] = 'search';
    require_once(__DIR__ . '/controllers/market.php');
  } elseif (preg_match("#^/market/search/([^/]+)/?$#", $request_url, $matches)) {
    //rewrite ^/market/search/([^/]+)/?$ /market.php?view=search&query=$1 last;
    $_GET['view'] = 'search';
    $_GET['query'] = $matches[1];
    require_once(__DIR__ . '/controllers/market.php');
  } elseif (preg_match("#^/market/search/([^/]+)/([^/]+)/?$#", $request_url, $matches)) {
    //rewrite ^/market/search/([^/]+)/([^/]+)/?$ /market.php?view=search&query=$1&page=$2 last;
    $_GET['view'] = 'search';
    $_GET['query'] = $matches[1];
    $_GET['page'] = $matches[2];
    require_once(__DIR__ . '/controllers/market.php');
  } elseif (preg_match("#^/market/category/([^/]+)/?$#", $request_url, $matches)) {
    //rewrite ^/market/category/([^/]+)/?$ /market.php?view=category&category_id=$1 last;
    $_GET['view'] = 'category';
    $_GET['category_id'] = $matches[1];
    require_once(__DIR__ . '/controllers/market.php');
  } elseif (preg_match("#^/market/category/([^/]+)/([^/]+)/?$#", $request_url, $matches)) {
    //rewrite ^/market/category/([^/]+)/([^/]+)/?$ /market.php?view=category&category_id=$1&category_url=$2 last;
    $_GET['view'] = 'category';
    $_GET['category_id'] = $matches[1];
    $_GET['category_url'] = $matches[2];
    require_once(__DIR__ . '/controllers/market.php');
  } elseif (preg_match("#^/market/category/([^/]+)/([^/]+)/([^/]+)/?$#", $request_url, $matches)) {
    //rewrite ^/market/category/([^/]+)/([^/]+)/([^/]+)/?$ /market.php?view=category&category_id=$1&category_url=$2&page=$3 last;
    $_GET['view'] = 'category';
    $_GET['category_id'] = $matches[1];
    $_GET['category_url'] = $matches[2];
    $_GET['page'] = $matches[3];
    require_once(__DIR__ . '/controllers/market.php');
  } elseif (preg_match("#^/market/([^/]+)/?$#", $request_url, $matches)) {
    //rewrite ^/market/([^/]+)/?$ /market.php?page=$1 last;
    $_GET['page'] = $matches[1];
    require_once(__DIR__ . '/controllers/market.php');
  } # Games
  elseif (preg_match("#^/games/played/?$#", $request_url, $matches)) {
    //rewrite ^/games/played/?$ /games.php?view=played last;
    $_GET['view'] = 'played';
    require_once(__DIR__ . '/controllers/games.php');
  } elseif (preg_match("#^/games/([^/]+)/?$#", $request_url, $matches)) {
    //rewrite ^/games/([^/]+)/?$ /game.php?id=$1 last;
    $_GET['id'] = $matches[1];
    require_once(__DIR__ . '/controllers/game.php');
  } # Profile
  elseif (preg_match("#^/?(?P<username>[^/.]+)$#", $request_url, $matches)) {
    //rewrite ^/?([^/.]+)$ /profile.php?username=$1 last;
    $_GET['username'] = $matches['username'];
    require_once(__DIR__ . '/controllers/profile.php');
  } else {
    $user = new User();
    if ($user->_logged_in) {
      require_once(__DIR__ . '/controllers/index.php');
    } else {
      $_GET['username'] = DEFAULT_PROFILE_USERNAME;
      require_once(__DIR__ . '/controllers/profile.php');
    }
  }
}
