<?php

/**
  * video streams
  *
  * @package Sngine
  * @author Dmitry Evdokimov
  */

// fetch bootstrap
require_once(__DIR__ . '/bootstrap.php');

// check if video streams enabled
if(!$system['video_streaming_enabled']) {
    _error(404);
}

try {
    // get view content
    switch ($_GET['view']) {

        case '':
            // page header
            page_header(__("Video Streams"));
            // get articles
            $articles = $user->get_articles();
            /* assign variables */
            $smarty->assign('articles', $articles);
            break;

        case 'stream':
            // get article
            $stream = $user->get_stream($_GET['stream_id']);
            if(!$stream)  {
                _error(404);
            }
            /* assign variables */
            $smarty->assign('video_stream', $stream);
            // page header
            page_header($stream['title']);
            break;

        case 'edit':
            // page header
            page_header(__("Edit Stream"));
            // get article
            $stream = $user->get_stream($_GET['stream_id']);
            if(!$stream)  {
                _error(404);
            }
            /* assign variables */
            $smarty->assign('stream', $stream);
            break;

        case 'new':
            // user access
            user_access();
            // page header
            page_header(__("Create New Video Stream"));
            break;

        default:
            _error(404);
            break;
    }

    /* assign variables */
    $smarty->assign('view', $_GET['view']);

} catch (Exception $e) {
    _error(__("Error"), $e->getMessage());
}

// page footer
page_footer("video_streams");

?>