<?php
require_once(__DIR__ . '/../bootstrap.php');

if(!$system['contact_enabled']) {
	_error(404);
}

page_header($system['system_title'].' - '.__("Contact Us"));

try {

	$ads = $user->ads('contact');
	$smarty->assign('ads', $ads);

	$widgets = $user->widgets('contact');
	$smarty->assign('widgets', $widgets);

} catch (Exception $e) {
	_error(__("Error"), $e->getMessage());
}

page_footer("contact");

?>
