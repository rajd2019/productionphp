<?php

use function GuzzleHttp\json_encode;

/**
 * profile
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../bootstrap.php');
require_once('TwitterAPIExchange.php');

// Load the cache process
include("../cache/cache.php");

// user access
if(!$system['system_public']) {
	//user_access();
}

// check username
if(is_empty($_GET['username'])) {
    _error(404);
}

try {
        //Get videos from channel by YouTube Data API
	//stream_context_set_default(['http' => ['ignore_errors' => true]]);
	$apiKeyLeft    = 'AIzaSyDm6osEzLSMyZ62sSHlch1Cplq6l8hvJ5M';
	$channelIdLeft  = 'UCO3pO3ykAUybrjv3RBbXEHw';
	$apiKeyRight    = 'AIzaSyDsAWH445F-Vyl69KEceKLbswelAVYWey0';
	$channelIdRight  = 'UCReq0vquaKuxA_ADjNVrcQA';
	$maxResults = 10;
		
	$targetDir = __DIR__ .'/../youtube_videodata';	
	$videoListLeftArr = $user->get_youtube_videos_list($targetDir, $apiKeyLeft, $channelIdLeft, $maxResults, '_left');
	$videoListRightArr = $user->get_youtube_videos_list($targetDir, $apiKeyRight, $channelIdRight, $maxResults, '_right');
	$smarty->assign('videoListLeft', $videoListLeftArr);
	$smarty->assign('videoListRight', $videoListRightArr);	
	$today = date("Y-m-d");
	$yesterday = date('Y-m-d',strtotime("-1 days"));
	$smarty->assign('today', $today);
	$smarty->assign('yesterday', $yesterday);

	/****** get the trending topics from twitter added by yasir 25-10-2018 ***/
		$settings = array(
			'oauth_access_token' => "1047778745631825920-47jvfqdM4JCFaZqoXvmeoIHPqHZxjj",
			'oauth_access_token_secret' => "eY8acPEnSSHDebCUYvIdumFBsWkHuEYa4nOjyMOWGPfDE",
			'consumer_key' => "wghw90Ye4QmPdDWaBaOQ8mwDy",
			'consumer_secret' => "OwScICQpEnIkO3H5ZCM2VO06P1ntlg58rUYlgPfBB0k0OtN4f5"
		);

		$url='https://api.twitter.com/1.1/trends/place.json';
		if($system['language']['code']=='en_us'){
		$getfield = '?id=23424977';//for united states replace id 1 with 23424977
		}else{
			$getfield = '?id=23424977';
		}
		$requestMethod = 'GET';

		$cache_file = md5($_SERVER['REQUEST_URI']) . ".json";

		//if(!isset($_SESSION['trends'])){
		if (empty(is_cached($cache_file))) {	
			//die('here');
			$twitter = new TwitterAPIExchange($settings);
			if($requestMethod=='POST'){
				$result=$twitter->setPostfields($postfields)
				->buildOauth($url, $requestMethod)
				->performRequest();
			}else{
				$result=$twitter->setGetfield($getfield)
				->buildOauth($url, $requestMethod)
				->performRequest();
			}
		
		
			$resultArray= json_decode($result,true);
			//echo'<pre>';
			//print_r($resultArray);die;
			
			
		
			if(count($resultArray['0']['trends']) > 0){
				// Grab the uncached page contents
				$cache_contents = $result;

				// Save it to the cache for next time
				write_cache($cache_file, $cache_contents);

				$_SESSION['trends']=$resultArray['0']['trends'];
				$trendsRaw=$resultArray['0']['trends'];
				//echo 'done';
				//die;
			}
		}else{
			//die('here look');
			$cache_file = md5($_SERVER['REQUEST_URI']) . ".json";
			if (is_cached($cache_file)) {
				$jsonDataTwitter= read_cache($cache_file);
				$trendsRawFull=json_decode($jsonDataTwitter,true);
				$trendsRaw=$trendsRawFull[0]['trends'];
			}

			//$trendsRaw=$_SESSION['trends'];
			//echo'<pre>';
			//print_r($trendsRaw);
			//die('yep');
		}
		
		//die('d');
		
		if(isset($resultArray['errors'][0]['code'])){
			$errorTwitter=$resultArray['errors'][0]['message'];	//Rate limit exceeded
			$smarty->assign('trending_tweets_error', $errorTwitter);
		}else{
			if($trendsRaw){
				$trends = array_slice($trendsRaw, 0, 5, true);
			}
			//echo'<pre>';
			//print_r($trends);
			//die('yep');

			if(sizeof($trends) > 0){
				foreach ($trends as $key => $value) {
					$hash=$value['name'];
					//Get the first character using substr.
					$firstCharacter = substr($hash, 0, 1);
					if($firstCharacter!='#'){
					  $hashName='#'.$hash;
					}else{
						$hashName=$hash;
					}
					$trends[$key]['name']=$hashName;
				}
			}
			
			//echo'<pre>';
			//print_r($trends);
			//die;
			$smarty->assign('trending_tweets', $trends);
		}

		
	/******************************* ENDS HERE ****************************** */
	
$content = "<p>The following information can also apply to USD</p>
<strong>Information regarding illegal assets of State-owned companies/listed companies in the US</strong>

<p>
Fake financial information, fake application document, bank/account transactions
Illegal assets/stocks/bond/fund info
Illegal business activities
</p>
<strong>Information regarding CCP officials, how their families illegally acquired capital and money-laundering activities</strong>
<p>
Traveling history, visa, passport information of CCP officials and their families who conducted money laundering activities or other crimes in the US.
Illegal capital/funds/stocks owned by CCP officials and their families
Transaction records/illegal bank accounts
Evidence of money laundering
Illegal real estate/business projects
Fake documents for Immigration/pollical asylum purposes
</p>
<strong>Evidence regarding human rights/racial/religious persecution</strong>
<p>
Written documents, videos, photos
Documents of detention, freedom restriction and legal sentences from CCP
</p>";
$contentHeading = "RULE OF LAW FUND";

	// if ($system['language']['code']=='zn-tw') {	
		// $contentArray = $user->ms_translate($content,"zh-TW");
		// $contentHeading = $user->ms_translate($contentHeading,"zh-TW");
	// }
	// elseif($system['language']['code']=='zh_cn'){
		// $contentArray = $user->ms_translate($content,"zh");
		// $contentHeading = $user->ms_translate($contentHeading,"zh");
	// }else{
		// $contentArray = $user->ms_translate($content,"en");
		// $contentHeading = $user->ms_translate($contentHeading,"en");
	// }
	
//	$smarty->assign('latest_news', $contentArray);
	$smarty->assign('contentHeading', $contentHeading);


	/******************************* ENDS HERE ****************************** */
	

	// [1] get main profile info
	$get_profile = $db->query(sprintf("SELECT users.* FROM users WHERE user_name = %s", secure($_GET['username']))) or _error(SQL_ERROR_THROWEN);
	if($get_profile->num_rows == 0) {
		_error(404);
	}
	$profile = $get_profile->fetch_assoc();

	//Below code is modified by yasir on 19-11-2018
	$profile['user_biography']=str_replace("Twitter: @KwokMiles","",$profile['user_biography']);

  if($profile['user_website'] !=''){
    $parsed_website_url = parse_url($profile['user_website']);
    if (empty($parsed_website_url['scheme'])) {
        $profile['user_website'] = 'http://' . ltrim($profile['user_website'], '/');
    }
  }
  
	/* check if banned by the system */
	if($user->banned($profile['user_id'])) {
		/*_error(404);*/
		_error(__("Error"), "This profile (".$_GET['username'].") has been banned");
	}
	/* check if blocked by the viewer */
	if($user->blocked($profile['user_id'])) {
		//_error(404);
		redirect('/');
	}
	/* check username case */
	if(strtolower($_GET['username']) == strtolower($profile['user_name']) && $_GET['username'] != $profile['user_name']) {
		redirect('/'.$profile['user_name']);
	}

	/* get profile picture */
	$profile['user_picture_default'] = ($profile['user_picture'])? false : true;
	$profile['user_picture'] = User::get_picture($profile['user_picture'], $profile['user_gender']);
	/* get the connection &  mutual friends */
	if($user->_logged_in && $profile['user_id'] != $user->_data['user_id']) {
		/* get the connection */
		$profile['we_friends'] = (in_array($profile['user_id'], $user->_data['friends_ids']))? true: false;
		$profile['he_request'] = (in_array($profile['user_id'], $user->_data['friend_requests_ids']))? true: false;
		$profile['i_request'] = (in_array($profile['user_id'], $user->_data['friend_requests_sent_ids']))? true: false;
		$profile['i_follow'] = (in_array($profile['user_id'], $user->_data['followings_ids']))? true: false;
    $profile['he_follow'] = (in_array($profile['user_id'], $user->get_followers_ids($user->_data['user_id'])))? true: false;
		/* get mutual friends */
		$profile['mutual_friends_count'] = $user->get_mutual_friends_count($profile['user_id']);
		$profile['mutual_friends'] = $user->get_mutual_friends($profile['user_id']);
	}

	// [2] get view content
	

	switch ($_GET['view']) {

		case '':

			/* get following count */
			$profile['followings_count'] = $user->number_shorten(count($user->get_followings_ids($profile['user_id'])));

			/* get followers count */
			if($profile['user_id'] == '356'){
				$profile['followers_count'] = $user->number_shorten($totoalFollowers);
			}
			else{
				$profile['followers_count'] = $user->number_shorten(count($user->get_followers_ids($profile['user_id'])));
			}

			$profile['broadcast_count'] = 552; //$user->number_shorten(count($user->_get_broadcast_count($profile['user_id'])));



			// get custom fields
			$smarty->assign('custom_fields', $user->get_custom_fields( array("get" => "profile", "user_id" => $profile['user_id']) ));

			/* get friends */
			$profile['friends'] = $user->get_friends($profile['user_id']);
			if(count($profile['friends']) > 0) {
				$profile['friends_count'] = count($user->get_friends_ids($profile['user_id']));
			}

			/* get photos */
			$profile['photos'] = $user->get_photos($profile['user_id']);

			/* get videos */
			$profile['thumbnail'] = $user->get_video_thumbnail($profile['user_id']);

			/* get pages */
			$profile['pages'] = $user->get_pages( array('user_id' => $profile['user_id'], 'results' => $system['min_results_even']) );

			/* get groups */
			$profile['groups'] = $user->get_groups( array('user_id' => $profile['user_id'], 'results' => $system['min_results_even']) );

			/* get events */
			$profile['events'] = $user->get_events( array('user_id' => $profile['user_id'], 'results' => $system['min_results_even']) );

			$profile['photos_count'] = count($user->_get_photo($profile['user_id']));

			$profile['likes_count'] = '6.1M'; //$user->_get_likes($profile['user_id']);

			/* get pinned post */
			$pinned_post = $user->get_post($profile['user_pinned_post']);
			// it comes from user table in database and can be set from front-end
			
      		$pinned_post_id = $pinned_post['post_id'];

      		$posts = $user->get_posts( array('get' => 'posts_profile', 'id' => $profile['user_id']) );

			foreach($posts as $keys=>$values)
					{
						if($values['post_id'] == $pinned_post_id)
						{
							unset($posts[$keys]);
						}
					}

			$smarty->assign('pinned_post', $pinned_post);

			/* prepare publisher */
			$smarty->assign('market_categories', $user->get_market_categories());
			$smarty->assign('feelings', get_feelings());
			$smarty->assign('feelings_types', get_feelings_types());

			/* get posts */
			$count_like_postsIds = $user->get_likes_postsIds($profile['user_id']);
			$count_media_postsIds = $user->get_media_postsIds($profile['user_id']);
			$profile['posts_count'] = '12M'; //count($user->_get_posts($profile['user_id']));

			/* assign variables */
			$smarty->assign('posts', $posts);
			$smarty->assign('count_like_posts', $count_like_postsIds);
			$smarty->assign('count_media_posts', $count_media_postsIds);
			break;

		case 'friends':
			/* get friends */
			$profile['friends'] = $user->get_friends($profile['user_id']);
			if(count($profile['friends']) > 0) {
				$profile['friends_count'] = count($user->get_friends_ids($profile['user_id']));
			}
			break;

		case 'photos':
			/* get photos */
			$profile['photos'] = $user->get_photos($profile['user_id']);
			$profile['photos_count'] = count($user->get_photos($profile['user_id']));
			break;

		case 'albums':
			/* get albums */
			$profile['albums'] = $user->get_albums($profile['user_id']);
			break;

		case 'album':
			/* get album */
			$album = $user->get_album($_GET['id']);
			if(!$album || $album['in_group'] || $album['user_type'] == "page" || ($album['user_type'] == "user" && $album['user_id'] != $profile['user_id'])) {
				_error(404);
			}
			/* assign variables */
			$smarty->assign('album', $album);
			break;

		case 'followers':
			/* get followers count */
			$profile['followers_count'] = count($user->get_followers_ids($profile['user_id']));
			/* get followers */
			if($profile['followers_count'] > 0) {
				$profile['followers'] = $user->get_followers($profile['user_id']);
			}
			break;

		case 'followings':
			/* get followings count */
			$profile['followings_count'] = count($user->get_followings_ids($profile['user_id']));
			/* get followings */
			if($profile['followings_count'] > 0) {
				$profile['followings'] = $user->get_followings($profile['user_id']);
			}
			break;

		case 'likes':
			/* get pages */
			$profile['pages'] = $user->get_pages( array('user_id' => $profile['user_id']) );
			break;

		case 'groups':
			/* get groups */
			$profile['groups'] = $user->get_groups( array('user_id' => $profile['user_id']) );
			break;

		case 'events':
			/* get events */
			$profile['events'] = $user->get_events( array('user_id' => $profile['user_id']) );
			break;

		default:
			_error(404);
			break;
	}

	// [3] profile visit notification
	if($_GET['view'] == "" && $user->_logged_in && $system['profile_notification_enabled']) {
		$user->post_notification( array('to_user_id'=>$profile['user_id'], 'action'=>'profile_visit') );
	}

	// recent rearches
	if(isset($_GET['ref']) && $_GET['ref'] == "qs") {
		$user->add_search_log($profile['user_id'], 'user');
	}

} catch (Exception $e) {
	_error(__("Error"), $e->getMessage());
}

// page header
page_header($profile['user_firstname']." ".$profile['user_lastname']);

// assign variables
$smarty->assign('profile', $profile);
$smarty->assign('view', $_GET['view']);

// page footer
page_footer("profile");

?>
