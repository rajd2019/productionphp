<?php
/**
 * report_to_miles
 * 
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../bootstrap.php');

// check if contact enabled
if(!$system['contact_enabled']) {
	_error(404);
}

// page header
page_header($system['system_title'].' - '.__("Message To Miles"));

try {

	// get ads
	$ads = $user->ads('message_to_miles');
	/* assign variables */
	$smarty->assign('ads', $ads);

	// get widgets
	$widgets = $user->widgets('message_to_miles');
	/* assign variables */
	$smarty->assign('widgets', $widgets);

} catch (Exception $e) {
	_error(__("Error"), $e->getMessage());
}

// page footer
page_footer("message_to_miles");

?>