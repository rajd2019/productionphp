<?php
/**
 * index
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../bootstrap.php');

try {
//echo phpinfo();die;
    // check user logged in
    if (!$user->_logged_in) {

        // page header
        page_header(__("Welcome to") . ' ' . $system['system_title']);

        // get random users
        if ($system['system_random_profiles']) {
            $random_profiles = array();
            $get_random_profiles = $db->query("SELECT user_name, user_firstname, user_lastname, user_gender, user_picture FROM users ORDER BY RAND() LIMIT 4") or _error(SQL_ERROR_THROWEN);
            if ($get_random_profiles->num_rows > 0) {
                while ($random_profile = $get_random_profiles->fetch_assoc()) {
                    $random_profile['user_picture'] = User::get_picture($random_profile['user_picture'], $random_profile['user_gender']);
                    $random_profiles[] = $random_profile;
                }
            }

            /* assign variables */
            $smarty->assign('random_profiles', $random_profiles);
        }
        $username_email = (isset($_COOKIE['username_email'])) ? $_COOKIE['username_email'] : '';
        $smarty->assign('username_email', $username_email);
        // get custom fields
        $smarty->assign('custom_fields', $user->get_custom_fields());

    } else {

        // user access
        user_access();

        
        //$smarty->assign('last_dir', 'home');
        

        // get view content
        switch ($_GET['view']) {
            case '':
                // page header
                page_header(__("Twitter Profile"));
           
                break;

            default:
                _error(404);
                break;
        }
        /* assign variables */
        //$smarty->assign('view', $_GET['view']);
        //$smarty->assign('last_dir', 'home');
    }

} catch (Exception $e) {
    _error(__("Error"), $e->getMessage());
}

$smarty->assign('website_current_lang', $_COOKIE['s_lang']);

$smarty->assign('current_user_feature_status', $user->_data['feature_status']);
// page footer
page_footer("twitter_profile");

?>
