<?php

/**
 * profile
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../bootstrap.php');
require_once('TwitterAPIExchange.php');

// user access
if(!$system['system_public']) {
	//user_access();
}

// check username
if(is_empty($_GET['username'])) {
    _error(404);
}

try {

	/****** get the trending topics from twitter added by yasir 25-10-2018 ***/
		$settings = array(
			'oauth_access_token' => "1047778745631825920-47jvfqdM4JCFaZqoXvmeoIHPqHZxjj",
			'oauth_access_token_secret' => "eY8acPEnSSHDebCUYvIdumFBsWkHuEYa4nOjyMOWGPfDE",
			'consumer_key' => "wghw90Ye4QmPdDWaBaOQ8mwDy",
			'consumer_secret' => "OwScICQpEnIkO3H5ZCM2VO06P1ntlg58rUYlgPfBB0k0OtN4f5"
		);

		$url='https://api.twitter.com/1.1/trends/place.json';
		if($system['language']['code']=='en_us'){
		$getfield = '?id=23424977';//for united states replace id 1 with 23424977
		}else{
			$getfield = '?id=23424977';
		}
		$requestMethod = 'GET';

		$twitter = new TwitterAPIExchange($settings);
		if($requestMethod=='POST'){
			$result=$twitter->setPostfields($postfields)
			->buildOauth($url, $requestMethod)
			->performRequest();
		}else{
			$result=$twitter->setGetfield($getfield)
			->buildOauth($url, $requestMethod)
			->performRequest();
		}
		
		   
		$resultArray= json_decode($result,true);
		$trends=$resultArray['0']['trends'];
		if($resultArray['0']['trends']){
			$trends = array_slice($resultArray['0']['trends'], 0, 5, true);
		}
		
		foreach ($trends as $key => $value) {
			$hash=$value['name'];
			//Get the first character using substr.
            $firstCharacter = substr($hash, 0, 1);
            if($firstCharacter!='#'){
              $hashName='#'.$hash;
            }else{
				$hashName=$hash;
			}
            $trends[$key]['name']=$hashName;
		}
		//echo'<pre>';
		//print_r($trends);
		//die;
		$smarty->assign('trending_tweets', $trends);
	/******************************* ENDS HERE ****************************** */



	/****** get the TOP news with google API added by yasir on 25-10-2018 *****/

	$jsonDATA=file_get_contents('https://newsapi.org/v2/top-headlines?sources=google-news&apiKey=8efed321d22640df9cf64d569d6ef5e2');
	$resultNewsArray=json_decode($jsonDATA,true);
	$newsArray = array_slice($resultNewsArray['articles'], 0, 10, true);
	
	if($system['language']['code']=='en_us'){
		$smarty->assign('latest_news_title','Latest News');
		$smarty->assign('trending_title','Trending Tweets');
	}
	elseif ($system['language']['code']=='zn-tw') {
		foreach($newsArray as $key =>$value){
			$newsArray[$key]['title'] = $user->ms_translate($value['title'],"zh-TW");
		}
		$smarty->assign('latest_news_title',$user->ms_translate('Latest News',"zh-TW"));
		$smarty->assign('trending_title',$user->ms_translate('Trending Tweets',"zh-TW"));
	}
	else{
		foreach($newsArray as $key =>$value){
			$newsArray[$key]['title'] = $user->ms_translate($value['title'],"zh-cn");
		}
		$smarty->assign('latest_news_title',$user->ms_translate('Latest News',"zh-cn"));
		$smarty->assign('trending_title',$user->ms_translate('Trending Tweets',"zh-cn"));
	}
	
	//print_r($newsArray);echo'</pre>';
	//$profile['latest_news']=$newsArray;
	$smarty->assign('latest_news', $newsArray);


	/******************************* ENDS HERE ****************************** */
	

	// [1] get main profile info
	$get_profile = $db->query(sprintf("SELECT users.* FROM users WHERE user_name = %s", secure($_GET['username']))) or _error(SQL_ERROR_THROWEN);
	if($get_profile->num_rows == 0) {
		_error(404);
	}
	$profile = $get_profile->fetch_assoc();

  if($profile['user_website'] !=''){
    $parsed_website_url = parse_url($profile['user_website']);
    if (empty($parsed_website_url['scheme'])) {
        $profile['user_website'] = 'http://' . ltrim($profile['user_website'], '/');
    }
  }
  
	/* check if banned by the system */
	if($user->banned($profile['user_id'])) {
		/*_error(404);*/
		_error(__("Error"), "This profile (".$_GET['username'].") has been banned");
	}
	/* check if blocked by the viewer */
	if($user->blocked($profile['user_id'])) {
		//_error(404);
		redirect('/');
	}
	/* check username case */
	if(strtolower($_GET['username']) == strtolower($profile['user_name']) && $_GET['username'] != $profile['user_name']) {
		redirect('/'.$profile['user_name']);
	}

	/* get profile picture */
	$profile['user_picture_default'] = ($profile['user_picture'])? false : true;
	$profile['user_picture'] = User::get_picture($profile['user_picture'], $profile['user_gender']);
	/* get the connection &  mutual friends */
	if($user->_logged_in && $profile['user_id'] != $user->_data['user_id']) {
		/* get the connection */
		$profile['we_friends'] = (in_array($profile['user_id'], $user->_data['friends_ids']))? true: false;
		$profile['he_request'] = (in_array($profile['user_id'], $user->_data['friend_requests_ids']))? true: false;
		$profile['i_request'] = (in_array($profile['user_id'], $user->_data['friend_requests_sent_ids']))? true: false;
		$profile['i_follow'] = (in_array($profile['user_id'], $user->_data['followings_ids']))? true: false;
    $profile['he_follow'] = (in_array($profile['user_id'], $user->get_followers_ids($user->_data['user_id'])))? true: false;
		/* get mutual friends */
		$profile['mutual_friends_count'] = $user->get_mutual_friends_count($profile['user_id']);
		$profile['mutual_friends'] = $user->get_mutual_friends($profile['user_id']);
	}

	// [2] get view content

	switch ($_GET['view']) {

		case '':

			/* get following count */
			$profile['followings_count'] = $user->number_shorten(count($user->get_followings_ids($profile['user_id'])));

			/* get followers count */
			if($profile['user_id'] == '356'){
				$profile['followers_count'] = $user->number_shorten($totoalFollowers);
			}
			else{
				$profile['followers_count'] = $user->number_shorten(count($user->get_followers_ids($profile['user_id'])));
			}

			$profile['broadcast_count'] = $user->number_shorten(count($user->_get_broadcast_count($profile['user_id'])));



			// get custom fields
			$smarty->assign('custom_fields', $user->get_custom_fields( array("get" => "profile", "user_id" => $profile['user_id']) ));

			/* get friends */
			$profile['friends'] = $user->get_friends($profile['user_id']);
			if(count($profile['friends']) > 0) {
				$profile['friends_count'] = count($user->get_friends_ids($profile['user_id']));
			}

			/* get photos */
			$profile['photos'] = $user->get_photos($profile['user_id']);

			/* get videos */
			$profile['thumbnail'] = $user->get_video_thumbnail($profile['user_id']);

			/* get pages */
			$profile['pages'] = $user->get_pages( array('user_id' => $profile['user_id'], 'results' => $system['min_results_even']) );

			/* get groups */
			$profile['groups'] = $user->get_groups( array('user_id' => $profile['user_id'], 'results' => $system['min_results_even']) );

			/* get events */
			$profile['events'] = $user->get_events( array('user_id' => $profile['user_id'], 'results' => $system['min_results_even']) );

			$profile['photos_count'] = count($user->_get_photo($profile['user_id']));

			$profile['likes_count'] = $user->_get_likes($profile['user_id']);

			/* get pinned post */
			$pinned_post = $user->get_post($profile['user_pinned_post']);
      $pinned_post_id = $pinned_post['post_id'];

      $posts = $user->get_posts( array('get' => 'posts_profile', 'id' => $profile['user_id']) );

      foreach($posts as $keys=>$values)
			{
				if($values['post_id'] == $pinned_post_id)
				{
					unset($posts[$keys]);
				}
			}

			$smarty->assign('pinned_post', $pinned_post);

			/* prepare publisher */
			$smarty->assign('market_categories', $user->get_market_categories());
			$smarty->assign('feelings', get_feelings());
			$smarty->assign('feelings_types', get_feelings_types());

			/* get posts */
			$count_like_postsIds = $user->get_likes_postsIds($profile['user_id']);
			$count_media_postsIds = $user->get_media_postsIds($profile['user_id']);
			$profile['posts_count'] = count($user->_get_posts($profile['user_id']));

			/* assign variables */
			$smarty->assign('posts', $posts);
			$smarty->assign('count_like_posts', $count_like_postsIds);
			$smarty->assign('count_media_posts', $count_media_postsIds);
			break;

		case 'friends':
			/* get friends */
			$profile['friends'] = $user->get_friends($profile['user_id']);
			if(count($profile['friends']) > 0) {
				$profile['friends_count'] = count($user->get_friends_ids($profile['user_id']));
			}
			break;

		case 'photos':
			/* get photos */
			$profile['photos'] = $user->get_photos($profile['user_id']);
			$profile['photos_count'] = count($user->get_photos($profile['user_id']));
			break;

		case 'albums':
			/* get albums */
			$profile['albums'] = $user->get_albums($profile['user_id']);
			break;

		case 'album':
			/* get album */
			$album = $user->get_album($_GET['id']);
			if(!$album || $album['in_group'] || $album['user_type'] == "page" || ($album['user_type'] == "user" && $album['user_id'] != $profile['user_id'])) {
				_error(404);
			}
			/* assign variables */
			$smarty->assign('album', $album);
			break;

		case 'followers':
			/* get followers count */
			$profile['followers_count'] = count($user->get_followers_ids($profile['user_id']));
			/* get followers */
			if($profile['followers_count'] > 0) {
				$profile['followers'] = $user->get_followers($profile['user_id']);
			}
			break;

		case 'followings':
			/* get followings count */
			$profile['followings_count'] = count($user->get_followings_ids($profile['user_id']));
			/* get followings */
			if($profile['followings_count'] > 0) {
				$profile['followings'] = $user->get_followings($profile['user_id']);
			}
			break;

		case 'likes':
			/* get pages */
			$profile['pages'] = $user->get_pages( array('user_id' => $profile['user_id']) );
			break;

		case 'groups':
			/* get groups */
			$profile['groups'] = $user->get_groups( array('user_id' => $profile['user_id']) );
			break;

		case 'events':
			/* get events */
			$profile['events'] = $user->get_events( array('user_id' => $profile['user_id']) );
			break;

		default:
			_error(404);
			break;
	}

	// [3] profile visit notification
	if($_GET['view'] == "" && $user->_logged_in && $system['profile_notification_enabled']) {
		$user->post_notification( array('to_user_id'=>$profile['user_id'], 'action'=>'profile_visit') );
	}

	// recent rearches
	if(isset($_GET['ref']) && $_GET['ref'] == "qs") {
		$user->add_search_log($profile['user_id'], 'user');
	}

} catch (Exception $e) {
	_error(__("Error"), $e->getMessage());
}

// page header
page_header($profile['user_firstname']." ".$profile['user_lastname']);

// assign variables
$smarty->assign('profile', $profile);
$smarty->assign('view', $_GET['view']);

// page footer
page_footer("profile");

?>
