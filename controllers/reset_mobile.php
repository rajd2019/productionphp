<?php
/**
 * ajax -> core -> forget password
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../bootstrap.php');


// check user logged in
if ($user->_logged_in) {
    return_json(['callback' => 'window.location.reload();']);
}

// forget password
try {
    
    modal("#forget-password-confirm", "{email: 'apatle@tiuconsulting.com'}");
} catch (Exception $e) {
    return_json(['error' => true, 'message' => $e->getMessage()]);
}