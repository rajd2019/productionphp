<?php

use function GuzzleHttp\json_encode;

/**
 * profile
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../bootstrap.php');

// check userid
if(is_empty($_GET['user_id']) || is_empty($_GET['other_user_id'])) {
    _error(404);
}

try {
	$user_id = base64_decode($_GET['user_id']);
	$other_user_id = base64_decode($_GET['other_user_id']);
	$options = array();
	$profile = array();
	//Get user details
	$get_profile = $db->query(sprintf("SELECT user_name FROM users WHERE user_id = %s", secure($other_user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
	if($get_profile->num_rows == 0) {
		_error(404);
	}
	$profile = $get_profile->fetch_assoc();
	$profile['user_id'] = $user_id;
	$profile['other_user_id'] = $other_user_id; 
	
	//Get report option
	$parent_options = $db->query(sprintf("SELECT * FROM report_options WHERE option_id = 0")) or _error(SQL_ERROR_THROWEN);
	if ($parent_options->num_rows > 0) {
		while ($option = $parent_options->fetch_assoc()) {
			$options[] = $option;
		}
    }
	$parent_options = $options;
	
	/* check if A muted B */
	$check = $db->query(sprintf('SELECT * FROM users_mute WHERE (user_id = %1$s AND mute_id = %2$s)', secure($user_id, 'int'), secure($other_user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($check->num_rows > 0) {
            $mute_status = true;
        }else{
			$mute_status = false;
		}
	$profile['mute_status'] = $mute_status;

	/* check if there is any blocking between the viewer & the target */
	$check = $db->query(sprintf('SELECT * FROM users_blocks WHERE (user_id = %1$s AND blocked_id = %2$s) OR (user_id = %2$s AND blocked_id = %1$s)', secure($user_id, 'int'), secure($other_user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
		if ($check->num_rows > 0) {
            $block_status = true;
        }else{
			$block_status = false;
		}
	$profile['block_status'] = $block_status;

	// Get the current user language code
	$query = $db->query(sprintf('SELECT set_mobile_language FROM users WHERE user_id = %s', secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
		if($query->num_rows > 0){
			$row = $query->fetch_assoc();
			$lang_code = $row['set_mobile_language'];
		}else{
			$lang_code = "zh_cn";
		}
	$profile['lang_code'] = $lang_code;
	
	$get_language = $db->query(sprintf('SELECT * FROM system_languages WHERE code = %s', secure($lang_code))) or _error(SQL_ERROR_THROWEN);
	if($get_language->num_rows > 0){
		$language = $get_language->fetch_assoc();
		$system['language'] = $language;
        T_setlocale(LC_MESSAGES, $system['language']['code']);
	}
	
	
} catch (Exception $e) {
	_error(__("Error"), $e->getMessage());
}


// assign variables
$smarty->assign('profile', $profile);
$smarty->assign('parent_options', $parent_options);

// page footer
page_footer("report");

?>
