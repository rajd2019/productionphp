<?php
/**
 * index
 * 
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../bootstrap.php');

try {

page_header(__("Welcome to").' '.$system['system_title']);

} catch (Exception $e) {
	_error(__("Error"), $e->getMessage());
}
	
// page footer
page_footer("broadcast_list");
