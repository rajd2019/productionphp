<?php
require_once(__DIR__ . '/../includes/config.php');

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => SYS_URL_API . "/api.php?method=getAllBroadcast",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    "cache-control: no-cache",
    "content-type: application/json",
    "postman-token: 814836f3-befc-9d41-9820-a4b9c139e398"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  header('Content-Type: application/json');
  echo $response;
}