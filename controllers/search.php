<?php
/**
 * search
 * 
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../bootstrap.php');

// user access
if(!$system['system_public']) {
	user_access();
}

// page header
page_header($system['system_title'].' - '.__("Search"));

try {

    
	// search
	if(isset($_GET['query'])) {

		$query = $_GET['query'];

	   $offset = $_REQUEST['ofset'];
		/* get results */

               $QArray=  explode('&', $query);
				$query = $QArray[0];
           $results = $user->search($query);
		
		$trend_results = $user->trend_search($query);
		$follow = $_REQUEST['view']; 
		//die;
 		$offset++;;
		/* assign variables */
		$smarty->assign('query', $query);
		$smarty->assign('results', $results);

		$smarty->assign('follow', $follow);
		$smarty->assign('trend_results', $trend_results);
		$smarty->assign('offset', $offset);
		$smarty->assign('results_num', count($results));
	}

	// get ads
	$ads = $user->ads('search');
	/* assign variables */
	$smarty->assign('ads', $ads);

	// get widgets
	$widgets = $user->widgets('search');
	/* assign variables */
	$smarty->assign('widgets', $widgets);

} catch (Exception $e) {
	_error(__("Error"), $e->getMessage());
}

// page footer
page_footer("search");

?>
