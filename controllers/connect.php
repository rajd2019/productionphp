<?php
/**
 * connect
 *
 * @package Sngine
 * @author Zamblek
 */

// fetch bootstrap
require_once(__DIR__ . '/../bootstrap.php');

function validateProvider($provider)
{
    global $system;

    if (!array_key_exists($provider . '_login_enabled', $system)) {
        _error(404);
    }
    if (!$system['social_login_enabled']) {
        _error(404);
    }
    if (!$system[$provider . '_login_enabled']) {
        _error(404);
    }
    if (!in_array($provider, ['facebook', 'twitter', 'google', 'instagram', 'linkedin', 'vkontakte'])) {
        _error(404);
    }
}


function revokeSocialLoginAccess($provider, $user)
{
    global $db;
    $social_id = $provider . "_id";
    $social_connected = $provider . "_connected";
    $db->query(sprintf("UPDATE users SET $social_connected = '0', $social_id = NULL WHERE user_id = %s", secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
}

validateProvider($provider);

// connect & revoke
try {
    $provider = $_REQUEST["provider"];

    $do = $_REQUEST['do'];

    if ($do == 'connect' && isEnabledProvider($provider)) {
        $hybridauth = new Hybrid_Auth($config);
        $adapter = $hybridauth->authenticate($provider);
        $user_profile = $adapter->getUserProfile();
        $user->socail_login($provider, $user_profile);

    } elseif ($do == 'revoke' && isEnabledProvider($provider)) {
        // user access
        user_access();
        revokeSocialLoginAccess($provider, $user);
        redirect('/settings/linked');
    } else {
        _error(404);
    }
} catch (Exception $e) {
    _error(__("Error"), $e->getMessage());
}
